USE `scr_ceaps`;
DROP TABLE IF EXISTS `kardex_copy`;



ALTER TABLE `detalle_productos`
ADD INDEX `clave_lote` (`cla_pro`, `lot_pro`) USING HASH ;

/*ALTER TABLE `receta`
DROP INDEX `folio` ; */

ALTER TABLE `receta`
ADD INDEX `folio` (`fol_rec`) USING HASH ;

ALTER TABLE `tb_codigob`
DROP INDEX `CB1` ,
ADD INDEX `CB1` (`F_Clave`, `F_Lote`, `F_Cadu`) USING HASH ; 

ALTER TABLE `inventario`
DROP INDEX `det_pro_fk_inv` ,
ADD INDEX `det_pro_fk_inv` (`det_pro`, `cant`) USING BTREE ;

ALTER TABLE `detalle_productos`
DROP INDEX `cla_pro_fk2` ,
ADD INDEX `cla_pro_fk2` (`cla_pro`, `lot_pro`) USING HASH ;

ALTER TABLE `detreceta`
DROP INDEX `id_rec_fk2` ,
ADD INDEX `id_rec_fk2` (`id_rec`, `det_pro`) USING HASH ;

ALTER TABLE `inventario`
MODIFY COLUMN `id_inv`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT AFTER `cant`;

ALTER TABLE `inventario`
DROP INDEX `det_pro_fk_inv` ,
ADD INDEX `det_pro_fk_inv` (`det_pro`, `cant`) USING BTREE ;

ALTER TABLE `tb_codigob`
ADD INDEX `codigo_barras` (`F_Cb`) USING HASH ;

ALTER TABLE `detreceta`
ADD INDEX `status` (`status`) USING BTREE ;

DELIMITER $$
DROP PROCEDURE IF EXISTS Alter_Table$$

CREATE PROCEDURE Alter_Table ()
BEGIN

IF (
	(SELECT
		COUNT(*) AS index_exists
	FROM
		information_schema.statistics
	WHERE
		TABLE_SCHEMA = DATABASE ()
	AND table_name = 'detalle_productos'
	AND index_name = 'clave_lote') > 0
) THEN
	ALTER TABLE `detalle_productos` DROP INDEX `clave_lote` ;
ELSE
	ALTER TABLE `detalle_productos` ADD INDEX `clave_lote` (`cla_pro`, `lot_pro`) USING HASH;
END
IF ;
IF (
	(SELECT
		COUNT(*) AS index_exists
	FROM
		information_schema.statistics
	WHERE
		TABLE_SCHEMA = DATABASE ()
	AND table_name = 'receta'
	AND index_name = 'folio') > 0
) THEN
	ALTER TABLE `receta` DROP INDEX `folio` ;
ELSE
	ALTER TABLE `receta` ADD INDEX `folio` (`fol_rec`) USING HASH ;
END
IF ;
IF NOT EXISTS (
	SELECT
		NULL
	FROM
		INFORMATION_SCHEMA. COLUMNS
	WHERE
		table_name = 'indices'
	AND table_schema = 'scr_ceaps'
	AND column_name = 'inicio_jornada'
) THEN
	ALTER TABLE `indices` ADD `inicio_jornada` TIMESTAMP NULL AFTER `paramRec`,
	ADD COLUMN `fin_jornada` TIMESTAMP NULL AFTER `inicio_jornada`,
	ADD COLUMN `numero_por_dia` INT (1) UNSIGNED NULL DEFAULT 3 AFTER `fin_jornada`,
	ADD COLUMN `maximo_dias` INT (1) UNSIGNED NULL DEFAULT 7 AFTER `numero_por_dia` ;
END
IF ;
IF NOT EXISTS (
	SELECT
		NULL
	FROM
		INFORMATION_SCHEMA. COLUMNS
	WHERE
		table_name = 'indices'
	AND table_schema = 'scr_ceaps'
	AND column_name = 'ciclicos'
)
THEN
ALTER TABLE `indices` ADD COLUMN `ciclicos` INT (10) NULL AFTER `maximo_dias` ; 
UPDATE `indices`SET `ciclicos` = '1' ;
ELSE
	ALTER TABLE `indices` MODIFY COLUMN `ciclicos` INT (10) NULL AFTER `maximo_dias` ;
    ALTER TABLE `scr_ceaps`.`inventariociclico` 
DROP FOREIGN KEY `Fk_TipCiclico`;
ALTER TABLE `scr_ceaps`.`inventariociclico` 
DROP INDEX `Fk_TipCiclico` ;
ALTER TABLE `scr_ceaps`.`inventariociclicoHistorico` 
DROP FOREIGN KEY `Fk_FCatalogoCiclico`;
ALTER TABLE `scr_ceaps`.`inventariociclicoHistorico` 
DROP INDEX `Fk_FCatalogoCiclico` ;
END
IF ; 
END$$
DELIMITER ;


CALL Alter_Table ();

DROP PROCEDURE Alter_Table;

-- ----------------------------
-- Table structure for catalogoCiclicos
-- ----------------------------
DROP TABLE IF EXISTS `catalogoCiclicos`;
CREATE TABLE `catalogoCiclicos` (
  `FId` int(10) NOT NULL AUTO_INCREMENT,
  `FTipoCiclico` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`FId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of catalogoCiclicos
-- ----------------------------
INSERT INTO `catalogoCiclicos` VALUES ('1', 'NORMAL');
INSERT INTO `catalogoCiclicos` VALUES ('2', 'ALTO COSTO');
INSERT INTO `catalogoCiclicos` VALUES ('3', 'CONTROLADO');
-- ----------------------------
-- Table structure for inventariociclico
-- ----------------------------
DROP TABLE IF EXISTS `inventariociclico`;
CREATE TABLE `inventariociclico` (
  `FId` int(10) NOT NULL AUTO_INCREMENT,
  `FFechaInventario` date NOT NULL,
  `FHora` time NOT NULL,
  `FDetalleProducto` int(11) NOT NULL,
  `FFisico` int(10) NOT NULL,
  `FInventario` int(10) NOT NULL DEFAULT '0',
  `FDiferencia` int(10) NOT NULL DEFAULT '0',
  `FTipoCiclico` int(10) NOT NULL,
  PRIMARY KEY (`FId`),
  KEY `Fk_FId` (`FDetalleProducto`),
  KEY `IndexFecha` (`FFechaInventario`) USING HASH,
  KEY `Fk_TipCiclico` (`FTipoCiclico`),
  CONSTRAINT `Fk_FId` FOREIGN KEY (`FDetalleProducto`) REFERENCES `detalle_productos` (`det_pro`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Fk_TipCiclico` FOREIGN KEY (`FTipoCiclico`) REFERENCES `catalogoCiclicos` (`FId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1002 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for inventariociclicoHistorico
-- ----------------------------
DROP TABLE IF EXISTS `inventariociclicoHistorico`;
CREATE TABLE `inventariociclicoHistorico` (
  `FId` int(10) NOT NULL AUTO_INCREMENT,
  `FFechaInventario` date NOT NULL,
  `FHora` time NOT NULL,
  `FDetalleProducto` int(11) NOT NULL,
  `FFisico` int(10) NOT NULL,
  `FInventario` int(10) NOT NULL DEFAULT '0',
  `FDiferencia` int(10) NOT NULL DEFAULT '0',
  `FFolioCiclicos` int(10) NOT NULL,
  `FTipoCiclico` int(10) NOT NULL,
  PRIMARY KEY (`FId`),
  KEY `Fk_FId` (`FDetalleProducto`),
  KEY `IndexFecha` (`FFechaInventario`) USING HASH,
  KEY `Fk_FCatalogoCiclico` (`FTipoCiclico`),
  CONSTRAINT `Fk_FCatalogoCiclico` FOREIGN KEY (`FTipoCiclico`) REFERENCES `catalogoCiclicos` (`FId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `inventariociclicoHistorico_ibfk_1` FOREIGN KEY (`FDetalleProducto`) REFERENCES `detalle_productos` (`det_pro`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1307 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS=0;
-- Actualizacion indices ---
/*ALTER TABLE `detalle_productos`
DROP INDEX `clave_lote` ; */

ALTER TABLE `inventariociclico` DROP FOREIGN KEY `Fk_FId`;
ALTER TABLE `inventariociclicoHistorico` DROP FOREIGN KEY `inventariociclicoHistorico_ibfk_1`;

ALTER TABLE `inventariociclico`
MODIFY COLUMN `FDetalleProducto`  int(11) NOT NULL AFTER `FHora`;

ALTER TABLE `inventariociclicoHistorico`
MODIFY COLUMN `FDetalleProducto`  int(11) NOT NULL AFTER `FHora`;


SET FOREIGN_KEY_CHECKS=0;

-- Elimacion relaciones ---
ALTER TABLE `inventario` DROP FOREIGN KEY `inventario_ibfk_1`;

ALTER TABLE `inventario_inicial` DROP FOREIGN KEY `inventario_inicial_ibfk_1`;

ALTER TABLE `kardex` DROP FOREIGN KEY `kardex_ibfk_2`;

ALTER TABLE `kardex` DROP FOREIGN KEY `kardex_ibfk_1`;

ALTER TABLE `detreceta` DROP FOREIGN KEY `detreceta_ibfk_1`;

ALTER TABLE `receta` DROP FOREIGN KEY `receta_ibfk_3`;

-- Modificacion campos ---

ALTER TABLE `scr_ceaps`.`detalle_productos` 
CHANGE COLUMN `det_pro` `det_pro` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ;



ALTER TABLE `inventario_inicial`
MODIFY COLUMN `det_pro`  int(11) UNSIGNED NULL DEFAULT NULL AFTER `cla_uni`;

ALTER TABLE `kardex`
MODIFY COLUMN `det_pro`  int(11) UNSIGNED NULL DEFAULT NULL AFTER `id_rec`;

ALTER TABLE `kardex`
MODIFY COLUMN `id_usu`  int(11) UNSIGNED NULL DEFAULT NULL AFTER `obser`;

ALTER TABLE `detreceta`
MODIFY COLUMN `det_pro`  int(11) UNSIGNED NULL DEFAULT NULL AFTER `fol_det`;

ALTER TABLE `receta`
MODIFY COLUMN `id_usu`  int(11) UNSIGNED NOT NULL DEFAULT 0 AFTER `id_tip`;

ALTER TABLE `usuarios`
MODIFY COLUMN `id_usu`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST ;

ALTER TABLE `scr_ceaps`.`inventario` 
CHANGE COLUMN `det_pro` `det_pro` INT(11) UNSIGNED NULL DEFAULT NULL ;

-- Se añaden relaciones ---
ALTER TABLE `inventario` ADD FOREIGN KEY (`det_pro`) REFERENCES `detalle_productos` (`det_pro`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `inventario_inicial` ADD FOREIGN KEY (`det_pro`) REFERENCES `detalle_productos` (`det_pro`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `kardex` ADD FOREIGN KEY (`det_pro`) REFERENCES `detalle_productos` (`det_pro`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `kardex` ADD FOREIGN KEY (`id_usu`) REFERENCES `usuarios` (`id_usu`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `detreceta` ADD FOREIGN KEY (`det_pro`) REFERENCES `detalle_productos` (`det_pro`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `receta` ADD FOREIGN KEY (`id_usu`) REFERENCES `usuarios` (`id_usu`) ON DELETE NO ACTION ON UPDATE CASCADE;

SET FOREIGN_KEY_CHECKS=1;
-- Insercion catalago unidades hibridas ---
DELETE FROM `scr_ceaps`.`productosnivel` WHERE (`nivel`='20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('0103', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('0104', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('0107', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('0109', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('0408', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('0429', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('0474', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('0572', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('0574', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('0596', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('0597', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('0599', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('0655', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('0657', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('0804', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('0813', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('0891', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1006', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1007', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1042', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1050', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1051', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1095', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1206', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1207', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1224', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1242', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1263', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1271', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1272', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1308.01', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1344', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1561', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1562', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1566', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1703', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1706', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1911', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1937', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1939', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('1971', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2018', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2024', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2096', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2111', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2128', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2132', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2133', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2144', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2188', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2191', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2230', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2247', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2301', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2307', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2462', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2463', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2500', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2501', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2520', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2540', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2608', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2613', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2707', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2714', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2814', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2821', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('2823', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('3111', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('3132', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('3407', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('3417', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('3422', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('3451', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('3604', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('3605', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('3607', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('3608', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('3609', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('3616', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('3622', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('4024.02', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('4095', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('4124.01', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('4148', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('4158', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('4158.01', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('4161', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('4162', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('4255', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('4263', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('4302', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('4308.01', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('4330', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('4356.01', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('4359', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('4376', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('4484', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('5106', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('5165', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('5186', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('5309.02', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('5395', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('5501', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('5505', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('5621', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('600040109', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('600340103', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('600580153', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('600660039', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('600660062', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('600660666', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('600660757', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('600660906', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('600660914', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('600820104', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('600880017', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('601252653', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('601252836', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('601660103', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('601670466', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('601680085', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('601684277', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('601686611', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('602030363', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('602030405', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('604360057', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('604360107', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('604560300', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('604560318', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('604560334', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('604560367', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('604560391', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('604610154', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('604610162', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('605430115', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('605500016', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('605500024', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('605500222', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('605500354', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('605500438', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('605502186', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('606210482', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('606210524', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('606810067', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('608410478', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('608410619', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('608690152', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('608690202', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('609040100', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('609090956', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('609530555', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('609530571', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('609532825', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('609532858', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('609532866', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('609532874', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('909091', '20');
INSERT INTO `productosnivel` (clave, nivel) VALUES ('9968', '20');
UPDATE `productos` SET f_status='A' WHERE  cla_pro IN (SELECT clave FROM productosnivel WHERE nivel=20);

SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `apartamiento`;
CREATE TABLE `apartamiento` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `detalle_producto` int(10) unsigned NOT NULL,
  `detalle_receta` int(11) NOT NULL,
  `cantidad` int(10) unsigned DEFAULT NULL,
  `fecha_actualizacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(3) unsigned DEFAULT NULL,
  `tipo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `producto_status` (`detalle_producto`,`status`) USING HASH,
  KEY `status` (`status`),
  KEY `detalle_receta` (`detalle_receta`),
  CONSTRAINT `apartamiento_ibfk_2` FOREIGN KEY (`status`) REFERENCES `estado_apartamiento` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `apartamiento_ibfk_3` FOREIGN KEY (`detalle_receta`) REFERENCES `detreceta` (`fol_det`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `estado_apartamiento`;
CREATE TABLE `estado_apartamiento` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of estado_apartamiento
-- ----------------------------
INSERT INTO `estado_apartamiento` VALUES ('3', 'DISPENSADO');
INSERT INTO `estado_apartamiento` VALUES ('1', 'APARTADO');
INSERT INTO `estado_apartamiento` VALUES ('2', 'CANCELADO');

ALTER 
ALGORITHM=UNDEFINED 
DEFINER=`root`@`localhost` 
SQL SECURITY DEFINER 
VIEW `recetas` AS 
SELECT
dr.fol_det AS fol_det,
r.fol_rec AS fol_rec,
p.nom_com AS nom_com,
r.fecha_hora AS fecha_hora,
pr.cla_pro AS cla_pro,
pr.des_pro AS des_pro,
dr.can_sol AS can_sol,
dr.cant_sur AS cant_sur,
r.transito AS transito,
r.id_rec AS id_rec,
r.baja AS baja,
r.tip_cons AS tip_cons,
dr.indicaciones AS indicaciones,
m.nom_com AS medico,
dp.lot_pro AS lote,
dp.cad_pro AS caducidad,
dr.id_cau AS cause,
m.cedulapro AS cedula,
m.rfc AS rfc,
p.expediente AS expe,
p.num_afi AS afi,
p.sexo AS sexo,
p.tip_cob AS tip_cob,
(
		YEAR (curdate()) - YEAR (`p`.`fec_nac`)
	) AS fechan,
u.des_uni AS uni,
u.claisem AS isem,
u.clajur AS juris,
u.cla_uni AS cla_uni,
u.domic AS domic,
u.licencia AS licencia,
r.id_tip AS id_tip,
dp.det_pro AS det_pro,
dr.fec_sur AS fec_sur,
dp.id_ori AS id_ori,
concat(
		`pr`.`cla_pro`,
		' - ',
		`pr`.`des_pro`
	) AS clavepro,
concat(
		'LOTE: ',
		`dp`.`lot_pro`,
		' - CADUCIDAD: ',
		`dp`.`cad_pro`
	) AS lotepro,
ser.nom_ser AS nom_ser,
usu.nombre AS nombre,
dr.id_cau2 AS cause2,
r.id_usu AS id_usu,
(
		`pr`.`cos_pro` * `dr`.`cant_sur`
	) AS costo,
dr.`status` AS `status`,
dr.baja AS baja_detalle
FROM
	(
		(
			(
				(
					(
						(
							(
								(
									`receta` `r`
									JOIN `pacientes` `p`
								)
								JOIN `detreceta` `dr`
							)
							JOIN `detalle_productos` `dp`
						)
						JOIN `productos` `pr`
					)
					JOIN `medicos` `m`
				)
				JOIN `unidades` `u`
			)
			JOIN `servicios` `ser`
		)
		JOIN `usuarios` `usu`
	)
WHERE
	(
		(`p`.`id_pac` = `r`.`id_pac`)
		AND (`r`.`id_rec` = `dr`.`id_rec`)
		AND (
			`dr`.`det_pro` = `dp`.`det_pro`
		)
		AND (
			`dp`.`cla_pro` = `pr`.`cla_pro`
		)
		AND (`r`.`cedula` = `m`.`cedula`)
		AND (
			`r`.`id_usu` = `usu`.`id_usu`
		)
		AND (
			`r`.`id_ser` = `ser`.`id_ser`
		)
		AND (
			`r`.`id_usu` = `usu`.`id_usu`
		)
		AND (
			`usu`.`cla_uni` = `u`.`cla_uni`
		)
	) ;


-- Inventarios ciclicos --
SET FOREIGN_KEY_CHECKS=0;

-- Actualización para backup drive

ALTER TABLE `indices`
ADD COLUMN `tipoCaptura`  int(1) NULL AFTER `ciclicos`;

UPDATE `indices` SET `tipoCaptura` = '2' LIMIT 1;

DELETE cb FROM 
tb_codigob AS cb
LEFT JOIN productos AS p ON p.cla_pro = cb.F_Clave
WHERE
p.cla_pro IS NULL;

ALTER TABLE `tb_codigob` ADD FOREIGN KEY (`F_Clave`) REFERENCES `productos` (`cla_pro`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `productosnivel` ADD FOREIGN KEY (`clave`) REFERENCES `productos` (`cla_pro`) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELETE cdm
FROM
	tbcdm AS cdm
LEFT JOIN productos AS p ON p.cla_pro = cdm.cla_pro
WHERE
	p.cla_pro IS NULL;

ALTER TABLE `tbcdm` ADD FOREIGN KEY (`cla_pro`) REFERENCES `productos` (`cla_pro`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `usuarios`
MODIFY COLUMN `user`  varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL AFTER `nombre`;

DELETE r FROM
registro_entradas AS r
LEFT JOIN usuarios AS u ON u.`user` = r.`user`
WHERE
u.`user` IS NULL;

ALTER TABLE `registro_entradas` ADD FOREIGN KEY (`user`) REFERENCES `usuarios` (`user`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `reporte_abasto`
MODIFY COLUMN `clave`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `id`;

DELETE ra FROM 
reporte_abasto AS ra
LEFT JOIN productos AS p ON p.cla_pro = ra.clave
WHERE
p.cla_pro IS NULL;

ALTER TABLE `reporte_abasto` ADD FOREIGN KEY (`clave`) REFERENCES `productos` (`cla_pro`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `carga_abasto`
MODIFY COLUMN `clave`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `id`;

ALTER TABLE `carga_abasto` ADD FOREIGN KEY (`clave`) REFERENCES `productos` (`cla_pro`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `reabastecimiento`
MODIFY COLUMN `clave`  varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `id`;

ALTER TABLE `reabastecimiento` ADD FOREIGN KEY (`clave`) REFERENCES `productos` (`cla_pro`) ON DELETE NO ACTION ON UPDATE NO ACTION;

DROP TABLE IF EXISTS `catalogo_descripcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogo_descripcion` (
  `clave` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `descripcion` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogo_descripcion`
--


/*!40000 ALTER TABLE `catalogo_descripcion` DISABLE KEYS */;
INSERT INTO `catalogo_descripcion` VALUES ('0005','MASCARILLA DESECHABLE TIPO NIOSH N95 CON UN NIVEL DE EFICACIA DE FILTRO DE 95% CONTRA PARTICULAS.'),('0101','ÁCIDO ACETILSALICÍLICO TABLETA. Cada tableta contiene: Ácido acetilsalicílico 500 mg.'),('0103','ÁCIDO ACETILSALICÍLICO TABLETA SOLUBLE O EFERVESCENTE. Cada tableta soluble o efervescente contiene: Ácido acetilsalicílico 300 mg. '),('0104','PARACETAMOL TABLETA. Cada tableta contiene: Paracetamol 500 mg. '),('0105','PARACETAMOL SUPOSITORIO. Cada supositorio contiene: Paracetamol 300 mg. '),('0106','PARACETAMOL SOLUCIÓN ORAL. Cada ml. contiene: Paracetamol 100 mg. '),('0107','DEXTROPROPOXIFENO CÁPSULA O COMPRIMIDO. Cada cápsula o comprimido contiene: Clorhidrato  de dextropropoxifeno 65 mg. '),('0108','METAMIZOL SÓDICO COMPRIMIDO. Cada comprimido contiene: Metamizol sódico 500 mg. '),('0109','METAMIZOL SÓDICO, SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Metamizol sódico 1 g. '),('0132.01','NALBUFINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Clorhidrato  de nalbufina\n10 mg. '),('0202','DIAZEPAM SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Diazepam 10 mg. '),('0204','ATROPINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Sulfato de atropina 1 mg. '),('0221','TIOPENTAL SÓDICO SOLUCIÓN INYECTABLE. Cada frasco ámpula con polvo contiene: Tiopental  sódico 0.5 g.'),('0226','KETAMINA SOLUCIÓN INYECTABLE. Cada frasco ámpula contiene: Clorhidrato  de ketamina equivalente a 500 mg. de ketamina '),('0232','ISOFLURANO LIQUIDO CADA ENVASE CONTIENE ISOFLURANO 100 ML'),('0233','SEVOFLURANO LIQUIDO O SOLUCIÓN. Cada envase contiene: Sevoflurano 250 ml. '),('0242','FENTANILO SOLUCIÓN INYECTABLE. Cada ampolleta o frasco ámpula contiene: Citrato  de fentanilo equivalente a 0.5 mg. de fentanilo.'),('0243','ETOMIDATO SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Etomidato 20 mg. '),('0246','PROPOFOL EMULSIÓN INYECTABLE. Cada ampolleta o frasco ámpula contiene: Propofol\n200 mg. En emulsión con edetato disódico (dihidratado).  '),('0247.01','DEXMEDETOMIDINA SOLUCIÓN INYECTABLE. Cada frasco ámpula contiene: Clorhidrato  de dexmedetomidina  200 µg. '),('0252','CLORURO DE SUXAMETONIO, SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Cloruro de suxametonio 40 mg. '),('0254','VECURONIO SOLUCIÓN INYECTABLE. Cada frasco ámpula con liofilizado contiene: Bromuro de vecuronio 4 mg. '),('0260.02','LIDOCAÍNA GEL. Cada ml. contiene: Clorhidrato  de lidocaína 20 mg. '),('0261','LIDOCAÍNA SOLUCIÓN INYECTABLE AL 1%. Cada frasco ámpula contiene: Clorhidrato  de lidocaína 500 mg. '),('0262','LIDOCAÍNA SOLUCIÓN INYECTABLE AL 2%. Cada frasco ámpula contiene: Clorhidrato de lidocaína 1 g. '),('0264','LIDOCAÍNA SOLUCIÓN AL 10%. Cada 100 ml. contiene: Lidocaína 10.0 g. '),('0265','LIDOCAÍNA, EPINEFRINA SOLUCIÓN INYECTABLE AL 2%. Cada frasco ámpula contiene: Clorhidrato  de lidocaína 1 g. Epinefrina  (1:200000) 0.25 mg. '),('0267','LIDOCAÍNA, EPINEFRINA SOLUCIÓN INYECTABLE AL 2%. Cada cartucho dental contiene: Clorhidrato  de lidocaína 36 mg. Epinefrina  (1:100000) 0.018 mg. '),('0269','ROPIVACAÍNA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Clorhidrato  de ropivacaína monohidratada equivalente a 40 mg. de clorhidrato de ropivacaína.'),('0271','BUPIVACAÍNA SOLUCIÓN INYECTABLE. Cada ml. contiene: Clorhidrato  de bupivacaína 5 mg. '),('0291','NEOSTIGMINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Metilsulfato de neostigmina 0.5 mg. '),('0302','NALOXONA SOLUCIÓN INYECTABLE . Cada ampolleta contiene:  Clorhidrato  de naloxona\n0.4 mg. '),('0402','CLORFENAMINA TABLETA. Cada tableta contiene: Maleato de clorfenamina 4.0 mg. '),('0405','DIFENHIDRAMINA JARABE.  Cada 100 mililitros contienen: Clorhidrato  de difenhidramina\n250 mg. '),('0406','DIFENHIDRAMINA SOLUCIÒN INYECTABLE. Cada frasco ámpula contiene: Clorhidrato de difenhidramina  100 mg. '),('0408','CLORFENAMINA JARABE.  Cada mililitro contiene: Maleato de clorfenamina 0.5 mg. '),('0426','AMINOFILINA SOLUCION INYECTABLE. CADA AMPOLLETA CONTIENE: AMINOFILINA 250 MG'),('0429','SALBUTAMOL SUSPENSIÓN EN AEROSOL. Cada inhalador contiene: Salbutamol 20 mg. o\nSulfato de salbutamol equivalente a 20 mg. de salbutamol '),('0431','SALBUTAMOL JARABE.  Cada 5 ml. contienen: Sulfato de salbutamol equivalente a 2 mg. de salbutamol '),('0432','TERBUTALINA SOLUCIÓN INYECTABLE. Cada ml. contiene: Sulfato de terbutalina 0.25 mg.'),('0433','TERBUTALINA TABLETA. Cada tableta contiene: Sulfato de terbutalina 5 mg. '),('0437','TEOFILINA COMPRIMIDO O TABLETA O CÁPSULA DE  LIBERACIÓN PROLONGADA. Cada comprimido, tableta o cápsula contiene: Teofilina anhidra 100 mg. '),('0439','SALBUTAMOL SOLUCIÓN PARA  NEBULIZADOR. Cada 100 ml. contienen: Sulfato de salbutamol 0.5 g. '),('0443','SALMETEROL, FLUTICASONA SUSPENSIÓN EN AEROSOL. Cada gramo contiene: Xinafoato de salmeterol equivalente a 0.33 mg. de salmeterol propionato de fluticasona 0.67 mg.'),('0464','CROMOGLICATO DE  SODIO SUSPENSIÓN AEROSOL. Cada inhalador contienen: Cromoglicato  disódico 560 mg. '),('0472','PREDNISONA TABLETA. Cada tableta contiene: Prednisona 5 mg. '),('0473','PREDNISONA TABLETA. Cada tableta contiene: Prednisona 50 mg. '),('0474','HIDROCORTISONA SOLUCIÓN INYECTABLE. Cada frasco ámpula contiene: Succinato sódico de hidrocortisona  equivalente a 100 mg. de hidrocortisona. '),('0476','METILPREDNISOLONA SOLUCIÓN INYECTABLE. Cada frasco ámpula con liofilizado contiene: Succinato sódico de metilprednisolona  equivalente a 500 mg. de metilprednisolona. '),('0477','DIPROPIONATO DE  BECLOMETASONA, SUSPENSIÓN EN AEROSOL. Cada\ninhalador contiene: Dipropionato de beclometasona 10 mg. '),('0502','DIGOXINA TABLETA. Cada tableta contiene: Digoxina 0.25 mg.'),('0503','DIGOXINA ELÍXIR. Cada ml. contiene: Digoxina 0.05 mg. '),('0504','DIGOXINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Digoxina 0.5 mg.'),('0514','PARACETAMOL SUPOSITORIO. Cada supositorio contiene: Paracetamol 100 mg. '),('0524','CLORURO DE  POTASIO SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Cloruro de potasio 1.49 g. (20 mEq de potasio, 20 mEq de cloro)'),('0525','FENITOÍNA TABLETA O CÁPSULA. Cada tableta o cápsula contiene: Fenitoína sódica 100 mg.'),('0539','PROPRANOLOL TABLETA. Cada tableta contiene: Clorhidrato  de propranolol 10 mg. '),('0561','CLORTALIDONA TABLETA. Cada tableta contiene: Clortalidona  50 mg. '),('0566','METILDOPA TABLETA. Cada tableta contiene: Metildopa  250 mg.'),('0569','NITROPRUSIATO DE  SODIO SOLUCIÓN INYECTABLE. Cada frasco ámpula con polvo o solución contiene: Nitroprusiato  de sodio 50 mg. '),('0570','HIDRALAZINA TABLETA. Cada tableta contiene: Clorhidrato  de hidralazina 10 mg. '),('0572','METOPROLOL TABLETA. Cada tableta contiene: Tartrato  de metoprolol 100 mg. '),('0574','CAPTOPRIL TABLETA. Cada tableta contiene: Captopril 25 mg. '),('0591','TRINITRATO DE  GLICERILO  CÁPSULA O TABLETA MASTICABLE. Cada cápsula o tableta masticable contiene: Trinitrato de glicerol 0.8 mg.'),('0592','ISOSORBIDA TABLETA SUBLINGUAL. Cada tableta contiene: Dinitrato  de isosorbida 5 mg. '),('0593','ISOSORBIDA TABLETA. Cada tableta contiene: Dinitrato  de isosorbida 10 mg. '),('0596','VERAPAMILO GRAGEA O TABLETA RECUBIERTA. Cada gragea o tableta recubierta contiene: Clorhidrato  de verapamilo 80 mg.'),('0597','NIFEDIPINO CÁPSULA DE  GELATINA BLANDA. Cada cápsula contiene: Nifedipino 10 mg. '),('0598','VERAPAMILO SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Clorhidrato  de verapamilo 5mg. '),('0599','NIFEDIPINO COMPRIMIDO DE  LIBERACIÓN PROLONGADA. Cada comprimido contiene: Nifedipino 30 mg. '),('0611','EPINEFRINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Epinefrina  1 mg. (1:1\n000)'),('0612','NOREPINEFRINA SOL INY SOLUCION INYECTABLE CADA AMPOLLETA CONTIENE: BITARTRATO DE NOREPINEFRINA EQUIVALENTE A 4 MG DE NOREPINEFRINA.'),('0614','DOPAMINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Clorhidrato  de dopamina\n200 mg. '),('0615','DOBUTAMINA SOLUCIÓN INYECTABLE. Cada frasco ámpula o ampolleta contiene: Clorhidrato  de dobutamina  equivalente a 250 mg. de dobutamina.  '),('0621','HEPARINA SOLUCIÓN INYECTABLE. Cada frasco ámpula contiene: Heparina  sódica equivalente a 10 000 UI de heparina. '),('0623','WARFARINA TABLETA. Cada tableta contiene: Warfarina sódica 5 mg. '),('0624.01','ACENOCUMAROL TABLETA. Cada tableta contiene: Acenocumarol 4 mg. '),('0641','DEXTRÁN SOLUCIÓN INYECTABLE AL 10%. Cada 100 mililitros contienen: Dextrán (40\n000): 10 g. Glucosa 5 g. '),('0655','BEZAFIBRATO TABLETA. Cada tableta contiene: Bezafibrato 200 mg. '),('0657','PRAVASTATINA TABLETA. Cada tableta contiene: Pravastatina sódica 10 mg. '),('0801.01','BAÑO COLOIDE POLVO. Cada gramo contiene: Harina de soya 965 mg. (contenido proteico\n45%) polividona 20 mg. '),('0804','ÓXIDO DE  ZINC PASTA. Cada 100 g. contienen: Óxido de zinc 25. 0 g. '),('0813','HIDROCORTISONA CREMA. Cada g. contiene: 17 Butirato de hidrocortisona 1 mg. '),('0822.01','BENZOILO LOCIÓN DÉRMICA O GEL DÉRMICO. Cada 100 mililitros o gramos contienen: Peróxido de benzoilo 5 g. '),('0831','ALANTOINA Y ALQUITRÁN DE  HULLA SUSPENSIÓN DÉRMICA. Cada ml. contiene: Alantoína 20.0 mg. Alquitrán de hulla 9.4 mg.'),('0861','BENCILO EMULSIÓN DÉRMICA. Cada ml. contiene: Benzoato de bencilo 300 mg. '),('0865','PERMETRINA SOLUCIÓN. Cada 100 ml. contienen: Permetrina  1 g. '),('0871','ALIBOUR POLVO. Cada gramo contiene: Sulfato de Cobre 177.0 mg. Sulfato de Zinc 619.5 mg. Alcanfor 26.5 mg.'),('0872','CLIOQUINOL CREMA. Cada g. contiene: Clioquinol 30 mg. '),('0891','MICONAZOL CREMA. Cada gramo contiene: Nitrato  de miconazol 20 mg. '),('0901','PODOFILINA SOLUCIÓN DÉRMICA. Cada ml. contiene: Resina de podofilina 250 mg. '),('0904','ÁCIDO RETINOICO CREMA. Cada 100 gramos contienen: Ácido retinoico 0.05 g.'),('1006','CALCIO COMPRIMIDO EFERVESCENTE. Cada comprimido contiene: Lactato gluconato de calcio 2.94 g. carbonato de calcio 300 mg. equivalente a 500 mg. de calcio ionizable. '),('1007','LEVOTIROXINA TABLETA . Cada tableta contiene:  Levotiroxina sódica equivalente a 100 µg. de levotiroxina sódica anhidra. '),('1022','TIAMAZOL TABLETA. Cada tableta contiene: Tiamazol 5 mg. '),('1042','GLIBENCLAMIDA TABLETA. Cada tableta contiene: Glibenclamida 5 mg. '),('1050','INSULINA HUMANA SUSPENSIÓN INYECTABLE ACCIÓN INTERMEDIA NPH. Cada ml. contiene: Insulina humana isófana (origen ADN  recombinante)  100 UI ó Insulina zinc isófana humana (origen ADN  recombinante)  100 UI '),('1050.01','INSULINA HUMANA SUSPENSIÓN INYECTABLE ACCIÓN INTERMEDIA NPH. Cada ml. contiene: Insulina humana isófana (origen ADN  recombinante)  100 UI ó Insulina zinc isófana humana (origen ADN  recombinante)  100 UI'),('1051','INSULINA HUMANA SOLUCIÓN INYECTABLE ACCIÓN RÁPIDA REGULAR. Cada ml. contiene: Insulina humana (origen ADN  recombinante)  100 UI ó Insulina zinc isófana humana (origen ADN  recombinante)  100 UI '),('1051.01','INSULINA HUMANA SOLUCIÓN INYECTABLE ACCIÓN RÁPIDA REGULAR. Cada ml. contiene: Insulina humana (origen ADN  recombinante)  100 UI ó Insulina zinc isófana humana (origen ADN  recombinante)  100 UI '),('1093','DANAZOL CÁPSULA O COMPRIMIDO. Cada cápsula o comprimido contiene: Danazol 100 mg. '),('1094','CABERGOLINA. TABLETA Cada tableta contiene: Cabergolina de 0.5 mg'),('1095','CALCITRIOL CÁPSULA DE  GELATINA BLANDA. Cada cápsula contiene: Calcitriol 0.25\n4g. '),('1096','BROMOCRIPTINA TABLETA. Cada tableta contiene: Mesilato de bromocriptina  equivalente a\n2.5 mg. de bromocriptina.  '),('1098','VITAMINAS A ,C y D SOLUCIÓN. Cada ml. contiene: Palmitato  de Retinol 7000 a 9000 UI\nÁcido ascórbico 80 a 125 mg. colecalciferol 1400 a 1800 UI '),('1206','BUTILHIOSCINA GRAGEA O TABLETA. Cada gragea o tableta contiene: Bromuro de butilhioscina 10 mg. '),('1207','BUTILHIOSCINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Bromuro de butilhioscina 20 mg. '),('1223','ALUMINIO Y MAGNESIO TABLETA MASTICABLE. Cada tableta masticable contiene: Hidróxido  de aluminio 200 mg. Hidróxido  de magnesio 200 mg. o trisilicato de magnesio: 447.3 mg. '),('1224','ALUMINIO Y MAGNESIO SUSPENSIÓN ORAL. Cada 100 ml. contienen: Hidróxido de aluminio 3.7 g. Hidróxido  de magnesio 4.0 g. o trisilicato de magnesio: 8.9 g. '),('1234.01','RANITIDINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Clorhidrato  de ranitidina equivalente a 50 mg. de ranitidina. '),('1241','METOCLOPRAMIDA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Clorhidrato  de metoclopramida  10 mg. '),('1242','METOCLOPRAMIDA TABLETA. Cada tableta contiene: Clorhidrato  de metoclopramida  10 mg. '),('1243','METOCLOPRAMIDA SOLUCIÓN. Cada ml. contiene:  Clorhidrato  de metoclopramida  4 mg. '),('1263','BISMUTO SUSPENSIÓN ORAL. Cada 100 ml. contienen: Subsalicilato de bismuto 1.750 g. '),('1270','SENÓSIDOS A-B SOLUCION ORAL . Cada 100 ml. contienen: Concentrado de Sen equivalente a 200 mg. de senósidos A y B. '),('1271','PLÁNTAGO PSYLLIUM POLVO. Cada 100 g. contienen: Polvo de cáscara de semilla de plántago psyllium 49.7 g.'),('1272','SENÓSIDOS A-B TABLETA. Cada tableta contiene: Concentrados  de Sen desecados 187 mg. (normalizado a 8.6 mg. de senósidos A-B).'),('1308','METRONIDAZOL TABLETA. Cada tableta contiene: Metronidazol  500 mg.'),('1308.01','METRONIDAZOL TABLETA. Cada tableta contiene: Metronidazol  500 mg. '),('1309','METRONIDAZOL SOLUCIÓN INYECTABLE. Cada ampolleta o frasco ámpula contiene: Metronidazol  200 mg.'),('1310','METRONIDAZOL SUSPENSIÓN ORAL. Cada 5 ml. contienen: Benzoilo de metronidazol equivalente a 250 mg. de metronidazol.  '),('1311','METRONIDAZOL SOLUCIÓN INYECTABLE. Cada 100 ml. contienen: Metronidazol  500 mg.'),('1344','ALBENDAZOL TABLETA. Cada tableta contiene: Albendazol 200 mg.'),('1345','ALBENDAZOL SUSPENSIÓN ORAL. Cada frasco contiene: Albendazol 400 mg. '),('1347','ALBENDAZOL TABLETA. Cada tableta contiene: Albendazol 200 mg. '),('1363','LIDOCAÍNA - HIDROCORTISONA UNGÜENTO. Cada 100 gramos contiene: Lidocaína 5 g. Acetato de Hidrocortisona 0.25 g. Subacetato de Aluminio 3.50 g. Óxido de Zinc 18 g. '),('1364','LIDOCAÍNA - HIDROCORTISONA SUPOSITORIO. Cada supositorio contiene: Lidocaína\n60 mg. Acetato de Hidrocortisona 5 mg. Óxido de Zinc 400 mg. Subacetato de Aluminio 50 mg. '),('1401','ISOCONAZOL OVULOS. CADA OVULO CONTIENE: NITRATO DE ISOCONAZOL 600 MG'),('1431','PERFENAZINA TABLETAS 10 MG'),('1437','MEMANTINA 10MG  '),('1438','PIPOTIAZINA SOL. INYECTABLE 25 MG  '),('1439','AMISULPRIDA 400MG'),('1439.1','AMISULPRIDA 200 MG'),('1472','OLANZAPINA/FLUOXETINA 6-25MG'),('1508','ESTROGENOS CONJUGADOS Y MEDROXIPROGESTERONA GRAGEA. Cada gragea contiene: Estrógenos conjugados de origen equino 0.625 mg. Acetato de medroxiprogesterona  2.5 mg. '),('1541','CARBETOCINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Carbetocina  100 µg. '),('1542','OXITOCINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Oxitocina: 5 UI '),('1544','ERGOMETRINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Maleato de ergometrina 0.2 mg. '),('1551','ORCIPRENALINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Sulfato de orciprenalina 0.5 mg. '),('1552','ORCIPRENALINA TABLETA. Cada tableta contiene: Sulfato de orciprenalina 20 mg. '),('1561','METRONIDAZOL ÓVULO O TABLETA VAGINAL. Cada óvulo o tableta contiene: Metronidazol  500 mg. '),('1562','NITROFURAL ÓVULO. Cada óvulo contiene: Nitrofural 6 mg. '),('1566','NISTATINA ÓVULO O TABLETA VAGINAL. Cada óvulo o tableta contiene: Nistatina 100\n000 UI '),('1703','SULFATO FERROSO TABLETA. Cada tableta contiene: Sulfato ferroso desecado aproximadamente  200 mg. equivalente a 60.27 mg. de hierro elemental. '),('1704','SULFATO FERROSO SOLUCIÓN ORAL. Cada ml. contiene: Sulfato ferroso heptahidratado\n125 mg. equivalente a 25 mg. de hierro elemental. '),('1706','ÁCIDO FÓLICO TABLETA. Cada tableta contiene: Ácido fólico 5 mg.'),('1706.01','ÁCIDO FÓLICO TABLETA. Cada tableta contiene: Ácido fólico 5 mg. '),('1708','HIDROXOCOBALAMINA SOLUCIÓN INYECTABLE. Cada ampolleta o frasco ámpula con solución o liofilizado contiene: Hidroxocobalamina  100 µg. '),('1711','ÁCIDO FÓLICO TABLETA. Cada tableta contiene: Acido fólico 0.4 mg. '),('1732.01','FITOMENADIONA SOLUCIÓN O EMULSION INYECTABLE. Cada ampolleta contiene: Fitomenadiona 2 mg. '),('1759','METOTREXATO TABLETA. Cada tableta contiene: Metotrexato  sódico equivalente a 2.5 mg. de metotrexato '),('1776','METOTREXATO SOLUCIÓN INYECTABLE. Cada frasco ámpula con liofilizado contiene: Metotrexato  sódico equivalente a 500 mg. de metotrexato '),('1903','TRIMETOPRIMA-SULFAMETOXAZOL COMPRIMIDO O TABLETA. Cada comprimido o tableta contiene: Trimetoprima 80 mg. y Sulfametoxazol 400 mg.'),('1904','TRIMETOPRIMA-SULFAMETOXAZOL SUSPENSIÓN ORAL. Cada 5 ml. contienen: Trimetoprima 40 mg. Sulfametoxazol 200 mg. '),('1911','NITROFURANTOINA CÁPSULA. Cada cápsula contiene: Nitrofurantoína 100 mg. '),('1923','BENCILPENICILINA PROCAÍNICA CON BENCILPENICILINA CRISTALINA SUSPENSIÓN INYECTABLE . Cada frasco ámpula con polvo  contiene: Bencilpenicilina procaínica equivalente a 300 000 UI de bencilpenicilina cristalina equivalente a 100 000 UI de bencilpenicilina. '),('1924','BENCILPENICILINA PROCAÍNICA CON BENCILPENICILINA CRISTALINA SUSPENSIÓN INYECTABLE. Cada frasco ámpula con polvo contiene: Bencilpenicilina procaínica equivalente a 600 000 UI de bencilpenicilina cristalina equivalente a 200 000 UI de bencilpenicilina. '),('1925','BENZATINA BENCILPENICILINA SUSPENSIÓN INYECTABLE. Cada frasco ámpula con polvo contiene: Benzatina bencilpenicilina equivalente a 1 200 000 UI de bencilpenicilina. '),('1926','DICLOXACILINA CÁPSULA O COMPRIMIDO. Cada cápsula o comprimido contiene: Dicloxacilina sódica 500 mg. '),('1927','DICLOXACILINA SUSPENSIÓN ORAL. Cada 5 ml. contienen: Dicloxacilina sódica 250 mg. '),('1928','DICLOXACILINA SOLUCIÓN INYECTABLE. Cada frasco ámpula con polvo contiene: Dicloxacilina sódica equivalente a 250 mg. de dicloxacilina. '),('1929','AMPICILINA TABLETA O CÁPSULA. Cada tableta o cápsula contiene: Ampicilina anhidra o ampicilina trihidratada  equivalente a 500 mg. de ampicilina. '),('1930','AMPICILINA SUSPENSIÓN ORAL. Cada 5 ml. contienen: Ampicilina trihidratada  equivalente a 250 mg. de ampicilina. '),('1931','AMPICILINA SOLUCIÓN INYECTABLE. Cada frasco ámpula con polvo contiene: Ampicilina sódica equivalente a 500 mg. de ampicilina.'),('1935','CEFOTAXIMA SOLUCIÓN INYECTABLE. Cada frasco ámpula con polvo contiene: Cefotaxima sódica equivalente a 1 g. de cefotaxima. '),('1937','CEFTRIAXONA SOLUCIÓN INYECTABLE. Cada frasco ámpula con polvo contiene: Ceftriaxona sódica equivalente a 1 g. de ceftriaxona.'),('1939','CEFALEXINA TABLETA O CÁPSULA. Cada tableta o cápsula contiene: Cefalexina monohidratada equivalente a 500 mg. de cefalexina. '),('1939.1','CEFALEXINA SUSPENSIàN 250MG'),('1940','DOXICICLINA CÁPSULA O TABLETA. Cada cápsula o tableta contiene: Hiclato de doxiciclina equivalente a 100 mg. de doxicilina. '),('1941','DOXICICLINA CÁPSULA O TABLETA. Cada cápsula o tableta contiene: Hiclato de doxiciclina equivalente a 50 mg. de doxicilina. '),('1951','KANAMICINA SOLUCIÓN INYECTABLE. Cada frasco ámpula contiene: Sulfato de kanamicina 1 g.'),('1954','GENTAMICINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Sulfato de gentamicina equivalente a 80 mg. de gentamicina. '),('1955','GENTAMICINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Sulfato de gentamicina equivalente a 20 mg. de gentamicina base. '),('1956','AMIKACINA SOLUCIÓN INYECTABLE. Cada ampolleta o frasco ámpula contiene: Sulfato de amikacina equivalente a 500 mg. de amikacina. '),('1957.01','AMIKACINA SOLUCIÓN INYECTABLE. Cada ampolleta o frasco ámpula contiene: Sulfato de amikacina equivalente a 100 mg. de amikacina.'),('1969.01','AZITROMICINA TABLETA. Cada tableta contiene: Azitromicina dihidratada equivalente a 500 mg. de azitromicina'),('1971','ERITROMICINA CÁPSULA O TABLETA. Cada cápsula o tableta contiene: Estearato de eritromicina equivalente a 500 mg. de eritromicina. '),('1972','ERITROMICINA SUSPENSIÓN ORAL. Cada 5 ml. contienen: Estearato o etilsuccinato o estolato de eritromicina equivalente a 250 mg. de eritromicina. '),('1973','CLINDAMICINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Fosfato de clindamicina equivalente a 300 mg. de clindamicina. '),('1976','CLINDAMICINA SOLUCIÓN INYECTABLE. Cada frasco contiene: Fosfato de clindamicina equivalente a 900 mg. de clindamicina. '),('1981','TETRACICLINA TABLETA O CÁPSULA. Cada tableta o cápsula contiene: Clorhidrato  de tetraciclina 250 mg. '),('1991','CLORANFENICOL CÁPSULA. Cada cápsula contiene: Cloranfenicol 500 mg.'),('2012','Amfotericina B   Solución inyectable   50 mg'),('2016','KETOCONAZOL TABLETA. Cada tableta contiene: Ketoconazol 200 mg. '),('2018','ITRACONAZOL CÁPSULA. Cada cápsula contiene: Itraconazol 100 mg. '),('2024','ISOCONAZOL CREMA. Cada 100 gramos contiene: Nitrato  de isoconazol 1g. '),('2030','CLOROQUINA TABLETA. Cada tableta contiene: Fosfato de cloroquina equivalente a 150 mg. de cloroquina.'),('2040','PRAZICUANTEL TABLETA. Cada tableta contiene: Prazicuantel 600 mg. '),('2096','TRAMADOL-PARACETAMOL TABLETA. Cada tableta contiene: Clorhidrato  de tramadol\n37.5 mg. Paracetamol 325.0 mg. '),('2097','BUPRENORFINA PARCHE. Cada parche contiene: Buprenorfina 30 mg. '),('2098','BUPRENORFINA PARCHE. Cada parche contiene: Buprenorfina 20 mg. '),('2099','MORFINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Sulfato de morfina pentahidratada 2.5 mg. '),('2100','BUPRENORFINA TABLETA SUBLINGUAL. Cada tableta sublingual contiene: Clorhidrato  de buprenorfina equivalente a 0.2 mg. de buprenorfina.'),('2101','CLONIDINA COMPRIMIDO. Cada comprimido contiene: Clorhidrato  de clonidina 0.1 mg. '),('2102','SOLUCION INYECTABLE Cada ampolleta contiene: Sulfato de morfina pentahidratada 50 mg'),('2106','TRAMADOL SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Clorhidrato  de tramadol\n100 mg. '),('2107','SOLUCION INYECTABLE\nCada ampolleta contiene:\nSulfato de efedrina 50 mg\n'),('2108','MIDAZOLAM SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Clorhidrato  de midazolam equivalente a 5 mg. de midazolam  o midazolam 5 mg. '),('2111.01','AMLODIPINO TABLETA O CAPSULA . Cada tableta o capsula contiene: Besilato o Maleato de amlodipino equivalente a 5 mg.  de amlodipino.  '),('2119','BETAMETASONA UNGÜENTO. Cada 100 gramos contiene: Dipropionato de betametasona 64 mg. equivalente a 50 mg. de betametasona. '),('2126','ACICLOVIR COMPRIMIDO O TABLETA. Cada comprimido o tableta contiene: Aciclovir\n400 mg. '),('2127','AMOXICILINA SUSPENSIÓN ORAL. Cada frasco con polvo contiene: Amoxicilina trihidratada equivalente a 7.5 g. de amoxicilina. '),('2128','AMOXICILINA CÁPSULA. Cada cápsula contiene: Amoxicilina trihidratada  equivalente a 500 mg. de amoxicilina. '),('2128.01','AMOXICILINA CÁPSULA. Cada cápsula contiene: Amoxicilina trihidratada  equivalente a 500 mg. de amoxicilina. '),('2129','AMOXICILINA – ACIDO CLAVULÁNICO SUSPENSIÓN ORAL. Cada frasco con polvo contiene: Amoxicilina trihidratada  equivalente a 1.5 g. de amoxicilina clavulanato de potasio equivalente a 375 mg. de ácido clavulánico.'),('2132','CLARITROMICINA TABLETA. Cada tableta contiene: Claritromicina  250 mg.'),('2133','CLINDAMICINA CÁPSULA. Cada cápsula contiene: Clorhidrato  de clindamicina equivalente a\n300 mg. de clindamicina.'),('2135','FLUCONAZOL SOLUCION INYECTABLE CADA FRASCO AMPULA CONTIENE FLUCONAZOL: 100 MG.'),('2136','MEBENDAZOL TABLETA. Cada tableta contiene: Mebendazol 100 mg. '),('2138','PIRANTEL TABLETA. Cada tableta contiene: Pamoato de pirantel 250 mg. '),('2141','BETAMETASONA SOLUCIÓN INYECTABLE. Cada ampolleta o frasco ámpula contiene: Fosfato sódico de betametasona 5.3 mg. equivalente a 4 mg. de betametasona. '),('2142','CLORFENAMINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Maleato de clorfenamina 10 mg. '),('2144','LORATADINA TABLETA O GRAGEA. Cada tableta o gragea contiene: Loratadina  10 mg. '),('2145','LORATADINA JARABE.  Cada 100 ml. contienen: Loratadina  100 mg. '),('2151','RANITIDINA JARABE.  Cada 10 ml. contiene: Clorhidrato  de ranitidina 150 mg. '),('2152','ÁCIDO FOLÍNICO SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Folinato cálcico equivalente a 15 mg. de ácido folínico.'),('2154','ENOXAPARINA SOLUCIÓN INYECTABLE. Cada jeringa contiene: Enoxaparina sódica 40 mg. '),('2156','ESPIRONOLACTONA TABLETA. Cada tableta contiene: Espironolactona  100 mg. '),('2162','IPRATROPIO SUSPENSIÓN EN AEROSOL. Cada g. contiene: Bromuro de ipratropio 0.286 mg. (20 µg. por nebulización)'),('2164','CARBAMAZEPINA TABLETA. Cada tableta contiene: Carbamazepina  400 mg. '),('2172','ALCOHOL POLIVINÍLICO SOLUCIÓN OFTÁLMICA Cada ml contiene: Alcohol polivinílico 14 mg. '),('2174','CIPROFLOXACINO SOLUCION OFTALMICA CADA 1 ML CONTIENE: CLORHIDRATO DE CIPROFLOXACINO MONOHIDRATADO EQUIVALENTE A 3.0 MG DE CIPROFLOXACINO.'),('2175','CLORANFENICOL-SULFACETAMIDA SÓDICA SUSPENSIÓN OFTÁLMICA. Cada\n100 ml. contiene: Cloranfenicol levógiro   0.5 g. sulfacetamida sódica   10 g. '),('2185','PREDNISOLONA UNGÜENTO OFTÁLMICO. Cada g. contiene: Acetato de prednisolona equivalente a 5 mg. de prednisolona.'),('2186','PREDNISOLONA-SULFACETAMIDA SUSPENSIÓN OFTÁLMICA. Cada ml. contiene: Acetato de prednisolona 5 mg. sulfacetamida sódica 100 mg. '),('2187','IPRATROPIO SOLUCIÓN. Cada 100 ml. contienen: Bromuro de ipratropio monohidratado equivalente a 25 mg. de bromuro de ipratropio. '),('2188','IPRATROPIO-SALBUTAMOL SOLUCIÓN. Cada ampolleta contiene: Bromuro de ipratropio monohidratado equivalente a 0.500 mg. de bromuro de ipratropio. Sulfato de salbutamol equivalente a 2.500 mg. de salbutamol. '),('2189','TOBRAMICINA SOLUCIÓN OFTÁLMICA. Cada ml. contiene: Sulfato de tobramicina equivalente a 3.0 mg. de tobramicina ó tobramicina 3.0 mg.'),('2190.01','SOLUCION PARA INHALACION\nCada disparo proporciona: Bromuro de ipratropio monohidratado equivalente a 20 µg  de bromuro de ipratropio Sulfato de salbutamol\nequivalente a 100 µg  de salbutamol'),('2191','VITAMINA A CÁPSULA. Cada cápsula contiene: Vitamina A 50 000 UI '),('2198','OXIMETAZOLINA SOLUCION NASAL. CADA 100 ML CONTIENEN: CLORHIDRATO DE OXIMETAZOLINA 50 MG'),('2230','AMOXICILINA – ACIDO CLAVULÁNICO TABLETA. Cada tableta contiene: Amoxicilina trihidratada  equivalente a 500 mg. de amoxilina clavulanato de potasio equivalente a 125 mg. de ácido clavulánico. '),('2230.01','AMOXICILINA – ACIDO CLAVULÁNICO TABLETA. Cada tableta contiene: Amoxicilina trihidratada  equivalente a 500 mg. de amoxilina clavulanato de potasio equivalente a 125 mg. de ácido clavulánico. '),('2242','CARBÓN ACTIVADO POLVO. Cada envase contiene: Carbón activado 1 kg.'),('2247','CINITAPRIDA COMPRIMIDO. Cada comprimido contiene: Bitartrato de cinitaprida equivalente a 1 mg. de cinitaprida. '),('2248','CINITAPRIDA GRANULADO. Cada sobre contiene: Bitartrato de cinitaprida equivalente a 1 mg. de cinitaprida.'),('2249','CINITAPRIDA SOLUCIÓN ORAL. Cada 100 ml. contienen: Bitartrato de cinitaprida equivalente a 20 mg. de cinitaprida. '),('2262','BROMURO DE   TIOTROPIO, CÁPSULA. Cada cápsula contiene: Bromuro de tiotropio monohidratado equivalente a 18 µg. de tiotropio. '),('2301','HIDROCLOROTIAZIDA TABLETA. Cada tableta contiene: Hidroclorotiazida 25 mg. '),('2302','ACETAZOLAMIDA TABLETA. Cada tableta contiene: Acetazolamida 250 mg.'),('2304','ESPIRONOLACTONA TABLETA. Cada tableta contiene: Espironolactona  25 mg. '),('2306','MANITOL SOLUCIÓN INYECTABLE AL 20%. Cada envase contiene: Manitol 50 g.'),('2307','FUROSEMIDA TABLETA. Cada tableta contiene: Furosemida 40 mg. '),('2308','FUROSEMIDA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Furosemida 20 mg. '),('2462','AMBROXOL COMPRIMIDO. Cada comprimido contiene: Clorhidrato  de ambroxol 30 mg. '),('2463','AMBROXOL SOLUCIÓN. Cada 100 ml. contienen: Clorhidrato  de ambroxol 300 mg. '),('2499','ALPRAZOLAM TABLETA. Cada tableta contiene: Alprazolam 2.0 mg. '),('2500','ALPRAZOLAM TABLETA. Cada tableta contiene: Alprazolam 0.25 mg. '),('2501','ENALAPRIL O LISINOPRIL O RAMIPRIL  CÁPSULA O TABLETA. Cada cápsula o tableta contiene: Maleato de enalapril 10 mg. o lisinopril 10 mg. o ramipril 10 mg. '),('2503','ALOPURINOL TABLETA. Cada tableta contiene: Alopurinol 100 mg. '),('2503.01','Alopurinol Tableta 100 mg'),('2504','KETOPROFENO CÁPSULA. Cada cápsula contiene: Ketoprofeno 100 mg. '),('2508','DIPROPIONATO DE  BECLOMETASONA, SUSPENSIÓN EN AEROSOL . Cada inhalador contiene:  Dipropionato de beclometasona 50 mg.  '),('2520','LOSARTÁN GRAGEA O COMPRIMIDO RECUBIERTO. Cada gragea o comprimido recubierto contiene: Losartán potásico 50 mg. '),('2540','TELMISARTÁN TABLETA. Cada tableta contiene: Telmisartán  40 mg.'),('2601','FENOBARBITAL TABLETA. Cada tableta contiene: Fenobarbital 100 mg. '),('2602','FENOBARBITAL TABLETA . Cada tableta contiene:  Fenobarbital 15 mg.  '),('2608','CARBAMAZEPINA TABLETA. Cada tableta contiene: Carbamazepina  200 mg. '),('2609','CARBAMAZEPINA SUSPENSIÓN ORAL. Cada 5 ml. contienen: Carbamazepina  100 mg. '),('2610','FENITOÍNA TABLETA. Cada tableta contiene: Fenitoína sódica 30 mg. '),('2611','FENITOÍNA SUSPENSIÓN ORAL. Cada 5 ml. contienen: Fenitoína 37.5 mg. '),('2612','CLONAZEPAM TABLETA. Cada tableta contiene: Clonazepam  2 mg.'),('2613','CLONAZEPAM SOLUCIÓN. Cada ml. contiene: Clonazepam  2.5 mg. '),('2614','CLONAZEPAM SOLUCIÓN INYECTABLE. Cada ml. contiene: Clonazepam  1 mg. '),('2619','FENOBARBITAL ELÍXIR. Cada 5 ml. contienen: Fenobarbital 20 mg. '),('2620','ÁCIDO VALPROICO CÁPSULA. Cada cápsula contiene: Ácido valproico 250 mg. '),('2622','VALPROATO DE  MAGNESIO TABLETA CON CUBIERTA ENTÉRICA. Cada tableta contiene Valproato de magnesio 200 mg. equivalente a 185.6 mg. de ácido valproico. '),('2623','VALPROATO DE  MAGNESIO SOLUCIÓN. Cada ml. contiene: Valproato de magnesio equivalente a 186 mg. de ácido valproico.'),('2624','FENITOÍNA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Fenitoína sódica 250 mg.'),('2627','OXCARBAMAZEPINA TABLETAS 600 MG'),('2630','VALPROATO SEMISÓDICO TABLETA DE  LIBERACIÓN PROLONGADA. Cada tableta de liberación prolongada contiene: Valproato semisódico equivalente a 500 mg. de ácido valproico '),('2651','TRIHEXIFENIDILO TABLETA. Cada tableta contiene: Clorhidrato  de trihexifenidilo 5 mg. '),('2652','BIPERIDENO TABLETA. Cada tableta contiene: Clorhidrato  de biperideno 2 mg. '),('2653','BIPERIDENO SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Lactato de biperideno 5 mg. '),('2654','LEVODOPA Y CARBIDOPA TABLETA. Cada tableta contiene: Levodopa 250 mg. carbidopa\n25 mg. '),('2657','LEVODOPA Y CARBIDOPA TABLETA DE  LIBERACIÓN PROLONGADA . Cada tableta contiene:  Levodopa 200 mg.  carbidopa hidratada equivalente a 50 mg. de carbidopa anhidra '),('2707','ÁCIDO ASCÓRBICO TABLETA. Cada tableta contiene: Ácido ascórbico 100 mg. '),('2711','VITAMINAS Y MINERALES SOLUCION ORAL CADA 100 ML CONTIENEN: RIVOFLAVINA 5-FOSFATO DE SODIO EQUIVALENTE A 0.060 G DE RIBOFLAVINA (VITAMINA B2) CLORHIDRATO DE TIAMINA (VITAMINA B1) 0.055 G.CLORHIDRATO DE PIRIDOXINA (VITAMINA B6) 0.075 G. CIANOCOBALAMINA (VIT'),('2714','COMPLEJO B TABLETA, COMPRIMIDO O CÁPSULA. Cada tableta, comprimido o cápsula contiene: Mononitrato o clorhidrato de Tiamina 100 mg. Clorhidrato de piridoxina 5 mg. cianocobalamina 50 µg. '),('2804','NAFAZOLINA SOLUCIÓN OFTÁLMICA. Cada ml. contiene: Clorhidrato  de nafazolina 1 mg. '),('2814','HIPROMELOSA SOLUCION OFTALMICA AL 0.5% CADA ML CONTIENE: HIPROMELOSA 5 MG'),('2821','CLORANFENICOL SOLUCIÓN OFTÁLMICA. Cada ml. contiene: Cloranfenicol levógiro 5 mg. '),('2822','CLORANFENICOL UNGÜENTO OFTÁLMICO. Cada g. contiene: Cloranfenicol levógiro 5 mg. '),('2823','NEOMICINA, POLIMIXINA B Y GRAMICIDINA SOLUCIÓN OFTÁLMICA. Cada ml. contiene: Sulfato de Neomicina equivalente a 1.75 mg. de neomicina. Sulfato de Polimixina B equivalente a 5 000 U de Polimixina B Gramicidina  25 µg. '),('2824','NEOMICINA, POLIMIXINA B Y BACITRACINA UNGÜENTO OFTÁLMICO. Cada gramo contiene: Sulfato de neomicina equivalente a 3.5 mg. de neomicina. Sulfato de polimixina B equivalente a 5 000 U de polimixina B bacitracina 400 U '),('2829','SULFACETAMIDA SOLUCIÓN OFTÁLMICA. Cada ml. contiene: Sulfacetamida sódica 0.1 g. '),('2830','ACICLOVIR UNGÜENTO OFTÁLMICO. Cada 100 gramos contienen Aciclovir 3 g. '),('2841','PREDNISOLONA SOLUCIÓN OFTÁLMICA. Cada ml. contiene: Fosfato sódico de prednisolona equivalente a 5 mg. de fosfato de prednisolona '),('2851','PILOCARPINA SOLUCIÓN OFTÁLMICA AL 2%. Cada ml. contiene: Clorhidrato  de pilocarpina 20 mg. '),('2852','PILOCARPINA SOLUCIÓN OFTÁLMICA AL 4%. Cada ml. contiene: Clorhidrato  de pilocarpina 40 mg. '),('2858','TIMOLOL SOLUCIÓN OFTÁLMICA. Cada ml. contiene: Maleato de timolol equivalente a 5 mg. de timolol. '),('2872','ATROPINA SOLUCIÓN OFTÁLMICA. Cada ml. contiene: Sulfato de Atropina 10 mg. '),('2893','HIPROMELOSA SOLUCION OFTALMICA AL 2% CADA ML CONTIENE: HIPROMELOSA 20 MG.'),('2899','CLORURO DE  SODIO POMADA O SOLUCIÓN OFTÁLMICA. Cada gramo o ml. contiene: Cloruro de sodio 50 mg. '),('3044','MEDROXIPROGESTERONA  TABLETA. Cada tableta contiene: Acetato de medroxiprogesterona  10 mg. '),('3045','MEDROXIPROGESTERONA  SUSPENSIÓN INYECTABLE. Cada frasco ámpula o jeringa prellenada contiene: Acetato de medroxiprogesterona  150 mg. '),('3111','DIFENIDOL TABLETA. Cada tableta contiene: Clorhidrato de difenidol equivalente a 25 mg. de difenidol '),('3112','DIFENIDOL SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Clorhidrato  de difenidol equivalente a 40 mg. de difenidol '),('3132','NEOMICINA POLIMIXINA B FLUOCINOLONA Y LIDOCAINA SOLUCION OTICA CADA 100 ML CONTIENEN ACETONIDA DE FLUOCINOLONA 0.025 G SULFATO DE NEOMICINA EQUIVALENTE A 0.350 G DE NEOMICINA SULFATO DE POLIMIXINA B EQUIVALENTE A 1 000 000 UI DE POLIMIXINA B CLORHI'),('3204','LEVOMEPROMAZINA TABLETA. Cada tableta contiene: Maleato de levomepromazina equivalente a 25 mg. de levomepromazina '),('3215','DIAZEPAM TABLETA. Cada tableta contiene: Diazepam 10 mg. '),('3241','TRIFLUOPERAZINA GRAGEA O TABLETA. Cada gragea o tableta contiene: Clorhidrato  de trifluoperazina equivalente a 5 mg. de trifluoperazina '),('3241.01','TRIFLUOPERAZINA GRAGEA O TABLETA. Cada gragea o tableta contiene: Clorhidrato  de trifluoperazina equivalente a 5 mg. de trifluoperazina '),('3247','PERFENAZINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Perfenazina 5 mg. '),('3251','HALOPERIDOL TABLETA. Cada tableta contiene: Haloperidol  5 mg. '),('3253','HALOPERIDOL SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Haloperidol  5 mg. '),('3255','LITIO TABLETA. Cada tableta contiene: Carbonato  de Litio 300 mg. '),('3258','RISPERIDONA TABLETA. Cada tableta contiene: Risperidona 2 mg. '),('3259','CLOZAPINA COMPRIMIDO. Cada comprimido contiene: Clozapina 100 mg.'),('3259.01','CLOZAPINA COMPRIMIDO. Cada comprimido contiene: Clozapina 100 mg. '),('3262','RISPERIDONA SOLUCIÓN ORAL. Cada mililitro contiene: Risperidona 1 mg. '),('3268','RISPERIDONA SUSPENSIÓN INYECTABLE DE  LIBERACIÓN PROLONGADA. Cada frasco ámpula contiene: Risperidona 25 mg. '),('3302','IMIPRAMINA GRAGEA O TABLETA. Cada gragea o tableta contiene: Clorhidrato  de imipramina 25 mg. '),('3305','AMITRIPTILINA TABLETA. Cada tableta contiene: Clorhidrato  de amitriptilina  25 mg. '),('3307','ATOMOXETINA CÁPSULA. Cada cápsula contiene: Clorhidrato  de atomoxetina equivalente a\n10 mg. de atomoxetina. '),('3307.1','Cada cápsula contiene: Clorhidrato de atomoxetina equivalente a 18 mg de Atomoxetina.  '),('3307.2','Cada cápsula contiene: Clorhidrato de atomoxetina equivalente a 25 mg de Atomoxetina.'),('3308','ATOMOXETINA CÁPSULA. Cada cápsula contiene: Clorhidrato  de atomoxetina equivalente a\n40 mg. de atomoxetina.'),('3309','ATOMOXETINA CÁPSULA. Cada cápsula contiene: Clorhidrato  de atomoxetina equivalente a\n60 mg. de atomoxetina. '),('3310','Cada cápsula contiene: Clorhidrato de atomoxetina equivalente a 80 mg de Atomoxetina.  '),('3310.1','Cada cápsula contiene: Clorhidrato de atomoxetina equivalente a 100 mg de Atomoxetina.  '),('3407','NAPROXENO TABLETA. Cada tableta contiene: Naproxeno 250 mg. '),('3409','COLCHICINA TABLETA. Cada tableta contiene: Colchicina 1 mg. '),('3412','INDOMETACINA SUPOSITORIO. Cada supositorio contiene: Indometacina  100 mg. '),('3413','INDOMETACINA CÁPSULA. Cada cápsula contiene: Indometacina  25 mg. '),('3417','DICLOFENACO CÁPSULA O GRAGEA DE  LIBERACIÓN PROLONGADA. Cada gragea contiene: Diclofenaco sódico 100 mg. '),('3422','KETOROLACO SOLUCIÓN INYECTABLE. Cada frasco ámpula o ampolleta contiene: Ketorolaco-trometamina 30 mg. '),('3432','DEXAMETASONA TABLETA. Cada tableta contiene: Dexametasona 0.5 mg. '),('3433','METILPREDNISOLONA SUSPENSIÓN INYECTABLE . Cada ml. contiene: Acetato de metilprednisolona  40 mg. '),('3444','METOCARBAMOL TABLETA. Cada tableta contiene: Metocarbamol  400 mg. '),('3451','ALOPURINOL TABLETA. Cada tableta contiene: Alopurinol 300 mg. '),('3461','AZATIOPRINA TABLETA. Cada tableta contiene: Azatioprina 50 mg. '),('3601','Glucosa, solución inyectable al 5%'),('3604','GLUCOSA SOLUCIÓN INYECTABLE AL 10%. Cada 100 ml. contienen: Glucosa anhidra o glucosa 10 g. ó glucosa monohidratada equivalente a 10.0 g. de glucosa  Contiene: Glucosa 50.0 g.'),('3605','GLUCOSA SOLUCIÓN INYECTABLE AL 10%. Cada 100 ml. contienen: Glucosa anhidra o glucosa 10 g. ó Glucosa monohidratada equivalente a 10.0 g. de glucosa, Contiene:  Glucosa 100.0 g.'),('3607','GLUCOSA SOLUCIÓN INYECTABLE al 50 %. Cada 100 ml. contienen: Glucosa anhidra o glucosa 50 g. ó Glucosa monohidratada equivalente a 50.0 g. de glucosa  Contiene: Glucosa 25.0 g.'),('3608','CLORURO DE  SODIO SOLUCIÓN INYECTABLE AL 0.9%. Cada 100 ml. contienen: Cloruro de sodio 0.9 g. Agua inyectable 100 ml.  Contiene:  Sodio 38.5 mEq. Cloruro 38.5 mEq.'),('3609','CLORURO DE  SODIO SOLUCIÓN INYECTABLE AL 0.9%. Cada 100 ml. contienen: Cloruro de sodio 0.9 g. Agua inyectable 100 ml.  Contiene:  Sodio 77 mEq. Cloruro 77 mEq.'),('3612','Cloruro de Sodio y Glucosa, 0.9g/5g/100ml, solución inyectable'),('3616','SOLUCIÓN HARTMANN SOLUCIÓN INYECTABLE. Cada 100 ml. contienen: Cloruro de sodio 0.600 g., cloruro de potasio 0.030 g., cloruro de calcio dihidratado  0.020 g., lactato de sodio\n0.310 g.  Miliequivalentes por litro: Sodio (130), Potasio'),('3617','FOSFATO DE  POTASIO SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Fosfato de potasio dibásico 1.550 g.  Fosfato de potasio monobásico 0.300 g. (potasio 20 mEq.) (fosfato 20 mEq.) '),('3618','BICARBONATO DE  SODIO SOLUCIÓN INYECTABLE AL 7.5%. Cada frasco ámpula contiene: Bicarbonato de sodio 3.75 g. El envase con 50 ml. contiene: Bicarbonato de sodio 44.5 mEq.'),('3619','BICARBONATO DE  SODIO SOLUCIÓN INYECTABLE AL 7.5%. Cada ampolleta contiene: Bicarbonato de sodio 0.75 g.Cada ampolleta con 10 ml. contiene: Bicarbonato de sodio 8.9 mEq.'),('3620','GLUCONATO DE  CALCIO SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Gluconato  de calcio 1 g. equivalente a 0.093 g. de calcio ionizable. '),('3620.01','GLUCONATO DE  CALCIO SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Gluconato  de calcio 1 g. equivalente a 0.093 g. de calcio ionizable. '),('3622','ELECTROLITOS ORALES POLVO (Fórmula de Osmolaridad  Baja). Cada sobre con polvo contiene: Glucosa anhidra 13.5 g. cloruro de potasio 1.5 g. cloruro de sodio 2.6 g. citrato trisódico dihidratado  2.9 g. '),('3624','GLUCOSA SOLUCIÓN INYECTABLE AL 5%. Cada 100 ml. contienen: Glucosa anhidra o glucosa 5 g. ó glucosa monohidratada equivalente a 5.0 g. de glucosa.  Contiene: Glucosa 2.5 g.'),('3625','GLUCOSA SOLUCIÓN INYECTABLE AL 5%. Cada 100 ml. contienen: Glucosa anhidra o glucosa 5 g. ó glucosa monohidratada equivalente a 5.0 g. de glucosa. Contiene: Glucosa 5.0 g.'),('3629','SULFATO DE  MAGNESIO SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Sulfato de magnesio 1g. (magnesio 8.1 mEq sulfato 8.1 mEq) '),('3631','GLUCOSA SOLUCIÓN INYECTABLE AL 5%. Cada 100 ml. contienen: Glucosa anhidra o glucosa 5 g. ó glucosa monohidratada equivalente a 5 g. de glucosa  y adaptador para vial.'),('3632','GLUCOSA SOLUCIÓN INYECTABLE AL 5%. Cada 100 ml. contienen: Glucosa anhidra o glucosa 5 g. ó glucosa monohidratada equivalente a 5 g. de glucosa . y adaptador para vial.'),('3661','POLIGELINA SOLUCIÓN INYECTABLE. Cada 100 ml. contienen: Poligelina 3.5 g.  con o sin equipo para su administración.'),('3662','SEROALBUMINA HUMANA SOLUCION INYECTABLE CADA FRASCO AMPULA CONTIENE SEROALBUMINA HUMANA 12.5G.'),('3673','Agua inyectable.  Cada ampolleta contiene 5ml de solución inyectable'),('3835','VITAMINA A SOLUCIÓN. Cada dosis contiene: Palmitato de vitamina A (retinol)\n200 000 UI '),('4024.05','EZETIMIBA TABLETA. Cada tableta contiene: Ezetimiba  10 mg. '),('4026','BUPRENORFINA SOLUCIÓN INYECTABLE. Cada ampolleta o frasco ámpula contiene: Clorhidrato  de buprenorfina equivalente a 0.3 mg. de buprenorfina. '),('4028','CLONIXINATO DE LISINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Clonixinato de lisina 100 mg. '),('4054','FLUMAZENIL SOLUCION INYECTABLE. Cada ampolleta contiene: Flumazenil 0.5 mg.'),('4055','BUPIVACAÍNA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Clorhidrato  de bupivacaína 15 mg. Dextrosa anhidra ó glucosa anhidra 240 mg. ó glucosa monohidratada equivalente a 240 mg. de glucosa anhidra. '),('4057','MIDAZOLAM SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Clorhidrato  de midazolam equivalente a 15 mg. de midazolam o midazolam 15 mg. '),('4059','BROMURO DE  ROCURONIO SOLUCIÓN INYECTABLE. Cada ampolleta o frasco ámpula contiene: Bromuro de rocuronio 50 mg. '),('4060','MIDAZOLAM SOLUCIÓN INYECTABLE. Cada ampolleta contiene:  Clorhidrato  de midazolam equivalente a 50 mg. de midazolam  o midazolam 50 mg.'),('4061','BESILATO DE CISATRACURIO, SOLUCIÓN INYECTABLE. Cada ml. contiene: Besilato de cisatracurio equivalente a 2 mg. de cisatracurio '),('4095','IRBESARTÁN TABLETA. Cada tableta contiene: Irbesartán 150 mg. '),('4096','IRBESARTÁN TABLETA. Cada tableta contiene: Irbesartán 300 mg. '),('4107','AMIODARONA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Clorhidrato  de amiodarona 150 mg. '),('4110','AMIODARONA TABLETA. Cada tableta contiene: Clorhidrato  de amiodarona 200 mg. '),('4111','TRINITRATO DE  GLICERILO PARCHE. Cada parche libera: Trinitrato de glicerilo 5 mg/día\n'),('4114','TRINITRATO DE  GLICERILO SOLUCIÓN INYECTABLE. Cada frasco ámpula contiene: Trinitrato de glicerilo 50 mg. '),('4120','MONONITRATO DE ISOSORBIDE CADA TABLETA CONTIENE: 5-MONONITRATO DE ISOSORBIDA 20 MG'),('4124.01','SIMVASTATINA TABLETA. Cada tableta contiene: Simvastatina 20 mg.'),('4126','SULFADIAZINA DE  PLATA CREMA. Cada 100 gramos contiene: Sulfadiazina de plata micronizada 1 g. '),('4136','CLINDAMICINA GEL. Cada 100 gramos contienen: Fosfato de clindamicina equivalente a 1 g. de clindamicina. '),('4139.01','MINOCICLINA GRAGEA. Cada gragea contiene: Clorhidrato  de minociclina equivalente a 100 mg. de minociclina. '),('4141','SUSPENSION PARA INHALACION\nCada 100 ml contienen: Furoato de mometasona monohidratada\nequivalente a 0.050 g de furoato de  mometasona\nanhidro.'),('4148','INSULINA LISPRO, LISPRO PROTAMINA SUSPENSIÓN INYECTABLE. Cada ml. contiene: Insulina lispro (origen ADN  recombinante)  25 UI Insulina lispro protamina (origen ADN recombinante)  75 UI '),('4152','COMPRIMIDO Cada comprimido contiene:\nFosfato de sitagliptina monohidratada\nequivalente a 100 mg de sitagliptina '),('4154','VASOPRESINA 20 UI'),('4157','INSULINA HUMANA SUSPENSIÓN INYECTABLE ACCIÓN INTERMEDIA LENTA. Cada ml. contiene: Insulina zinc compuesta humana (origen ADN  recombinante)  100 UI '),('4158','SOLUCION INYECTABLE CADA ML. DE SOLUCION CONTIENE INSULINA GLARGINA 3.64MG EQUIVALENTE A 100.0 UI DE INSULINA HUMANA'),('4158.01','INSULINA GLARGINA SOLUCIÓN INYECTABLE Envase con 5 cartuchos de vidrio con 3 ml. en dispositivo.'),('4161','ACIDO ALENDRÓNICO TABLETA O COMPRIMIDO. Cada tableta o comprimido contiene: Alendronato  de sodio equivalente a 10 mg. de ácido alendrónico. '),('4162','INSULINA LISPRO SOLUCIÓN INYECTABLE.. Cada ml. contiene: Insulina lispro (origen\nADN  recombinante)  100 UI '),('4163','RALOXIFENO TABLETA. Cada tableta contiene: Clorhidrato  de raloxifeno 60 mg. '),('4163.01','TABLETA Cada tableta contiene: Clorhidrato de raloxifeno 60 mg'),('4164','ACIDO ALENDRÓNICO TABLETA O COMPRIMIDO. Cada tableta o comprimido contiene: Alendronato  de sodio equivalente a 70 mg. de ácido alendrónico. '),('4165','Insulina detemir solución inyectable  100 U (14.20mg)'),('4167','ÁCIDO RISEDRÓNICO GRAGEA O TABLETA. Cada gragea o tableta contiene: Risedronato sódico 35 mg. '),('4184','LOPERAMIDA COMPRIMIDO, TABLETA O GRAGEA. Cada comprimido, tabletas o gragea contiene: Clorhidrato  de loperamida 2 mg. '),('4185','CAPSULA Cada cápsula contiene: Acido ursodeoxicólico 250 mg'),('4186','MESALAZINA GRAGEA CON CAPA ENTERICA O TABLETA. CADA GRAGEA O TABLETA CONTIENE: MESALAZINA 500 MG'),('4201','HIDRALAZINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Clorhidrato  de hidralazina 20 mg. '),('4202','INDOMETACINA SOLUCIÓN INYECTABLE. Cada frasco ámpula contiene: Indometacina  1 mg. '),('4224','ENOXAPARINA SOLUCION INYECTABLE. Cada jeringa contiene Enoxaparina sódica 60 mg. '),('4238.01','SOLUCION INYECTABLE Cada frasco ámpula con liofilizado contiene: Factor de coagulación VII alfa recombinante 60 000 UI (1.2 mg) o 1 mg (50 KUI)'),('4241','DEXAMETASONA SOLUCIÓN INYECTABLE. Cada frasco ámpula o ampolleta contiene: Fosfato sódico de dexametasona equivalente a 8 mg. de fosfato de dexametasona. '),('4246.01','CLOPIDOGREL GRAGEA O TABLETA. Cada gragea o tableta contiene: bisulfato de clopidogrel o bisulfato de clopidogrel (Polimorfo forma 2) equivalente a 75 mg. de clopidogrel. '),('4249','LEVOFLOXACINO SOLUCIÓN INYECTABLE. Cada envase contiene: Levofloxacino hemihidratado equivalente a 500 mg. de levofloxacino. '),('4251','VANCOMICINA SOLUCIÓN INYECTABLE. Cada frasco ámpula con polvo contiene: Clorhidrato  de vancomicina equivalente a 500 mg. de vancomicina. '),('4255','CIPROFLOXACINO CÁPSULA O TABLETA. Cada cápsula o tableta contiene: Clorhidrato  de ciprofloxacino monohidratado equivalente a 250 mg. de ciprofloxacino.'),('4258','CIPROFLOXACINO SUSPENSIÓN ORAL. Cada 5 mililitros contienen: Clorhidrato  de ciprofloxacino equivalente a 250 mg. de ciprofloxacino.'),('4259','CIPROFLOXACINO SOLUCIÓN INYECTABLE . Cada 100 ml. contiene: Lactato o clorhidrato de  ciprofloxacino equivalente a 200 mg. de ciprofloxacino.'),('4260','NISTATINA SUSPENSIÓN ORAL. Cada frasco con polvo contiene: Nistatina 2 400 000 UI\n'),('4263','ACICLOVIR COMPRIMIDO O TABLETA. Cada comprimido o tableta contiene: Aciclovir\n200 mg. '),('4264','ACICLOVIR SOLUCIÓN INYECTABLE. Cada frasco ámpula con liofilizado contiene: Aciclovir sódico equivalente a 250 mg. de aciclovir. '),('4299','LEVOFLOXACINO TABLETA. Cada tableta contiene: Levofloxacino hemihidratado equivalente a 500 mg. de levofloxacino.'),('4300','LEVOFLOXACINO TABLETA. Cada tableta contiene: Levofloxacino hemihidratado equivalente a 750 mg. de levofloxacino. '),('4302','FINASTERIDA GRAGEA O TABLETA RECUBIERTA. Cada gragea o tableta recubierta contiene: Finasterida  5 mg. '),('4308.01','TABLETA.  Cada tableta contiene Citrato de Slidenafil equivalente a Sildenafil 50mgs'),('4330','MONTELUKAST COMPRIMIDO RECUBIERTO. Cada comprimido contiene: Montelukast sódico equivalente a 10 mg. de montelukast'),('4332','BUDESONIDA SUSPENSION PARA NEBULIZAR DE 0.250 MCG'),('4356.01','PREGABALINA CÁPSULA. Cada cápsula contiene: Pregabalina 75 mg. '),('4359','GABAPENTINA CÁPSULA. Cada cápsula contiene: Gabapentina 300 mg. '),('4362','Toxina Botulinica Tipo A, solución inyectable'),('4372','VALACICLOVIR COMPRIMIDO RECUBIERTO. Cada comprimido recubierto contiene: Clorhidrato  de valaciclovir equivalente a 500 mg. de valaciclovir '),('4372.01','VALACICLOVIR COMPRIMIDO RECUBIERTO. Cada comprimido recubierto contiene: Clorhidrato  de valaciclovir equivalente a 500 mg. de valaciclovir '),('4376','MULTIVITAMINAS (POLIVITAMINAS) Y MINERALES TABLETA, CAPSULA O GRAGEA'),('4407','TETRACAÍNA SOLUCIÓN OFTÁLMICA. Cada ml. contiene: Clorhidrato  de tetracaína\n5.0 mg. '),('4411','SOLUCION OFTALMICA Cada ml contiene:\nLatanoprost 50 µg'),('4411.01','SOLUCION OFTALMICA Cada ml contiene:\nLatanoprost 50 µg'),('4416','CICLOSPORINA SOLUCION OFTALMICA. CADA ML CONTIENE: CICLOSPORINA A 1.0 MG'),('4418','TRAVOPROST SOLUCIÓN OFTÁLMICA. Cada ml. contiene: Travoprost 40 9µg'),('4420','BRIMONIDINA - TIMOLOL SOLUCIÓN OFTÁLMICA. Cada mililitro contiene: Tartrato de brimonidina   2.00 mg. Maleato de timolol 6.80 mg. '),('4429','DACTINOMICINA SOLUCIÓN INYECTABLE. Cada frasco ámpula con liofilizado contiene: Dactinomicina  0.5 mg. '),('4465','CAPSULA DE LIBERACION PROLONGADA Cada cápsula de liberación prolongada contiene: Bromhidrato de galantamina equivalente a 16 mg de galantamina'),('4470.01','METILFENIDATO TABLETA DE  LIBERACIÓN PROLONGADA. Cada tableta de liberación prolongada contiene: Clorhidrato  de metilfenidato 18 mg. '),('4471.01','METILFENIDATO TABLETA DE  LIBERACIÓN PROLONGADA. Cada tableta de liberación prolongada contiene: Clorhidrato de metilfenidato  27 mg.'),('4472','METILFENIDATO TABLETA DE  LIBERACIÓN PROLONGADA. Cada tableta de liberación prolongada contiene: Clorhidrato de metilfenidato 36 mg.'),('4472.01','METILFENIDATO TABLETA DE  LIBERACIÓN PROLONGADA. Cada tableta de liberación prolongada contiene: Clorhidrato de metilfenidato 36 mg.'),('4481','HALOPERIDOL SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Decanato de haloperidol equivalente a 50 mg. de haloperidol '),('4481.01','HALOPERIDOL SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Decanato de haloperidol equivalente a 50 mg. de haloperidol '),('4482','BROMAZEPAM. COMPRIMIDO CADA COMPRIMIDO CONTIENE BROMAZEPAM 3 MG.  '),('4483','FLUOXETINA CÁPSULA O TABLETA. Cada cápsula o tableta contiene: Clorhidrato  de fluoxetina equivalente a 20 mg. de fluoxetina. '),('4483.01','FLUOXETINA CÁPSULA O TABLETA. Cada cápsula o tableta contiene: Clorhidrato  de fluoxetina equivalente a 20 mg. de fluoxetina. '),('4484','SERTRALINA CÁPSULA O TABLETA. Cada cápsula o tableta contiene: Clorhidrato  de sertralina equivalente a 50 mg. de sertralina. '),('4485','DULOXETINA CAPSULA DE  LIBERACIÓN RETARDADA. Cada cápsula de liberación retardada contiene: Clorhidrato  de duloxetina  equivalente a 60 mg. de duloxetina.'),('4485.1','DULOXETINA 30 MG'),('4488','VENLAFAXINA CAPSULA O GRAGEA DE  LIBERACIÓN PROLONGADA. Cada cápsula o gragea de liberación prolongada contiene: Clorhidrato  de venlafaxina equivalente a 75 mg. de venlafaxina. '),('4489','OLANZAPINA SOLUCION INYECTABLE. CADA FRASCO AMPULA CON LIOFILIZADO CONTIENE: OLANZAPINA 10 MG ENVASE CON UN FRASCO AMPULA.  '),('4490','ARIPIPRAZOL TABLETA. Cada tableta contiene: Aripiprazol 15 mg. '),('4491','TABLETA Cada tableta contiene: Aripiprazol 20 mg'),('4492','ARIPIPRAZOL TABLETA. Cada tableta contiene: Aripiprazol 30 mg.'),('4504','SULFASALAZINA TABLETA CON CAPA  ENTÉRICA. Cada tableta con capa entérica contiene: Sulfasalazina 500 mg. '),('4514','LEFLUNOMIDA COMPRIMIDO. Cada comprimido contiene: Leflunomida 20 mg. '),('4515','LEFLUNOMIDA COMPRIMIDO. Cada comprimido contiene: Leflunomida 100 mg. '),('4526','LEVONORGESTREL GRAGEA. Cada gragea contiene: Levonorgestrel 0.03 mg. '),('4592','PIPERACILINA-TAZOBACTAM SOLUCIÓN INYECTABLE. Cada frasco ámpula con polvo contiene: Piperacilina sódica equivalente a 4 g. de piperacilina, tazobactam sódico equivalente a\n500 mg. de tazobactam. '),('5075','TEOFILINA ELÍXIR. Cada 100 ml. contienen: Teofilina anhidra 533 mg. '),('5079','CLOROPIRAMINA SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Clorhidrato  de cloropiramina 20 mg. '),('5099','ADENOSINA SOLUCIÓN INYECTABLE. Cada frasco ámpula contiene: Adenosina 6 mg. '),('5100','Lactato de Milrinona, solución inyectable.  Frasco ampula con 20mg'),('5104','ESMOLOL SOLUCIÓN INYECTABLE. Cada frasco ámpula contiene: Clorhidrato  de esmolol\n100 mg. '),('5106','ATORVASTATINA TABLETA. Cada tableta contiene: Atorvastatina cálcica trihidratada equivalente a 20 mg. de atorvastatina.'),('5107','ALTEPLASA SOLUCION INYECTABLE. CADA FRASCO AMPULA CON LIOFILIZADO CONTIENE: ALTEPLASA (ACTIVADOR TISULAR DEL PLASMINàGENO HUMANO) 50 MG'),('5165','METFORMINA TABLETA. Cada tableta contiene: Clorhidrato  de metformina  850 mg. '),('5176','SUCRALFATO TABLETA. Cada tableta contiene: Sucralfato 1 g. '),('5186','PANTOPRAZOL O RABEPRAZOL U OMEPRAZOL TABLETA O GRAGEA O CÁPSULA. Cada tableta o gragea o cápsula contiene: Pantoprazol  40 mg. o rabeprazol sódico 20 mg. u omeprazol 20 mg. '),('5186.01','PANTOPRAZOL O RABEPRAZOL U OMEPRAZOL TABLETA O GRAGEA O CÁPSULA. Cada tableta o gragea o cápsula contiene: Pantoprazol  40 mg. o rabeprazol sódico 20 mg. u omeprazol 20 mg. '),('5187','OMEPRAZOL O PANTOPRAZOL SOLUCIÓN INYECTABLE. Cada frasco ámpula con liofilizado contiene: Omeprazol sódico equivalente a 40 mg. de omeprazol. o pantoprazol sódico equivalente a 40 mg. de pantoprazol. '),('5233','ÁCIDO FOLÍNICO TABLETA. Cada tableta contiene: Folinato cálcico equivalente a 15 mg. de ácido folínico '),('5240','SOLUCION INYECTABLE Cada frasco ámpula con liofilizado o solución contienen: Inmunoglobulina G no modificada 6 g'),('5255','TRIMETOPRIMA Y SULFAMETOXAZOL SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Trimetoprima 160 mg. Sulfametoxazol 800 mg. '),('5256','CEFALOTINA SOLUCIÓN INYECTABLE. Cada frasco ámpula con polvo contiene: Cefalotina sódica equivalente a 1 g. de cefalotina. '),('5265','IMIPENEM Y CILASTATINA SOLUCIÓN INYECTABLE.. Cada frasco ámpula con polvo contiene: Imipenem  monohidratado equivalente a 500 mg. de imipenem.  Cilastatina sódica equivalente a 500 mg. de cilastatina. '),('5267','FLUCONAZOL CAPSULAS O TABLETA. CADA CAPSULA O TABLETA CONTIENE: FLUCONAZOL 100 MG'),('5284','CEFEPIMA SOLUCIÓN INYECTABLE El frasco ámpula contiene: Clorhidrato  monohidratado de cefepima equivalente a 500 mg. de cefepima. '),('5287','IMIPENEM Y CILASTATINA SOLUCIÓN INYECTABLE. Cada frasco ámpula con polvo contiene: Imipenem  monohidratado equivalente a 250 mg. de imipenem. Cilastatina sódica equivalente a 250 mg. de cilastatina. '),('5292','MEROPENEM SOLUCIÓN INYECTABLE. Cada frasco ámpula con polvo contiene: Meropenem  trihidratado  equivalente a 1 g. de meropenem. '),('5295','CEFEPIMA SOLUCIÓN INYECTABLE. Cada frasco ámpula contiene: Clorhidrato monohidratado de cefepima equivalente a 1 g. de cefepima. '),('5295.01','CEFEPIMA SOLUCIÓN INYECTABLE. Cada frasco ámpula contiene: Clorhidrato monohidratado de cefepima equivalente a 1 g. de cefepima.'),('5302','NITROFURANTOINA SUSPENSIÓN ORAL. Cada 100 ml. contienen: Nitrofurantoina 500 mg.'),('5309.02','TAMSULOSINA CÁPSULA DE  LIBERACIÓN PROLONGADA. Cada cápsula de liberación prolongada contiene: Clorhidrato de tamsulosina 0.4 mg. '),('5332','ERITROPOYETINA SOLUCION INYECTABLE CADA FRASCO AMPULA CON LIOFILIZADO O SOLUCION CONTIENE: ERITROPOYETINA HUMANA RECOMBINANTE O ERITROPOYETINA HUMANA RECOMBINANTE ALFA O ERITROPOYETINA BETA 2000 UI.'),('5335.01','SUSPENSION Cada mililitro contiene:\nFosfolípidos de pulmón porcino 80 mg'),('5351','METILFENIDATO COMPRIMIDO. Cada comprimido contiene: Clorhidrato  de metilfenidato\n10 mg. '),('5358','LAMOTRIGINA TABLETAS DISPERSABLES 25 MG'),('5359','VALPROATO DE  MAGNESIO TABLETA DE  LIBERACIÓN PROLONGADA. Cada tableta contiene: Valproato de magnesio 600 mg. '),('5363','TOPIRAMATO TABLETA. Cada tableta contiene: Topiramato  100 mg.'),('5383','MULTIVITAMINAS (POLIVITAMINAS) Y MINERALES '),('5384','MULTIVITAMINAS SOLUCION INYECTABLE ADULTO CADA FRASCO AMPULA CON LIOFILIZADO CONTIENE: RETINOL (VITAMINA A) 3300.0 U COLECALCIFEROL (VITAMINA D3) 200.0 U ACETATO DE TOCOFEROL (VITAMINA E) 10.0 U NICOTINAMIDA 40.0 MG RIBOFLAVINA 3.6 MG CLORHIDRATO DE PIRI'),('5385','MULTIVITAMINAS SOLUCION INYECTABLE INFANTIL CADA FRASCO AMPULA CON LIOFILIZADO CONTIENE: RETINOL (VITAMINA A) 2000.0 UI COLECALCIFEROL (VITAMINA D3) 200.0 UI ACETATO DE ALFA TOCOFEROL (VITAMINA E) 7.0 UI NICOTINAMIDA 17.0 MG RIBOFLAVINA 1.4 MG CLORHIDRAT'),('5386','CLORURO DE SODIO. SOLUCION INYECTABLE AL 17.7%. CADA ML CONTIENE: CLORURO DE SODIO 0.177 G.'),('5395','TIAMINA SOLUCIÓN INYECTABLE. Cada frasco ámpula con liofilizado contiene: Clorhidrato  de tiamina 500 mg. '),('5451','CINARIZINA TABLETA. Cada tableta contiene: Cinarizina 75 mg. '),('5468','ACIDO ZOLEDRÓNICO SOLUCIÓN INYECTABLE. Cada frasco ámpula con 5 ml. contiene: Acido zoledrónico monohidratado equivalente a 4.0 mg. de ácido zoledrónico '),('5472','Bevacizumab solución inyectable de 100mg'),('5476','LEVOMEPROMAZINA SOLUCIÓN INYECTABLE. Cada ml. contiene: Clorhidrato  de levomepromazina equivalente a 25 mg. de levomepromazina. '),('5478','LORAZEPAM TABLETA. Cada tableta contiene: Lorazepam 1 mg. '),('5481','PAROXETINA TABLETA. Cada tableta contiene: Clorhidrato de paroxetina equivalente a 20 mg. de paroxetina. '),('5483','ZUCLOPENTIXOL SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Decanato de zuclopentixol 200 mg. '),('5484','ZUCLOPENTIXOL TABLETA. Cada tableta contiene: Diclorhidrato  de zuclopentixol equivalente a 25 mg. de zuclopentixol '),('5485','OLANZAPINA TABLETA. Cada tableta contiene: Olanzapina  5 mg. '),('5485.01','OLANZAPINA TABLETA. Cada tableta contiene: Olanzapina  5 mg. '),('5486','OLANZAPINA TABLETA. Cada tableta contiene: Olanzapina  10 mg. '),('5486.01','OLANZAPINA TABLETA. Cada tableta contiene: Olanzapina  10 mg. '),('5487','CITALOPRAM TABLETA. Cada tableta contiene: Bromhidrato  de citalopram equivalente a 20 mg. de citalopram. '),('5487.01','CITALOPRAM TABLETA. Cada tableta contiene: Bromhidrato  de citalopram equivalente a 20 mg. de citalopram. '),('5489','QUETIAPINA TABLETA. Cada tableta contiene: Fumarato de quetiapina equivalente a 100 mg. de quetiapina'),('5489.1','QUETIAPINA TAB. LIB. PROL. 50 MG'),('5490','MIRTAZAPINA COMPRIMIDOS 30 MG'),('5494','QUETIAPINA TABLETA DE  LIBERACIÓN PROLONGADA. Cada tableta de liberación prolongada contiene: Fumarato de quetiapina equivalente a 300 mg. de quetiapina '),('5501','DICLOFENACO SOLUCIÓN INYECTABLE. Cada ampolleta contiene: Diclofenaco sódico 75 mg.'),('5505','CELECOXIB CAPSULA. Cada cápsula contiene: Celecoxib 100 mg. '),('5551','DABIGATRAN ETEXILATO CAPSULA. Cada cápsula contiene: Dabigatrán  etexilato mesilato equivalente a 75 mg.  de dabigatrán etexilato '),('5552','DABIGATRAN ETEXILATO CAPSULA. Cada cápsula contiene: Dabigatrán  etexilato mesilato equivalente a 110 mg. de dabigatrán etexilato'),('5621','Linagliptina, Cada tableta contiene 5 mg de Linagliptina  '),('5660','Lacosamida Tableta 50 mg'),('5699','Etoricoxib comprimidos de 90mg'),('5721.01','PARACETAMOL SOLUCION INYECTABLE DE 1 GR/100 ML'),('5730.01','Ticagrelor tabletas de 90 miligramos'),('5741','Metformina / Linagliptina 2.5/850mgs tabletas'),('5943','IBUPROFENO SUSPENSION infantil, cada  100 ml contiene ibuprofeno 2 g.'),('5944','IBUPROFENO SUSPENSION pediatrica, cada ml contiene ibuprofeno 40 mg.\n'),('6073','PAQUETE PARA PARTO ESTERELIZADO CON RADIACIoN GAMMA'),('6074','PAQUETE PARA CIRUGIA MAYOR BASICO ESTERILIZADO CON RADIACIoN GAMMA'),('6075','BATA PARA PACIENTE'),('6076','BATA PARA CIRUJANO'),('6077','UNIFORME QUIRURGICO BASICO'),('6078','PAQUETE DE CESAREA'),('6079','PAQUETE DE ORTOPEDIA'),('9090','CLINDAMICINA Y KETOCONAZOL. Cada ÓVULO para aplicación intravaginal contiene: Clindamicina 100 mg y Ketoconazol  400 mg.'),('9092','FLUVOXAMIMA DE 100 MG.'),('9093','SILIMARINA 70 MG TAB'),('9134','ETAMSILATO, SOLUCION INYECTABLE. CADA AM POLLETA CONTIENE: ETAMSILATO 250 MG.'),('9134.1','ETAMSILATO, COMPRIMIDOS DE 500 MG.'),('9899','Fosfomicina granulado. Cada sobre contiene Fosfomicina trometamol equivalente a 2 y 3 g de fosfomicina base Excipiente, c.b.p. 1 sobre. '),('9929','TRIMEBUTINA'),('9952','NITROGLICERINA SOLUCION INYECTABLE Cada ampolleta contiene: 5 mg  de nitroglicerina.'),('9953','PROSTAGLANDINA E1 SOLUCION INYECTABLE Cada ampula contiene 500 microgramos de  prostaglandina E 1, en 1 ml de alcohol deshidratado.'),('9968','KETOPROFENO IV AMP 100 MG/5ML'),('9975','CEFUROXIMA SUSPENSION 250MG'),('9976','TOBRAMICINA/DEXAMETASONA SOL UNGUENTO'),('9999','Mifepristona tabletas de 200mgs'),('10001','Metamizol sódico supositorio infantil 300mgs'),('24414','JERINGA DESECHABLE (PARA RECONSTITUCION DE VACUNAS SR Y SRP) CAPACIDAD 5 ML GRADUADA EN DECIMAS DE ML. EN EL BARRIL DEBE DE TENER IMPRESA LA LEYENDA VACUNACION UNIVERSAL POR CADA JERINGA SE DEBE INCLUIR UNA AGUJA CON CALIBRE 20 G X 32 MM PARA LA RECONSTI'),('25239','FIJADOR DENTAL FCO. P/ PREPARAR 1 GALON. 3.8 LT. DE FIJADOR CONT. AGUA TIOSULFITO DE AMONIO ACETATO DE SODIO BIOSULFITO DE SODIO SULFITO AMINO AC. ACETICO AC. BORICO ACTIVADO.'),('25248','FLUORURO DE SODIO AL 0.2% POLVO PARA 2 LITROS DE ENJUAGUE BUCAL PREVENTIVO DE CARIES CONT. 4 GRS. CON INSTRUCTIVO DE MODO DE EMPLEO EN EL SOBRE. PRESENTACION SOBRE C/4 GRS.'),('25268','FRESA PARA UTILIZARSE EN LA PIEZA DE MANO DE ALTA VELOCIDAD DE CARBURO FORMA DE CONO INVERTIDO Nø 34 ACERO INOXIDABLE ESTERILIZABLE PIEZA C/ BLISTER Y ETIQUETADO CON EL Nø DE LA FRESA'),('25269','FRESA DE CARBURO No.35 PARA UTILIZARSE EN LA PIEZA DE MANO DE ALTA VELOCIDAD DE CARBURO FORMA DE CONO INVERTIDO Nø 35 ACERO INOXIDABLE ESTERILIZABLE PIEZA C/ BLISTER Y ETIQUETADO CON EL Nø DE LA FRESA'),('25270','FRESA PARA UTILIZARSE EN LA PIEZA DE MANO DE ALTA VELOCIDAD DE CARBURO FORMA DE BOLA Nø 4 ACERO INOXIDABLE ESTERILIZABLE PIEZA C/ BLISTER Y ETIQUETADO CON EL Nø DE LA FRESA'),('25272','FRESA PARA UTILIZARSE EN LA PIEZA DE MANO DE ALTA VELOCIDAD DE CARBURO FORMA DE BOLA Nø 6 ACERO INOXIDABLE ESTERILIZABLE PIEZA C/ BLISTER Y ETIQUETADO CON EL Nø DE LA FRESA'),('25276','FRESA PARA PROFILAXIS CORTA ESTERILIZABLE PARA UTILIZARSE EN LA PIEZA DE MANO DE ALTA VELOCIDAD ACERO INOXIDABLE ESTERILIZABLE PIEZA C/ BLISTER Y ETIQUETADO CON EL Nø DE LA FRESA PUNTA ACTIVA CORTA LISA SIN ESTRIAS Y SIN FILO'),('25319','LENTULOS DEL Nø 1-4 DE 21 MM ESPIRAL BAJA VELOCIDAD'),('25407','PELICULA DENTAL EKTASPEED EP -21 RADIOGRAFIA PERIAPICAL ADULTO ENVASE C/ 150 EP- 021P DEL Nø 02 PELICULAS RAPIDAS DE ALTA SENCIBILIDAD'),('25560','REVELADOR DENTAL FCO. . P/ PREPERAR 1 GALON 3.8 LT. DE REVELADOR CON. AGUA SULFATO DE POTASIO DIETILENO DE GLICOL SULFATO DE SODIO CARBONATO DE POTASIO'),('25603','FRESA DE BAJA VELOCIDAD QUIRURGICA NO.703L QUIRURGICA PIEZA CON BLISTER ETIQUETADO CON EL NUMERO DE LA FRESA DE ACERO INOXIDABLE'),('25606','Alambre de osteosíntesis'),('25624','TIRANERVIOS NO. 1-4 DE ACERO INOXIDABLE PARA EXTIRPAR LA PULPA DENTAL INSTRUMENTO PARA ENDODONCIA'),('25927','LIMAS PARA ENDODONCIA DE CONDUCTOS RADICULARES NUMEROS 1-6 DE 25 MM INOXIDABLES'),('25928','LUBRICANTE P/ PIEZA DE MANO EN SPRAY PARA LUBRICACION Y LIMPIEZA DE PIEZA DE MANO DE ALTA VELOCIDAD ESTERILIZABLE CONT. 165 G. FRASCO CON ATOMIZADOR Y APLICADOR'),('25991','FRESA DE BAJA VELOCIDAD DE BOLA NO. 6 QUIRURGICA PIEZA CON BLISTER ETIQUETADO CON EL NUMERO DE LA FRESA DE ACERO INOXIDABLE.'),('50226','JERINGA DESECHABLE PARA APLICAR VACUNAS: CONTRA ANTIHEPATITIS B CAPACIDAD DE 1.0 ML GRADUADA DE MANERA QUE PERMITA IDENTIFICAR FACILMENTE CON UNA LINEA LA DOSIFICACION DE 0.5 ML Y OTRA LINEA LA DE 1 ML. EN EL BARRIL DEBE TENER IMPRESA LA LEYENDA VAC'),('70369','LENTES PROTECTORES DE SEGURIDAD (GOOGLES)'),('80109','PROTECTOR CUTANEO A BASE DE DIMETICONA TUBO DE 4 OZ'),('80110','LIMPIADOR CUTANEO A BASE DE LAURETH GLICERINA Y ACIDO CITRICO FRASCO DE 8 OZ CON APLICADOR'),('80111','MANOPLA SUAVE Y DESECHABLE DE VISCOSA PLASTIFICADA CON ELaSTICO EN LA MUÑECA Y ESPONJA ENJABONADA Y MANOPLA SUAVE Y DESECHABLE DE VISCOSA PLASTIFICADA CON ELaSTICO(KIT)'),('80112','TAPETES PARA CONTROL DE SUCIEDAD Y CONTAMINANTES DE 0.457 X 0.914 CAJA CON 4 PAQUETES DE 30 (120 HOJAS)'),('80113','SOLUCIoN QUIRURGICA QUE CONTIENE YODOFORO (0.7% de yodo libre)ALCOHOL ISOPROPILICO AL 74% Y UN POLIMERO QUE FORMA SOBRE LA PIEL UNA PELICULA. Contiene: DOS HISOPOSUN APLICADOR Y UNA AMPOLLETA O TUBO CON 26 ML. DE SOLUCIoN ESTeRIL.'),('90804','MISOPROSTOL TABLETAS CADA TABLETA CONTIENE MISOPROSTOL 200 MCG.'),('90818','TERMOMETRO DIGITAL OTICO'),('90819','MICRONEBULIZADOR. VASO DOSIFICADOR DE PLASTICO DE 5 CM. CON LINEA DE PRESION DE OXIGENO Y MASCARILLA DE VINIL'),('90820','NEBULIZADOR. VASO DE 500 ML. DE PLASTICO Y BLENDER PARA EL PORCENTAJE'),('90821','SUTURAS. Sintéticas no absorbibles, monofilamento de polipropileno, con aguja. Quirúrgicas. Longitud de la hebra: 90 cm Calibre de la sutura: 5-0 Características de la aguja: 1/2 círculo, doble armado ahusada (15-17 mm).'),('90900','TERMOMETRO DIGITAL FRONTAL'),('91003','CLORPROMAZINA 100MG'),('250440','JERINGA DESECHABLE PARA APLICAR 0.5 ML DE VACUNAS: TRIPLE VIRAL DOBLE VIRAL Y PARA APLICAR 0.1 ML DE VACUNA BCG CAPACIDAD DE 0.5 ML GRADUADA EN DECIMAS DE ML EN EL BARRIL DEBE TEENER IMPRESA LA LEYENDA VACUNACION UNIVERSAL POR CADA JERINGA SE'),('250441','JERINGA DESECHABLE PARA APLICAR 0.5 ML DE LAS VACUNA: ANTIINFLUENZA EN ADULTOS DPT+HB+Hib DPT Y Td CAPACIDAD DE 0.5 ML GRADUADA EN DECIMAS DE ML EN EL BARRIL DEBE TEENER IMPRESA LA LEYENDA VACUNACION UNIVERSAL POR CADA JERINGA SE DEBEN INCLUIR D'),('250443','JERINGA DESECHABLE PARA APLICAR VACUNA: ANTIHEPATITIS B< 1 AÑO Y ANTINEUMOCOCCICA HEPTAVALENTE INFLUENZA INFANTIL DPaT+HIb+IPV. CAPACIDAD DE 0.5 ML GRADUADA DE MANERA QUE PERMITA IDENTIFICAR FACILMENTE CON UNA LINEA LA DOSIFICACION DE 0.5 Y 0.25 ML EN'),('607002','BATA PARA EXPLORACIoN MAMARIA'),('909090','Dique de Hule realizados en látex, sin talco, hipo-alergénicos, resistentes y flexibles, grosor medio 0.20mm, medida estándar 15.25 x 15.25 cm (6x6 pulgadas)'),('909091','Amalgama cápsulas pre dosificadas de auto activación encapsulada (o en cápsulas).  Polvo: plata (Ag) dentro del rango 40% a 78%, estaño (Sn) dentro del rango de 415% a 32%, cobre (Cu) dentro del rango del 10 a 29%.  Líquido:  Mercurio (Hg) dentro del rang'),('909092','Ondasentron solución inyectable 8mg/4ml ampolleta con 4mg de ondasentron (clorhidrato en 2ml de solución acuosa estéril itransaprete incolora para inyección o infusión)'),('909093','Fluoresceina al 10% solución inyectable.  Cada ml contiene:  Fluoresceina sódica 100mg. '),('909094','Tropicamida con fenilefrina.  Cada envase contiene tropicamida 5mg con 50mg de clorhidrato de Fenilefrina'),('909095','Gas intraocular SF6 (Hexafluoruro de azufre) expansivo intraocular'),('909096','Gas C3F8n (Octafluoruro de propano) gas intraocular'),('909097','Azul Brillante, solución colorante para uso intraocular'),('909098','Agua Bidestilada'),('600040109','ABATELENGUAS. DE MADERA; DESECHABLES.LARGO 142.0 MM ANCHO 18.0 MM.'),('600110464','CONECTOR ROTADOR METaLICO PARA CATeTER (MACHO-HEMBRA).'),('600160154','ACEITE MINERAL PARA USO EXTERNO. ENVASE CON 250 ML.'),('600160287','Aceite de silicón para cirugía oftálmica'),('600160287.1','Aceite de silicon 5000mm2/s'),('600160287.2','Aceite de silicón pesado '),('600340103','AGUA OXIGENADA EN CONCENTRACION DEL 2.5-3.5%. ENVASE CON 480 ML.'),('600400287','AGUJA PARA ANESTESIA EPIDURAL. DE PAREDES DELGADAS. TIPO: TOUHY. LONGITUD: 7.5 A 8.6 CM CALIBRE:16 G.'),('600400543','Para raquianestesia o bloqueo subaracnoideo.De acero inoxidable, punta tipo lápiz, conector roscado luer lock hembra translúcido y mandril con botón indicador; sin depósito ó con  Depósito de 0.2ml en pabellón para líquido cefalorraquídeo. Estéril y'),('600403711','AGUJA HIPODERMICA;CON PABELLON LUER LOCK HEMBRA DE PLASTICO DESECHABLES LONGITUD 32 MM CALIBRE 20 G.. ENVASE CON 100 PIEZAS'),('600403729','AGUJA HIPODERMICA;CON PABELLON LUER LOCK HEMBRA DE PLASTICO DESECHABLES LONGITUD 38 MM CALIBRE 20 G. ENVASE CON 100 PIEZAS'),('600403745','AGUJA HIPODERMICA CON PABELLON LUER - LOCK HEMBRA DE PLASTICO DESECHABLES LONGITUD 32 MM CALIBRE 21 G. ENVASE CON 100 PIEZAS'),('600403760','AGUJA HIPODERMICA CON PABELLON LUER - LOCK HEMBRA DE PLASTICO DESECHABLES LONGITUD 16 MM CALIBRE 25 G. ENVASE CON 100 PIEZAS'),('600403786','AGUJA HIPODERMICA CON PABELLOR LUER - LOCK HEMBRA DE PLASTICO DESECHABLES LONGITUD 32 MM CALIBRE 22 G. ENVASE CON 100 PIEZAS'),('600408041','AGUJAS  Dentales. Tipo carpule desechables longitud: 20-25 mm. Calibre: 30 G. Tamaño: Corta. '),('600408058','AGUJAS  Dentales. Tipo carpule. Desechables. Longitud: 25-42 mm. Calibre: 27 G. Tamaño: Larga. '),('600409007','Para raquianestesia o bloqueo subaracnoideo. De acero inoxidable, punta tipo lápiz, conector roscado luer hembra translúcido y mandril con botón indicador; sin depósito ó con depósito de 0.2ml en pabellón para líquido cefalorraquídeo. Estéril y'),('600580153','ALGODON. EN LAMINAS. ENROLLADO O PLISADO. ENVASE CON 300 GR.'),('600660039','ALCOHOL DESNATURALIZADO. ENVASE CON 1 LITRO'),('600660062.01','JABON PARA USO  PREQUIRURGICO LIQUIDO Y  NEUTRO FORMULADO A BASE DE GLUCONATO DE CLOREHEXIDINA AL 4 %'),('600660500','FLUORURO DE  SODIO Para prevención de caries. Acidulado al 2%. En gel de sabor. '),('600660666','ANTISEPTICO Y GERMICIDA. IODOPOVIDONA; SOLUCION. CADA 100 ML.CONTIENEN:IODOPOVIDONA 11 GR.EQUIVALENTE A 1.1 GR.DE YODO. ENVASE CON 3.5 LITROS'),('600660757','ANTISEPTICO Y GERMICIDA. CLORURO DE BENZALCONIO AL 12%.CADA 100 ML. CONTIENEN: CLORURO DE BENZALCONIO 12 GR.;NITRITO DE SODIO (ANTIOXIDANTE) 5 GR.. ENVASE CON 500 ML.'),('600660765','ANTISEPTICO Y GERMICIDA GLUTARALDEHiDO AL 2%.CON ACTIVADOR EN POLVO (COLOR VERDE AL ACTIVARSE) CON EFECTIVIDAD DE 14 DiAS. ENVASE DE PLaSTICO CON 4 L.'),('600660773','ALCOHOL DESNATURALIZADO. ENVASE CON 20 LITROS'),('600660880','Solución concentrada esterilizante en frío del 8 al 12.5% de glutaraldehido, para preparar una dilución de uso final del 2 al 3.5%. Para utilizarse en  instrumental termosensible limpio y sin material orgánico. Frasco con un litro y dosificador integrado'),('600660906.01','LIQUIDO DESINFECTANTE, PARA MANOS Y PIEL QUE NO REQUIERE ENJUAGUE, PARA SER UTILIZADO EN AREAS BLANCAS Y/O AISLADAS. FORMULADO A BASE DE ALCOHOL ETILICO AL 73% ADICIONADO CON GLICERINA Y BISABOLOL COMO DERMOPROTECTORES.'),('600660914','Jabón líquido desinfectante para lavado pre y postquirúrgico de manos y piel, formulado a base de 0.75% mínimo de triclosan, 1.1% mínimo de ortofenilfenol, con 10% mínimo de jabón anhidro de coco en base seca, humectantes y suavizantes'),('600660971','DETERGENTE O LIMPIADOR ENZIMATICOMONOENZIMATICO COMPUESTO DE CLORURO DE DODECIL O DIDECIL DIMETILAMONIO ENZIMAS PROTEOLITICAS pH QUE ASEGURE LA ACCION OPTIMA DE LAS ENZIMAS ACTIVO EN TODO TIEMPO DE AGUA NO CORROSIVO. SOBRE DE 20 A 25 GRAMOS. ENVASE CO'),('600661052','Gluconato de Clorhexidina al 2% en alcohol isopropílico al 70% de 3ml'),('600661060','Solucion con Gluconato de Clorhexidina al 2% p/v en alcohol isopropilico al 70% con tinta naranja.\nContiene: 26 ml  Esteril y desechable'),('600820104','APLICADORES CON ALGODoN. DE MADERA.ENVASE CON 150 PIEZAS. ENVASE CON 150 PIEZAS'),('600880017','APOSITOS. TRANSPARENTE MICROPOROSO AUTOADHERIBLE ESTeRIL Y DESECHABLE. MEDIDAS: 7.0 A 8.5 X 5.08 A 6.0 CM.. ENVASE CON 50 PIEZAS'),('600880025','APOSITO TRANSPARENTE MICROPOROSO AUTOADHERIBLE ESTeRIL Y DESECHABLE. MEDIDAS: 10.0 a 10.16 x 12.0 a 14.0 CM..ENVASE CON 50 PIEZAS'),('600880033','APOSITO PARA CIRUGIA PARODONTAL ESTUCHE CON : POLVO. OXIDO DE ZINC 70.25 G. BREA OXIGENADA 29.45 G. ACETATO DE ZINC 0.30 G. LIQUIDO: EUGENOL 85 ML. ACEITE DE OLIVA 15 ML.'),('600880058','APOSITOS. COMBINADOS. DE CELULOSA CON TELA NO TEJIDA MEDIDAS: 20 X 8 CM. ENVASE CON 200 PIEZAS'),('600880108','APOSITOS COMBINADOS. DE CELULOSA CON TELA NO TEJIDA. MEDIDAS 20 X 13 CM. ENVASE CON 150 PIEZAS'),('601000011','BABERO DE TELA NO TEJIDA DE RAYON ANATOMICO AJUSTABLE DESECHABLE TAMAÑO ADULTO'),('601250228','BOLSA PARA UROCULTIVO (NIÑO). ESTeRIL DE PLaSTICO GRADO MeDICO FORMA RECTANGULAR CON CAPACIDAD DE 50 ML Y ESCALA DE 10 20 30 Y 50 ML CON ORIFICIO REDONDO DE 30 MM aREA ADHESIVA DE 45 X 60 ML. PIEZA.'),('601250236','BOLSA. Para enema.Capacidad 1500 ml con tubo transportador de 5.0 a 6.0\nmm de diámetro interno, 128 cm de Longitud y dispositivo obturador de plástico para control de flujo. El\nextremo proximal debe tener la punta roma sin filos, un orificio lateral'),('601250244','BOLSAS PARA UROCULTIVO (NIÑA) ESTERIL DE PLASTICO GRADO MEDICO; FORMA RECTANGULAR; CON CAPACIDAD DE 50 ML. Y ESCALA A 10; 20; 30; Y 50 ML.; CON ORIFICIO EN FORMA DE PERA 2.5 CM EN SU LADO MAS ANCHO Y 1 CM. EN EL MAS ANGOSTO AREA ADHESIVA DE 45 X 60 MM.'),('601250491','Bolsa de propileno, desechable, flexible, transparente, para aspiración y recolección de fluidos corporales, con tapa de poliestireno ensamblada en una sola pieza con cuatro puertos, cada puerto con leyenda indicando su función.\n1. Puerto para paciente:\n '),('601250582','BOLSA para ileostomia o colostomia. Autoadherible, de plastico, grado medico, suave, transparente, a prueba de olor, drenable en forma de botella de 30 x 15 cm, abierta en su parte mas angosta, con cuello de 6 a 9 cm de ancho, y de 3.0 a 6.2 cm de largo'),('601251879','BOLSA PARA RECOLECCION DE ORINA.RECTANGULAR; ELABORADA A BASE DE CLORURO DE POLIVINILO.CON GRADUACIONES CADA 100 ML.Y LECTURA CADA 200 ML.;SISTEMA CERRADO. CAPACIDAD 2000 ML.'),('601252653','BOLSA DE PAPEL GRADO MEDICO PARA ESTERILIZAR EN GAS O VAPOR CON O SIN TRATAMIENTO ANTIBACTERIANO CON REACTIVO QUIMICO IMPRESO Y SISTEMA DE APERTURA.MEDIDAS: 7.5 X 23.0 X 4.0 CM. ENVASE CON 1000 PIEZAS'),('601252836','BOLSA DE PAPEL GRADO MEDICO PARA ESTERILIZAR EN GAS O VAPOR CON O SIN TRATAMIENTO ANTIBACTERIANO CON REACTIVO QUIMICO IMPRESO Y SISTEMA DE APERTURA.MEDIDAS: 25 X 38 X 8.0 CM. ENVASE CON 250 PIEZAS.'),('601300015','Bota quirúrgica de tela no tejida 100%\nde polipropileno, tipo SMS, de 35 g/m2\nmínimo, impermeable a la penetración de líquidos y fluidos, antiestática, con dos cintas de sujeción. Desechable.'),('601320054','BRAZALETES PARA IDENTIFICACION; DE PLASTICO.ADULTO. ENVASE CON 100 PIEZAS'),('601320203','BRAZALETES PARA IDENTIFICACION; DE PLASTICO. INFANTIL. ENVASE CON 100 PIEZAS'),('601570104','CAL SODADA. CON INDICADOR. LATA CON 16 KG.'),('601650815','CATETERES PARA CATETERISMO VENOSO CENTRAL DE UN LUMEN DE ELASTOMERO DE SILICON RADIOPACO CON AGUJA INTRODUCTORA PERCUTANEA. ESTERIL Y DESECHABLE. NEONATAL. CALIBRE. 2.0 A 3.0 FR'),('601650823','CATETER PARA CATETERISMO VENOSO CENTRAL DE UN LUMEN DE ELASTOMERO DE SILICON RADIOPACO CON AGUJA INTRODUCTORA PERCUTANEA.ESTERIL Y DESECHABLE NEONATAL DE ( 4.0 FR. )'),('601650849','Para cateterismo venoso central, de doble lumen, de inserción periférica, de poliuretano o elastómero de silicón, con aguja introductora con funda o camisa desprendible. Estéril y desechable. Tamaño neonatal. Calibre 1.9 a 3.0 Fr.'),('601660103','CATETER PARA VENOCLISIS; DE POLIURETANO RADIOPACO. CON AGUJA LONGITUD 17-24 MM. CALIBRE 24G. ENVASE CON 50 PIEZAS'),('601660558','CANULAS PARA ASPIRACIoN MANUAL ENDOUTERINA DE POLIETILENO FLEXIBLE ESTeRIL Y DESECHABLE.DIaMETRO. 4 MM COLOR AMARILLO'),('601660566','CANULAS PARA ASPIRACIoN MANUAL ENDOUTERINA DE POLIETILENO FLEXIBLE ESTeRIL Y DESECHABLE.DIaMETRO. 5 MM VERDE'),('601660574','Cánula de polietileno, flexible, estéril desechable (usarse una vez) para aspiración manual endouterina, color azul 6mm'),('601660582','Cánula de polietileno, flexible, estéril desechable (usarse una vez) para aspiración manual endouterina, color café clarol 7mm'),('601660590','CANULAS PARA ASPIRACIoN MANUAL ENDOUTERINA DE POLIETILENO FLEXIBLE ESTeRIL Y DESECHABLE.DIaMETRO 8 MM . COLOR MARFIL'),('601660608','Cánula de polietileno, flexible, estéril desechable (usarse una vez) para aspiración manual endouterina, color café oscuro 9mm'),('601660616','CANULAS PARA ASPIRACIoN MANUAL ENDOUTERINA DE POLIETILENO FLEXIBLE ESTeRIL Y DESECHABLE.DIaMETRO 10 MM . COLOR VERDE SECO'),('601660624','Cánula de polietileno, flexible, estéril desechable (usarse una vez) para aspiración manual endouterina, color azul oscuro 12mm'),('601660657','SONDA PARA ESOFAGO. DE TRES VIAS PUNTA CERRADA CON CUATRO ORIFICIOS DE LATEX CON ARILLO RADIOPACO. ESTERIL Y DESECHABLE. TIPO; SENGSTAKEN BLAKEMORE. LONGITUD 100 CM CALIBRE 16 FR.'),('601661671','CANULAS PARA DRENAJE TORaCICO.RECTA CON MARCA RADIOPACA. LONGITUD:45 CM CALIBRE: 36 FR.'),('601661911','Catéter venoso central, calibre 5 Fr y 13 cm de longitud, de poliuretano o  silicón, radiopaco, estéril y desechable, con dos lúmenes internos calibres 18 G y 20 G, con punta flexible, con aguja calibre 20 G, con catéter introductor calibre 20 G,'),('601670458','CANULAS OROFARINGEAS DE PLASTICO TRANSPARENTE TIPO GUEDEL/BERMAN TAMAÑO; 0 LONGITUD 50 MM.'),('601670466','CANULAS OROFARINGEAS DE PLASTICO TRANSPARENTE TIPO GUEDEL/BERMAN TAMAÑO; 2 LONGITUD 70 MM.'),('601670482','CANULAS OROFARINGEAS DE PLASTICO TRANSPARENTE TIPO GUEDEL/BERMAN TAMAÑO 4 LONGITUD 90 MM.'),('601670680','CANULAS OROFARINGEAS DE PLASTICO TRANSPARENTE. TIPO GUEDEL/BERMAN TAMAÑO 6 LONGITUD 110 MM.'),('601670789','CATETERES. Para cateterismo venoso central, radiopaco estérily desechable, de poliuretano, que permita retirar la\naguja y el mandril una vez instalado, longitud 60 a 70 cm, calibre l6 G con aguja de 3.5 a 6.5 cm de largo, de pared delgada calibre 14'),('601671779','Para registro de presión venosa, aurícula izquierda, presión arterial sistémica y pulmonar. De plástico con balón para flotación. Tipo: Swan- Ganz.\nLongitud: 80 cm Calibre:  5 Fr.'),('601671977','Para registro de presión venosa, aurícula izquierda, presión arterial sistémica y pulmonar. De plástico con balón para flotación. Tipo: Swan-Ganz.\nLongitud: 110 cm Calibre 7 Fr.'),('601672884','CATETER.  Epidural con adaptador guía, estéril, desechable, calibre 18 o 19 G, de material plástico flexible, radiopaco, resistente a acodaduras con marcas indelebles cm a cm, iniciando a partir de 4.8 a 5.5cm, del primer orificio proximal hasta 20'),('601673312','CANULA OROFARINGEA.DE PLASTICO TRANSPARENTE.TIPO .GU EDEL/BERMAN; TAMAÑO 1.LONGITUD.60 MM.'),('601673320','CANULA OROFARINGEA.DE PLASTICO TRANSPARENTE.TIPO .GU EDEL/BERMAN; TAMAÑO 3. LONGITUD 80 MM.'),('601673346','CANULAS OROFARINGEAS DE PLASTICO TRANSPARENTE TIPO TIPO GUEDEL/BERMAN TAMAÑO; 5 LONGITUD 100 MM.'),('601674930','SONDA DE LaTEX PUNTA REDONDA.TIPO: NELATON.LONGITUD 40 CM.CALIBRE 28 FR.'),('601675010','CATETER PARA SUMINISTRO DE OXIGENO. CON TUBO DE CONEXION Y CANULA NASAL DE PLASTICO CON DIAMETRO INTERNO DE 2.0 MM. LONGITUD 180 CM.PIEZA'),('601676638','CATETERES PARA VASOS UMBILICALES. RADIOPACOS DE CLORURO DE POLIVINILO O POLIURETANO. ESTERILES Y DESECHABLES. LONGITUD. 35 A 38 CM CALIBRE. 3.5 FR CON ACOTACIONES A 5 10 Y 15 CM.'),('601676646','CATETERES PARA VASOS UMBILICALES. RADIOPACOS DE CLORURO DE POLIVINILO O POLIURETANO. ESTERILES Y DESECHABLES LONGITUD. 35 A 38 CM CALIBRE. 5.0 FR CON ACOTACIONES A 5 10 Y 15 CM.'),('601676661','CATETER.  Para cateterismo venoso central, calibre 7 Fr x 20 cm de longitud de poliuretano o silicón, con punta flexible, radiopaco, con tres lúmenes internos, distal calibre 16 G, medio calibre 18 G y proximal calibre 18 G.\nDispositivo de fijación'),('601678089','SONDA. PARA ALIMENTACION. DE PLASTICO TRANSPARENTE ESTERIL Y DESCH. CON UN ORIFICIO EN EL EXTREMO PROXIMAL Y OTRO EN LOS PRIMEROS 2 CM TAMAÑO INFANTIL LONGITUD 38.5 CM. CAL. 8 FR.'),('601678220','TUBO ENDOTRAQUEAL SIN GLOBO DE ELASTOMERO DE SILICON TRANSPARENTE GRADUADOS CON MARCA RADIOPACA ESTERILES Y DESECHABLES CALIBRE 8 FR.'),('601678238','TUBO ENDOTRAQUEAL SIN GLOBO DE ELASTOMERO DE SILICON TRANSPARENTE GRADUADOS CON MARCA RADIOPACA ESTERILES Y DESECHABLES CALIBRE 10 FR.'),('601679999','BALON DE BAKRI'),('601680077','SONDA. PARA ASPIRAR SECRECIONES; DE PLASTICO; CON VALVULA DE CONTROL. ESTERIL Y DESECHABLE. TAMAÑO ADULTO. LONGITUD 55 CM CALIBRE 18 FR. DIAMETRO EXTERNO 6.0 MM.'),('601680085','SONDA. PARA ASPIRAR SECRECIONES; DE PLASTICO; CON VALVULA DE CONTROL ESTERIL Y DESECHABLE. TAMAÑO INFANTIL LONGITUD 55 CM CALIBRE 10 FR. DIAMETRO EXTERNO 3.3 MM.'),('601680440','CATETER PARA CATETERISMO VENOSO CENTRAL RADIOPACO ESTeRIL Y DESECHABLE DE POLIURETANO LONGITUD 30.5 CM CALIBRE 18 G CON AGUJA DE 5.2 A 6.5 CM DE LARGO DE PARED DELGADA CALIBRE 16 G CON MANDRIL Y ADAPTADOR PARA VENOCLISIS LUER LOCK. PIEZA.'),('601681356','TUBOS Endotraqueales. De plástico grado médico, con marca radiopaca,\nestériles, desechables, con globo de alto volumen y bajapresión, incluye una válvula, un conector y una escala\nen mm para determinar la profundidad de la colocación del tubo.'),('601681455','TUBO ENDOTRAQUEAL SIN GLOBO DE ELASTOMERO DE SILICON TRANSPARENTE GRADUADOS CON MARCA RADIOPACA ESTERILES Y DESECHABLES CALIBRE 12 FR.'),('601681752','SONDA DE LATEX PUNTA REDONDA. TIPO NELATON. LONGITUD 40 CM. CALIBRE 8 FR.'),('601682214','TUBO. Endotraqueales. De plástico grado médico, con marca radiopaca,\nestériles, desechables, con globo de alto volumen y baja presión, incluye una válvula, un conector y una escala\nen mm para determinar la profundidad de la colocación del tubo.'),('601682446','TUBOS. Endotraqueales. De plástico grado médico, con marca radiopaca,\nestériles, desechables, con globo de alto volumen y baja presión, incluye una válvula, un conector y una escala\nen mm para determinar la profundidad de la colocación del tubo.'),('601682453','CATÉTERES. Para cateterismo venoso central, radiopaco, estéril y desechable de poliuretano, que permita retirar la aguja y el mandril una vez instalado, longitud 30.5 cm calibre l6 G con aguja de 5.2 a 6.5 cm de largo, de pared Delgada calibre l4 G,'),('601682495','TUBOS.  Endotraqueales. De plástico grado médico, con marca radiopaca,\nestériles, desechables, con globo de alto volumen y baja presión, incluye una válvula, un conector y una escala\nen mm para determinar la profundidad de la colocación del tubo.'),('601682511','TUBOS. Endotraqueales. De plástico grado médico, con marca radiopaca,\nestériles, desechables, con globo de alto volumen y baja presión, incluye una válvula, un conector y una escala\nen mm para determinar la profundidad de la colocación del tubo.'),('601682529','TUBOS. Endotraqueales. De plástico grado médico, con marca radiopaca,\nestériles, desechables, con globo de alto volumen y baja presión, incluye una válvula, un conector y una escala\nen mm para determinar la profundidad de la colocación del tubo.'),('601682537','TUBOS. Endotraqueales. De plástico grado médico, con marca radiopaca,\nestériles, desechables, con globo de alto volumen y baja presión, incluye una válvula, un conector y una escala\nen mm para determinar la profundidad de la colocación del tubo.'),('601682552','TUBOS. Endotraqueales. De plástico grado médico, con marca radiopaca,\nestériles, desechables, con globo de alto volumen y baja presión, incluye una válvula, un conector y una escala\nen mm para determinar la profundidad de la colocación del tubo.'),('601682560','TUBOS. Endotraqueales. De plástico grado médico, con marca radiopaca,\nestériles, desechables, con globo de alto volumen y baja presión, incluye una válvula, un conector y una escala\nen mm para determinar la profundidad de la colocación del tubo.'),('601682578','TUBOS. Endotraqueales. De plástico grado médico, con marca radiopaca,\nestériles, desechables, con globo de alto volumen y baja presión, incluye una válvula, un conector y una escala\nen mm para determinar la profundidad de la colocación del tubo.'),('601682594','TUBOS. Endotraqueales. De plástico grado médico, con marca radiopaca,\nestériles, desechables, con globo de alto volumen y baja presión, incluye una válvula, un conector y una escala\nen mm para determinar la profundidad de la colocación del tubo.'),('601683311','SONDA PARA DRENAJE URINARIO. DE LATEX. CON GLOBO DE AUTORRETENCION DE 3 ML CON VALVULA PARA JERINGA. ESTERIL Y DESECHABLE. TIPO FOLEY DE DOS VIAS CALIBRE 8 FR.'),('601683394','CATETER PARA DIALISIS PERITONEAL. DE INSTALACION SUBCUTANEA BLANDO DE SILICON CON UN COJINETE DE POLIESTER CON CONECTOR Y TAPON LUER LOCK SEGURO CON BANDA RADIOPACA .ESTERIL Y DESECHABLE TIPO TENCKHOFF ADULTO. LONGITUD 40 A 43 CM.'),('601684186','CATETER PARA DIALISIS PERITONEAL. DE INSTALACION SUBCUTANEA BLANDO DE SILICON CON UN COJINETE DE POLIESTER CON CONECTOR Y TAPON LUER LOCK SEGURO CON BANDA RADIOPACA TIPO TENCKHOFF. INFANTIL. LONGITUD 30 A 37 CM.'),('601684277','SONDA GASTROINTESTINAL DESECHABLE Y CON MARCA OPACA A LOS RAYOS X TIPO. LEVIN; CALIBRE 12 FR.'),('601684418','SONDA GASTROINTESTINAL DESECHABLE Y CON MARCA OPACA A LOS RAYOS X TIPO. LEVIN; CALIBRE 18 FR.'),('601684475','CATETER PARA VENOCLISIS CALIBRE 17G PUNZOCAT 1.5 (38MM) OD 1.40 MM FLUJO 104 ML/MIN'),('601685340','TUBO ENDOTRAQUEAL SIN GLOBO DE ELASTOMERO DE SILICON TRANSPARENTE GRADUADOS CON MARCA RADIOPACA ESTERILES Y DESECHABLES CALIBRE 14 FR.'),('601685365','TUBO ENDOTRAQUEAL SIN GLOBO DE ELASTOMERO DE SILICON TRANSPARENTE GRADUADOS CON MARCA RADIOPACA ESTERILES Y DESECHABLES CALIBRE 16 FR.'),('601685381','TUBO ENDOTRAQUEAL SIN GLOBO DE ELASTOMERO DE SILICON TRANSPARENTE GRADUADOS CON MARCA RADIOPACA ESTERILES Y DESECHABLES CALIBRE 18 FR.'),('601685407','TUBO ENDOTRAQUEAL SIN GLOBO DE ELASTOMERO DE SILICON TRANSPARENTE GRADUADOS CON MARCA RADIOPACA ESTERILES Y DESECHABLES CALIBRE 22 FR.'),('601686413','SONDA URETRALES PARA IRRIGACION CONTINUA; DE LATEX CON GLOBO DE 30 ML Y VALVULA TIPO FOLEY-OWEN (DE 3 VIAS) CALIBRE 18 FR.'),('601686439','SONDA URETRALES PARA IRRIGACIoN CONTINUA.DE LaTEX CON GLOBO DE 30 ML Y VaLVULA.TIPO: FOLEY-OWEN (DE 3 ViAS).CALIBRE.20 FR.'),('601686454','SONDA URETRALES PARA IRRIGACIoN CONTINUA. DE LaTEX CON GLOBO DE 30 ML Y VaLVULA. TIPO: FOLEY-OWEN (DE 3 ViAS). CALIBRE 22 FR'),('601686512','SONDA URETRALES PARA IRRIGACIoN CONTINUA.DE LaTEX CON GLOBO DE 30 ML Y VaLVULA.TIPO: FOLEY-OWEN (DE 3 ViAS).CALIBRE.24 FR'),('601686603','CATETER PARA VENOCLISIS. DE POLIURETANO RADIOPACO CON AGUJA. LONGITUD: 46-52 MM CALIBRE:14 G. ENVASE CON 50 PIEZAS'),('601686611','SONDA DE LATEX PUNTA REDONDA; TIPO NELATON LONGITUD 40 CM CALIBRE 12 FR.'),('601686629','CATETER PARA VENOCLISIS. DE POLIURETANO RADIOPACO CON AGUJA. LONGITUD: 46-52 MM CALIBRE:16 G.ENVASE CON 50 PIEZAS'),('601686637','SONDA DE LATEX PUNTA REDONDA. TIPO; NELATON. LONGITUD 40 CM CALIBRE 14 FR.'),('601686645','CATETER PARA VENOCLISIS DE POLIURETANO RADIOPACO CON AGUJA LONGITUD 28 - 34 MM. CALIBRE 18 G. ENVASE CON 50 PIEZAS.'),('601686652','SONDA DE LATEX PUNTA REDONDA. TIPO; NELATON. LONGITUD 40 CM CALIBRE 16 FR.'),('601686660','CATETER. Para venoclisis. De Fluoropolímeros (Politetrafluoretileno, luoretilenpropileno y Etilentrifluoretileno) o poliuretano, radiopaco, con aguja. Longitud: 28-34 mm Calibre: 20 G.'),('601686678','SONDA DE LATEX PUNTA REDONDA TIPO NELATON. LONGITUD 40 CM. CALIBRE 18 FR.'),('601686686','CATETER PARA VENOCLISIS DE POLIURETANO RADIOPACO CON AGUJA LONGITUD 23 - 27 MM. CALIBRE 22 G. ENVASE CON 50'),('601688302','SONDA DE LATEX PUNTA REDONDA; TIPO NELATON. LONGITUD 40 CM. CALIBRE 20 FR.'),('601688310','SONDA DE LATEX PUNTA REDONDA; TIPO NELATON. LONGITUD 40 CM. CALIBRE 22 FR.'),('601689243','SONDA PARA ALIMENTACION. DE PLASTICO TRANSPARENTE; ESTERIL Y DESECHABLE CON UN ORIFICIO EN EL EXTREMO PROXIMAL Y OTRO EN LOS PRIMEROS 2 CM. TAMAÑO PREMATURO LONGITUD 38.5 CM. CALIBRE 5 FR.'),('601689268','SONDA. Para alimentación. De plástico transparente, estéril y desechable con un orificio en el extremo proximal y otro en los primeros 2 cm.  Tamaño: Adulto Longitud: 125.0 cm Calibre: 16 Fr.'),('601689375','CATETER PARA DIALISIS PERITONEAL; DE PLASTICO; RIGIDO ESTERIL Y DESECHABLE; CON ORIFICIOS LATERALES ESTILETE METALICO Y TUBO DE CONEXION. ADULTO.'),('601689417','SONDA PARA DRENAJE. EN FORMA DE T. DE LaTEX. TIPO: KEHR. CALIBRE 12 FR.'),('601689425','SONDAS PAR DRENAJE EN FORMA DE T DE LATEX TIPO KEHR 14 FR.'),('601689433','SONDAS PAR DRENAJE EN FORMA DE T DE LATEX TIPO KEHR 16 FR.'),('601689441','SONDAS PAR DRENAJE EN FORMA DE T DE LATEX TIPO KEHR 18 FR.'),('601689623','SONDA PARA DRENAJE URINARIO DE LATEX; ;CON GLOBO DE AUTORETENCION; DE 5 ML CON VALVULA PARA JERINGA. ESTERIL Y DESECHABLE TIPO. FOLEY (DE DOS VIAS) CAL.14.FR.'),('601689631','SONDA PARA DRENAJE URINARIO DE LATEX; CON GLOBO DE AUTORRETENCION; DE 5 ML CON VALVULA PARA JERINGA ESTERIL Y DESECHABLE TIPO FOLEY DE DOS VIAS CALIBRE 16 FR.'),('601689649','SONDA PARA DRENAJE URINARIO DE LATEX;;CON GLOBO DE AUTORRETENCION; DE 5 ML CON VALVULA PARA JERINGA ESTERIL Y DESECHABLE. TIPO FOLEY DE DOS VIAS CALIBRE 18 FR.'),('601689656','SONDA PARA DRENAJE URINARIO. DE LATEX. CON GLOBO DE AUTORRETENCION DE 5 ML CON VALVULA PARA JERINGA. ESTERIL Y DESECHABLE. TIPO FOLEY DE DOS VIAS CALIBRE 20 FR.'),('601689672','SONDA PARA DRENAJE URINARIO.DE LaTEX CON GLOBO DE AUTORRETENCIoN DE 5 ML CON VaLVULA PARA JERINGA. ESTeRIL Y DESECHABLE.TIPO: FOLEY DE DOS ViAS.CALIBRE 24 Fr.'),('601689896','SONDA GASTROINTESTINAL ; DESECHABLE; Y CON MARCA OPACA A LOS RAYOS X. TIPO LEVIN CALIBRE 14 FR.'),('601689904','SONDA GASTROINTESTINAL; DESECHABLE; Y CON MARCA OPACA A LOS RAYOS X. TIPO LEVIN. CALIBRE 16 FR.'),('601820160','CEMENTOS Ionómero de vidrio I. Para cementaciones definitivas. Polvo 35 g. Silicato de aluminio\n95% -97%. Acido poliacrílico 3% - 5%. Líquido 25 g, 20 ml. Acido poliacrílico 75%. Acido polibásico\n10-15%. '),('601821150','PROTECTOR PULPAR PARA SELLAR CAVIDADES DENTALES DE HIDROXIDO DE CALCIO COMPUESTO AUTOPOLIMERIZABLE 2 PASTAS SEMI LIQUIDAS BASE 13 G. CATALIZADOR 11G. CON BLOQUE DE PAPEL PARA MEZCLAR. ESTUCHE CON 1 JUEGO Y APLICADOR DESECHABLE'),('601821176','CEMENTO DENTAL DE OXIFOSFATO DE ZINC POLVO Y LIQUIDO; CAJA CON 32 G. DE POLVO Y 15 ML. DE SOLVENTE'),('601821275','CEMENTO DENTAL PARA RESTAURACION INTERMEDIA DE OXIDO DE ZINC (POLVO) 38 G Y EUGENOL (LIQUIDO) 14ML. CON GOTERO DE PLASTICO'),('601821283','CEMENTO. Dental para uso quirúrgico, para sellar conductos radiculares.\nPolvo de óxido de zinc y sulfato de calcio (resina). Líquido: 7 ml (Eugenol).'),('601821366','CEMENTO DENTAL DE OXIDO DE -ZINC CON ENDURECEDOR POLVO65 G Y EUGENOL LIQUIDO 30 MLCON GOTERO DE PLASTICO.'),('601890015','CEPILLOS. DENTAL PARA ADULTO CON MANGO DE PLASTICO Y CERDAS RECTAS DE NYLON 6.12 100 % VIRGEN O POLIESTER P.B.T. 100 % VIRGEN DE PUNTAS REDONDEADAS EN 4 HILERAS CABEZA CORTA CONSISTENCIA MEDIANA.'),('601890106','CEPILLOS. DENTAL INFANTIL CON MANGO DE PLASTICO Y CERDAS RECTAS DE NYLON 6.12 100 % VIRGEN O POLIESTER P.B.T. 100 % VIRGEN DE PUNTAS REDONDEADAS EN 3 HILERAS CABEZA CORTA CONSISTENCIA MEDIANA.'),('601890205','CEPILLOS DE CERDAS NEGRAS EN FORMA DE BROCHA P/ PIEZA DE MANO'),('601890304','CEPILLO PARA USO QUIRURGICO.DE PLASTICO.DE FORMA RECTANGULAR; CON DOS AGARRADERAS LATERALES SIMETRICAS Y CERDAS DE NYLON.'),('601960057','CERA P/HUESO (PASTA BECK). ESTERIL.SOBRE CON 2.5 G. ENVASE CON 12 SOBRES'),('602030066','CINTA PARA PORTA MATRIZ DE AMALGAMA METALICA DE 7 MM DE LONGITUD ENVASE CON 12 PIEZAS'),('602030108','CINTA MeTRICA.AHULADA GRADUADA EN CENTiMETROS Y MILIMETROS. LONGITUD 1.50 M'),('602030165','CINTA UMBILICAL .DE ALGODON;TEJIDO PLANO (TRENZADO DE 21 HILOS); ESTERILES. LONG.:41 CM.;ANCHO:4 MM. ENVASE CON 100 SOBRES'),('602030207','CINTA PARA ESTERILIZACION EN VAPOR A PRESION TAMAÑO 18 MM X 50 MTS.'),('602030306','CINTA MICROPOROSA DE TELA NO TEJIDA UNIDIRECCIONAL DE COLOR BLANCO CON RECUBRIMIENTYOS ADHESIVOS EN UNA DE SUS CRAS. LONGITUD 10M. ANCHO 1.25 CM. ENVASE CON 24 ROLLOS'),('602030363','CINTA MICROPOROSA DE TELA NO TEJIDA UNIDIRECCIONAL DE COLOR BLANCO CON RECUBRIMIENTOS ADHESIVOS EN UNA DE SUS CARAS. LONGITUD 10 M ANCHO 5.00 CM. ENVASE CON 6 ROLLOS'),('602030397','CINTA MICROPOROSA DE TELA NO TEJIDA UNIDIRECCIONAL DE COLOR BLANCO CON RECUBRIMIENTOS ADHESIVOS EN UNA DE SUS CARAS. LONGITUD 10 M. ANCHO 2.50 CM. ENVASE CON 12 ROLLOS.'),('602030405','CINTA MICROPOROSA DE TELA NO TEJIDA. UNIDIRECCIONAL DE COLOR BLANCO CON RECUBRIMIENTOS ADHESIVOS EN UNA DE SUS CARAS. LONGITUD 10 M. ANCHO 7.50 CM. ENVASE CON 4 ROLLOS'),('602070013','CIRCUITOS DE VENTILACIoN PARA ANESTESIA DE POLIVINILO CONSTA DE DOS MANGUERAS UN FILTRO CONEXIoN EN ?Y? DE PLaSTICO CODO MASCARILLA Y BOLSAS DE 3 Y 5 L. EQUIPO'),('602190068','REVELADORES DE PLACA DENTOBACTERIANA TABLETA SIN SABOR. ENVASE CON 100 PIEZAS'),('602310104','COMPRESA PARA VIENTRE; DE ALGODON.CON TRAMA OPACA A LOS RAYOS X.LONG.70 CM.;ANCHO 45 CM. ENVASE CON 6 PIEZAS'),('602330011','CONECTOR. DE UNA VIA; DE PLASTICO DESECHABLE TIPO SIMS. DELGADO.'),('602330052','CONECTOR DE UNA ViA.DE PLaSTICO DESECHABLES.TIPO: SIMS.GRUESO'),('602330235','CONECTOR DE TITANIO LUER LOCK.PARA AJUSTAR LA PUNTA DEL CATeTER A LA LiNEA DETRANSFERENCIA.TIPO: TENCKHOFF. PIEZA'),('602330300','CONECTOR DE PLaSTICO CON TRANSPARENCIA DE CRISTAL EN ?Y? DE 12 MM EN SUS 3 ENTRADAS. PIEZA.'),('603080193','DISPOSITIVOS Dispositivo Intrauterino, T de cobre para nulíparas, estéril, con 380 mm2 de cobre enrollado con bordes redondos, con longitud horizontal de 22.20 a 23.20 mm, longitud vertical de\n28.0 a 30.0 mm, filamento de 20 a 25 cm, bastidor con una mezc'),('603140054','EQUIPO. PARA DRENAJE DE LA CAVIDAD PLEURAL. CON TRES CAMARAS PARA SELLO DE AGUA; SUCCION Y COLECCION DE LIQUIDOS. CON DOS VALVULAS DE SEGURIDAD DE ALTA PRESION POSITIVA Y NEGATIVA. ESTERIL Y DESECHABLE. CAPACIDAD 2100 A 2500 ML.'),('603300054','Electrodo de broche desechable para monitoreo cardiaco continuo, con gel conductor, con respaldo de tela no tejida, suave con la piel, con adhesivo grado médico, medida estándar, diámetro 4.5 cm.'),('603300054.1','Electrodo de broche desechable para monitoreo cardiaco continuo, con gel conductor, con respaldo de tela no tejida, suave con la piel, con adhesivo grado médico,  para área neonatal  de 3 cm. de diámetro'),('603450115','EQUIPO. Conector luer-lock con línea de transferencia para unirseal catéter tipo Tenckhoff, con cierre hermético para pacientes en diálisis peritoneal continua ambulatoria, incluye: filtro protector, conector, tubo de extensión,\nsistema de regulación'),('603450305','EQUIPO PARA MEDICIoN DE PRESIoN VENOSA CENTRAL.CONSTA DE:UNA LLAVE DE 3 ViAS.UNA ESCALA PARA MEDIR EN MILiMETROS.TUBO DE CONEXIoN AL PACIENTE.TUBO DE CONEXIoN AL FRASCO DE SOLUCIoN.TUBO PARA MEDIR LA PRESIoN CON INDICADOR FLOTANTE.'),('603450503','EQUIPO PARA APLICACIoN DE VOLuMENES MEDIDOS. DE PLaSTICO GRADO MeDICO ESTeRIL DESECHABLE CONSTA DE: BAYONETA FILTRO DE AIRE CaMARA BURETA FLEXIBLE CON UNA CAPACIDAD DE 100 ML Y ESCALA GRADUADA EN MILiMETROS CaMARA DE GOTEO FLEXIBLE MICROGOTERO TUB'),('603451865','EQUIPO PARA DRENAJE POR ASPIRACIoN PARA USO POSTQUIRuRGICO.CONSTA DE: FUELLE SUCCIONADOR SONDA CONECTORA CINTA DE FIJACIoN SONDA DE SUCCIoN MULTIPERFORADA CON DIaMETRO EXTERNO DE 3 MM CON VaLVULA DE REFLUJO Y VaLVULA DE ACTIVACIoN.'),('603451873','EQUIPO PARA DRENAJE POR ASPIRACION PARA USO POSTQUIRURGICO. CONSTA DE FUELLE SUCCIONADOR SONDA CONECTORA CINTA DE FIJACION SONDA DE SUCCION MULTIPERFORADA CON DIAMETRO EXTERNO DE 6 MM CON VALVULA DE REFLUJO Y VALVULA DE ACTIVACION.'),('603452152','EQUIPO. Básico para bloqueo epidural, contiene: Aguja Tipo Tuohy, calibre 16 o 17G, longitud de 75 a 91 mm, con adaptador luer lock hembra y mandril\nplástico con botón indicador de orientación del bisel, con o sin orificio en la parte curva del bisel.'),('603453135','SISTEMAS. Sistema de succión, cerrado, para paciente con tubo endotraqueal conectado a ventilador, 14 Fr, contiene: Un tubo de succión de cloruro de polivinilo, con marca de  profundidad de 2 cm, empezando desde los 10 cm hasta 42 cm y una marca tope.'),('603453143','Sistema de succión cerrado, para paciente con tubo endotraqueal conectado a ventilador, 16 Fr, contiene: Un tubo de succión de cloruro de polivinilo, con marca de profundidad de 2 cm, empezando desde los 10 cm hasta 42 cm y una marca tope. Dos orificios'),('603453168','Sistema de succión, cerrado, para paciente con traqueostomía, conectado a ventilador, 14 Fr, contiene: Un tubo de succión de cloruro de polivinilo, de 30.5 cm de largo, marca tope a 4.5 cm. Dos orificios laterales en la punta proximal del tubo, envuelto'),('603453424','Equipo para Anestesia Epidural, contiene:  Aguja modelo Tuohy calibre 17G, longitud 75-91 mm. Sujetador filtrante de 0.2 micras o filtro epidural de 0.2 micras y un adaptador Luer-lock para catéter con tapón de seguridad. - Catéter epidural, calibre 19G'),('603454257','Equipo para venoclisis con regulador de infusión. Consta de: Bayoneta con protector, conector Luer lock rosca con protector, filtro de aire, cámara de  goteo, regulador de flujo con  mecanismo para ajustar el flujo a  velocidades calibradas o'),('603454265','Equipo para venoclisis con regulador de infusión. Consta de: Bayoneta con protector, conector Luer lock rosca con protector, filtro de aire, cámara de goteo, regulador de flujo con  mecanismo para ajustar el flujo a velocidades calibradas o'),('603480013','ENSANCHADORES. De canales. No. 10 al 40.'),('603480021','ENSANCHADOR DE CANALES DEL No. 45 AL 80'),('603540014','ESPÁTULA. De Ayre modificada, de madera inastillable. Instrumento  alargado con dos diferentes extremos.\nDimensiones: Largo total 170.0 mm.\nAncho 7.0 mm. Grosor 1.5 mm. Ginecología. Extremo 1: forma  bifurcada en forma de hueso, donde la cresta A es'),('603600032','ESPEJO VAGINAL DESECHABLE MEDIANO VALVA SUPERIOR DE 10.7 CM VALVA INFERIOR DE 12.0 CM ORIFICIO CENTRAL DE 3.4 CM.'),('604090035','TRANSDUCTOR DE PRESIoN CON ACCESORIOS COMPLETOS. DESECHABLE.'),('604210153','Ferula de Arco de Erick'),('604310011','FRESA PARA UTILIZARSE EN LA PIEZA DE MANO DE ALTA VELOCIDAD DE CARBURO FORMA DE PERA Nø 330 ACERO INOXIDABLE ESTERILIZABLE PIEZA CON BLISTER Y ETIQUETADO CON EL Nø DE LA FRESA'),('604310409','FRESA PARA UTILIZARSE EN LA PIEZA DE MANO DE ALTA VELOCIDAD DE CARBURO FORMA REDONDA Nø 3 DE ACERO INOXIDABLE ESTERILIZABLE PIEZA CON BLISTER Y ETIQUETADO CON EL Nø DE LA FRESA'),('604310417','FRESA PARA UTILIZARSE EN LA PIEZA DE MANO DE ALTA VELOCIDAD DE CARBURO FORMA REDONDA Nø 5 DE ACERO INOXIDABLE ESTERILIZABLE PIEZA CON BLISTER Y ETIQUETADO CON EL Nø DE LA FRESA'),('604310466','FRESA PARA UTILIZARSE EN LA PIEZA DE MANO DE ALTA VELOCIDAD DE CARBURO FORMA CILINDRICA Nø 701 ACERO INOXIDABLE ESTERILIZABLE PIEZA CON BLISTER Y ETIQUETADO CON EL Nø DE LA FRESA'),('604310664','FRESA PARA UTILIZARSE EN LA PIEZA DE MANO DE ALTA VELOCIDAD DE CARBURO FORMA CILINDRICA Nø 557 ACERO INOXIDABLE ESTERILIZABLE PIEZA CON BLISTER Y ETIQUETADO CON EL Nø DE LA FRESA'),('604360057','GASA SECA CORTADA DE ALGODON LARGO 7.5 CM ANCHO 5 CM? ENVASE CON 200 PIEZAS'),('604360107','GASA SECA CORTADA DE ALGODoN LARGO 10CM. ANCHO 10CM. ENVASE CON 200 PIEZAS'),('604360206','GASA SIMPLE SECA. DE ALGODoN TIPO HOSPITAL. ROLLO TEJIDO PLANO (DOBLADA). LARGO 91 M ANCHO 91 CM.'),('604360552','GASA SECA CORTADA DE ALGODoN CON MARCA OPACA A LOS RAYOS X. LARGO 10 CM. ANCHO 10 CM. ENVASE CON 200 PIEZAS'),('604390039','Gorro de tela no tejida de polipropileno, desechable. Impermeable a la penetración de líquidos y fluidos; antiestática y resistente a la tensión. Cintas de ajuste en el extremo distal.\nTamaño estándar. Desechable'),('604390070','Gorro redondo con elástico ajustable al contorno de la cara, de tela no tejida de polipropileno, desechable. Impermeable a la penetración de líquidos y fluidos; antiestática y resistente a la tensión. Tamaño: Mediano'),('604560300','GUANTES.  Para cirugía. De látex natural, estériles y desechables. Tallas:  6 1/2'),('604560318','GUANTES. Para cirugía. De látex natural, estériles y desechables. Tallas: 7'),('604560334','GUANTES. Para cirugía. De látex natural, estériles y desechables. Tallas: 7 1/2'),('604560367','GUANTES. Para cirugía. De látex natural, estériles y desechables. Tallas: 8 1/2'),('604560383','GUANTE. PARA EXPLORACION; AMBIDIESTRO ESTERILES DE LATEX DESECHABLES TAMAÑO CHICO. ENVASE CON 100 PIEZAS'),('604560391','GUANTE PARA EXPLORACION; AMBIDIESTRO ESTERILES DE LATEX DESECHABLES TAMAÑO MEDIANO. ENVASE CON 100 PIEZAS.'),('604560409','GUANTE PARA EXPLORACION AMBIDIESTRO ESTERILES DE LATEX DESECHABLES. TAMAÑO GRANDE. ENVASE CON 100 PIEZAS'),('604610147','GUATA DE TELA NO TEJIDA DE ALGODoN O FIBRAS DERIVADAS DE CELULOSA Y RESINAS LONGITUD 5 MTS ANCHO 5 CM. ENVASE CON 24 PIEZAS'),('604610154','GUATA DE TELA NO TEJIDA DE ALGODoN O FIBRAS DERIVADAS DE CELULOSA Y RESINAS LONGITUD 5 MTS. ANCHO 10 CM. ENVASE CON 24 PIEZAS'),('604610162','GUATA DE TELA NO TEJIDA DE ALGODoN O FIBRAS DERIVADAS DE CELULOSA Y RESINAS LONGITUD 5 M ANCHO 15 CM. ENVASE CON 24 PIEZAS'),('604700112','ESPONJA HEMOSTaTICA DE GELATINA O COLaGENO.50 A 100 X 70 A 125 MM. ENVASE CON UNA PIEZA'),('604830091','HOJA PARA BISTURI DE ACERO INOXIDABLE; ESTERIL Y DESECHABLE. NUMERO. 10. EMPAQUE INDIVIDUAL.. ENVASE CON 100 PIEZAS.'),('604830125','HOJA DE BISTURI ACERO INOXIDABLE ESTERIL Y DESECHABLE EMPAQUE INDIVIDUAL NO. 11. ENVASE CON 100 PIEZAS'),('604830133','HOJA PARA BISTURI DE ACERO INOXIDABLE; ESTERIL Y DESECHABLE. NUMERO 20. EMPAQUE INDIVIDUAL. ENVASE CON 100 PIEZAS'),('604830141','HOJA PARA BISTURI DE ACERO INOXIDABLE; ESTERIL Y DESECHABLE. NUMERO 15. EMPAQUE INDIVIDUAL. ENVASE CON 100 PIEZAS.'),('604830158','HOJA PARA BISTURI DE ACERO INOXIDABLE; ESTERIL Y DESECHABLE. NUMERO 21. EMPAQUE INDIVIDUAL. ENVASE CON 100 PIEZAS'),('604830778','SIERRA MANUAL GIGLI. PIEZA'),('604910018','PAPELES. Indicador de contacto oclusal. En tiras, con pegamento en ambas caras.'),('605320175','EQUIPO PARA TRANSFUSION CON FILTRO SIN AGUJA'),('605430115','JALEA LUBRICANTE ASEPTICA;. ENVASE CON 135 G.'),('605500016','JERINGAS.  De plástico. Con pivote tipo luer lock, con aguja, estériles y desechables. Capacidad 10 ml, escala graduada en ml, divisiones de 1.0 y subdivisiones de 0.2. Con aguja de\nLongitud: 38 mm  Calibre:  20 G.'),('605500024','JERINGAS.  De plástico. Con pivote tipo luer lock, estériles y desechables.\nCapacidad 20 ml, escala graduada en ml, divisiones de 5.0 y subdivisiones de 1.0. Con aguja de Longitud: 38 mm Calibre: 20 G.'),('605500222','JERINGAS .DE PLASTICO; SIN AGUJA CON PIVOTE TIPO LUER LOCK ESTERILES Y DESECHABLES CAPACIDAD 3 ML. ESCALA GRADUADA EN ML. DIVISIONES DE 0.5 Y SUBDIVISIONES DE 0.1. ENVASE CON 100 PIEZAS.'),('605500354','JERINGAS. De plástico. Con pivote tipo luer lock, con aguja, estériles y desechables. Capacidad 10 ml, escala graduada en ml, divisiones de 1.0 y subdivisiones de 0.2. Con aguja de:\nLongitud: 32 mm Calibre: 20 G.'),('605500438','JERINGA DE PLASTICO SIN AGUJA CON PIVOTE TIPO LUER LOCK ESTERIL Y DESECHABLE. CAPACIDAD DE 5 ML ESCALA GRADUADA EN ML DIVISIONES DE 1.0 Y SUBDIVISIONES DE 0.2 ML. ENVASE CON 100 PIEZAS'),('605500453','JERINGAS. De plástico, sin aguja con pivote tipo luer lock, estériles y  Desechables. Capacidad: 20 ml  Escala graduada en ml: Divisiones de 5.0 y subdivisiones de 1.0.'),('605500685','JERINGA PARA EXTRAER SANGRE O INYECTAR SUSTANCIAS C/PIVOTE TIPO LUER LOCK DE POLIPROPILENO VOL. D/5ML. Y AGUJA CAL. 21 G Y 32 MM DE LONGITUD ESTERIL. ENVASE SON 100 PIEZAS'),('605501147','JERINGA.PARA TUBERCULINA;CON AGUJA; PLASTICO GRADO MEDICO CAPACIDAD 1 ML ESCALA GRADUADA EN ML CON DIVISIONES DE 0.05 ML Y SUBDIVISIONES DE 0.01 ML. CON AGUJA LONGITUD 16 MM CALIBRE 25 G. ESTERILES Y DESECHABLES. ENVASE CON 200 PIEZAS'),('605501279','JERINGA DE PLASTICO GRADO MEDICO CON PIVOTE TIPO LUER LOCK CAPACIDAD DE 3 ML ESCALA GRADUADA EN ML CON DIVISIONES DE 0.5 ML Y SUBDIVISIONES DE 0.1 ML CON AGUJA CALIBRE 22 G Y 32 MM DE LONGITUD ESTERIL Y DESECHABLES.'),('605502186','JERINGAS. Jeringa para insulina, de plástico grado médico; graduada de 0 a 100 unidades, con capacidad de 1 ml.\nCon aguja de acero inoxidable, longitud 13 mm, calibre 27 G. Estéril y Desechable.'),('605512227','JERINGAS. De plástico grado médico, para aspiración manual endouterina, reesterilizable, capacidad de 60 ml, con\nanillo de seguridad, émbolo en forma de abanico, extremo interno en forma cónica, con anillo de goma negro en su interior y'),('605930106','LOSETA PARA BATIR CEMENTO DE VIDRIO TAMAÑO 8 X 12 X 0.5 CM.'),('605960129','Líquido lubricante de silicon para jeringa de aspiración manual endouterina'),('605980010','LLAVES.  De 4 vías con marcas indicadoras del sentido en el que fluyen las soluciones y posición de cerrado, aditamento de cierre luer lock (móvil) en el ramal de la llave, que se conecta al tubo de la extensión. Tubo de extensión removible de plástico'),('605980036','LLAVES DE 3 VIAS CON TUBO DE EXTENSION.DE PLASTICO RIGIDO O EQUIVALENTE.CON TUBO DE EXTENSION DE CLORURO DE POLIVINILO DE 80 CM. DE LONGITUD.'),('606030013','MALLA DE POLIPROPILENO ANUDADO DE 25 A 35 CM X 35 CM'),('606210482','MASCARILLA DESECHABLE PARA ADMINISTRACIoN DE OXiGENO CON TUBO DE CONEXIoN DE 180 CM Y ADAPTADOR'),('606210524','CUBREBOCAS. De dos capas de tela no tejida, resistente a fluidos, antiestático,  hipoalergénico, con bandas o ajuste elástico a la cabeza. Desechable.'),('606220143','FORMOCRESOL PARA MOMIFICACION Y DESVITALIZACION DE LA PULPA DENTARIA DE BUCKLEY ENVACE CON 30 Ml.'),('606260024','Medias antiembólicas elásticas de compresión mediana, para miembros inferiores, hasta el muslo. Tallas:  Chica larga.'),('606260040','Medias antiembólicas elásticas de compresión mediana, para miembros inferiores, hasta el muslo. Tallas: Mediana larga.'),('606260065','Medias antiembólicas elásticas de compresión mediana, para miembros inferiores, hasta el muslo. Tallas: Grande larga.'),('606810034','PAÑAL DE FORMA ANATOMICA DESECHABLE PARA NIÑO MEDIDA : CHICO.'),('606810042','PAÑAL DE FORMA ANATOMICA DESECHABLE PARA NIÑO MEDIDA MEDIANO'),('606810059','PAÑAL DE FORMA ANATOMICA DESECHABLE PARA NIÑO MEDIDA GRANDE.'),('606810067','PAÑAL PREDOBLADO DESECHABLES PARA ADULTO'),('606830057','PAÑO PARA EXPRIMIR AMALGAMA DE ALGODoN FORMA CIRCULAR. ENVASE CON 100 PIEZAS.'),('606850584','PAPEL ELECTROCARDIOGRAFO (44 MM X 30 M) PARA CABEZA TERMICA TERMOSENSIBLE UN CANAL.'),('606970267','PASTA O GEL CONDUCTIVA.PARA ELECTROCARDIOGRAMA. ENVASE CON 120 ML'),('607010378','PERILLAS PARA ASPIRACION DE SECRECIONES DE HULE NO. 4.'),('607400025','PROTECTOR DE PIEL. TINTURA DE BENJUi AL 20%. ENVASE CON 1000 ML.'),('607490703','PASTA PARA PROFILAXIS DENTAL ABRASIVA CON ABRASIVOS BLANDOS. ENVASE CON 200 GR.'),('607530011','PUNTAS DE  GUTAPERCHA Para obturación de conductos radiculares. Números: 45 a 80 (de 5 en 5). '),('607530029','PUNTAS ABSORBENTES Para endodoncia. De papel, estériles. Números: 45 a 80 (de 5 en 5).'),('607530052','PUNTAS ABSORBENTES Para endodoncia. De papel, estériles. Números: 10 a 40 (de 5 en 5). '),('607530102','PUNTAS DE  GUTAPERCHA Para obturación de conductos radiculares. Números: 10 a 40 (de 5 en 5). '),('607710050','RASTRILLOS CON DIENTES DE BORDES ROMOS Y HOJA DE UN FILO DESECHABLES.'),('607970019','ALGODoN P/USO DENTAL DE 3.8 X 0.8 CM.ENVASE CON 500 ROLLOS.'),('608110060','HILO DENTAL SEDA DENTAL CON CERA'),('608150058','SELLADORES De fisuras y fosetas. Envase con 3 ml. de Bond base. Envase con 3 ml. de sellador de fisuras. 2 envases con 3 ml. Cada uno con Bond catalizador. Jeringa con 2 ml. de gel grabador. 2 portapinceles. 10 cánulas. 1 block de mezcla. 5 pozos de mezcl'),('608190021','SOLVENTES ACETONA PARA USOS DIVERSOS. ENVASE CON 1000 ML'),('608307070','SONDA PARA DRENAJE TORaCICO DE ELASToMERO DE SILICoN OPACA A LOS RAYOS X. LONGITUD 45 A 51 CM CALIBRE 36 Fr.'),('608307195','SONDA PARA DRENAJE TORaCICO DE ELASToMERO DE SILICoN OPACA A LOS RAYOS X. LONGITUD 45 A 51 CM CALIBRE 28 Fr.'),('608330098','Hialuronato de sodio, solución oftálmica.  Cada ml contiene:  Hialuronato de Sodio 10mg o 16mg.'),('608330171','Líquido pesado, purificado para uso intraocular, perfluoruro de Kalina'),('608330312','Dexpantenol al 5% (Gel)'),('608330320','Carbómero 0.2% (Gel)'),('608330338','Acido Hialurónico al 0.3%'),('608410221','SUTURA SINTETICA NO ABSORBIBLES MONOFILAMENTO DE POLIPROPILENO CON AGUJA. LONGITUD DE LA HEBRA 45 CM CALIBRE DE LA SUTURA 2-0 CARACTERISTICAS DE LA AGUJA 3/8 DE CIRCULO REVERSO CORTANTE (24-26 MM). ENVASE CON 12 PIEZAS.'),('608410445','SUTURA. SINTETICA NO ABSORBIBLE. MONOFILAMENTO DE NYLON; CON AGUJA. LONG. DE LA HEBRA 45 CM. CAL. DE LA SUTURA. 5-0. CARACTERISTICAS DE LA AGUJA. CORTANTE (12-13 MM.). ENVASE CON 12 PIEZAS.'),('608410460','SUTURA. SINTETICA NO ABSORBIBLE. MONOFILAMENTO DE NYLON; CON AGUJA. LONG. DE LA HEBRA 45 CM. CAL. DE LA SUTURA. 4-0. CARACTERISTICAS DE LA AGUJA. 3/8 CIRCULO; REVERSO CORTANTE (12-13 MM.). ENVASE CON 12 PIEZAS'),('608410478','SUTURA SINETICA NO ABSORBIBLE. MONOFILAMENTO DE NYLON; CON AGUJA. LONG. DE LA HEBRA 45 CM. CAL. DE LA SUTURA 3-0. CARACTERISTICAS DE LA AGUJA. 3/8 CIRCULO; CORTANTE (19-26 MM.) ENVASE CON 12 PIEZAS.'),('608410486','SUTURA SINTETICA NO ABSORBIBLE. MONOFILAMENTO DE NYLON;CON AGUJA. LONG. DE LA HEBRA 45 CM. CAL. DE LA SUTURA 2-0. CARACTERISTICAS DE LA AGUJA. 3/8 CIRCULO; CORTANTE(19-26 MM.) ENVASE CON 12 PIEZAS.'),('608410551','SUTURA CATGUT CROMICO.CON AGUJA.LONG. DE LA HEBRA 68 A 75 CM. CAL. DE LA SUTURA. 2-0. CARACTERISTICA DE LA AGUJA. 1/2 CIRCULO; AHUSADA(35-37 MM.) ENVASE CON 12 PIEZAS'),('608410569','SUTURA CATGUT CROMICO.CON AGUJA. LONG. DE LA HEBRA DE 68 A 75 CM. CAL.DE LA SUTURA. 1. CARACTERISTICA DE LA AGUJA. 1/2 CIRCULO; AHUSADA 35-37 MM. ENVASE CON 12 PIEZAS.'),('608410585','SUTURA CARGUT CROMICO LONGITUD DE LA HEBRA 30 CM CALIBRE DE LA SUTURA 4-0 CARACTERISTICAS DE LA AGUJA 1/2 CIRCULO DOBLE ARMADOREVERSO CORTANTE (12-13 MM) ENVASE CON 12 PIEZAS.'),('608410619','SUTURA SEDA NEGRA; TRENZADA;CON AGUJA. LONG. DE LA HEBRA 75 CM. CAL DE LA SUTURA 3-0. CARACTERISTICAS DE LA AGUJA. 1/2 CIRCULO; AHUSADA (25-26 MM) ENVASE CON 12 PIEZAS.'),('608410627','SUTURA SEDA NEGRA TRENZADA CON AGUJA LONGITUD DE LA HEBRA 75 CM CALIBRE DE LA SUTURA 2-0 CARACTERISTICAS DE LA AGUJA 1/2 CIRCULO AHUSADA (25-26 MM). ENVASE CON 12 PIEZAS.'),('608410635','SUTURA SINTETICA NO ABSORBIBLE MONOFILAMENTO DE POLIPROPILENO CON AGUJA. LONGITUD DE LA HEBRA 90 CM CALIBRE DE LA SUTURA 3-0 CARACTERISTICAS DE LA AGUJA 1/2 CIRCULO PUNTA AHUSADA (35-37 MM) ENVASE CON 12 PIEZAS.'),('608410734','SUTURA SEDA NEGRA TRENZADA;SIN AGUJA. LONG DE LAS HEBRA 75 CM.; CAL. DE LA SUTURA.3-0.SOBRE CON 7 A 12 HEBRAS. ENVASE CON 12 PIEZAS.'),('608410742','SUTURA SEDA NEGRA TRENZADA;SIN AGUJA. LONG. DE LA HEBRA 75 CM.; CAL. DE LA SUTURA 2-0.SOBRE CON 7 A 12 HEBRAS. ENVASE CON 12 PIEZAS'),('608410833','SUTURA SINTETICA ABSORBIBLE POLIMERO DE ACIDO GLICOLICO TRENZADO CON AGUJA. LONGITUD DE LA HEBRA 67-70 CM CALIBRE DE LA SUTURA 4-0 CARACTERISTICAS DE LA AGUJA 1/2 CIRCULO AUSADA (25-26 MM). ENVASE CON 12 PIEZAS'),('608410858','SUTURA SINTETICA ABSORBIBLE POLIMERO DE ACIDO GLICOLICO TRENZADO CON AGUJA. LONGITUD DE LA HEBRA 67-70 CM CALIBRE DE LA SUTURA 3-0 CARACTERISTICAS DE LA AGUJA 1/2 CIRCULO AHUSADA (25-26 MM).ENVASE CON 12 PIEZAS.'),('608410866','SUTURA SINTETICA ABSORBIBLE POLIMERO DE ACIDO GLICOLICO TRENZADO.CON AGUJA. LONG. DE LA HEBRA 67 A 70 CM. CAL. DE LA SUTURA 2-0. CARACTERISTICAS DE LA AGUJA. 1/2 CIRCULO; AHUSADA(25-26 MM.). ENVASE CON 12 PIEZAS.'),('608410882','SUTURAS Sintéticas absorbibles, polímero de ácido glicólico, trenzado, con aguja. Longitud  de la hebra: 67-70 cm Calibre de la sutura: 1 Características de la aguja: 1/2 círculo ahusada (35-37 mm). '),('608410890','SUTURA SINTETICA ABSORBIBLE POLIMERO DE ACIDO GLICOLICO TRENZADO.CON AGUJA. LONG. DE LA HEBRA 67 A 70 CM CAL. DE LA SUTURA 1-0. CARACTERISTICAS DE LA AGUJA.1/2 CIRCULO; AHUSADA(35-37 MM.). ENVASE CON 12 PIEZAS.'),('608411393','SUTURA.CATGUT SIMPLE CON AGUJA.LONGITUD DE LA HEBRA 68 A 75 CM. CALIBRE DE LA SUTURA 2-0.CARACTERISTICAS DE LA AGUJA 1/2 CIRCULO AHUSADA (25-27 MM) ENVASE CON 12 PIEZAS.'),('608412623','SUTURA.CATGUT CROMICO;CON AGUJA.LONG. DE LA HEBRA 68 A 75 CM.;CAL.DE LA SUTURA 1-0.CARACTERISTICAS DE LA AGUJA.1/2 CIRCULO;AHUSADA (35-37 MM.). ENVASE CON 12 PIEZAS.'),('608414264','SUTURA.CATGUT SIMPLE;CON AGUJA.LONG. DE LA HEBRA 68 A 75 CM.;CAL. DE LA SUTURA.3-0.CARACTERISTICAS DE LA AGUJA. 1/2 CIRCULO;AHUSADA (25-27 MM.). ENVASE CON 12 PIEZAS.'),('608414371','SUTURA CATGUT CROMICO CON AGUJA LONGITUD DE LA HEBRA 68-75 CM CALIBRE DE LA SUTURA 2-0 CARACTERISTICAS DE LA AGUJA 1/2 CIRCULO AHUSADA (25-27 MM). ENVASE CON 12 PIEZAS.'),('608490207','TALCO PARA PACIENTES. COMPUESTO DE SILICATO DE MAGNESIO HIDRATADO Y SILICATO DE ALUMINIO CON PERFUME. ENVASE TIPO SALERO CON 100 GR.'),('608590097','TAPONES PARA SONDA FOLEY DE PLASTICO DESECHABLES. PIEZA'),('608590501','TAPONES LUER LOCK PARA CATeTER DE DIaLISIS PERITONEAL AMBULATORIA DE PLaSTICO. TIPO: TENCKHOFF. PIEZA.'),('608590519','TAPONES LUER LOCK PARA CATeTER DE HICKMAN PARA HEPARINIZACIoN. ESTeRIL Y DESECHABLE. PIEZA.'),('608690152','TELA ADHESIVA.DE ACETATO;CON ADHESIVO EN UNA DE SUS CARAS.LONG.10 MT.;ANCHO:2.50 CM. ENVASE CON 12 PIEZAS.'),('608690202','TELA ADHESIVA.DE ACETATO;CON ADHESIVO EN UNA DE SUS CARAS.LONG.10 MT.;ANCHO:5.00 CM. ENVASE CON 6 PIEZAS.'),('608690251','TELA ADHESIVA.DE ACETATO;CON ADHESIVO EN UNA DE SUS CARAS.LONG.10 MT.;ANCHO 7.50 CM. ENVASE CON 4 PIEZAS.'),('608890158','TIRA DE CELULOIDE P/ CONFORMAR RESTAURACIONES DE RESINA ANCHO 8 A 10 MM. CALIBRE FINO. ENVASE CON 50 PIEZAS.'),('608890216','Tiras de Fluoresceína, para uso oftalmológico'),('608890254.1','Solución salina balanceada para irrigación   Cada 100ml contiene:  Cloruro de Sodio 0.640g, Cloruro de potasio 0.075g, cloruro de calcio 0.048g, cloruro de Magnesio 0.030g, acetato de sodio 0.170g, agua inyectable cbp 100ml'),('608940052','TOALLA PARA GINECO-OBSTETRICIA.RECTANGULARES CONSTITUIDAS POR CUATRO CAPAS DE MATERIAL ABSORBENTE.DESECHABLES. ENVASE CON 100 PIEZAS.'),('609040100','ALGODON TORUNDA. ENVASE CON 500 GR.'),('609080015','TUBOS.  Tubo para canalización.\nDe látex natural, radiopaco. Longitud 45 cm. Diámetro: 7.94 mm (5/16\").'),('609080114','TUBOS. Tubo para canalización.\nDe látex natural, radiopaco. Longitud 45 cm. Diámetro: 12.70 mm (1/2\").'),('609080122','TUBOS. Tubo para canalización.\nDe látex natural, radiopaco. Longitud 45 cm. Diámetro: 19.05 mm (3/4\").'),('609080130','TUBOS. Tubo para canalización.\nDe látex natural, radiopaco. Longitud 45 cm. Diámetro: 25.40 mm (1\").'),('609080890','TUBO PARA TORNIQUETE.DE LaTEX COLOR aMBAR CON ESPESOR DE LA PARED DE 1.13 A 1.37 MM'),('609090956','Tubo flexible de polivinilo diametro de 0.8mm de 11.1mm tipo Tygon'),('609100011','EYECTORES Para saliva, de plástico, desechable. '),('609530282','VENDA DE GOMA (SMARCH). DE HULE NATURAL GRADO MeDICO.LONGITUD: 2.7 M ANCHO: 8 CM. PIEZA.'),('609530456','VENDA ENYESADA DE GASA DE ALGODON RECUBIERTAS DE UNA CAPA UNIFORME DE YESO DE GRADO MEDICO. LONGITUD 2.75 M ANCHO 5 CM. ENVASE CON 12 PIEZAS.'),('609530555','VENDA.ENYESADA;DE GASA DE ALGODON;RECUBIERTA DE UNA CAPA UNIFORME DE YESO;GRADO MEDICO.LONG.2.75 MTS.; ANCHO:10 CM. ENVASE CON 12 PIEZAS.'),('609530571','VENDA ENYESADA DE GASA DE ALGODON RECUBIERTAS DE UNA CAPA UNIFORME DE YESO DE GRADO MEDICO. LONGITUD 2.75 M ANCHO 15 CM. ENVASE CON 12 PIEZAS.'),('609532825','VENDA. ELASTICA DE TEJIDO PLANO DE ALGODON; CON FIBRAS SINTETICAS. LONGUITUD 5 MT.; ANCHO 30 CM. ENVASE CON UNA PIEZA'),('609532858','VENDA ELASTICA (TEJIDO PLANO); DE ALGODoN CON FIBRAS SINTETICAS. LONGITUD 5 MTS; ANCHO : 5 CMS ENVASE CON 12 PIEZAS.'),('609532866','VENDA ELASTICA TEJIDO PLANO DE ALGODON CON FIBRAS SINTETICAS.LONGITUD 5 M ANCHO 10 CM . ENVASE CON 12 PIEZAS.'),('609532874','VENDA ELASTICA TEJIDO PLANO DE ALGODON CON FIBRAS SINTETICAS.LONGITUD 5 M ANCHO 15 CM . ENVASE CON 12 PIEZAS.'),('609990136','KIT QUE CONSTA DE  1 CANULA CPAP (PRESIÓN POSITIVA  CONTINUA DE LA VÍA AÉREA)  DEL NO. 0, CON DOS CODOS, 1 LINEA PARA MONITOREO, 1 ADAPTADOR RIGIDO PARA HUMIDIFICADOR DE 22 MM, 1 GORRO DE LANA Y 2 PZAS DE CINTA VELCRO DE FIJACIÓN DE 6 PULGADAS.'),('609990138','KIT QUE CONSTA DE  1 CANULA CPAP (PRESIÓN POSITIVA CONTINUA DE LA VÍA AÉREA) DEL NO. 2, CON DOS CODOS, 1 LINEA PARA MONITOREO, 1 ADAPTADOR RIGIDO PARA HUMIDIFICADOR DE 22 MM, 1 GORRO DE LANA Y 2 PZAS DE CINTA VELCRO DE FIJACIÓN DE 6 PULGADAS.'),('609990140','KIT QUE CONSTA DE  1 CANULA CPAP (PRESIÓN POSITIVA  CONTINUA DE LA VÍA AÉREA)  DEL NO. 4, CON DOS CODOS, 1 LINEA PARA MONITOREO, 1 ADAPTADOR RIGIDO PARA HUMIDIFICADOR DE 22 MM, 1 GORRO DE LANA Y 2 PZAS DE CINTA VELCRO DE FIJACIÓN DE 6 PULGADAS.'),('707070504','PLACA DENTAL OCLUSAL DE 5.7 X 7.6 CM. SENSIBLE AL AZUL'),('707070587','PELICULAS RADIOGRAFICAS DENTALES SENSIBLES AL AZUL MEDIDAS: INFANTIL SENCILLA PERIAPICAL 2.2 X 3.5 CM.'),('802650523','Cubreobjetos.  De vidrio No. 1 con un espesor de 0.13 a 0.16mm con dimensiones 24 x 50mm '),('805740164','LANCETAS. Metálicas, estériles, desechables, con envoltura individual: punta de 3 mm, de longitud. Para punción que mide el tiempo de sangrado.'),('807290010','Portaobjetos de vidrio, rectangulares, de grosor uniforme, de 75 x 25 x 0.8 a 1.1mm, lisos.'),('807290051','Portaobjetos de vidrio, rectangulares, de grosor uniforme, de 75 x 25 x 0.8 a 1.1mm, con esquinas y un extremo esmerilado'),('808308084','Alcohol etílico absoluto.  Grado biología molecular.'),('1302580624','COLLARINES TIPO: FILADELFIA. Mediano. Evita movimientos de flexión dorsal, ventral y laterales. Elaborado en espuma plástica, bivalvo, con orificios para ventilación en la parte dorsal y en la ventral, abertura traqueal con marco rígido y alma de'),('1302580632','COLLARIN TIPO FILADELFIA GRANDE'),('5373830081','Espejo dental, rosca sencilla, plano, sin aumento.Nº 5.');

UPDATE productos dest, (SELECT * FROM catalogo_descripcion) src 
  SET dest.des_pro = src.descripcion where dest.cla_pro = src.clave ;
UPDATE productos SET des_pro = REPLACE(des_pro,"\n","");
UPDATE productos SET des_pro='IBUPROFENO SUSPENSION pediatrica  cada ml contiene ibuprofeno 40 mg.' WHERE cla_pro='5944';

DROP VIEW IF EXISTS vistamodvig;
DROP TABLE IF EXISTS `catalogo_descripcion`;
DROP TABLE IF EXISTS `consultorios`;
DROP TABLE IF EXISTS `mod_vig`;
DROP TABLE IF EXISTS `tb_cat2015`;
DROP TABLE IF EXISTS `unidades_censos`;
DROP TABLE IF EXISTS `transferencias`;
DROP TABLE IF EXISTS `catalogoabril2016`;
DROP TABLE IF EXISTS `catalogoabril2016nuevo`;
DROP TABLE IF EXISTS `productosnivelabril`;
DROP TABLE IF EXISTS `productonivel2016`;
DROP TABLE IF EXISTS `tb_catprecios`;
DROP TABLE IF EXISTS `catalogoproductos`;
DROP TABLE IF EXISTS `niv_serv`;

ALTER TABLE `usuarios`
MODIFY COLUMN `ape_pat`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' AFTER `email`,
MODIFY COLUMN `ape_mat`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' AFTER `ape_pat`,
ADD COLUMN `nombre_completo`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `ape_mat`;

UPDATE `usuarios`
SET `nombre_completo` = CONCAT(
	nombre,
	" ",
	ape_pat,
	" ",
	ape_mat
);

UPDATE `kardex`
SET 
 `id_rec` = '1'
WHERE
	(`id_rec` = '0');
	
-- Corrección de los CBs faltantes

INSERT INTO `tb_codigob` (
	`F_Cb`,
	`F_Clave`,
	`F_Lote`,
	`F_Cadu`
) SELECT
	'-',
	dp.cla_pro,
	dp.lot_pro,
	dp.cad_pro
FROM
	tb_codigob AS cb
RIGHT JOIN detalle_productos AS dp ON cb.F_Clave = dp.cla_pro
AND cb.F_Lote = dp.lot_pro
AND cb.F_Cadu = dp.cad_pro
WHERE
	cb.F_Clave IS NULL
AND dp.lot_pro <> '-';

-- Elimina inventarios creados erroneamente por el antiguo sistema de captura.
DELETE target
FROM
	inventario AS target
JOIN (
	SELECT
		i.id_inv AS id
	FROM
		inventario AS i
	INNER JOIN detalle_productos AS dp ON i.det_pro = dp.det_pro
	WHERE
		dp.lot_pro = '-'
	AND dp.cad_pro = '2020-01-01'
) AS source ON target.id_inv = source.id;

INSERT INTO `tipo_receta` (`id_tip`, `des_tip`) VALUES ('3', 'RM');
DROP TABLE IF EXISTS `existencias_trg`;
DROP TABLE IF EXISTS `solicitado_vs_surtido_trg`;
DROP TABLE IF EXISTS `status_proceso`;

ALTER TABLE `receta`
ADD INDEX `transito_baja` (`transito`, `baja`) USING BTREE ;

ALTER TABLE `medicos`
ADD INDEX `cedula` (`cedulapro`) USING BTREE ,
ADD INDEX `paterno` (`ape_pat`) ;

INSERT INTO `tipo_receta` (`id_tip`, `des_tip`) VALUES ('3', 'RM');
