ALTER TABLE receta CHANGE web notificahl7 INT;
ALTER TABLE	receta ADD hl7_obser longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE	pacientes ADD idpac_hl7 VARCHAR(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '-';
/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 50612
Source Host           : localhost:3306
Source Database       : scr_ceaps

Target Server Type    : MYSQL
Target Server Version : 50612
File Encoding         : 65001

Date: 2017-05-26 09:03:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `mensaje_hl7`
-- ----------------------------
DROP TABLE IF EXISTS `mensaje_hl7`;
CREATE TABLE `mensaje_hl7` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`mensaje`  longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`status`  int(11) NOT NULL ,
`fecha`  varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'NOW()' ,
`idrec`  int(11) NULL DEFAULT NULL ,
`nummsg`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of mensaje_hl7
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `receta_hl7_log`
-- ----------------------------
DROP TABLE IF EXISTS `receta_hl7_log`;
CREATE TABLE `receta_hl7_log` (
`tipo_mensaje`  varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`hl7_mensaje`  longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`fecha`  datetime NULL DEFAULT NULL ,
`estatus`  int(11) NULL DEFAULT NULL ,
`proceso`  varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`id`  bigint(20) NOT NULL AUTO_INCREMENT ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of receta_hl7_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Auto increment value for `mensaje_hl7`
-- ----------------------------
ALTER TABLE `mensaje_hl7` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `receta_hl7_log`
-- ----------------------------
ALTER TABLE `receta_hl7_log` AUTO_INCREMENT=1;
