<%-- 
    Document   : soporte
    Created on : 27-nov-2015, 9:34:38
    Author     : Sebastian
--%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    HttpSession sesion = request.getSession();

    String id_usu = "";
    try {
        id_usu = (String) sesion.getAttribute("id_usu");
    } catch (Exception e) {
        Logger.getLogger("usuario.jsp").log(Level.SEVERE, e.getMessage(), e);
    }

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="../css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../css/topPadding.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/codeJs.js" type="text/javascript"></script>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <title>LODIMED</title>
    </head>
    <body>
        <%@include file="../jspf/mainMenu.jspf"%>
        <div class="container">

            <!-- Main component for a success marketing message or call to action -->
            <br>
            <div class="jumbotron text-center">
                <div class="page-header">
                    <h1>¿Tiene alguna pregunta?</h1>
                </div>

                <p class="text-justify">Aquí podra encontrar información que le servira de ayuda cuando tenga dificultades con el sistema. Por favor, revise los siguientes apartados de ayuda, si aún después persiste el problema no dude en contactarnos.
                </p>


                <div class="row">
                    <a data-toggle="modal" href="#modalSistema">
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">                     
                                <div class="caption">
                                    <h2>Estado del Sistema</h2>
                                    <i class="fa fa-desktop fa-5x"></i>
                                    <p><br/>Verifique si ha cargado y configurado su sistema correctamente.</p>
                                </div>
                            </div>
                        </div>
                    </a>
                    <!--a href="../Manual SIALSS 2016.pdf">
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">                     
                                <div class="caption">
                                    <h2>Manual de usuario</h2>
                                    <i class="fa fa-book fa-5x"></i>
                                    <p><br/>Consulte pasos de configuración o soluciones a los errores más comunes.</p>
                                </div>
                            </div>
                        </div>
                    </a-->
                    <a data-toggle="modal" href="#modalSoporte">
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">                     
                                <div class="caption">
                                    <h2>Reporte de fallos</h2>
                                    <i class="fa fa-bug fa-5x"></i>
                                    <p><br/>¿Todo ha fallado?, contáctenos y reporte su problema.</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>




            <!--
            Envío de Correo
            -->
            <div class="modal fade" id="modalSoporte" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title">Reporte de fallos</h3>
                        </div>
                        <div class="modal-body">
                            <form name="form_com" method="post" id="form_com">
                                Nombre: <input type="text" class="form-control" autofocus placeholder="Ingrese su Nombre" data-behavior="only-alphanum" name="txtf_nom" id="txtf_nom">
                                Teléfono: <input type="text" class="form-control" autofocus placeholder="Ingrese su télefono" data-behavior="only-number" name="txtf_tel" id="txtf_tel">
                                Cuenta de Correo: <input type="text" class="form-control"  placeholder="Ingrese su Cuenta de Correo" data-behavior="only-alphanum" name="txtf_cor" id="txtf_cor" onblur="validarEmail(this.form.txtf_cor.value);" >
                                Deje su Comentario: <textarea name="txta_com" cols="10" rows="5" class="form-control" data-behavior="only-alphanum" id="txta_com"></textarea>
                                <div class="modal-footer">
                                    <div id="contenidoAjax">
                                        <p></p>
                                    </div>
                                    <input type="submit" class="btn btn-success" value="Enviar" id="btn_com" onClick="return verificaCom(document.forms.form_com);">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                </div>

                            </form>
                        </div>

                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <!--
            Fin envío de correo
            -->

            <div class="modal fade" id="modalManual" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title">Manual de usuario</h3>
                        </div>
                        <div class="modal-body text-center">
                            <i class="fa fa-cog fa-5x fa-spin"></i>
                            <h4>Oops! Nos has descubierto.</h4>
                            <p>Esta página aún se encuentra en construción, en poco tiempo estará disponible.</p>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>

                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>

            <div class="modal fade" id="modalSistema" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title">Estado del sistema</h3>
                        </div>
                        <div class="modal-body text-center">
                            <ul class="list-group">
                                <li class="list-group-item" id="sistema"><h4>Reinicio incial del sistema <i id="sistema_icon" class="fa"></i></h4></li>
                                <li class="list-group-item" id="unidad"><h4>Asignación de la unidad <i id="unidad_icon" class="fa"></i></h4></li>
                                <li class="list-group-item" id="servicio"><h4>Carga de los servicios de la unidad <i id="servicio_icon" class="fa"></i></h4></li>
                                <li class="list-group-item" id="cdm"><h4>Carga del CDM de la unidad <i id="cdm_icon" class="fa"></i></h4></li>
                                <li class="list-group-item" id="usuario"><h4>Dar de alta mínimo un usuario <i id="usuario_icon" class="fa"></i></h4></li>
                            </ul>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>

                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>            

        </div> <!-- /container -->
        <!-- Funciones con Ajax -->
        <script src="../js/jquery-2.1.4.js" type="text/javascript"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/jquery.alphanum.js" type="text/javascript"></script>
        <script>
                                        $(document).ready(function () {
                                            $('#form_login').submit(function () {
                                                alert("Introduzca credenciales válidas");
                                                return false;
                                            });
                                            $('#form_com').submit(function () {
                                                return false;
                                            });
                                            $("#btn_com").click(function () {

                                                var nom = $('#txtf_nom').val();
                                                var cor = $('#txtf_cor').val();
                                                var com = $('#txta_com').val();
                                                var tel = $('#txtf_tel').val();
                                                if (nom === '' || cor === '' || com === '' || tel === '') {
                                                    return false;
                                                }
                                                else {
                                                    var $contenidoAjax = $('div#contenidoAjax').html('<p><img src="../imagenes/ajax-loader.gif" /></p>');
                                                    var dir = '../servletCorreo';
                                                    var userData = $.param({uas: navigator.userAgent});
                                                    $.ajax({
                                                        url: dir,
                                                        type: "POST",
                                                        data: {txtf_nom: nom, txtf_cor: cor, txtf_tel: tel, txta_com: com, esSoporte: true, navegador: userData, id:<%=id_usu%>},
                                                        success: function (data) {
                                                            $contenidoAjax.html(data);
                                                            alert("Sus datos han sido Enviados");
                                                            location.reload();
                                                        },
                                                        error: function (jqXHR, textStatus, errorThrown) {
                                                            alert(jqXHR.responseText);
                                                        }
                                                    });
                                                }
                                            });
                                            function checkEstadoSistema() {

                                                $.ajax({
                                                    url: '../estadoSistema',
                                                    type: 'POST',
                                                    dataType: 'json',
                                                    success: function (data) {
                                                        if (data.sistema) {
                                                            $("#sistema_icon").addClass("fa-check-square-o text-success");
                                                        } else {
                                                            $("#sistema").addClass("list-group-item-danger");
                                                            $("#sistema_icon").addClass("fa-exclamation-triangle");
                                                        }

                                                        if (data.unidad) {
                                                            $("#unidad_icon").addClass("fa-check-square-o text-success");
                                                        } else {
                                                            $("#unidad").addClass("list-group-item-danger");
                                                            $("#unidad_icon").addClass("fa-exclamation-triangle");
                                                        }

                                                        if (data.servicio) {
                                                            $("#servicio_icon").addClass("fa-check-square-o text-success");
                                                        } else {
                                                            $("#servicio").addClass("list-group-item-danger");
                                                            $("#servicio_icon").addClass("fa-exclamation-triangle");
                                                        }

                                                        if (data.cdm) {
                                                            $("#cdm_icon").addClass("fa-check-square-o text-success");
                                                        } else {
                                                            $("#cdm").addClass("list-group-item-danger");
                                                            $("#cdm_icon").addClass("fa-exclamation-triangle");
                                                        }

                                                        if (data.usuario) {
                                                            $("#usuario_icon").addClass("fa-check-square-o text-success");
                                                        } else {
                                                            $("#usuario").addClass("list-group-item-danger");
                                                            $("#usuario_icon").addClass("fa-exclamation-triangle");
                                                        }

                                                    },
                                                    error: function (data) {
                                                        alert('Error' + data);
                                                        console.log(data);
                                                    }
                                                });
                                            }
                                            checkEstadoSistema();
                                        });

                                        $("[data-behavior~=only-alphanum]").alphanum({
                                            allowOtherCharSets: true,
                                            disallow: "'",
                                            allow: ".@_"
                                        });

                                        $("[data-behavior~=only-number]").alphanum({
                                            allowLatin: false,
                                            allowOtherCharSets: false,
                                            disallow: "'",
                                            allow: "+*#"
                                        });


        </script>
    </body>
</html>
