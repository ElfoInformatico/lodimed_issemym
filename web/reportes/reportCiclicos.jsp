<%-- 
    Document   : reportCiclicos
    Created on : 18/05/2016, 11:11:14 PM
    Author     : juan
--%>

<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    HttpSession sesion = request.getSession();

    String id_usu = "";
    int rol = 0;
    try {
        id_usu = (String) sesion.getAttribute("id_usu");
        rol = (int) sesion.getAttribute("rol");

    } catch (Exception e) {
        Logger.getLogger("reportCiclicos.jsp").log(Level.SEVERE, e.getMessage(), e);
    }

    if (id_usu == null) {
        response.sendRedirect("../../index.jsp");
        return;
    }

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reporte de inventarios Ciclicos</title>
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="../css/cupertino/jquery-ui-1.10.3.custom.css" rel="stylesheet">
    </head>
    <body>
        <%@include file="../jspf/mainMenu.jspf"%>
        <div class="container text-center" style="padding-top: 50px">
            <h1>Reporte de inventarios cíclicos</h1>
        </div>
        <div class="container" style="padding-top: 50px; background-color:#FFFFFF; padding:10px; margin:auto" >
            <input type="hidden" value="<%=rol%>" id="idRol">
            <div class="panel panel-success" id="tableAuditar" >
                <!-- Default panel contents -->
                <div class="panel-heading"  >
                    <div class="row">
                        <h4 class="col-lg-1 col-md-1 col-sm-1">Cíclicos</h4>

                    </div>
                </div>
                <div class="panel-body"  >
                    <div id="container">

                        <div id="dynamic"></div>

                    </div>
                </div>

            </div>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"></h4>
                        </div>
                        <div class="modal-body">
                            <div class="text-center" id="imagenCarga">
                                <img src="../imagenes/ajax-loader-1.gif" alt="" />
                            </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            var context = "${pageContext.servletContext.contextPath}";
        </script>
        <script src="../js/jquery-2.1.4.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery-ui.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/dataTables.tableTools.js"></script>
        <script src="../js/inventarios/reporteCiclicos.js"></script>
    </body>
</html>
