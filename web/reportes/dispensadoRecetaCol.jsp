<%@page contentType="text/html; charset=UTF-8" language="java" %>
<%
    HttpSession sesion = request.getSession();
    String id_usu = "";
    id_usu = (String) sesion.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }
%>


<!DOCTYPE HTML">
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Sistema de Captura de Receta</title>

        <link href="../css/bootstrap.css" rel="stylesheet" media="screen"/>
        <link href="../css/topPadding.css" rel="stylesheet"/>
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/cupertino/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css"/>
        <link href="../css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>

    </head>

    <body>
        <%@include file="../jspf/mainMenu.jspf"%>
        <br/>
        <br/>
        <div class="container">
            <div class="row">
                <div class="col-sm-3"><img src="../imagenes/isem.png" width="100"/></div>
                <div class="col-sm-6"><h2>Dispensado por Clave Receta Colectiva</h2></div>
                <div class="col-sm-3"><img src="../imagenes/gobierno.png" width="100"/></div>
            </div>
            <div class="row">
                <br>
                <label class="col-sm-3">Rango de fechas del:&nbsp;&nbsp;
                    <input class="form-control" name="txtf_caduc" type="text" id="fechaInicial" data-date-format="yyyy/mm/dd" size="10" readonly title="aaaa/mm/dd"/>
                </label>                  &nbsp;&nbsp;&nbsp;&nbsp;
                <label class="col-sm-3"> al:&nbsp;&nbsp;
                    <input class="form-control" name="txtf_caduci" type="text" id="fechaFinal" data-date-format="yyyy/mm/dd" size="10" readonly title="aaaa/mm/dd"/>
                </label>
                <input class="btn-success btn col-sm-2" name="submit" value="Buscar" id="consultar" onclick="consultar()"/>
            </div>
            <div class="row">
                <br>
                <button class="btn btn-success" onclick="exportarExcel()">Descargar</button>
            </div>
            <div class="row">
                <br>
                <table id="tabla" class="table table-condensed table-striped table-bordered">
                    <thead>
                        <tr>
                            <td>Clave</td>
                            <td>Descripci&oacute;n</td>
                            <td>Cantidad</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <br>
                <div class="co-sm-1">Total de cajas dispensadas:</div>
                <div class="co-sm-1"><input id="total" readonly></div>
            </div>
            <br>
            <br>
        </div>

        <script src="../js/jquery-2.1.4.js" type="text/javascript"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery-ui.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script>
                    var tableReporte;
                    $(document).ready(function () {
                        $("#fechaInicial").datepicker({
                            dateFormat: 'dd/mm/yy',
                            onClose: function (selectedDate) {
                                $("#fechaFinal").datepicker("option", "minDate", selectedDate);
                            }
                        });
                        $("#fechaFinal").datepicker({
                            dateFormat: 'dd/mm/yy',
                            onClose: function (selectedDate) {
                                $("#fechaInicial").datepicker("option", "maxDate", selectedDate);
                            }
                        });
                        tableReporte = $('#tabla').dataTable({"bDestroy": true,
                            "columnDefs": [
                                {"width": "20%", "targets": 0},
                                {"width": "20%", "targets": 2}
                            ]});

                    });

                    function exportarExcel() {
                        var fechaInicial, fechaFinal;

                        fechaInicial = $('#fechaInicial').val();
                        fechaFinal = $('#fechaFinal').val();
                        window.open('../ReporteDispRec?accion=generarExcelReceta&fechaInicial=' + fechaInicial + "&fechaFinal=" + fechaFinal + "&tipo=2");
                    }

                    function consultar() {
                        var fechaInicial, fechaFinal;

                        fechaInicial = $('#fechaInicial').val();
                        fechaFinal = $('#fechaFinal').val();

                        var myKeyVals = {accion: 'receta', fechaInicial: fechaInicial, fechaFinal: fechaFinal, tipo: '2'};

                        $.ajax({
                            type: "POST",
                            url: "../ReporteDispRec",
                            data: myKeyVals,
                            dataType: "json",
                            success: function (data) {
                                tableReporte.fnDestroy();
                                var table = $("#tabla");
                                $("#tabla > tbody").html("");
                                var total = 0;
                                $.each(data, function (idx, elem) {
                                    table.append("<tr><td>" + elem.clave + "</td><td>" + elem.descripcion + "</td>   <td>" + elem.cantidad + "</td></tr>");
                                    total += parseInt(elem.cantidad);
                                    console.log(parseInt(elem.cantidad));
                                });

                                $('#total').val(total);
                                tableReporte = table.dataTable({
                                    "bDestroy": true,
                                    "columnDefs": [
                                        {"width": "20%", "targets": 0},
                                        {"width": "20%", "targets": 2}
                                    ]}
                                );
                            }
                        });
                    }
        </script>
    </body>
</html>
