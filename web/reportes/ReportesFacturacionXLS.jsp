<%-- 
    Document   : xlsReportes
    Created on : 28/04/2018, 12:53:14 PM
    Author     : Americo
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    HttpSession sesion = request.getSession();
    String id_usu = "";
    id_usu = (String) session.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }

    String cla_uni = request.getParameter("cla_uni");
    ConectionDB con = new ConectionDB();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reporte XLS</title>
    </head>
    <body>
        <%
            String tipoReporte = request.getParameter("tipoReporte");
            String fecha_inicial = request.getParameter("fecha_inicial");
            String fecha_final = request.getParameter("fecha_final");
            String clasificacion = request.getParameter("clasificacion");
            String cobertura = request.getParameter("cobertura");
            String origen = request.getParameter("origen");
            String tipoProducto = request.getParameter("tipoProducto");
            String exportToExcel = request.getParameter("exportToExcel");
            response.setContentType("application/vnd.ms-excel");
            String tipo="";
            if (tipoReporte.equals("1")) {
                tipo="RecetaFarmacia";
            } else {
                tipo="RecetaColectiva";
            }
            response.setHeader("Content-Disposition", "inline; filename="
                      + cla_uni + "_" + tipo + "_" + clasificacion + "_" + cobertura + "_" + origen +"_"+tipoProducto+ ".xls");

            if (tipoReporte.equals("1")) {
                out.print("Receta Farmacia");
            } else {
                out.print("Receta Colectiva");
            }
        %>
        <table >
            <tr>
                <td>CONTRATO</td>
                <td>FECHA DE EMISIÓN</td>
                <td>NO DE REMISIÓN</td>
                <td>MES</td>
                <td>FECHA DE FACTURA</td>
                <td>NO FACTURA</td>
                <td>IMPORTE FACTURA</td>
                <td>NOMBRE DEL PROVEEDOR</td>
                <td>DESCRIPCION DEL PRODUECTO(MEDICAMENTO)</td>
                <td>PRESENTACIÓN</td>
                <td>CLAVE  (11 DIGITOS)</td>
                <td>PRECIO UNITARIO</td>
                <td>CANTIDAD O PIEZAS</td>
                <td>SUBTOTAL</td>
                <td>IVA</td>
                <td>SUMINISTRO</td>
                <td>ANEXO</td>
                <td>UNIDAD O NOMBRE DEL CLIENTE</td>
                <td>FURISDICCION</td>
                <td>PACIENTE</td>
                <td>NO DE POLIZA</td>
                <td>NO DE RECETA</td>
            </tr>
            <%
                con.conectar();
                System.out.println(
                          "SELECT 'SSM-LPN-006/2018-01' AS contrato, DATE_FORMAT(NOW(), '%d/%m/%Y') AS fecha_emi, 'Marzo 2018' AS mes, DATE_FORMAT(r.fecha_hora, '%d/%m/%Y') AS fecha_factura, 'MEDALFA S.A. de C.V.' AS proveedor, p.des_pro AS descripcion, p.cla_lic AS clave, p.cos_pro AS precio_unitario, IFNULL(SUM(dr.cant_sur), 0) AS surtido, IFNULL(IF(p.cobertura='SEGURO POPULAR', 0,  p.cos_pro * dr.cant_sur), 0) AS importe, IFNULL(IF(p.cobertura='SEGURO POPULAR', 0, IF(p.tip_pro = 'MATERIAL DE CURACION', p.cos_pro * dr.cant_sur * 0.16, 0)), 0) AS iva, p.tip_pro AS suministro, p.cobertura AS ANEXO, DATE_FORMAT(dp.cad_pro, '%d/%m/%Y') AS caducidad, uni.des_uni, jur.des_jur, pac.nom_com, pac.num_afi, r.fol_rec AS folio FROM detreceta AS dr INNER JOIN detalle_productos AS dp ON dr.det_pro = dp.det_pro AND dr.fec_sur BETWEEN '"
                          + fecha_inicial + "' AND '" + fecha_final
                          + "' AND dr.baja = 0 AND dr.cant_sur <> 0 INNER JOIN receta AS r ON dr.id_rec = r.id_rec AND r.id_tip='" + tipoReporte
                          + "' INNER JOIN pacientes AS pac ON pac.id_pac = r.id_pac INNER JOIN medicos AS m ON r.cedula = m.cedula INNER JOIN unidades AS uni ON m.clauni = uni.cla_uni AND uni.cla_uni  ='"
                          + cla_uni
                          + "' INNER JOIN municipios AS mun ON mun.cla_mun = uni.cla_mun INNER JOIN jurisdicciones AS jur ON jur.cla_jur = mun.cla_jur INNER JOIN productos AS p ON dp.cla_pro = p.cla_pro AND p.origen='"
                          + origen + "' AND p.tip_pro='" + tipoProducto + "' AND p.cobertura = '" + cobertura + "' AND p.clasificacion='" + clasificacion
                          + "' GROUP BY r.id_rec,	r.id_tip,	p.clasificacion,p.cobertura,p.tip_pro,dp.cla_pro ORDER BY p.tip_pro,p.cobertura,r.id_tip,r.id_rec,p.cla_pro;");

                ResultSet rset = con.consulta(
                          "SELECT 'SSM-LPN-006/2018-01' AS contrato, DATE_FORMAT(NOW(), '%d/%m/%Y') AS fecha_emi, 'Marzo 2018' AS mes, DATE_FORMAT(r.fecha_hora, '%d/%m/%Y') AS fecha_factura, 'MEDALFA S.A. de C.V.' AS proveedor, p.des_pro AS descripcion, p.cla_lic AS clave, p.cos_pro AS precio_unitario, IFNULL(SUM(dr.cant_sur), 0) AS surtido, IFNULL(IF(p.cobertura='SEGURO POPULAR', 0,  p.cos_pro * dr.cant_sur), 0) AS importe, IFNULL(IF(p.cobertura='SEGURO POPULAR', 0, IF(p.tip_pro = 'MATERIAL DE CURACION', p.cos_pro * dr.cant_sur * 0.16, 0)), 0) AS iva, p.tip_pro AS suministro, p.cobertura AS ANEXO, DATE_FORMAT(dp.cad_pro, '%d/%m/%Y') AS caducidad, uni.des_uni, jur.des_jur, pac.nom_com, pac.num_afi, r.fol_rec AS folio FROM detreceta AS dr INNER JOIN detalle_productos AS dp ON dr.det_pro = dp.det_pro AND dr.fec_sur BETWEEN '"
                          + fecha_inicial + "' AND '" + fecha_final
                          + "' AND dr.baja = 0 AND dr.cant_sur <> 0 INNER JOIN receta AS r ON dr.id_rec = r.id_rec AND r.id_tip='" + tipoReporte
                          + "' INNER JOIN pacientes AS pac ON pac.id_pac = r.id_pac INNER JOIN medicos AS m ON r.cedula = m.cedula INNER JOIN unidades AS uni ON m.clauni = uni.cla_uni AND uni.cla_uni  ='"
                          + cla_uni
                          + "' INNER JOIN municipios AS mun ON mun.cla_mun = uni.cla_mun INNER JOIN jurisdicciones AS jur ON jur.cla_jur = mun.cla_jur INNER JOIN productos AS p ON dp.cla_pro = p.cla_pro AND p.origen='"
                          + origen + "' AND p.tip_pro='" + tipoProducto + "' AND p.cobertura = '" + cobertura + "' AND p.clasificacion='" + clasificacion
                          + "' GROUP BY r.id_rec,	r.id_tip,	p.clasificacion,p.cobertura,p.tip_pro,dp.cla_pro ORDER BY p.tip_pro,p.cobertura,r.id_tip,r.id_rec,p.cla_pro;");
                while (rset.next()) {
            %>
            <tr>
                <td><%=rset.getString(1)%></td>
                <td><%=rset.getString("fecha_factura")%></td>
                <td></td>
                <td><%=rset.getString(3)%></td>
                <td></td>
                <td></td>
                <td></td>
                <td><%=rset.getString(5)%></td>
                <td><%=rset.getString(6)%></td>
                <td></td>
                <td><%=rset.getString(7)%></td>
                <td><%=rset.getString(8)%></td>
                <td><%=rset.getString(9)%></td>
                <td><%=rset.getString(10)%></td>
                <td><%=rset.getString(11)%></td>
                <td><%=rset.getString(12)%></td>
                <td><%=rset.getString(13)%></td>
                <td><%=rset.getString("des_uni")%></td>
                <td><%=rset.getString("des_jur")%></td>
                <td><%=rset.getString("nom_com")%></td>
                <td><%=rset.getString("num_afi")%></td>
                <td><%=rset.getString("folio")%></td>
            </tr>
            <%
                }
                con.cierraConexion();
            %>
        </table>
    </body>
    <script>
        window.close();
    </script>
</html>
