<%-- 
    Document   : index
    Created on : 07-mar-2014, 15:38:43
    Author     : Americo
--%>

<%@page import="java.sql.SQLException"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%    HttpSession sesion = request.getSession();
    String id_usu = "";
    
    try {
        
        id_usu = (String) sesion.getAttribute("id_usu");
        
    } catch (Exception e) {
        Logger.getLogger("reporteAbasto.jsp").log(Level.SEVERE, e.getMessage(), e);
    }

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }
    ConectionDB con = new ConectionDB();

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="../css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../css/topPadding.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <title>Reporte de Abastos</title>
    </head>
    <body>
        <%@include file="../jspf/mainMenu.jspf"%>
        <br/>
        <div class="container">
            <div class="row text-left" style="width: 600px;">
                <h1>Reporte de Abastos</h2>
            </div>          
            <br/><br/>
            <div class="panel"  id="paAbasto">
                <table id="tbAbastos" class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="h2 text-center" colspan="4">Abastos</th>
                        </tr>
                        <tr>
                            <th>Cantidad de Claves</th>
                            <th>Nombre</th>
                            <th>Fecha</th>
                            <th>Ver Detalle</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%       //                   
                            try {
                                con.conectar();
                                ResultSet rs = con.consulta("SELECT Count(ra.id) AS CantidadClaves, ra.nom_abasto, ra.fecha FROM reporte_abasto AS ra GROUP BY ra.nom_abasto ORDER BY ra.fecha DESC");
                                while (rs.next()) {
                        %>
                        <tr id="">
                            <td><%=rs.getString(1)%></td>
                            <td><%=rs.getString(2)%></td>
                            <td><%=rs.getString(3)%></td>
                            <td>
                                <a class="btn btn-default btn-sm" id="Detalles" onclick="DetalleAb('<%=rs.getString(2)%>')" data-toggle="modal" data-target="#DetalleAb"><span class="glyphicon glyphicon-eye-open"></span></a>
                                <button class="btn btn-sm btn-success" title="Exportar a Excel" type="button" onclick="window.open('reporteAbastoExcel.jsp?nom=<%=rs.getString(2)%>', '_blank');"><span class="glyphicon glyphicon-export"></span></button>
                                <button class="btn btn-sm btn-danger" title="Exportar a Pdf" type="button" onclick="window.open('pdfExport.jsp?nom=<%=rs.getString(2)%>', '_blank');"><span class="glyphicon glyphicon-download"></span></button>
                            </td>
                        </tr>
                        <%
                                }
                            } catch (SQLException ex) {
                                Logger.getLogger("reporteAbasto.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                            } finally {
                                try {
                                    if (con.estaConectada()) {
                                        con.cierraConexion();
                                    }
                                } catch (SQLException ex) {
                                    Logger.getLogger("reporteAbasto.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                }
                            }
                        %>
                    </tbody>
                </table>
            </div>
        </div>

        <br>
        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <img src="../imagenes/gobierno.png" width=100 alt="Logo">
            </div>
        </div>

        <div id="DetalleAb" class="modal fade in" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <a data-dismiss="modal" class="close">×</a>
                        <h2 class="text-center"d="dta">Detalle Abasto</h2>
                        <br>
                        <h4  id="nombre_abasto"></h4>
                        <h4  id="total">Total de piezas:</h4>
                        <h4 id="total_filtro">Total a cargar: </h4>

                    </div>
                    <div class="modal-body">
                        <div class="panel panel-body">
                            <table class="table table-bordered" id="tbDetalleAba">
                                <thead>
                                    <tr>
                                        <th>Clave</th>
                                        <th>Lote</th>
                                        <th>Caducidad</th>
                                        <th>Cantidad</th>
                                        <th>Origen</th>
                                        <th>Observaciones</th>
                                    </tr>
                                </thead>
                                <tbody id="dataDetalle">
                                </tbody>
                            </table>                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" data-dismiss="modal" class="btn btn-success">Cerrar</a>
                    </div>
                </div>
            </div>                
        </div>
        <!-- 
================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="../js/jquery-2.1.4.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery-ui.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/jquery.progressTimer.js"></script>
        <script src="../js/dataTables.bootstrap.js" type="text/javascript"></script>
        <script type="text/javascript">
                                    var tabla_detalle;
                                    $(document).ready(function () {
                                        $('#tbAbastos').dataTable({
                                            "order": [[3, "desc"]],
                                            "sPaginationType": "full_numbers"
                                        });
                                        tabla_detalle = $('#tbDetalleAba').DataTable({
                                            columns: [
                                                {data: 'clave'},
                                                {data: 'lote'},
                                                {data: 'caducidad'},
                                                {data: 'cantidad'},
                                                {data: 'origen'},
                                                {data: 'observacion'}
                                            ],
                                            "sPaginationType": "full_numbers"
                                        });
                                    });
                                    function DetalleAb(nom) {
                                        var dir = "../EditaAbasto";
                                        $("#nombre_abasto").text("Nombre del abasto: " + nom);

                                        $.ajax({
                                            type: "POST",
                                            url: dir,
                                            data: {accion: "Detalle", nom: nom},
                                            success: function (data) {
                                                var json = JSON.parse(data);
                                                if (json.msg !== "") {
                                                    alert(json.msg);
                                                } else {
                                                    //console.log(json);
                                                    var cantidad = json.cantidad;
                                                    delete json.cantidad;
                                                    $("#total").text("Total de piezas: " + cantidad);
                                                    var cantidad_filtro = json.cantidad_filtro;
                                                    delete json.cantidad_filtro;
                                                    $("#total_filtro").text("Total cargadas: " + cantidad_filtro);
                                                    delete json.msg;
                                                    //console.log(cantidad, cantidad_filtro);


                                                    tabla_detalle
                                                            .clear()
                                                            .draw();
                                                    //var filas = new Array();
                                                    $.each(json, function (index, value) {

                                                        var cargado = value.cargado;
                                                        delete value.cargado;
                                                        //console.log(value);
                                                        ;
                                                        var node = tabla_detalle.row.add(value)
                                                                .draw("full-reset")
                                                                .node();
                                                        if (cargado !== "1") {
                                                            $(node).addClass('danger text-danger');
                                                        }

                                                    });
                                                    //tabla_detalle.draw("full-reset");
                                                }

                                            },
                                            error: function () {
                                                alert("Ocurrió un error");
                                            }
                                        });
                                    }
        </script>
        <br/>
    </body>
</html>


