<%-- 
    Document   : imprimeCiclico
    Created on : 18/05/2016, 09:51:18 AM
    Author     : juan
--%>

<%@page import="java.io.File"%>
<%@page import="net.sf.jasperreports.engine.JasperRunManager"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.sql.Connection"%>
<%@page import="Clases.ConectionDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

    String Folio = "";

    try {
        Folio = request.getParameter("folio");

    } catch (Exception e) {
        e.printStackTrace();
    }


%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%            ConectionDB con = new ConectionDB();
            Connection conn;
            try {
                con.conectar();
                conn = con.getConn();
                Map parameter = new HashMap();
                parameter.put("folio", Folio);
                 File reportFile = new File(application.getRealPath("/reportes/ciclicos.jasper"));
                    byte[] bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), parameter, conn);
                    /*Indicamos que la respuesta va a ser en formato PDF*/
                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);
                    ServletOutputStream ouputStream = response.getOutputStream();
                    ouputStream.write(bytes, 0, bytes.length);
                    /*Limpiamos y cerramos flujos de salida*/

                    ouputStream.flush();
                    ouputStream.close();
               
                   
                    
                

            } catch (SQLException ex) {
                Logger.getLogger("imprimeCiclico.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                out.println("<script>alert('Error al General el Reporte')</script>");
                    out.println("<script>window.close();</script>");
            } finally {
                try {
                    con.cierraConexion();
                } catch (SQLException ex) {
                    Logger.getLogger("imprimeCiclico.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                }
            }
        %>
    </body>
</html>
