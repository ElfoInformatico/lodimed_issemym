<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    HttpSession sesion = request.getSession();
    String id_usu = "";
    try {
        id_usu = (String) session.getAttribute("id_usu");
    } catch (Exception e) {
    }
    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
    }
    ConectionDB con = new ConectionDB();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Estilos CSS -->
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/cupertino/jquery-ui-1.10.3.custom.css" />
        <link href="../css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!---->
        <title>Reporte de Reabastecimiento</title>


    </head>
    <body>
        <div class="container" >
            <%@include file="../jspf/mainMenu.jspf"%>
            <br/><br/><br/>
            <h1>Reporte de Reabastecimiento</h1>
            <hr/>
            <form name="cargaAbasto" method="POST" enctype="multipart/form-data" action="../requerimiento" onsubmit="funcionCarga()">
                <div class="row">
                    <label class="form-horizontal col-lg-4">Seleccione un archivo:</label>
                    <div class="col-lg-8">
                        <input required type="file" class="form-control" name="archivo" accept=".csv">
                    </div>
                </div>
                <br />
                <div class="row">
                    <label class="form-horizontal col-lg-4">Contraseña:</label>
                    <div class="col-lg-8">
                        <input required type="password" name="contra" placeholder="Contraseña de Administrador" class="form-control">
                    </div>
                </div>
                <br />
                <div class="col-lg-12">
                    <button class="btn btn-block btn-success" id="btnCarga">Cargar</button>
                </div>
            </form>
            <form method="post" action="../Reabastecimiento?accion=cargaReabastecimiento">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-bordered table-condensed table-responsive table-striped">
                            <thead>
                                <tr>
                                    <td>Clave</td>
                                    <td>Descripción</td>
                                    <td style="width: 130px">Cantidad</td>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    con.conectar();
                                    ResultSet rset = con.consulta(
                                              "select p.cla_pro, p.des_pro from reabastecimiento r INNER JOIN productos p on r.clave = p.cla_pro");
                                    while (rset.next()) {
                                %>
                                <tr>
                                    <td><%=rset.getString(1)%></td>
                                    <td><%=rset.getString(2)%></td>
                                    <td><input class="form-control" id="clave<%=rset.getString(1)%>" name="clave<%=rset.getString(1)%>" type="number" min="0"  onkeypress="return tabular(event, this)" value=""></td>
                                </tr>
                            </tbody>
                            <%
                                }
                                con.cierraConexion();
                            %>
                        </table>

                    </div>
                    <!--div class="panel-body" id="tableTot" >
        
                        <div id="container">
        
                            <div id="dynamic"></div>
        
                        </div>
                    </div-->
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <button class="btn btn-block btn-primary" onclick=" return confirm('Desea carga el requerimiento?')">Cargar requerimiento</button>
                    </div>
                </div>
            </form>
        </div>
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/reabastecimiento/reabastecimiento.js"></script>
    </body>

</html>
