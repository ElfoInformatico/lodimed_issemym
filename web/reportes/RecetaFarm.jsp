<%@page import="Clases.ConectionDB"%><%@page import="javax.print.attribute.standard.Copies"%><%@page import="javax.print.attribute.standard.MediaSizeName"%><%@page import="javax.print.attribute.standard.MediaSize"%><%@page import="javax.print.attribute.standard.MediaPrintableArea"%><%@page import="javax.print.attribute.PrintRequestAttributeSet"%><%@page import="javax.print.attribute.HashPrintRequestAttributeSet"%><%@page import="net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter"%><%@page import="net.sf.jasperreports.engine.export.JRPrintServiceExporter"%><%@page import="javax.print.PrintServiceLookup"%><%@page import="javax.print.PrintService"%><%@page import="net.sf.jasperreports.view.JasperViewer"%><%@page import="net.sf.jasperreports.engine.util.JRLoader"%><%@page import="java.net.URL"%><%@page import="java.sql.DriverManager"%><%@page import="java.io.*"%><%@page import="java.util.HashMap"%><%@page import="java.util.Map"%><%@page import="net.sf.jasperreports.engine.*"%><%@page import="java.sql.ResultSet"%><%@page import="java.sql.Connection"%><%@page import="NumerosLetras.NumLetrasScriptlet"%><%
    String Folio = "", Tipo = "", Usuario = "", unidad = "";
    String nom = "", ap = "", am = "";
    HttpSession sesion = request.getSession(true);
    nom = (String) sesion.getAttribute("nombre");
    ap = (String) sesion.getAttribute("ap");
    am = (String) sesion.getAttribute("am");

    try {
        Folio = request.getParameter("fol_rec");
        Tipo = request.getParameter("tipo");
        Usuario = nom + " " + ap + " " + am;
        unidad = request.getParameter("uni");
    } catch (Exception e) {
        System.out.println("ErrorGeneral->" + e);
    }

    ConectionDB con = new ConectionDB();
    Connection conn;
    con.conectar();
    conn = con.getConn();

    PrintRequestAttributeSet printRequestAttributeSet = new HashPrintRequestAttributeSet();
    //MediaSizeName mediaSizeName = MediaSize.findMedia(4, 4, MediaPrintableArea.INCH);
    //printRequestAttributeSet.add(mediaSizeName);
    printRequestAttributeSet.add(new Copies(1));

    //Farmacia o m�dico.
    if (Tipo.equals("1") || Tipo.equals("3")) {
        File reporticket = null;
        try {
            reporticket = new File(application.getRealPath("/reportes/Ticket.jasper"));
        } catch (Exception ex) {
            System.out.println("ErrorRutaCOL->" + ex);
            //reporticket = new File("/home/linux9/NetBeansProjects/SCR_MEDALFA/SCR_MEDALFA/web/reportes/RecetaTicket.jasper");
        }
        Map parameterticket = new HashMap();
        parameterticket.put("folio", Folio);
        parameterticket.put("NomUsu", Usuario);
        byte[] bytes = JasperRunManager.runReportToPdf(reporticket.getPath(), parameterticket, conn);
        /*Indicamos que la respuesta va a ser en formato PDF*/
        response.setContentType("application/pdf");
        response.setContentLength(bytes.length);
        ServletOutputStream ouputStream = response.getOutputStream();
        ouputStream.write(bytes, 0, bytes.length);
        /*Limpiamos y cerramos flujos de salida*/

        ouputStream.flush();
        ouputStream.close();

        //JasperPrintManager.printReport(jasperPrintticket, false);
    } else if (Tipo.equals("2") || Tipo.equals("4")) {
        File reporticket = null;
        try {
            reporticket = new File(application.getRealPath("/reportes/RecetaCol.jasper"));
        } catch (Exception ex) {
            System.out.println("ErrorRutaCOL->" + ex);
        }
        Map parameterticket = new HashMap();
        parameterticket.put("folio", Folio);
        parameterticket.put("NomUsu", Usuario);
        byte[] bytes = JasperRunManager.runReportToPdf(reporticket.getPath(), parameterticket, conn);
        /*Indicamos que la respuesta va a ser en formato PDF*/
        response.setContentType("application/pdf");
        response.setContentLength(bytes.length);
        ServletOutputStream ouputStream = response.getOutputStream();
        ouputStream.write(bytes, 0, bytes.length);
        /*Limpiamos y cerramos flujos de salida*/

        ouputStream.flush();
        ouputStream.close();

        //JasperPrintManager.printReport(jasperPrintticket, false);
    } else if (Tipo.equals("4")) {
        File reporticket = null;
        try {
            reporticket = new File(application.getRealPath("/reportes/Ticket.jasper"));
        } catch (Exception ex) {
            System.out.println("ErrorRutaCOL->" + ex);
            //reporticket = new File("/home/linux9/NetBeansProjects/SCR_MEDALFA/SCR_MEDALFA/web/reportes/RecetaTicket.jasper");
        }
        Map parameterticket = new HashMap();
        parameterticket.put("folio", Folio);
        parameterticket.put("NomUsu", Usuario);
        byte[] bytes = JasperRunManager.runReportToPdf(reporticket.getPath(), parameterticket, conn);
        /*Indicamos que la respuesta va a ser en formato PDF*/
        response.setContentType("application/pdf");
        response.setContentLength(bytes.length);
        ServletOutputStream ouputStream = response.getOutputStream();
        ouputStream.write(bytes, 0, bytes.length);
        /*Limpiamos y cerramos flujos de salida*/

        ouputStream.flush();
        ouputStream.close();

    } else if (Tipo.equals("5")) {

        File reporticket = null;
        try {
            reporticket = new File(application.getRealPath("/reportes/Ticket_Farmacia.jasper"));
        } catch (Exception ex) {
            System.out.println("ErrorRutaCOL->" + ex);
        }
        Map parameterticket = new HashMap();
        parameterticket.put("folio", Folio);
        byte[] bytes = JasperRunManager.runReportToPdf(reporticket.getPath(), parameterticket, conn);
        /*Indicamos que la respuesta va a ser en formato PDF*/
        response.setContentType("application/pdf");
        response.setContentLength(bytes.length);
        ServletOutputStream ouputStream = response.getOutputStream();
        ouputStream.write(bytes, 0, bytes.length);
        /*Limpiamos y cerramos flujos de salida*/

        ouputStream.flush();
        ouputStream.close();
    } else {
        File reporticket = null;
        try {
            reporticket = new File(application.getRealPath("/reportes/TicketUsuarios.jasper"));
        } catch (Exception ex) {
            System.out.println("ErrorRutaCOL->" + ex);
            //reporticket = new File("/home/linux9/NetBeansProjects/SCR_MEDALFA/SCR_MEDALFA/web/reportes/RecetaTicket.jasper");
        }
        Map parameter = new HashMap();
        parameter.put("cu", unidad);
        parameter.put("NomUsu", Usuario);
        byte[] bytes = JasperRunManager.runReportToPdf(reporticket.getPath(), parameter, conn);
        /*Indicamos que la respuesta va a ser en formato PDF*/
        response.setContentType("application/pdf");
        response.setContentLength(bytes.length);
        ServletOutputStream ouputStream = response.getOutputStream();
        ouputStream.write(bytes, 0, bytes.length);
        /*Limpiamos y cerramos flujos de salida*/

        ouputStream.flush();
        ouputStream.close();

    }
%>