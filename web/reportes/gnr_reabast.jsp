
<%@page import="Impl.reabDaoImpl"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%java.text.DateFormat df3 = new java.text.SimpleDateFormat("yyyyMMddhhmmss"); %>
<%java.text.DateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("dd/MM/yyyy");%>
<!DOCTYPE html>
<%
    DecimalFormat formatter = new DecimalFormat("#,###,###");
    DecimalFormat formatter2 = new DecimalFormat("#,###,###.##");
    DecimalFormatSymbols custom = new DecimalFormatSymbols();
    custom.setDecimalSeparator(',');
    formatter.setDecimalFormatSymbols(custom);
    ConectionDB con = new ConectionDB();
    con.conectar();
    HttpSession sesion = request.getSession();
    response.setContentType("application/vnd.ms-excel");
    response.setHeader("Content-Disposition", "attachment;filename=\"Reporte de Reabastecimiento.xls\"");
//declaracion de variables-------------------------------------------------------------------------------------------------------------------
    String but = "";
    String fecha1 = "", fecha2 = "", mensaje = "";
//-------------------------------------------------------------------------------------------------------------------------------------------

    Calendar calendar = Calendar.getInstance();
    Calendar calendar2 = Calendar.getInstance();
//out.println("Fecha Actual: " + calendar.getTime());
    calendar.add(Calendar.MONTH, -1);
//out.println("Fecha antigua: " + df.format(calendar.getTime()));
    String fecha_act = "" + df.format(calendar.getTime());

    String d = "0";
    int dias = 0;

    try {
        but = "" + request.getParameter("submit");
    } catch (Exception e) {
        System.out.print("not");
    }

    //out.print(but);
    try {
        d = request.getParameter("dias");
    } catch (Exception e) {
        System.out.print("not");
    }
    if (d.equals("")) {
        d = "0";
    }
    //out.print(d);
    dias = Integer.parseInt(d);
    calendar2.add(Calendar.DATE, dias);
    String fecha_imp = "" + df.format(calendar2.getTime());
    String cu = "";
    int frecuenciaPaso = 0;
%>
<html>
    <head>
        <title>Reporte de Reabastecimiento</title>
    </head>
    <body>
        <div>
            Reporte de Reabastecimiento</div>
        <form method="post" style="">
            <p>
                <%
                    String qry_unidad = "SELECT des_uni,cla_uni FROM unidades where cla_uni = '" + (String) sesion.getAttribute("cla_uni") + "'";
                    String unidad = "";
                    ResultSet rset = con.consulta(qry_unidad);
                    while (rset.next()) {
                        unidad = rset.getString("des_uni");
                        cu = rset.getString("cla_uni");
                    }
                %>
                Unidad: <%=unidad%>
                <br />D&iacute;as para su pr&oacute;ximo abasto: <%=d%>
                <br />
                Fecha de entrega: <%=fecha_imp%></p>
        </form>
        <table width="780" border="1" style="font-size: 8px;">
            <tr>
                <td width="258"><Strong>Descripcion</Strong></td>
                <td width="53"><Strong>Consumo mensual</Strong></td>
                <td width="53"><Strong>IFS</Strong></td>
                <td width="57"><Strong>Exist.<br/>Inventario</Strong></td>
                <td width="54"><Strong>Sobreabasto</Strong></td>
                <td width="54"><Strong>Recomendado<br /> a Surtir</Strong></td>
                <td width="62"><strong>Clave</strong></td>
                <td width="60"><Strong>Confirmacion / <br /> Autorizacion</Strong></td>
                <td width="130"><Strong>Observaciones</Strong></td>
            </tr>

            <%
                PreparedStatement ps = con.getConn().prepareStatement("SELECT ra.clave, p.des_pro AS descripcion, ra.cdm, ra.inventario, ra.sobreAbasto, ra.stockMaximo AS ifs, ra.sugerido FROM reabastecimiento AS ra INNER JOIN productos AS p ON ra.clave = p.cla_pro;");
                ResultSet rs = ps.executeQuery();
                String descrip_rf, clave_rf;
                int cant_total, cant_quincenal, cant_inv, sobre, total_t;
                while (rs.next()) {
                    descrip_rf = rs.getString("descripcion");
                    clave_rf = rs.getString("clave");
                    cant_total = rs.getInt("cdm");
                    cant_quincenal = rs.getInt("ifs");
                    cant_inv = rs.getInt("inventario");
                    sobre = rs.getInt("sobreAbasto");
                    total_t = rs.getInt("sugerido");

            %>
            <tr>
                <td style="text-align:left"><%=descrip_rf%></td>
                <td><%=cant_total%></td>
                <td><%=cant_quincenal%></td>
                <td><%=cant_inv%></td>
                <td <%
                    if (sobre != 0) {
                        out.print("style='color:#FFF; background-color:#F00;'");
                    }
                    %>><%=sobre%>
                </td>
                <td><strong><%=total_t%>
                    </strong></td>
                <td><%=clave_rf%></td>
                <td><strong><%=total_t%>
                    </strong></td>
                <td></td>
            </tr>
            <%

                }
                rs.close();
                ps.close();
                con.cierraConexion();
            %>
        </table>
        <p>&nbsp;</p>


    </body>

