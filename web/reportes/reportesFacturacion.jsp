<%-- 
    Document   : movClaves
    Created on : 24/04/2015, 10:36:45 AM
    Author     : Americo
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>
<%
    HttpSession sesion = request.getSession();
    String id_usu = "";
    id_usu = (String) session.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }

    ConectionDB con = new ConectionDB();
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Estilos CSS -->
        <link href="../css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../css/topPadding.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

        <!---->
        <title>Reporte de movimiento de claves</title>
    </head>
    <body class="container">
        <%@include file="../jspf/mainMenu.jspf" %>
        <br/>
        <%           if (((String) sesion.getAttribute("tipo")).equals("RURAL")) {
        }else{
        %> 
        <form onsubmit="return false;">
            <div class="row" style="padding-top: 50px">
                <h1>Reportes Facturación</h1>
            </div>
            <hr/>
            <div class="row">
                <div class="col-sm-3">
                    <h4>Seleccione el reporte a generar</h4>
                </div>
                <div class="col-sm-4">
                    <select class="form-control" name="tipoReporte" id="tipoReporte" onchange="seleccionaTipoReporte()">
                        <option value="farmacia">Reporte Facturación Farmacia</option>
                        <option value="colectivo">Reporte Facturación Colectivo</option>
                        <option value="global">Reporte Facturación Global</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <h4>Fechas del reporte</h4>
                </div>
                <div class="col-sm-2">
                    <input class="form-control" type="date" id="fechaInicial" name="fechaInicial" required>
                </div>
                <div class="col-sm-2">
                    <input class="form-control" type="date" id="fechaFinal" name="fechaFinal" required>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <h4>Remisión No</h4>
                </div>
                <div class="col-sm-2">
                    <input class="form-control" type="number" name="remision" id="remision" required>
                </div>
                <div class="col-sm-2">
                    <button class="btn btn-primary btn-block" onclick="compararFechas()">Generar</button>
                </div>
                <div class="col-sm-2">
                    <button class="btn btn-success btn-block" onclick="compararFechasXLS()">Generar XLS</button>
                </div>
            </div>
        </form>
        <%            }
        %>
        <hr/>
        <form>
            <div class="row">
                <h1>Reportes Facturación Rurales</h1>
            </div>
            <hr/>
            <div class="row">
                <div class="col-sm-3">
                    <h4>Tipo de reporte</h4>
                </div>
                <div class="col-sm-4">
                    <select class="form-control" name="tipoRural" id="tipoRural" onchange="seleccionaRural()">
                        <option value="rural">Rural</option>
                        <option value="global">Global</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <h4>Seleccione la Jurisdicción</h4>
                </div>
                <div class="col-sm-4">
                    <%           if (((String) sesion.getAttribute("tipo")).equals("RURAL")) {

                    %> 
                    <select class="form-control" name="jurisReporte" id="jurisReporte" onchange="seleccionaJuris()">
                        <option value="JURISDICCION SANITARIA NO. 1 MORELIA">JURISDICCION SANITARIA NO. 1 MORELIA</option>
                        <option value="JURISDICCION SANITARIA NO. 2 ZAMORA">JURISDICCION SANITARIA NO. 2 ZAMORA</option>
                        <option value="JURISDICCION SANITARIA NO. 3 ZITÁCUARO">JURISDICCION SANITARIA NO. 3 ZITÁCUARO</option>
                        <option value="JURISDICCION SANITARIA NO. 4 PÁTZCUARO">JURISDICCION SANITARIA NO. 4 PÁTZCUARO</option>
                        <option value="JURISDICCION SANITARIA NO. 5 URUAPAN">JURISDICCION SANITARIA NO. 5 URUAPAN</option>
                        <option value="JURISDICCION SANITARIA NO. 6 LA PIEDAD">JURISDICCION SANITARIA NO. 6 LA PIEDAD</option>
                        <option value="JURISDICCION SANITARIA NO. 7 APATZINGÁN">JURISDICCION SANITARIA NO. 7 APATZINGÁN</option>
                        <option value="JURISDICCION SANITARIA NO. 8 LÁZARO CÁRDENAS">JURISDICCION SANITARIA NO. 8 LÁZARO CÁRDENAS</option>
                    </select>
                    <%                    } else {
                    %>
                    <select class="form-control" name="jurisReporte" id="jurisReporte" onchange="seleccionaJuris()">
                        <option value="JURISDICCION SANITARIA NO. 1 MORELIA">JURISDICCION SANITARIA NO. 1 MORELIA</option>
                        <option value="JURISDICCION SANITARIA NO. 2 ZAMORA">JURISDICCION SANITARIA NO. 2 ZAMORA</option>
                        <option value="JURISDICCION SANITARIA NO. 3 ZITÁCUARO">JURISDICCION SANITARIA NO. 3 ZITÁCUARO</option>
                        <option value="JURISDICCION SANITARIA NO. 4 PÁTZCUARO">JURISDICCION SANITARIA NO. 4 PÁTZCUARO</option>
                        <option value="JURISDICCION SANITARIA NO. 5 URUAPAN">JURISDICCION SANITARIA NO. 5 URUAPAN</option>
                        <option value="JURISDICCION SANITARIA NO. 6 LA PIEDAD">JURISDICCION SANITARIA NO. 6 LA PIEDAD</option>
                        <option value="JURISDICCION SANITARIA NO. 7 APATZINGÁN">JURISDICCION SANITARIA NO. 7 APATZINGÁN</option>
                        <option value="JURISDICCION SANITARIA NO. 8 LÁZARO CÁRDENAS">JURISDICCION SANITARIA NO. 8 LÁZARO CÁRDENAS</option>
                    </select>
                    <%
                        }
                    %>
                </div>
            </div>
            <div class="row" id="divTipoMedRural">
                <div class="col-sm-3">
                    <h4>Tipo Producto</h4>
                </div>
                <div class="col-sm-4">
                    <select class="form-control" name="tipoProducto" id="tipoProducto" >
                        <option value="MEDICAMENTO">MEDICAMENTO</option>
                        <option value="MATERIAL DE CURACION">MATERIAL DE CURACIÓN</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <h4>Fechas de los reportes</h4>
                </div>
                <div class="col-sm-2">
                    <input class="form-control" type="date" id="fechaInicialJuris" name="fechaInicialJuris" required>
                </div>
                <div class="col-sm-2">
                    <input class="form-control" type="date" id="fechaFinalJuris" name="fechaFinalJuris" required>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <h4>Remisión No</h4>
                </div>
                <div class="col-sm-2">
                    <input class="form-control" type="number" name="remisionJuris" id="remisionJuris" required>
                </div>
                <div class="col-sm-2">
                    <button class="btn btn-primary btn-block" onclick="compararFechasJuris()">Generar</button>
                </div>
                <div class="col-sm-2">
                    <button class="btn btn-success btn-block" onclick="compararFechasRuralesXLS()">Generar XLS</button>
                </div>
            </div>
        </form>
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery-ui.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/dataTables.bootstrap.js"></script>
        <script>

                        function seleccionaRural() {
                            var seleccion = $('#tipoRural').val();
                            if (seleccion === 'global') {
                                $('#divTipoMedRural').hide(500);
                            }
                            if (seleccion === 'rural') {
                                $('#divTipoMedRural').show(500);
                            }
                        }
                        function compararFechas()
                        {
                            var fechaInicial = document.getElementById("fechaInicial").value;
                            var fechaFinal = document.getElementById("fechaFinal").value;
                            var remision = document.getElementById("remision").value;
                            if ((new Date(fechaInicial).getTime() <= new Date(fechaFinal).getTime()))
                            {
                                if (remision !== "") {
                                    generaPDF();
                                } else {
                                    alert("Verifique la remision");
                                }
                            } else {
                                alert("Verifique las fechas");
                            }
                        }
                        function compararFechasXLS()
                        {
                            var fechaInicial = document.getElementById("fechaInicial").value;
                            var fechaFinal = document.getElementById("fechaFinal").value;
                            var remision = document.getElementById("remision").value;
                            if ((new Date(fechaInicial).getTime() <= new Date(fechaFinal).getTime()))
                            {
                                generaXLS();

                            } else {
                                alert("Verifique las fechas");
                            }
                        }
                        function compararFechasJuris()
                        {
                            var fechaInicial = document.getElementById("fechaInicialJuris").value;
                            var fechaFinal = document.getElementById("fechaFinalJuris").value;
                            var remision = document.getElementById("remisionJuris").value;
                            if ((new Date(fechaInicial).getTime() <= new Date(fechaFinal).getTime()))
                            {
                                if (remision !== "") {
                                    generaPDFJuris();
                                } else {
                                    alert("Verifique la remision");
                                }
                            } else {
                                alert("Verifique las fechas");
                            }
                        }

                        function compararFechasRuralesXLS()
                        {
                            var fechaInicial = document.getElementById("fechaInicialJuris").value;
                            var fechaFinal = document.getElementById("fechaFinalJuris").value;
                            if ((new Date(fechaInicial).getTime() <= new Date(fechaFinal).getTime()))
                            {
                                generaXLSRurales();
                            } else {
                                alert("Verifique las fechas");
                            }
                        }

                        function generaXLS() {
                            var fecha_inicial = $('#fechaInicial').val();
                            var fecha_final = $('#fechaFinal').val();
                            window.open('../ReportesFacturacion?accion=generaXLS&fechaInicial=' + fecha_inicial + '&fechaFinal=' + fecha_final);

                        }

                        function generaXLSRurales() {
                            var fecha_inicial = $('#fechaInicialJuris').val();
                            var fecha_final = $('#fechaFinalJuris').val();
                            window.open('../ReportesFacturacion?accion=generaXLSRurales&fechaInicial=' + fecha_inicial + '&fechaFinal=' + fecha_final);

                        }


                        function generaPDF() {
                            var tipoReporte = $('#tipoReporte').val();
                            var fecha_inicial = $('#fechaInicial').val();
                            var fecha_final = $('#fechaFinal').val();
                            var remision = $('#remision').val();
                            var remisionint = parseInt(remision);
                            var clasificacion = ["ABIERTO", "CONTROLADO"];
                            var cobertura = ["SEGURO POPULAR", "SIGLO XXI", "POBLACIÓN ABIERTA", "GASTOS CATASTROFICOS"];
                            var tipoProducto = ["MEDICAMENTO", "MATERIAL DE CURACION"];
                            var origen = [0, 2];
                            if (tipoReporte === "global") {
                                window.open('ReporteFacturacionPDF.jsp?tipoReporte=' + tipoReporte + '&fechaInicial=' + fecha_inicial + '&fechaFinal=' + fecha_final + '&remision=' + remision + '&clasificacion=1&cobertura=1&origen=1');
                            } else if (tipoReporte === "farmacia") {
                                for (var k = 0; k < clasificacion.length; k++) {
                                    for (var j = 0; j < cobertura.length; j++) {
                                        for (var i = 0; i < origen.length; i++) {
                                            window.open('ReporteFacturacionPDF.jsp?tipoReporte=' + tipoReporte + '&fechaInicial=' + fecha_inicial + '&fechaFinal=' + fecha_final + '&remision=' + remisionint + '&clasificacion=' + clasificacion[k] + '&cobertura=' + cobertura[j] + '&origen=' + origen[i] + '');
                                            remisionint++;
                                        }
                                    }
                                }
                            } else if (tipoReporte === "colectivo") {
                                for (var l = 0; l < tipoProducto.length; l++) {
                                    for (var k = 0; k < clasificacion.length; k++) {
                                        for (var j = 0; j < cobertura.length; j++) {
                                            for (var i = 0; i < origen.length; i++) {
                                                window.open('ReporteFacturacionPDF.jsp?tipoReporte=' + tipoReporte + '&fechaInicial=' + fecha_inicial + '&fechaFinal=' + fecha_final + '&remision=' + remisionint + '&clasificacion=' + clasificacion[k] + '&cobertura=' + cobertura[j] + '&origen=' + origen[i] + '&tipoProducto=' + tipoProducto[l]);
                                                remisionint++;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        function generaPDFJuris() {
                            var juris = $('#jurisReporte').val();
                            var tipoRural = $('#tipoRural').val();
                            var fecha_inicial = $('#fechaInicialJuris').val();
                            var fecha_final = $('#fechaFinalJuris').val();
                            var remision = $('#remisionJuris').val();
                            var tipoProducto = $('#tipoProducto').val();
                            var remisionint = parseInt(remision);
                            window.open('../ReportesFacturacion?accion=generaPDFJuris&juris=' + juris + '&fechaInicial=' + fecha_inicial + '&fechaFinal=' + fecha_final + '&remision=' + remisionint + "&tipoProducto=" + tipoProducto + "&tipoRural=" + tipoRural);
                        }
        </script>
    </body>
</html>
