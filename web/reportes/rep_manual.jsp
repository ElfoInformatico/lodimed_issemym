
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%java.text.DateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("dd/MM/yyyy");%>
<!DOCTYPE html>
<%
    HttpSession sesion = request.getSession();
    String id_usu = "";
    id_usu = (String) sesion.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Estilos CSS -->
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/jquery.dataTables2.min.css" rel="stylesheet">
        <link href="../css/buttons.dataTables.min.css" rel="stylesheet">
        <link href="../css/cupertino/jquery-ui-1.10.3.custom.css" rel="stylesheet">
        <link href="../css/dataTables.tableTools.css" rel="stylesheet">
        <!---->
        <title>Reporte Recetas Manuales</title>
    </head>
    <body>
        <%@include file="../jspf/mainMenu.jspf"%>
        <div class="container" style="padding-top: 50px">
            <h1>Reporte Recetas Manuales</h1>
        </div>

        <div class="container" style="padding-top: 50px; background-color:#FFFFFF; padding:10px; margin:auto" >
            <div>
            </div>
            <form  method="post">
                <h3>Seleccione las fechas de la consulta.</h3>

                <br />
                <div class="row">
                    <h4 class="col-sm-1">Del</h4>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="f1" name="f1" />
                    </div>
                    <h4 class="col-sm-2 text-center ">al</h4>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="f2" name="f2" />
                    </div>
                    <div class="col-sm-3">
                        <button type="button" class="btn btn-block btn-success" value="Por Fechas" name="fechas" id="fechas">
                            <i class="glyphicon glyphicon-search"></i> Por Fechas
                        </button>
                    </div>

                </div>
            </form>
            <hr/>

        </div>

        <div class="container-fluid"> 
            <div class="panel panel-success" id="tablaNivel" >
                <div class="panel-heading">

                </div>
                <div class="panel-body" id="tableTot" >

                    <div id="container">

                        <div id="dynamic"></div>

                    </div>
                    <br/>

                </div>
            </div>
            <br/>
        </div>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="text-center" id="imagenCarga">
                            <img src="../imagenes/ajax-loader-1.gif" alt="" />
                        </div>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
        <script>
            var context = "${pageContext.servletContext.contextPath}";
        </script>
        <script src="../js/jquery-2.1.4.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery.maskedinput.min.js"></script>
        <script src="../js/jquery-ui.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/dataTables.tableTools.js"></script>
        <script src="../js/hl7manuales/Manuales.js"></script>
        <script src="../js/js_exportar/jquery.dataTables.min.js"></script>
        <script src="../js/js_exportar/dataTables.buttons.min.js"></script>
        <script src="../js/js_exportar/buttons.flash.min.js"></script>
        <script src="../js/js_exportar/jszip.min.js"></script>
        <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
        <script src="../js/js_exportar/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
        <script src="../js/js_exportar/buttons.print.min.js"></script>

        <!--script src="../js/js_exportar/jquery-1.12.4.js"></script-->       




    </body>