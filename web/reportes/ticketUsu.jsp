<%-- 
    Document   : ticketUsu
    Created on : 20/12/2015, 03:36:34 PM
    Author     : anonimus
--%>

<%@page import="net.sf.jasperreports.engine.JasperRunManager"%>
<%@page import="net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.JRExporterParameter"%>
<%@page import="net.sf.jasperreports.engine.export.JRPrintServiceExporter"%>
<%@page import="net.sf.jasperreports.engine.JasperPrint"%>
<%@page import="net.sf.jasperreports.engine.JasperFillManager"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.io.File"%>
<%@page import="javax.print.attribute.standard.Copies"%>
<%@page import="javax.print.attribute.standard.MediaSizeName"%>
<%@page import="javax.print.attribute.standard.MediaSize"%>
<%@page import="javax.print.attribute.standard.MediaPrintableArea"%>
<%@page import="javax.print.attribute.PrintRequestAttributeSet"%>
<%@page import="javax.print.attribute.HashPrintRequestAttributeSet"%>
<%@page import="javax.print.PrintServiceLookup"%>
<%@page import="javax.print.PrintService"%>
<%@page import="java.sql.Connection"%>
<%@page import="Clases.ConectionDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

    String nombre = "", ap = "", am = "", unidad = "", Tipo = "", usu = "", pass = "";
    String pag = "";
    try {
        nombre = request.getParameter("nom");
        ap = request.getParameter("ap");
        am = request.getParameter("am");
        unidad = request.getParameter("uni");
        usu = request.getParameter("usu");
        pass = request.getParameter("pass");
        nombre = nombre + " " + ap + " " + am;
    } catch (Exception e) {
        e.printStackTrace();
    }


%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%            ConectionDB con = new ConectionDB();
            Connection conn;
            con.conectar();
            conn = con.getConn();

            PrintRequestAttributeSet printRequestAttributeSet = new HashPrintRequestAttributeSet();
            //MediaSizeName mediaSizeName = MediaSize.findMedia(4, 4, MediaPrintableArea.INCH);
            //printRequestAttributeSet.add(mediaSizeName);
            printRequestAttributeSet.add(new Copies(2));
            File reporticket = null;
            try {
                reporticket = new File(application.getRealPath("/reportes/TicketUsuarios.jasper"));
            } catch (Exception ex) {
                System.out.println("ErrorRutaUS->" + ex);
                //reporticket = new File("/home/linux9/NetBeansProjects/SCR_MEDALFA/SCR_MEDALFA/web/reportes/RecetaTicket.jasper");
            }
            Map parameterticket = new HashMap();
            parameterticket.put("NomUsu", nombre);
            parameterticket.put("cu", unidad);
            parameterticket.put("user", usu);
            parameterticket.put("pass", pass);

            byte[] bytes = JasperRunManager.runReportToPdf(reporticket.getPath(), parameterticket, conn);
            /*Indicamos que la respuesta va a ser en formato PDF*/
            response.setContentType("application/pdf");
            response.setContentLength(bytes.length);
            ServletOutputStream ouputStream = response.getOutputStream();
            ouputStream.write(bytes, 0, bytes.length);
            /*Limpiamos y cerramos flujos de salida*/

            ouputStream.flush();
            ouputStream.close();

                //JasperPrintManager.printReport(jasperPrintticket, false);
%>
        <script type="text/javascript">
            var ventana = window.self;
            ventana.opener = window.self;
            setTimeout("window.close()", 50);
        </script>


    </body>
</html>
