<%-- 
    Document   : Reporte
    Created on : 26/12/2012, 09:05:24 AM
    Author     : Unknown
--%>

<%@page import="Clases.ConectionDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="net.sf.jasperreports.engine.*" %> 
<%@ page import="java.util.*" %> 
<%@ page import="java.io.*" %> 
<%@ page import="java.sql.*" %> 
<% /*Parametros para realizar la conexión*/

    HttpSession sesion = request.getSession();
    if (sesion.getAttribute("nombre") != null) {
    } else {
        //response.sendRedirect("index.jsp");
    }
    ConectionDB con = new ConectionDB();
    Connection conexion;
    con.conectar();
    conexion = con.getConn();

    /*Establecemos la ruta del reporte*/
    File reportFile = new File(application.getRealPath("/reportes/transferenciaTraspaso.jasper"));
    /* No enviamos parámetros porque nuestro reporte no los necesita asi que escriba 
     cualquier cadena de texto ya que solo seguiremos el formato del método runReportToPdf*/
    Map parameters = new HashMap();
    parameters.put("fec_ini", request.getParameter("fec_ini")+" 00:00:00");
    parameters.put("fec_fin", request.getParameter("fec_fin")+" 23:59:59");
    parameters.put("cla_uni", request.getParameter("cla_uni"));
    parameters.put("des_uni", sesion.getAttribute("des_uni"));
    /*Enviamos la ruta del reporte, los parámetros y la conexión(objeto Connection)*/
    byte[] bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), parameters, conexion);
    /*Indicamos que la respuesta va a ser en formato PDF*/ response.setContentType("application/pdf");
    response.setContentLength(bytes.length);
    ServletOutputStream ouputStream = response.getOutputStream();
    ouputStream.write(bytes, 0, bytes.length); /*Limpiamos y cerramos flujos de salida*/ ouputStream.flush();
    ouputStream.close();
    conexion.close();
    con.cierraConexion();
%>
