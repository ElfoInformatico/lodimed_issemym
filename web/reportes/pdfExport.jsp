<%-- 
    Document   : pdfExport
    Created on : 10/02/2016, 10:11:48 AM
    Author     : anonimus
--%>

<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.Map"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="net.sf.jasperreports.engine.JasperRunManager"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.io.File"%>
<%@page import="java.sql.Connection"%>
<%@page import="Clases.ConectionDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    HttpSession sesion = request.getSession(true);

    String nombre = request.getParameter("nom");
    String claUni = "";
    String nomUni = "";
    claUni = (String) sesion.getAttribute("cla_uni");
    ConectionDB con = new ConectionDB();

    con.conectar();
    ResultSet rs = con.consulta("SELECT des_uni FROM unidades WHERE cla_uni='" + claUni + "';");
    while (rs.next()) {
        nomUni = rs.getString(1);
    }

    Connection conn;
    conn = con.getConn();

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%            try {

                File reportFile = null;
                try {

                    reportFile = new File(application.getRealPath("/reportes/abastoPdf.jasper"));
                } catch (Exception ex) {
                    System.out.println("ErrorRutaCOL->" + ex);
                    //reporticket = new File("/home/linux9/NetBeansProjects/SCR_MEDALFA/SCR_MEDALFA/web/reportes/RecetaTicket.jasper");
                }
                Map parameterticket = new HashMap();
                parameterticket.put("nombre", nombre);
                parameterticket.put("unidad", nomUni);
                byte[] bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), parameterticket, conn);
                /*Indicamos que la respuesta va a ser en formato PDF*/
                response.setContentType("application/pdf");
                response.setContentLength(bytes.length);
                ServletOutputStream ouputStream = response.getOutputStream();
                ouputStream.write(bytes, 0, bytes.length); /*Limpiamos y cerramos flujos de salida*/

                ouputStream.flush();
                ouputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (con.estaConectada()) {
                        con.cierraConexion();
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }


        %>
    </body>
</html>
