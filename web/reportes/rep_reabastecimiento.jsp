<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    HttpSession sesion = request.getSession();
    String id_usu = "";
    try {
        id_usu = (String) session.getAttribute("id_usu");
    } catch (Exception e) {
    }
    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Estilos CSS -->
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/cupertino/jquery-ui-1.10.3.custom.css" />
        <link href="../css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!---->
        <title>Reporte de Reabastecimiento</title>


    </head>
    <body>
        <div class="container" >
            <%@include file="../jspf/mainMenu.jspf"%>
            <br/><br/><br/>
            <h1>Reporte de Reabastecimiento</h1>
            <br/>
            <div class="row">
                <!--h4 class="col-sm-3">Días para su próximo abasto:</h4>
                <div class="col-sm-2">
                    <input type="text" class="form-control" name="dia" id="dia" value=""/> 
                </div>
                <div class="col-sm-3">
                    <input type="button" class="btn btn-success btn-block" id="calcRepo" value="Calcular Reposicion"/>
                </div-->
                <div class="col-sm-2">
                    <button class="btn btn-success" id="enviar" type="button">Enviar Requerimiento</button>
                </div>
                <div class="col-sm-2">
                    <button class="btn btn-default" id="descarga" type="button">Descargar</button>
                </div>
            </div>
            <div class="panel-body" id="tableTot" >

                <div id="container">

                    <div id="dynamic"></div>

                </div>
            </div>

        </div>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="text-center" id="imagenCarga">
                            <img src="../imagenes/ajax-loader-1.gif" alt="" />
                        </div>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
        <script>
            var context = "${pageContext.servletContext.contextPath}";
        </script>
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/reabastecimiento/reabastecimiento.js"></script>
    </body>

</html>
