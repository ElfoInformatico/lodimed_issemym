<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="Clases.ConectionDB"%>
<%@page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" import="java.text.*" import="java.lang.*" import="java.util.*" import= "javax.swing.*" import="java.io.*" import="java.text.DateFormat" import="java.text.ParseException" import="java.text.SimpleDateFormat" import="java.util.Calendar" import="java.util.Date"  import="java.text.NumberFormat" import="java.util.Locale" errorPage="" %>
<%
    HttpSession sesion = request.getSession();
    String id_usu = "";
    id_usu = (String) sesion.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }
    ConectionDB conn = new ConectionDB();

    conn.conectar();
    Connection con = conn.getConn();
    Statement stmt = con.createStatement();
    ResultSet rset = null;
    String pag = "";

    String reporte = request.getParameter("reporte");

    if (reporte == null) {
        reporte = "";
    }

    if (reporte.equals("global")) {
        pag = "validaMesColGlob.jsp?cla_uni=" + request.getParameter("cla_uni") + "&f1=" + request.getParameter("txtf_caduc") + "&f2=" + request.getParameter("txtf_caduci") + "&ori=" + request.getParameter("ori") + "";
    }
    if (reporte.equals("receta")) {
        pag = "validaMesColFarm.jsp?cla_uni=" + request.getParameter("cla_uni") + "&f1=" + request.getParameter("txtf_caduc") + "&f2=" + request.getParameter("txtf_caduci") + "&ori=" + request.getParameter("ori") + "";
    }

    String cla_uni = "", des_uni = "";

    try {
        rset = stmt.executeQuery("select un.des_uni, us.cla_uni from usuarios us, unidades un where us.cla_uni = un.cla_uni and id_usu = '" + id_usu + "' ");
        while (rset.next()) {
            cla_uni = rset.getString("cla_uni");
            des_uni = rset.getString("des_uni");

        }
    } catch (SQLException ex) {
        Logger.getLogger("validaMesCol").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getMessage()), ex);
    }
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <!-- DW6 -->
    <head>
        <!-- Copyright 2005 Macromedia, Inc. All rights reserved. -->
        <title>Sistema de Reportes WEB :: GNKL</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <link href="../css/bootstrap.css" rel="stylesheet" media="screen"/>
        <link href="../css/topPadding.css" rel="stylesheet"/>
        <link href="../css/datepicker3.css" rel="stylesheet"/>
        <link href="../css/dataTables.bootstrap.css" rel="stylesheet"/>
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

        <script language="JavaScript" type="text/javascript">
            //--------------- LOCALIZEABLE GLOBALS ---------------
            var d = new Date();
            var monthname = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Deciembre");
            //Ensure correct for language. English is "January 1, 2004"
            var TODAY = monthname[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear();
            //---------------   END LOCALIZEABLE   ---------------
        </script>
    </head>
    <body>
        <%@include file="../jspf/mainMenu.jspf"%>
        <div class="container-fluid " >
            <h2 class="text-center" > REPORTE DE VALIDACIONES MENSUAL RECETA COLECTIVA  </h2>
            <div class="row" >
                <div class="col-md-4 col-md-offset-4  text-center " > <img src="../imagenes/gobierno.png" width="115"/> </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center" ><p></p> <script language="JavaScript" type="text/javascript">document.write(TODAY);</script></div>
            </div>

            <p></p>

            <div class="row" >
                <div class="col-md-8 col-md-offset-2" >
                    <div class="panel panel-default" >
                        <div class="panel-body" >
                            <form action="validaMesCol.jsp" method="get" class="form-horizontal" >
                                <div class="hidden">
                                    <input type="text" name="txtf_hf" id="txtf_hf" size="10" readonly="true"/>
                                </div>

                                <div class="form-group " >  
                                    <div class="col-md-12" >
                                        <label class="checkbox-inline h4 " > Global Receta Colectivo: <input name="reporte" id="rec" type="radio" value="receta" />  </label>
                                        <label class="checkbox-inline h4 " > Desglose  Receta Colectivo: <input name="reporte" id="glo" type="radio" value="global" checked />   </label>
                                    </div>    
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12" >
                                        <div class="form-group row" >
                                            <label for="cla_uni" class=" h4 col-md-2 control-label">Unidad:</label>
                                            <div class="col-md-5" >
                                                <select name="cla_uni" class="form-control">
                                                    <%                                                        try {
                                                            rset = stmt.executeQuery("select DISTINCT cla_uni, uni from recetas");
                                                            while (rset.next()) {
                                                    %>
                                                    <option value="<%=rset.getString("cla_uni")%>"
                                                            <%
                                                                if (cla_uni.equals(rset.getString("cla_uni"))) {
                                                                    out.println("selected");
                                                                }
                                                            %>
                                                            ><%=rset.getString("uni")%></option>
                                                    <%
                                                            }
                                                        } catch (SQLException ex) {
                                                            Logger.getLogger("validaMesRec").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getMessage()), ex);
                                                        }
                                                    %>
                                                </select>
                                            </div>
                                            <input  name="ori" hidden value=""/>

                                            <label for="ori" class=" h4 col-md-2 control-label">Origen:</label>
                                            <div class="col-md-3">
                                                <select name="ori" class="form-control">
                                                    <option value="">Todos</option>         
                                                    <option value="0">0</option>                                                                ontrol">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span class="col-md-3 h4 control-label" >Rango de fechas:</span>
                                    <div class="col-md-9" >
                                        <div class="form-group row">

                                            <label for="txtf_caduc" class=" h4 col-md-2 control-label">del:</label>
                                            <div class="col-md-3">
                                                <input name="txtf_caduc" type="text" id="txtf_caduc" size="10" readonly data-date-format="yyyy/mm/dd" title="AAAA-MM-DD" class="form-control"/>
                                            </div>

                                            <label for="txtf_caduci" class=" h4 col-md-2 control-label">al:</label>
                                            <div class="col-md-3">
                                                <input name="txtf_caduci" type="text" id="txtf_caduci" size="10" readonly data-date-format="yyyy/mm/dd" title="AAAA-MM-DD" class="form-control"/>
                                            </div>

                                            <input type="hidden" name="cmd" value="1" />
                                            <div class="col-md-2">
                                                <input type="submit" name="Submit" value="Buscar" class="btn-success btn-sm"/>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <iframe src="<%=pag%>" width="1550" height="800"></iframe>         
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery-ui.js"></script>
        <script src="../js/bootstrap-datepicker.js"></script>
        <script>
                    $("#txtf_caduc").datepicker({minDate: 0});
                    $("#txtf_caduci").datepicker({minDate: 0});
        </script>
    </body>
</html>
<%
    con.close();
%>