<%-- 
    Document   : Reporte
    Created on : 26/12/2012, 09:05:24 AM
    Author     : Unknown
--%>

<%@page import="Clases.ConectionDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="net.sf.jasperreports.engine.*" %> 
<%@ page import="java.util.*" %> 
<%@ page import="java.io.*" %> 
<%@ page import="java.sql.*" %> 
<% /*Parametros para realizar la conexión*/

    HttpSession sesion = request.getSession();
    if (sesion.getAttribute("nombre") != null) {
    } else {
        //response.sendRedirect("index.jsp");
    }
    ConectionDB con = new ConectionDB();
    Connection conexion;
    con.conectar();
    conexion = con.getConn();
    File reportFile = null;
    try {
        /*Establecemos la ruta del reporte*/
        Map parameters = new HashMap();
        String tipoReporte = request.getParameter("tipoRural");
        String tipo_receta = "0", unidad = request.getParameter("unidad");
        switch (tipoReporte) {
        case "rural":
            reportFile = new File(application.getRealPath("/reportes/reporte_facturacion_rural.jasper"));
            tipo_receta = "1";
            break;
        case "global":
            reportFile = new File(application.getRealPath("/reportes/reporte_facturacion_global_rural.jasper"));
            tipo_receta = "1";
            break;
        }
        parameters.put("tipoReporte", request.getParameter("tipoReporte"));
        parameters.put("fecha_inicial", request.getParameter("fechaInicial"));
        parameters.put("fecha_final", request.getParameter("fechaFinal"));
        parameters.put("clasificacion", request.getParameter("clasificacion"));
        parameters.put("cobertura", request.getParameter("cobertura"));
        parameters.put("remision", request.getParameter("remision"));
        parameters.put("tipo_producto", request.getParameter("tipo_producto"));
        parameters.put("folio", request.getParameter("folio"));
        parameters.put("unidad", unidad);
        parameters.put("jurisdiccion", request.getParameter("jurisdiccion"));
        
        String anexo = "1.1.2";
        if (request.getParameter("tipo_producto").equals("MEDICAMENTO")){
            anexo = "1.1.1";
        }
        parameters.put("anexo", anexo);
        //parameters.put("tipo_receta", Integer.parseInt(tipo_receta));
        /*Enviamos la ruta del reporte, los parámetros y la conexión(objeto Connection)*/
        byte[] bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), parameters, conexion);
        /*Indicamos que la respuesta va a ser en formato PDF*/ response.setContentType("application/pdf");
        response.setContentLength(bytes.length);
        ServletOutputStream ouputStream = response.getOutputStream();
        ouputStream.write(bytes, 0, bytes.length);
        /*Limpiamos y cerramos flujos de salida*/ ouputStream.flush();
        ouputStream.close();
        conexion.close();
        con.cierraConexion();

    } catch (Exception e) {
        System.out.println(e);
    }

%>
