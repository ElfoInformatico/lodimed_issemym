<%-- 
    Document   : index
    Created on : 07-mar-2014, 15:38:43
    Author     : Americo
--%>

<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.sql.SQLException"%>
<%@page import="Servlets.EditaAbasto"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    String nombre = request.getParameter("nom");
    nombre = nombre.replace(".csv", "");
    response.setHeader("Content-Disposition", "attachment; filename=Reporte abasto-" + nombre + ".xls");
    ConectionDB con = new ConectionDB();
    Date ahora = new Date();
    PreparedStatement ps;
    ResultSet rs;
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../css/bootstrap.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <title>LODIMED</title>
    </head>
    <body>            

        <h3>Detalle Abasto-<%=request.getParameter("nom")%></h3>
        <h4>Fecha: <%=ahora%></h4>

        <table id="tbDetalleAba">
            <thead>
                <tr>
                    <th>Clave</th>
                    <th>Lote</th>
                    <th>Caducidad</th>
                    <th>Cantidad</th>
                    <th>Origen</th>
                    <th>Observaciones</th>
                </tr>
            </thead>
            <tbody>
                <%
                    try {
                        String observacion;
                        con.conectar();
                        ps = con.getConn().prepareStatement(EditaAbasto.SELECT_DETALLE);
                        ps.setString(1, request.getParameter("nom"));
                        rs = ps.executeQuery();
                        if (!rs.isBeforeFirst()) {
                            observacion = "No se pudo obtener el detalle del abasto.";
                            throw new SQLException(observacion, ps.toString());
                        }

                        while (rs.next()) {
                            String estilo = (rs.getInt(7) == 0) ? "background-color: red; color: white" : "";


                %>
                <tr style="<%=estilo%>" >
                    <td><%= rs.getString(1)%></td>
                    <td ><%= rs.getString(2)%></td>
                    <td><%= rs.getString(3)%></td>
                    <td><%= rs.getString(4)%></td>
                    <td><%= rs.getString(5)%></td>
                    <td><%= rs.getString(6)%></td>
                </tr>
                <%
                        }
                    } catch (SQLException ex) {
                        String mensaje = String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState());
                        Logger.getLogger("reporteAbastoExcel.jsp").log(Level.SEVERE, mensaje, ex);
                    } finally {
                        try {
                            if (con.estaConectada()) {
                                con.cierraConexion();
                            }
                        } catch (SQLException ex) {
                            Logger.getLogger("reporteAbastoExcel.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                        }
                    }
                %>
            </tbody>
        </table>                            
    </div>
</div>
</div>
</div>                
</div>
<br/>
</body>
</html>


