<%-- 
    Document   : xlsReportes
    Created on : 28/04/2018, 12:53:14 PM
    Author     : Americo
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    HttpSession sesion = request.getSession();
    String id_usu = "";
    id_usu = (String) session.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }

    String folio = request.getParameter("folio");
    ConectionDB con = new ConectionDB();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reporte XLS</title>
    </head>
    <body>
        <%
            String exportToExcel = request.getParameter("exportToExcel");
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "inline; filename=Folio_"
                      + folio + ".xls");
        %>
        <table >
            <tr>
                <td>CONTRATO</td>
                <td>FECHA DE EMISIÓN</td>
                <td>NO DE REMISIÓN</td>
                <td>MES</td>
                <td>FECHA DE FACTURA</td>
                <td>NO FACTURA</td>
                <td>IMPORTE FACTURA</td>
                <td>NOMBRE DEL PROVEEDOR</td>
                <td>DESCRIPCION DEL PRODUECTO(MEDICAMENTO)</td>
                <td>PRESENTACIÓN</td>
                <td>CLAVE  (11 DIGITOS)</td>
                <td>PRECIO UNITARIO</td>
                <td>CANTIDAD O PIEZAS</td>
                <td>SUBTOTAL</td>
                <td>IVA</td>
                <td>SUMINISTRO</td>
                <td>ANEXO</td>
                <td>UNIDAD O NOMBRE DEL CLIENTE</td>
                <td>FURISDICCION</td>
                <td>PACIENTE</td>
                <td>NO DE POLIZA</td>
                <td>NO DE RECETA</td>
            </tr>
            <%
                String fecha_inicial = request.getParameter("fecha_inicial");
                String fecha_final = request.getParameter("fecha_final");
                con.conectar();
                ResultSet rset = con.consulta(
                          "SELECT   'SSM-LPN-006/2018-01' AS contrato,   DATE_FORMAT(NOW(), '%d/%m/%Y') AS fecha_emi,   'Marzo 2018' AS mes,   DATE_FORMAT(r.Fecha, '%d/%m/%Y') AS fecha_factura,   'MEDALFA S.A. de C.V.' AS proveedor,   r.Description AS descripcion,   r.CLAVE_CB AS clave,   r.PRECIO_UNITARIO AS precio_unitario,   IFNULL(SUM(r.Cant_Sur), 0) AS surtido,   IFNULL(      IF (     r.COBERTURA = 'SEGURO POPULAR',     0,     r.PRECIO_UNITARIO * r.Cant_Sur    ),    0   ) AS importe,   IFNULL(      IF (     r.COBERTURA = 'SEGURO POPULAR',     0,      IF (     r.SUMINISTRO = 'MATERIAL DE CURACION',     r.PRECIO_UNITARIO * r.Cant_Sur * 0.16,     0    )    ),    0   ) AS iva,   r.SUMINISTRO AS suministro,   r.COBERTURA AS ANEXO,   DATE_FORMAT(r.Caducidad, '%d/%m/%Y') AS caducidad,   r.UNIDAD,   r.JURISDICCION,      r.Folio AS folio  FROM  reporte_rurales r WHERE Fecha BETWEEN '"+fecha_inicial+"' AND '"+fecha_final+"' AND r.Folio='"+folio+"' GROUP BY  r.UNIDAD, r.SUMINISTRO, r.COBERTURA, r.CLAVE_CB, r.Lote, r.Caducidad  ORDER BY   r.SUMINISTRO,   r.COBERTURA,   r.Folio,  r.CLAVE_CB;");
                while (rset.next()) {
            %>
            <tr>
                <td><%=rset.getString(1)%></td>
                <td><%=rset.getString("fecha_factura")%></td>
                <td></td>
                <td><%=rset.getString(3)%></td>
                <td></td>
                <td></td>
                <td></td>
                <td><%=rset.getString(5)%></td>
                <td><%=rset.getString(6)%></td>
                <td></td>
                <td><%=rset.getString(7)%></td>
                <td><%=rset.getString(8)%></td>
                <td><%=rset.getString(9)%></td>
                <td><%=rset.getString(10)%></td>
                <td><%=rset.getString(11)%></td>
                <td><%=rset.getString(12)%></td>
                <td><%=rset.getString(13)%></td>
                <td><%=rset.getString("UNIDAD")%></td>
                <td><%=rset.getString("JURISDICCION")%></td>
                <td></td>
                <td></td>
                <td><%=rset.getString("folio")%></td>
            </tr>
            <%
                }
                con.cierraConexion();
            %>
        </table>
    </body>
    <script>
        window.close();
    </script>
</html>
