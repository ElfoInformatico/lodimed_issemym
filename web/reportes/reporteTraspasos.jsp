<%-- 
    Document   : reporteTraspasos
    Created on : 11-abr-2018, 12:33:39
    Author     : americoguzman
--%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="Servlets.Catalogo"%>
<%@page import="Servlets.movientos.MovimentosProductos"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>
<%
    HttpSession sesion = request.getSession();

    String id_usu = (String) session.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }
    ConectionDB con = new ConectionDB();
    con.conectar();
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Estilos CSS -->
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/cupertino/jquery-ui-1.10.3.custom.css" />
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/sweetalert.css" rel="stylesheet" type="text/css"/>
        <script>
            var DES_UNI ="<%=sesion.getAttribute("des_uni")%>";
        </script>
        <!---->
        <title>Ajustes por Traspasos</title>
    </head>
    <body>

        <%@include file="../jspf/mainMenu.jspf" %>%>
        <div class="container" style="padding-top: 50px">
            <h1>Salidas por Traspaso (-)</h1>
            <hr/>
            <form action="../reportes/csvTransferencia.jsp" method="get">
                <div class="row">
                    <h4 class="col-sm-2">
                        Unidad Destino:
                    </h4>
                    <h4 class="col-sm-5">
                        <select class="form-control" name="listaUnidades" id="listaUnidades" data-toggle="tooltip" title="Lista de Unidades" required>
                            <option value="">--Unidad--</option>
                            <%
                                try {
                                    con.conectar();
                                    ResultSet rset = con.consulta("select cla_uni, des_uni from unidades");
                                    while (rset.next()) {
                            %>
                            <option value="<%=rset.getString("cla_uni")%>"><%=rset.getString("des_uni")%></option>
                            <%
                                    }
                                } catch (SQLException ex) {
                                    Logger.getLogger("receta_colectiva.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.
                                            getSQLState()), ex);
                                } finally {
                                    try {
                                        if (con.estaConectada()) {
                                            con.cierraConexion();
                                        }
                                    } catch (SQLException ex) {
                                        Logger.getLogger("receta_colectiva.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.
                                                getSQLState()), ex);
                                    }
                                }
                            %>
                        </select>
                    </h4>

                </div>
                <hr>
                <div class="row">
                    <label class="control-label col-lg-2" for="hora_ini">Fecha Inicio:</label>
                    <div class="col-lg-2">
                        <input class="form-control" id="hora_ini" data-date-format="yyyy/mm/dd" name="hora_ini" />
                    </div>
                    <label class="control-label col-lg-2"  for="hora_fin">Fecha Fin:</label>
                    <div class="col-lg-2">
                        <input class="form-control" data-date-format="yyyy/mm/dd" id="hora_fin" name="hora_fin" />
                    </div>                        
                    <div class="hidden">
                        <label>
                            <input type="checkbox" name="receta" value="1" checked> Por Receta?
                        </label>
                    </div>
                    <div class="col-lg-2">
                        <button class="btn btn-success btn-block" id="generarReporte" type="submit"  >CSV</button>
                        <button class="btn btn-success btn-block" type="button" onclick="generarReportePDF()">PDF</button>
                    </div>

                </div>
                <hr/>


            </form>
    </body>
    <script src="../js/jquery-2.1.4.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/jquery.dataTables.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/dataTables.tableTools.js"></script>
    <script src="../js/jquery.maskedinput.min.js" type="text/javascript"></script>

    <script src="../js/movimientos/reportesTraspasos.js" type="text/javascript"></script>

</html>
