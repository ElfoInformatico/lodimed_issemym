<%-- 
    Document   : movClaves
    Created on : 24/04/2015, 10:36:45 AM
    Author     : Americo
--%>

<%
    HttpSession sesion = request.getSession();
    String id_usu = "";
    id_usu = (String) session.getAttribute("id_usu");

    if (id_usu == null) 
    {
        response.sendRedirect("../index.jsp");
        return;
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Estilos CSS -->
        <link href="../css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../css/topPadding.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

        <!---->
        <title>Reporte de movimiento de claves</title>
    </head>
    <body>
        <%@include file="../jspf/mainMenu.jspf" %>
        <br/>
        <div class="container" style="padding-top: 50px">
            <h1>Reporte de movimiento de claves</h1>
        </div>
        <div class="container">
            <div class="col-sm-1 col-sm-offset-11">
                <a class="btn btn-block btn-success" href="gnrMovClaves.jsp"><span class="glyphicon glyphicon-download-alt"></span></a>
            </div>
            <h3>Alto Movimiento</h3>
            <table id="tablaAlto" class="table table-responsive table-striped dataTable table-bordered">
                <thead>
                    <tr>
                        <td>Clave</td>
                        <td>Descripción</td>
                        <td>Cantidad</td>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
            <br>
            <h3>Bajo Movimiento</h3>
            <table id="tablaBajo" class="table table-responsive table-striped dataTable table-bordered">
                <thead>
                    <tr>                
                        <td>Clave</td>
                        <td>Descripción</td>
                        <td>Cantidad</td>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
            <br>
            <h3>Nulo Movimiento</h3>
            <table id="tablaNulo" class="table table-bordered table-condensed table-hover">
                <thead>
                    <tr>
                        <th>Clave</th>
                        <th>Descripción</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery-ui.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $.ajax({
                    type: "GET",
                    url: "../ReporteMovClaves?accion=alto",
                    dataType: "json",
                    success: function (data) {
                        var table = $("#tablaAlto");
                        $.each(data, function (idx, elem) {
                            table.append("<tr><td>" + elem.clave + "</td><td>" + elem.descripcion + "</td>   <td>" + elem.cantidad + "</td></tr>");
                        });
                        $('#tablaAlto').dataTable({
                            "order": [[2, "desc"]]
                        });
                    }
                });
                $.ajax({
                    type: "GET",
                    url: "../ReporteMovClaves?accion=bajo",
                    dataType: "json",
                    success: function (data) {
                        var table = $("#tablaBajo");
                        $.each(data, function (idx, elem) {
                            table.append("<tr><td>" + elem.clave + "</td><td>" + elem.descripcion + "</td>   <td>" + elem.cantidad + "</td></tr>");
                        });
                        $('#tablaBajo').dataTable({
                            "order": [[2, "asc"]]
                        });
                    }
                });
                $.ajax({
                    type: "GET",
                    url: "../ReporteMovClaves?accion=nulo",
                    dataType: "json",
                    success: function (data) {
                        var table = $("#tablaNulo");
                        $.each(data, function (idx, elem) {
                            table.append("<tr><td>" + elem.clave + "</td><td>" + elem.descripcion + "</td></tr>");
                        });
                        $('#tablaNulo').dataTable();
                    }
                });
            });

            function cambiaLoteCadu(elemento) {
                var indice = elemento.selectedIndex;
                document.getElementById('Cadu').selectedIndex = indice;
            }
        </script>
    </body>
</html>
