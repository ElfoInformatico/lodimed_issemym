<%@page import="Clases.ConectionDB"%><%@page import="java.sql.ResultSet"%><%
    ConectionDB con = new ConectionDB();
    con.conectar();
    response.setHeader("Content-Type", "text/csv; charset=UTF-8");
    response.setContentType("text/csv; charset=UTF-8");
    response.setHeader("Content-Disposition", "inline; filename=Reporte.csv");
    String outputData = "";
    try {
        con.conectar();
        ResultSet rset = con.consulta(
                  "SELECT k.id_kardex, dp.cla_pro,p.des_pro,dp.lot_pro, DATE_FORMAT(dp.cad_pro, '%d/%m/%Y') as cad_pro,  k.cant, dp.id_ori, SUBSTR(k.obser, 1, 4)AS uni_ori, SUBSTR(k.obser, 8, 4)AS uni_des, SUBSTR(k.obser, 15)AS observaciones, cb.F_Cb as cb"
                  + " FROM kardex k, detalle_productos dp, productos p, tb_codigob cb"
                  + " WHERE cb.F_Clave=dp.cla_pro and cb.F_Lote = dp.lot_pro and k.det_pro = dp.det_pro AND dp.cla_pro = p.cla_pro AND k.tipo_mov = 'SALIDA POR AJUSTE' AND k.fecha BETWEEN '"
                  + request.getParameter("hora_ini") + " 00:00:00' AND '" + request.getParameter("hora_fin") + " 23:59:59' AND SUBSTR(k.obser, 8, 4)= '"
                  + request.getParameter("listaUnidades") + "' "
                  + " group by k.id_kardex ORDER BY k.id_kardex;");
        while (rset.next()) {
            outputData = outputData + rset.getString(2) + ",-," + rset.getString(4) + "," + rset.getString(5) + "," + rset.getString(6) + "," + rset.
                      getString(7) + "," + rset.getString(11) + "\n";
        }
        con.cierraConexion();
    } catch (Exception e) {
    }
    out.print(outputData);
%>