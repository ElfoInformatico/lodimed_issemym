<%-- 
    Document   : alta_pacientes
    Created on : 10-mar-2014, 9:14:09
    Author     : Americo
--%>

<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    /**
     * alta de pacientes
     */
    HttpSession sesion = request.getSession();
    String id_usu = "";
    id_usu = (String) session.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../../index.jsp");
        return;
    }

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="../../css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../../css/pie-pagina.css" rel="stylesheet" media="screen">
        <link href="../../css/topPadding.css" rel="stylesheet">
        <link href="../../css/font-awesome.min.css" rel="stylesheet">
        <link href="../../css/cupertino/jquery-ui-1.10.3.custom.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <title>Alta de Pacientes</title>
    </head>
    <body>
        <%@include file="../../jspf/mainMenu.jspf"%>
        <br/>
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10">
                        <h3>Alta de Pacientes</h3>
                    </div>
                    <div class="col-lg-2">
                        <!--a class="btn btn-block btn-danger" href="pacientes.jsp">Regresar</a-->
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form method="post">
                            <div class="row">
                                <label for="tip_cob" class="col-sm-2 control-label">Tipo de Cobranza</label>
                                <div class="col-sm-2">
                                    <select class="form-control" id="tip_cob" name="tip_cob" onchange="numAfiPA(this);">
                                        <!--Tipo de cobranza de los pacientes-->
                                        <option>SP</option>
                                        <option>PR</option>
                                        <option>PA</option>
                                        <option value="60">Mas de 60</option>
                                        <option value="IRA">Infecc. Resp Aguda</option>
                                        <option>EDA</option>
                                        <option value="AR">Antirrabico</option>
                                        <option value="SE">Seguro Escolar</option>
                                        <option value="IND">Indigente</option>
                                        <option value="PN">Prenatal</option>
                                    </select>
                                </div>
                                <label for="no_afi" class="col-sm-2 control-label">Número de Afiliación</label>
                                <div class="col-sm-5">
                                    <!-- Número de afiliacion -->
                                    <input type="text" class="form-control" id="no_afi" name="no_afi" data-behavior = "only-alphanum" placeholder=""  value="" required/>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1">
                                    <button class="btn btn-danger btn-block" title="Cerrar ventana" type="button" id="closeWindow" ><span class="glyphicon glyphicon-remove" ></span></button>
                                </div>
                            </div>

                            <br />
                            <div class="row">
                                <label for="num_afil" class="col-sm-2 control-label">Número de Afiliados</label>
                                <div class="col-sm-2">
                                    <!-- Número de afiliados -->
                                    <input type="number" name="numAfiliados" id="numAfiliados" class="form-control" required  data-behavior = "only-num" max="20" min="1"/>
                                </div>
                                <div class="col-sm-2">
                                    <button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span></button>
                                </div>
                            </div>
                            <hr/>

                        </form>
                        <form class="form-horizontal" role="form" name="formulario_pacientes" id="formulario_pacientes" method="get" action="../../Pacientes">
                            <%                                int numAfiliados = 0;

                                String afiliados = request.getParameter("numAfiliados");
                                afiliados = (afiliados == null) ? "0" : afiliados;
                                numAfiliados = Integer.parseInt(afiliados);

                                if (numAfiliados > 0) {
                                    /**
                                     * Muesta los datos de los afiliados
                                     */
                            %>
                            <div class="row">
                                <label for="no_afi" class="col-sm-2 control-label">Número de Afiliación</label>
                                <div class="col-sm-4">
                                    <input id="no_afi1" name="no_afi" value="<%=request.getParameter("no_afi")%>" readonly class="form-control" data-behavior = "only-num"/>
                                </div>

                                <label for="tip_cob" class="col-sm-2 control-label">Tipo de Cobranza</label>
                                <div class="col-sm-4">
                                    <input id="tip_cob1" name="tip_cob1" value="<%=request.getParameter("tip_cob")%>" readonly class="form-control" />
                                </div>
                            </div>
                            <br/>
                            <hr/>

                            <br/>
                            <div class="row" id="afiliados">
                                <label for="slctPacientes" class="col-sm-3 control-label">Afiliados a este número de afiliación</label>
                                <div class="col-lg-6 col-md-6 col-sm-6" >
                                    <select class="form-control " id="slctPacientes"></select>
                                </div>
                            </div>
                            <br/>
                            <div class="row" id="vigencia" >
                                <label for="fecIni" class="col-sm-2 control-label">Inicio de vigencia</label>
                                <div class="col-sm-2">
                                    <input class="form-control" id="fecIni" name="fecIni" type="text" />
                                </div>
                                <label for="fecFin" class="col-sm-2 control-label">Fin de vigencia</label>
                                <div class="col-sm-2">
                                    <input class="form-control" id="fecFin" name="fecFin" type="text"/>
                                </div>
                            </div> 

                            <input name="numeroAfiliados" value="<%=numAfiliados%>" class="hidden">
                            <hr/>
                            <%
                                }
                                for (int i = 1; i <= numAfiliados; i++) {
                                    /**
                                     * Número de afiliados, captura de cada uno
                                     * de ellos
                                     */
                            %>
                            <h3>Afiliado <%=i%></h3>
                            <div class="row">
                                <label for="ape_pat" class="col-sm-2 control-label">Apellido Paterno</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="<%=i%>ape_pat" name="<%=i%>ape_pat" placeholder="" required  value="" data-behavior = "only-alpha"/>
                                </div>
                                <label for="ape_mat" class="col-sm-2 control-label">Apellido Materno</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="<%=i%>ape_mat" name="<%=i%>ape_mat" placeholder="" required value="" data-behavior = "only-alpha"/>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <label for="nombre" class="col-sm-2 control-label">Nombre(s)</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="<%=i%>nombre" name="<%=i%>nombre" placeholder="" required value="" data-behavior = "only-alpha"/>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <label for="fec_nac" class="col-sm-2 control-label">Fecha de Nacimiento</label>

                                <div class="col-sm-2">
                                    <input class="form-control fec" id="<%=i%>fec_nac" name="<%=i%>fec_nac" type="text" required/>
                                </div>
                                <label for="sexo" class="col-sm-1 control-label">Sexo</label>
                                <div class="col-sm-2">
                                    <select class="form-control" id="<%=i%>sexo" name="<%=i%>sexo" required>
                                        <option value = 'M' >MASCULINO</option>
                                        <option value = 'F'>FEMENINO</option>
                                    </select>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <label for="no_afi" class="col-sm-2 control-label">Número de Expediente</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control expediente " id="<%=i%>no_exp" name="<%=i%>no_exp" placeholder="" data-behavior = "only-alphanum"  required  value="" />
                                </div>                                
                            </div>
                            <br/>   

                            <hr />

                            <%
                                }
                                if (numAfiliados > 0) {
                            %>

                            <div class="row">
                                <div class="col-lg-6">
                                    <button class="btn btn-block btn-success" id="Guardar" type="button" onclick="return confirm('Desea agregar al paciente(s)?')">Guardar</button>
                                    <div hidden><button class="active" id="Guardar1" type="submit" ></button></div>
                                    
                                </div>
                                <div class="col-lg-6">
                                    <button class="btn btn-block btn-success" type="button" id="Regresar" >Regresar</button>
                                </div>
                            </div>
                            <%
                                }
                            %>
                        </form>
                    </div>

                </div>

            </div>
        </div>

        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="../../js/jquery-1.9.1.js"></script>
        <script src="../../js/bootstrap.js"></script>
        <script src="../../js/datepickerEspanol.js"></script>  
        <script src="../../js/jquery-ui.js"></script>        
        <script src="../../js/moment.js"></script>
        <script src="../../js/pacientes/pacientes.js"></script>
        <script src="../../js/jquery.alphanum.js" type="text/javascript"></script>
        <script src="../../js/jquery.maskedinput.min.js" type="text/javascript"></script>
        <script>
                                        var context = "${pageContext.servletContext.contextPath}";

                                        $(function ()
                                        {

                                            if ($("#tip_cob1").val() === "SP")
                                            {
                                                $("#vigencia").show();

                                            }
                                            else
                                            {
                                                $("#vigencia").hide();


                                            }

                                            $(".expediente").val("-");

                                            $('.fec').mask("99-99-9999", {placeholder: " "});

                                            $(".fec").datepicker({
                                                dateFormat: 'dd-mm-yy',
                                                changeYear: true,
                                                changeMonth: true,
                                                maxDate: 'today',
                                                yearRange: "1910:2020"
                                            });


                                            $("#Guardar").click(function ()
                                            {

                                                var fi = $("#fecIni").val();
                                                var ff = $("#fecFin").val();
                                                var tc = $("#tip_cob1").val();


                                                if (fi === "" && tc === "SP")
                                                {
                                                    alert("Ingresar una fecha de inicio válida porfavor.");
                                                    return false;
                                                }
                                                else if (ff === "" && tc === "SP")
                                                {
                                                    alert("Ingresar una fecha de fin de vigencia válida porfavor.");
                                                    return false;
                                                }
                                                else
                                                {
                                                    $("#fecIni").removeAttr("disabled");

                                                    $("#fecFin").removeAttr("disabled");

                                                    $("#Guardar1").click();
                                                }

                                            });
                                        });


                                        $("#closeWindow").click(function () {
                                            window.close();
                                        });



                                        function numAfiPA(e) {
                                            var tipo = e.value;
                                            var d = new Date();
                                            //alert(d);
                                            var now = moment();
                                            now.format('dddd');
                                            //alert(now);
                                            //var tipo = "";
                                            if (tipo === "") {

                                            }
                                            if (document.getElementById('tip_cob').value === 'PR') {
                                                tipo = 'PR';
                                                document.getElementById('numAfiliados').value = 1;
                                                document.getElementById('numAfiliados').readOnly = true;
                                            } else if (document.getElementById('tip_cob').value === 'PA') {
                                                tipo = 'PA';
                                                document.getElementById('numAfiliados').value = 0;
                                                document.getElementById('numAfiliados').readOnly = false;
                                            } else if (document.getElementById('tip_cob').value === 'SP') {
                                                tipo = 'SP';
                                                document.getElementById('numAfiliados').value = 0;
                                                document.getElementById('numAfiliados').readOnly = false;
                                            }
                                            /**
                                             * Genera npumero de afiliación para PA
                                             */
                                            document.getElementById('no_afi').value = tipo + now.format('YYMMDDHHmmss');
                                            document.getElementById('no_afi').readOnly = true;
                                            if (document.getElementById('tip_cob').value === 'SP') {
                                                document.getElementById('no_afi').value = "";
                                                document.getElementById('no_afi').readOnly = false;
                                            }
                                            if (document.getElementById('tip_cob').value === '--Seleccione--') {
                                                document.getElementById('no_afi').value = "";
                                            }




                                        }


                                        function tabular(e, obj)
                                        {
                                            /**
                                             * Siguiente objeto al enter
                                             */
                                            tecla = (document.all) ? e.keyCode : e.which;
                                            if (tecla !== 13)
                                                return;
                                            frm = obj.form;
                                            for (i = 0; i < frm.elements.length; i++)
                                                if (frm.elements[i] === obj)
                                                {
                                                    if (i === frm.elements.length - 1)
                                                        i = -1;
                                                    break
                                                }
                                            /*ACA ESTA EL CAMBIO*/
                                            if (frm.elements[i + 1].disabled === true)
                                                tabular(e, frm.elements[i + 1]);
                                            else
                                                frm.elements[i + 1].focus();
                                            return false;
                                        }

                                        otro = 0;
                                        function LP_data() {
                                            var key = window.event.keyCode;//codigo de tecla. 
                                            if (key < 48 || key > 57) {//si no es numero 
                                                window.event.keyCode = 0;//anula la entrada de texto. 
                                            }
                                        }
                                        function anade(esto) {
                                            if (esto.value.length > otro) {
                                                if (esto.value.length === 2) {
                                                    esto.value += "/";
                                                }
                                            }
                                            if (esto.value.length > otro) {
                                                if (esto.value.length === 5) {
                                                    esto.value += "/";
                                                }
                                            }
                                            if (esto.value.length < otro) {
                                                if (esto.value.length === 2 || esto.value.length === 5) {
                                                    esto.value = esto.value.substring(0, esto.value.length - 1);
                                                }
                                            }
                                            otro = esto.value.length;
                                        }

                                        $('#Regresar').click(function () {
                                            window.close();
                                        });


                                        $("[data-behavior~=only-alphanum]").alphanum({
                                            allowSpace: true,
                                            allowOtherCharSets: false,
                                        });
                                        $("[data-behavior~=only-num]").numeric("integer");

                                        $("[data-behavior~=only-alpha]").alphanum({
                                            allowNumeric: false,
                                            allowSpace: true,
                                            allowOtherCharSets: true,
                                            forceUpper: true
                                        });
        </script>
    </body>
</html>