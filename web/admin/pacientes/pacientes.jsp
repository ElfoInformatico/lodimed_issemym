<%-- 
    Document   : alta_pacientes
    Created on : 10-mar-2014, 9:14:09
    Author     : Americo
--%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.*" %>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    /**
     * Listado de pacientes
     */
    HttpSession sesion = request.getSession();
    String id_usu = "";
    request.setCharacterEncoding("UTF-8");
    id_usu = (String) session.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../../index.jsp");
        return;
    }

    ResultSet rset;
    ConectionDB con = new ConectionDB();

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="../../css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../../css/pie-pagina.css" rel="stylesheet" media="screen">
        <link href="../../css/topPadding.css" rel="stylesheet">
        <link href="../../css/datepicker3.css" rel="stylesheet">
        <link href="../../css/cupertino/jquery-ui-1.10.3.custom.css" rel="stylesheet">
        <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <title>Pacientes</title>
    </head>
    <body>
        <%@include file="../../jspf/mainMenu.jspf"%>
        <br/>
        <div class="container">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10">

                        <div class="row">
                            <div class="col-md-5"><h2>Pacientes Registrados</h2></div>
                            <div class="col-md-1"></div>
                            <div class="col-md-3"><img src="../../imagenes/isem.png" width=100 alt="Logo"></div>
                            <div class="col-md-3"><img src="../../imagenes/gobierno.png" width=100 alt="Logo"></div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <!--a class="btn btn-block btn-danger" href="../receta/captura.jsp">Regresar</a-->
                    </div>
                </div>
                <br />
                <form action="pacientes.jsp" method="post">
                    <div class="row">
                        <h4 class="col-sm-3">Buscar un Paciente</h4>
                        <div class="col-sm-5">
                            <input class="form-control" name="nombre_jq" id="nombre_jq" data-behavior = "only-alphanum"/>
                        </div>
                        <div class="col-sm-1">
                            <button class="btn btn-success btn-block" type="button" id="search" name="accion" >Buscar</button>
                            <button class="hidden" type="submit" id="buscarPaciente" name="accion" value="buscarPaciente">Buscar</button>
                        </div>
                        <div class="col-sm-2">
                            <a class="btn btn-block btn-success" href="alta_pacientes.jsp" target="_blank" >Alta Paciente</a>
                        </div>
                        <div class="col-sm-1">
                            <a class="btn btn-block btn-danger" href="javascript:window.close();" ><span class="glyphicon glyphicon-remove" ></span></a>
                        </div>
                    </div>
                </form>
                <%
                    if (request.getParameter("accion") != null) {
                        if (request.getParameter("accion").equals("buscarPaciente")) {
                %>
                <div class="panel panel-default">
                    <form class="form-horizontal" role="form" name="formulario_pacientes" id="formulario_pacientes" method="post" action="">
                        <div class="panel-body">
                            <div class="col-lg-3">
                            </div>                            
                            <br />

                            <table class="table table-striped table-bordered" id="datosPacientes">
                                <thead>
                                    <tr>
                                        <td>ID Paciente</td>
                                        <td class="col-sm-2">Nombre Completo</td>                                                                           
                                        <td>No. Afiliación</td>
                                        <td>No. Expediente</td> 
                                        <td>Estado</td>
                                        <td>Tipo Cob.</td>
                                        <!--<td>Vigencia</td>-->
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        try {

                                            String nombre_jq = request.getParameter("nombre_jq");
                                            con.conectar();
                                            PreparedStatement ps;
                                            ps = con.getConn().prepareStatement("SELECT p.id_pac, p.nom_com, p.num_afi, p.expediente, p.f_status, p.tip_cob, CONCAT(p.ini_vig,' al ',p.fin_vig) AS Vigencia, fin_vig FROM pacientes  AS p  where p.nom_com like ? AND ape_pat<>'RECETA'");
                                            nombre_jq = String.format("%%%s%%", nombre_jq);
                                            ps.setString(1, nombre_jq);
                                            rset = ps.executeQuery();

                                            while (rset.next()) {
                                                Date fecAfi = rset.getDate("fin_vig");
                                    %>
                                    <tr
                                        class="
                                        <%
                                            if ((new Date()).after(fecAfi)) {
                                                out.println("danger text-danger");
                                            }

                                            if (rset.getString(5).equals("S")) {
                                                out.println("danger text-danger");
                                            }
                                        %>
                                        "
                                        >
                                        <td><%=rset.getString(1)%></td>
                                        <td class="col-sm-2"><%=rset.getString(2)%></td>
                                        <td><%=rset.getString(3)%></td>
                                        <td><%=rset.getString(4)%></td>
                                        <td><%=rset.getString(5)%></td>
                                        <td><%=rset.getString(6)%></td>
                                        <!--<td><%=rset.getString(7)%></td>-->
                                        <td>
                                            <a class="btn btn-sm btn-danger" href="editar_paciente.jsp?id=<%=rset.getString(1)%>"><span class='glyphicon glyphicon-pencil' ></span></a>
                                            <a class="btn btn-sm btn-danger" href="../../paciente/suspender?id=<%=rset.getString(1)%>"><span class='glyphicon glyphicon-remove' ></span></a>
                                        </td>                                                                      
                                    </tr>
                                    <%
                                            }
                                        } catch (SQLException ex) {
                                            Logger.getLogger("pacientes.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                        } finally {
                                            try {
                                                if (con.estaConectada()) {
                                                    con.cierraConexion();
                                                }
                                            } catch (SQLException ex) {
                                                Logger.getLogger("pacientes.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                            }
                                        }
                                    %>
                                </tbody>
                            </table>

                        </div>
                    </form>

                </div>
                <%
                        }
                    }
                %>
            </div>
        </div>
        <script src="../../js/jquery-1.9.1.js"></script>
        <script src="../../js/bootstrap.js"></script>
        <script src="../../js/jquery-ui.js"></script>
        <script src="../../js/bootstrap-datepicker.js"></script>
        <script src="../../js/moment.js"></script>
        <script src="../../js/jquery.dataTables.js"></script>
        <script src="../../js/dataTables.bootstrap.js"></script>
        <script src="../../js/jquery.alphanum.js" type="text/javascript"></script>

        <script>
            $(document).ready(function () {
                $('#datosPacientes').dataTable();

                $("#search").click(function () {
                    var nombre = $("#nombre_jq").val();
                    if (nombre === "")
                    {
                        $("#nombre_jq").focus();
                        alert("Ingresar un criterio de búsqueda porfavor.");
                        return false;

                    }

                    else
                    {
                        $("#buscarPaciente").click();
                    }
                });

            });

            $(function () {
                $("#nombre_jq").keyup(function () {
                    var nombre = $("#nombre_jq").val();
                    $("#nombre_jq").autocomplete({
                        source: "../../AutoPacientes?nombre=" + nombre + "&receta=no",
                        minLength: 2,
                        select: function (event, ui) {
                            $("#nombre_jq").val(ui.item.nom_com);
                            return false;
                        }
                    }).data("ui-autocomplete")._renderItem = function (ul, item) {
                        var encabezado = "<li class='bg-success'>";
                        var cuerpo = "<a class='text-success'>" + item.nom_com + "</a>"

                        if (item.status === 'S') {
                            encabezado = "<li class='bg-danger'>";
                            cuerpo = "<a class='text-danger'>" + item.nom_com + "</a>"
                        }

                        return $(encabezado)
                                .data("ui-autocomplete-item", item)
                                .append(cuerpo)
                                .appendTo(ul);
                    };
                });
            });

            $("[data-behavior~=only-alphanum]").alphanum({
                allowSpace: true,
                allowOtherCharSets: true
            });
        </script>
    </body>
</html>
<!-- 
================================================== -->
<!-- Se coloca al final del documento para que cargue mas rapido -->
<!-- Se debe de seguir ese orden al momento de llamar los JS -->
