<%-- 
    Document   : reporteEntradas
    Created on : 9/12/2015, 04:31:15 PM
    Author     : anonimus
--%>

<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    HttpSession sesion = request.getSession();
    String id_usu = "";
    try {
        id_usu = (String) session.getAttribute("id_usu");
    } catch (Exception e) {
        Logger.getLogger("reporteEntradas.jsp").log(Level.SEVERE, e.getMessage(), e);
    }
    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Estilos CSS -->
        <link href="../../css/bootstrap.css" rel="stylesheet">
        <link href="../../css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!---->
        <title>Reporte Entradas</title>

    </head>
    <body>
        <div class="container" >
            <%@include file="../../jspf/mainMenu.jspf"%>
            <br/><br/><br/>
            <h1>Reporte de entradas al sistema</h1>
            <br/>
            <div class="panel-body" id="tableTot" >

                <div id="container">

                    <div id="dynamic"></div>

                </div>
            </div>

        </div>

        <script>
            var context = "${pageContext.servletContext.contextPath}";
        </script>
        <script src="../../js/jquery-1.9.1.js"></script>
        <script src="../../js/bootstrap.js"></script>
        <script src="../../js/jquery.dataTables.js"></script>
        <script src="../../js/entradas/entradasRegistros.js"></script>
    </body>
</html>
