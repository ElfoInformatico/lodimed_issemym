<%-- 
    Document   : alta_pacientes
    Created on : 10-mar-2014, 9:14:09
    Author     : Americo
--%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    /**
     * Alta de médicos
     */
    HttpSession sesion = request.getSession();
    String id_usu = "";
    id_usu = (String) sesion.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../../index.jsp");
        return;
    }

    String cla_uni = "", des_uni = "";
    ConectionDB con = new ConectionDB();
    try {
        con.conectar();
        PreparedStatement ps;
        ResultSet rs;

        ps = con.getConn().prepareStatement(
                  "SELECT uni.cla_uni,uni.des_uni FROM unidades AS uni INNER JOIN usuarios AS u ON u.cla_uni = uni.cla_uni WHERE u.id_usu = ?");
        ps.setString(1, id_usu);
        rs = ps.executeQuery();
        if (rs.next()) {
            cla_uni = rs.getString(1);
            des_uni = rs.getString(2);
        }
    } catch (SQLException ex) {
        Logger.getLogger("alta_medico.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
    } finally {
        try {
            if (con.estaConectada()) {
                con.cierraConexion();
            }
        } catch (SQLException ex) {
            Logger.getLogger("alta_medico.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        }
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="../../css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../../css/pie-pagina.css" rel="stylesheet" media="screen">
        <link href="../../css/topPadding.css" rel="stylesheet">
        <link href="../../css/datepicker3.css" rel="stylesheet">
        <link href="../../css/cupertino/jquery-ui-1.10.3.custom.css" rel="stylesheet">
        <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <title>Registro de médicos</title>
    </head>
    <body>

        <%@include file="../../jspf/mainMenu.jspf" %> 
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10">
                        <h3>Alta de Médico</h3>
                    </div>
                    <div class="col-lg-2">
                        <!--a class="btn btn-block btn-danger" href="pacientes.jsp">Regresar</a-->
                    </div>
                </div>
                <div class="panel panel-default">
                    <form class="form-horizontal" role="form" name="formulario_pacientes" id="formulario_pacientes" enctype="multipart/form-data" method="POST" action="../../Medicos">
                        <div class="panel-body">                            
                            <div class="row">
                                <label for="ape_pat" class="col-sm-2 control-label">Apellido Paterno</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="ape_pat" name="ape_pat" placeholder="" value="" data-behavior = "only-alpha"/>
                                </div>
                                <label for="ape_mat" class="col-sm-2 control-label">Apellido Materno</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="ape_mat" name="ape_mat" placeholder="" value="" data-behavior = "only-alpha"/>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <label for="nombre" class="col-sm-2 control-label">Nombre(s)</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" value="" data-behavior = "only-alpha-space"/>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <label for="cedula" class="col-sm-2 control-label">Cédula</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="cedula" name="cedula" placeholder="" value="" data-behavior = "only-alphanum-upper"/>
                                </div>
                                <label for="fec_nac" class="col-sm-1 control-label">Fecha Nac</label>
                                <div class="col-sm-2">
                                    <input type="text" required class="form-control" id="fec_nac" name="fec_nac" />
                                </div>
                                <label for="rfc" class="col-sm-1 control-label" >R.F.C.</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="rfc" name="rfc" placeholder="" value="" data-behavior = "only-alphanum-upper"/>
                                </div>
                                <div class="col-sm-1">
                                    <button type="button" class="btn btn-danger btn-block" title="Generar RFC" onclick="generarRFC()"><span class="glyphicon glyphicon-hdd" ></span></button>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <label for="usuario" class="col-sm-2 control-label" id="labelUsu" >Usuario</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="usuario" name="usuario" placeholder="" value="" data-behavior = "only-alphanum"/>
                                </div>
                                <label for="password" class="col-sm-1 control-label" id="labelPass" >Password</label>
                                <div class="col-sm-2">
                                    <input type="password" class="form-control" id="password" name="password" placeholder="" value="" data-behavior = "only-alphanum"/>
                                </div>
                                <div class="col-sm-1" hidden>
                                    <button type="button" class="btn btn-danger btn-block" title="Generar Usuario y Contraseña" onclick="generarUserPass()"><span class="glyphicon glyphicon-hdd"></span></button>
                                </div>
                                <label for="folIni" class="col-sm-1 control-label" id="folIniLabel" >Folio Ini</label>
                                <div class="col-sm-2">
                                    <input type="number" class="form-control" readonly id="folIni" name="folIni" required />
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <label for="fec_nac" class="col-sm-2 control-label">Unidad</label>
                                <div class="col-sm-6">
                                    <input type="hidden" value="<%=cla_uni%>" name="cla_uni">
                                    <select disabled="" class="form-control" id="unidad" name="unidad">
                                        <option value="<%=cla_uni%>"><%=des_uni%></option>
                                    </select>
                                </div>
                                <label for="folFin" class="col-sm-1 control-label">Folio Fin</label>
                                <div class="col-sm-2">
                                    <input type="number" class="form-control" readonly id="folFin" name="folFin" required />
                                </div>
                            </div><br/>
                            <div class="row">
                                <label class="col-sm-2 control-label">Tipo de Consulta</label>
                                <div class="col-sm-3">
                                    <select class="form-control" id="slcTipoConsu" name="slcTipoConsu">
                                        <option value="Consulta Externa">Consulta Externa</option>
                                        <option value="Urgencias">Urgencias</option>
                                        <option value="Hospitalización">Hospitalización</option>
                                    </select>
                                </div>


                            </div>
                            <br/>
                            <div class="row" id="firmaDiv"  >
                                <label class="col-sm-2 control-label" >Firma:</label>
                                <div class="col-sm-4" > 
                                    <input class="form-control" type="file" id="txtFirma" name="txtFirma" accept=".jpg" >
                                </div>
                                <label class="control-label col-lg-3 col-md-3 col-sm-3">¿Se cuenta con imagen?</label>
                                <div style="margin-top: 7px" class="col-lg-2 col-md-2 col-sm-2">
                                    <input type="checkbox" id="chkImg" name="chkImg" checked >
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-lg-6">
                                    <button class="btn btn-block btn-success" id="Guardar" type="button" >Guardar</button>
                                    <button class="hidden" type="submit" id="btnEnviar" ></button>
                                </div>
                                <div class="col-lg-6">
                                    <button class="btn btn-block btn-success" id="Regresar" type="button" >Regresar</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>

            </div>
        </div>

        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="../../js/jquery-1.9.1.js"></script>
        <script src="../../js/bootstrap.js"></script>
        <script src="../../js/jquery-ui.js"></script>
        <script src="../../js/datepickerEspanol.js"></script>
        <script src="../../js/adminUsu/editUsu.js"></script>
        <script src="../../js/jquery.alphanum.js" type="text/javascript"></script>
        <script src="../../js/jquery.maskedinput.min.js" type="text/javascript"></script>

        <script>

                                        $("#fec_nac").datepicker({
                                            dateFormat: 'dd-mm-yy',
                                            changeYear: true,
                                            changeMonth: true,
                                            maxDate: '-20Y',
                                            yearRange: "1910:2020"

                                        });

                                        $.ajax({
                                            url: "${pageContext.servletContext.contextPath}/Operaciones",
                                            data: {ban: 0},
                                            type: 'POST',
                                            async: false,
                                            success: function (data)
                                            {
                                                $("#folIni").val(data.folIni);
                                                $("#folFin").val(data.folFin);

                                            }, error: function (jqXHR, textStatus, errorThrown) {

                                                alert("Error Contactar al departamento de sistemas");

                                            }
                                        });

                                        $.ajax({
                                            dataType: 'json',
                                            url: "${pageContext.servletContext.contextPath}/Operaciones",
                                            data: {ban: 4},
                                            type: 'POST',
                                            async: false,
                                            success: function (data)
                                            {
                                                var sis = data.sis;

                                                if (sis === 2)
                                                {
                                                    $("#folIniLabel").addClass("col-sm-offset-1 ");
                                                } else
                                                {
                                                    var folInicial = $("#folIni").val();
                                                    $("#labelUsu").hide();
                                                    $("#labelPass").hide();
                                                    $("#usuario").hide();
                                                    $("#password").hide();
                                                    $("#folIniLabel").addClass("col-sm-offset-8");
                                                    $("#usuario").val("medico" + folInicial);
                                                    $("#password").val("medico");
                                                    //$("#firmaDiv").hide();
                                                    $("#chkImg").prop('checked', false);
                                                }


                                            }, error: function (jqXHR, textStatus, errorThrown) {

                                                alert("Error Contactar al departamento de sistemas");

                                            }
                                        });
                                        function generarRFC() {
                                            /**
                                             * Genera el RFC
                                             */
                                            var apePat = $('#ape_pat').val();
                                            var apeMat = $('#ape_mat').val();
                                            var nombres = $('#nombre').val();
                                            var fecNac = $('#fec_nac').val();
                                            if (apePat === "" || apeMat === "" || nombres === "" || fecNac === "") {
                                                alert("Faltan datos, verifique");
                                            } else {
                                                var ape1 = apePat.substr(0, 2);
                                                var ape2 = apeMat.substr(0, 1);
                                                var nom1 = nombres.substr(0, 1);
                                                var fechaNa = fecNac.split('-');
                                                var fec2 = fechaNa[0];
                                                var fec3 = fechaNa[1];
                                                var fec4 = fechaNa[2];
                                                fec4 = fec4.substr(2, 3);
                                                var fec1 = fec4 + fec3 + fec2;

                                                $('#rfc').val(ape1 + ape2 + nom1 + fec1);
                                            }
                                        }
                                        function generarUserPass() {
                                            /**
                                             * Genera usuario y contraseñas
                                             */
                                            var apePat = $('#ape_pat').val();
                                            var apeMat = $('#ape_mat').val();
                                            var nombres = $('#nombre').val();
                                            var fecNac = $('#fec_nac').val();

                                            if (apePat === "" || apeMat === "" || nombres === "" || fecNac === "") {
                                                alert("Faltan datos, verifique");
                                            } else {
                                                var userArray = nombres.split(" ");
                                                var user = userArray[0];
                                                var fec1 = fecNac.replace("-", "");
                                                var fec1 = fec1.replace("-", "");
                                                var fec1 = fec1.substr(2, 8);
                                                $('#usuario').val(user.toLowerCase());
                                                $('#password').val(user.toLowerCase() + fec1);
                                            }
                                        }
                                        function tabular(e, obj)
                                        {
                                            /**
                                             * Siguiente objeto
                                             */
                                            tecla = (document.all) ? e.keyCode : e.which;
                                            if (tecla !== 13)
                                                return;
                                            frm = obj.form;
                                            for (i = 0; i < frm.elements.length; i++)
                                                if (frm.elements[i] === obj)
                                                {
                                                    if (i === frm.elements.length - 1)
                                                        i = -1;
                                                    break
                                                }
                                            /*ACA ESTA EL CAMBIO*/
                                            if (frm.elements[i + 1].disabled === true)
                                                tabular(e, frm.elements[i + 1]);
                                            else
                                                frm.elements[i + 1].focus();
                                            return false;
                                        }

                                        otro = 0;
                                        function LP_data() {
                                            /**
                                             * Sólo números
                                             */
                                            var key = window.event.keyCode;//codigo de tecla. 
                                            if (key < 48 || key > 57) {//si no es numero 
                                                window.event.keyCode = 0;//anula la entrada de texto. 
                                            }
                                        }
                                        function anade(esto) {
                                            if (esto.value.length > otro) {
                                                if (esto.value.length === 2) {
                                                    esto.value += "/";
                                                }
                                            }
                                            if (esto.value.length > otro) {
                                                if (esto.value.length === 5) {
                                                    esto.value += "/";
                                                }
                                            }
                                            if (esto.value.length < otro) {
                                                if (esto.value.length === 2 || esto.value.length === 5) {
                                                    esto.value = esto.value.substring(0, esto.value.length - 1);
                                                }
                                            }
                                            otro = esto.value.length;
                                        }

                                        $(document).ready(function () {

                                            $('#Guardar').click(function () {

                                                var ape_pat = $('#ape_pat').val();
                                                var ape_mat = $('#ape_mat').val();
                                                var nombre = $('#nombre').val();
                                                var cedula = $('#cedula').val();
                                                var unidad = $('#unidad').val();
                                                var rfc = $('#rfc').val();
                                                var user = $('#usuario').val();
                                                var pass = $("#password").val();
                                                var folIni = $('#folIni').val();
                                                var folFin = $('#folFin').val();
                                                var firma;

                                                if ($("#chkImg").prop('checked'))
                                                {
                                                    firma = $("#txtFirma").val();
                                                } else
                                                {
                                                    firma = "vacio";
                                                }

                                                if (ape_pat === "" || ape_mat === "" || nombre === "" || cedula === "" || rfc === "" || unidad === "" || folIni === "" || folFin === "" || firma === "" || user === "" || pass === "") {
                                                    alert("Tiene campos vacíos, verifique.");
                                                    return false;
                                                }
                                                if (parseInt(folIni) > parseInt(folFin)) {
                                                    alert("El folio inicial no puede ser mayor al folio final");
                                                    return false;
                                                }

                                                $("#btnEnviar").click();

                                            });
                                            $('#Regresar').click(function () {
                                                self.location = 'medico.jsp';
                                            });

                                        });

                                        $("[data-behavior~=only-alphanum]").alphanum({
                                            allowSpace: false,
                                            allowOtherCharSets: false
                                        });

                                        $("[data-behavior~=only-alphanum-upper]").alphanum({
                                            allowSpace: false,
                                            allowOtherCharSets: false,
                                            forceUpper: true
                                        });

                                        $("[data-behavior~=only-alpha]").alphanum({
                                            allowNumeric: false,
                                            allowSpace: true,
                                            allowOtherCharSets: true,
                                            forceUpper: true
                                        });

                                        $("[data-behavior~=only-alpha-space]").alphanum({
                                            allowNumeric: false,
                                            allowSpace: true,
                                            allowOtherCharSets: true,
                                            allowUpper: true,
                                            forceUpper: true
                                        });

                                        $('#fec_nac').mask("99-99-9999", {placeholder: " "});
        </script>
    </body>
</html>