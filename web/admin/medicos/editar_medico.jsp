<%-- 
    Document   : alta_pacientes
    Created on : 10-mar-2014, 9:14:09
    Author     : Americo
--%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="Clases.ConectionDB"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    /**
     * Para edición de médico
     */
    HttpSession sesion = request.getSession();
    String id_usu = "";
    id_usu = (String) sesion.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../../index.jsp");
        return;
    }

    ResultSet rset;
    ConectionDB con = new ConectionDB();

    String id = "", ape_pat = "", ape_mat = "", nom_pac = "", rfc = "", cedulap = "", f_status = "", usuario = "", iniRec = "", finRec = "", folAct = "", tipoConsu = "";

    id = request.getParameter("id");

    try {
        con.conectar();
        /**
         * Se obtienen los datos del médico a editar
         */
        PreparedStatement ps;
        ps = con.getConn().prepareStatement("SELECT m.cedula,m.ape_pat,m.ape_mat,nom_med,rfc,cedulapro,f_status,u.user,m.iniRec,m.finRec,m.folAct,m.tipoConsulta FROM medicos m INNER JOIN usuarios u on m.cedula=u.cedula WHERE u.cedula=?");
        ps.setString(1, id);
        rset = ps.executeQuery();

        if (rset.next()) {
            ape_pat = rset.getString(2);
            ape_mat = rset.getString(3);
            nom_pac = rset.getString(4);
            rfc = rset.getString(5);
            cedulap = rset.getString(6);
            f_status = rset.getString(7);
            usuario = rset.getString(8);
            iniRec = rset.getString(9);
            finRec = rset.getString(10);
            folAct = rset.getString(11);
            tipoConsu = rset.getString(12);
        }

    } catch (SQLException ex) {
        Logger.getLogger("alta_medico.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
    } finally {
        try {
            if (con.estaConectada()) {
                con.cierraConexion();
            }
        } catch (SQLException ex) {
            Logger.getLogger("alta_medico.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        }
    }

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="../../css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../../css/pie-pagina.css" rel="stylesheet" media="screen">
        <link href="../../css/topPadding.css" rel="stylesheet">
        <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <title>Edición Médico</title>
    </head>
    <body>

        <%@include file="../../jspf/mainMenu.jspf" %> 
        <div class="container-fluid">
            <div class="container">
                <h3>Navegación y Edición de Médicos</h3>
                <div class="panel panel-default">
                    <form class="form-horizontal" role="form" name="formulario_pacientes" id="formulario_pacientes" enctype="multipart/form-data" method="post" action="../../ModiMedico">
                        <div class="panel-body">                                                        
                            <div class="row">
                                <label for="clave" class="col-sm-2 control-label">Clave</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="clave" name="clave" placeholder=""  value="<%=id%>" readonly=""/>
                                    <input type="hidden" value="<%=id%>" name="id" >
                                </div>
                                <label for="Clave" class="col-sm-1 control-label"> Estatus:</label>
                                <div class="col-md-1">
                                    <input type="text" class="form-control" id="estatus" readonly name="estatus" placeholder="" value="<%=f_status%>"/>                                    
                                </div>
                                <div class="col-md-2">
                                    <select id="selectsts" class="form-control">
                                        <option id="op">--Estatus--</option>
                                        <option value="A">Activo</option>
                                        <option value="S">Suspendido</option>
                                    </select>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <label for="ape_pat" class="col-sm-2 control-label">Apellido Paterno</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="ape_pat" name="ape_pat" placeholder="" value="<%=ape_pat%>" data-behavior = "only-alpha"/>
                                </div>
                                <label for="ape_mat" class="col-sm-2 control-label">Apellido Materno</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="ape_mat" name="ape_mat" placeholder="" value="<%=ape_mat%>" data-behavior = "only-alpha"/>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <label for="nombre" class="col-sm-2 control-label">Nombre(s)</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" value="<%=nom_pac%>" data-behavior = "only-alpha-space"/>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <label for="cedula" class="col-sm-2 control-label">Cédula</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="cedula" name="cedula" placeholder=""  value="<%=cedulap%>" data-behavior = "only-alphanum-upper"/>
                                </div>    
                                <label for="rfc" class="col-sm-2 control-label">R.F.C.</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="rfc" name="rfc" placeholder=""  value="<%=rfc%>" data-behavior = "only-alphanum-upper"/>
                                </div>                                
                            </div><br/>
                            <div class="row">
                                <label for="iniRec" class="control-label col-sm-2">Folio Inicial</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="iniRec" name="iniRec" placeholder=""  value="<%=iniRec%>" data-behavior = "only-num"/>
                                </div>
                                <label for="finRec" class="col-sm-2 control-label">Folio Final</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="finRec" name="finRec" placeholder=""  value="<%=finRec%>" data-behavior = "only-num"/>
                                </div>
                                <label for="folAct" class="col-sm-2 control-label">Folio Actual</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="folAct" name="folAct" placeholder=""  value="<%=folAct%>" data-behavior = "only-num"/>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <label class="col-sm-2 control-label">Tipo de Consulta</label>
                                <div class="hidden">
                                    <input type="text" class="form-control" value="<%=tipoConsu%>" name="txtTipoConsu" id="txtTipoConsu" readonly>
                                </div>
                                <div class="col-sm-2">
                                    <select class="form-control" id="slcTipoConsu" name="slcTipoConsu">
                                        <option value="">---Seleccione---</option>
                                        <option value="Consulta Externa" 
                                                <%
                                                    if (tipoConsu.equals("Consulta Externa")) {
                                                        out.println("selected");
                                                    }
                                                %>
                                                >Consulta Externa</option>
                                        <option value="Urgencias"
                                                <%
                                                    if (tipoConsu.equals("Urgencias")) {
                                                        out.println("selected");
                                                    }
                                                %>
                                                >Urgencias</option>
                                        <option value="Hospitalizacion"
                                                <%
                                                    if (tipoConsu.equals("Hospitalizacion")) {
                                                        out.println("selected");
                                                    }
                                                %>
                                                >Hospitalización</option>
                                    </select>
                                </div>
                            </div><br/> 
                            <br/>
                            <div class="row" >
                                <label class="col-sm-2 control-label" id="labelFirma" >Firma:</label>
                                <div class="col-sm-4" id="conteneorArchivos" > 
                                    <input class="form-control" type="file" id="txtFirma" name="txtFirma" accept=".jpg">
                                </div>
                                <label class="control-label col-lg-3 col-md-3 col-sm-3">Actualizar firma</label>
                                <div style="margin-top: 7px" class="col-lg-2 col-md-2 col-sm-2">
                                    <input type="checkbox" id="chkImg" name="chkImg">
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <label for="usuario" class="col-sm-2 control-label">Usuario</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" id="usuario" name="usuario" placeholder="" value="<%=usuario%>" data-behavior = "only-alphanum" readonly/>
                                </div>    
                                <label for="rfc" class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control" id="password" name="password" placeholder=""  value="" data-behavior = "only-alphanum"/> (Sino ingresa una nueva contraseña se mantendrá la anterior).
                                </div>                                
                            </div>                           
                            <br />
                            <div class="row">                                
                                <div class="col-lg-6">
                                    <button class="btn btn-block btn-success" id="Guardar" type="button">Actualizar</button>
                                    <button class="hidden" type="submit" id="btnEnviar" ></button>
                                </div>
                                <div class="col-lg-6">
                                    <button class="btn btn-block btn-success" id="Regresar" type="button" >Regresar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="../../js/jquery-1.9.1.js"></script>
        <script src="../../js/bootstrap.js"></script>
        <script src="../../js/jquery-ui-1.10.3.custom.js"></script>
        <script src="../../js/bootstrap-datepicker.js"></script>    
        <script src="../../js/jquery.alphanum.js" type="text/javascript"></script>
        <script>

        $(function ()
        {
            $("#labelFirma").hide();
            $("#conteneorArchivos").hide();
            $("#chkImg").click(function () {
                if ($("#chkImg").prop('checked'))
                {
                    $("#labelFirma").show();
                    $("#conteneorArchivos").show();
                }
                else
                {
                    $("#labelFirma").hide();
                    $("#conteneorArchivos").hide();
                }
            });

        });

        function tabular(e, obj)
        {
            /**
             * Saltar al siguiente objeto
             */
            tecla = (document.all) ? e.keyCode : e.which;
            if (tecla !== 13)
                return;
            frm = obj.form;
            for (i = 0; i < frm.elements.length; i++)
                if (frm.elements[i] === obj)
                {
                    if (i === frm.elements.length - 1)
                        i = -1;
                    break
                }
            /*ACA ESTA EL CAMBIO*/
            if (frm.elements[i + 1].disabled === true)
                tabular(e, frm.elements[i + 1]);
            else
                frm.elements[i + 1].focus();
            return false;
        }

        otro = 0;
        function LP_data() {
            /**
             * Solo números
             */
            var key = window.event.keyCode;//codigo de tecla. 
            if (key < 48 || key > 57) {//si no es numero 
                window.event.keyCode = 0;//anula la entrada de texto. 
            }
        }

        $(document).ready(function () {
            $('#selectsts').change(function () {
                /**
                 * Cambia status
                 */
                var valor = $('#selectsts').val();
                $('#estatus').val(valor);
            });
            $('#slcTipoConsu').change(function () {
                /**
                 * Cambia tipo de consulta
                 */
                $('#txtTipoConsu').val($('#slcTipoConsu').val());
            });

            $('#Guardar').click(function () {
                /**
                 * Guardar cambios
                 */

                var iniRec = $('#iniRec').val();
                var finRec = $('#finRec').val();
                var folAct = $('#folAct').val();
                var ape_pat = $('#ape_pat').val();
                var ape_mat = $('#ape_mat').val();
                var nombre = $('#nombre').val();
                var rfc = $('#rfc').val();
                var estatus = $('#estatus').val();
                var password = $('#password').val();
                var form = $('#formulario_pacientes');
                var firma;

                if ($("#chkImg").prop('checked'))
                {
                    firma = $("#txtFirma").val();
                }
                else
                {
                    firma = "vacio";
                }
                if (ape_pat === "" || ape_mat === "" || nombre === "" || rfc === "" || iniRec === "" || finRec === "" || firma === "" || folAct === "" || $('#slcTipoConsu').val() === "") {
                    alert("Tiene campos vacíos, verifique.");
                    return false;
                }
                if (parseInt(iniRec) > parseInt(finRec)) {
                    alert("El folio inicial no puede ser mayor que el folio final");
                    return false;
                }
                if (parseInt(folAct) > parseInt(finRec) || parseInt(folAct) < parseInt(iniRec)) {
                    alert("El folio actual debe estar dentro del rango");
                    return false;
                }
                if (estatus === "--Estatus--") {
                    alert("Favor verifique el ESTATUS del médico");
                    return false;
                }

                $("#btnEnviar").click();
            });
            $('#Regresar').click(function () {
                self.location = 'medico.jsp';
            });
        });

        $("[data-behavior~=only-alphanum]").alphanum({
            allowSpace: false,
            allowOtherCharSets: true
        });
        
        $("[data-behavior~=only-alphanum-upper]").alphanum({
            allowSpace: false,
            allowOtherCharSets: true,
            forceUpper: true
        });
        
        $("[data-behavior~=only-num]").numeric("integer");

        $("[data-behavior~=only-alpha]").alphanum({
            allowNumeric: false,
            allowSpace: true,
            allowOtherCharSets: false,
            forceUpper: true
        });

        $("[data-behavior~=only-alpha-space]").alphanum({
            allowNumeric: false,
            allowSpace: true,
            allowOtherCharSets: true,
            allowUpper: true,
            forceUpper: true
        });
        </script>
    </body>
</html>