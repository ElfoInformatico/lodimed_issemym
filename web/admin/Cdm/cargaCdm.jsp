<%-- 
    Document   : cargaCdm
    Created on : 25/11/2015, 06:39:14 PM
    Author     : anonimus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
    HttpSession sesion = request.getSession();
    String id_usu = "";
    id_usu = (String) sesion.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../../index.jsp");
        return;
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <title>Carga de CDM</title>
    </head>
    <body>
        <%@include file="../../jspf/mainMenu.jspf" %> 
        <br/>   
        <br/>
        <div class="container">
            <h2>Cargar CDM</h2>
        <form name="cargaCdm" method="POST" enctype="multipart/form-data" action="../../servletCdm" onsubmit="funcionCarga()" >
            <div class="row">
                <label class="form-horizontal col-lg-4">Seleccione un archivo:</label>
                <div class="col-lg-8">
                    <input required type="file" class="form-control" name="archivo" id="archivo" accept=".csv">
                </div>
                <br/>
                <br/>

                <div class="col-lg-12">
                    <button class="btn btn-block btn-success" id="btnCarga">Cargar</button>
                </div>
                <c:if test="${sessionScope.info_upload!=null}">
                    <br>
                    <div class="col-lg-1"></div>
                    <div class="col-lg-10 alert alert-danger">
                        <p><h4>Mensaje:</h4><c:out value="${sessionScope.info_upload}"/></p>
                        <c:set var="info_upload" scope = "session" value="${null}"/>

                    </div>
                </c:if>
                <div class="row" id="imgCarga">
                    <div class="col-md-12 text-center">
                        <img src="../../imagenes/ajax-loader-1.gif" width=100 alt="Logo">
                    </div>
                </div>
            </div>
        </form>
        </div>    
        <script src="../../js/jquery.js" ></script>
        <script src="../../js/bootstrap.js"></script>
        <script>
                $(document).ready(function ()
                {

                    $('#imgCarga').toggle();


                });

                function funcionCarga() {
                    $('#btnCarga').attr('disabled', true);
                    $('#imgCarga').toggle();
                }



        </script>
    </body>
</html>
