<%-- 
    Document   : acutalizarPrefijo
    Created on : 2/12/2015, 04:34:18 PM
    Author     : anonimus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    HttpSession sesion = request.getSession();
    String id_usu = "";
    id_usu = (String) sesion.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../../index.jsp");
        return;
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../../css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <title>Actualizar prefijo</title>        
    </head>
    <body>
        <%@include file="../../jspf/mainMenu.jspf" %> 
        <br/>   
        <br/>
        <div class="container">
            <br/>
            <h1>Cambio de prefijo en receta manual</h1>
            <br/>
            <br/>
            <div class="row">
                <h4 class="col-lg-2 col-md-2 col-sm-2" >Prefijo actual:</h4>
                <div class="col-lg-2 col-md-2 col-sm-2" >
                    <input class="form-control" readonly id="preOri" > 
                </div>
                <h4 class="col-lg-2 col-md-2 col-sm-2" >Prefijo nuevo:</h4>
                <div class="col-lg-2 col-md-2 col-sm-2" >
                    <input class="form-control" onkeyup="validate()" id="preNew" name="preNew"> 
                </div>

            </div>
            <br/>
            <br/>
            <div class="row" >
                <div class="col-lg-2 col-md-2 col-sm-2" ></div>
                <div class="col-lg-4 col-md-4 col-sm-4" >
                    <button class="btn btn-success btn-block" id="btnSave" type="button" >Guardar</button>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4" >
                    <button class="btn btn-danger btn-block" id="btnCancel" type="button" >Cancelar</button>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2" ></div>
            </div>
        </div>
        <script src="../../js/jquery-1.9.1.js"></script>
        <script src="../../js/bootstrap.js"></script>
        <script src="../../js/prefijo/prefijoCambio.js"></script>
        <script>
                        var context = "${pageContext.servletContext.contextPath}";

                        function validate() {
                            if (!/^[a-zA-Z]*$/g.test($('#preNew').val())) {
                                alert("Solo letras");
                                $("#preNew").focus();
                                $("#preNew").val('');
                                return false;
                            } else {
                                $("#preNew").val(upperCase($('#preNew').attr('id')));
                            }
                        }

        </script>
    </body>
</html>
