<%-- 
    Document   : alta_pacientes
    Created on : 10-mar-2014, 9:14:09
    Author     : Americo
--%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="Clases.ConectionDB"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    HttpSession sesion = request.getSession();

    String id_usu = "";
    int rol = 0;
    try {
        id_usu = (String) sesion.getAttribute("id_usu");
        rol = (int) sesion.getAttribute("rol");

    } catch (Exception e) {
        Logger.getLogger("usuario.jsp").log(Level.SEVERE, e.getMessage(), e);
    }

    if (id_usu == null) {
        response.sendRedirect("../../index.jsp");
        return;
    }
    ResultSet rset;
    ConectionDB con = new ConectionDB();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="../../css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../../css/pie-pagina.css" rel="stylesheet" media="screen">
        <link href="../../css/topPadding.css" rel="stylesheet">
        <link href="../../css/datepicker3.css" rel="stylesheet">
        <link href="../../css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="../../css/dataTables.bootstrap.css" rel="stylesheet">
        <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <title>Usuarios</title>
    </head>
    <body>

        <%@include file="../../jspf/mainMenu.jspf" %> 
        <div class="container">
            <div class="container">
                <input type="hidden" value="<%=rol%>" id="idRol">
                <div class="row">
                    <div class="col-lg-10">

                        <div class="row">
                            <div class="col-md-8"><h2>Usuarios Registrados Farmacia</h2></div>
                            <div class="col-md-1"></div>
                            <div class="col-md-3"><img src="../../imagenes/isem.png" style="margin-top: 10px" width=100 alt="LOGO"></div>
                        </div>
                    </div>
                </div>
                <br />
                <div class="panel panel-default">
                    <form class="form-horizontal" role="form" name="formulario_pacientes" id="formulario_pacientes" method="get" action="">
                        <div class="panel-body">
                            <div class="col-lg-3">
                                <input type="hidden" id="uni" value="<%=(String) sesion.getAttribute("cla_uni")%>" >
                                <button class="btn btn-success btn-block" type="button" id="addUser" >Alta Usuarios</button>
                            </div>                            
                            <br/>
                            <br/>
                            <div class="panel-footer">
                                <table class="table table-striped table-bordered" id="datosPacientes">
                                    <thead>
                                        <tr>
                                            <td>Clave</td>
                                            <td>Nombre Completo</td>
                                            <td>Unidad</td>  
                                            <td>Acción</td>
                                            <td style="width: 17%" >Opciones</td> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%                                            try {
                                                con.conectar();
                                                PreparedStatement ps = null;

                                                if (rol != 0) {
                                                    ps = con.getConn().prepareStatement("SELECT us.id_usu,CONCAT(us.nombre,' ',us.ape_pat,' ',us.ape_mat) as Nombre, uni.des_uni, us.baja FROM usuarios AS us INNER JOIN unidades AS uni ON us.cla_uni = uni.cla_uni WHERE us.cedula='-' and rol  NOT IN ('0','3')");
                                                } else {
                                                    ps = con.getConn().prepareStatement("SELECT us.id_usu,CONCAT(us.nombre,' ',us.ape_pat,' ',us.ape_mat) as Nombre, uni.des_uni, us.baja FROM usuarios AS us INNER JOIN unidades AS uni ON us.cla_uni = uni.cla_uni WHERE us.cedula='-' and rol!=0");
                                                }

                                                rset = ps.executeQuery();

                                                while (rset.next()) {
                                                    boolean sts = rset.getBoolean(4);
                                                    String estado = "";
                                                    if (sts) {
                                                        estado = "ACTIVO";
                                                    } else {
                                                        estado = "SUSPENDIDO";
                                                    }
                                        %>
                                        <tr>
                                            <td><%=rset.getString(1)%></td>
                                            <td><%=rset.getString(2)%></td>
                                            <td><%=rset.getString(3)%></td>
                                            <td><%=estado%></td>
                                            <td>
                                                <button class="btn btn-danger" type="button" onclick="edit(<%=rset.getString(1)%>)" ><span class="glyphicon glyphicon-edit"></span></button>

                                                <button class="btn btn-danger" type="button" onclick="del(<%=rset.getString(1)%>)" ><span class="glyphicon glyphicon-remove"></span></button>
                                            </td>                                                                     
                                        </tr>
                                        <%
                                                }
                                            } catch (SQLException ex) {
                                                Logger.getLogger("medico.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                            } finally {
                                                try {
                                                    if (con.estaConectada()) {
                                                        con.cierraConexion();
                                                    }
                                                } catch (SQLException ex) {
                                                    Logger.getLogger("medico.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                                }
                                            }
                                        %>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </form>

                </div>
                <button type="button" class="btn btn-success hidden " id="btnModal1" data-toggle="modal" data-target="#myModal1">
                </button>
                <div class="modal fade bs-example-modal-lg" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title text-center text-success" id="myModalLabel1">Editar usuario</h3>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <input type="hidden" id="txtIdUsu" >   
                                    <label for="ape_pat"  class="col-sm-2 control-label text-center">Apellido Paterno:</label>
                                    <div class="col-sm-4">

                                        <input type="text" class="form-control" id="ape_pat" name="ape_pat" placeholder="" data-behavior = "only-alpha" />
                                    </div>
                                    <label for="ape_mat"  class="col-sm-2 control-label text-center">Apellido Materno:</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="ape_mat" name="ape_mat" placeholder="" data-behavior = "only-alpha" />
                                    </div>
                                </div>
                                <br/>
                                <div class="row" >
                                    <label for="txtNom" style="margin-top: 10px" class="col-sm-2 control-label text-center">Nombre(s):</label>
                                    <div class="col-sm-10">

                                        <input type="text" class="form-control" id="txtNom" name="txtNom" placeholder="" data-behavior = "only-alpha-space" />
                                    </div>
                                </div>
                                <br/>
                                <div class="row" >
                                    <label for="txtNom" style="margin-top: 10px" class="col-sm-2 control-label text-center">Unidad:</label>
                                    <div class="col-sm-10">

                                        <input type="text" class="form-control" id="txtUni" readonly name="txtUni"/>
                                    </div>
                                    <div class="col-md-3">
                                        <select id="selectsts" class="form-control">
                                            <option id="op">--Estatus--</option>
                                            <option value="A">Activo</option>
                                            <option value="S">Suspendido</option>
                                        </select>
                                    </div>

                                </div>
                                <br/>   
                                <div class="row" >
                                    <label for="txtUser" style="margin-top: 10px" class="col-sm-2 control-label text-center">Usuario:</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="txtUser" readonly name="txtUser"  data-behavior = "only-alphanum"/>
                                    </div>
                                    <label for="txtPass" style="margin-top: 10px" class="col-sm-1 control-label text-center">Password:</label> 
                                    <div class="col-sm-5">
                                        <input type="password" class="form-control" id="txtPass"  name="txtPass" data-behavior = "only-alphanum"/> (Sino ingresa una nueva contraseña se mantendrá la anterior).
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success" id="btnSave1" >Guardar</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" id="btnCancel1" >Cancelar</button>

                            </div>
                        </div>
                    </div>
                </div> 
                <button type="button" class="btn btn-success hidden " id="btnModal" data-toggle="modal" data-target="#myModal">
                </button>
                <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title text-center text-success" id="myModalLabel1">Nuevo usuario</h3>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <input type="hidden" id="txtIdUsu" >   
                                    <label for="ape_pat1"  class="col-sm-2 control-label text-center">Apellido Paterno:</label>
                                    <div class="col-sm-4">

                                        <input type="text" class="form-control" id="ape_pat1" name="ape_pat" placeholder="" data-behavior = "only-alpha"/>
                                    </div>
                                    <label for="ape_mat1"  class="col-sm-2 control-label text-center">Apellido Materno:</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="ape_mat1" name="ape_mat" placeholder="" data-behavior = "only-alpha"/>
                                    </div>
                                </div>
                                <br/>
                                <div class="row" >
                                    <label for="txtNom1" style="margin-top: 10px" class="col-sm-2 control-label text-center">Nombre(s):</label>
                                    <div class="col-sm-10">

                                        <input type="text" class="form-control" id="txtNom1" name="txtNom1" placeholder="" data-behavior = "only-alpha-space"/>
                                    </div>
                                </div>
                                <br/>
                                <div class="row" >
                                    <label for="txtUni1" style="margin-top: 10px" class="col-sm-2 control-label text-center">Unidad:</label>
                                    <div class="col-sm-10">

                                        <input type="text" class="form-control" id="txtUni1" readonly name="txtUni"/>
                                    </div>

                                </div>
                                <br/>
                                <div class="row" id="rolUsu" >
                                    <label for="txtUni1" style="margin-top: 10px" class="col-sm-2 control-label text-center">Perfil:</label>
                                    <div class="col-sm-5">
                                        <select class="form-control" id="selectRol">
                                            <option value="">--- Seleccione ---</option>
                                            <option value="1">FARMACIA</option>
                                            <option value="3">SUPERVISOR</option>
                                            <option value="5">RURAL</option>
                                        </select>
                                    </div>

                                </div>

                                <br/>   
                                <div class="row" >
                                    <label for="txtUser1" style="margin-top: 10px" class="col-sm-2 control-label text-center">Usuario:</label>
                                    <div class="col-sm-3">

                                        <input type="text" class="form-control" id="txtUser1" name="txtUser1" data-behavior = "only-alphanum"/>
                                    </div>
                                    <label for="txtPass1" style="margin-top: 10px" class="col-sm-2 control-label text-center">Password:</label>
                                    <div class="col-sm-3">

                                        <input type="password" class="form-control" id="txtPass1"  name="txtPass1" data-behavior = "only-alphanum"/>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success" id="btnSave" >Guardar</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" id="btnCancel" >Cancelar</button>

                            </div>
                        </div>
                    </div>
                </div>                    
            </div>
        </div>

        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="../../js/jquery-1.9.1.js"></script>
        <script src="../../js/bootstrap.js"></script>
        <script src="../../js/bootstrap-datepicker.js"></script>
        <script src="../../js/moment.js"></script>
        <script src="../../js/jquery.dataTables.js"></script>
        <script src="../../js/dataTables.bootstrap.js"></script>
        <script src="../../js/adminUsu/editUsu.js"></script>
        <script src="../../js/jquery.alphanum.js" type="text/javascript"></script>

        <script type="text/javascript">
                                                    var context = "${pageContext.servletContext.contextPath}";
                                                    $(document).ready(function () {
                                                        $('#datosPacientes').dataTable({
                                                            "bScrollCollapse": true,
                                                            "sScrollY": "400px",
                                                            "bProcessing": true,
                                                            "sPaginationType": "full_numbers",
                                                            "sDom": 'T<"clear">lfrtip'
                                                        });
                                                    });

                                                    $("[data-behavior~=only-alphanum]").alphanum({
                                                        allowSpace: false,
                                                        allowOtherCharSets: false
                                                    });

                                                    $("[data-behavior~=only-alpha]").alphanum({
                                                        allowNumeric: false,
                                                        allowSpace: false,
                                                        allowOtherCharSets: true,
                                                        forceUpper: true
                                                    });

                                                    $("[data-behavior~=only-alpha-space]").alphanum({
                                                        allowNumeric: false,
                                                        allowSpace: true,
                                                        allowOtherCharSets: true,
                                                        allowUpper: true,
                                                        forceUpper: true
                                                    });
        </script>    
    </body>
</html>
