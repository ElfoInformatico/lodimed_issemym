<%-- 
    Document   : folio
    Created on : 13/04/2015, 08:34:38 AM
    Author     : Americo
--%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    HttpSession sesion = request.getSession();
    String id_usu = "";
    id_usu = (String) session.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../../index.jsp");
        return;
    }
    ConectionDB con = new ConectionDB();
    String uni = "", domicilio = "";
    try {
        con.conectar();
        PreparedStatement ps;
        ps = con.getConn().prepareStatement("SELECT uni.des_uni, uni.domic FROM unidades AS uni INNER JOIN usuarios AS u ON u.cla_uni = uni.cla_uni WHERE u.id_usu = ?");
        ps.setString(1, id_usu);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            uni = rs.getString(1);
            domicilio = rs.getString(2);
        }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="../../css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../../css/pie-pagina.css" rel="stylesheet" media="screen">
        <link href="../../css/topPadding.css" rel="stylesheet">
        <link href="../../css/datepicker3.css" rel="stylesheet">
        <link href="../../css/select2.css" rel="stylesheet">
        <link href="../../css/dataTables.bootstrap.css" rel="stylesheet">
        <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <title>LODIMED</title>
    </head>
    <body>

        <%@include file="../../jspf/mainMenu.jspf" %> 
        <div class="container-fluid">
            <div class="container">
                <h1>Administración del Sistema</h1>

                <div class="row">
                    <div class="panel panel-default">
                        <div class="panel-heading"> <h3>Tipo de sistema:</h3></div>

                        <div class="panel-body">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="opciones" id="opciones_1" value="1">
                                    Farmacia
                                </label>
                            </div>
                            <div class="radio">
                                <label style="font: bold" >
                                    <input type="radio" name="opciones" id="opciones_2" value="2">
                                    Medico-Farmacia
                                </label>
                            </div>
                            <div class="radio">
                                <label style="font: bold" >
                                    <input type="radio" name="opciones" id="opciones_3" value="3">
                                    HL7
                                </label>
                            </div>
                            <div class="radio">
                                <label style="font: bold" >
                                    <input type="radio" name="opciones" id="opciones_4" value="4">
                                    Rural
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="panel panel-default">
                        <div class="panel-heading"> <h3>Tipo de captura en farmacia:</h3></div>

                        <div class="panel-body">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="opcionesCaptura" id="opcionesCptura1" value="1">
                                    Código de barras
                                </label>
                            </div>
                            <div class="radio">
                                <label style="font: bold" >
                                    <input type="radio" name="opcionesCaptura" id="opcionesCaptura2" value="2">
                                    Automática
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3>Folio de Recetas Colectivas</h3></div>

                        <div class="panel-body">
                            <form action="../../administrarSistema">

                                <div class="row">

                                    <h4 class="col-sm-2">Folio Actual:</h4>
                                    <div class="col-sm-2">
                                        <%
                                            String folioRec = "";
                                            ps = con.getConn().prepareStatement("select id_rec from indices");
                                            rs = ps.executeQuery();
                                            while (rs.next()) {
                                                folioRec = rs.getString("id_rec");
                                            }
                                        %>
                                        <input class="form-control" readonly value="<%=folioRec%>" />
                                    </div>
                                    <h4 class="col-sm-2">Nuevo Folio:</h4>
                                    <div class="col-sm-2">
                                        <input class="form-control" type="number" required min="1" name="id_rec" />
                                    </div>
                                </div>
                                <button class="col-lg-4 col-lg-offset-4 btn btn-success" name="accion" value="actualizarFolio" onclick="return confirm('Seguro que desea actualizar el Folio?')"><span class="glyphicon glyphicon-floppy-disk"></span> Actualizar folio</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3>Unidad Perteneciente</h3></div>
                        <div class="panel-body">
                            <form action="../../administrarSistema">
                                <div class="row">
                                    <h4 class="col-sm-1">Unidad asignada</h4>
                                    <div class="col-sm-6"><input class="form-control" id="unidad" value="<%=uni%>" disabled type="text"></div>
                                    <h4 class="col-sm-1">Domicilio</h4>
                                    <div class="col-sm-4"><input class="form-control" id="domicilio" value="<%=domicilio%>" disabled type="text"></div>
                                </div>

                                <div class="row">
                                    <h4 class="col-sm-1">Unidad</h4>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="uni" id="uni">
                                            <%
                                                ps = con.getConn().prepareStatement("SELECT u.cla_uni, u.des_uni, u.domic, u.tip_uni FROM unidades AS u");
                                                rs = ps.executeQuery();
                                                while (rs.next()) {
                                                    out.print("<option value='" + rs.getString(1) + "'>" + rs.getString(2)
                                                            + " | " + rs.getString(3) + " | " + rs.getString(4) + "</option>");
                                                }
                                            %>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <button name="accion" value="uni" class="col-sm-offset-4 col-sm-4 btn btn-success" title="Guardar Institución"><span class="glyphicon glyphicon-floppy-disk"></span> Actualizar unidad</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3>Frecuencia de paso</h3></div>
                        <div class="panel-body">
                            <div class="row">
                                <h4 class="col-lg-2 col-md-2 col-sm-2" >Frecuencia de paso:</h4>
                                <div class="col-lg-2 col-md-2 col-sm-2" >
                                    <input class="form-control" readonly id="frecActu" > 
                                </div>
                                <h4 class="col-lg-2 col-md-2 col-sm-2" >Frecuencia de paso nueva:</h4>
                                <div class="col-lg-2 col-md-2 col-sm-2" >
                                    <input class="form-control" type="number" id="newFrec" onkeyup="sumar();"  onkeypress="return isNumberKey(event, this);" min="1" > 
                                </div>
                                <button class="col-lg-offset-1 col-lg-2 col-md-2 col-sm-2 btn btn-success" type="button" id="btnNewFrec" ><span class="glyphicon glyphicon-floppy-disk"></span> Actualizar frecuencia</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3>Rango de folios de médicos</h3></div>
                        <div class="panel-body">
                            <div class="row">
                                <h4 class="col-lg-2 col-md-2 col-sm-2" >Rango actual:</h4>
                                <div class="col-lg-2 col-md-2 col-sm-2" >
                                    <input class="form-control" readonly id="rangFol" > 
                                </div>
                                <h4 class="col-lg-2 col-md-2 col-sm-2" >Nuevo rango:</h4>
                                <div class="col-lg-2 col-md-2 col-sm-2" >
                                    <input class="form-control" type="number" id="newRan" onkeyup="sumar();"  onkeypress="return isNumberKey(event, this);" min="1" > 
                                </div>

                                <button class="col-lg-offset-1 col-lg-2 col-md-2 col-sm-2 btn btn-success" type="button" id="btnNewRan" ><span class="glyphicon glyphicon-floppy-disk"></span> Actualizar rango</button>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3>Backups del sistema</h3></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <div class="checkbox">
                                        <label class="h4">
                                            <input type="checkbox" id="is24"> Jornada de 24 horas
                                        </label>
                                    </div>
                                </div>
                                <div id="horas">
                                    <h4 class="col-lg-2 col-md-2 col-sm-2" >Hora inicio:</h4>
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <select class="form-control col-lg-2 col-md-2 col-sm-2" name="inicio_hora" id="inicio_hora">
                                            <option value="8">08:00</option>
                                        </select>
                                    </div>

                                    <h4 class="col-lg-2 col-md-2 col-sm-2" >Hora fin:</h4>
                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                        <select class="form-control col-lg-2 col-md-2 col-sm-2" name="fin_hora" id="fin_hora">
                                            <option value="8">08:00</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <h4 class="col-lg-3 col-md-3 col-sm-3" >Número de backups diarios:</h4>
                                <div class="col-lg-1 col-md-1 col-sm-1" >
                                    <input class="form-control" id="backupsPerDay" type="number" min="3" value="3">
                                </div>
                                <h4 class="col-lg-offset-1 col-lg-3 col-md-3 col-sm-3" >Máximo de días a guardar:</h4>
                                <div class="col-lg-1 col-md-1 col-sm-1" >
                                    <input class="form-control" type="number" id="maximumBackups" min="7" value="7">
                                </div>
                            </div>
                            <div class="row">
                                <button class="col-lg-offset-4 col-lg-4 col-md-4 col-sm-4 btn btn-success" type="button" id="btnUpdate" ><span class="glyphicon glyphicon-floppy-disk"></span> Actualizar parámetros</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3>Reiniciar Sistema</h3></div>
                        <div class="panel-body">
                            <form action="../../administrarSistema">
                                <button class="btn btn-block btn-danger" name="accion" value="reiniciarExistenciasYRecetas" onclick="return confirm('Seguro que desea reiniciar sistema ?')">Existencias y Recetas</button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- 
================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="../../js/jquery-1.9.1.js"></script>
        <script src="../../js/bootstrap.js"></script>
        <script src="../../js/bootstrap-datepicker.js"></script>
        <script src="../../js/jquery.dataTables.js"></script>
        <script src="../../js/dataTables.bootstrap.js"></script>
        <script src="../../js/select2.js"></script>
        <script type="text/javascript">
                                    $(document).ready(function () {
                                        $("#is24").click(check24h);
                                        addHours(0, 23, $("#inicio_hora"));
                                        $("#inicio_hora").change(setHours);
                                        $("#btnUpdate").click(updateParameters);


                                        $.ajax({
                                            url: "${pageContext.servletContext.contextPath}/Operaciones",
                                            data: {ban: 4},
                                            type: 'POST',
                                            async: true,
                                            success: function (data)
                                            {
                                                var sis = data.sis;
                                                var $radios = $('input[name=opciones]');
                                                $radios.filter('[value=' + sis + ']').prop('checked', true);


                                            }, error: function (jqXHR, textStatus, errorThrown) {

                                                alert("Error Contactar al departamento de sistemas");

                                            }
                                        });
                                        $.ajax({
                                            url: "${pageContext.servletContext.contextPath}/Operaciones",
                                            data: {ban: 13},
                                            type: 'POST',
                                            async: true,
                                            success: function (data)
                                            {
                                                var cap = data.cap;
                                                var $radios = $('input[name=opcionesCaptura]');
                                                $radios.filter('[value=' + cap + ']').prop('checked', true);


                                            }, error: function (jqXHR, textStatus, errorThrown) {

                                                alert("Error Contactar al departamento de sistemas");

                                            }
                                        });

                                        $('input[name=opciones]').click(function ()
                                        {
                                            var sistema = $('input:radio[name=opciones]:checked').val();

                                            $.ajax({
                                                url: "${pageContext.servletContext.contextPath}/Operaciones",
                                                data: {ban: 3, sis: sistema},
                                                type: 'POST',
                                                async: false,
                                                success: function (data)
                                                {
                                                    alert(data.msj);

                                                }, error: function (jqXHR, textStatus, errorThrown) {

                                                    alert("Error Contactar al departamento de sistemas");

                                                }
                                            });

                                        });
                                        $('input[name=opcionesCaptura]').click(function ()
                                        {
                                            var captura = $('input:radio[name=opcionesCaptura]:checked').val();

                                            $.ajax({
                                                url: "${pageContext.servletContext.contextPath}/Operaciones",
                                                data: {ban: 12, captura: captura},
                                                type: 'POST',
                                                async: false,
                                                success: function (data)
                                                {
                                                    alert(data.msj);

                                                }, error: function (jqXHR, textStatus, errorThrown) {

                                                    alert("Error Contactar al departamento de sistemas");

                                                }
                                            });

                                        });

                                        $("#uni").select2();
                                    });

                                    $.ajax({
                                        url: "${pageContext.servletContext.contextPath}/Operaciones",
                                        data: {ban: 1},
                                        type: 'POST',
                                        async: true,
                                        success: function (data)
                                        {
                                            $("#rangFol").val(data.rango);

                                        }, error: function (jqXHR, textStatus, errorThrown) {

                                            alert("Error Contactar al departamento de sistemas");

                                        }
                                    });
                                    $.ajax({
                                        url: "${pageContext.servletContext.contextPath}/Operaciones",
                                        data: {ban: 9},
                                        type: 'POST',
                                        async: true,
                                        success: function (data)
                                        {
                                            $("#frecActu").val(data.frec);

                                        }, error: function (jqXHR, textStatus, errorThrown) {

                                            alert("Error Contactar al departamento de sistemas");

                                        }
                                    });

                                    $(function () {

                                        $("#btnNewRan").click(function ()

                                        {
                                            var e = confirm("¿Esta seguro de cambiar los rangos de los folios?");
                                            if (e) {
                                                var rangoNuevo = $("#newRan").val();
                                                if (rangoNuevo === "")
                                                {
                                                    alert("Seleccionar un rango válido");
                                                    $("#newRan").focus();
                                                    return false;
                                                }
                                                else if (rangoNuevo < 1)
                                                {
                                                    alert("Seleccionar un rango válido");
                                                    $("#newRan").focus();
                                                    return false;
                                                }

                                                $.ajax({
                                                    url: "${pageContext.servletContext.contextPath}/Operaciones",
                                                    data: {ban: 2, ran: rangoNuevo},
                                                    type: 'POST',
                                                    async: true,
                                                    success: function (data)
                                                    {
                                                        alert(data.msj);
                                                        location.reload();

                                                    }, error: function (jqXHR, textStatus, errorThrown) {

                                                        alert("Error Contactar al departamento de sistemas");

                                                    }
                                                });
                                            }
                                        });

                                        $("#btnNewFrec").click(function ()
                                        {
                                            var e = confirm("¿Esta seguro de cambiar la frecuencia de paso?");
                                            if (e) {
                                                var rangoNuevo = $("#newFrec").val();
                                                if (rangoNuevo === "")
                                                {
                                                    alert("Seleccionar una frecuencia de paso válida");
                                                    $("#newFrec").focus();
                                                    return false;
                                                }
                                                else if (rangoNuevo < 1)
                                                {
                                                    alert("Seleccionar una frecuencia de paso válida");
                                                    $("#newFrec").focus();
                                                    return false;
                                                }

                                                $.ajax({
                                                    url: "${pageContext.servletContext.contextPath}/Operaciones",
                                                    data: {ban: 10, frec: rangoNuevo},
                                                    type: 'POST',
                                                    async: true,
                                                    success: function (data)
                                                    {
                                                        alert(data.msj);
                                                        location.reload();

                                                    }, error: function (jqXHR, textStatus, errorThrown) {

                                                        alert("Error Contactar al departamento de sistemas");

                                                    }
                                                });
                                            }

                                        });


                                    });

                                    function isNumberKey(evt, obj)
                                    {
                                        var charCode = (evt.which) ? evt.which : event.keyCode;
                                        if (charCode === 13 || charCode > 31 && (charCode < 48 || charCode > 57)) {
                                            if (charCode === 13) {
                                                frm = obj.form;
                                                for (i = 0; i < frm.elements.length; i++)
                                                    if (frm.elements[i] === obj)
                                                    {
                                                        if (i === frm.elements.length - 1)
                                                            i = -1;
                                                        break
                                                    }
                                                if (frm.elements[i + 1].disabled === true)
                                                    tabular(e, frm.elements[i + 1]);
                                                else
                                                    frm.elements[i + 1].focus();
                                                return false;
                                            }
                                            return false;
                                        }
                                        return true;

                                    }
                                    function sumar() {
                                        var unidades = document.formulario_receta.unidades.value;
                                        if (unidades === "")
                                            unidades = 0;
                                        var horas = document.formulario_receta.horas.value;
                                        if (horas === "")
                                            horas = 0;
                                        var dias = document.formulario_receta.dias.value;
                                        if (dias === "")
                                            dias = 0;
                                        var hrs = 24 / parseInt(horas);
                                        var total = parseInt(unidades) * hrs * parseInt(dias);
                                        var amp = document.formulario_receta.amp.value;
                                        var cajas = total / amp;
                                        if (isNaN(amp))
                                            amp = 0;
                                        if (isNaN(cajas))
                                            cajas = 0;
                                        if (isNaN(total))
                                            total = 0;


                                        document.formulario_receta.piezas_sol.value = Math.ceil(unidades);
                                        document.formulario_receta.can_sol.value = Math.ceil(unidades);
                                    }

                                    function check24h() {
                                        if ($("#is24").is(':checked')) {
                                            $("#horas").hide();
                                        } else {
                                            $("#horas").show();
                                            //otra cosa
                                        }
                                    }

                                    function addHours(init, end, select) {

                                        select.empty();

                                        var label;
                                        for (var i = init, max = (end + 1); i < max; i++) {

                                            label = "";
                                            if (i < 10) {
                                                label = "0";
                                            }

                                            label += i + ":00";

                                            select.append($('<option>', {
                                                value: i,
                                                text: label
                                            }));
                                        }

                                    }

                                    function setHours() {
                                        var selected = parseInt($("#inicio_hora").val());
                                        var init = selected + 1;
                                        var end = 23;
                                        if (selected === 23) {
                                            init = 0;
                                            end = 22;
                                        }

                                        addHours(init, end, $("#fin_hora"));
                                    }

                                    function updateParameters() {

                                        var init = $("#inicio_hora option:selected").text();
                                        var end = $("#fin_hora option:selected").text();

                                        if (!$("#horas").is(':visible')) {
                                            init = "24";
                                            end = "";
                                        }

                                        var backups = $("#backupsPerDay").val();
                                        var maximum = $("#maximumBackups").val();

                                        $.ajax({
                                            url: "${pageContext.servletContext.contextPath}/Operaciones",
                                            data: {ban: 11, "init": init,
                                                "end": end, "backups": backups,
                                                "maximum": maximum},
                                            type: 'POST',
                                            async: true,
                                            success: function (data)
                                            {
                                                alert(data.msj);
                                                location.reload();

                                            }, error: function (jqXHR, textStatus, errorThrown) {

                                                alert("Error Contactar al departamento de sistemas");

                                            }
                                        });
                                    }


        </script>
    </body>
</html>
<%
    } catch (SQLException ex) {
        Logger.getLogger("folio.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
    } finally {
        try {
            if (con.estaConectada()) {
                con.cierraConexion();
            }
        } catch (SQLException ex) {
            Logger.getLogger("folio.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        }
    }
%>
