<%-- 
    Document   : index
    Created on : 07-mar-2014, 15:38:43
    Author     : Americo
--%>

<%@page import="Servlets.Catalogo"%>
<%@page import="Impl.RecetaImpl"%>
<%@page import="Servlets.recetas.PacienteNuevo"%>
<%@page import="Modelos.Receta"%>
<%@page import="Servlets.recetas.RecetaPorCB"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.util.Date"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("dd-MM-yyyy"); %>
<%    ConectionDB con = new ConectionDB();
    HttpSession sesion = request.getSession();
    String id_usu = "";
    String uni_ate = "", medico = "";
    int tipoSistema = 0;
    try {
        tipoSistema = (Integer) session.getAttribute("tipCap");
        id_usu = (String) session.getAttribute("id_usu");
        uni_ate = (String) session.getAttribute("cla_uni");
        medico = (String) session.getAttribute("id_usu");

        con.conectar();

        ResultSet rset = con.consulta("SELECT us.nombre, un.des_uni FROM usuarios us, unidades un WHERE us.cla_uni = un.cla_uni AND us.id_usu = '" + id_usu + "' ");
        while (rset.next()) {
            medico = rset.getString(1);
            uni_ate = rset.getString(2);
        }

        con.cierraConexion();
    } catch (SQLException ex) {
        Logger.getLogger("receta_colectiva.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
    } finally {
        try {
            if (con.estaConectada()) {
                con.cierraConexion();
            }
        } catch (SQLException ex) {
            Logger.getLogger("receta_colectiva.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        }
    }

    String fechaAct = df2.format(new Date());

%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="../css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../css/pie-pagina.css" rel="stylesheet" media="screen">
        <link href="../css/topPadding.css" rel="stylesheet">
        <link href="../css/cupertino/jquery-ui-1.10.3.custom.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/select2.css" rel="stylesheet" type="text/css"/>
        <link href="../css/sweetalert.css" rel="stylesheet" type="text/css"/>
        <link href="../css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery-1.9.1.js"></script>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <title>LODIMED</title>
        <script language="JavaScript">
            var isCanceled = false;
            $("#btn_capturar2").val("0");

            window.onbeforeunload = confirmExit;
            function confirmExit()
            {
                var envio = $("#btn_capturar2").val();
                if (envio === "1")
                {
                    isCanceled = false;
                    console.log("value: ", isCanceled);
                }
                else
                {
                    isCanceled = true;
                    console.log("value: ", isCanceled);
                    return "Ha intentado salir de esta pagina. Si ha realizado algun cambio en los campos sin hacer clic en el boton Guardar, los cambios se perderan. Seguro que desea salir de esta pagina? ";
                }
            }

            window.onunload = cancelCurrent;
            function cancelCurrent() {
                console.log("value: ", isCanceled);
                if (isCanceled) {
                    console.log("Aquí debería cancelar la recta actual.");
                    cancelarReceta();
                }
            }

        </script>

    </head>
    <body>
        <%@include file="../jspf/mainMenu.jspf"%>

        <div class="container-fluid">
            <div class="container">
                <h3>Captura de Recetas Colectiva</h3>


                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-1">
                                <label for="uni_ate" class="control-label"> Unidad de Salud:</label>
                                <input type="hidden" id="tipCaptura" name="tipCaptura" value="<%=tipoSistema%>">
                            </div>
                            <div class="col-md-11">
                                <input type="text" class="form-control" id="uni_ate" readonly name="uni_ate" placeholder="" value="<%=uni_ate%>"/>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-sm-1">
                                <label for="medico" class="control-label"> Usuario:</label>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="medico" readonly name="medico" placeholder="" value="<%=medico%>" data-behavior="only-alphanum"/>
                            </div>
                            <label for="fecha" class="col-sm-1 control-label">Fecha</label>
                            <div class="col-sm-2">
                                <input type="text" class="input-sm form-control" id="fecha1" name="fecha1"  value="<%=fechaAct%>"/>
                            </div>
                            <label for="fecha" class="col-sm-1 control-label" id="lblFolio">Folio</label>
                            <div class="col-sm-2">
                                <input name="folio" type="text" class="form-control" id="folio" placeholder="Folio" required data-behavior="only-alphanum">
                            </div>
                            <div class="col-sm-1" id="descartarReceta" hidden>
                                <button class="btn btn-block btn-danger btn-sm" id="btnDescartar" name="btnDescartar" type="button" ><span class="glyphicon glyphicon glyphicon-remove"></span></button>
                            </div>
                        </div>
                        <br />
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <label class="col-sm-2">Servicio:</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="listServicios" id="listServicios" data-toggle="tooltip" title="Servicio de la receta">
                                    <option value="0">--Servicio--</option>
                                    <%
                                        try {
                                            con.conectar();
                                            ResultSet rset = con.consulta("select id_ser, nom_ser from servicios where id_ser!=1");
                                            while (rset.next()) {
                                    %>
                                    <option value="<%=rset.getString("id_ser")%>"><%=rset.getString("nom_ser")%></option>
                                    <%
                                            }
                                        } catch (SQLException ex) {
                                            Logger.getLogger("receta_colectiva.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                        } finally {
                                            try {
                                                if (con.estaConectada()) {
                                                    con.cierraConexion();
                                                }
                                            } catch (SQLException ex) {
                                                Logger.getLogger("receta_colectiva.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                            }
                                        }
                                    %>
                                </select>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-sm-2">
                                <label for="medico_jq" class="control-label">
                                    <button type="button" class="btn btn-default" data-placement="left" data-toggle="tooltip" data-placement="left" title="Buscar Médico por su nombre" id="bus_pacn"><span class="glyphicon glyphicon-search"></span></button>
                                    Buscar médico
                                </label>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="medico_jq" name="medico_jq" placeholder="Médico" onkeypress="return tabular(event, this);" data-toggle="tooltip" title="M&eacute;dico responsable" data-behavior="only-alphanum">
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-block btn-success" name="mostrar2" id="mostrar2">Mostrar</button>
                            </div>
                            <div class="col-sm-2">
                                <a class="btn btn-block btn-success" onclick="window.open('../farmacia/catalogoMedicos.jsp', '', 'width=1200,height=800,left=50,top=50,toolbar=no')">Médicos - <span class="glyphicon glyphicon-list"></span></a>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-sm-2">
                                <label for="nom_pac" class="control-label">Médico</label>
                            </div>
                            <div class="col-sm-6">
                                <input name="nom_med" type="text" class="form-control" id="nom_med" placeholder="Médico" readonly data-behavior="only-alphanum"/>
                            </div>
                            <div class="col-sm-1">
                                <label for="sexo" class="control-label">Cédula</label>
                            </div>
                            <div class="col-sm-3">
                                <input name="cedula" type="text" class="form-control" id="ced_med" placeholder="Cédula" readonly data-behavior="only-alphanum"/>
                            </div>                                
                        </div> 
                    </div>
                    <div id="captura" hidden class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="col-sm-2">
                                    <label for="cla_pro" class="control-label">Clave</label>
                                </div>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="cla_pro" name="cla_pro" placeholder="Clave" onkeypress="return tabular(event, this);"  value="" data-toggle="tooltip" title="Clave del medicamento" data-behavior="only-alphanum"/>
                                </div>
                                <div class="col-sm-3">
                                    <button class="btn btn-block btn-success" name="btn_clave" id="btn_clave">Clave</button>
                                    <br>
                                </div>
                                <div class="col-sm-2">
                                    <label for="des_pro" class="control-label">Descripción</label>
                                </div>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="des_pro" name="des_pro" placeholder="Descripción"  onkeypress="return tabular(event, this);"  value="" data-toggle="tooltip" title="Descripci&oacute;n del medicamento" data-behavior="only-alphanum">
                                </div>
                                <div class="col-sm-3">
                                    <button class="btn btn-block btn-success" name="btn_descripcion" id="btn_descripcion">Descripción</button>
                                </div>
                            </div>
                        </div>


                        <br>

                        <div class="row">
                            <div class="col-lg-12">
                                <table align="center">
                                    <tr>
                                        <td width="70"><input type="text" class="form-control" name="unidades" id="unidades" placeholder="" onkeyup="sumar();"
                                                              data-toggle="tooltip" title="Cantidad a surtir"/></td>
                                        <td><b>Cajas</b></td>                                            
                                        <td></td>
                                        <td></td>
                                        <td><!--b>Piezas Solicitadas</b--></td>
                                        <td><input type="text" class="hidden" id="piezas_sol" name="piezas_sol" placeholder="0" size="1" value="" readonly></td>
                                        <td><b>Total Cajas</b></td>
                                        <td><input type="text" class="form-control" id="can_sol" name="can_sol" placeholder="0" size="1"  onkeypress="return tabular(event, this);" value="" readonly="" ></td>
                                    </tr>
                                </table>

                            </div>

                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <button class="btn btn-block btn-success" id="btn_capturar" name="btn_capturar" type="button">Capturar</button>
                                <button class="hidden" id="btn_capturar2" name="btn_capturar2" type="button" onclick="">Capturar</button>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer" hidden id="tablaMedicamento">
                        <div class="row">
                            <table class="table table-striped table-bordered" id="tablaMedicamentos">
                            </table>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-lg-offset-3">
                                <button class="btn btn-success btn-block" name="accion" id="validarBtn" type="button">Validar Receta</button>
                            </div>
                        </div>
                    </div>

                    <div class="panel-footer" hidden id="surtimiento">
                        <div class="row">
                            <label class="col-sm-2">Código de barras</label>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm" data-behavior="only-alphanum" id="cb" name="cb" placeholder="Escaneé el CB" onkeypress="return tabular(event, this);" autofocus>
                                    <span class="input-group-btn">
                                        <button class="btn btn-danger input-sm" type="button" id="limpiarCB" name="limpiarCB"><i class="fa fa-times" aria-hidden="true"></i></button>
                                    </span>
                                </div>
                            </div>
                            <label class="col-sm-1">Clave</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control input-sm" data-behavior="only-alphanum" id="claveCB" name="claveCB" onkeypress="return tabular(event, this);" disabled>
                            </div>
                            <label class="col-sm-1">Lote</label>
                            <div id="loteTXT" class="col-sm-3">
                                <input type="text" class="form-control input-sm" data-behavior="only-alphanum" id="lote" name="lote" onkeypress="return tabular(event, this);" placeholder="Lote">
                                <br>
                            </div>
                            <div id="loteSLC" class="col-sm-3" hidden>
                                <select class="form-control" name="loteSelect" id="loteSelect">
                                    <option value=-1>-- Lote a entregar --</option>
                                </select>
                                <br>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2">Caducidad</label>
                            <div class="col-sm-2">
                                <input value="" type="text" class="form-control input-sm" data-behavior="only-alphanum" id="caducidadCB" name="caducidadCB" disabled>
                            </div>
                            <label class="col-sm-1 col-sm-offset-1">Solicitado</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control input-sm" data-behavior="only-alphanum" id="solicitadoCB" name="solicitadoCB" onkeypress="return tabular(event, this);" disabled>
                            </div>
                            <label class="col-sm-1">Surtido</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control input-sm" data-behavior="only-alphanum" id="surtidoCB" name="surtidoCB" onkeypress="return tabular(event, this);" placeholder="Cantidad a entregar">
                                <br>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-sm-4 col-sm-offset-4">
                                <button class="btn btn-block btn-success" type="button" id="validarDetalle"><i class="fa fa-check" aria-hidden="true"></i>  Validar producto</button>
                            </div>  
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <h5><b>Surtido</b></h5>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-condensed"  id="tablaSurtido">
                            <thead>
                                <tr>
                                    <th>Clave</th>
                                    <th>Lote</th>
                                    <th>Caducidad</th>
                                    <th>Solicitado</th>
                                    <th>Surtido</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                        <hr>

                        <button class="btn btn-block btn-success" type="button" id="guardarReceta"><span class="glyphicon glyphicon-floppy-disk"></span>  Finalizar Receta</button>
                    </div>
                </div>  
            </div>  
        </div>
    </div>
</div>
<!--div id="footer">
    <div class="container">
        <p class="text-muted">Place sticky footer content here.</p>
    </div>
</div-->

<!-- 
    ================================================== -->
<!-- Se coloca al final del documento para que cargue mas rapido -->
<!-- Se debe de seguir ese orden al momento de llamar los JS -->
<script src="../js/bootstrap.js"></script>
<script src="../js/jquery-ui.js"></script>
<script src="../js/jquery.maskedinput.min.js" type="text/javascript"></script>
<script src="../js/jquery.alphanum.js" type="text/javascript"></script>
<script src="../js/sweetalert.min.js" type="text/javascript"></script>
<script src="../js/select2.js" type="text/javascript"></script>
<script src="../js/jquery.dataTables.js" type="text/javascript"></script>
<script src="../js/js_colectiva.js"></script>
<script>

                                    var CANCELAR = "<%=RecetaPorCB.CANCELAR_RECETA_ACCION%>";
                                    var ENCABEZADO = "<%=RecetaPorCB.CREAR_ENCABEZADO_ACCION%>";
                                    var CAPTURA = "<%=RecetaPorCB.CAPTURA_DETALLE_ACCION%>";
                                    var COLECTIVO = "<%=Receta.COLECTIVA_TIPO%>";
                                    var RECARGAR_DETALLES = "<%=RecetaPorCB.RECARGAR_DETALLES_ACCION%>";
                                    var CANCELAR_DETALLE = "<%=RecetaPorCB.CANCELAR_DETALLE_ACCION%>";
                                    var VALIDAR = "<%=RecetaPorCB.VALIDAR_RECETA_ACCION%>";
                                    var LOTE = "<%=RecetaPorCB.OBTENER_LOTES_ACCION%>";
                                    var CAMBIO = <%=RecetaImpl.NO_ENCONTRADO%>;
                                    var CAMBIAR = "<%=RecetaPorCB.CAMBIAR_LOTE_ACCION%>";
                                    var SURTIR = "<%=RecetaPorCB.SURTIR_LOTE_ACCION%>";
                                    var FINALIZAR = "<%=RecetaPorCB.FINALIZAR_RECETA_ACCION%>";
                                    var VALIDAR_ENCABEZADO = "<%=RecetaPorCB.VALIDAR_ENCABEZADO_ACCION%>";
                                    var CODIGO_BARRAS_CAPTURA = "<%=RecetaImpl.CODIGO_BARRAS_CAPTURA%>";
                                    var descripcion_productos = [];
                                    var claves_productos = [];

                                    $('#medico_jq').tooltip({'trigger': 'hover focus', 'position': {my: "top center", at: "center top-25"}});
                                    // $('#fecha1').tooltip({'trigger': 'hover focus', 'position': {my: "top center", at: "center top-25"}});
                                    $('#id_ser').tooltip({'trigger': 'hover focus', 'position': {my: "top center", at: "center top-25"}});
                                    $('#cla_pro').tooltip({'trigger': 'hover focus', 'position': {my: "top center", at: "center top-25"}});
                                    $('#des_pro').tooltip({'trigger': 'hover focus', 'position': {my: "top center", at: "center top-25"}});
                                    $('#unidades').tooltip({'trigger': 'hover focus', 'position': {my: "top center", at: "center top-25"}});
                                    $('#fecha1').mask("99-99-9999", {placeholder: " "});
                                    $("#fecha1").datepicker({
                                        dateFormat: 'dd-mm-yy',
                                        changeYear: true,
                                        changeMonth: true,
                                        maxDate: 'today',
                                        yearRange: "1910:2020",
                                        onClose: function (dateText, inst) {
                                            if ($(this).val() === "undefined") {
                                                alert("");
                                                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                                                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                                                $(this).datepicker('setDate', new Date(year, month, 1));
                                            }
                                        }

                                    });

                                    $(function () {
                                        $("#listServicios").select2();

                                        $.ajax({
                                            url: "${pageContext.servletContext.contextPath}/Catalogo",
                                            data: {ban: <%=Catalogo.OBTENER_CATALAGO_PRODUCTOS%>},
                                            type: 'POST',
                                            async: true,
                                            dataType: 'json',
                                            success: function (data)
                                            {
                                                data.forEach(function (value, id, arr) {
                                                    descripcion_productos.push(value.descripcion);
                                                    claves_productos.push(value.id);
                                                });
                                                $("#des_pro").autocomplete({
                                                    source: descripcion_productos
                                                });
                                                $("#cla_pro").autocomplete({
                                                    source: claves_productos
                                                });
                                            }, error: function (jqXHR, textStatus, errorThrown) {

                                                alert("Error Contactar al departamento de sistemas");

                                            }
                                        });
                                    });


                                    $("[data-behavior~=only-num]").numeric({
                                        allowMinus: false,
                                        allowThouSep: false
                                    });
                                    $("[data-behavior~=only-alphanum]").alphanum({
                                        allowOtherCharSets: true,
                                        disallow: "'",
                                        allow: ".-/():,%"
                                    });

                                    var config_num = {
                                        allowMinus: false,
                                        allowThouSep: false,
                                        min: 1,
                                        max: 5000
                                    };

                                    $("#unidades").numeric(config_num);


</script>

</body>

</html>
