<%-- 
    Document   : index
    Created on : 07-mar-2014, 15:38:43
    Author     : Americo
--%>

<%@page import="Servlets.Catalogo"%>
<%@page import="Modelos.Usuario"%>
<%@page import="Impl.adminUsuDaoImpl"%>
<%@page import="Impl.RecetaImpl"%>
<%@page import="Servlets.recetas.PacienteNuevo"%>
<%@page import="Servlets.recetas.RecetaPorCB"%>
<%@page import="Modelos.Receta"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%    ConectionDB con = new ConectionDB();
    HttpSession sesion = request.getSession();
    String id_usu = (String) sesion.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }

    int tipoSistema = (Integer) sesion.getAttribute("tipCap");
    int rol = (Integer) sesion.getAttribute("rol");
    String nombreSistema = "Manual", script = "";
    if (rol == Usuario.MEDICO_ROL) {
        nombreSistema = "Médico";
        script = "<script src=\"../js/receta_medico.js\"></script>";
        //Se indica quien crea la receta.
        rol = Receta.MEDICO_TIPO;
    }

%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="../css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../css/pie-pagina.css" rel="stylesheet" media="screen">
        <link href="../css/topPadding.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/cupertino/jquery-ui-1.10.3.custom.css" rel="stylesheet">
        <link href="../css/sweetalert.css" rel="stylesheet" type="text/css"/>
        <link href="../css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <title>LODIMED</title>
        <script language="JavaScript">
            var isCanceled = false;

            window.onbeforeunload = confirmExit;
            function confirmExit()
            {
                var envio = $("#btn_capturar").val();
                if (envio === "1")
                {
                    isCanceled = false;
                    console.log("value: ", isCanceled);
                } else
                {
                    isCanceled = true;
                    console.log("value: ", isCanceled);
                    return "Ha intentado salir de esta pagina. Si ha realizado algun cambio en los campos sin hacer clic en el boton Guardar, los cambios se perderan. Seguro que desea salir de esta pagina? ";
                }
            }

            window.onunload = cancelCurrent;
            function cancelCurrent() {
                console.log("value: ", isCanceled);
                if (isCanceled) {
                    console.log("Aquí debería cancelar la recta actual.");
                    cancelarReceta();
                }
            }

        </script>
    </head>
    <body>
        <%@include file="../jspf/mainMenu.jspf"%>
        <br/>

        <div class="container-fluid">
            <div class="col-sm-12">

                <div class="row">
                    <div class="col-md-4"><h3>Receta <%=nombreSistema%> - Farmacia</h3> </div>
                    <div class="col-md-4"><img src="../imagenes/isem.png" title="Logo" height="50" alt="Logo"></div> 
                    <div class="col-md-4"><img src="../imagenes/gobierno.png" title="Logo" height="50" alt="Logo"></div> 
                </div>

                <form class="form-horizontal" role="form" name="formulario_receta" id="formulario_receta" method="get" action="../Receta">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <label class="col-sm-1 control-label"> Unidad de Salud:</label>
                                <div class="col-md-4">
                                    <input type="text" class="input-sm form-control" id="uni_ate" readonly name="uni_ate" placeholder="" />
                                    <input type="hidden" name="tipRec" value="RM">
                                    <input type="hidden" name="banderaFolio" id="banderaFolio">
                                    <input type="hidden" id="tipCaptura" name="tipCaptura" value="<%=tipoSistema%>">
                                    <input type="hidden" name="tipRecNum" id="tipRecNum" value="1">
                                </div>
                                <label for="prefijo" class="col-sm-1 control-label">Prefijo</label>
                                <div class="col-sm-1">
                                    <input name="prefijo" type="text" class="input-sm form-control" id="prefijo" readonly >
                                </div>    
                                <label for="folio" class="col-sm-1 control-label">Folio</label>
                                <div class="col-sm-1 has-error">
                                    <input name="folio" type="text" data-behavior="only-alphanum" class="input-sm form-control" id="folio" placeholder="Folio" style="color:red;" autocomplete="off">

                                    <input name="idRec" type="text" class="hidden" id="idRec">
                                </div>
                                <div class="col-sm-1" id="descartarReceta" hidden>
                                    <button class="btn btn-block btn-danger btn-sm" id="btnDescartar" name="btnDescartar" type="button" ><span class="glyphicon glyphicon glyphicon-remove"></span></button>
                                </div>
                                <label class="col-sm-1 control-label">Fecha</label>
                                <div class="col-sm-1">
                                    <input type="text" class="input-sm form-control" id="fecha1" name="fecha" />
                                </div>
                            </div>

                            <br/>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class=" panel panel-default">
                                        <br>
                                        <label class="col-sm-3">Tipo de Consulta</label>
                                        <div class="col-sm-9">
                                            <select class="form-control" name="tipoCons" id="tipoCons">
                                                <option value="0">--Seleccione--</option>
                                                <option value="Consulta Externa">Consulta Externa</option>
                                                <option value="Urgencias">Urgencias</option>
                                                <option value="Hospitalizacion">Hospitalización</option>
                                            </select>
                                            <br>
                                        </div>
                                        <div class="col-sm-12">
                                            <button type="button" class="btn btn-default btn-sm" data-placement="left" data-toggle="tooltip" title="Buscar el médico por su nombre" ><span class="glyphicon glyphicon-search"></span></button>
                                            <strong>Médico</strong>
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control input-sm" data-behavior="only-alphanum" id="medico_jq" name="medico_jq" placeholder="Nombre del Médico" onkeypress="return tabular(event, this);" autofocus>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="hidden" id="m3" name="m3" value="si">
                                            <button class="btn btn-block btn-success btn-sm" name="mostrar3" id="mostrar3"><span class="glyphicon glyphicon-ok"></span> Mostrar</button>
                                        </div>
                                        <div class="col-sm-6">
                                            <a class="btn btn-block btn-success btn-sm" onclick="window.open('../farmacia/catalogoMedicos.jsp', '')"><span class="glyphicon glyphicon-th-list"></span> Ver Médicos</a>
                                        </div>


                                        <br/>
                                        <div class="col-sm-12">
                                            <strong>Nombre</strong>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" class="input-sm form-control" id="medico" readonly name="medico" placeholder="" />
                                        </div>
                                        <div class="col-sm-12">
                                            <strong>Cédula</strong>
                                        </div>
                                        <!--div class="col-md-12">Tipo Consulta:</div-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-12">
                                                    <input type="text" class="input-sm form-control" id="cedula" readonly name="cedula" placeholder=""/>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>                                       
                                        <div class="col-sm-12">
                                            <button type="button" class="btn btn-default btn-sm" data-placement="left" data-toggle="tooltip" title="Buscar Paciente por folio de paciente" id="bus_pac"><span class="glyphicon glyphicon-search"></span></button>
                                            <strong>Paciente - No. SP</strong>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" class="input-sm form-control" data-behavior="only-num" id="sp_pac" onkeypress="return tabular(event, this);" name="sp_pac" placeholder="Clave Paciente"  value=""/>
                                        </div>
                                        <div class="col-sm-6">
                                            <button class="btn btn-block btn-success btn-sm" name="mostrar1" id="mostrar1"><span class="glyphicon glyphicon-ok"></span> Mostrar</button>
                                        </div>
                                        <br/>
                                        <div class="col-sm-12">
                                            <select class="input-sm form-control" id="select_pac" name="select_pac">
                                                <option>Seleccione Nombre</option>
                                            </select>
                                            <br>
                                        </div>
                                        <br/>
                                        <div class="col-sm-12">
                                            <button type="button" class="btn btn-default btn-sm" data-placement="left" data-toggle="tooltip" title="Buscar Paciente por su nombre"><span class="glyphicon glyphicon-search"></span></button>
                                            <strong>Nombre</strong>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="col-sm-12">
                                                    <input type="text" data-behavior="only-alphanum" class="input-sm form-control" id="nombre_jq" name="nombre_jq" placeholder="Nombre" onkeypress="return tabular(event, this);" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="col-sm-6">
                                                    <button class="btn btn-block btn-success btn-sm" name="mostrar2" id="mostrar2"><span class="glyphicon glyphicon-ok"></span> Mostrar</button>
                                                </div>

                                                <div class="col-sm-6">
                                                    <a href="../admin/pacientes/pacientes.jsp" class="btn btn-success btn-sm btn-block" target="_blank"><span class="glyphicon glyphicon-th-list"></span> Ver Pacientes</a>
                                                </div>

                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="col-sm-12">
                                                    <strong>Paciente</strong>
                                                </div>
                                                <div class="col-sm-12">
                                                    <input name="nom_pac" type="text" class="input-sm form-control" id="nom_pac" placeholder="Paciente" readonly/>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label for="sexo" class="col-sm-2 control-label">Sexo</label>
                                                <div class="col-sm-4">
                                                    <input name="sexo" type="text" class="input-sm form-control" id="sexo" placeholder="Sexo"   readonly/>
                                                </div>
                                                <label for="fec_nac" class="col-sm-2 control-label">Fecha Nac.</label>
                                                <div class="col-sm-4">
                                                    <input name="fec_nac" type="text" class="input-sm form-control" id="fec_nac" placeholder="Fecha de Nacimiento"  readonly>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label for="txt_carnet" class="col-sm-2 control-label">Exp.</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="input-sm form-control" id="txt_carnet" name="txt_carnet" onkeypress="return tabular(event, this);" placeholder="Expediente" readonly=""/>
                                                </div>
                                                <label for="fol_sp" class="col-sm-2 control-label">Folio DH.</label>
                                                <div class="col-sm-4">
                                                    <input name="fol_sp" type="text" class="input-sm form-control" id="fol_sp" placeholder="Folio SP." readonly/>
                                                </div>
                                            </div>
                                        </div>

                                        <br/>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="col-sm-12">
                                                    <a id="nuevoPaciente" class="btn btn-default btn-sm btn-block" onkeypress="return tabular(event, this);" href="../admin/pacientes/alta_pacientes.jsp" target="_blank" ><span class="glyphicon glyphicon-plus-sign"></span> Nuevo paciente</a>
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="col-md-12 hidden ">Observaciones</div>
                                        <div class="col-md-12 hidden ">
                                            <textarea class="form-control" name="observaciones" onkeyup="$('#observa').val(this.value)"></textarea>
                                        </div>
                                        <br/>
                                    </div>
                                </div>
                                <div class="col-sm-8" id="capturaReceta" hidden>
                                    <div class=" panel panel-default">

                                        <br/>
                                        <div class="panel-body">
                                            <div class="row">
                                                <label class="col-sm-2">Diagnóstico</label>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <label class="col-lg-2">Primario</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="input-sm form-control" data-behavior="only-alphanum-enter" id="causes" name="causes" placeholder="CIE-10" size="1"  onkeypress="return tabular(event, this);">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-lg-2">Secundario</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="input-sm form-control" data-behavior="only-alphanum-enter" id="causes2" name="causes2" placeholder="CIE-10" size="1"  onkeypress="return tabular(event, this);">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <br>
                                                <div class="col-sm-3 col-sm-offset-2">
                                                    <!--a class="btn btn-danger btn-sm btn-block" id="borraCauses"><span class="glyphicon glyphicon-remove-circle"></span> Limpiar causes</a-->
                                                </div>
                                                <div class="col-sm-3">
                                                    <a class="btn btn-success btn-sm btn-block" onclick="window.open('../farmacia/catalogoCIE.jsp', '_blank')"><span class="glyphicon glyphicon-list"></span> Catálogo CIE-10</a>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <strong class="col-sm-6">Captura de Medicamentos</strong>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="col-sm-12">
                                                        <button type="button" class="btn btn-default btn-sm" data-placement="left" data-toggle="tooltip" title="Buscar el medicamento por clave." id="busc_clave"><span class="glyphicon glyphicon-search"></span></button>
                                                        <strong>Clave</strong>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="input-sm form-control" data-behavior="only-num" id="cla_pro" name="cla_pro" placeholder="Clave" onkeypress="return tabular(event, this);"  value=""/>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <button class="btn btn-block btn-success btn-sm" name="btn_clave" id="btn_clave"><span class="glyphicon glyphicon-search"></span> Clave</button>
                                                        <br>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <button type="button" class="btn btn-default btn-sm" data-placement="left" data-toggle="tooltip" title="Buscar el medicamento por descripción." id="busc_desc"><span class="glyphicon glyphicon-search"></span></button>
                                                        <strong>Descripción</strong>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <textarea class="input-sm form-control" id="des_pro" name="des_pro" placeholder="Descripción"  onkeypress="return tabular(event, this);"></textarea>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <button class="btn btn-block btn-success btn-sm" name="btn_descripcion" id="btn_descripcion"><span class="glyphicon glyphicon-search"></span> Descripción</button>
                                                    </div>
                                                </div>

                                            </div>
                                            <hr>
                                            <div class="row">
                                                <strong class="col-sm-6">Indicaciones</strong>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <input type="text" class="input-sm form-control" name="unidades" id="unidades" placeholder="" onkeyup="sumar();" value=""/> 
                                                </div>
                                                <div class="col-sm-2">
                                                    <strong>Caja(s)/Frasco(s),</strong>
                                                </div>
                                                <div class="col-sm-1">
                                                    <strong>por</strong>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" class="input-sm form-control" name="dias" id="dias" placeholder="" onkeyup="sumar();" value=""/>
                                                </div>
                                                <div class="col-sm-2">
                                                    <strong>días. Total Cajas</strong>
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" class="hidden" id="piezas_sol" name="piezas_sol" placeholder="0" size="1" value="" readonly>
                                                    <input type="text" class="input-sm form-control" id="can_sol" name="can_sol" placeholder="0" size="1"  onkeypress="return tabular(event, this);" value="" readonly >
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <textarea class="form-control" placeholder="Observaciones al paciente." name="indicaciones" id="indicacionesRF"></textarea>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4 col-sm-offset-3">

                                                    <button class="btn btn-block btn-success btn-sm" id="btn_capturar2" name="btn_capturar2" type="button" onclick="validaCauses()"><span class="glyphicon glyphicon glyphicon-open"></span> Capturar</button>
                                                    <button class="hidden" id="btn_capturar" name="btn_capturar" type="button" onclick="">Capturar</button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel-footer"  id="tablaMedicamentoMod">
                                            <table class="table table-striped table-bordered table-condensed"  id="tablaMedicamentos">
                                                <tr>
                                                    <td>Clave</td>
                                                    <td>Descripción</td>
                                                    <td>Cant. Sol.</td>
                                                    <td>Cant. Sur.</td>
                                                    <td>Origen</td>
                                                    <td></td>
                                                </tr>
                                            </table>

                                            <div id="valirReceta" class="row" hidden>
                                                <div class="col-sm-12">
                                                    <button class="btn btn-block btn-success" type="button" id="printImpr"><i class="fa fa-shopping-basket" aria-hidden="true"></i>  Surtir Receta</button>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                                <div class="col-sm-9" id="surtimiento" hidden>


                                    <div class=" panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <label class="col-sm-2">Código de barras</label>
                                                <div class="col-sm-3">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm" data-behavior="only-alphanum" id="cb" name="cb" placeholder="Escaneé el CB" onkeypress="return tabular(event, this);" autofocus>
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-danger input-sm" type="button" id="limpiarCB" name="limpiarCB"><i class="fa fa-times" aria-hidden="true"></i></button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <label class="col-sm-1">Clave</label>
                                                <div class="col-sm-2">
                                                    <input type="text" class="form-control input-sm" data-behavior="only-alphanum" id="claveCB" name="claveCB" onkeypress="return tabular(event, this);" disabled>
                                                </div>
                                                <label class="col-sm-1">Lote</label>
                                                <div id="loteTXT" class="col-sm-3">
                                                    <input type="text" class="form-control input-sm" data-behavior="only-alphanum" id="lote" name="lote" onkeypress="return tabular(event, this);" placeholder="Lote">
                                                    <br>
                                                </div>
                                                <div id="loteSLC" class="col-sm-3" hidden>
                                                    <select class="form-control" name="loteSelect" id="loteSelect">
                                                        <option value=-1>-- Lote a entregar --</option>
                                                    </select>
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-2">Caducidad</label>
                                                <div class="col-sm-2">
                                                    <input value="" type="text" class="form-control input-sm" data-behavior="only-alphanum" id="caducidadCB" name="caducidadCB" disabled>
                                                </div>
                                                <label class="col-sm-1 col-sm-offset-1">Solicitado</label>
                                                <div class="col-sm-2">
                                                    <input type="text" class="form-control input-sm" data-behavior="only-alphanum" id="solicitadoCB" name="solicitadoCB" onkeypress="return tabular(event, this);" disabled>
                                                </div>
                                                <label class="col-sm-1">Surtido</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control input-sm" data-behavior="only-alphanum" id="surtidoCB" name="surtidoCB" onkeypress="return tabular(event, this);" placeholder="Cantidad a entregar">
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="row">

                                                <div class="col-sm-4 col-sm-offset-4">
                                                    <button class="btn btn-block btn-success" type="button" id="validarDetalle"><i class="fa fa-check" aria-hidden="true"></i>  Validar producto</button>
                                                </div>  
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h5><b>Surtido</b></h5>
                                                </div>
                                            </div>
                                            <table class="table table-striped table-bordered table-condensed"  id="tablaSurtido">
                                                <thead>
                                                    <tr>
                                                        <th>Clave</th>
                                                        <th>Lote</th>
                                                        <th>Caducidad</th>
                                                        <th>Solicitado</th>
                                                        <th>Surtido</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>

                                            <hr>

                                            <button class="btn btn-block btn-success" type="button" id="guardarReceta"><span class="glyphicon glyphicon-floppy-disk"></span>  Finalizar Receta</button>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </form>


            </div>
        </div>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="text-center" id="imagenCarga">
                            <img src="../imagenes/ajax-loader-1.gif" />
                        </div>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>

        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery-ui.js"></script>
        <script src="../js/jquery.maskedinput.min.js" type="text/javascript"></script>
        <script src="../js/jquery.alphanum.js" type="text/javascript"></script>
        <script src="../js/sweetalert.min.js" type="text/javascript"></script>
        <script src="../js/jquery.dataTables.js" type="text/javascript"></script>
        <script src="../js/js_manual.js"></script>
        <script>
                                                        var FOLIO_MEDICO = "<%=RecetaPorCB.OBTENER_FOLIO_MEDICO%>";
                                                        var CANCELAR = "<%=RecetaPorCB.CANCELAR_RECETA_ACCION%>";
                                                        var ENCABEZADO = "<%=RecetaPorCB.CREAR_ENCABEZADO_ACCION%>";
                                                        var CAPTURA = "<%=RecetaPorCB.CAPTURA_DETALLE_ACCION%>";
                                                        var BUSCAR_PACIENTE = "<%=PacienteNuevo.BUSCAR_POR_NOMBRE%>";
                                                        var RECARGAR_DETALLES = "<%=RecetaPorCB.RECARGAR_DETALLES_ACCION%>";
                                                        var CANCELAR_DETALLE = "<%=RecetaPorCB.CANCELAR_DETALLE_ACCION%>";
                                                        var VALIDAR = "<%=RecetaPorCB.VALIDAR_RECETA_ACCION%>";
                                                        var LOTE = "<%=RecetaPorCB.OBTENER_LOTES_ACCION%>";
                                                        var CAMBIO = <%=RecetaImpl.NO_ENCONTRADO%>;
                                                        var CAMBIAR = "<%=RecetaPorCB.CAMBIAR_LOTE_ACCION%>";
                                                        var SURTIR = "<%=RecetaPorCB.SURTIR_LOTE_ACCION%>";
                                                        var FINALIZAR = "<%=RecetaPorCB.FINALIZAR_RECETA_ACCION%>";
                                                        var VALIDAR_ENCABEZADO = "<%=RecetaPorCB.VALIDAR_ENCABEZADO_ACCION%>";
                                                        var CODIGO_BARRAS_CAPTURA = "<%=RecetaImpl.CODIGO_BARRAS_CAPTURA%>";

                                                        var tipo = <%=rol%>;
                                                        var causes_id = [];
                                                        var causes_ambos = [];
                                                        var descripcion_productos = [];
                                                        var claves_productos = [];

                                                        $('#fecha1').mask("99-99-9999", {placeholder: " "});
                                                        $("#fecha1").datepicker({
                                                            dateFormat: 'dd-mm-yy',
                                                            changeYear: true,
                                                            changeMonth: true,
                                                            maxDate: 'today',
                                                            yearRange: "1910:2020",
                                                            onClose: function (dateText, inst) {
                                                                if ($(this).val() === "undefined") {
                                                                    alert("");
                                                                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                                                                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                                                                    $(this).datepicker('setDate', new Date(year, month, 1));
                                                                }
                                                            }

                                                        });

                                                        $("#fecha1").datepicker("setDate", new Date());
                                                        $("#uni_ate").val("<%=(new adminUsuDaoImpl()).uniName((String) sesion.getAttribute("cla_uni"))%>");

                                                        $(function ()
                                                        {
                                                            var medico = $("#medico").val();
                                                            var paciente = $("#nom_pac").val();
                                                            if (medico !== "")
                                                            {
                                                                $("#medico_jq").prop('readonly', true);
                                                            }

                                                            if (paciente !== "")
                                                            {
                                                                $("#nombre_jq").prop('readonly', true);
                                                                $("#sp_pac").prop('readonly', true);
                                                            }

                                                            $.ajax({
                                                                url: "${pageContext.servletContext.contextPath}/Operaciones",
                                                                data: {ban: 5},
                                                                type: 'POST',
                                                                async: true,
                                                                success: function (data)
                                                                {
                                                                    $("#prefijo").val(data.pre);
                                                                    $("#preOcul").val(data.pre);
                                                                }, error: function (jqXHR, textStatus, errorThrown) {

                                                                    alert("Error Contactar al departamento de sistemas");

                                                                }
                                                            });

                                                            $.ajax({
                                                                url: "${pageContext.servletContext.contextPath}/Catalogo",
                                                                data: {ban: <%=Catalogo.OBTENER_CATALAGO_CAUSES%>},
                                                                type: 'POST',
                                                                async: true,
                                                                dataType: 'json',
                                                                success: function (data)
                                                                {
                                                                    data.forEach(function (value, id, arr) {
                                                                        causes_ambos.push(value.ambos);
                                                                        causes_id.push(value.id);
                                                                    });
                                                                    $("#causes").autocomplete({
                                                                        source: causes_ambos
                                                                    });
                                                                    $("#causes2").autocomplete({
                                                                        source: causes_ambos
                                                                    });
                                                                }, error: function (jqXHR, textStatus, errorThrown) {

                                                                    alert("Error Contactar al departamento de sistemas");

                                                                }
                                                            });

                                                            $.ajax({
                                                                url: "${pageContext.servletContext.contextPath}/Catalogo",
                                                                data: {ban: <%=Catalogo.OBTENER_CATALAGO_PRODUCTOS%>},
                                                                type: 'POST',
                                                                async: true,
                                                                dataType: 'json',
                                                                success: function (data)
                                                                {
                                                                    data.forEach(function (value, id, arr) {
                                                                        descripcion_productos.push(value.descripcion);
                                                                        claves_productos.push(value.id);
                                                                    });
                                                                    $("#des_pro").autocomplete({
                                                                        source: descripcion_productos
                                                                    });
                                                                    $("#cla_pro").autocomplete({
                                                                        source: claves_productos
                                                                    });
                                                                }, error: function (jqXHR, textStatus, errorThrown) {

                                                                    alert("Error Contactar al departamento de sistemas");

                                                                }
                                                            });

                                                        });


                                                        function validaCauses() {
                                                            var causes = document.getElementById('causes').value;

                                                            if (causes === "") {
                                                                swal({
                                                                    title: "Captura medicamento",
                                                                    text: 'Diagnóstico NO INGRESADO, por favor ingreselo.',
                                                                    type: "error",
                                                                    showCancelButton: false,
                                                                    confirmButtonClass: "btn-default",
                                                                    confirmButtonText: "Continuar"
                                                                }, function () {
                                                                    $("#causes").focus();
                                                                });
                                                                return false;
                                                            }
                                                            var causesArray = causes.split(" - ");
                                                            causes = causesArray[0];
                                                            if (causes_id.indexOf(causes) === -1) {
                                                                swal({
                                                                    title: "Captura medicamento",
                                                                    text: 'Diagnóstico NO válido, por favor ingrese otro.',
                                                                    type: "error",
                                                                    showCancelButton: false,
                                                                    confirmButtonClass: "btn-default",
                                                                    confirmButtonText: "Continuar"
                                                                }, function () {
                                                                    $("#causes").focus();
                                                                });
                                                                return false;
                                                            }

                                                            $('#btn_capturar').click();
                                                        }

                                                        $("[data-behavior~=only-alphanum]").alphanum({
                                                            allowOtherCharSets: true,
                                                            allowNewline: true,
                                                            disallow: "'",
                                                            allow: ".\\t()/,-%:"
                                                        });
                                                        $("[data-behavior~=only-alphanum-enter]").alphanum({
                                                            allowOtherCharSets: true,
                                                            disallow: "'",
                                                            allow: "-"
                                                        });

                                                        $("[data-behavior~=only-num]").numeric({
                                                            allowMinus: false,
                                                            allowThouSep: false
                                                        });

                                                        var config_num = {
                                                            allowMinus: false,
                                                            allowThouSep: false,
                                                            min: 1,
                                                            max: 50
                                                        };

                                                        $("#unidades").numeric(config_num);

                                                        $("#dias").numeric(config_num);

                                                        $("#indicacionesRF").alphanum({
                                                            allowOtherCharSets: true,
                                                            disallow: "'",
                                                            allow: "./-"
                                                        }
                                                        );
        </script>
        <%=script%>
    </body>



</html>