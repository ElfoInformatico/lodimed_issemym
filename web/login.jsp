<%-- 
    Document   : index
    Created on : 07-mar-2014, 15:38:43
    Author     : Americo
--%>

<%@page import="Clases.Hash"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    HttpSession sesion = request.getSession();
    String info = "";
    int intentos = 0;
    try {
        info = (String) session.getAttribute("mensaje");
    } catch (Exception e) {
    }
%>
<!DOCTYPE html>
<!--
Index para administradores
-->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="css/singnin.css" rel="stylesheet" media="screen">
        <link href="css/isem.css" rel="stylesheet" media="screen">
        <script>var va1 = '<%=Hash.SessionToHash(sesion.getId())%>';</script>
        <title>Sistema de Captura de Receta</title>
        <%

            if (session.getAttribute("cont") == null) {
                intentos = 0;
            } else {
                //recuperas de la session el número de intentos, p. ej así
                intentos = ((Integer) request.getSession().getAttribute("cont"));

            }
            if (intentos == 3) {
        %>
        <script>
            alert("Se han superado el número máximo de intentos.");
            window.location = 'index.jsp';

        </script>
        <%
            }
        %>  
    </head>
    <body>
        <div class="container">
            <div class="text-right">
            </div>

            <form class="marco" action="Login" id="forma-login" method="post">

                <div class="row">
                    <div class="col-md-10"><h2 style="color: white">Acceso al sistema</h2   > </div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div> 
                </div>
                <div class="panel panel-body">
                    <div class="row">
                        <div class="col-md-12"><img src="imagenes/logo-ISEM-en-grande.png" class="img-responsive" alt="logo"></div>
                    </div>
                    <h3>LODIMED</h3>
                    <h4 class="form-signin-heading">Ingrese sus credenciales</h4>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <label class="glyphicon glyphicon-user"></label>
                        </span>
                        <input type="text" name="user" id="user" class="form-control" data-behavior = "only-alphanum" placeholder="Introduzca Nombre de Usuario"  required>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <label class="glyphicon glyphicon-lock"></label>
                        </span>
                        <input type="password" name="pass" id="pass" class="form-control" data-behavior = "only-alphanum" placeholder="Introduzca Contrase&ntilde;a V&aacute;lida"  required>
                    </div>
                    <%
                        if (info != null) {
                    %>
                    <br>
                    <div class="row">
                        <div class="alert alert-danger">
                            <strong>Atención!</strong> Datos inv&aacute;lidos, intente otra vez...
                        </div>
                    </div>
                    <br>
                    <%
                        }
                    %>
                    <button class="btn btn-lg btn-isem btn-block" type="submit" name="accion" value="3">Entrar</button>
                </div>
            </form>
            <div class="row" >
                <div class="col-md-4"></div>
                <div class="col-md-4"><h4 class="left"><a href="index.jsp"><font color="#222">Regresar a Men&uacute;</font></a></h4></div>                        
                <div class="col-md-4"></div>
            </div>
        </div>

        <script src="js/jquery-2.1.4.js" type="text/javascript"></script>
        <script src="js/md5-min.js" type="text/javascript"></script>
        <script src="js/datos.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.alphanum.js" type="text/javascript"></script>
        <script>

            //Valida que solo se añadan numeros.
            $("[data-behavior~=only-alphanum]").alphanum({
                allowSpace: false,
                allowOtherCharSets: false,
                allow: '#$\r'

            });

        </script>
    </body>

</html>
