<%-- 
    Document   : index_main
    Created on : 07-mar-2014, 15:38:43
    Author     : Americo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%    HttpSession sesion = request.getSession();
    String id_usu = "";

    id_usu = (String) session.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("index.jsp");
        return;
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/topPadding.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <title>LODIMED</title>
    </head>
    <body>
        <%@include file="../jspf/mainMenu.jspf" %>

        <div class="container-fluid">
            <div class="starter-template">
                <h1>LODIMED</h1>

                <%if (((String) sesion.getAttribute("tipo")).equals("FARMACIA")) {
                %>
                <h4>Médico</h4>
                <%
                } else if (((String) sesion.getAttribute("tipo")).equals("ADMON")) {
                %>
                <h4>Administrador de Usuarios</h4>
                <%
                }else if (((String) sesion.getAttribute("tipo")).equals("SUPERVISOR")){
                %>
                <h4>Supervisor</h4>
                <%
                    }else {
                %>
                <h4>Farmacia</h4>
                <%
                    }
                %>
                <p class="lead">Sistema de Captura de Receta</p>                
            </div>

        </div>
        <div class="row">
            <div class="col-md-5"></div>
            <div class="col-md-2"><center><img src="imagenes/gobierno.png" width=200 alt="Logo"></center></div>
            <div class="col-md-5"></div>

        </div>

        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="js/jquery-1.9.1.js"></script>
        <script src="js/bootstrap.js"></script>
        <script language="javascript" src="js/codeJs.js"></script>

    </body>
</html>

