var idReceta = -1;


function sumar() {
    var unidades = document.formulario_receta.unidades.value;
    if (unidades === "")
        unidades = 0;


    document.formulario_receta.piezas_sol.value = Math.ceil(unidades);
    document.formulario_receta.can_sol.value = Math.ceil(unidades);
}


function tabular(e, obj)
{
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla !== 13)
        return;
    frm = obj.form;
    for (i = 0; i < frm.elements.length; i++)
        if (frm.elements[i] === obj)
        {
            if (i === frm.elements.length - 1)
                i = -1;
            break
        }
    /*ACA ESTA EL CAMBIO*/
    if (frm.elements[i + 1].disabled === true)
        tabular(e, frm.elements[i + 1]);
    else
        frm.elements[i + 1].focus();
    return false;
}

$(function () {

    $("#banderaFolio").val("0");

    $(function () {
        $("#nombre_jq").keyup(function () {
            var nombre = $("#nombre_jq").val();
            $("#nombre_jq").autocomplete({
                source: "../AutoPacientes?nombre=" + nombre,
                minLength: 2,
                select: function (event, ui) {
                    $("#nombre_jq").val(ui.item.nom_com);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li>")
                        .data("ui-autocomplete-item", item)
                        .append("<a>" + item.nom_com + "</a>")
                        .appendTo(ul);
            };
        });
    });
    $(function () {
        $("#medico_jq").keyup(function () {
            var nombre = $("#medico_jq").val();
            $("#medico_jq").autocomplete({
                source: "../AutoMedico?nombre=" + nombre,
                minLength: 1,
                select: function (event, ui) {
                    $("#medico_jq").val(ui.item.nom_med);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li>")
                        .data("ui-autocomplete-item", item)
                        .append("<a>" + item.nom_med + "</a>")
                        .appendTo(ul);
            };
        });
    });

    $('#bus_pac').tooltip();
    $('#bus_pacn').tooltip();
});

function validarEncabezado(paso) {
    var folio = $('#folio').val();
    if (folio === "" && paso != 1) {
        $('#tipoCons').val(0);
        $('#folio').focus();
        swal({
            title: "Folio no ingresado",
            text: "Debe ingresar el folio de la receta para continuar",
            type: "warning",
            showCancelButton: false,
            confirmButtonClass: "btn-warning",
            confirmButtonText: "Ingresar",
            closeOnConfirm: false
        });

        return false;
    }

    if (paso === 1) {
        return true;
    }

    var tipo = $('#tipoCons').val();
    if (tipo === "0" && paso != 2) {
        $('#medico_jq').val("");
        $('#tipoCons').focus();
        swal({
            title: "Tipo de consulta no ingresado",
            text: "Debe ingresar el Tipo de consulta de la receta para continuar",
            type: "warning",
            showCancelButton: false,
            confirmButtonClass: "btn-warning",
            confirmButtonText: "Ingresar"
        });

        return false;
    }

    if (paso === 2) {
        return true;
    }

    var medico = $('#medico').val();
    if (medico === "" && paso != 3) {
        $('#nombre_jq').val("");
        $('#medico_jq').focus();
        swal({
            title: "Médico no ingresado",
            text: "Debe ingresar el médico de la receta para continuar",
            type: "warning",
            showCancelButton: false,
            confirmButtonClass: "btn-warning",
            confirmButtonText: "Ingresar"
        });

        return false;
    }

    if (paso === 3) {
        return true;
    }

    var paciente = $('#nom_pac').val();
    if (paciente === "" && paso != 4) {
        $('#causes').val("");
        $('#nombre_jq').focus();
        swal({
            title: "Paciente no ingresado",
            text: "Debe ingresar el paciente de la receta para continuar",
            type: "warning",
            showCancelButton: false,
            confirmButtonClass: "btn-warning",
            confirmButtonText: "Ingresar"
        });

        return false;
    }

    return true;
}


$(document).ready(function () {

    $("#btnDescartar").click(function ()
    {
        var e = confirm("Esta seguro de eliminar esta receta");
        if (e) {
            $("#btn_capturar").val("1");
            isCanceled = true;
            cancelarReceta();
        }

    });

    $('#tipoCons').change(function () {
        validarEncabezado(2);
    });

    $('#medico_jq').click(function () {
        validarEncabezado(3);
    });

    $('#sp_pac').click(function () {
        validarEncabezado(4);
    });

    $('#nombre_jq').click(function () {
        validarEncabezado(4);
    });

    //Añade prefijo al folio.
    $('#folio').focusout(function () {
        var folio = $('#folio').val();
        var prefijo = $('#prefijo').val();
        var bandera = $("#banderaFolio").val();
        if (folio === "") {
            return;
        }

        if (folio.indexOf(prefijo) === -1) {
            $('#folio').val(prefijo.concat(folio));
        }
        if (bandera !== "0")
        {

            return;
        }

        folio = $('#folio').val();
        dir = '../recetacb';
        $.ajax({
            dataType: 'json',
            type: "POST",
            url: dir,
            data: {"accion": VALIDAR_ENCABEZADO, "tipo": tipo,
                "folio": folio},
            async: false,
            success: function (data) {
                console.log("Folio no existe");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error", jqXHR, textStatus, errorThrown);
                swal({
                    title: "Creación receta",
                    text: jqXHR.responseText,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-default",
                    confirmButtonText: "Continuar"
                }, function () {
                    $('#folio').val("");
                    $('#cedula').val("");

                    $("#nombre_jq").prop('readonly', false);
                    $("#medico_jq").prop('readonly', false);
                    $("#sp_pac").prop('readonly', false);

                    $('#nom_pac').val("");
                    $('#tipoCons').val("0");
                    $('#medico').val("");
                    $('#sexo').val("");
                    $('#fec_nac').val("");
                    $('#txt_carnet').val("");
                    $('#fol_sp').val("");
                    $('#nombre_jq').val("");
                    $('#medico_jq').val("");

                });
            }
        });

    });

//captura de detalle
    $('#btn_capturar').click(function () {
        var cla_pro = $('#cla_pro').val();
        var des_pro = $('#des_pro').val();
        var dias = $('#dias').val();
        var cantSol = $('#can_sol').val();
        var causes = $('#causes').val();
        var causes2 = $('#causes2').val();

        var indicaciones = $('#indicacionesRF').val();
        if (cla_pro !== "" && des_pro !== "") {

            if ($('#can_sol').val() === "" || $('#can_sol').val() === "0") {
                alert('Capture la cantidad a entregar');
            } else if (dias === "" || dias === "0") {
                alert('Capture Horas/Días');
            } else if (indicaciones === "") {
                alert('Capture Indicaciones');
            } else {
                $("#myModal").modal({show: true});
                indicaciones = "".concat(cantSol, " CAJAS POR ", dias, " DIA(S) | \n", indicaciones);

                dir = '../recetacb';
                $.ajax({
                    type: 'POST',
                    url: dir,
                    data: {"accion": CAPTURA, "tipo": tipo, "id": idReceta, "clave": cla_pro, "solicitado": cantSol,
                        "indicacion": indicaciones, "cause": causes, "cause2": causes2},
                    async: false,
                    success: function (data) {
                        $('#cla_pro').val("");
                        $("#des_pro").val("");
                        $("#indica").val("");
                        $("#can_sol").val("");
                        $("#unidades").val("");
                        $("#dias").val("");
                        $("#indicacionesRF").val("");
                        $("#cla_pro").focus();
                        recargarDetalles();

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Error", jqXHR, textStatus, errorThrown);
                        $("#myModal").modal('hide');
                        swal({
                            title: "Captura de medicamento",
                            text: jqXHR.responseText,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-default",
                            confirmButtonText: "Continuar"
                        });
                    }
                });

            }
        } else {
            alert("Capture primero el medicamento");
            $("#des_pro").focus();
        }
    });


    function dameProducto(data) {
        var json = data;

        for (var i = 0; i < json.length; i++) {
            var cla_pro = json[i].cla_pro;
            var descripcion = json[i].des_pro;


            if (cla_pro === "null" || descripcion === "null") {
                swal({
                    title: "Buscar producto",
                    text: "Clave fuera del catálogo",
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-default",
                    confirmButtonText: "Continuar"
                });
                $("#cla_pro").val("");
                $("#des_pro").val("");
                $("#indica").val("");
                $("#can_sol").val("");
                $("#des_pro").focus();
                return;
            }

            $("#cla_pro").val(cla_pro);
            $("#unidades").focus();
            $("#des_pro").val(descripcion);
            $("#idRec").val(idReceta);
        }
    }


    $('#btn_descripcion').click(function () {
        var dir = '../ProductoDescripcion';
        var form = $('#formulario_receta');

        $.ajax({
            dataType: 'json',
            type: form.attr('method'),
            url: dir,
            data: form.serialize(),
            success: function (data) {
                dameProducto(data);
            }
        });

    });

    $('#btn_clave').click(function () {
        var dir = '../ProductoClave';
        var form = $('#formulario_receta');

        $.ajax({
            dataType: 'json',
            type: form.attr('method'),
            url: dir,
            data: form.serialize(),
            success: function (data) {

                dameProducto(data);
            },
            error: function () {
                alert("Ha ocurrido un error - clave");
            }
        });

    });

    $('#formulario_receta').submit(function () {
        //alert("Ingresó");
        return false;
    });

    $('#mostrar1').click(function () {
        var dir = '../Receta';
        var form = $('#formulario_receta');
        if ($('#tipoCons').val() === "0") {
            alert("seleccione el tipo de consulta");
            $('#tipoCons').focus();
            return false;
        }
        if ($('#folio').val() === "") {
            alert("Ingrese folio");
            $('#folio').focus();
            return false;
        }
        $.ajax({
            type: form.attr('method'),
            url: dir,
            data: form.serialize(),
            success: function (data) {
                devuelveFolio(data);
            },
            error: function () {
                alert("Ha ocurrido un error - mostrar");
            }
        });
        function devuelveFolio(data) {
            var json = JSON.parse(data);
            $('#select_pac').empty();
            $('#select_pac').focus();
            $("#sp_pac").prop('readonly', true);
            $("#nombre_jq").prop('readonly', true);
            $('#select_pac').append(
                    $('<option>', {
                        value: "",
                        text: "--Seleccione una Opción--"
                    }));
            for (var i = 0; i < json.length; i++) {
                var nom_com = json[i].nom_com;
                var mensaje = json[i].mensaje;
                // alert(nom_com);

                $('#select_pac').append(
                        $('<option>', {
                            value: nom_com,
                            text: nom_com
                        }));

                if (mensaje !== "") {
                    alert("Paciente Inexistente");
                    $("#sp_pac").prop('readonly', false);
                    $("#nombre_jq").prop('readonly', false);
                }

                var vigencia = json[i].vig;

                if (vigencia === 0)
                {
                    alert("Vigencia vencida.");
                    $("#sp_pac").prop('readonly', false);
                    $("#nombre_jq").prop('readonly', false);
                    return false;
                }
            }
        }
    });

    function getPaciente(paciente) {
        if (!validarEncabezado(4)) {
            return;
        }

        var dir = '../paciente/nombre';

        $.ajax({
            dataType: 'json',
            type: "POST",
            url: dir,
            data: {"accion": BUSCAR_PACIENTE, "nombre": paciente},
            success: function (data) {
                //console.log("Aserto", data);

                if (data.vigencia < -60) {
                    swal({
                        title: "Vigencia terminada",
                        text: "La vigencia del paciente terminó el ".concat(data.fin),
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-default",
                        confirmButtonText: "Continuar",
                        closeOnConfirm: true
                    });

                    $('#nombre_jq').val("");
                    return;
                } else if (data.vigencia <= 0) {
                    swal({
                        title: "Vigencia en prorroga",
                        text: "Recuerde una vez vencida su licencia, tiene 60 días naturales para solvertar esta situación. La vigencia del paciente terminó el ".concat(data.fin, "."),
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonClass: "btn-warning",
                        confirmButtonText: "Continuar",
                        closeOnConfirm: true
                    });
                }



                $("#nombre_jq").prop('readonly', true);
                $("#sp_pac").prop('readonly', true);

                $('#nom_pac').val(data.nombre);
                $('#sexo').val(data.sexo);
                $('#fec_nac').val(data.nacimiento);
                $('#txt_carnet').val(data.expediente);
                $('#fol_sp').val(data.folio);

                dir = '../recetacb';
                $.ajax({
                    dataType: 'json',
                    type: "POST",
                    url: dir,
                    data: {"accion": ENCABEZADO, "tipo": tipo,
                        "folio": $('#folio').val(), "fecha": $('#fecha1').val(),
                        "medico": $('#cedula').val(), "paciente": data.id,
                        "consulta": $("#tipoCons option:selected").text(),
                        "expediente": data.expediente},
                    success: function (data) {
                        console.log("Creo ", data);
                        idReceta = data.id;
                        $("#capturaReceta").show();
                        $("#descartarReceta").show();
                        $("#folio").prop('readonly', true);
                        $("#fecha1").prop('readonly', true);
                        $('#fecha1').prop('disabled', true);
                        $('#tipoCons').prop('disabled', true);
                        $('#mostrar3').prop('disabled', true);
                        $('#mostrar1').prop('disabled', true);
                        $('#mostrar2').prop('disabled', true);
                        $('#nuevoPaciente').prop('disabled', true);
                        $("#banderaFolio").val("1");
                        $('#causes').focus();

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Error", jqXHR, textStatus, errorThrown);
                        swal({
                            title: "Creación receta",
                            text: jqXHR.responseText,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-default",
                            confirmButtonText: "Continuar"
                        }, function () {
                            $("#btn_capturar").val("1");
                            location.reload();
                        });
                    }
                });

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error", jqXHR, textStatus, errorThrown);
                swal({
                    title: "Buscar paciente",
                    text: jqXHR.responseText,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-default",
                    confirmButtonText: "Continuar",
                    closeOnConfirm: false
                });
            }

        });

    }


    $('#select_pac').change(function () {
        getPaciente($('#select_pac').val());
    });


    //Crear el encabezado.
    $('#mostrar2').click(function () {
        getPaciente($('#nombre_jq').val());
    });


    $('#mostrar3').click(function () {
        var dir = '../RecetaNombreCol';
        var form = $('#formulario_receta');
        if ($('#folio').val() === "") {
            alert("Ingrese folio");
            $('#folio').focus();
            return false;
        }
        $.ajax({
            type: form.attr('method'),
            url: dir,
            data: form.serialize(),
            success: function (data) {
                devuelveFolio2(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("Ha ocurrido un error mostrar 3");
                console.log("Error", jqXHR, textStatus, errorThrown);
                swal({
                    title: "Buscar médico",
                    text: jqXHR.responseText,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-default",
                    confirmButtonText: "Continuar"
                });
            }
        });
        function devuelveFolio2(data) {
            var json = JSON.parse(data);

            var nom_med = json.nom_med;
            var tipoConsulta = json.tipoConsulta;
            var cedula = json.cedula;

            $("#medico").val(nom_med);
            $("#cedula").val(cedula);
            $("#tipoConsulta").val(tipoConsulta);
            $("#medico_jq").prop('readonly', true);
            $("#mostrar3").prop('disabled', true);
            $("#nombre_jq").focus();

        }
    });


    $("#printImpr").click(function () {
        swal({
            title: "Captura de la receta",
            text: "Esta a punto de terminar la captura de la receta, una vez hecho esto no podra añadir más claves a la receta, ¿Esta seguro?.",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-warning",
            confirmButtonText: "Si, Surtirla",
            cancelButtonText: "¡No!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
        function (isConfirm) {
            var tipoCaptura = $("#tipCaptura").val();
            if (isConfirm) {
                dir = '../recetacb';
                $.ajax({
                    type: 'POST',
                    url: dir,
                    data: {"accion": VALIDAR, "tipo": tipo,
                        "id": idReceta},
                    success: function (data)
                    {
                        if (tipoCaptura === CODIGO_BARRAS_CAPTURA) {
                            console.log("c_detalle", data);
                            $('#capturaReceta').hide();
                            $("#surtimiento").show();
                            var url = '../reportes/RecetaFarm.jsp?fol_rec=';
                            url = url.concat(idReceta, "&tipo=5&usuario=d");
                            swal({
                                title: "Captura de la receta",
                                text: data,
                                type: "success",
                                showCancelButton: true,
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Aceptar"
                            }, function () {
                                window.open(url, '', 'width=1200,height=800,left=50,top=50,toolbar=no');
                            });
                        }
                        else
                        {
                            swal({
                                title: "Finalizar receta",
                                text: "El proceso de captura de la receta a sido completado. Folio consecutivo:" + idReceta + "",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Aceptar"
                            }, function () {
                                var url = '../reportes/RecetaFarm.jsp?fol_rec=';
                                url = url.concat(idReceta, "&tipo=", tipo);
                                window.open(url, '', 'width=1200,height=800,left=50,top=50,toolbar=no');
                                $("#btn_capturar").val("1");
                                window.location.reload();
                            });
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Error", jqXHR, textStatus, errorThrown);
                        //$("#myModal").modal('hide');
                        swal({
                            title: "Captura de la receta",
                            text: jqXHR.responseText,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-default",
                            confirmButtonText: "Continuar"
                        });
                    }
                });
            }
        });

    });

    //Primer dato a ingresar.
    $('#folio').focus();

    $('#cb').focusout(function () {
        var cb = $('#cb').val();
        console.log("cb", cb);
        if (cb !== "") {
            buscarLotes(cb);
        }
    });

    $("#loteSelect").change(function () {
        var posicion = $("#loteSelect").val();
        if (posicion !== "-1") {
            asignarLote(listaLotes[posicion]);
        } else {
            $("#caducidadCB").val("");
            $("#claveCB").val("");
            $("#solicitadoCB").val("");
        }
    });

    $("#validarDetalle").click(surtirInsumo);

    $("#surtidoCB").keyup(function () {
        var solicitado = parseInt($("#solicitadoCB").val());
        if (solicitado === -1) {
            return;
        }

        var surtido = parseInt($("#surtidoCB").val());
        if (surtido > solicitado) {
            swal({
                title: "Surtir insumo",
                text: "La cantidad a surtir NO PUEDE ser mayor a lo solicitado.",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-default",
                confirmButtonText: "Continuar"
            }, function () {
                $("#surtidoCB").val("");
                $("#surtidoCB").focus();
            });
        }
    });

    $("#limpiarCB").click(limpiarCamposCB);

    $("#guardarReceta").click(finalizarReceta);

});


function hacerTabla(data) {
    var json = data;

    $("#tablaMedicamentos").empty();
    $("#tablaMedicamentos").append(
            $("<tr>")
            .append($("<td>").append("Clave"))
            .append($("<td>").append("Descripción"))
            .append($("<td>").append("Diagnóstico 1"))
            .append($("<td>").append("Diagnóstico 2"))
            .append($("<td>").append(""))
            );
    for (var i = 0; i < json.length; i++) {
        var cla_pro = json[i].clave;
        var des_pro = json[i].descripcion;
        var lot_pro = json[i].lote;
        var cad_pro = json[i].caducidad;
        var detalleReceta = json[i].id;
        var can_sol = json[i].solicitado;
        var cant_sur = json[i].surtido;
        var indicaciones = json[i].indicaciones;
        var id_cau = json[i].cause;
        var id_cau2 = json[i].cause2;
        var duracionArray = indicaciones.split("POR");
        var duracionArray2 = duracionArray[1].split("|");
        var duracion = duracionArray2[0];

        var btn_eliminar = "<button style='z-index:6' class='btn btn-danger btn-sm' id='btn_eli' name = 'btn_eli' onclick=\"cancelarDetalle('" + cla_pro + "')\"><span class='glyphicon glyphicon-remove' ></span></button>";


        $("#tablaMedicamentos").append(
                $("<tr>")
                .append($("<td>").append(cla_pro))
                .append($("<td>").append(des_pro))
                .append($("<td>").append(id_cau))
                .append($("<td>").append(id_cau2))
                .append($("<td>").append("Eliminar:"))
                .append($("<td>").append(btn_eliminar))
                );
        $("#tablaMedicamentos").append(
                $("<tr>")
                .append($("<td colspan='3'>").append("Indicaciones: " + indicaciones + " -Lote:" + lot_pro + "-Caducidad:" + cad_pro))
                .append($("<td>").append("Duración: " + duracion))
                .append($("<td>").append("Can. Sol.: " + can_sol))
                .append($("<td>").append("Can. Sur.: " + cant_sur))
                );
    }
}

function cancelarReceta() {
    console.log("id", idReceta);
    if (idReceta === -1) {
        return;
    }
    $.ajax({
        type: "POST",
        url: '../recetacb',
        data: {"accion": CANCELAR, "tipo": tipo, "folio": $('#folio').val(), "id": idReceta},
        async: false,
        success: function (data)
        {
            window.location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Error", jqXHR, textStatus, errorThrown);
            swal({
                title: "Cancelación de la receta",
                text: jqXHR.responseText,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-default",
                confirmButtonText: "Continuar"
            });
        }
    });

}

function cancelarDetalle(clave) {
    swal({
        title: "Cancelación del producto",
        text: "Esta a punto de eliminar la clave: " + clave + ", ¿Esta seguro?.",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-warning",
        confirmButtonText: "Si, ¡Elimina ese producto!",
        cancelButtonText: "¡No!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    },
    function (isConfirm) {
        if (isConfirm) {
            dir = '../recetacb';
            $.ajax({
                type: 'POST',
                url: dir,
                data: {"accion": CANCELAR_DETALLE, "tipo": tipo,
                    "id": idReceta, "clave": clave},
                success: function (data) {
                    console.log("c_detalle", data);
                    $('#cla_pro').val("");
                    $("#des_pro").val("");
                    $("#indica").val("");
                    $("#can_sol").val("");
                    $("#cla_pro").focus();
                    recargarDetalles();
                    swal({
                        title: "Cancelación del producto",
                        text: data,
                        type: "success",
                        showCancelButton: true,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Aceptar"
                    });

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error", jqXHR, textStatus, errorThrown);
                    //$("#myModal").modal('hide');
                    swal({
                        title: "Cancelación del producto",
                        text: jqXHR.responseText,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-default",
                        confirmButtonText: "Continuar",
                        closeOnConfirm: false
                    });
                }
            });
        }
    });
}

function recargarDetalles() {
    var dir = '../recetacb';
    $.ajax({
        dataType: 'json',
        type: "POST",
        url: dir,
        async: false,
        data: {"accion": RECARGAR_DETALLES, "tipo": tipo, "id": idReceta},
        success: function (data) {
            console.log("detalles ", data);
            //Verifica si muestra boton de validar receta.
            if (data.length > 0) {
                $("#valirReceta").show();
            } else {
                $("#valirReceta").hide();
            }
            hacerTabla(data);
            $("#myModal").modal('hide');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Error", jqXHR, textStatus, errorThrown);
            swal({
                title: "Captura de medicamento",
                text: jqXHR.responseText,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-default",
                confirmButtonText: "Continuar",
                closeOnConfirm: false
            });
        }
    });


}

var listaLotes;

function buscarLotes(cb) {
    var dir = '../recetacb';
    $.ajax({
        dataType: 'json',
        type: "POST",
        url: dir,
        async: false,
        data: {"accion": LOTE, "tipo": tipo, "id": idReceta, "cb": cb},
        success: function (data) {
            console.log("detalles ", data);
            listaLotes = data;
            limpiarCamposCB();
            $("#cb").val(cb);
            if (data.length > 1) {
                $("#loteTXT").hide();
                $("#loteSLC").show();
                $('#loteSelect').append(new Option('-- Lote a entregar --', -1));
                var aux;
                for (i = 0; i < data.length; i++) {
                    aux = data[i];
                    $('#loteSelect').append(new Option(aux.lote, i));
                }

            } else {
                $("#loteSLC").hide();
                $("#loteTXT").show();
                $("#lote").val(data[0].lote);
                $('#lote').prop('disabled', 'disabled');
                asignarLote(data[0]);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Error", jqXHR, textStatus, errorThrown);
            swal({
                title: "Busqueda insumo",
                text: jqXHR.responseText,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-default",
                confirmButtonText: "Continuar"
            }, function () {
                $("#cb").val("");
                $("#cb").focus();
            });
        }
    });

}

function asignarLote(elegido) {

    if (elegido.solicitado === -1) {
        swal({
            title: "Validación producto",
            text: "El lote elegido no es correcto.",
            type: "error",
            showCancelButton: false,
            confirmButtonClass: "btn-default",
            confirmButtonText: "Continuar"
        }, function () {
            limpiarCamposCB();
        });
        return;
    }

    $("#caducidadCB").val(elegido.caducidad);
    $("#claveCB").val(elegido.clave);
    $("#solicitadoCB").val(elegido.solicitado);

}

function surtirInsumo() {
    var elegido = 0;
    var aux = $('#loteSelect').val();
    if (aux !== null) {
        elegido = parseInt(aux);
        if (elegido === CAMBIO) {
            swal({
                title: "Validación producto",
                text: "Debe elegir por lo menos un lote.",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-default",
                confirmButtonText: "Continuar"
            }, function () {
                $("#loteSelect").focus();
            });
            return;
        }
    }


    var surtido = $('#surtidoCB').val();
    if (surtido === "") {
        swal({
            title: "Validación producto",
            text: "Debe indicar la cantidad a surtir.",
            type: "error",
            showCancelButton: false,
            confirmButtonClass: "btn-default",
            confirmButtonText: "Continuar"
        }, function () {
            $("#surtidoCB").focus();
        });
        return;
    }
    surtido = parseInt(surtido);
    var producto = listaLotes[elegido].detalle_producto;

    var solicitado = listaLotes[elegido].solicitado;
    var accion = SURTIR;
    if (solicitado === CAMBIO) {
        accion = CAMBIAR;
        swal({
            title: "Cambio de lote",
            text: "El lote que intenta dispensar es distinto al indicado por el sistema. Puede realizar este cambio pero es necesaria la contraseña del supervisor.",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            inputPlaceholder: "Ingrese la contraseña de supervisor",
            showLoaderOnConfirm: true
        }, function (inputValue) {
            if (inputValue === false)
                return false;
            if (inputValue === "") {
                swal.showInputError("Contraseña no ingresa, favor ingresarla.");
                return false;
            }
            $.ajax({
                type: "POST",
                url: "../Farmacias",
                dataType: "json",
                data: {accion: "verificar", pass: inputValue},
                success: function (data) {
                    if (data.permitido) {
                        validarInsumo(accion, producto, surtido);
                    } else {
                        swal.showInputError("Contraseña errónea, por favor ingresar de nuevo.");
                        return false;
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error", jqXHR, textStatus, errorThrown);
                    swal({
                        title: "Cambio de lote",
                        text: jqXHR.responseText,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-default",
                        confirmButtonText: "Continuar"
                    });
                }
            }
            );
        });

        return;
    }


    validarInsumo(accion, producto, surtido);

}

function limpiarCamposCB() {
    $('#loteSelect').empty();
    $("#lote").val('');
    $('#lote').prop('disabled', '');

    $("#loteSLC").hide();
    $("#loteTXT").show();

    $("#cb").val('');
    $("#claveCB").val('');
    $("#lote").val('');
    $("#caducidadCB").val('');
    $("#solicitadoCB").val('');
    $("#surtidoCB").val('');

}


function validarInsumo(accion, producto, surtido) {

    var dir = '../recetacb';
    $.ajax({
        type: "POST",
        url: dir,
        data: {"accion": accion, "tipo": tipo, "id": idReceta,
            "producto": producto, "surtido": surtido},
        success: function (data) {
            console.log("validacion completa");
            agregarInsumoValido();
            swal({title: "Validación producto", text: "Validación completa", type: "success", confirmButtonClass: "btn-success",
                confirmButtonText: "Ok"}, limpiarCamposCB);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Error", jqXHR, textStatus, errorThrown);
            swal({
                title: "Validación producto",
                text: jqXHR.responseText,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-default",
                confirmButtonText: "Continuar"
            });
        }
    });
}

var tablaSurtido = $("#tablaSurtido").DataTable();

function agregarInsumoValido() {

    var lote = $("#lote").val();
    if (lote === "") {
        lote = listaLotes[$("#loteSelect").val()].lote;
    }

    var surtido = parseInt($("#surtidoCB").val());
    var solicitado = parseInt($("#solicitadoCB").val());
    if (solicitado === CAMBIO) {
        solicitado = surtido;
    }


    tablaSurtido.row.add([
        $("#claveCB").val(),
        lote,
        $("#caducidadCB").val(),
        solicitado,
        surtido
    ]).draw(false);
}

function finalizarReceta() {

    swal({
        title: "Finalizar receta",
        text: "Esta a finalizar esta receta, ¿Esta seguro?.",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-warning",
        confirmButtonText: "Si, ¡He terminado!",
        cancelButtonText: "¡No!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    },
    function (isConfirm) {
        if (isConfirm) {
            dir = '../recetacb';
            $.ajax({
                type: 'POST',
                url: dir,
                data: {"accion": FINALIZAR, "tipo": tipo,
                    "id": idReceta},
                success: function (data) {
                    swal({
                        title: "Finalizar receta",
                        text: "El proceso de captura de la receta a sido completado.",
                        type: "success",
                        showCancelButton: true,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Aceptar"
                    }, function () {
                        var url = '../reportes/RecetaFarm.jsp?fol_rec=';
                        url = url.concat($("#folio").val(), "&tipo=", tipo);
                        window.open(url, '', 'width=1200,height=800,left=50,top=50,toolbar=no');
                        $("#btn_capturar").val("1");
                        window.location.reload();
                    });

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error", jqXHR, textStatus, errorThrown);
                    swal({
                        title: "Cancelación del producto",
                        text: jqXHR.responseText,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-default",
                        confirmButtonText: "Continuar"
                    });
                }
            });
        }
    });
}