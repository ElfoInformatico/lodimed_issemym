$(function ()
{
    $.ajax({
        url: context + "/regEntradas",
        data: {ban: 1},
        type: 'POST',
        async: true,
        success: function (data)
        {

            limpiarTabla();
            MostrarTabla(data);

        }, error: function (jqXHR, textStatus, errorThrown) {

            alert("Clave invalida");
        }



    });


});

function limpiarTabla() {
    $("#example").remove();
}

function MostrarTabla(data) {
    var json = JSON.parse(data);
    var aDataSet = [];
    for (var i = 0; i < json.length; i++) {
        var usuario = json[i].user;
        var fecha = json[i].fec;
        var hora = json[i].hora;
        var estatus = json[i].status;

        aDataSet.push([usuario, fecha, hora, estatus]);
    }
    $(document).ready(function () {
        $('#dynamic').html('<table class="display" width="100%" cellspacing="0"  id="example"></table>');
        $('#example').dataTable({
            "aaData": aDataSet,
            "button": 'aceptar',
            "bScrollInfinite": true,
            "bScrollCollapse": true,
            "sScrollY": "500px",
            "bProcessing": true,
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                /* Append the grade to the default row class name */
                if (aData[3] === "Error")
                {
                    $('td', nRow).css("color", "red");
                }
                else
                {
                    $('td', nRow).css("color", "green");
                }

            },
            "sPaginationType": "full_numbers",
            "order": [[0, "asc"]],
            "aoColumns": [
                {"sTitle": "Usuario", "sClass": "text-center"},
                {"sTitle": "Fecha", "sClass": "text-center"},
                {"sTitle": "Hora", "sClass": "text-center"},
                {"sTitle": "Estatus", "sClass": "text-center"}


            ]


        });

    }
    );
}





