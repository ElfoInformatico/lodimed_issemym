$(function ()
{
    $.ajax({
        url: context + "/inventariosCiclicos",
        data: {ban: 5},
        type: 'POST',
        async: false,
        beforeSend: function ()
        {
            $("#myModal").modal({show: true});
        },
        success: function (data)
        {

            limpiarTabla();
            MostrarTabla(data);
            $("#myModal").modal('hide');



        }, error: function (jqXHR, textStatus, errorThrown) {

            alert("Error en sistema");

        }



    });
});
function limpiarTabla() {
    $("#example").remove();
}

function MostrarTabla(data) {
    var json = JSON.parse(data);
    var aDataSet = [];
    for (var i = 0; i < json.length; i++) {
        var folio = json[i].folio;
        var fecha = json[i].fecha;
        var hora = json[i].hora;
        var dif = json[i].diferencia;
        var numCla = json[i].claves;
        var tipo = json[i].tipo;
        var button = '<button class="btn btn-danger" type="button" onclick="pdfExport(' + folio + ')" ><i class="fa fa-file-pdf-o" aria-hidden="true"> PDF</i></button> <button class="btn  btn-success" type="button" onclick="excelExport(' + folio + ')" ><i class="fa fa-file-excel-o" aria-hidden="true"> EXCEL</i></button>';

        aDataSet.push([folio, fecha, hora, dif, numCla, tipo, button]);



    }
    $(document).ready(function () {
        $('#dynamic').html('<table class="row-border" width="100%" cellspacing="0"  id="example"></table>');
        $('#example').dataTable({
            "aaData": aDataSet,
            "button": 'aceptar',
            "bScrollInfinite": true,
            "bScrollCollapse": true,
            "sScrollY": "500px",
            "bProcessing": true,
            "sPaginationType": "full_numbers",
            "order": [[0, "asc"]],
            "aoColumns": [
                {"sTitle": "Folio", "sClass": "text-center"},
                {"sTitle": "Fecha", "sClass": "center"},
                {"sTitle": "Hora", "sClass": "text-center"},
                {"sTitle": "Diferencia", "sClass": "text-center"},
                {"sTitle": "Número de claves", "sClass": "text-center"},
                {"sTitle": "Tipo", "sClass": "text-center"},
                {"sTitle": "Exportación", "sClass": "text-center"}
            ]


        });

    }
    );
}

function pdfExport(folio)
{
    window.open(context + "/reportes/imprimeCiclico.jsp?folio=" + folio + "");

}
function excelExport(folio)
{
    window.open(context + "/inventariosCiclicos?ban=1&folio=" + folio + "");
}