$(function ()
{
    $("#tableAuditar").hide();
    $("#divCiclico").hide();
    $("#generarClaves").click(function ()
    {
        var numeroClaves = $("#numeroCLaves").val();
       
        if (numeroClaves === "" || numeroClaves <= 0)
        {
            alert("Seleccionar un número valido de claves");
            return false;

        } 
        else
        {
            $.ajax({
                url: context + "/inventariosCiclicos",
                data: {ban: 4, cantidad:numeroClaves},
                type: 'POST',
                async: false,
                beforeSend: function () 
                {
                     $("#myModal").modal({show:true});
                },
                success: function (data)
                {
                    $("#tableAuditar").show();
                    limpiarTabla();
                    MostrarTabla(data);
                    
                    $("#myModal").modal('hide');
                    

                }, error: function (jqXHR, textStatus, errorThrown) {

                    alert("Error en sistema");

                }



            });
        }

    });
    
    $("#recorrer").click(function()
    {
        var detpro = [];
        var myObject = new Object();
        $('input[name=checkbox]').each(
                function () 
                {               
                    detpro.push
                    ({
                        id:$(this).attr("id"),
                        cantidad:$(this).val()
                    });
                    
                }
        );
        var jsonString = JSON.stringify(detpro);
        $.ajax({
                url: context + "/inventariosCiclicos",
                data: {ban: 2, detPro:jsonString, tipo: 2},
                type: 'POST',
                async: true,
                beforeSend: function () 
                {
                     $("#myModal").modal({show:true});
                },
                success: function (data)
                {
                    alert("Cíclico elaborado con éxito");
                    location.reload();
                    
                }, error: function (jqXHR, textStatus, errorThrown) {

                    alert("Error en sistema");

                }



            });
    });
    
    
    

});

function limpiarTabla() {
    $("#example").remove();
}

function MostrarTabla(data) {
    var json = JSON.parse(data);
    var aDataSet = [];
    for (var i = 0; i < json.length; i++) {
        var clave = json[i].clave;
        var des = json[i].descripcion;
        var lote = json[i].lote;
        var caducidad = json[i].caducidad;
        var detProduct = json[i].detProduct;
        var idInv = json[i].idInv;
        var input='<input  type="number" value="0" class="form-control" name="checkbox" id='+detProduct+' >';

        aDataSet.push([clave, des, lote, caducidad,input]);
        
        
        
    }
    $(document).ready(function () {
        $('#dynamic').html('<table class="row-border" width="100%" cellspacing="0"  id="example"></table>');
        $('#example').dataTable({
            "aaData": aDataSet,
            "button": 'aceptar',
            "bScrollInfinite": true,
            "bScrollCollapse": true,
            "sScrollY": "500px",
            "bProcessing": true,
            "sPagination": false,
            "paging": false,
            "ordering": true,
            "info": false,
            "order": [[0, "asc"]],
            "aoColumns": [
                {"sTitle": "Clave", "sClass": "text-center"},
                {"sTitle": "Descripcion", "sClass": "center"},
                {"sTitle": "Lote", "sClass": "text-center"},
                {"sTitle": "Caducidad", "sClass": "text-center"},
                {"sTitle": "Cantidad a capturar", "sClass": "text-center"}
            ]


        });

    }
    );
}

function MostrarResultado(data) {
    var json = JSON.parse(data);
    var aDataSet = [];
    for (var i = 0; i < json.length; i++) {
        var clave = json[i].clave;
        var des = json[i].descripcion;
        var lote = json[i].lote;
        var caducidad = json[i].caducidad;
        var detProduct = json[i].detProduct;
        var fisico = json[i].fisico;
        var inventario = json[i].inventario;
        var diferencia = json[i].diferencia;
        var input="";
        if(diferencia!=0)
        {
            input='<button  name="btn" onclick="kardex(\'' +clave+ '\',\'' +lote+ '\',\'' +caducidad+ '\')"  type="button" class="btn btn-success" ><span class=" glyphicon glyphicon-search" ></span></button>';            
        }
        else
        {
            input='';
        }
        
        
        aDataSet.push([clave, des, lote, caducidad,fisico,inventario,diferencia,input]);
        
        
        
    }
    $(document).ready(function () {
        $('#dynamic').html('<table class="row-border" width="100%" cellspacing="0"  id="example"></table>');
        $('#example').dataTable({
            "aaData": aDataSet,
            "button": 'aceptar',
            "bScrollInfinite": true,
            "bScrollCollapse": true,
            "sScrollY": "500px",
            "bProcessing": true,
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                /* Append the grade to the default row class name */
                
                if (aData[6] != "0")
                {
                    $('td', nRow).css("color", "red");
                }
                else
                {
                    $('td', nRow).css("color", "blue");                    
                }

            },
            "sPaginationType": "full_numbers",
            "order": [[0, "asc"]],
            "aoColumns": [
                {"sTitle": "Clave", "sClass": "text-center"},
                {"sTitle": "Descripcion", "sClass": "center"},
                {"sTitle": "Lote", "sClass": "text-center"},
                {"sTitle": "Caducidad", "sClass": "text-center"},
                {"sTitle": "Cantidad fisica", "sClass": "text-center"},
                {"sTitle": "Inventario", "sClass": "text-center"},
                {"sTitle": "Diferencia", "sClass": "text-center"},
                {"sTitle": "Kardex", "sClass": "text-center"}
                
            ]


        });

    }
    );
}

function kardex(clave,lote,caducidad)
{
    var cla=clave;
    var lot=lote;
    var cad=caducidad;
    
     window.open(context + "/farmacia/kardex.jsp?accion=buscarLote&cla_pro="+cla+"&lot_pro="+lot+"&cad_pro="+cad+"&id_ori=0"); 
    
}