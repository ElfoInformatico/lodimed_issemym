$(function ()
{
    var perfilUsuario =$("#idRol").val();
    
    
     $("#rolUsu").hide();
    if(perfilUsuario==="0")
    {
       $("#rolUsu").show();
    }
    
      $("#btnCancel").click(function()
    {
       
        $("#txtNom1").val("");
        $("#ape_pat1").val("");
        $("#ape_mat1").val("");
        $("#txtUser1").val("");
        $("#txtPass1").val("");
    });

    $("#btnSave1").click(function ()
    {
        
        var id = $("#txtIdUsu").val();
        var nom = $("#txtNom").val();
        var ap = $("#ape_pat").val();
        var am = $("#ape_mat").val();
        var usu = $("#txtUser").val();
        var pass = $("#txtPass").val();
        var sts = $("#selectsts").val();
        
        
        
        if (nom === "")
        {
            alert("Ingresar un nombre válido porfavor.");
            return false;
        }
        else if (ap === "")
        {
            alert("Ingresar un apellido paterno porfavor.");
            return false;
        }
        else if (am === "")
        {
            alert("Ingresar un apellido materno porfavor.");
            return false;
        } 
        else if (sts === "")
        {
            alert("Ingresar un estatus porfavor.");
            return false;
        }
        

        $.ajax({
            url: context + "/Usuarios",
            data: {ban: 1, id: id, nom: nom, ap: ap, am: am, pass: pass, sts:sts},
            type: 'POST',
            async: false,
            success: function (data)
            {
                var msj = data.msj;
                alert(msj);
                $("#btnCancel1").click();
                
                location.reload();

            }, error: function (jqXHR, textStatus, errorThrown) {

                alert("Error Contactar al departamento de sistemas");

            }
        });


    });

    $("#addUser").click(function () {

        $.ajax({
            url: context + "/Usuarios",
            data: {ban: 3},
            type: 'POST',
            async: false,
            success: function (data)
            {
                $("#txtUni1").val(data.uni);

            }, error: function (jqXHR, textStatus, errorThrown) {

                alert("Error Contactar al departamento de sistemas");

            }
        });

        $("#btnModal").click();


    });

    $("#btnSave").click(function () {
        var rolUsu = $("#idRol").val();
        var nom = $("#txtNom1").val();
        var ap = $("#ape_pat1").val();
        var am = $("#ape_mat1").val();
        var usu = $("#txtUser1").val();
        var pass = $("#txtPass1").val();
        var uni = $("#uni").val();
        var rol =$("#selectRol").val();
        
        if(rolUsu!=0)
        {            
            rol=1;
            alert(rol);
        }
        if (nom === "")
        {
            alert("Ingresar un nombre válido porfavor.");
            return false;
        }
        else if (ap === "")
        {
            alert("Ingresar un apellido paterno porfavor.");
            return false;
        }
        else if (am === "")
        {
            alert("Ingresar un apellido materno porfavor.");
            return false;
        }
        else if (pass === "")
        {
            alert("Ingresar un password porfavor.");
            return false;
        }
        else if (usu === "")
        {
            alert("Ingresar un usuario porfavor.");
            return false;
        }
        else if(rol==="")
        {
          alert("Seleccionar un perfil por favor");
          return false;
        }
        $.ajax({
            url: context + "/Usuarios",
            data: {ban: 2, nom: nom, ap: ap, am: am, pass: pass, usu: usu, rol:rol},
            type: 'POST',
            async: false,
            success: function (data)
            {
                var msj = data.msj;
                alert(msj);
                window.open("../../reportes/ticketUsu.jsp?nom=" +nom+ "&ap=" +ap+ "&am=" +am+ "&uni=" +uni+ "&usu=" +usu+ "&pass=" +pass+ "&tipo=5");
                $("#btnCancel1").click();
                location.reload();

            }, error: function (jqXHR, textStatus, errorThrown) {

                alert("Error Contactar al departamento de sistemas");

            }
        });


    });

    $("#txtUser1").focusout(function () {

        var usu = $("#txtUser1").val();
        $.ajax({
            url: context + "/Usuarios",
            data: {ban: 4, usu: usu},
            type: 'POST',
            async: false,
            success: function (data)
            {
                
                var msj = data.msj;
                if (msj === "ocupado")
                {
                    alert("Usuario actualmente en uso intente con otro diferente.");
                    $("#txtUser1").focus();
                }


            }, error: function (jqXHR, textStatus, errorThrown) {

                alert("Error Contactar al departamento de sistemas");

            }
        });
    });
    $("#usuario").focusout(function () {

        var usu = $("#usuario").val();
        $.ajax({
            url: context + "/Usuarios",
            data: {ban: 4, usu: usu},
            type: 'POST',
            async: false,
            success: function (data)
            {
                var msj = data.msj;
                if (msj === "ocupado")
                {
                    alert("Usuario actualmente en uso intente con otro diferente.");
                    $("#usuario").focus();
                }


            }, error: function (jqXHR, textStatus, errorThrown) {

                alert("Error Contactar al departamento de sistemas");

            }
        });
    });

  

});

function edit(id)
{
    $.ajax({
        url: context + "/Usuarios",
        data: {ban: 0, id: id},
        type: 'POST',
        async: false,
        success: function (data)
        {
            $("#txtIdUsu").val(data.Id);
            $("#ape_mat").val(data.Am).text(data.Am);
            $("#ape_pat").val(data.Ap).text(data.Ap);
            $("#txtNom").val(data.Nom).text(data.Nom);
            $("#txtUser").val(data.User).text(data.User);
            $("#txtUni").val(data.Uni).text(data.Uni);
            $("#txtUser").val(data.User).text(data.User);
            $("#txtPass").val(data.Pass).text(data.Pass);
            $("#selectsts option[value=" + data.Sts + "] ").prop('selected', 'selected');
            $("#btnModal1").click();

        }, error: function (jqXHR, textStatus, errorThrown) {

            alert("Error Contactar al departamento de sistemas");

        }
    });


}

function del(id) {

    var e = confirm("Seguro que desea eliminar al usuario?");

    if (e) 
    {
        $.ajax({
            url: context + "/Usuarios",
            data: {ban: 5, id:id},
            type: 'POST',
            async: false,
            success: function (data)
            {
                alert("Usuario suspendido con éxito");
                location.reload();

            }, error: function (jqXHR, textStatus, errorThrown) {

                alert("Error Contactar al departamento de sistemas");

            }
        });
       
    }
}

