$(function ()
{

    $("#tot").hide();
    $("#tablaNivel").hide();
    var foot = $("#example").find('tfoot');

    $('#f1').mask("9999-99-99", {placeholder: " "});
    $('#f2').mask("9999-99-99", {placeholder: " "});
    $("#f1").datepicker(
            {
                dateFormat: 'yy-mm-dd',
                changeYear: true,
                changeMonth: true,
                onClose: function (selectedDate) {
                    $("#f2").datepicker("option", "minDate", selectedDate);
                }
            });
    $("#f2").datepicker({
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true

    });

    $("#fechas").click(function () {


        var f1 = $("#f1").val();
        var f2 = $("#f2").val();

        if (f1 === "")
        {
            alert("Seleccione una fecha válida porfavor");
            return false;
        } else if (f2 === "")
        {
            alert("Seleccione una fecha válida porfavor");
            return false;
        } else
        {

            $.ajax({
                url: context + "/RecetasHL7Manuales",
                data: {ban: 1, f1: f1, f2: f2},
                type: 'POST',
                async: true,
                beforeSend: function ()
                {
                    $("#myModal").modal({show: true});
                },
                success: function (data)
                {
                    $("#tablaNivel").show();
                    $("#tot").show();
                    limpiarTabla();
                    MostrarTabla(data);
                    $("#myModal").modal('hide');

                }, error: function (jqXHR, textStatus, errorThrown) {

                    alert("Clave invalida");

                }



            });

        }
    });




});

function limpiarTabla() {
    $("#example").remove();
}

function MostrarTabla(data) {
    var json = JSON.parse(data);
    var patron = ",";
    var totpzSol = 0;
    var totpzSur = 0;
    var totRecetaE = 0;
    var totRecetaHl7 = 0;
    var totRecetaManu = 0;
    var totRecetaSurt = 0;
    var totRecetaPend = 0;

    var contador = 0;

    var aDataSet = [];
    for (var i = 0; i < json.length; i++) {
        var fecha = json[i].fecha;
        var receta = json[i].receta;
        var recetahl7 = json[i].recetahl7;
        var recetamanual = json[i].recetamanual;
        var recetasur = json[i].recetasur;
        var recetapen = json[i].recetapen;
        var pzSol = json[i].solicitado;
        var pzSur = json[i].surtido;



        contador++;

        totpzSol = totpzSol + pzSol;
        totpzSur = totpzSur + pzSur;
        totRecetaE = totRecetaE + receta;
        totRecetaHl7 = totRecetaHl7 + recetahl7;
        totRecetaManu = totRecetaManu + recetamanual;
        totRecetaSurt = totRecetaSurt + recetasur;
        totRecetaPend = totRecetaPend + recetapen;


        aDataSet.push([fecha, receta, recetahl7, recetamanual, recetasur, recetapen, pzSol, pzSur]);


    }

    totpzSol = formatoNumero(totpzSol);
    totpzSur = formatoNumero(totpzSur);
    totRecetaE = formatoNumero(totRecetaE);
    totRecetaHl7 = formatoNumero(totRecetaHl7);
    totRecetaManu = formatoNumero(totRecetaManu);
    totRecetaSurt = formatoNumero(totRecetaSurt);
    totRecetaPend = formatoNumero(totRecetaPend);

    $(document).ready(function () {

        $('#dynamic').html('<table class="cell-border" width="100%" cellspacing="0"  id="example"><thead  ></thead><tfoot style="text-align: center; text-decoration: underline; font-size: 20px " ><tr><td>Totales:</td><td>' + totRecetaE + '</td><td>' + totRecetaHl7 + '</td><td>' + totRecetaManu + '</td><td>' + totRecetaSurt + '</td><td>' + totRecetaPend + '</td><td>' + totpzSol + '</td><td>' + totpzSur + '</td></tr></tfoot></table>');
        $('#example').dataTable({
            "aaData": aDataSet, "button": 'aceptar',
            "bScrollInfinite": true,
            "bScrollCollapse": true,
            "sScrollY": "500px",
            "bFooter": true,
            "bProcessing": true,
            dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
            "sPaginationType": "full_numbers",
            "bAutoWidth": false,
            "aoColumns": [
                {"sTitle": "Fecha", "sClass": "text-center", "sWidth": "10%"},
                {"sTitle": "Recetas Elaboradas", "sClass": "text-center"},
                {"sTitle": "Recetas HL7", "sClass": "text-center"},
                {"sTitle": "Recetas Manuales", "sClass": "text-center"},
                {"sTitle": "Recetas Surtidas", "sClass": "text-center"},
                {"sTitle": "Recetas Pendientes", "sClass": "text-center"},
                {"sTitle": "Piezas Solicitadas", "sClass": "text-center"},
                {"sTitle": "Piezas Surtidas", "sClass": "text-center"}
            ]
        });


    }
    );
}

function formatoNumero(numero, decimales, separadorDecimal, separadorMiles) {
    var partes, array;

    if (!isFinite(numero) || isNaN(numero = parseFloat(numero))) {
        return "";
    }
    if (typeof separadorDecimal === "undefined") {
        separadorDecimal = ".";
    }
    if (typeof separadorMiles === "undefined") {
        separadorMiles = ",";
    }

    // Redondeamos
    if (!isNaN(parseInt(decimales))) {
        if (decimales >= 0) {
            numero = numero.toFixed(decimales);
        } else {
            numero = (
                    Math.round(numero / Math.pow(3, Math.abs(decimales))) * Math.pow(3, Math.abs(decimales))
                    ).toFixed();
        }
    } else {
        numero = numero.toString();
    }

    // Damos formato
    partes = numero.split(".", 2);
    array = partes[0].split("");
    for (var i = array.length - 3; i > 0 && array[i - 1] !== "-"; i -= 3) {
        array.splice(i, 0, separadorMiles);
    }
    numero = array.join("");

    if (partes.length > 1) {
        numero += separadorDecimal + partes[1];
    }

    return numero;
}
