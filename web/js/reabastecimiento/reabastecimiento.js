$(function ()
{
    $.ajax({
        url: context + "/Reabastecimiento",
        data: {ban: 1},
        type: 'POST',
        async: true,
        beforeSend: function ()
        {
            $("#myModal").modal({show: true});
        },
        success: function (data)
        {
            limpiarTabla();
            MostrarTabla(data);
            $("#myModal").modal('hide');

        }, error: function (jqXHR, textStatus, errorThrown) {

            alert("Clave invalida");
        }



    });
    $("#enviar").click(function ()
    {

        $.ajax({
            url: context + "/Reabastecimiento",
            data: {ban: 3},
            type: 'POST',
            async: true,
            beforeSend: function ()
            {
                $("#myModal").modal({show: true});
            },
            success: function (data)
            {

                var msj = data.msj;
                alert(msj);
                $("#myModal").modal('hide');

            }, error: function (jqXHR, textStatus, errorThrown) {

                alert("Clave invalida");
            }



        });

        $("#calcRepo").click(function ()
        {
            var dias = $("#dia").val();

            if (dias === "")
            {
                alert("Ingresar dias restantes para visita.");
                return false;
            }

        });
        $.ajax({
            url: context + "/Reabastecimiento",
            data: {ban: 2, dias: dias},
            type: 'POST',
            async: true,
            success: function (data)
            {

                limpiarTabla();
                MostrarTabla(data);

            }, error: function (jqXHR, textStatus, errorThrown) {

                alert("Clave invalida");
            }



        });

    });

    $("#descarga").click(function () {
        window.location = 'gnr_reabast.jsp?dias=0&submit=submit';

    });


});

function limpiarTabla() {
    $("#example").remove();
}

function MostrarTabla(data) {
    var json = JSON.parse(data);
    var aDataSet = [];
    for (var i = 0; i < json.length; i++) {
        var clave = json[i].cl;
        var des = json[i].des;
        var cdm = json[i].cdm;
        var ifs = json[i].ifs;
        var exist = json[i].exist;
        var sob = json[i].sob;
        var cs = json[i].cs;
        aDataSet.push([clave, des, cdm, ifs, exist, sob, cs]);
    }
    $(document).ready(function () {
        $('#dynamic').html('<table class="cell-border" width="100%" cellspacing="0"  id="example"></table>');
        $('#example').dataTable({
            "aaData": aDataSet,
            "button": 'aceptar',
            "bScrollInfinite": true,
            "bScrollCollapse": true,
            "sScrollY": "500px",
            "bProcessing": true,
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                /* Append the grade to the default row class name */
                if (aData[5] > "0")
                {
                    $('td', nRow).css("color", "red");
                }

            },
            "sPaginationType": "full_numbers",
            "order": [[0, "asc"]],
            "aoColumns": [
                {"sTitle": "Clave", "sClass": "text-center"},
                {"sTitle": "Descripcion", "sClass": "center"},
                {"sTitle": "Consumo mensual", "sClass": "text-center"},
                {"sTitle": "Stock Max", "sClass": "text-center"},
                {"sTitle": "Existencia Actual", "sClass": "center"},
                {"sTitle": "Sobreabasto", "sClass": "text-center"},
                {"sTitle": "Cant. sugerida", "sClass": "text-center"}
            ]


        });

    }
    );
}




function tabular(e, obj) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla !== 13)
        return;
    frm = obj.form;
    for (i = 0; i < frm.elements.length; i++)
        if (frm.elements[i] === obj) {
            if (i === frm.elements.length - 1)
                i = -1;
            break
        }
    frm.elements[i + 1].focus();
    return false;
}


function validarCarga(obj) {
    var frm = obj.form;
    
}