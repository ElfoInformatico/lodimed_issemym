$(function ()
{
    $("#searchClave").click(function ()
    {

        var cb = $("#cb").val();
        var clave = $("#claveKardex").val();


        if (cb === "" && clave === "")
        {

            swal({
                title: "Campos vacíos",
                text: "Ingresar un cricterio de búsqueda por favor.",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-default",
                confirmButtonText: "Continuar",
                closeOnConfirm: true
            });

            return false;
        } else if (cb === "" && clave !== "")
        {
            $("#myModal").modal({show: true});
            $.ajax({
                url: "../kardexVista",
                data: {accion: "CLAVE", clave: clave},
                type: 'POST',
                async: true,
                success: function (data)
                {
                    $("#claveResultado").text(data.clave);
                    $("#DescripcionResultado").text(data.descripcion);
                    $("#existenciasActuales").text("Existencias:" + data.existencia);
                }, error: function (jqXHR, textStatus, errorThrown) {

                    alert("Clave invalida");

                }



            });
            $.ajax({
                url: "../kardexVista",
                data: {accion: "kardex", clave: clave},
                type: 'POST',
                async: true,
                success: function (data)
                {
                    limpiarTabla();
                    MostrarTabla(data);
                    $("#myModal").modal('hide');
                }, error: function (jqXHR, textStatus, errorThrown) {

                    alert("Clave invalida");

                }



            });

        } else if (cb !== "" && clave === "")
        {

        }

    });

    $("#reloadButton").click(function ()
    {
        var clave = $("#claveKardex").val();
        $("#myModal").modal({show: true});
        $.ajax({
            url: "../kardexVista",
            data: {accion: "CLAVE", clave: clave},
            type: 'POST',
            async: true,
            success: function (data)
            {
                $("#claveResultado").text(data.clave);
                $("#DescripcionResultado").text(data.descripcion);
                $("#existenciasActuales").text("Existencias:" + data.existencia);
            }, error: function (jqXHR, textStatus, errorThrown) {

                alert("Clave invalida");

            }



        });
        $.ajax({
            url: "../kardexVista",
            data: {accion: "kardex", clave: clave},
            type: 'POST',
            async: true,
            success: function (data)
            {
                limpiarTabla();
                MostrarTabla(data);
                $("#myModal").modal('hide');
            }, error: function (jqXHR, textStatus, errorThrown) {

                alert("Clave invalida");

            }



        });
    });


});

function limpiarTabla() {
    $("#example").remove();
}

function MostrarTabla(data) {
    var json = JSON.parse(data);
    var aDataSet = [];
    for (var i = 0; i < json.length; i++) {
        var clave = json[i].clave;
        var lote = json[i].lote;
        var cad = json[i].cad;
        var origen = json[i].origen;
        var cantidad = json[i].cantidad;
        var tipoMov = json[i].tipoMov;
        var abasto = json[i].abasto;
        var folRec = json[i].folRec;
        var paciente = json[i].paciente;
        var medico = json[i].medico;
        var fecha = json[i].fecha;
        var obs = json[i].obs;
        var sumatoria = json[i].sumatoria;
        var usuario = json[i].usuario;

        aDataSet.push([clave, lote, cad, origen, cantidad, tipoMov, abasto,
            folRec, paciente, medico, fecha, obs, usuario, sumatoria]);



    }
    $(document).ready(function () {
        $('#dynamic').html('<table class="table table-bordered table-condensed table-hover" cellspacing="0" width="100%" id="example"></table>');
        $('#example').dataTable({
            "aaData": aDataSet,
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                /* Append the grade to the default row class name */

                if (aData[4] > 0)
                {
                    $('td', nRow).addClass("success");
                }
            },
            "aoColumns": [
                {"sTitle": "Clave", "sClass": "text-center"},
                {"sTitle": "Lote", "sClass": "center"},
                {"sTitle": "Caducidad", "sClass": "text-center"},
                {"sTitle": "Origen", "sClass": "text-center"},
                {"sTitle": "Cantidad", "sClass": "text-center"},
                {"sTitle": "Tipo Mov", "sClass": "text-center"},
                {"sTitle": "Abasto", "sClass": "text-center"},
                {"sTitle": "FolReceta", "sClass": "text-center"},
                {"sTitle": "Paciente", "sClass": "text-center"},
                {"sTitle": "Médico", "sClass": "text-center"},
                {"sTitle": "Fecha", "sClass": "text-center", "sWidth": "200px"},
                {"sTitle": "Observaciones", "sClass": "text-center"},
                {"sTitle": "Usuario", "sClass": "text-center"},
                {"sTitle": "Sumatoria", "sClass": "text-center"}
            ]


        });

    }
    );
}