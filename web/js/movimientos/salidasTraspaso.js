/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* global context, OBTENER_CATALAGO_PRODUCTOS, TRASPASO_SALIDA, BUSQUEDA_PRODUCTO, APARTADO_SALIDA, listaApartados, QUITAR_APARTADO_SALIDA */

var lotesDisponibles;


window.onbeforeunload = preguntarAntesDeSalir;

function preguntarAntesDeSalir() {
    return "¿Seguro que quieres salir? Los cambios que hayas realizado se perderán";
}

function validaContra() {
    var password = $("#confirmarPass").val();
    $.ajax({
        type: "POST",
        url: "../Farmacias",
        dataType: "json",
        data: {accion: "verificar", pass: password},
        success: function (data) {
            if (data.permitido) {

                aplicar();
            } else {
                $("#confirmarPass").val("");

                swal({
                    title: "Validacion contraseña",
                    text: "Datos incorrectos, por favor verique la defaultrmación ingresada.",
                    type: "Error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-default",
                    confirmButtonText: "Continuar"
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            swal({
                title: "Validacion contraseña",
                text: jqXHR.responseText,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-default",
                confirmButtonText: "Continuar"
            });
        }
    }
    );
}

function buscar() {

    if ($("#listaUnidades").val() !== "") {
        $("#listaUnidades").attr('disabled', 'disabled');
        $("#sinExistencia").hide();
        $("#seleccion").hide();
        $("#ajuste").hide();
        $('#lot_pro').empty();
        $('#cad_pro').empty();
        $('#id_ori').empty();
        $('#lot_pro').append(new Option("--Seleccione el Lote--", 0));
        $('#cad_pro').append(new Option("--Seleccione la Caducidad --", 0));
        $('#id_ori').append(new Option("--Seleccione el Origen--", 0));

        var cb, clave, patron;
        cb = $('#cb').val();
        if (cb !== "") {
            patron = cb;
        } else {
            clave = $('#claveBusqueda').val();
            if (clave !== "") {
                patron = clave;
            }
        }


        $.ajax({
            type: "GET",
            url: "../mover",
            data: {"accion": BUSQUEDA_PRODUCTO,
                "busqueda": patron},
            dataType: "json",
            success: function (data) {
                console.log("busqueda", data);
                lotesDisponibles = data;
                $("#seleccion").show();
                $("#ingresar").show();

                $("#claveProd").val(data[0].clave);
                $("#descPro").val(data[0].descripcion);
                var aux;
                for (var i = 0; i < data.length; i++) {
                    aux = data[i];
                    $('#lot_pro').append(new Option(aux.lote, i));
                    $('#cad_pro').append(new Option(aux.caducidad, i));
                    $('#id_ori').append(new Option(aux.origen_descripcion, i));
                }
                $('#cb').val("");
                //$('#claveBusqueda').val("");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                swal({
                    title: "Buscar producto",
                    text: jqXHR.responseText,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-default",
                    confirmButtonText: "Continuar"
                });
            }
        });
    } else {
        alert("Elija una unidad de destino");
    }
}

function seleccionar() {
    $("#sinExistencia").hide();
    $("#ajuste").show();
    $("#registros").show();
    var data = lotesDisponibles[parseInt($('#lot_pro').val())];

    $('#cantidadExistente').val(data.cantidad);
    $('#id_inv').val(data.detalle_producto); //Es necesario para ajustar la cantidad.
    $("#cantAjuste").attr({
        "max": data.cantidad
    });
    $("#loteLabel").text("Lote: " + data.lote);
    $("#caducidadLabel").text("Caducidad: " + data.caducidad);
    $("#origenLabel").text("Origen: " + data.origen);
}

$('#lot_pro').on('change', function () {
    document.getElementById('cad_pro').selectedIndex = this.selectedIndex;
    document.getElementById('id_ori').selectedIndex = this.selectedIndex;
});

$('#cad_pro').on('change', function () {
    document.getElementById('lot_pro').selectedIndex = this.selectedIndex;
    document.getElementById('id_ori').selectedIndex = this.selectedIndex;
});

$('#id_ori').on('change', function () {
    document.getElementById('lot_pro').selectedIndex = this.selectedIndex;
    document.getElementById('cad_pro').selectedIndex = this.selectedIndex;
});

function validarDatos() {
    var uni_des = $('#listaUnidades').val();

    var id_inv, obs;
    var cantAjuste = 0, cantidadExistente = 0;
    id_inv = $('#id_inv').val();
    cantidadExistente = parseInt($('#cantidadExistente').val());
    obs = $('#obs').val();
    cantAjuste = parseInt($('#cantAjuste').val());

    if (obs !== "" && uni_des !== "") {
        $('#Observaciones').modal('toggle');
    }
}

function apartarInsumo() {
    var detalleProducto, obs, cantAjuste, cantidadExistente;
    detalleProducto = $('#id_inv').val();
    cantidadExistente = parseInt($('#cantidadExistente').val());
    //obs = $('#obs').val();
    cantAjuste = parseInt($('#cantAjuste').val());
    if (cantAjuste > 0 && obs !== "" & cantAjuste <= cantidadExistente) {
        var r = confirm("¿Desea agregar el ajuste?");
        if (r) {
            var $form = $(this);
            $.ajax({
                type: $form.attr('method'),
                url: "../mover",
                data: {"accion": APARTADO_SALIDA,
                    "detalle": detalleProducto, "cantidad": cantAjuste},
                dataType: "json",
                success: function (data) {
                    console.log("ajuste", data);
                    listaApartados.push(data.id);
                    swal({
                        title: "Traspaso",
                        text: "Clave agregada correctamente",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Continuar"
                    }, function () {
                        //location.reload();
                        buscar();
                        $("#registros").show();
                        $('#cantAjuste').val('0');
                        $("#tablaInsumoApartado").load(location.href + " #tablaInsumoApartado");

                    });

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    swal({
                        title: "Salida por traspaso",
                        text: jqXHR.responseText,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-default",
                        confirmButtonText: "Continuar"
                    });
                }
            });
        }
    }
}


function aplicar() {
    var observaciones, cla_uni;

    var uni_des = $('#listaUnidades').val();
    observaciones = $('#obs').val();
    cla_uni = $('#cla_uni').val();
    var r = confirm("¿Desea agregar el ajuste?");
    if (r) {
        var $form = $(this);
        for (var i = 0; i < listaApartados.length; i += 1) {
            $.ajax({
                type: $form.attr('method'),
                url: "../mover",
                data: {"accion": TRASPASO_SALIDA, "observaciones": cla_uni + " | " + uni_des + " | " + observaciones, "idApartado": listaApartados[i]},
                dataType: "json",
                success: function (data) {
                    swal({
                        title: "Salida por traspaso",
                        text: "Traspaso exitoso. ",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Continuar"
                    }, function () {
                        window.onbeforeunload = null;
                        location.reload();
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    swal({
                        title: "Salida por traspaso",
                        text: jqXHR.responseText,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-default",
                        confirmButtonText: "Continuar"
                    });
                }

            });
        }

    }
}

function removerInsumoApartado(e) {
    var idApartamiento = e.value;
    var r = confirm("¿Desea agregar el ajuste?");
    if (r) {
        var $form = $(this);

        $.ajax({
            type: $form.attr('method'),
            url: "../mover",
            data: {"accion": QUITAR_APARTADO_SALIDA, "idApartado": idApartamiento},
            dataType: "json",
            success: function (data) {
                swal({
                    title: "Traspaso",
                    text: "Clave removida correctamente",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonClass: "btn-success",
                    confirmButtonText: "Continuar"
                }, function () {
                    $("#tablaInsumoApartado").load(location.href + " #tablaInsumoApartado");

                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                swal({
                    title: "Salida por traspaso",
                    text: jqXHR.responseText,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-default",
                    confirmButtonText: "Continuar"
                });
            }
        });
    }
}



$('#lot_pro').on('change', function () {
    document.getElementById('cad_pro').selectedIndex = this.selectedIndex;
});
$('#cad_pro').on('change', function () {
    document.getElementById('lot_pro').selectedIndex = this.selectedIndex;
});

$("[data-behavior~=only-alphanum]").alphanum({
    allowSpace: false,
    allowOtherCharSets: false,
    allow: '.'
});
$("[data-behavior~=only-num]").numeric({
    allowMinus: false,
    allowThouSep: false
});


$("[data-behavior~=only-alpha]").alphanum({
    allowNumeric: false,
    allowSpace: false,
    allowOtherCharSets: true
});

$.ajax({
    url: context + "/Catalogo",
    data: {ban: OBTENER_CATALAGO_PRODUCTOS},
    type: 'POST',
    async: true,
    dataType: 'json',
    success: function (data)
    {
        var claves_productos = [];

        data.forEach(function (value, id, arr) {
            claves_productos.push(value.id);
        });

        $("#claveBusqueda").autocomplete({
            source: claves_productos
        });
    }, error: function (jqXHR, textStatus, errorThrown) {

        alert("Error Contactar al departamento de sistemas");

    }
});