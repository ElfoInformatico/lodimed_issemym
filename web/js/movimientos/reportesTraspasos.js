/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* global DES_UNI */

$(function ()
{
    $('#hora_ini').mask("99-99-9999", {placeholder: " "});
    $('#hora_fin').mask("99-99-9999", {placeholder: " "});
    $("#hora_ini").datepicker(
            {
                dateFormat: 'yy-mm-dd',
                changeYear: true,
                changeMonth: true,
                onClose: function (selectedDate) {
                    $("#hora_fin").datepicker("option", "minDate", selectedDate);
                }
            });
    $("#hora_fin").datepicker({
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true,
        onClose: function (selectedDate) {
            $("#hora_ini").datepicker("option", "maxDate", selectedDate);
        }

    });

    $("#generar").click(function ()
    {
        var hora_ini = $("#hora_ini").val();
        var hora_fin = $("#hora_fin").val();

        if (hora_ini === "")
        {
            alert("Ingresar una fecha de inicio porfavor.");
            return false;
        } else if (hora_fin === "")
        {
            alert("Ingresar una fecha final porfavor.");
            return false;

        } else
        {
            $.ajax({
                url: "${pageContext.servletContext.contextPath}/repSolSur",
                data: {ban: 1, hora_ini: hora_ini, hora_fin: hora_fin},
                type: 'POST',
                async: true,
                success: function (data)
                {

                    limpiarTabla();
                    MostrarTabla(data);

                }, error: function (jqXHR, textStatus, errorThrown) {

                    alert("Clave invalida");
                }



            });
        }
    });


});

function limpiarTabla() {
    $("#example").remove();
}

function MostrarTabla(data) {

    var json = JSON.parse(data);
    var totpzSol = 0;
    var totpzSur = 0;
    var aDataSet = [];
    for (var i = 0; i < json.length; i++) {
        var cons = json[i].cons;
        var fol = json[i].fol;
        var cl = json[i].cl;
        var des = json[i].des;
        var sol = json[i].sol;
        var sur = json[i].sur;

        totpzSol = totpzSol + sol;
        totpzSur = totpzSur + sur;

        aDataSet.push([cons, fol, cl, des, sol, sur]);
    }
    $(document).ready(function () {
        $('#dynamic').html('<table class="cell-border" width="100%" cellspacing="0"  id="example"><tfoot style="text-align: center; text-decoration: underline; font-size: 20px " ><tr><td></td><td></td><td></td><td>Totales:</td><td>' + totpzSol + '</td><td>' + totpzSur + '</td></tr></tfoot></table>');
        $('#example').dataTable({
            "aaData": aDataSet,
            "button": 'aceptar',
            "bScrollInfinite": true,
            "bScrollCollapse": true,
            "sScrollY": "500px",
            "bProcessing": true,
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                /* Append the grade to the default row class name */
                if (aData[4] > aData[5])
                {
                    $('td', nRow).css("color", "red");
                }

            },
            "sPaginationType": "full_numbers",
            "order": [[0, "asc"]],
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "../swf/copy_csv_xls_pdf.swf"
            },
            "aoColumns": [
                {"sTitle": "Consecutivo", "sClass": "text-center"},
                {"sTitle": "Folio", "sClass": "center"},
                {"sTitle": "Clave", "sClass": "text-center"},
                {"sTitle": "Descripción", "sClass": "text-center"},
                {"sTitle": "Solicitado", "sClass": "center"},
                {"sTitle": "Surtido", "sClass": "text-center"}

            ]


        });
    });
}


function generarReportePDF() {
    var listaUnidades = $('#listaUnidades').val();
    var fechaInicio = $('#hora_ini').val();
    var fechaFin = $('#hora_fin').val();


    if (confirm("Desea generar el reporte?")) {
        window.open('ReporteSalidaTraspaso.jsp?fec_ini=' + fechaInicio + '&fec_fin=' + fechaFin + '&cla_uni=' + listaUnidades + "&des_uni=" + DES_UNI);
    }
}