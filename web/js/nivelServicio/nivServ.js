$(function ()
{
    
    $("#tot").hide();
    $("#tablaNivel").hide();
    var foot = $("#example").find('tfoot');

    $('#f1').mask("9999-99-99", {placeholder: " "});
    $('#f2').mask("9999-99-99", {placeholder: " "});
    $("#f1").datepicker(
            {
                dateFormat: 'yy-mm-dd',
                changeYear: true,
                changeMonth: true,
                onClose: function (selectedDate) {
                    $("#f2").datepicker("option", "minDate", selectedDate);
                }
            });
    $("#f2").datepicker({
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true

    });

    $("#fechas").click(function () {


        var f1 = $("#f1").val();
        var f2 = $("#f2").val();

        if (f1 === "")
        {
            alert("Seleccione una fecha válida porfavor");
            return false;
        }
        else if (f2 === "")
        {
            alert("Seleccione una fecha válida porfavor");
            return false;
        }

        else
        {
            
            $.ajax({
                url: context + "/nivelServicio",
                data: {ban: 1, f1: f1, f2: f2},
                type: 'POST',
                async: true,
                beforeSend: function () 
                {
                     $("#myModal").modal({show:true});
                },
                 success: function (data)
                {
                    $("#tablaNivel").show();
                    $("#tot").show();
                    limpiarTabla();
                    MostrarTabla(data);
                    $("#myModal").modal('hide');

                }, error: function (jqXHR, textStatus, errorThrown) {

                    alert("Clave invalida");

                }



            });

        }
    });




});

function limpiarTabla() {
    $("#example").remove();
}

function MostrarTabla(data) {
    var json = JSON.parse(data);
    var patron = ",";
    var totpzSol = 0;
    var totpzNoSur = 0;
    var totClaSol = 0;
    var totClaNoSur = 0;
    var totRecSol = 0;
    var totPor = 0.0;
    var totPzVend = 0;
    var totImpor = 0.0;
    var totRecPar = 0;
    var totRec100 = 0;

    var contador = 0;

    var aDataSet = [];
    for (var i = 0; i < json.length; i++) {
        var fecha = json[i].fecha;
        var pzSol = json[i].pzSol;
        var pzNoSur = json[i].pzNoSur;
        var claSol = json[i].claSol;
        var claNoSur = json[i].claNoSur;
        var recSol = json[i].recSol;
        var recPar = json[i].recPar;
        var porc = json[i].porc;
        var pzVend = json[i].pzVend;
        var importe = json[i].importe;

        contador++;

        var myString = recPar;
        var arr = myString.split('/');

        var recPar2 = parseInt(arr[1]);

        var rec100 = parseInt(arr[0]);


        totpzSol = totpzSol + pzSol;
        totpzNoSur = totpzNoSur + pzNoSur;
        totClaSol = totClaSol + claSol;
        totRecPar = totRecPar + recPar2;
        totClaNoSur = totClaNoSur + claNoSur;
        totRecSol = totRecSol + recSol;
        totPor = totPor + porc;
        totPzVend = pzVend + totPzVend;
        totImpor = importe + totImpor;
        importe = formatoNumero(importe);


        totRec100 = rec100 + totRec100;



        aDataSet.push([fecha, pzSol, pzNoSur, claSol, claNoSur, recSol, recPar, porc + '%', pzVend, '$' + importe]);


    }

    totpzSol = formatoNumero(totpzSol);
    totpzNoSur = formatoNumero(totpzNoSur);
    totClaSol = formatoNumero(totClaSol);
    totClaNoSur = formatoNumero(totClaNoSur);
    totRecSol = formatoNumero(totRecSol);
    totPzVend = formatoNumero(totPzVend);
    totImpor = formatoNumero(totImpor, 2);
    totPor = totPor / contador;
    totPor = formatoNumero(totPor, 2);
    totRec100 = formatoNumero(totRec100);

    $(document).ready(function () {

        $('#dynamic').html('<table class="cell-border" width="100%" cellspacing="0"  id="example"><thead  ></thead><tfoot style="text-align: center; text-decoration: underline; font-size: 20px " ><tr><td>Totales:</td><td>' + totpzSol + '</td><td>' + totpzNoSur + '</td><td>' + totClaSol + '</td><td>' + totClaNoSur + '</td><td>' + totRecSol + '</td><td>' + totRec100 + ' / ' + totRecPar + '</td><td>' + totPor + '%</td><td>' + totPzVend + '</td><td>$' + totImpor + '</td></tr></tfoot></table>');
        $('#example').dataTable({
            "aaData": aDataSet, "button": 'aceptar',
            "bScrollInfinite": true,
            "bScrollCollapse": true,
            "sScrollY": "500px",
            "bFooter": true,
            "bProcessing": true,
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "../swf/copy_csv_xls_pdf.swf"
            },
            "sPaginationType": "full_numbers",
            "bAutoWidth": false,
            "aoColumns": [
                {"sTitle": "Fecha", "sClass": "text-center", "sWidth": "10%"},
                {"sTitle": "Piezas Solicitadas", "sClass": "text-center"},
                {"sTitle": "Piezas No Surtidas", "sClass": "text-center"},
                {"sTitle": "Claves solicitadas", "sClass": "text-center"},
                {"sTitle": "Claves No Surtidas", "sClass": "text-center"},
                {"sTitle": "Recetas Solicitadas", "sClass": "text-center"},
                {"sTitle": "Rec 100 / Rec Parciales ", "sClass": "text-center"},
                {"sTitle": "Porcentaje", "sClass": "text-center"},
                {"sTitle": "Piezas Vendidas", "sClass": "text-center"},
                {"sTitle": "Importe De Ventas ", "sClass": "text-center"}



            ]
        });


    }
    );
}

function formatoNumero(numero, decimales, separadorDecimal, separadorMiles) {
    var partes, array;

    if (!isFinite(numero) || isNaN(numero = parseFloat(numero))) {
        return "";
    }
    if (typeof separadorDecimal === "undefined") {
        separadorDecimal = ".";
    }
    if (typeof separadorMiles === "undefined") {
        separadorMiles = ",";
    }

    // Redondeamos
    if (!isNaN(parseInt(decimales))) {
        if (decimales >= 0) {
            numero = numero.toFixed(decimales);
        } else {
            numero = (
                    Math.round(numero / Math.pow(3, Math.abs(decimales))) * Math.pow(3, Math.abs(decimales))
                    ).toFixed();
        }
    } else {
        numero = numero.toString();
    }

    // Damos formato
    partes = numero.split(".", 2);
    array = partes[0].split("");
    for (var i = array.length - 3; i > 0 && array[i - 1] !== "-"; i -= 3) {
        array.splice(i, 0, separadorMiles);
    }
    numero = array.join("");

    if (partes.length > 1) {
        numero += separadorDecimal + partes[1];
    }

    return numero;
}
