$(function ()
{
    dir = '../recetacb';
    $.ajax({
        dataType: 'json',
        type: "POST",
        url: dir,
        data: {"accion": FOLIO_MEDICO, "tipo": tipo,
            "folio": "-1"},
        async: false,
        success: function (data) {
            $('#folio').val(data.id);
            $('#tipoCons').val(data.tipo_consulta);
            $('#medico_jq').val(data.nombre_completo);
            $('#mostrar3').click();
            
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Error", jqXHR, textStatus, errorThrown);
            swal({
                title: "Creación receta",
                text: jqXHR.responseText,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-default",
                confirmButtonText: "Continuar"
            }, function () {
                $('#folio').val("");
                $('#cedula').val("");

                $("#nombre_jq").prop('readonly', false);
                $("#medico_jq").prop('readonly', false);
                $("#sp_pac").prop('readonly', false);

                $('#nom_pac').val("");
                $('#tipoCons').val("0");
                $('#medico').val("");
                $('#sexo').val("");
                $('#fec_nac').val("");
                $('#txt_carnet').val("");
                $('#fol_sp').val("");
                $('#nombre_jq').val("");
                $('#medico_jq').val("");

            });
        }
    });
});
