var idReceta = -1;

function cancelarReceta() {
    console.log("id", idReceta);
    if (idReceta === -1) {
        return;
    }
    $.ajax({
        type: "POST",
        url: '../recetacb',
        data: {"accion": CANCELAR, "tipo": COLECTIVO, "folio": $('#folio').val(), "id": idReceta},
        async: false,
        success: function (data)
        {
            window.location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Error", jqXHR, textStatus, errorThrown);
            swal({
                title: "Cancelación de la receta",
                text: jqXHR.responseText,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-default",
                confirmButtonText: "Continuar"
            });
        }
    });

}

function validarEncabezado(paso) {
    var fecha = $('#fecha1').val();
    if (fecha === "" && paso != 1) {
        $('#listServicios').val("0").trigger("change");
        $('#fecha1').focus();
        swal({
            title: "Fecha no ingresada",
            text: "Debe ingresar el fecha de la receta para continuar.",
            type: "warning",
            showCancelButton: false,
            confirmButtonClass: "btn-warning",
            confirmButtonText: "Ingresar",
            closeOnConfirm: false
        });

        return false;
    }

    if (paso === 1) {
        return true;
    }

    var servicio = $('#listServicios').val();
    if (servicio === "0" && paso != 2) {
        $('#medico_jq').val("");
        $('#listServicios').focus();
        swal({
            title: "Servicio no ingresado",
            text: "Debe ingresar el servicio de la receta para continuar.",
            type: "warning",
            showCancelButton: false,
            confirmButtonClass: "btn-warning",
            confirmButtonText: "Ingresar"
        });

        return false;
    }

    if (paso === 2) {
        return true;
    }

    var medico = $('#medico_jq').val();
    if (medico === "" && paso != 3) {
        $('#medico_jq').focus();
        swal({
            title: "Médico no ingresado",
            text: "Debe ingresar el médico de la receta para continuar",
            type: "warning",
            showCancelButton: false,
            confirmButtonClass: "btn-warning",
            confirmButtonText: "Ingresar"
        });

        return false;
    }

    return true;
}

function sumar() {
    //alert('hola');
    var unidades = $("#unidades").val();
    if (unidades === "")
        unidades = 0;

    //document.formulario_receta.piezas_sol.value = Math.ceil(total);
    //document.formulario_receta.can_sol.value = Math.ceil(cajas);
    $("#piezas_sol").val(Math.ceil(unidades));
    $("#can_sol").val(Math.ceil(unidades));
}


function tabular(e, obj)
{
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla !== 13)
        return;
    frm = obj.form;
    for (i = 0; i < frm.elements.length; i++)
        if (frm.elements[i] === obj)
        {
            if (i === frm.elements.length - 1)
                i = -1;
            break
        }
    /*ACA ESTA EL CAMBIO*/
    if (frm.elements[i + 1].disabled === true)
        tabular(e, frm.elements[i + 1]);
    else
        frm.elements[i + 1].focus();
    return false;
}

$('#btn_capturar').click(function () {
    var cla_pro = $('#cla_pro').val();
    var des_pro = $('#des_pro').val();
    var form = $('#formulario_receta');
    if (cla_pro !== "" && des_pro !== "") {
        if ($('#can_sol').val() === "" || $('#can_sol').val() === "0") {
            alert('Capture la cantidad a entregar');
            return;
        }
        var cantSol = parseInt($('#can_sol').val());
        dir = '../recetacb';
        $.ajax({
            type: 'POST', url: dir,
            data: {"accion": CAPTURA, "tipo": COLECTIVO, "id": idReceta, "clave": cla_pro, "solicitado": cantSol,
                "indicacion": "-", "cause": "-", "cause2": "-"},
            async: false,
            success: function (data) {
                $('#cla_pro').val("");
                $("#des_pro").val("");
                $("#can_sol").val("");
                $("#unidades").val("");
                $("#cla_pro").focus();
                recargarDetalles();

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error", jqXHR, textStatus, errorThrown);
                swal({
                    title: "Captura de medicamento",
                    text: jqXHR.responseText,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-default",
                    confirmButtonText: "Continuar"
                });
            }
        });

    } else {
        alert("Capture primero el medicamento");
        $("#des_pro").focus();
    }
});

$(function () {
    $("#limpiarCB").click(limpiarCamposCB);

    $('#cb').focusout(function () {
        var cb = $('#cb').val();
        console.log("cb", cb);
        if (cb !== "") {
            buscarLotes(cb);
        }
    });

    $(function () {
        $("#medico_jq").keyup(function () {
            var nombre = $("#medico_jq").val();
            $("#medico_jq").autocomplete({
                source: "../AutoMedico?nombre=" + nombre,
                minLength: 1,
                select: function (event, ui) {
                    $("#medico_jq").val(ui.item.nom_med);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li>")
                        .data("ui-autocomplete-item", item)
                        .append("<a>" + item.nom_med + "</a>")
                        .appendTo(ul);
            };
        });
    });

    $('#bus_pac').tooltip();
    $('#bus_pacn').tooltip();

    $("#listServicios").change(function () {
        if (!validarEncabezado(2))
            return;
        $("#medico_jq").focus();
    });

    $("#medico_jq").click(function () {
        //console.log("cualquier");
        validarEncabezado(3);
    });

    var form = $('#formulario_receta');
    $.ajax({
        type: form.attr('method'),
        url: '../MuestraInsumosReceta',
        data: form.serialize(),
        success: function (data) {
            limpiaCampos();
            hacerTabla(data);
        },
        error: function () {
            alert("Ha ocurrido un error - insumo receta");
        }
    });

    function limpiaCampos() {
        $("#cla_pro").val("");
        $("#des_pro").val("");
        $("#ori1").attr("value", "");
        $("#ori2").attr("value", "");
        $("#existencias").attr("value", "");
        $("#indica").val("");
        //$("#causes").val("");
        $("#can_sol").val("");
    }

});


$(document).ready(function () {

    $("#btnDescartar").click(function ()
    {
        var e = confirm("Esta seguro de eliminar esta receta");
        if (e) {
            $("#btn_capturar2").val("1");
            isCanceled = true;
            cancelarReceta();
        }

    });

    $("#guardarReceta").click(finalizarReceta);

    $("#validarDetalle").click(surtirInsumo);

    $("#loteSelect").change(function () {
        var posicion = $("#loteSelect").val();
        if (posicion !== "-1") {
            asignarLote(listaLotes[posicion]);
        } else {
            $("#caducidadCB").val("");
            $("#claveCB").val("");
            $("#solicitadoCB").val("");
        }
    });

    $('#folio').click(function () {
        validarEncabezado(1);
    });

    $('#btn_eli').click(function () {
        var cla_pro = $('#btn_eli').val();
        alert(cla_pro);
    });

    function dameProducto(data) {
        var json = data;

        for (var i = 0; i < json.length; i++) {
            var cla_pro = json[i].cla_pro;
            var descripcion = json[i].des_pro;

            if (cla_pro === "null" || descripcion === "null") {
                swal({
                    title: "Buscar producto",
                    text: "Clave fuera del catálogo",
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-default",
                    confirmButtonText: "Continuar"
                });
                $("#cla_pro").val("");
                $("#des_pro").val("");
                $("#can_sol").val("0");
                $("#des_pro").focus();
                return;
            }

            $("#cla_pro").val(cla_pro);
            $("#unidades").focus();
            $("#des_pro").val(descripcion);
        }
    }

    $('#btn_descripcion').click(function () {
        var dir = '../ProductoDescripcion';

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: dir,
            data: {"des_pro": $('#des_pro').val()},
            success: function (data) {
                dameProducto(data);
            }
        });
    });

    $('#btn_clave').click(function () {
        var dir = '../ProductoClave';

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: dir,
            data: {"cla_pro": $('#cla_pro').val()},
            success: function (data) {

                dameProducto(data);
            },
            error: function () {
                alert("Ha ocurrido un error - clave");
            }
        });

    });

    $('#formulario_receta').submit(function () {
        //alert("Ingresó");
        return false;
    });

    $('#select_pac').change(function () {
        var select_pac = $('#select_pac').val();
        $('#nombre_jq').attr("value", "");
        //alert(sp_pac);
        var dir = '../RecetaNombre';
        var form = $('#formulario_receta');
        $.ajax({
            type: form.attr('method'),
            url: dir, data: form.serialize(),
            success: function (data) {
                devuelveFolio(data);
            },
            error: function () {
                alert("Ha ocurrido un error - paciente");
            }
        });
        function devuelveFolio(data) {
            var json = data;
            for (var i = 0; i < json.length; i++) {
                var fol_rec = json[i].fol_rec;
                var nom_com = json[i].nom_com;
                var sexo = json[i].sexo;
                var fec_nac = json[i].fec_nac;
                var num_afi = json[i].num_afi;
                var mensaje = json[i].mensaje;
                var carnet = json[i].carnet;
                alert(mensaje);
                if (mensaje === "vig_no_val") {
                    $("#nom_pac").val("");
                    $("#sexo").val("");
                    $("#fec_nac").val("");
                    $("#fol_sp").val("");
                    $("#nombre_jq").val("");
                    $("#nombre_jq").focus();
                    alert("Vigencia no Valida");
                }
                if (mensaje === "inexistente") {
                    alert("Paciente Inexistente");
                    $("#nom_pac").val("");
                    $("#sexo").val("");
                    $("#fec_nac").val("");
                    $("#fol_sp").val("");
                    $("#nombre_jq").val("");
                    $("#nombre_jq").focus();
                }
                if (mensaje === "ok") {
                    $("#folio").val(fol_rec);
                    $("#nom_pac").val(nom_com);
                    $("#sexo").val(sexo);
                    $("#fec_nac").val(fec_nac);
                    $("#fol_sp").val(num_afi);
                    $("#carnet").val(carnet);
                    $("#cla_pro").focus();
                }
            }
        }
    });

    $('#mostrar2').click(function () {
        var sp_pac = $('#medico_jq').val();
        var dir = '../RecetaNombreCol';
        $.ajax({
            dataType: 'json',
            type: "POST",
            url: dir,
            data: {"medico_jq": sp_pac},
            success: function (data) {
                var nom_med = data.nom_med;
                var cedula = data.cedula;
                $("#nom_med").val(nom_med);
                $("#ced_med").val(cedula);

                dir = '../recetacb';
                $.ajax({dataType: 'json',
                    type: "POST",
                    url: dir,
                    data: {"accion": ENCABEZADO, "tipo": COLECTIVO,
                        "folio": $('#folio').val(), "fecha": $('#fecha1').val(), "medico": cedula,
                        "servicio": $("#listServicios option:selected").val()},
                    success: function (data) {
                        console.log("Creo ", data);
                        idReceta = data.id;
                        $("#captura").show();
                        $("#tablaBotones").show();
                        $("#descartarReceta").show();
                        if ($('#folio').val() === "") {
                            $('#folio').val(data.id);
                            $('#lblFolio').text("Consecutivo");
                        }
                        $("#folio").prop('readonly', true);
                        $("#fecha1").prop('readonly', true);
                        $('#fecha1').prop('disabled', 'disabled');
                        $('#listServicios').prop('disabled', 'disabled');
                        $('#mostrar2').prop('disabled', 'disabled');
                        $("#medico_jq").prop('readonly', true);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Error", jqXHR, textStatus, errorThrown);
                        swal({
                            title: "Creación receta",
                            text: jqXHR.responseText,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-default",
                            confirmButtonText: "Continuar"
                        }, function () {
                            $('#folio').val("");
                            $('#ced_med').val("");
                            $("#nom_med").prop('readonly', false);
                            $("#medico_jq").prop('readonly', false);

                            $('#listServicios').val("0").trigger("change");
                            $('#nom_med').val("");
                            $('#nom_med').val("");
                            $('#medico_jq').val("");

                        });
                    }
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error", jqXHR, textStatus, errorThrown);
                $("#nom_med").val("");
                $("#ced_med").val("");
                $("#medico_jq").val("");
                swal({
                    title: "Buscar médico",
                    text: jqXHR.responseText,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-default",
                    confirmButtonText: "Continuar",
                    closeOnConfirm: false
                });
            }
        });
    });

    $("#validarBtn").click(function () {
        swal({
            title: "Captura de la receta",
            text: "Esta a punto de terminar la captura de la receta, una vez hecho esto no podra añadir más claves a la receta, ¿Esta seguro?.",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-warning",
            confirmButtonText: "Si, ¡Vamos a surtirla!",
            cancelButtonText: "¡No!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
        function (isConfirm) {
            var tipoCaptura = $("#tipCaptura").val();
            if (isConfirm) {
                dir = '../recetacb';
                $.ajax({
                    type: 'POST',
                    url: dir,
                    data: {"accion": VALIDAR, "tipo": COLECTIVO,
                        "id": idReceta},
                    success: function (data) {
                        if (tipoCaptura === "1") {
                            console.log("c_detalle", data);
                            $('#captura').hide();
                            $('#tablaMedicamento').hide();
                            $("#surtimiento").show();
                            var url = '../reportes/RecetaFarm.jsp?fol_rec=';
                            url = url.concat(idReceta, "&tipo=5&usuario=d");
                            swal({
                                title: "Captura de la receta",
                                text: data,
                                type: "success",
                                showCancelButton: true,
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Aceptar"
                            }, function () {
                                window.open(url, '', 'width=1200,height=800,left=50,top=50,toolbar=no');
                            });
                        } else
                        {
                            swal({
                                title: "Finalizar receta",
                                text: "El proceso de captura de la receta a sido completado. Folio consecutivo:" + idReceta + "",
                                type: "success",
                                showCancelButton: true,
                                confirmButtonClass: "btn-success",
                                cancelButtonClass: "btn-info",
                                confirmButtonText: "Carta",
                                cancelButtonText: "Ticket"
                            }, function (isConfirm) {
                                $("#btn_capturar2").val("1");
                                if (isConfirm) {
                                    var url = '../reportes/RecetaFarm.jsp?fol_rec=';
                                    url = url.concat(idReceta, "&tipo=2");
                                    window.open(url, '', 'width=1200,height=800,left=50,top=50,toolbar=no');
                                    window.location.reload();
                                } else {
                                    var url = '../reportes/RecetaFarm.jsp?fol_rec=';
                                    url = url.concat(idReceta, "&tipo=1");
                                    window.open(url, '', 'width=1200,height=800,left=50,top=50,toolbar=no');
                                    window.location.reload();
                                }
                            });

                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Error", jqXHR, textStatus, errorThrown);
                        //$("#myModal").modal('hide');
                        swal({
                            title: "Captura de la receta",
                            text: jqXHR.responseText,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-default",
                            confirmButtonText: "Continuar"
                        });
                    }
                });
            }
        });

    });

});


function hacerTabla(data) {
    var json = data;
    $("#tablaMedicamentos").empty();
    $("#tablaMedicamentos").append(
            $("<tr>")
            .append($("<td>").append("Clave"))
            .append($("<td>").append("Descripción"))
            .append($("<td>").append("Lote"))
            .append($("<td>").append("Caducidad"))
            .append($("<td>").append("Cant. Sol."))
            .append($("<td>").append("Cant. Sur."))
            .append($("<td>").append("Acción"))
            );
    for (var i = 0; i < json.length; i++) {
        var cla_pro = json[i].clave;
        var des_pro = json[i].descripcion;
        var lot_pro = json[i].lote;
        var cad_pro = json[i].caducidad;
        var can_sol = json[i].solicitado;
        var cant_sur = json[i].surtido;

        var btn_eliminar = "<button style='z-index:6' class='btn btn-danger btn-sm' name = 'btn_eli' onclick=\"cancelarDetalle('" + cla_pro + "')\"><span class='glyphicon glyphicon-remove' ></span></button>";
        $("#tablaMedicamentos").append(
                $("<tr>")
                .append($("<td>").append(cla_pro))
                .append($("<td>").append(des_pro))
                .append($("<td>").append(lot_pro))
                .append($("<td>").append(cad_pro))
                .append($("<td>").append(can_sol))
                .append($("<td>").append(cant_sur))
                .append($("<td>").append(btn_eliminar))
                );
    }
}

function recargarDetalles() {
    var dir = '../recetacb';
    $.ajax({
        dataType: 'json',
        type: "POST",
        url: dir,
        async: false,
        data: {"accion": RECARGAR_DETALLES, "tipo": COLECTIVO, "id": idReceta},
        success: function (data) {
            console.log("detalles ", data);
            //Verifica si muestra boton de validar receta.
            if (data.length > 0) {
                $("#tablaMedicamento").show();
            } else {
                $("#tablaMedicamento").hide();
            }
            hacerTabla(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Error", jqXHR, textStatus, errorThrown);
            swal({
                title: "Captura de medicamento",
                text: jqXHR.responseText,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-default",
                confirmButtonText: "Continuar",
                closeOnConfirm: false
            });
        }
    });
}

function cancelarDetalle(cla_pro) {

    swal({
        title: "Cancelación del producto",
        text: "Esta a punto de eliminar la clave: " + cla_pro + ", ¿Esta seguro?.",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-warning",
        confirmButtonText: "Si, ¡Elimina ese producto!",
        cancelButtonText: "¡No!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    },
    function (isConfirm) {
        if (isConfirm) {
            dir = '../recetacb';
            $.ajax({
                type: 'POST',
                url: dir,
                data: {"accion": CANCELAR_DETALLE, "tipo": COLECTIVO,
                    "id": idReceta, "clave": cla_pro},
                success: function (data) {
                    console.log("c_detalle", data);
                    $('#cla_pro').val("");
                    $("#des_pro").val("");
                    $("#can_sol").val("");
                    $("#cla_pro").focus();
                    recargarDetalles();
                    swal({
                        title: "Cancelación del producto",
                        text: data,
                        type: "success",
                        showCancelButton: true,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Aceptar"
                    });

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error", jqXHR, textStatus, errorThrown);
                    //$("#myModal").modal('hide');
                    swal({
                        title: "Cancelación del producto",
                        text: jqXHR.responseText,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-default",
                        confirmButtonText: "Continuar",
                        closeOnConfirm: false
                    });
                }
            });
        }
    });
}

var listaLotes;

function buscarLotes(cb) {
    var dir = '../recetacb';
    $.ajax({
        dataType: 'json',
        type: "POST",
        url: dir,
        async: false,
        data: {"accion": LOTE, "tipo": COLECTIVO, "id": idReceta, "cb": cb},
        success: function (data) {
            console.log("detalles ", data);
            listaLotes = data;
            limpiarCamposCB();
            $("#cb").val(cb);
            if (data.length > 1) {
                $("#loteTXT").hide();
                $("#loteSLC").show();
                $('#loteSelect').append(new Option('-- Lote a entregar --', -1));
                var aux;
                for (i = 0; i < data.length; i++) {
                    aux = data[i];
                    $('#loteSelect').append(new Option(aux.lote, i));
                }

            } else {
                $("#loteSLC").hide();
                $("#loteTXT").show();
                $("#lote").val(data[0].lote);
                $('#lote').prop('disabled', 'disabled');
                asignarLote(data[0]);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Error", jqXHR, textStatus, errorThrown);
            swal({
                title: "Busqueda insumo",
                text: jqXHR.responseText,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-default",
                confirmButtonText: "Continuar"
            }, function () {
                $("#cb").val("");
                $("#cb").focus();
            });
        }
    });

}

function limpiarCamposCB() {
    $('#loteSelect').empty();
    $("#lote").val('');
    $('#lote').prop('disabled', '');

    $("#loteSLC").hide();
    $("#loteTXT").show();

    $("#cb").val('');
    $("#claveCB").val('');
    $("#lote").val('');
    $("#caducidadCB").val('');
    $("#solicitadoCB").val('');
    $("#surtidoCB").val('');

}

function asignarLote(elegido) {

    if (elegido.solicitado === -1) {
        swal({
            title: "Validación producto",
            text: "El lote elegido no es correcto.",
            type: "error",
            showCancelButton: false,
            confirmButtonClass: "btn-default",
            confirmButtonText: "Continuar"
        }, function () {
            limpiarCamposCB();
        });
        return;
    }

    $("#caducidadCB").val(elegido.caducidad);
    $("#claveCB").val(elegido.clave);
    $("#solicitadoCB").val(elegido.solicitado);

}


function surtirInsumo() {
    var elegido = 0;
    var aux = $('#loteSelect').val();
    if (aux !== null) {
        elegido = parseInt(aux);
        if (elegido === CAMBIO) {
            swal({
                title: "Validación producto",
                text: "Debe elegir por lo menos un lote.",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-default",
                confirmButtonText: "Continuar"
            }, function () {
                $("#loteSelect").focus();
            });
            return;
        }
    }


    var surtido = $('#surtidoCB').val();
    if (surtido === "") {
        swal({
            title: "Validación producto",
            text: "Debe indicar la cantidad a surtir.",
            type: "error",
            showCancelButton: false,
            confirmButtonClass: "btn-default",
            confirmButtonText: "Continuar"
        }, function () {
            $("#surtidoCB").focus();
        });
        return;
    }
    surtido = parseInt(surtido);
    var producto = listaLotes[elegido].detalle_producto;

    var solicitado = listaLotes[elegido].solicitado;
    var accion = SURTIR;
    if (solicitado === CAMBIO) {
        accion = CAMBIAR;
        swal({
            title: "Cambio de lote",
            text: "El lote que intenta dispensar es distinto al indicado por el sistema. Puede realizar este cambio pero es necesaria la contraseña del supervisor.",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            inputPlaceholder: "Ingrese la contraseña de supervisor",
            showLoaderOnConfirm: true
        }, function (inputValue) {
            if (inputValue === false)
                return false;
            if (inputValue === "") {
                swal.showInputError("Contraseña no ingresa, favor ingresarla.");
                return false;
            }
            $.ajax({
                type: "POST",
                url: "../Farmacias",
                dataType: "json",
                data: {accion: "verificar", pass: inputValue},
                success: function (data) {
                    if (data.permitido) {
                        validarInsumo(accion, producto, surtido);
                    } else {
                        swal.showInputError("Contraseña errónea, por favor ingresar de nuevo.");
                        return false;
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error", jqXHR, textStatus, errorThrown);
                    swal({
                        title: "Cambio de lote",
                        text: jqXHR.responseText,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-default",
                        confirmButtonText: "Continuar"
                    });
                }
            }
            );
        });

        return;
    }


    validarInsumo(accion, producto, surtido);

}

function validarInsumo(accion, producto, surtido) {

    var dir = '../recetacb';
    $.ajax({
        type: "POST",
        url: dir,
        data: {"accion": accion, "tipo": COLECTIVO, "id": idReceta,
            "producto": producto, "surtido": surtido},
        success: function (data) {
            console.log("validacion completa");
            agregarInsumoValido();
            swal({title: "Validación producto", text: "Validación completa", type: "success", confirmButtonClass: "btn-success",
                confirmButtonText: "Ok"}, limpiarCamposCB);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Error", jqXHR, textStatus, errorThrown);
            swal({
                title: "Validación producto",
                text: jqXHR.responseText,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-default",
                confirmButtonText: "Continuar"
            });
        }
    });
}

var tablaSurtido = $("#tablaSurtido").DataTable();

function agregarInsumoValido() {

    var lote = $("#lote").val();
    if (lote === "") {
        lote = listaLotes[$("#loteSelect").val()].lote;
    }

    var surtido = parseInt($("#surtidoCB").val());
    var solicitado = parseInt($("#solicitadoCB").val());
    if (solicitado === CAMBIO) {
        solicitado = surtido;
    }


    tablaSurtido.row.add([
        $("#claveCB").val(),
        lote,
        $("#caducidadCB").val(),
        solicitado,
        surtido
    ]).draw(false);
}

function finalizarReceta() {

    swal({
        title: "Finalizar receta",
        text: "Esta a finalizar esta receta, ¿Esta seguro?.",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-warning",
        confirmButtonText: "Si, ¡He terminado!",
        cancelButtonText: "¡No!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    },
    function (isConfirm) {
        if (isConfirm) {
            dir = '../recetacb';
            $.ajax({
                type: 'POST',
                url: dir,
                data: {"accion": FINALIZAR, "tipo": COLECTIVO,
                    "id": idReceta},
                success: function (data) {
                    swal({
                        title: "Finalizar receta",
                        text: "El proceso de captura de la receta a sido completado.",
                        type: "success",
                        showCancelButton: true,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Aceptar"
                    }, function () {
                        $("#btn_capturar2").val("1");
                        var url = '../reportes/RecetaFarm.jsp?fol_rec=';
                        url = url.concat(idReceta, "&tipo=2");
                        window.open(url, '', 'width=1200,height=800,left=50,top=50,toolbar=no');
                        window.location.reload();
                    });

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error", jqXHR, textStatus, errorThrown);
                    swal({
                        title: "Cancelación del producto",
                        text: jqXHR.responseText,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-default",
                        confirmButtonText: "Continuar"
                    });
                }
            });
        }
    });
}