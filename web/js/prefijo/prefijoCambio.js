$(function () {

    $.ajax({
        url: context + "/Operaciones",
        data: {ban: 5},
        type: 'POST',
        async: true,
        success: function (data)
        {
            $("#preOri").val(data.pre);

        }, error: function (jqXHR, textStatus, errorThrown) {

            alert("Error Contactar al departamento de sistemas");

        }
    });

    $("#btnCancel").click(function () {
        window.location = '../../main_menu.jsp';
    });

    $("#btnSave").click(function () {
        var pre = $("#preNew").val();
        if (pre === "")
        {
            alert("Ingresar un prefijo válido porfavor.");
            return false;
        }
        else
        {
            $.ajax({
                url: context + "/Operaciones",
                data: {ban: 6, per: pre},
                type: 'POST',
                async: true,
                success: function (data)
                {
                    var msj = data.msj;
                    alert(msj);
                    location.reload();

                }, error: function (jqXHR, textStatus, errorThrown) {

                    alert("Error Contactar al departamento de sistemas");

                }
            });
        }


    });

});

function upperCase(x)
{
    var y = document.getElementById(x).value;
    document.getElementById(x).value = y.toUpperCase();
    document.getElementById("mySpan").value = y.toUpperCase();

}