$(function ()
{
    $('#fecIni').mask("99-99-9999", {placeholder: " "});
    $("#fecIni").datepicker({
        dateFormat: 'dd-mm-yy',
        changeYear: true,
        changeMonth: true,
        maxDate: 'today',
        yearRange: "1910:2020"
    });

    $('#fecFin').mask("99-99-9999", {placeholder: " "});
    $("#fecFin").datepicker({
        dateFormat: 'dd-mm-yy',
        changeYear: true,
        changeMonth: true,
        minDate: 'today',
        yearRange: "2015:2050"
    });
    $("#afiliados").hide();
    var numeroAfiliacion = $("#no_afi1  ").val();

    $.ajax({
        url: context + "/Operaciones",
        data: {ban: 7, na: numeroAfiliacion},
        type: 'POST',
        async: false,
        success: function (data)
        {
            var nombre = JSON.stringify(data);

            if (nombre === "{}")
            {
                $("#afiliados").hide();
            }
            else
            {
                $("#afiliados").show();
                var $select = $("#slctPacientes");
                $select.find('option').remove();
                $.each(data, function (key, value) {
                    $('<option>').val(key).text(value).appendTo($select);

                });

                $.ajax({
                    url: context + "/Operaciones",
                    data: {ban: 8, na: numeroAfiliacion},
                    type: 'POST',
                    async: false,
                    success: function (data)
                    {
                        var fi = data.fi;
                        var ff = data.ff;
                        $("#fecIni").datepicker("option", "disabled", true);
                        $("#fecIni").val(fi);
                        $("#fecFin").datepicker("option", "disabled", true);
                        $("#fecFin").val(ff);

                    }, error: function (jqXHR, textStatus, errorThrown) {

                        alert("Error Contactar al departamento de sistemas");

                    }
                });

            }


        }, error: function (jqXHR, textStatus, errorThrown) {

            alert("Error Contactar al departamento de sistemas");

        }
    });

});



function aver(data)
{
    var json = JSON.parse(data);
    var aDataSet = [];

    for (var i = 0; i < json.length; i++)
    {
        var clave = json[i].nC;
        aDataSet.push([clave]);

    }
    $.each(aDataSet, function (index, contenido) {
        $("#afiliados").append(
                $("<div>").append($("<h4 style='text-decoration: underline;'>").append(contenido))


                );
    });

}


