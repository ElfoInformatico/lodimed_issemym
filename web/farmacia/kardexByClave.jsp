<%-- 
    Document   : kardexByClave1
    Created on : 12/10/2016, 11:50:23 AM
    Author     : manuel
--%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%

    HttpSession sesion = request.getSession();
    String id_usu = "";
    String accion = "";
    accion = request.getParameter("accion");
    if (accion == null) {
        accion = "";
    }
    try {
        id_usu = (String) sesion.getAttribute("id_usu");
    } catch (Exception e) {
        Logger.getLogger("reporteAbasto.jsp").log(Level.SEVERE, e.getMessage(), e);
    }

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../css/topPadding.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/sweetalert.css" rel="stylesheet" type="text/css"/>

        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/jquery-ui.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/dataTables.bootstrap.js"></script>
        <script src="../js/sweetalert.min.js" type="text/javascript"></script>
        <script src="../js/kardex/kardexView.js"></script>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <title>Kardex</title>
    </head>
    <body>
        <%@include file="../jspf/mainMenu.jspf" %>
        <div class="container" style="padding-top: 50px">
            <h1 style="color: black" >Kardex</h1>
            <hr/>
            <div class="row">

                <div class="col-sm-2">
                    <input class="form-control" placeholder="Código de Barras" id="cb" name="cb" />
                </div>
                <div class="col-sm-2">
                    <input class="form-control" placeholder="Clave" id="claveKardex" name="cla_pro" />
                </div>
                <div class="col-sm-1">
                    <button class="btn btn-success btn-block" id="searchClave" name="searchClave" type="button" ><span class="glyphicon glyphicon-search"></span></button>
                </div>

            </div>  
            <hr/>
            <div class="row" style="margin-left: 10px">
                <h4 class="col-sm-12">
                    <input class="hidden" placeholder="Clave" name="claveConsulta" id="claveConsulta" />
                    <h4 id="claveResultado"></h4>
                </h4> 

                <h4 class="col-sm-12">
                    <h4 id="DescripcionResultado"></h4>
                </h4> 
            </div>
            <hr/>
        </div>
        <div style="width: 90%; margin: auto" id="muestraKardex">
            <h4 id="existenciasActuales" ></h4>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row" >
                        <h5 class="col-lg-1 col-md-1 col-sm-1">kardex</h5>
                        <div class="col-lg-9 col-md-9 col-sm-9"></div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <button class="btn btn-danger btn-block" type="button" id="reloadButton" ><span class="glyphicon glyphicon-refresh" ></span></button>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div id="container">

                        <div id="dynamic"></div>

                    </div>                    
                </div>
        </div>
             <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"></h4>
                        </div>
                        <div class="modal-body">
                            <div class="text-center" id="imagenCarga">
                                <img src="../imagenes/ajax-loader-1.gif" alt="" />
                            </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
    </body>
</html>
