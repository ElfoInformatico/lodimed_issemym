<%-- 
    Document   : index
    Created on : 07-mar-2014, 15:38:43
    Author     : GNK
--%>

<%@page import="Impl.RecetaImpl"%>
<%@page import="Servlets.recetas.RecetaPorCB"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%    ConectionDB con = new ConectionDB();
    HttpSession sesion = request.getSession();
    String id_usu = "";
    request.setCharacterEncoding("UTF-8");

    id_usu = (String) sesion.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }

    String fol_rec = "", nom_pac = "", fec_sur = "", fec_sur2 = "", id_rec = "";

    id_rec = request.getParameter("id_rec");
    fol_rec = request.getParameter("fol_rec");
    nom_pac = request.getParameter("nom_pac");
    fec_sur = request.getParameter("fec_sur");
    fec_sur2 = request.getParameter("fec_sur2");

    if (fol_rec == null) {
        fol_rec = "";
    }
    if (nom_pac == null) {
        nom_pac = "";
    }
    if (fec_sur == null) {
        fec_sur = "";
    }
    if (fec_sur2 == null) {
        fec_sur2 = "";
    }
    if (id_rec == null) {
        id_rec = "";
    }

    if (id_rec == null) {
        id_rec = "";
    }
    if (!id_rec.equals("")) {
        id_rec = " id_rec = '" + id_rec + "' and ";
    }
    if (!fol_rec.equals("")) {
        fol_rec = " fol_rec like '%" + fol_rec + "%' and ";
    }
    if (!nom_pac.equals("")) {
        nom_pac = " nom_com like '%" + nom_pac + "%' and ";
    }
    if (!fec_sur.equals("")) {
        fec_sur = " DATE(fecha_hora) between '" + fec_sur + "' and '" + fec_sur2 + "' and ";
    }
%>
<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); %>
<%java.text.DateFormat df1 = new java.text.SimpleDateFormat("HH:mm:ss"); %>
<%java.text.DateFormat df3 = new java.text.SimpleDateFormat("dd/MM/yyyy");%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="../css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../css/pie-pagina.css" rel="stylesheet" media="screen">
        <link href="../css/topPadding.css" rel="stylesheet">
        <link href="../css/cupertino/jquery-ui-1.10.3.custom.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/sweetalert.css" rel="stylesheet" type="text/css"/>
        <link href="../css/dataTables.jqueryui.css" rel="stylesheet" media="screen">
        <link href="../css/jquery-ui.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <title>Sistema de Captura de Receta</title>
    </head>
    <body>
        <%@include file="../jspf/mainMenu.jspf"%>
        <br />
        <div class="container-fluid">
            <div style="width: 90%;margin: auto;">
                <a class="btn btn-default active btn-sm">Recetas Pendientes</a>
                <a class="btn btn-default btn-sm" href="modRecetasSurtidas.jsp">Recetas Surtidas</a>
                <a class="btn btn-default btn-sm" href="modRecetasCanceladas.jsp">Recetas Canceladas</a>
                <h3>Recetas Pendientes por Surtir</h3>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                Búsqueda de Folios
                            </div>
                            <div class="panel-body">
                                <h5><b>Recuerde:</b> Solo se listan 50 recetas, si no aparece, búsquela con las siguientes opciones.</h5>
                                <hr>
                                <form method="post">
                                    Consecutivo:
                                    <input type="text" class="form-control" name="id_rec" />
                                    Por Folio:
                                    <input type="text" class="form-control" name="fol_rec" id="fol_rec" />
                                    Por Nombre de Derechohabiente:
                                    <input type="text" onclick="return tabular(event, this);" class="form-control" name="nom_pac" id="nom_pac" placeholder="Derechohabiente" autofocus/>
                                    Por Fecha:
                                    <input type="date" class="form-control" name="fec_sur" id="fec_sur" />
                                    Fin:
                                    <input type="date" class="form-control" name="fec_sur2" id="fec_sur2" />
                                    <button class="btn btn-success btn-block" type="submit">Buscar</button>
                                    <button class="btn btn-default btn-block" type="submit">Todas</button>
                                    <button class="btn btn-danger btn-block" type="submit">Actualizar</button>
                                </form>
                            </div>
                        </div>
                        <div class="panel panel-success">
                            <div class="panel-body">
                                <%
                                    int numRecetas = 0, numHL7 = 0, numManual = 0, numPend = 0, sumSol = 0, sumSur = 0, rsurtido;
                                    String Fecha = "";
                                    try {
                                        con.conectar();
                                        String qry = "SELECT COUNT(rs.receta) AS recetas, SUM(rs.solicitado) AS solicitado, SUM(rs.surtido) AS surtido,COUNT(HL7.fol_rec) AS HL7,  COUNT(rs.receta) - COUNT(HL7.fol_rec) AS Manual,COUNT(PEND.receta) AS pendiente,DATE_FORMAT(fecha_hora,'%d/%m/%Y') AS FECHA,COUNT(rs.receta)-COUNT(PEND.receta) AS rsurtido FROM ( SELECT r.id_rec AS receta,fol_rec, Sum(dr.can_sol) AS solicitado, Sum(dr.cant_sur) AS surtido,fecha_hora FROM receta AS r INNER JOIN detreceta AS dr  ON dr.id_rec = r.id_rec WHERE r.transito IN (0, 2) AND r.baja IN (0, 2) AND DATE_FORMAT(fecha_hora,'%Y-%m-%d')=CURDATE() GROUP BY r.id_rec ) AS rs  LEFT JOIN (SELECT fol_rec FROM receta WHERE fol_rec LIKE '%HL7%' AND DATE_FORMAT(fecha_hora,'%Y-%m-%d')=CURDATE()) AS HL7 ON rs.fol_rec=HL7.fol_rec LEFT JOIN (SELECT r.fol_rec AS receta FROM receta AS r INNER JOIN detreceta AS dr ON dr.id_rec = r.id_rec WHERE r.transito IN (0, 2) AND r.baja IN (0, 2) AND dr.can_sol <> dr.cant_sur AND DATE_FORMAT(fecha_hora,'%Y-%m-%d')=CURDATE() GROUP BY r.fol_rec) AS PEND ON rs.fol_rec=PEND.receta ;";
                                        ResultSet rset = con.consulta(qry);
                                        rset.next();
                                        numRecetas = rset.getInt(1);
                                        sumSol = rset.getInt(2);
                                        sumSur = rset.getInt(3);
                                        numHL7 = rset.getInt(4);
                                        numManual = rset.getInt(5);
                                        numPend = rset.getInt(6);
                                        Fecha = rset.getString(7);
                                        rsurtido = rset.getInt(8);
                                        rset.close();

                                %>
                                <div class="row"><strong class="col-sm-12">Fecha: <%=Fecha%></strong></div>
                                <div class="row"><strong class="col-sm-12">Recetas Elaboradas: <%=numRecetas%></strong></div>
                                <div class="row"><strong class="col-sm-12">Recetas HL7: <%=numHL7%>&nbsp;&nbsp;&nbsp;<button class="btn btn-danger glyphicon glyphicon-eye-open" id="ModalHl7" type="button" ></button></strong></div>
                                <div class="row"><strong class="col-sm-12">Recetas Manuales: <%=numManual%> &nbsp;&nbsp;&nbsp;<button class="btn btn-danger glyphicon glyphicon-eye-open" id="ModalManual" type="button" ></button></strong></div>
                                <div class="row"><strong class="col-sm-12">Recetas Sutidas: <%=rsurtido%></strong></div>
                                <div class="row"><strong class="col-sm-12">Recetas Pendientes: <%=numPend%></strong></div>                                  
                                <div class="row"><strong class="col-sm-12">Solicitado/Surtido: <%=sumSol%> / <%=sumSur%></strong></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                Folios
                            </div>
                            <div class="panel-body">
                                <%
                                    String FolioRec;
                                    rset = con.consulta("SELECT fol_rec, nom_com, fecha_hora, id_rec, medico, id_tip,SUM(can_sol) AS can_sol1,SUM(cant_sur) AS cant_sur1 from recetas where can_sol!=cant_sur and id_usu like '%%' and " + id_rec + " " + fec_sur + " " + fol_rec + " " + nom_pac + " transito IN (0,2) and baja IN (0,2) GROUP BY fol_rec,fol_rec, nom_com, fecha_hora, id_rec, medico, id_tip HAVING can_sol1!=cant_sur1 order by fecha_hora DESC limit 0,50;");
                                    while (rset.next()) {

                                        ResultSet rset3 = con.consulta("select notificahl7 from receta where id_rec =" + rset.getString(4));

                                        String webreceta = "";

                                        while (rset3.next()) {
                                            webreceta = rset3.getString(1);
                                        }
                                        String foldet1 = rset.getString(1);
                                        FolioRec = rset.getString(1);
                                        if (FolioRec.contains("HL7")) {
                                            FolioRec = FolioRec.substring(3);
                                        }
                                %>
                                <form action="../recetacb" name="form_<%=rset.getString(4)%>" id="form_<%=rset.getString(4)%>">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <h4>No. Rec <%=rset.getString("id_rec")%> || Folio: <%=FolioRec%> ||  <%=rset.getString("medico")%> </h4>
                                                </div>
                                                <%
                                                    if (webreceta.equals("1") && foldet1.contains("HL7")) {
                                                %>
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <button class="btn btn-block btn-success" type="button"  onclick="surtir('form_<%=rset.getString(4)%>')">Surtir</button>
                                                </div>                                                
                                                <%} else if (!(foldet1.contains("HL7"))) {%>
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <button class="btn btn-block btn-success" type="button"  onclick="surtir('form_<%=rset.getString(4)%>')">Surtir</button>
                                                </div>    
                                                <%}%>
                                                <div class="col-sm-2">
                                                    <button class="btn btn-block btn-default" type="button" name="imprimir" id="imprimir" value="imprimir" onclick="window.open('../reportes/RecetaFarm.jsp?fol_rec=<%=rset.getString(4)%>&tipo=<%=rset.getString("id_tip")%>&pag=0', '', 'width=1200,height=800,left=50,top=50,toolbar=no');">Imprimir</button> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <input class="hidden" name="id" value="<%=rset.getInt(4)%>" />
                                            <input class="hidden" name="folioreceta" value="<%=rset.getString(1)%>" />
                                            <div class="col-lg-2">
                                                <b>Paciente:</b><br /> <%=rset.getString(2)%> <br>
                                                <b>Fecha y hora:</b><br /><%=df3.format(df2.parse(rset.getString(3)))%>
                                                <br /><%=df1.format(df2.parse(rset.getString(3)))%>
                                                <%

                                                    if (foldet1.contains("HL7") && webreceta.equals("0")) {
                                                %>
                                                <div class="row">
                                                    <button class="btn btn-block btn-default" type="submit" name="accion" value="hl7notif" onclick="return confirm('Seguro que desea notificar SGM?')">Notificar SGM</button> 
                                                </div>
                                                <%
                                                    }
                                                %>
                                            </div>
                                            <div class="col-lg-10">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <td>Clave:</td>
                                                        <td>Sol:</td>
                                                        <td>Sur:</td>
                                                        <td>Pend:</td>
                                                        <td>Lote/Caducidad</td>

                                                    </tr>
                                                    <%                                                        int sol = 0, sur = 0, dife = 0;
                                                        ResultSet rset2 = con.consulta("select cla_pro, des_pro, can_sol, cant_sur, fol_det, lote,DATE_FORMAT(caducidad,'%d/%m/%Y') AS caducidad, fol_det, det_pro AS detalle from recetas where id_rec = '" + rset.getString(4) + "' and can_sol!=cant_sur ");
                                                        while (rset2.next()) {
                                                            sol = Integer.parseInt(rset2.getString(3));
                                                            sur = Integer.parseInt(rset2.getString(4));
                                                            dife = sol - sur;
                                                    %>
                                                    <tr title="<%=rset2.getString(2)%>">

                                                        <td><input type="text" class="form-control" value="<%=rset2.getString(1)%>"  readonly /></td>                                                        
                                                        <td><input type="text" class="form-control" value="<%=rset2.getString(3)%>" readonly /></td>
                                                        <td><input type="text" class="form-control" value="<%=sur%>" readonly  /></td>
                                                        <td><input type="number" class="form-control" value="<%=dife%>" name="surtido" min="0" data-behavior="only-num" /></td>
                                                        <td>Lote:-<br />Cadu:&nbsp;-</td>

                                                        <td class="hidden"><input  name="producto" value="<%=rset2.getInt("detalle")%>" /></td>
                                                        <td class="hidden"><input  name="tipo" value="<%=RecetaImpl.MEDICO_CAPTURA%>" /></td>
                                                    </tr>
                                                    <%
                                                        }
                                                    %>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <%
                                        }
                                    } catch (SQLException ex) {
                                        Logger.getLogger("modSurteFarmaciaP.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                    } finally {
                                        try {
                                            if (con.estaConectada()) {
                                                con.cierraConexion();
                                            }
                                        } catch (SQLException ex) {
                                            Logger.getLogger("modSurteFarmaciaP.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                        }
                                    }
                                %>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="container">
                <button type="button" class="btn hidden " id="btnModal" data-toggle="modal" data-target="#myModal"></button>
                <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel">
                    <div class="modal-dialog">
                        <form action="RecolectaFarmacia">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            Recetas HL7
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <table class="cell-border table table-striped table-bordered" cellspacing="0"  id="example">
                                            <thead>
                                                <tr>
                                                    <td>Folio</td>
                                                    <td>Médico</td>
                                                    <td>Paciente</td>
                                                    <td>Solicitado</td>
                                                    <td>Surtido</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%
                                                    try {
                                                        con.conectar();
                                                        ResultSet rset2 = con.consulta("SELECT fol_rec,M.nom_com,P.nom_com AS nompac,D.can_sol,D.cant_sur FROM receta R INNER JOIN medicos M ON R.cedula=M.cedula  INNER JOIN pacientes P ON R.id_pac=P.id_pac INNER JOIN (SELECT id_rec,SUM(can_sol) AS can_sol,SUM(cant_sur) AS cant_sur FROM detreceta GROUP BY id_rec) AS D  ON R.id_rec=D.id_rec WHERE fol_rec LIKE '%HL7%' AND DATE_FORMAT(fecha_hora,'%Y-%m-%d')=CURDATE()");
                                                        while (rset2.next()) {
                                                %>
                                                <tr>
                                                    <td><%=rset2.getString(1)%></td>
                                                    <td><%=rset2.getString(2)%></td>
                                                    <td><%=rset2.getString(3)%></td>
                                                    <td><%=rset2.getString(4)%></td>
                                                    <td><%=rset2.getString(5)%></td>
                                                </tr>
                                                <%
                                                        }
                                                    } catch (SQLException ex) {
                                                        Logger.getLogger("modSurteFarmaciaP.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                                    } finally {
                                                        try {
                                                            if (con.estaConectada()) {
                                                                con.cierraConexion();
                                                            }
                                                        } catch (SQLException ex) {
                                                            Logger.getLogger("modSurteFarmaciaP.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                                        }
                                                    }
                                                %>
                                            </tbody>
                                        </table>                                    
                                    </div>
                                    <div class="modal-footer">                                        
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <button type="button" class="btn hidden " id="btnModalManual" data-toggle="modal" data-target="#myModalManual"></button>
                <div class="modal fade bs-example-modal-lg" id="myModalManual" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel">
                    <div class="modal-dialog">
                        <form action="RecolectaFarmacia">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            Recetas Manuales
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <table class="cell-border table table-striped table-bordered" cellspacing="0"  id="examplemanual">
                                            <thead>
                                                <tr>
                                                    <td>Folio</td>
                                                    <td>Médico</td>
                                                    <td>Paciente</td>
                                                    <td>Solicitado</td>
                                                    <td>Surtido</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%
                                                    try {
                                                        con.conectar();
                                                        ResultSet rset2 = con.consulta("SELECT fol_rec,M.nom_com,P.nom_com AS nompac,D.can_sol,D.cant_sur FROM receta R INNER JOIN medicos M ON R.cedula=M.cedula  INNER JOIN pacientes P ON R.id_pac=P.id_pac INNER JOIN (SELECT id_rec,SUM(can_sol) AS can_sol,SUM(cant_sur) AS cant_sur FROM detreceta GROUP BY id_rec) AS D  ON R.id_rec=D.id_rec WHERE fol_rec NOT LIKE '%HL7%' AND DATE_FORMAT(fecha_hora,'%Y-%m-%d')=CURDATE()");
                                                        while (rset2.next()) {
                                                %>
                                                <tr>
                                                    <td><%=rset2.getString(1)%></td>
                                                    <td><%=rset2.getString(2)%></td>
                                                    <td><%=rset2.getString(3)%></td>
                                                    <td><%=rset2.getString(4)%></td>
                                                    <td><%=rset2.getString(5)%></td>
                                                </tr>
                                                <%
                                                        }
                                                    } catch (SQLException ex) {
                                                        Logger.getLogger("modSurteFarmaciaP.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                                    } finally {
                                                        try {
                                                            if (con.estaConectada()) {
                                                                con.cierraConexion();
                                                            }
                                                        } catch (SQLException ex) {
                                                            Logger.getLogger("modSurteFarmaciaP.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                                        }
                                                    }
                                                %>
                                            </tbody>
                                        </table>                                    
                                    </div>
                                    <div class="modal-footer">                                        
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- 
            ================================================== -->
            <!-- Se coloca al final del documento para que cargue mas rapido -->
            <!-- Se debe de seguir ese orden al momento de llamar los JS -->
            <script src="../js/jquery-2.1.4.js"></script>
            <script src="../js/bootstrap.js"></script>
            <script src="../js/jquery-ui.js"></script>
            <script src="../js/jquery.alphanum.js" type="text/javascript"></script>
            <script src="../js/sweetalert.min.js" type="text/javascript"></script>
            <script src="../js/jquery.dataTables.js"></script>
            <script src="../js/dataTables.jqueryui.js"></script>
            <script type="text/javascript">


                                                        var productos;
                                                        var total_sin_surtir;
                                                        var id_receta;
                                                        var folioreceta;
                                                        function surtir(form)
                                                        {
                                                            var form = $('#' + form);
                                                            var values = {};
                                                            productos = [];
                                                            //Obtiene todos los productos a surtir.
                                                            $.each(form.serializeArray(), function (i, field) {
                                                                if (field.name === "id") {
                                                                    id_receta = field.value;
                                                                }

                                                                if (field.name === "folioreceta") {
                                                                    folioreceta = field.value;
                                                                }

                                                                values[field.name] = field.value;
                                                                if (field.name === "tipo") {
                                                                    values["accion"] = "<%=RecetaPorCB.SURTIR_LOTE_ACCION%>";
                                                                    productos.push(values);
                                                                    values = {};
                                                                    values["id"] = id_receta;
                                                                }
                                                            });
                                                            console.log(productos);
                                                            total_sin_surtir = productos.length;
                                                            swal({
                                                                title: "Surtido de receta",
                                                                text: "Esta a punto surtir la receta con los cantidades indicadas, ¿Esta seguro?.",
                                                                type: "warning",
                                                                showCancelButton: true,
                                                                confirmButtonClass: "btn-warning",
                                                                confirmButtonText: "Si, Surtirla",
                                                                cancelButtonText: "¡No!",
                                                                closeOnConfirm: false,
                                                                showLoaderOnConfirm: true
                                                            },
                                                                    function (isConfirm) {

                                                                        if (isConfirm) {
                                                                            surtir_siguiente();
                                                                        }
                                                                    });
                                                        }

                                                        function surtir_siguiente() {
                                                            dir = '../recetacb';
                                                            if (productos.length > 0) {

                                                                $.ajax({
                                                                    type: "POST",
                                                                    url: dir,
                                                                    data: productos[0],
                                                                    dataType: 'json',
                                                                    success: function (data) {
                                                                        if (data.surtido == productos[0].surtido) {
                                                                            total_sin_surtir--;
                                                                        }

                                                                        productos.shift();
                                                                        surtir_siguiente();
                                                                    },
                                                                    error: function (jqXHR, textStatus, errorThrown) {
                                                                        console.log("Error", jqXHR, textStatus, errorThrown);
                                                                        productos.shift();
                                                                        surtir_siguiente();
                                                                    }
                                                                });
                                                                return;
                                                            }

                                                            $.ajax({
                                                                type: "POST",
                                                                url: dir,
                                                                data: {"id": id_receta, "accion": "<%=RecetaPorCB.FINALIZAR_RECETA_ACCION%>",
                                                                    "tipo": "<%=RecetaImpl.MEDICO_CAPTURA%>"},
                                                                success: function () {
                                                                    var completo = true;
                                                                    if (total_sin_surtir > 0) {
                                                                        completo = false;
                                                                    }

                                                                    var Verifica = folioreceta.includes("HL7");

                                                                    if (Verifica === true) {
                                                                        Mensaje_hl7(completo);
                                                                    } else {
                                                                        confirmacion(completo);
                                                                    }
                                                                },
                                                                error: function (jqXHR, textStatus, errorThrown) {
                                                                    console.log("Error", jqXHR, textStatus, errorThrown);
                                                                    swal({
                                                                        title: "Surtido de receta",
                                                                        text: jqXHR.responseText,
                                                                        type: "error",
                                                                        showCancelButton: false,
                                                                        confirmButtonClass: "btn-default",
                                                                        confirmButtonText: "Continuar"
                                                                    }, function () {
                                                                        location.reload();
                                                                    });
                                                                }
                                                            });

                                                        }

                                                        function Mensaje_hl7(surtido_completo) {
                                                            $.ajax({
                                                                type: "POST",
                                                                url: dir,
                                                                data: {"id": id_receta, "accion": "<%=RecetaPorCB.NOTIFICAR_SURTIDO_HL7%>",
                                                                    "tipo": "<%=RecetaImpl.MEDICO_CAPTURA%>"},
                                                                success: function (data) {
                                                                    confirmacion(surtido_completo);
                                                                },
                                                                error: function (jqXHR, textStatus, errorThrown) {
                                                                    var msg2 = "Quedaron productos sin surtir por favor, verificar las cantidades y existencias.";
                                                                    if (surtido_completo) {
                                                                        msg2 = "El surtido sea dado completamente.";
                                                                    }
                                                                    console.log("Error", jqXHR, textStatus, errorThrown);
                                                                    var url = '../reportes/RecetaFarm.jsp?fol_rec=';
                                                                    url = url.concat(id_receta, "&tipo=3&&pag=0");
                                                                    swal({
                                                                        title: "<span class='title'>Surtido de receta</span>",
                                                                        text: "<span class='text'><b>Mensaje 1</b>: " + msg2 +
                                                                                "<br><b>Mensaje 2</b>: No se puedo enviar mensaje de surtido por HL7 a SGM.",
                                                                        type: "error",
                                                                        html: true,
                                                                        showCancelButton: false,
                                                                        confirmButtonClass: "btn-default",
                                                                        confirmButtonText: "Continuar"
                                                                    }, function () {
                                                                        window.open(url, '', 'width=1200,height=800,left=50,top=50,toolbar=no');
                                                                        location.reload();
                                                                    });
                                                                }
                                                            });
                                                        }

                                                        function confirmacion(surtio_completo) {
                                                            if (surtio_completo) {
                                                                swal({
                                                                    title: "Surtido de receta",
                                                                    text: "El surtido sea dado completamente.",
                                                                    type: "success",
                                                                    showCancelButton: false,
                                                                    confirmButtonClass: "btn-success",
                                                                    confirmButtonText: "Aceptar"
                                                                }, function () {
                                                                    location.reload();
                                                                });
                                                                return;
                                                            }
                                                            swal({
                                                                title: "Surtido de receta",
                                                                text: "Quedaron productos sin surtir por favor, verificar las cantidades y existencias.",
                                                                type: "error",
                                                                showCancelButton: false,
                                                                confirmButtonClass: "btn-default",
                                                                confirmButtonText: "Continuar"
                                                            }, function () {
                                                                window.open(url, '', 'width=1200,height=800,left=50,top=50,toolbar=no');
                                                                location.reload();
                                                            });

                                                        }



                                                        $(document).ready(function () {
                                                            $("[data-behavior~=only-num]").numeric({
                                                                allowMinus: false,
                                                                allowThouSep: false
                                                            });
                                                            $("#nom_pac").keyup(function () {
                                                                var nombre2 = $("#nom_pac").val();
                                                                $("#nom_pac").autocomplete({
                                                                    source: "../AutoPacientes?nombre=" + nombre2,
                                                                    minLength: 2,
                                                                    select: function (event, ui) {
                                                                        $("#nom_pac").val(ui.item.nom_com);
                                                                        return false;
                                                                    }
                                                                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                                                                    return $("<li>")
                                                                            .data("ui-autocomplete-item", item)
                                                                            .append("<a>" + item.nom_com + "</a>")
                                                                            .appendTo(ul);
                                                                };
                                                            });
                                                        });
                                                        $(document).ready(function () {
                                                            $("[data-behavior~=only-num]").numeric({
                                                                allowMinus: false,
                                                                allowThouSep: false
                                                            });
                                                            $("#fol_rec").keyup(function () {
                                                                var fol_rec2 = $("#fol_rec").val();
                                                                $("#fol_rec").autocomplete({
                                                                    source: "../AutoFolio?folio=" + fol_rec2 + "&Tipo=Pendientes",
                                                                    minLength: 2,
                                                                    select: function (event, ui) {
                                                                        $("#fol_rec").val(ui.item.fol_rec);
                                                                        return false;
                                                                    }
                                                                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                                                                    return $("<li>")
                                                                            .data("ui-autocomplete-item", item)
                                                                            .append("<a>" + item.fol_rec + "</a>")
                                                                            .appendTo(ul);
                                                                };
                                                            });

                                                            $.ajax({
                                                                type: "POST",
                                                                url: '../recetacb',
                                                                data: {"accion": "<%=RecetaPorCB.ACTUALIZAR_CADUCAS_ACCTION%>",
                                                                    "tipo": "<%=RecetaImpl.MEDICO_CAPTURA%>"},
                                                                error: function (jqXHR, textStatus, errorThrown) {
                                                                    console.log("Error", jqXHR, textStatus, errorThrown);
                                                                    swal({
                                                                        title: "Surtido de receta",
                                                                        text: "No se puedo actualizar las recetas caducas.",
                                                                        type: "error",
                                                                        showCancelButton: false,
                                                                        confirmButtonClass: "btn-default",
                                                                        confirmButtonText: "Continuar"
                                                                    });
                                                                }
                                                            });
                                                        });

                                                        function tabular(e, obj) {
                                                            tecla = (document.all) ? e.keyCode : e.which;
                                                            if (tecla !== 13)
                                                                return;
                                                            frm = obj.form;
                                                            for (i = 0; i < frm.elements.length; i++)
                                                                if (frm.elements[i] === obj)
                                                                {
                                                                    if (i === frm.elements.length - 1)
                                                                        i = -1;
                                                                    break
                                                                }
                                                            /*ACA ESTA EL CAMBIO*/
                                                            if (frm.elements[i + 1].disabled === true)
                                                                tabular(e, frm.elements[i + 1]);
                                                            else
                                                                frm.elements[i + 1].focus();
                                                            return false;
                                                        }

                                                        $(document).ready(function () {


                                                            $("#ModalHl7").click(function () {
                                                                $("#btnModal").click();
                                                                $('#example').dataTable();
                                                            });

                                                            $("#ModalManual").click(function () {
                                                                $("#btnModalManual").click();
                                                                $('#examplemanual').dataTable();
                                                            });
                                                        });
            </script>

    </body>
</html>

