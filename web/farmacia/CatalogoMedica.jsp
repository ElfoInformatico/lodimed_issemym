<%-- 
    Document   : index
    Created on : 07-mar-2014, 15:38:43
    Author     : Americo
--%>

<%@page import="java.sql.SQLException"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>

<%java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd"); %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    ConectionDB con = new ConectionDB();
    HttpSession sesion = request.getSession();
    String id_usu = "";
    String nivel = (String) sesion.getAttribute("nivel");
    try {
        id_usu = (String) session.getAttribute("id_usu");
    } catch (Exception e) {
        Logger.getLogger("CatalogoMedica.jsp").log(Level.SEVERE, e.getMessage(), e);
    }

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }

    int meses = 0;
    try {
        meses = Integer.parseInt(request.getParameter("meses"));
        if (meses == 0) {
            meses = 500;
        }
        Calendar c1 = GregorianCalendar.getInstance();
        c1.add(Calendar.MONTH, meses);

        if (meses == 500) {
            meses = 0;
        }

    } catch (NumberFormatException e) {
        Logger.getLogger("CatalogoMedica.jsp").log(Level.SEVERE, e.getMessage(), e);
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="../css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../css/topPadding.css" rel="stylesheet">
        <link href="../css/dataTables.bootstrap.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <title>Sistema de Captura de Receta</title>
    </head>
    <body>

        <%@include file="../jspf/mainMenu.jspf"%>

        <div class="container-fluid" style="padding-top: 50px">
            <div class="container">
                <h3>Catálogo Medicamento</h3>                
                <br />
                <div class="row">
                    <h3 class="col-lg-3 col-md-3 col-sm-3" >Nivel del catálogo:</h3>
                    <h3 class="col-lg-1 col-md-1 col-sm-1" style="text-decoration: underline; "><strong id="nivel" ></strong></h3>
                </div>
                <div class="row">
                    <h3 class="col-lg-3 col-md-3 col-sm-3" >CC: Compra Consolidada</h3>
                    <h4 class="col-lg-1 col-md-1 col-sm-1" ></h4>
                    <h3 class="col-lg-2 col-md-2 col-sm-2" >VD: Venta Directa</h3>
                    <h4 class="col-lg-1 col-md-1 col-sm-1" ></h4>

                </div>
                <div class="row">
                    <h3 class="col-lg-3 col-md-3 col-sm-3" >Claves CC (1):</h3>
                    <h3 class="col-lg-1 col-md-1 col-sm-1" style="text-decoration: underline; "><strong id="Ca" ></strong></h3>
                    <h3 class="col-lg-2 col-md-2 col-sm-2" >Claves VD (2):</h3>
                    <h3 class="col-lg-1 col-md-1 col-sm-1" style="text-decoration: underline; "><strong id="Cv" ></strong></h3>
                    <h3 class="col-lg-2 col-md-2 col-sm-2" >Total de claves: </h3>
                    <h3 class="col-lg-1 col-md-1 col-sm-1" style="text-decoration: underline; "><strong id="Ct" ></strong></h3>
                </div>

                <br/>
                <table class="table table-bordered table-condensed table-responsive table-striped" id="existencias">
                    <thead>
                        <tr>
                            <td>Clave</td>
                            <td>Descripción</td>
                            <td>Tipo Med.</td>
                            <td>Origen</td>


                        </tr>
                    </thead>
                    <tbody>
                        <%                            try {
                                con.conectar();
                                ResultSet rset = con.consulta("SELECT p.cla_pro,p.des_pro,p.tip_pro,p.origen,pn.nivel FROM productos p, productosnivel pn WHERE p.cla_pro=pn.clave AND p.f_status='A' AND pn.nivel='" + nivel + "' GROUP BY p.cla_pro ;");
                                while (rset.next()) {
                        %>
                        <tr>

                            <td><%=rset.getString(1)%></td>
                            <td><%=rset.getString(2)%></td>
                            <td><%=rset.getString(3)%></td>
                            <td><%=rset.getString(4)%></td> 



                        </tr>
                        <%
                                }

                            } catch (SQLException ex) {
                                Logger.getLogger("modRecetasSurtidas.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                            } finally {
                                try {
                                    if (con.estaConectada()) {
                                        con.cierraConexion();
                                    }
                                } catch (SQLException ex) {
                                    Logger.getLogger("modRecetasSurtidas.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                }
                            }
                        %>
                    </tbody>
                </table>

            </div>
        </div>

    </body>
    <!-- 
    ================================================== -->
    <!-- Se coloca al final del documento para que cargue mas rapido -->
    <!-- Se debe de seguir ese orden al momento de llamar los JS -->
    <script src="../js/jquery-1.9.1.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/jquery.dataTables.js"></script>
    <script src="../js/dataTables.bootstrap.js"></script>
    <script src="../js/catalogo/catalogo.js"></script>
    <script>
        var context = "${pageContext.servletContext.contextPath}";

        $(document).ready(function () {
            $('#existencias').dataTable();

        });
    </script>
</html>

