<%-- 
    Document   : index
    Created on : 07-mar-2014, 15:38:43
    Author     : Americo
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    HttpSession sesion = request.getSession();

    String id_usu = "";
    id_usu = (String) session.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../  index.jsp");
        return;
    }

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="../css/bootstrap.css" rel="stylesheet" media="screen"/>
        <link href="../css/topPadding.css" rel="stylesheet"/>
        <link href="../css/dataTables.tableTools.css" rel="stylesheet"/>
        <link href="../css/jquery.dataTables.min.css" rel="stylesheet"/>
        <link href="../css/cupertino/jquery-ui-1.10.3.custom.css" rel="stylesheet"/>
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <title>Solicitado contra Surtido</title>
    </head>
    <body>
        <%@include file="../jspf/mainMenu.jspf"%>
        <br />
        <div class="container-fluid">
            <div class="container">
                <form class="form-horizontal" action="../reportes/repSolSur.jsp" method="post">

                    <div class="row">
                        <div class="col-md-5"><h3>Reporte Solicitado Contra Surtido</h3></div>
                        <div class="col-md-1"></div>
                        <div class="col-md-6"></div>
                    </div>

                    <br />
                    <div class="row">
                        <label class="control-label col-lg-2" for="hora_ini">Fecha Inicio:</label>
                        <div class="col-lg-3">
                            <input class="form-control" id="hora_ini" data-date-format="yyyy/mm/dd" name="hora_ini" />
                        </div>
                        <label class="control-label col-lg-2"  for="hora_fin">Fecha Fin:</label>
                        <div class="col-lg-3">
                            <input class="form-control" data-date-format="yyyy/mm/dd" id="hora_fin" name="hora_fin" />
                        </div>                        
                        <div class="hidden">
                            <label>
                                <input type="checkbox" name="receta" value="1" checked> Por Receta?
                            </label>
                        </div>
                        <div class="col-lg-2">
                            <button class="btn btn-success btn-block" id="generar" type="button"  >Generar Reporte</button>
                        </div>

                    </div>
                    <br>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-md-2 col-sm-offset-4"><img src="../imagenes/isem.png" width="150" alt="Logo"></div>
                        <div class="col-md-2"><img src="../imagenes/gobierno.png" width="150" alt="Logo"></div>
                    </div>
                </form>
                <div class="panel-body" id="tableTot" >

                    <div id="container">

                        <div id="dynamic"></div>

                    </div>
                </div>

            </div>
        </div>
        <script src="../js/jquery-2.1.4.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/jquery-ui.js"></script>
        <script src="../js/dataTables.tableTools.js"></script>
        <script src="../js/jquery.maskedinput.min.js" type="text/javascript"></script>
        <script>
            $(function ()
            {
                $('#hora_ini').mask("99-99-9999", {placeholder: " "});
                $('#hora_fin').mask("99-99-9999", {placeholder: " "});
                $("#hora_ini").datepicker(
                        {
                            dateFormat: 'yy-mm-dd',
                            changeYear: true,
                            changeMonth: true,
                            onClose: function (selectedDate) {
                                $("#hora_fin").datepicker("option", "minDate", selectedDate);
                            }
                        });
                $("#hora_fin").datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeYear: true,
                    changeMonth: true,
                    onClose: function (selectedDate) {
                        $("#hora_ini").datepicker("option", "maxDate", selectedDate);
                    }

                });

                $("#generar").click(function ()
                {
                    var hora_ini = $("#hora_ini").val();
                    var hora_fin = $("#hora_fin").val();

                    if (hora_ini === "")
                    {
                        alert("Ingresar una fecha de inicio porfavor.");
                        return false;
                    } else if (hora_fin === "")
                    {
                        alert("Ingresar una fecha final porfavor.");
                        return false;

                    } else
                    {
                        $.ajax({
                            url: "${pageContext.servletContext.contextPath}/repSolSur",
                            data: {ban: 1, hora_ini: hora_ini, hora_fin: hora_fin},
                            type: 'POST',
                            async: true,
                            success: function (data)
                            {

                                limpiarTabla();
                                MostrarTabla(data);

                            }, error: function (jqXHR, textStatus, errorThrown) {

                                alert("Clave invalida");
                            }



                        });
                    }
                });


            });

            function limpiarTabla() {
                $("#example").remove();
            }

            function MostrarTabla(data) {

                var json = JSON.parse(data);
                var totpzSol = 0;
                var totpzSur = 0;
                var aDataSet = [];
                for (var i = 0; i < json.length; i++) {
                    var cons = json[i].cons;
                    var fol = json[i].fol;
                    var cl = json[i].cl;
                    var des = json[i].des;
                    var sol = json[i].sol;
                    var sur = json[i].sur;

                    totpzSol = totpzSol + sol;
                    totpzSur = totpzSur + sur;

                    aDataSet.push([cons, fol, cl, des, sol, sur]);
                }
                $(document).ready(function () {
                    $('#dynamic').html('<table class="cell-border" width="100%" cellspacing="0"  id="example"><tfoot style="text-align: center; text-decoration: underline; font-size: 20px " ><tr><td></td><td></td><td></td><td>Totales:</td><td>' + totpzSol + '</td><td>' + totpzSur + '</td></tr></tfoot></table>');
                    $('#example').dataTable({
                        "aaData": aDataSet,
                        "button": 'aceptar',
                        "bScrollInfinite": true,
                        "bScrollCollapse": true,
                        "sScrollY": "500px",
                        "bProcessing": true,
                        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                            /* Append the grade to the default row class name */
                            if (aData[4] > aData[5])
                            {
                                $('td', nRow).css("color", "red");
                            }

                        },
                        "sPaginationType": "full_numbers",
                        "order": [[0, "asc"]],
                        "dom": 'T<"clear">lfrtip',
                        "tableTools": {
                            "sSwfPath": "../swf/copy_csv_xls_pdf.swf"
                        },
                        "aoColumns": [
                            {"sTitle": "Consecutivo", "sClass": "text-center"},
                            {"sTitle": "Folio", "sClass": "center"},
                            {"sTitle": "Clave", "sClass": "text-center"},
                            {"sTitle": "Descripción", "sClass": "text-center"},
                            {"sTitle": "Solicitado", "sClass": "center"},
                            {"sTitle": "Surtido", "sClass": "text-center"}

                        ]


                    });
                });
            }

        </script>
    </body>
</html>

