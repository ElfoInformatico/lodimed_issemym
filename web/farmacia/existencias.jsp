<%-- 
    Document   : index
    Created on : 07-mar-2014, 15:38:43
    Author     : Americo
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    DecimalFormat formatNumber = new DecimalFormat("#,###,###,###");
    java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd");
    java.text.DateFormat df3 = new java.text.SimpleDateFormat("dd/MM/yyyy");
    java.text.DateFormat dfKardex = new java.text.SimpleDateFormat("yyyyMMdd");
    ConectionDB con = new ConectionDB();
    HttpSession sesion = request.getSession();
    String id_usu = "";
    String cla_uni = "";
    Date date = null;
    Date date1 = null;
    Date date2 = null;
    Date date3 = null;
    String fechaSistema = "";
    try {
        id_usu = (String) session.getAttribute("id_usu");
    } catch (Exception e) {
        Logger.getLogger("existencias.jsp").log(Level.SEVERE, e.getMessage(), e);
    }

    try {
        con.conectar();
        ResultSet rset = con.consulta("select cla_uni from usuarios where id_usu = '" + id_usu + "' ");
        while (rset.next()) {
            cla_uni = rset.getString("cla_uni");
        }
    } catch (SQLException ex) {
        Logger.getLogger("existencias.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
    } finally {
        try {
            if (con.estaConectada()) {
                con.cierraConexion();
            }
        } catch (SQLException ex) {
            Logger.getLogger("existencias.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        }
    }
    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }

    int meses = 0;
    try {
        String m = request.getParameter("meses") == null ? "0" : request.getParameter("meses");

        meses = Integer.parseInt(m);
        if (meses == 0) {
            meses = 500;
        }
        Calendar c1 = GregorianCalendar.getInstance();
        c1.add(Calendar.MONTH, meses);

      
        if (meses == 500) {
            meses = 0;
        }

    } catch (Exception e) {
        Logger.getLogger("existencias.jsp").log(Level.SEVERE, e.getMessage(), e);
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="../css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../css/topPadding.css" rel="stylesheet">
        <link href="../css/dataTables.bootstrap.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <title>Existencias</title>
    </head>
    <body>
        <%@include file="../jspf/mainMenu.jspf"%>
        <br/>
        <div class="container-fluid">
            <div class="container">

                <div class="row">
                    <div class="col-md-8"><h1>Existencias</h1></div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3"><img src="../imagenes/gobierno.png" width=100 alt="Logo"></div>
                </div>
                <div class="row">
                    <div class="col-lg-12 form-horizontal">
                        <h4><strong>Total de Piezas:
                                <%                                    try {
                                        con.conectar();
                                        ResultSet rset = con.consulta("select sum(cant) from existencias where cant!=0 and f_status='A'");
                                        while (rset.next()) {
                                            out.println(formatNumber.format(rset.getInt(1)));
                                        }
                                    } catch (SQLException ex) {
                                        Logger.getLogger("existencias.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                    } finally {
                                        try {
                                            if (con.estaConectada()) {
                                                con.cierraConexion();
                                            }
                                        } catch (SQLException ex) {
                                            Logger.getLogger("existencias.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                        }
                                    }
                                %>
                                piezas</strong>
                        </h4>
                    </div>
                </div>
                <br />
                <div class="row">
                    <form action="existencias.jsp" method="post">
                        <label class="col-lg-2 control-label">Próximos a Caducar en Meses</label>

                        <div class="col-lg-1">
                            <input class="form-control" name="meses" id="meses" value="<%=meses%>" />
                        </div>
                        <div class="col-lg-1">
                            <button class="btn btn-success" type="submit">Consultar</button>
                        </div>
                        <div class="col-lg-1">
                            <a class="btn btn-success" href="gnrExistencias.jsp" >Descargar</a>
                        </div>
                        <div class="col-lg-1">
                            <a class="btn btn-danger" href="existencias.jsp" >Actualizar</a>
                        </div>

                    </form>
                    <!--div class="col-lg-2">
                        <form action="../drive">
                            <button type="submit" class="btn btn-danger btn-block" onclick="return validaRespaldo()">Respaldar</button>
                        </form>
                    </div-->
                </div>
                <div class="row" id="imgCarga">
                    <div class="text-center">
                        <img src="../imagenes/ajax-loader-1.gif" width="100">
                    </div>
                </div>
                <br/>
                <table class="table table-bordered table-condensed table-responsive table-striped" id="existencias">
                    <thead>
                        <tr>
                            <td>Clave</td>
                            <!--td>CB</td-->
                            <td>Descripción</td>
                            <td>Lote</td>
                            <td>Caducidad</td>
                            <td>Origen</td>
                            <td>Cantidad</td>
                            <td>Semaforización</td>
                            <td>Kardex</td>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            try {
                                con.conectar();
                                String clave = "", descrip = "", lote = "", caducidad = "", origen = "", cantidad = "0";

                                ResultSet rset = con.consulta("select * from existencias where cant!=0 and f_status='A' ORDER BY cad_pro ASC ");
                                String cb = "";
                                String caducado = "";
                                String url = "";
                                String caducidadKardex = "";
                                while (rset.next()) {
                                    clave = rset.getString(1);
                                    descrip = rset.getString(3);
                                    lote = rset.getString(4);
                                    caducidad = rset.getString(5);
                                    origen = rset.getString(6);
                                    cantidad = rset.getString(7);
                                    //Rutina para semaforizacion                                
                                    SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
                                    Calendar c = Calendar.getInstance();
                                    c.setTime(new Date());
                                    c.add(Calendar.MONTH, 3);  // añade tres meses

                                    Calendar c6 = Calendar.getInstance();
                                    c6.setTime(new Date());
                                    c6.add(Calendar.MONTH, 6);

                                    String fecAct = formateador.format(c.getTime());
                                    date = formateador.parse(fecAct);

                                    String fecAct1 = formateador.format(c6.getTime());
                                    date2 = formateador.parse(fecAct1);

                                    String caduSis = formateador.format(rset.getDate(5));
                                    date1 = formateador.parse(caduSis);

                                    if (date1.before(date)) {

                                        caducado = "<button class='btn btn-danger text-center ' style='color: #d9534f' >3</button>";
                                    } else if (date1.before(date2)) {
                                        caducado = "<button class='btn btn-warning text-center ' style='color: #f0ad4e' >2</button>";
                                    } else {
                                        caducado = "<button class='btn btn-success text-center ' style='color: #5cb85c' >1</button>";
                                    }

                                    caducidad = df3.format(df2.parse(caducidad));
                                    caducidadKardex = dfKardex.format(df3.parse(caducidad));
                                    url = String.format("/farmacia/kardex.jsp?accion=buscarLote&cla_pro=%s&lot_pro=%s&cad_pro=%s&id_ori=%s&enviado=existencia", clave, lote, caducidadKardex, origen);
                        %>
                        <tr>

                            <td><%=clave%></td>
                            <!--td></td-->
                            <td><%=descrip%></td>
                            <td><%=lote%></td>
                            <td><%=caducidad%></td>
                            <td><%=origen%></td>
                            <td><%=cantidad%></td>
                            <td class="text-center" ><%=caducado%></td>
                            <td><a class="btn btn-success btn-block"  href="${pageContext.servletContext.contextPath}<%=url%>">Visualizar</a></td>
                        </tr>
                        <%

                                }
                            } catch (SQLException ex) {
                                Logger.getLogger("existencias.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                            } finally {
                                try {
                                    if (con.estaConectada()) {
                                        con.cierraConexion();
                                    }
                                } catch (SQLException ex) {
                                    Logger.getLogger("existencias.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                }
                            }
                        %>
                    </tbody>
                </table><br/>
                <label class="text-center h3 ">Claves en Cero</label><br/>
                <table class="table table-bordered table-condensed table-responsive table-striped" id="existenciasCero">

                    <thead>
                        <tr>
                            <td>Clave</td>
                            <!--td>CB</td-->
                            <td>Descripción</td>
                            <td>Cantidad</td>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            try {
                                con.conectar();
                                String clave = "", descrip = "", lote = "", caducidad = "", origen = "", cantidad = "0";

                                ResultSet rset = con.consulta("select cla_pro, des_pro from productos where cla_pro not in ( SELECT DISTINCT d.cla_pro FROM detalle_productos d, inventario i WHERE d.det_pro=i.det_pro AND i.cant<>0  AND i.cla_uni='" + cla_uni + "' )  AND f_status='A';");
                                while (rset.next()) {
                                    clave = rset.getString(1);
                                    descrip = rset.getString(2);

                        %>
                        <tr>

                            <td><%=clave%></td>
                            <!--td></td-->
                            <td><%=descrip%></td>
                            <td>0</td>
                        </tr>
                        <%

                                }
                            } catch (SQLException ex) {
                                Logger.getLogger("existencias.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                            } finally {
                                try {
                                    if (con.estaConectada()) {
                                        con.cierraConexion();
                                    }
                                } catch (SQLException ex) {
                                    Logger.getLogger("existencias.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                }
                            }
                        %>
                    </tbody>
                </table>

            </div>
        </div>
        <!-- 
            ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/dataTables.bootstrap.js"></script>
        <script>

                                $(document).ready(function () {
                                    $('#existencias').dataTable();
                                    $('#existenciasCero').dataTable();
                                    $('#imgCarga').toggle();
                                });

                                function validaRespaldo() {
                                    var c = confirm('Seguro de generar el respaldo?');
                                    if (c) {
                                        $('#imgCarga').toggle();
                                        return true;
                                    }
                                    return false;
                                }

                                function enviarAKardex(clave) {
                                    var url = '${pageContext.servletContext.contextPath}/farmacia/kardex.jsp?accion=buscarInsumo&cla_pro=&cb=';
                                    $.post(url, {accion: 'buscarInsumo', cla_pro: clave, cb: ''}, function () {
                                        window.location = $this.attr(url);
                                    });
                                }


        </script>
    </body>

</html>

