<%-- 
    Document   : invenCiclicos
    Created on : 27/04/2016, 11:33:38 AM
    Author     : juan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    HttpSession sesion = request.getSession();
    String id_usu = "";
    id_usu = (String) sesion.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Inventarios Ciclicos</title>
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="../css/cupertino/jquery-ui-1.10.3.custom.css" rel="stylesheet">
        <link href="../css/dataTables.tableTools.css" rel="stylesheet">
    </head>
    <body>
        <%@include file="../jspf/mainMenu.jspf"%>
        <div class="container text-center" style="padding-top: 50px">
            <h1>Inventarios Cíclicos Medicamento Controlado</h1>
        </div>
        <div class="container" style="padding-top: 50px; background-color:#FFFFFF; padding:10px; margin:auto" >
            <div>
            </div>

            <h3>Ingresar claves a auditar</h3>

            <br />
            <div class="row">

                <div class="col-sm-3">
                    <input type="number" min="0" class="form-control" id="numeroCLaves" name="numeroCLaves" />
                </div>
                <div class="col-sm-3">
                    <button type="button" class="btn btn-block btn-success" name="generarClaves" id="generarClaves">
                        <i class="glyphicon glyphicon-search"></i> Consultar
                    </button>
                </div>

            </div>

            <hr/>
            <div class="panel panel-success" id="tableAuditar" >
                <!-- Default panel contents -->
                <div class="panel-heading"  >
                    <div class="row">
                        <h4 class="col-lg-1 col-md-1 col-sm-1">Claves</h4>
                        <div class="col-lg-9 col-md-9 col-sm-9" ></div>
                        <div class="col-lg-2 col-md-2 col-sm-2" >
                            <button class="btn btn-block btn-success" type="button" id="recorrer" >Verificar Claves</button>
                        </div>
                        
                    </div>
                </div>
                <div class="panel-body"  >
                    <div id="container">

                        <div id="dynamic"></div>

                    </div>
                </div>

            </div>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"></h4>
                        </div>
                        <div class="modal-body">
                            <div class="text-center" id="imagenCarga">
                                <img src="../imagenes/ajax-loader-1.gif" alt="" />
                            </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            var context = "${pageContext.servletContext.contextPath}";
        </script>
        <script src="../js/jquery-2.1.4.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery-ui.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/dataTables.tableTools.js"></script>
        <script src="../js/inventarios/controlado.js"></script>
    </body>
</html>
