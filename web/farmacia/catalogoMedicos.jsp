<%-- 
    Document   : catalogoMedicos
    Created on : 16/02/2015, 03:56:50 PM
    Author     : Americo
--%>

<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    ConectionDB con = new ConectionDB();
    HttpSession sesion = request.getSession();
    String id_usu = "";
    id_usu = (String) session.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }

%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../css/dataTables.bootstrap.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <title>LODIMED</title>
    </head>
    <body>

        <%@include file="../jspf/mainMenu.jspf"%>

        <div class="container-fluid">
            <div class="container">
                <h3>Catálogo Medicamento</h3>   
                <h1>Catálogo de Médicos</h1>
                <hr/>
                <table class="table table-bordered table-condensed table-striped" id="catMedicos">
                    <thead>
                        <tr>
                            <td>Cédula</td>
                            <td>Nombre</td>
                            <td>Unidad</td>
                            <td>Folio Inicio</td>
                            <td>Folio Fin</td>
                            <td>Folio Actual</td>
                        </tr>
                    </thead>
                    <tbody>
                        <%                            try {
                                con.conectar();
                                ResultSet rset = con.consulta("SELECT medicos.cedula, medicos.nom_com, unidades.des_uni, medicos.iniRec, medicos.finRec, medicos.folAct FROM medicos INNER JOIN unidades ON unidades.cla_uni = medicos.clauni where cedula !=1");
                                while (rset.next()) {
                        %>
                        <tr>
                            <td><%=rset.getString(1)%></td>
                            <td><%=rset.getString(2)%></td>
                            <td><%=rset.getString(3)%></td>
                            <td><%=rset.getString(4)%></td>
                            <td><%=rset.getString(5)%></td>
                            <td><%=rset.getString(6)%></td>
                        </tr>
                        <%
                                }
                            } catch (SQLException ex) {
                                Logger.getLogger("catalogoMedicos.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                            } finally {
                                try {
                                    if (con.estaConectada()) {
                                        con.cierraConexion();
                                    }
                                } catch (SQLException ex) {
                                    Logger.getLogger("catalogoMedicos.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                }
                            }
                        %>
                    </tbody>

                </table>
            </div>
        </div>
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $.ajax({
                    type: "GET",
                    url: "../CatalogoMedico?accion=medicos",
                    dataType: "json",
                    success: function (data) {
                        var table = $("#tablaAlto");
                        $.each(data, function (idx, elem) {
                            table.append("<tr><td>" + elem.cedula + "</td><td>" + elem.nombre + "</td>   <td>" + elem.unidad + "</td><td>" + elem.folioInicio + "</td><td>" + elem.folioFin + "</td><td>" + elem.folioActual + "</td></tr>");
                        });
                        $('#catMedicos').dataTable({
                        });
                    }
                });
            });
        </script>
    </body>
</html>
