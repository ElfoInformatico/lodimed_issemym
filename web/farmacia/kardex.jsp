<%-- 
    Document   : kardex
    Created on : 23/02/2015, 05:09:23 PM
    Author     : Americo
    Author     : Sebastian
--%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>
<%
    DecimalFormat formatter = new DecimalFormat("#,###,###");
    DecimalFormatSymbols custom = new DecimalFormatSymbols();
    custom.setDecimalSeparator(',');
    formatter.setDecimalFormatSymbols(custom);
    java.text.DateFormat df2 = new java.text.SimpleDateFormat("yyyy-MM-dd");
    java.text.DateFormat dfKardex = new java.text.SimpleDateFormat("yyyyMMdd");
    ConectionDB con = new ConectionDB();
    HttpSession sesion = request.getSession();
    String id_usu = "";
    String accion = "";
    accion = request.getParameter("accion");
    if (accion == null) {
        accion = "";
    }
    try {
        id_usu = (String) sesion.getAttribute("id_usu");
    } catch (Exception e) {
        Logger.getLogger("reporteAbasto.jsp").log(Level.SEVERE, e.getMessage(), e);
    }

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link href="../css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../css/topPadding.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>


        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <title>Kardex</title>
    </head>
    <body>
        <%@include file="../jspf/mainMenu.jspf" %>
        <div class="container" style="padding-top: 50px">
            <h1>Kardex</h1>
            <hr/>
            <div class="row">
                <form action="kardex.jsp" method="post">
                    <div class="col-sm-2">
                        <input class="form-control" placeholder="Código de Barras" name="cb" />
                    </div>
                    <div class="col-sm-2">
                        <input class="form-control" placeholder="Clave" name="cla_pro" />
                    </div>
                    <div class="col-sm-1">
                        <button class="btn btn-success btn-block" name="accion" value="buscarInsumo"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
                </form>
            </div>
            <hr/>
            <%                try {
                    con.conectar();
                    if (accion.equals("buscarInsumo")) {
                        ResultSet rset = con.consulta("select cla_pro, des_pro from productos p, tb_codigob cb where p.cla_pro = cb.F_Clave and (p.cla_pro = '" + request.getParameter("cla_pro") + "' or cb.F_Cb='" + request.getParameter("cb") + "') group by cla_pro");
                        while (rset.next()) {
            %>
            <form action="kardex.jsp" method="post">
                <div class="row">
                    <h4 class="col-sm-12">
                        <input class="hidden" placeholder="Clave" name="cla_pro" value="<%=rset.getString("cla_pro")%>" />
                        Clave: <%=rset.getString("cla_pro")%>
                    </h4> 

                    <h4 class="col-sm-12">
                        Descripción: <%=rset.getString("des_pro")%>
                    </h4> 
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <select class="form-control" name="id_ori">
                            <option value="">Origen</option>
                            <%
                                ResultSet rset2 = con.consulta("select dp.id_ori, o.des_ori from detalle_productos dp, origen o where dp.id_ori = o.id_ori and dp.cla_pro = '" + rset.getString("cla_pro") + "' group by id_ori order by id_ori");
                                while (rset2.next()) {
                            %>
                            <option value="<%=rset2.getString("id_ori")%>"><%=rset2.getString("des_ori")%></option>
                            <%
                                }
                            %>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control" name="lot_pro" id="lot_pro">
                            <option value="">Lote</option>
                            <%
                                rset2 = con.consulta("select lot_pro from detalle_productos where cla_pro = '" + rset.getString("cla_pro") + "' ");
                                while (rset2.next()) {
                            %>
                            <option value="<%=rset2.getString("lot_pro")%>"><%=rset2.getString("lot_pro")%></option>
                            <%
                                }
                            %>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control" id="Cadu" name="cad_pro">
                            <option value="">Caducidad</option>
                            <%
                                rset2 = con.consulta("select cad_pro from detalle_productos where cla_pro = '" + rset.getString("cla_pro") + "' ");
                                while (rset2.next()) {
                            %>
                            <option value="<%=rset2.getString("cad_pro")%>"><%=rset2.getString("cad_pro")%></option>
                            <%
                                }
                            %>
                        </select>
                    </div>
                    <div class="col-sm-1">
                        <button class="btn btn-success btn-block" name="accion" value="buscarLote"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
                </div>
            </form>
            <%                }
                }
                if (accion.equals("buscarLote")) {
                    ResultSet rset = con.consulta("select cla_pro, des_pro from productos p, tb_codigob cb where p.cla_pro = cb.F_Clave and (p.cla_pro = '" + request.getParameter("cla_pro") + "' or cb.F_Cb='" + request.getParameter("cb") + "') group by cla_pro");
                    while (rset.next()) {
            %>
            <form action="kardex.jsp" method="post">
                <div class="row">
                    <h4 class="col-sm-12">
                        <input class="hidden" placeholder="Clave" name="cla_pro" value="<%=rset.getString("cla_pro")%>" />
                        Clave: <%=rset.getString("cla_pro")%>
                    </h4> 

                    <h4 class="col-sm-12">
                        Descripción: <%=rset.getString("des_pro")%>
                    </h4> 
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <select class="form-control" name="id_ori">
                            <option value="">Origen</option>
                            <%
                                ResultSet rset2 = con.consulta("select dp.id_ori, o.des_ori from detalle_productos dp, origen o where dp.id_ori = o.id_ori and dp.cla_pro = '" + rset.getString("cla_pro") + "' group by id_ori order by id_ori");
                                while (rset2.next()) {
                            %>
                            <option value="<%=rset2.getString("id_ori")%>"><%=rset2.getString("des_ori")%></option>
                            <%
                                }
                            %>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control" name="lot_pro" id="lot_pro">
                            <option value="">Lote</option>
                            <%
                                rset2 = con.consulta("select lot_pro from detalle_productos where cla_pro = '" + rset.getString("cla_pro") + "' ");
                                while (rset2.next()) {
                            %>
                            <option value="<%=rset2.getString("lot_pro")%>"><%=rset2.getString("lot_pro")%></option>
                            <%
                                }
                            %>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control" id="Cadu" name="cad_pro">
                            <option value="">Caducidad</option>
                            <%
                                rset2 = con.consulta("select cad_pro from detalle_productos where cla_pro = '" + rset.getString("cla_pro") + "' ");
                                while (rset2.next()) {
                            %>
                            <option value="<%=rset2.getString("cad_pro")%>"><%=rset2.getString("cad_pro")%></option>
                            <%
                                }
                            %>
                        </select>
                    </div>
                    <div class="col-sm-1">
                        <button class="btn btn-success btn-block" name="accion" value="buscarLote"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
                </div>
            </form>
            <hr/>
        </div>
        <div style="width: 90%; margin: auto">
            <%
                String clave, lote, caducidad, origen, enviado;
                clave = request.getParameter("cla_pro");
                lote = request.getParameter("lot_pro");
                enviado = request.getParameter("enviado");
                caducidad = request.getParameter("cad_pro");
                if (enviado != null) {
                    caducidad = df2.format(dfKardex.parse(caducidad));
                }
                origen = request.getParameter("id_ori");
                rset2 = con.consulta("select SUM(cant) from existencias where cla_pro = '" + clave + "' and lot_pro = '" + lote + "' and cad_pro = '" + caducidad + "' and id_ori = '" + origen + "' ");
                int existencias = 0;
                while (rset2.next()) {
                    existencias = rset2.getInt(1);
                }
            %>
            <h4>Existencias: <%=existencias%></h4>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row" >
                        <h5 class="col-lg-1 col-md-1 col-sm-1">kardex</h5>
                        <div class="col-lg-9 col-md-9 col-sm-9"></div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <button class="btn btn-danger btn-block" type="button" onclick="location.reload();" ><span class="glyphicon glyphicon-refresh" ></span></button>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-condensed table-hover" id="tbEntradas">
                        <thead>
                            <tr>
                                <td>Clave</td>
                                <td>Lote</td>
                                <td>Caducidad</td>
                                <td>Origen</td>
                                <td>Cantidad</td>
                                <td>Tipo Mov</td>
                                <td>Abasto</td>
                                <td>FolReceta</td>
                                <td>Paciente</td>
                                <td>M&eacute;dico</td>
                                <td>Fecha</td>
                                <td>Observaciones</td>
                                <td>Usuario</td>
                                <td>Sumatoria</td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                int sumatoria = 0;
                                PreparedStatement ps = con.getConn().prepareStatement("SELECT dp.cla_pro AS clave, dp.lot_pro AS lote, dp.cad_pro AS caducidad, dp.id_ori AS origen, IF ( LOCATE('SALIDA', k.tipo_mov) > 0, k.cant * - 1, k.cant ) AS cantidad, k.tipo_mov AS movimiento, k.fol_aba AS fol_aba, r.fol_rec AS folio, IF ( LOCATE('SALIDA RECETA', k.tipo_mov) > 0, pa.nom_com, '-' ) AS paciente, IF ( LOCATE('SALIDA RECETA', k.tipo_mov) > 0, m.nom_com, '-' ) AS medico, k.fecha AS fecha, k.obser AS obser, u.nombre_completo AS usuario FROM kardex AS k INNER JOIN detalle_productos AS dp ON k.det_pro = dp.det_pro LEFT JOIN receta AS r ON r.id_rec = k.id_rec INNER JOIN pacientes AS pa ON r.id_pac = pa.id_pac INNER JOIN medicos AS m ON m.cedula = r.cedula INNER JOIN usuarios AS u ON k.id_usu = u.id_usu WHERE dp.cla_pro = ? AND dp.lot_pro = ? AND dp.cad_pro = ? AND k.cant > 0 ORDER BY fecha ASC;");
                                ps.setString(1, clave);
                                ps.setString(2, lote);
                                ps.setString(3, caducidad);

                                rset2 = ps.executeQuery();
                                while (rset2.next()) {
                                    sumatoria += rset2.getInt("cantidad");
                            %>
                            <tr class="
                                <%
                                    if (rset2.getInt("cantidad") > 0) {
                                        out.println("success");
                                    }
                                %>
                                "
                                >
                                <td><%=rset2.getString("clave")%></td>
                                <td><%=rset2.getString("lote")%></td>
                                <td><%=rset2.getString("caducidad")%></td>
                                <td><%=rset2.getString("origen")%></td>
                                <td><%=rset2.getString("cantidad")%></td>
                                <td><%=rset2.getString("movimiento")%></td>
                                <td><%=rset2.getString("fol_aba")%></td>
                                <td><%=rset2.getString("folio")%></td>
                                <td><%=rset2.getString("paciente")%></td>
                                <td><%=rset2.getString("medico")%></td>
                                <td><%=rset2.getString("fecha")%></td>
                                <td><%=rset2.getString("obser")%></td>
                                <td><%=rset2.getString("usuario")%></td>
                                <td><%=sumatoria%></td>
                            </tr>
                            <%
                                }
                                rset2.close();
                                ps.close();
                            %>
                        </tbody>
                    </table>
                </div>
            </div>

            <%
                        }
                    }
                } catch (SQLException ex) {
                    Logger.getLogger("kardex.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                } finally {
                    try {
                        if (con.estaConectada()) {
                            con.cierraConexion();
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger("kardex.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                    }
                }
            %>
            <h4></h4>
        </div>
        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/jquery-ui.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/dataTables.bootstrap.js"></script>
        <script>
                                $(document).ready(function () {
                                    $('#tbSalidas').dataTable();
                                    $('#tbEntradas').dataTable();
                                });

                                $('#lot_pro').change(function () {
                                    var idx = this.selectedIndex;
                                    $("#Cadu").prop('selectedIndex', idx);
                                });
        </script>
    </body>
</html>
