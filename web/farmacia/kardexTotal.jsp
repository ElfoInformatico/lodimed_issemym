<%-- 
    Document   : kardex
    Created on : 23/02/2015, 05:09:23 PM
    Author     : Americo
    Author     : Sebastian
--%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>
<%
    DecimalFormat formatter = new DecimalFormat("#,###,###");
    DecimalFormatSymbols custom = new DecimalFormatSymbols();
    custom.setDecimalSeparator(',');
    formatter.setDecimalFormatSymbols(custom);
    ConectionDB con = new ConectionDB();
    HttpSession sesion = request.getSession();
    String id_usu = "";
    String accion = "";
    accion = request.getParameter("accion");
    if (accion == null) {
        accion = "";
    }
    try {
        id_usu = (String) sesion.getAttribute("id_usu");
    } catch (Exception e) {
        Logger.getLogger("reporteAbasto.jsp").log(Level.SEVERE, e.getMessage(), e);
    }

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link href="../css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../css/topPadding.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>


        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <title>Kardex</title>
    </head>
    <body>
        <%@include file="../jspf/mainMenu.jspf" %>
        <div class="container" style="padding-top: 50px">
            <h1 style="color: black" >Kardex</h1>
            <hr/>
            <hr/>
            <%                try {
                    con.conectar();
                    ResultSet rset = con.consulta("SELECT cla_pro, des_pro FROM productos p, tb_codigob cb WHERE p.cla_pro = cb.F_Clave GROUP BY cla_pro");
                    ResultSet rset2 = null;
                    while (rset.next()) {
            %>

            <hr/>
        </div>
        <div style="width: 90%; margin: auto">
            <%
                String clave, lote, caducidad, origen, enviado;
                clave = request.getParameter("cla_pro");

                rset2 = con.consulta("SELECT sum(cant) FROM existencias");
                int existencias = 0;
                while (rset2.next()) {
                    existencias = rset2.getInt(1);
                }
            %>
            <h4>Existencias: <%=existencias%></h4>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row" >
                        <h5 class="col-lg-1 col-md-1 col-sm-1">kardex</h5>
                        <div class="col-lg-9 col-md-9 col-sm-9"></div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <button class="btn btn-danger btn-block" type="button" onclick="location.reload();" ><span class="glyphicon glyphicon-refresh" ></span></button>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-condensed table-hover" id="tbEntradas">
                        <thead>
                            <tr>
                                <td>Clave</td>
                                <td>Lote</td>
                                <td>Caducidad</td>
                                <td>Origen</td>
                                <td>Cantidad</td>
                                <td>Tipo Mov</td>
                                <td>Abasto</td>
                                <td>FolReceta</td>
                                <td>Paciente</td>
                                <td>M&eacute;dico</td>
                                <td>Fecha</td>
                                <td>Observaciones</td>
                                <td>Sumatoria</td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                int sumatoria = 0;
                                rset2 = con.consulta("( SELECT cla_pro, lot_pro, cad_pro, id_ori, cant, tipo_mov, fol_aba, fol_rec, '' AS paciente, '' AS medico, fecha, obser FROM ventradas ) UNION ALL ( SELECT cla_pro, lot_pro, cad_pro, id_ori, - cant, tipo_mov, '' AS fol_aba, fol_rec, paciente, medico, fecha, obser FROM vsalidas ) UNION ALL ( SELECT cla_pro, lot_pro, cad_pro, id_ori, - cant, tipo_mov, '' AS fol_aba, fol_rec, paciente, medico, fecha, obser FROM vsalidasajustes ) ORDER BY fecha ASC;");
                                while (rset2.next()) {
                                    sumatoria += rset2.getInt("cant");
                            %>
                            <tr class="
                                <%
                                    if (rset2.getInt("cant") > 0) {
                                        out.println("success");
                                    }
                                %>
                                "
                                >
                                <td><%=rset2.getString("cla_pro")%></td>
                                <td><%=rset2.getString("lot_pro")%></td>
                                <td><%=rset2.getString("cad_pro")%></td>
                                <td><%=rset2.getString("id_ori")%></td>
                                <td><%=rset2.getString("cant")%></td>
                                <td><%=rset2.getString("tipo_mov")%></td>
                                <td><%=rset2.getString("fol_aba")%></td>
                                <td><%=rset2.getString("fol_rec")%></td>
                                <td><%=rset2.getString("paciente")%></td>
                                <td><%=rset2.getString("medico")%></td>
                                <td><%=rset2.getString("fecha")%></td>
                                <td><%=rset2.getString("obser")%></td>
                                <td><%=sumatoria%></td>
                            </tr>
                            <%
                                }
                            %>
                        </tbody>
                    </table>
                </div>
            </div>

            <%
                    }
                } catch (SQLException ex) {
                    Logger.getLogger("kardexByClave.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                } finally {
                    try {
                        if (con.estaConectada()) {
                            con.cierraConexion();
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger("kardexByClave.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                    }
                }
            %>
            <h4></h4>
        </div>
        <!-- 
        ================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/jquery-ui.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/dataTables.bootstrap.js"></script>
        <script>
                                $(document).ready(function () {
                                    $('#tbSalidas').dataTable();
                                    $('#tbEntradas').dataTable();
                                });
        </script>
    </body>
</html>
