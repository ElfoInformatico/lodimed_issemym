<%-- 
    Document   : kardex
    Created on : 23/02/2015, 05:09:23 PM
    Author     : Americo
--%>
<%@page import="Servlets.Catalogo"%>
<%@page import="Servlets.movientos.MovimentosProductos"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>
<%
    HttpSession sesion = request.getSession();

    String id_usu = (String) session.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Estilos CSS -->
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/cupertino/jquery-ui-1.10.3.custom.css" />
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/sweetalert.css" rel="stylesheet" type="text/css"/>
        <!---->
        <title>Entrada por traspaso</title>
    </head>
    <body>
        <%@include file="../jspf/mainMenu.jspf" %>%>
        <div class="container" style="padding-top: 50px">
            <h1>Entrada por traspaso (+)</h1>
            <hr/>
            <form id="formAplicar"  data-async data-target="#rating-modal" action="javascript:void(0);" method="POST">
                <div class="row">
                    <h4 class="col-sm-1">Clave</h4>
                    <div class="col-sm-2">
                        <input class="form-control" placeholder="Clave" name="clave" id="clave" required data-behavior = "only-alphanum"/>
                    </div>
                    <h4 class="col-sm-1">Lote</h4>
                    <div class="col-sm-2">
                        <input class="form-control" placeholder="Lote" name="lote" id="lote" required required data-behavior = "only-alphanum"/>
                    </div>
                    <h4 class="col-sm-1">Caducidad</h4>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="caducidad" name="caducidad" required/>
                    </div>
                </div>
                <br>
                <div class="row">
                    <h4 class="col-sm-1">Origen</h4>
                    <div class="col-sm-3">
                        <select class="form-control" name="origen" id="origen">
                            <option value="" disabled>--Seleccione el Origen--</option>
                            <option value="0" >COMPRA CONSOLIDADA</option>
                            <option value="1" >COMPRA CONSOLIDADA (VENTA)</option>
                            <option value="2" >VENTA DIRECTA</option>
                        </select>     
                    </div>
                    <h4 class="col-sm-1">C.B.</h4>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="cb" name="cb" placeholder="Código de Barras" required required data-behavior = "only-num"/>
                    </div>
                </div>
                <br>
                <div class="row">
                    <h4 class="col-sm-2">Cantidad del ajuste</h4>
                    <div class="col-sm-2">
                        <input class="form-control" type="text" name="cantAjuste"  min="1" required id="cantAjuste" required data-behavior = "only-num"/>
                    </div>
                </div>
                <br>
                <div class="row">
                    <h4 class="col-sm-2">Justificaci&oacute;n</h4>
                    <textarea class="form-control col-sm-10" rows="5" required placeholder="Justificación" name="obs" id="obs" required data-behavior = "only-alphanum-white"></textarea>
                </div>
                <br>
                <div class="row">
                    <button class="btn btn-success" name="accion" value="salidaAjuste" onclick="validarDatos()">Ajustar (+)</button>
                </div>
                <hr/> 
            </form>

            <div class="modal fade" id="Observaciones" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                <div class="modal-dialog ">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="row">
                                <div class="col-sm-5">
                                    Aplicar Entrada
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <h4 class="modal-title" id="myModalLabel">Contraseña</h4>
                            <div class="row">
                                <div class="col-sm-12">
                                    <input id="confirmarPass" class="form-control" type="password" onkeyup="" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button id="btnPassCancela" type="button" class="btn btn-danger" onclick="validaContra()" >Ajustar(+)</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery-ui.js"></script>
        <script src="../js/jquery.alphanum.js" type="text/javascript"></script>
        <script src="../js/jquery.maskedinput.min.js" type="text/javascript"></script>
        <script src="../js/sweetalert.min.js" type="text/javascript"></script>
        <script type="text/javascript">



                                    $("#caducidad").datepicker({
                                        dateFormat: 'dd/mm/yy',
                                        changeYear: true,
                                        changeMonth: true,
                                        minDate: 'today',
                                        onClose: function (dateText, inst) {
                                            if ($(this).val() === "undefined") {
                                                alert("");
                                                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                                                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                                                $(this).datepicker('setDate', new Date(year, month, 1));
                                            }
                                        }

                                    });

                                    $('#caducidad').mask("99/99/9999", {placeholder: " "});

                                    function validaContra() {
                                        var password = $("#confirmarPass").val();
                                        $.ajax({
                                            type: "POST",
                                            url: "../Farmacias",
                                            dataType: "json",
                                            data: {accion: "verificar", pass: password},
                                            success: function (data) {
                                                if (data.permitido) {
                                                    aplicar();
                                                } else {
                                                    $("#confirmarPass").val("");

                                                    swal({
                                                        title: "Validacion contraseña",
                                                        text: "Datos incorrectos, por favor verique la defaultrmación ingresada.",
                                                        type: "Error",
                                                        showCancelButton: false,
                                                        confirmButtonClass: "btn-default",
                                                        confirmButtonText: "Continuar"
                                                    });
                                                }
                                            },
                                            error: function (jqXHR, textStatus, errorThrown) {
                                                swal({
                                                    title: "Validacion contraseña",
                                                    text: jqXHR.responseText,
                                                    type: "error",
                                                    showCancelButton: false,
                                                    confirmButtonClass: "btn-default",
                                                    confirmButtonText: "Continuar"
                                                });
                                            }
                                        }
                                        );
                                    }

                                    function aplicar() {
                                        var caducidad, obs, cantAjuste, clave, lote, id_ori, cb;
                                        id_ori = $('#origen').val();
                                        clave = $('#clave').val();
                                        lote = $('#lote').val();
                                        caducidad = $('#caducidad').val();
                                        obs = $('#obs').val();
                                        cantAjuste = parseInt($('#cantAjuste').val());
                                        cb = $('#cb').val();
                                        if (cantAjuste > 0 && obs !== "" && clave !== "" && lote !== "" && caducidad !== "" && cb !== "") {

                                            var r = confirm("¿Desea realizar el traspaso de entrada?");
                                            if (r) {
                                                var $form = $('#formAplicar');
                                                console.log($form.serialize());


                                                $.ajax({
                                                    type: $form.attr('method'),
                                                    url: "../mover",
                                                    data: {"accion": "<%=MovimentosProductos.ENTRADA%>",
                                                        "clave": clave, "lote": lote, "caducidad": caducidad,
                                                        "codigoBarra": cb, "cantidad": cantAjuste, "observaciones": obs},
                                                    dataType: "json",
                                                    success: function (data) {
                                                        console.log(data);
                                                        swal({
                                                            title: "Traspaso exitoso",
                                                            text: 'Traspaso Realizado. Nueva cantidad inventario ' + data.nuevo,
                                                            type: "success",
                                                            showCancelButton: false,
                                                            confirmButtonClass: "btn-success",
                                                            confirmButtonText: "Continuar"
                                                        }, function () {
                                                            location.reload();
                                                        });
                                                    },
                                                    error: function (jqXHR, textStatus, errorThrown) {
                                                        console.log("r", jqXHR);
                                                        console.log("s", textStatus);
                                                        console.log("e", errorThrown);
                                                        swal({
                                                            title: "Validacion contraseña",
                                                            text: jqXHR.responseText,
                                                            type: "error",
                                                            showCancelButton: false,
                                                            confirmButtonClass: "btn-default",
                                                            confirmButtonText: "Continuar"
                                                        });
                                                    }
                                                });
                                            }
                                        }
                                    }

                                    function validarDatos() {

                                        var caducidad, obs, cantAjuste, clave, lote, id_ori, cb;
                                        id_ori = $('#origen').val();
                                        clave = $('#clave').val();
                                        lote = $('#lote').val();
                                        caducidad = $('#caducidad').val();
                                        obs = $('#obs').val();
                                        cantAjuste = parseInt($('#cantAjuste').val());
                                        cb = $('#cb').val();
                                        if (cantAjuste > 0 && obs !== "" && clave !== "" && lote !== "" && caducidad !== "" && cb !== "") {
                                            $('#Observaciones').modal('toggle');
                                        }
                                    }

                                    $("[data-behavior~=only-alphanum]").alphanum({
                                        allowSpace: false,
                                        allowOtherCharSets: false,
                                        allow: '.'
                                    });
                                    $("[data-behavior~=only-alphanum-white]").alphanum({
                                        allow: '.',
                                        disallow: "'",
                                        allowSpace: true
                                    });
                                    $("[data-behavior~=only-num]").numeric({
                                        allowMinus: false,
                                        allowThouSep: false
                                    });


                                    $("[data-behavior~=only-alpha]").alphanum({
                                        allowNumeric: false,
                                        allowSpace: false,
                                        allowOtherCharSets: true
                                    });

                                    $.ajax({
                                        url: "${pageContext.servletContext.contextPath}/Catalogo",
                                        data: {ban: <%=Catalogo.OBTENER_CATALAGO_PRODUCTOS%>},
                                        type: 'POST',
                                        async: true,
                                        dataType: 'json',
                                        success: function (data)
                                        {
                                            var claves_productos = [];
                                            
                                            data.forEach(function (value, id, arr) {
                                                claves_productos.push(value.id);
                                            });

                                            $("#clave").autocomplete({
                                                source: claves_productos
                                            });
                                        }, error: function (jqXHR, textStatus, errorThrown) {

                                            alert("Error Contactar al departamento de sistemas");

                                        }
                                    });
        </script>

    </body>
</html>
