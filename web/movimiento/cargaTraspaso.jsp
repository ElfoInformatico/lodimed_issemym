<%-- 
    Document   : index
    Created on : 07-mar-2014, 15:38:43
    Author     : Americo
    Author     : Sebastian
--%>

<%@page import="java.sql.SQLException"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="Clases.LeerCSV"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%    DecimalFormat formatter = new DecimalFormat("#,###,###");
    HttpSession sesion = request.getSession();
    String ver = "hidden";
    String id_usu = "";
    try {
        id_usu = (String) sesion.getAttribute("id_usu");
    } catch (Exception e) {
        Logger.getLogger("cargaTraspaso.jsp").log(Level.SEVERE, e.getMessage(), e);
    }

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }
    ConectionDB con = new ConectionDB();
    if (sesion.getAttribute("ver") != null) {
        if (sesion.getAttribute("ver").toString().equals("si")) {
            ver = "";
        } else {
            ver = "hidden";
        }
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
        <link href="../css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="../css/topPadding.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <title>LODIMED</title>
    </head>
    <body>
        <%@include file="../jspf/mainMenu.jspf"%>
        <br/>
        <div class="container">
            <div class="container" style="width: 600px;">
                <h2>Cargar Traspaso</h2>

                <form name="cargaAbasto" method="POST" enctype="multipart/form-data" action="../FileUploadServlet?pag=movimiento/cargaTraspaso.jsp" onsubmit="funcionCarga()">
                    <div class="row">
                        <label class="form-horizontal col-lg-4">Seleccione un archivo:</label>
                        <div class="col-lg-8">
                            <input required type="file" class="form-control" name="archivo" accept=".csv">
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <label class="form-horizontal col-lg-4">Contraseña:</label>
                        <div class="col-lg-8">
                            <input required type="password" name="contra" placeholder="Contraseña de Administrador" class="form-control">
                        </div>
                    </div>
                    <br />
                    <div class="col-lg-12">
                        <button class="btn btn-block btn-success" id="btnCarga">Cargar</button>
                    </div>

                    <c:if test="${sessionScope.info_upload!=null}">
                        <br>
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10 alert alert-danger">
                            <p><h4>Mensaje:</h4><c:out value="${sessionScope.info_upload}"/></p>
                            <c:set var="info_upload" scope = "session" value="${null}"/>

                        </div>
                    </c:if>
                </form>
            </div>          
            <div class="row" id="imgCarga">
                <div class="col-md-12 text-center">
                    <img src="../imagenes/ajax-loader-1.gif" width=100 alt="Logo">
                </div>
            </div>
            <br/><br/>

            <div class="panel <%=ver%>"  id="paAbasto">
                <%
                    try {
                        con.conectar();
                        String SUM_CANTIDAD = "SELECT SUM(cantidad) FROM carga_abasto";
                        PreparedStatement ps = con.getConn().prepareStatement(SUM_CANTIDAD);
                        ResultSet rs = ps.executeQuery();

                        while (rs.next()) {
                %>

                <h3>Total de piezas: <%=formatter.format(rs.getInt(1))%></h3>
                <%
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger("cargaTraspaso.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                    } finally {
                        try {
                            if (con.estaConectada()) {
                                con.cierraConexion();
                            }
                        } catch (SQLException ex) {
                            Logger.getLogger("cargaTraspaso.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                        }
                    }
                %>

                <%
                    try {
                        con.conectar();
                        String SUM_CANTIDAD = "SELECT SUM(cantidad) FROM carga_abasto WHERE carga_abasto.cargado = 1";
                        PreparedStatement ps = con.getConn().prepareStatement(SUM_CANTIDAD);
                        ResultSet rs = ps.executeQuery();

                        while (rs.next()) {
                %>

                <h3>Total a cargar: <%=formatter.format(rs.getInt(1))%></h3>
                <%
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger("cargaTraspaso.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                    } finally {
                        try {
                            if (con.estaConectada()) {
                                con.cierraConexion();
                            }
                        } catch (SQLException ex) {
                            Logger.getLogger("cargaTraspaso.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                        }
                    }
                %>

                <table id="tbAbastos" class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="h2 text-center" colspan="8">Abasto</th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>Clave</th>
                            <th>Lote</th>
                            <th>Caducidad</th>
                            <th>Cantidad</th>
                            <th>Origen</th>
                            <th>Observación</th>
                            <th>Editar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            try {
                                con.conectar();
                                ResultSet rs = con.consulta("Select clave,lote,caducidad,cantidad,origen,cb,id,cargado,observacion from carga_abasto");
                                String estadoBoton, icono, accion, target;
                                ;
                                while (rs.next()) {
                                    estadoBoton = "btn-danger";
                                    icono = "glyphicon-edit";
                                    accion = String.format("EditarAb(%s)", rs.getString(7));
                                    target = "#EditarAbasto";

                                    String caducado = "";

                                    LeerCSV.EstadoAbasto estado;
                                    estado = LeerCSV.EstadoAbasto.fromInt(rs.getInt(8));
                                    if (estado == LeerCSV.EstadoAbasto.NO_CARGO) {
                                        caducado = "class='danger text-danger'";
                                        icono = "glyphicon glyphicon-warning-sign";
                                        estadoBoton = "btn-danger";
                                        accion = "";
                                        target = "";
                                    }
                        %>
                        <tr id="f_<%=rs.getString(7)%>" <%=caducado%> >
                            <td id="id_<%=rs.getString(7)%>"><%=rs.getString(7)%></td>
                            <td id="clave_<%=rs.getString(7)%>"><%=rs.getString(1)%></td>
                            <td id="lote_<%=rs.getString(7)%>"><%=rs.getString(2)%></td>
                            <td id="caducidad_<%=rs.getString(7)%>"><%=rs.getString(3)%></td>
                            <td id="cantidad_<%=rs.getString(7)%>"><%=rs.getString(4)%></td>
                            <td id="origen_<%=rs.getString(7)%>"><%=rs.getString(5)%></td>
                            <td id="observacion_<%=rs.getString(7)%>"><%=rs.getString(9)%></td>
                            <td><a class="btn btn-block <%=estadoBoton%>" id="Editar" onclick="<%=accion%>" data-toggle="modal" data-target=<%=target%>><span class="glyphicon <%=icono%>"></span></a></td>
                        </tr>
                        <%
                                }
                            } catch (SQLException ex) {
                                Logger.getLogger("cargaTraspaso.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                            } finally {
                                try {
                                    if (con.estaConectada()) {
                                        con.cierraConexion();
                                    }
                                } catch (SQLException ex) {
                                    Logger.getLogger("cargaTraspaso.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                }
                            }
                        %>
                    </tbody>
                </table>
                <center> <button type="button" class="btn btn-success btn-lg" id="ValidarAbasto" onclick="confirmaValidacion()">Validar Abasto</button></center>
            </div>
            <div class="progress hidden" id="loadProg"></div>
        </div>

        <br>
        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <img src="../imagenes/gobierno.png" width=100 alt="Logo">
            </div>
        </div>

        <div id="EditarAbasto" class="modal fade in" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <a data-dismiss="modal" class="close">×</a>
                        <h3>Editar Producto</h3>
                    </div>
                    <div class="modal-body">
                        <div class="panel panel-body">
                            <form name="editarProducto">
                                <table id="tbEditaClave">
                                    <%
                                        String clave = "", lote = "", caduc = "", cant = "", ori = "", cb = "", id = "";
                                        try {
                                            con.conectar();
                                            ResultSet rs = con.consulta("Select clave,lote,caducidad,cantidad,origen,cb,id from carga_abasto where id='" + session.getAttribute("id") + "'");
                                            if (rs.next()) {
                                                clave = rs.getString(1);
                                                lote = rs.getString(2);
                                                caduc = rs.getString(3);
                                                cant = rs.getString(4);
                                                ori = rs.getString(5);
                                                cb = rs.getString(6);
                                                id = rs.getString(7);

                                            }
                                        } catch (SQLException ex) {
                                            Logger.getLogger("cargaTraspaso.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                        } finally {
                                            try {
                                                if (con.estaConectada()) {
                                                    con.cierraConexion();
                                                }
                                            } catch (SQLException ex) {
                                                Logger.getLogger("cargaTraspaso.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                                            }
                                        }
                                    %>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <input required type="hidden" id="hId" value="<%=id%>">
                                                <label class="col-sm-2">Clave</label>
                                                <div class="col-sm-2">
                                                    <input required class="form-control" name="clave" id="clave" type="text" disabled value="<%=clave%>">
                                                </div>
                                                <label class="col-sm-1">Lote</label>
                                                <div class="col-sm-2">
                                                    <input required class="form-control" name="lote" id="lote" type="text" value="<%=lote%>" onchange="reporteCambio('lote')">
                                                </div>
                                                <label class="col-sm-2">Caducidad</label>
                                                <div class="col-sm-3">
                                                    <input required class="form-control" name="caducidad" id="caducidad" type="date" value="<%=caduc%>" onchange="reporteCambio('caducidad')">
                                                </div>
                                            </div><br/> 
                                            <div class="row">
                                                <label class="col-sm-2">Cantidad</label>
                                                <div class="col-sm-2">
                                                    <input class="form-control" name="cantidad" id="cantidad" type="number" min="1" value="<%=cant%>" onchange="reporteCambio('cantidad')">
                                                </div>
                                                <label class="col-sm-1">Origen</label>
                                                <div class="col-sm-2">
                                                    <input readonly class="form-control" name="ori" id="ori" type="text" value="<%=ori%>">
                                                </div>
                                            </div><br/> 
                                            <div class="row">
                                                <label class="col-sm-2">Justificación</label>
                                                <div class="col-sm-12">
                                                    <input class="form-control" name="justi" id="justi" type="text" placeholder="Introduzca las razones de sus cambios.">
                                                </div>
                                            </div><br/> 
                                            <div class="row"></div>
                                        </td>
                                    </tr>
                                </table>                            
                            </form>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success" id="Guardar" name="Guardar" value="Guardar" onclick="guardarEditar()" >Guardar</button>
                                <a href="#" data-dismiss="modal" class="btn btn-success">Cerrar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                
        </div>
        <!-- 
================================================== -->
        <!-- Se coloca al final del documento para que cargue mas rapido -->
        <!-- Se debe de seguir ese orden al momento de llamar los JS -->
        <script src="../js/jquery.js" ></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/jquery-ui.js"></script>
        <script src="../js/jquery.progressTimer.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/dataTables.bootstrap.js"></script>

        <script type="text/javascript">
                                    var cambiosCampos = new Array();
                                    var valoresCampos = new Array();

                                    $(document).ready(function () {
                                        $('#imgCarga').toggle();
                                        $('#tbAbastos').dataTable();

                                    });

                                    function reporteCambio(campo) {
                                        var index = cambiosCampos.indexOf(campo);
                                        if (index === -1) {
                                            cambiosCampos.push(campo);
                                            valoresCampos.push($("#" + campo).val());
                                        } else {
                                            valoresCampos[index] = $("#" + campo).val();
                                        }
                                    }

                                    function funcionCarga() {
                                        $('#btnCarga').attr('disabled', true);
                                        $('#imgCarga').toggle();
                                    }

                                    function EditarAb(id) {
                                        var dir = "../EditaAbasto";
                                        cambiosCampos = new Array();
                                        valoresCampos = new Array();
                                        $.ajax({
                                            url: dir,
                                            data: {accion: "Ver", id: id},
                                            success: function (data) {
                                                $('#tbEditaClave').load('cargaTraspaso.jsp #tbEditaClave');
                                            },
                                            error: function () {
                                                alert("Ocurrió un error");
                                            }
                                        });
                                    }

                                    function guardarEditar() {
                                        var cant = $('#cantidad').val();
                                        var id = $('#hId').val();
                                        var justi = $('#justi').val();

                                        if (cambiosCampos.length === 0) {
                                            alert("No se ha realizado ningún cambio, no puede guardarse los cambios.");
                                            return false;
                                        }

                                        if ($('#lote').val() === "" || $('#caducidad').val() === "" ||
                                                cant === "" || $('#ori').val() === "" || justi === "") {

                                            alert("Hay campos vacios, por favor verifique");
                                            return false;
                                        }

                                        var dir = "../EditaAbasto";
                                        $.ajax({
                                            url: dir,
                                            data: {accion: "Editar", id: id, clave: $('#clave').val(), lote: $('#lote').val(),
                                                caduc: $('#caducidad').val(), cant: cant, ori: $('#ori').val(),
                                                observacion: justi, cambios: cambiosCampos, valores: valoresCampos},
                                            success: function (data) {
                                                var json = JSON.parse(data);
                                                if (json.msg !== "Actualización existosa.") {
                                                    alert(json.msg);
                                                } else {
                                                    alert(json.msg);
                                                    $('#id_' + id).val(id);
                                                    $('#clave_' + id).val(json.clave);
                                                    $('#lote_' + id).val(json.lote);
                                                    $('#caducidad_' + id).val(json.caducidad);
                                                    $('#cantidad_' + id).val(json.cantidad);
                                                    $('#origen_' + id).val(json.origen);
                                                    $('#observacion_' + id).val(json.observacion);

                                                    //$('#EditarAbasto').click();
                                                    location.reload();
                                                }
                                            },
                                            error: function (request, status, error) {
                                                console.log(request, status, error);
                                                alert("Ocurrió un error");
                                            }
                                        });
                                    }

                                    function confirmaValidacion() {
                                        if (confirm("Desea cargar el traspaso?")) {
                                            var dir = "../EditaAbasto";
                                            $('#ValidarAbasto').attr("disabled", "disabled");
                                            $("#loadProg").removeClass("hidden");
                                            var progress = $("#loadProg").progressTimer({
                                                timeLimit: 150,
                                                onFinish: function () {
                                                }
                                            });
                                            $.ajax({
                                                url: dir,
                                                data: {accion: "ValidarTraspaso"},
                                                success: function (data) {
                                                    progress.progressTimer('complete');
                                                    alert(data);
                                                    window.location.reload();
                                                },
                                                error: function () {
                                                    alert("Ocurrió un error");
                                                    $("#loadProg").addClass()("hidden");
                                                    $('#ValidarAbasto').attr("disabled", "");
                                                }
                                            });
                                        }
                                    }

                                    /*$('#ValidarAbasto').click(function () {
                                     var dir = "../EditaAbasto";
                                     $('#ValidarAbasto').attr("disabled", "disabled");
                                     $("#loadProg").removeClass("hidden");
                                     var progress = $("#loadProg").progressTimer({
                                     timeLimit: 150,
                                     onFinish: function () {
                                     }
                                     });
                                     $.ajax({
                                     url: dir,
                                     data: {accion: "ValidarTraspaso"},
                                     success: function (data) {
                                     progress.progressTimer('complete');
                                     alert(data);
                                     window.location.reload();
                                     },
                                     error: function () {
                                     alert("Ocurrió un error");
                                     $("#loadProg").addClass()("hidden");
                                     $('#ValidarAbasto').attr("disabled", "");
                                     }
                                     });
                                     }
                                     );*/

        </script>
        <br/>
    </body>
</html>


