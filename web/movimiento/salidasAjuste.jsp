<%-- 
    Document   : salidaAjuste
    Created on : 23/02/2015, 05:09:23 PM
    Author     : GNK
--%>
<%@page import="Servlets.Catalogo"%>
<%@page import="Servlets.movientos.MovimentosProductos"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>
<%
    HttpSession sesion = request.getSession();

    String id_usu = (String) session.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }

%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Estilos CSS -->
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/cupertino/jquery-ui-1.10.3.custom.css" />
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/sweetalert.css" rel="stylesheet" type="text/css"/>
        <!---->
        <title>Salidas por Ajuste</title>
    </head>
    <body>
        <%@include file="../jspf/mainMenu.jspf" %>%>
        <div class="container" style="padding-top: 50px">
            <h1>Salidas por Ajuste (-)</h1>
            <hr/>
            <div class="row">
                <div class="col-sm-2">
                    <input class="form-control" placeholder="Código de Barras" name="cb" id="cb" data-behavior = "only-alphanum"/>
                </div>
                <div class="col-sm-2">
                    <input class="form-control" placeholder="Clave" name="cla_pro" id="claveBusqueda" data-behavior = "only-alphanum"/>
                </div>
                <div class="col-sm-1">
                    <button class="btn btn-success btn-block" name="accion" value="buscarInsumo" onclick="buscar()"><span class="glyphicon glyphicon-search"></span></button>
                </div>
            </div>
            <hr/>
            <div id="seleccion" hidden>
                <div class="row">
                    <h4 class="col-sm-12">
                        Clave: <input class="form-control" readonly placeholder="Clave" id="claveProd" name="cla_pro" />
                    </h4> 

                    <h4 class="col-sm-12">
                        Descripción: <input class="form-control"readonly placeholder="Descripción" id="descPro" name="descPro" />
                    </h4> 
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <select class="form-control" name="lot_pro" id="lot_pro">
                            <option value="" disabled>--Seleccione el Lote--</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" name="cad_pro" id="cad_pro">
                            <option value="" disabled>--Seleccione la Caducidad --</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" name="id_ori" id="id_ori">
                            <option value="" disabled>--Seleccione el Origen--</option>
                        </select>
                    </div>
                    <div class="col-sm-1">
                        <button class="btn btn-success btn-block" name="accion" value="buscarLote" onclick="seleccionar()"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
                </div>
            </div>
            <div id="ajuste" hidden>
                <hr/>
                <div class="row">
                    <h4 class="col-sm-4" id="loteLabel">Lote:</h4>
                    <h4 class="col-sm-4" id="caducidadLabel">Caducidad:</h4>
                    <h4 class="col-sm-4" id="origenLabel">Origen:</h4>
                </div>

                <div class="row">
                    <h4 class="col-sm-2">Cantidad Existente</h4>
                    <div class="col-sm-2">
                        <input class="form-control" id="cantidadExistente" readonly />
                    </div>
                </div>

                <form id="formAplicar"  data-async data-target="#rating-modal" action="javascript:void(0);" method="POST">
                    <input class="hidden" name="id_inv" id="id_inv" >
                    <div class="row">
                        <h4 class="col-sm-2">Cantidad del ajuste</h4>
                        <div class="col-sm-2">
                            <input class="form-control" type="number" name="cantAjuste"  min="1" required id="cantAjuste"/>
                        </div>
                    </div>
                    <textarea class="form-control" rows="5" required placeholder="Justificación" name="obs" id="obs"></textarea>
                    <br/>
                    <button class="btn btn-block btn-success" name="accion" value="salidaAjuste" onclick="validarDatos()">Ajustar(-)</button>
                </form>

            </div>
            <div id="sinOrigenes" hidden>
                <hr/>
                <div class="row">
                    <h4 class="col-sm-12">
                        Clave: <input class="form-control" readonly placeholder="Clave" id="claveProdRO" name="cla_pro" />
                    </h4> 

                    <h4 class="col-sm-12">
                        Descripción: <input class="form-control"readonly placeholder="Descripción" id="descProRO" name="cla_pro" />
                    </h4> 
                </div>
                <div class="row">
                    <h3 class="col-sm-12">Clave sin abasto</h3>
                </div>

            </div>
            <div id="sinExistencia" hidden>
                <hr/>
                <div class="row">
                    <h4 class="col-sm-4" id="loteRO2">Lote:</h4>
                    <h4 class="col-sm-4" id="caducidadRO2">Caducidad:</h4>
                    <h4 class="col-sm-4" id="origenRO2">Origen:</h4>
                </div>
                <div class="row">
                    <h3 class="col-sm-12">Lote sin existencia</h3>
                </div>
            </div>
        </div>
        <div class="modal fade" id="Observaciones" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog ">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="row">
                            <div class="col-sm-5">
                                Aplicar Salida
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <h4 class="modal-title" id="myModalLabel">Contraseña</h4>
                        <div class="row">
                            <div class="col-sm-12">
                                <input id="confirmarPass" class="form-control" type="password" onkeyup="" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="btnPassCancela" type="button" class="btn btn-danger" onclick="validaContra()" >Ajustar(-)</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery-ui.js"></script>
        <script src="../js/jquery.alphanum.js" type="text/javascript"></script>
        <script src="../js/sweetalert.min.js" type="text/javascript"></script>
        <script type="text/javascript">
                                var lotesDisponibles;

                                function validaContra() {
                                    var password = $("#confirmarPass").val();
                                    $.ajax({
                                        type: "POST",
                                        url: "../Farmacias",
                                        dataType: "json",
                                        data: {accion: "verificar", pass: password},
                                        success: function (data) {
                                            if (data.permitido) {
                                                aplicar();
                                            } else {
                                                $("#confirmarPass").val("");

                                                swal({
                                                    title: "Validacion contraseña",
                                                    text: "Datos incorrectos, por favor verique la defaultrmación ingresada.",
                                                    type: "Error",
                                                    showCancelButton: false,
                                                    confirmButtonClass: "btn-default",
                                                    confirmButtonText: "Continuar"
                                                });
                                            }
                                        },
                                        error: function (jqXHR, textStatus, errorThrown) {
                                            swal({
                                                title: "Validacion contraseña",
                                                text: jqXHR.responseText,
                                                type: "error",
                                                showCancelButton: false,
                                                confirmButtonClass: "btn-default",
                                                confirmButtonText: "Continuar"
                                            });
                                        }
                                    }
                                    );
                                }

                                function buscar() {
                                    $("#sinExistencia").hide();
                                    $("#seleccion").hide();
                                    $("#ajuste").hide();
                                    $('#lot_pro').empty();
                                    $('#cad_pro').empty();
                                    $('#id_ori').empty();
                                    $('#lot_pro').append(new Option("--Seleccione el Lote--", 0));
                                    $('#cad_pro').append(new Option("--Seleccione la Caducidad --", 0));
                                    $('#id_ori').append(new Option("--Seleccione el Origen--", 0));

                                    var cb, clave, patron;
                                    cb = $('#cb').val();
                                    if (cb !== "") {
                                        patron = cb;
                                    } else {
                                        clave = $('#claveBusqueda').val();
                                        if (clave !== "") {
                                            patron = clave;
                                        }
                                    }


                                    $.ajax({
                                        type: "GET",
                                        url: "../mover",
                                        data: {"accion": "<%=MovimentosProductos.BUSQUEDA_PRODUCTO%>",
                                            "busqueda": patron},
                                        dataType: "json",
                                        success: function (data) {
                                            console.log("busqueda", data);
                                            lotesDisponibles = data;
                                            $("#seleccion").show();

                                            $("#claveProd").val(data[0].clave);
                                            $("#descPro").val(data[0].descripcion);
                                            var aux;
                                            for (var i = 0; i < data.length; i++) {
                                                aux = data[i];
                                                $('#lot_pro').append(new Option(aux.lote, i));
                                                $('#cad_pro').append(new Option(aux.caducidad, i));
                                                $('#id_ori').append(new Option(aux.origen_descripcion, i));
                                            }
                                            $('#cb').val("");
                                            $('#claveBusqueda').val("");
                                        },
                                        error: function (jqXHR, textStatus, errorThrown) {
                                            swal({
                                                title: "Buscar producto",
                                                text: jqXHR.responseText,
                                                type: "error",
                                                showCancelButton: false,
                                                confirmButtonClass: "btn-default",
                                                confirmButtonText: "Continuar"
                                            });
                                        }
                                    });
                                }

                                function seleccionar() {
                                    $("#sinExistencia").hide();
                                    $("#ajuste").show();
                                    var data = lotesDisponibles[parseInt($('#lot_pro').val())];

                                    $('#cantidadExistente').val(data.cantidad);
                                    $('#id_inv').val(data.detalle_producto); //Es necesario para ajustar la cantidad.
                                    $("#cantAjuste").attr({
                                        "max": data.cantidad
                                    });
                                    $("#loteLabel").text("Lote: " + data.lote);
                                    $("#caducidadLabel").text("Caducidad: " + data.caducidad);
                                    $("#origenLabel").text("Origen: " + data.origen);
                                }

                                $('#lot_pro').on('change', function () {
                                    document.getElementById('cad_pro').selectedIndex = this.selectedIndex;
                                    document.getElementById('id_ori').selectedIndex = this.selectedIndex;
                                });

                                $('#cad_pro').on('change', function () {
                                    document.getElementById('lot_pro').selectedIndex = this.selectedIndex;
                                    document.getElementById('id_ori').selectedIndex = this.selectedIndex;
                                });

                                $('#id_ori').on('change', function () {
                                    document.getElementById('lot_pro').selectedIndex = this.selectedIndex;
                                    document.getElementById('cad_pro').selectedIndex = this.selectedIndex;
                                });

                                function validarDatos() {

                                    var id_inv, obs;
                                    var cantAjuste = 0, cantidadExistente = 0;
                                    id_inv = $('#id_inv').val();
                                    cantidadExistente = parseInt($('#cantidadExistente').val());
                                    obs = $('#obs').val();
                                    cantAjuste = parseInt($('#cantAjuste').val());



                                    if (cantAjuste > 0 && obs !== "" && cantAjuste <= cantidadExistente) {
                                        $('#Observaciones').modal('toggle');
                                    }
                                }

                                function aplicar() {
                                    var detalleProducto, obs, cantAjuste, cantidadExistente;
                                    detalleProducto = $('#id_inv').val();
                                    cantidadExistente = parseInt($('#cantidadExistente').val());
                                    obs = $('#obs').val();
                                    cantAjuste = parseInt($('#cantAjuste').val());
                                    if (cantAjuste > 0 && obs !== "" & cantAjuste <= cantidadExistente) {
                                        var r = confirm("¿Desea realizar el ajuste?");
                                        if (r) {
                                            var $form = $(this);
                                            $.ajax({
                                                type: $form.attr('method'),
                                                url: "../mover",
                                                data: {"accion": "<%=MovimentosProductos.AJUSTE_SALIDA%>",
                                                    "detalle": detalleProducto, "cantidad": cantAjuste, "observaciones": obs},
                                                dataType: "json",
                                                success: function (data) {
                                                    console.log("ajuste", data);
                                                    swal({
                                                        title: "Salida por ajuste",
                                                        text: "Ajuste exitoso. Nueva cantidad: " + data.nuevo,
                                                        type: "success",
                                                        showCancelButton: false,
                                                        confirmButtonClass: "btn-success",
                                                        confirmButtonText: "Continuar"
                                                    }, function () {
                                                        location.reload();
                                                    });

                                                },
                                                error: function (jqXHR, textStatus, errorThrown) {
                                                    swal({
                                                        title: "Salida por ajuste",
                                                        text: jqXHR.responseText,
                                                        type: "error",
                                                        showCancelButton: false,
                                                        confirmButtonClass: "btn-default",
                                                        confirmButtonText: "Continuar"
                                                    });
                                                }
                                            });
                                        }
                                    }
                                }


                                $('#lot_pro').on('change', function () {
                                    document.getElementById('cad_pro').selectedIndex = this.selectedIndex;
                                });
                                $('#cad_pro').on('change', function () {
                                    document.getElementById('lot_pro').selectedIndex = this.selectedIndex;
                                });

                                $("[data-behavior~=only-alphanum]").alphanum({
                                    allowSpace: false,
                                    allowOtherCharSets: false,
                                    allow: '.'
                                });
                                $("[data-behavior~=only-num]").numeric({
                                    allowMinus: false,
                                    allowThouSep: false
                                });


                                $("[data-behavior~=only-alpha]").alphanum({
                                    allowNumeric: false,
                                    allowSpace: false,
                                    allowOtherCharSets: true
                                });

                                $.ajax({
                                    url: "${pageContext.servletContext.contextPath}/Catalogo",
                                    data: {ban: <%=Catalogo.OBTENER_CATALAGO_PRODUCTOS%>},
                                    type: 'POST',
                                    async: true,
                                    dataType: 'json',
                                    success: function (data)
                                    {
                                        var claves_productos = [];

                                        data.forEach(function (value, id, arr) {
                                            claves_productos.push(value.id);
                                        });

                                        $("#claveBusqueda").autocomplete({
                                            source: claves_productos
                                        });
                                    }, error: function (jqXHR, textStatus, errorThrown) {

                                        alert("Error Contactar al departamento de sistemas");

                                    }
                                });
        </script>



    </body>
</html>
