<%-- 
    Document   : salidaAjuste
    Created on : 23/02/2015, 05:09:23 PM
    Author     : SEBASTIAN
--%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="Servlets.Catalogo"%>
<%@page import="Servlets.movientos.MovimentosProductos"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Clases.ConectionDB"%>
<%
    HttpSession sesion = request.getSession();

    String id_usu = (String) session.getAttribute("id_usu");

    if (id_usu == null) {
        response.sendRedirect("../index.jsp");
        return;
    }
    ConectionDB con = new ConectionDB();
    con.conectar();
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Estilos CSS -->
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="../css/cupertino/jquery-ui-1.10.3.custom.css" />
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/sweetalert.css" rel="stylesheet" type="text/css"/>
        <script>
            var listaApartados = [];
        </script>
        <!---->
        <title>Salidas por Ajuste</title>
    </head>
    <body>
        <div hidden="">
            <input id="cla_uni" value="<%=sesion.getAttribute("cla_uni")%>">
        </div>
        <%@include file="../jspf/mainMenu.jspf" %>%>
        <div class="container" style="padding-top: 50px">
            <h1>Salidas por Traspaso (-)</h1>
            <hr/>
            <div class="row">
                <h4 class="col-sm-2">
                    Unidad Destino:
                </h4>
                <h4 class="col-sm-5">
                    <select class="form-control" name="listaUnidades" id="listaUnidades" data-toggle="tooltip" title="Lista de Unidades" required>
                        <option value="">--Unidad--</option>
                        <%
                            try {
                                con.conectar();
                                ResultSet rset = con.consulta("select cla_uni, des_uni from unidades");
                                while (rset.next()) {
                        %>
                        <option value="<%=rset.getString("cla_uni")%>"><%=rset.getString("des_uni")%></option>
                        <%
                                }
                            } catch (SQLException ex) {
                                Logger.getLogger("receta_colectiva.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.
                                        getSQLState()), ex);
                            } finally {
                                try {
                                    if (con.estaConectada()) {
                                        con.cierraConexion();
                                    }
                                } catch (SQLException ex) {
                                    Logger.getLogger("receta_colectiva.jsp").log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.
                                            getSQLState()), ex);
                                }
                            }
                        %>
                    </select>
                </h4>
                <h4>
                    <div class="col-sm-2">
                        <input class="form-control" placeholder="Código de Barras" name="cb" id="cb" data-behavior = "only-alphanum"/>
                    </div>
                    <div class="col-sm-2">
                        <input class="form-control" placeholder="Clave" name="cla_pro" id="claveBusqueda" data-behavior = "only-alphanum"/>
                    </div>
                    <div class="col-sm-1">
                        <button class="btn btn-success btn-block" name="accion" value="buscarInsumo" onclick="buscar();"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
                </h4>
            </div>
            <hr/>
            <div id="seleccion" hidden>
                <div class="row">
                    <h4 class="col-sm-12">
                        Clave: <input class="form-control" readonly placeholder="Clave" id="claveProd" name="cla_pro" />
                    </h4> 

                    <h4 class="col-sm-12">
                        Descripción: <input class="form-control"readonly placeholder="Descripción" id="descPro" name="descPro" />
                    </h4> 
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <select class="form-control" name="id_ori" id="id_ori">
                            <option value="" disabled>--Seleccione el Origen--</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" name="lot_pro" id="lot_pro">
                            <option value="" disabled>--Seleccione el Lote--</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" name="cad_pro" id="cad_pro">
                            <option value="" disabled>--Seleccione la Caducidad --</option>
                        </select>
                    </div>
                    <div class="col-sm-1">
                        <button class="btn btn-success btn-block" name="accion" value="buscarLote" onclick="seleccionar()"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
                </div>
            </div>
            <div id="ajuste" hidden>
                <hr/>
                <div class="row">
                    <h4 class="col-sm-4" id="loteLabel">Lote:</h4>
                    <h4 class="col-sm-4" id="caducidadLabel">Caducidad:</h4>
                    <h4 class="col-sm-4" id="origenLabel">Origen:</h4>
                </div>

                <div class="row">
                    <h4 class="col-sm-2">Cantidad Existente</h4>
                    <div class="col-sm-2">
                        <input class="form-control" id="cantidadExistente" readonly />
                    </div>
                </div>
                <div class="row">
                    <h4 class="col-sm-2">Cantidad del ajuste</h4>
                    <div class="col-sm-2">
                        <input class="hidden" name="id_inv" id="id_inv" >
                        <input class="form-control" type="number" name="cantAjuste"  min="1" required id="cantAjuste"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <button class="btn btn-block btn-success" name="accion" value="apartarInsumo" onclick="apartarInsumo()">Agregar</button>
                    </div>
                </div>
            </div>
            <div id="Registros">
                <hr/>
                <div class="row" id="tablaInsumoApartado">

                    <div class="col-sm-12">
                        <table class="table table-bordered table-condensed table-responsive table-striped" id="existencias">
                            <thead>
                                <tr>
                                    <td>Clave</td>
                                    <!--td>CB</td-->
                                    <td>Descripción</td>
                                    <td>Lote</td>
                                    <td>Caducidad</td>
                                    <td>Cantidad</td>
                                    <td>Remover</td>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    try {
                                        con.conectar();
                                        ResultSet rset = con.consulta(
                                                "SELECT productos.cla_pro, productos.des_pro, detalle_productos.lot_pro, detalle_productos.cad_pro, apartamiento.cantidad, apartamiento.id "
                                                + "FROM apartamiento, detalle_productos, productos " + "WHERE productos.cla_pro = detalle_productos.cla_pro "
                                                + "AND apartamiento.detalle_producto = detalle_productos.det_pro " + "AND apartamiento.status='1' "
                                                + "GROUP BY apartamiento.id");
                                        while (rset.next()) {
                                %>
                            <script>
                                listaApartados.push(<%=rset.getString(6)%>);
                            </script>
                            <tr>
                                <td><%=rset.getString(1)%></td>
                                <!--td>CB</td-->
                                <td><%=rset.getString(2)%></td>
                                <td><%=rset.getString(3)%></td>
                                <td><%=rset.getString(4)%></td>
                                <td><%=rset.getString(5)%></td>
                                <td>
                                    <button class="btn btn-block btn-success btn-danger" value="<%=rset.getString(6)%>" id="<%=rset.getString(6)%>" onclick="removerInsumoApartado(this);"><span class="glyphicon glyphicon-remove"></span></button>
                                </td>
                            </tr>
                            <%   }
                                    con.cierraConexion();
                                } catch (Exception e) {
                                    out.println(e);
                                }
                            %>
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr/>
                <form id="formAplicar"  data-async data-target="#rating-modal" action="javascript:void(0);" method="POST">

                    <textarea class="form-control" rows="5" required placeholder="Justificación" name="obs" id="obs"></textarea>
                    <br/>
                    <button class="btn btn-block btn-success" name="accion" value="salidaAjuste" onclick="validarDatos()">Ajustar(-)</button>
                </form>

            </div>
            <div id="sinOrigenes" hidden>
                <hr/>
                <div class="row">
                    <h4 class="col-sm-12">
                        Clave: <input class="form-control" readonly placeholder="Clave" id="claveProdRO" name="cla_pro" />
                    </h4> 

                    <h4 class="col-sm-12">
                        Descripción: <input class="form-control"readonly placeholder="Descripción" id="descProRO" name="cla_pro" />
                    </h4> 
                </div>
                <div class="row">
                    <h3 class="col-sm-12">Clave sin abasto</h3>
                </div>

            </div>
            <div id="sinExistencia" hidden>
                <hr/>
                <div class="row">
                    <h4 class="col-sm-4" id="loteRO2">Lote:</h4>
                    <h4 class="col-sm-4" id="caducidadRO2">Caducidad:</h4>
                    <h4 class="col-sm-4" id="origenRO2">Origen:</h4>
                </div>
                <div class="row">
                    <h3 class="col-sm-12">Lote sin existencia</h3>
                </div>
            </div>
        </div>
        <div class="modal fade" id="Observaciones" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
            <div class="modal-dialog ">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="row">
                            <div class="col-sm-5">
                                Aplicar Salida
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <h4 class="modal-title" id="myModalLabel">Contraseña</h4>
                        <div class="row">
                            <div class="col-sm-12">
                                <input id="confirmarPass" class="form-control" type="password" onkeyup="" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="btnPassCancela" type="button" class="btn btn-danger" onclick="validaContra()" >Ajustar(-)</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="../js/jquery-1.9.1.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery-ui.js"></script>
        <script src="../js/jquery.alphanum.js" type="text/javascript"></script>
        <script src="../js/sweetalert.min.js" type="text/javascript"></script>
        <script>
                                var context = "${pageContext.servletContext.contextPath}";
                                var OBTENER_CATALAGO_PRODUCTOS = "<%=Catalogo.OBTENER_CATALAGO_PRODUCTOS%>";
                                var BUSQUEDA_PRODUCTO = "<%=MovimentosProductos.BUSQUEDA_PRODUCTO%>";
                                var TRASPASO_SALIDA = "<%=MovimentosProductos.TRASPASO_SALIDA%>";
                                var APARTADO_SALIDA = "<%=MovimentosProductos.APARTADO_SALIDA%>";
                                var QUITAR_APARTADO_SALIDA = "<%=MovimentosProductos.QUITAR_APARTADO_SALIDA%>";
                                var CLAVE_UNIDAD = "<%=(String) sesion.getAttribute("cla_uni")%>";
        </script>
        <script src="../js/movimientos/salidasTraspaso.js" type="text/javascript"></script>

    </body>
</html>
