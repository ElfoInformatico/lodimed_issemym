<%-- 
    Document   : index
    Created on : 18-feb-2014, 13:00:45
    Author     : wence
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--
Menú Principal
-->

<%
    HttpSession sesion = request.getSession();

    sesion.invalidate();
%>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>LODIMED</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/isem.css" rel="stylesheet" type="text/css"/>


        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-danger.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <!-- Fixed navbar -->
        <div class="navbar navbar-inverse navbar-fixed-top" style="background-color: black; color: white" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" ><b>Bienvenidos</b></a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#"><i class="fa fa-home"></i> Inicio</a></li>
                        <li><a data-toggle="modal" href="#myModal2"><i class="fa fa-info"></i> Acerca de...</a></li>
                        <li><a data-toggle="modal" href="#myModal"><i class="fa fa-question"></i> Cont&aacute;ctanos</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>

        <div class="container">

            <!-- Main component for a success marketing message or call to action -->
            <div class="jumbotron" style="background-color: white">
                <br>
                <div hidden class="row">
                    <div class="col-md-12 text-center "><h1 id="2">LODIMED RECETA M&Eacute;DICO</h1><h1 id="1" >LODIMED RECETA FARMACIA</h1><h1 id="3" >LODIMED RECETA HL7</h1></div>

                </div>
                <div class="row">
                    <img class="col-md-12 text-center" src="imagenes/logo_app.png" alt=""/>
                </div>
                <br>
                <p class="text-center" >Escoja una de las opciones que se muestran a continuaci&oacute;n:</p>
                <p class="text-center" >
                    <a class="btn btn-lg btn-success" id="medicoRec" href="login.jsp" role="button">Generaci&oacute;n de Recetas</a>
                    <a class="btn btn-lg btn-success" id="farmaciaRec" href="login.jsp" role="button">Farmacia</a>
                    <a class="btn btn-lg btn-success" id="hl7Rec" href="login.jsp" role="button">Farmacia HL7</a>
                    <a class="btn btn-lg btn-success" href="login.jsp" role="button">Administraci&oacute;n de Usuarios</a>
                </p>
                <p class="text-right">V1.0.0</p>

            </div>

            <!--
            Envío de Correo
            -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Ingresa tus Datos</h4>
                        </div>
                        <div class="modal-body">
                            <form name="form_com" method="post" id="form_com">
                                Nombre: <input type="text" class="form-control" data-behavior="only-alphanum" autofocus placeholder="Ingrese su nombre" name="txtf_nom" id="txtf_nom">
                                Teléfono: <input type="text" class="form-control" data-behavior="only-number" autofocus placeholder="Ingrese su télefono" name="txtf_tel" id="txtf_tel">
                                Cuenta de Correo: <input type="text" class="form-control" data-behavior="only-alphanum" placeholder="Ingrese su cuenta de correo" name="txtf_cor" id="txtf_cor" onblur="validarEmail(this.form.txtf_cor.value);" >
                                Deje su Comentario: <textarea name="txta_com" cols="10" rows="5" class="form-control" data-behavior="only-alphanum" id="txta_com"></textarea>
                                <div class="modal-footer">
                                    <div id="contenidoAjax">
                                        <p></p>
                                    </div>
                                    <input type="submit" class="btn btn-success" value="Enviar" id="btn_com"  onClick="return verificaCom(document.forms.form_com);">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                </div>

                            </form>
                        </div>

                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <!--
            Fin envío de correo
            -->

            <!-- 
          Mensaje de Acerca de...
            -->
            <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Desarrollo WEB</h4>
                        </div>
                        <div class="modal-body">
                            <br>
                            <img src="imagenes/GNKL_Small.JPG" >&Aacute;rea de Desarrollo y Aplcaciones WEB<br>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->


            <!-- 
             fin Mensaje de Acerca de...
            -->


        </div> <!-- /container -->
        <!-- Funciones con Ajax -->
        <script src="js/jquery-2.1.4.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.alphanum.js" type="text/javascript"></script>
        <script language="javascript" src="js/codeJs.js"></script>
        <script>
                                        $(document).ready(function () {

                                            $.ajax({
                                                url: "${pageContext.servletContext.contextPath}/Operaciones",
                                                data: {ban: 4},
                                                type: 'POST',
                                                async: true,
                                                success: function (data)
                                                {
                                                    var sis = data.sis;

                                                    if (sis === 1) {
                                                        $("#medicoRec").hide();
                                                        $("#hl7Rec").hide();
                                                        $("#1").show();
                                                        $("#2").hide();
                                                        $("#3").hide();
                                                    } else if (sis === 2) {
                                                        $("#medicoRec").show();
                                                        $("#hl7Rec").hide();
                                                        $("#1").hide();
                                                        $("#2").show();
                                                        $("#3").hide();
                                                    } else if (sis === 3) {
                                                        $("#medicoRec").hide();
                                                        $("#farmaciaRec").hide();
                                                        $("#1").hide();
                                                        $("#2").hide();
                                                        $("#3").show();
                                                    }else if (sis === 4) {
                                                        $("#medicoRec").hide();
                                                        $("#hl7Rec").hide();
                                                        $("#1").show();
                                                        $("#2").hide();
                                                        $("#3").hide();
                                                    }


                                                }, error: function (jqXHR, textStatus, errorThrown) {

                                                    alert("Error Contactar al departamento de sistemas");

                                                }
                                            });


                                            $('#form_login').submit(function () {
                                                alert("Introduzca credenciales válidas");
                                                return false;
                                            });


                                            $('#form_com').submit(function () {
                                                return false;
                                            });
                                            $("#btn_com").click(function () {

                                                var nom = $('#txtf_nom').val();
                                                var cor = $('#txtf_cor').val();
                                                var com = $('#txta_com').val();
                                                var tel = $('#txtf_tel').val();
                                                if (nom === '' || cor === '' || com === '' || tel === '') {
                                                    return false;
                                                } else {
                                                    var $contenidoAjax = $('div#contenidoAjax').html('<p><img src="imagenes/ajax-loader.gif" /></p>');
                                                    var dir = 'servletCorreo';
                                                    var userData = $.param({uas: navigator.userAgent});
                                                    $.ajax({
                                                        url: dir,
                                                        type: "POST",
                                                        data: {txtf_nom: nom, txtf_cor: cor, txta_com: com, txtf_tel: tel, esSoporte: false, navegador: userData},
                                                        success: function (data) {
                                                            $contenidoAjax.html(data);
                                                            alert("Sus datos han sido Enviados");
                                                            location.reload();
                                                        },
                                                        error: function (data) {
                                                            alert('Error');
                                                        }
                                                    });
                                                }
                                            });

                                        });

                                        $("[data-behavior~=only-alphanum]").alphanum({
                                            allowOtherCharSets: true,
                                            disallow: "'",
                                            allow: ".@"
                                        });

                                        $("[data-behavior~=only-number]").alphanum({
                                            allowLatin: false,
                                            allowOtherCharSets: false,
                                            disallow: "'",
                                            allow: "+*#"
                                        });

        </script>
    </body>
</html>
