package com.gnkl.services.hl7;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.sql.Connection;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Socket HL7
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class SocketUtils {

    private Socket socket;
    private final MessageHL7Service serviceHl7;
    private static final int MAX_ATTEPTS = 3;

    public SocketUtils(Connection con) {
        serviceHl7 = new MessageHL7Service(con);
    }

    public void sendDatatoSocket(String messageAck, Integer idrec) throws IOException, InterruptedException {

        try {

            Properties prop = new Properties();
            prop.load(getClass().getResourceAsStream("/com/gnkl/config/hl7/socket.properties"));

            InetSocketAddress address = new InetSocketAddress(prop.getProperty("host"), Integer.valueOf(prop.getProperty("port")));
            socket = new Socket();

            int attempts = 1;

            while (attempts <= MAX_ATTEPTS) {
                try {
                    socket.connect(address, 3000);
                    break;
                } catch (IOException e) {
                    socket.close();
                    socket = new Socket();
                    Thread.sleep(15000);
                    attempts++;
                    if (attempts > MAX_ATTEPTS) {
                        throw new IOException("Numero de intentos de conexion por socket superado", e);
                    }
                }
            }

            OutputStream os = socket.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            bw.write(messageAck);
            bw.flush();

            serviceHl7.createMensajeHl7(messageAck, 1, idrec, null);

        } catch (IOException | NumberFormatException ce) {
            Logger.getLogger(SocketUtils.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ce.getMessage(), ce.getClass()), ce);
            serviceHl7.createMensajeHl7(messageAck, 0, idrec, null);
            throw ce;
        } finally {
            //Closing the socket
            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (Exception e) {
                Logger.getLogger(SocketUtils.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", e.getMessage(), e.getClass()), e);
            }
        }
    }

}
