/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gnkl.services.hl7;

import Clases.ConectionDB;
import Dao.Hl7LogDao;
import Dao.MensajeHl7Dao;
import Impl.Hl7LogDaoImpl;
import Impl.MensajeHl7DaoImpl;
import com.google.gson.Gson;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import mx.gnk.hl7.bean.Hl7Message;
import mx.gnk.hl7.bean.MensajeHl7;
import mx.gnk.hl7.bean.PatientMsh;
import mx.gnk.hl7.bean.PatientOrc;
import mx.gnk.hl7.bean.PatientPid;
import mx.gnk.hl7.bean.PatientRxe;
import mx.gnk.hl7.bean.PatientVisit;
import mx.gnk.hl7.bean.RecetaHl7Log;
import org.apache.commons.codec.digest.DigestUtils;
import Impl.MedicoImpl;
import Impl.MovimientosImpl;
import Impl.PacienteDaoImpl;
import Impl.RecetaImpl;
import Modelos.DetalleCapturado;
import Modelos.DetalleReceta;
import Modelos.Medico;
import Modelos.MovimientoProducto;
import Modelos.Paciente;
import Modelos.Receta;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Servicio de Mensajería HL7
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class MessageHL7Service {

    static final char END_OF_BLOCK = '\u001c';
    static final char START_OF_BLOCK = '\u000b';
    static final char CARRIAGE_RETURN = 13;
    private final Hl7LogDao hl7RecetaLogDao;
    private final MensajeHl7Dao mensajeDao;

    public MessageHL7Service(Connection con) {
        this.hl7RecetaLogDao = new Hl7LogDaoImpl(con);
        this.mensajeDao = new MensajeHl7DaoImpl(con);
    }

    public String createHL7MessageRREO12(Hl7Message hl7message, String message, String tipo) {

        String messageId = "";
        String messageDate = "";
        String tipomensaje = tipo != null ? tipo : "AA";
        String unidadId = "N/A";

        if (hl7message != null) {
            PatientOrc orcpatient = hl7message.getPatientOrc();
            PatientMsh patientmsh = hl7message.getPatientMsh();
            PatientVisit patientvisit = hl7message.getPatientVisit();
            messageId = orcpatient.getIdReceta();
            messageDate = patientmsh.getFecha();
            if (patientmsh.getIdUnidadMedica() != null && !patientmsh.getIdUnidadMedica().isEmpty()) {
                unidadId = patientmsh.getIdUnidadMedica();
            } else if (patientvisit.getUnidadMedica() != null && !patientvisit.getUnidadMedica().isEmpty()) {
                unidadId = patientvisit.getUnidadMedica();
            }
        } else {
            messageId = DigestUtils.md5Hex(String.valueOf(new Date().getTime()));
            messageDate = new SimpleDateFormat("YYYYMMHH24mmSS").format(new Date());
        }

        String mshMessage = "MSH|^~\\&|EXT_SYS_RECETA|" + unidadId + "|ALERT|" + unidadId + "|" + messageDate + "||RRE^O12^RRE_O12|1|P|2.5|||AL|ER|";
        String msaMessage = "MSA|" + tipomensaje + "|" + messageId + "|" + message + "|";
        StringBuilder ackMessage = new StringBuilder();
        ackMessage.append(START_OF_BLOCK)
                .append(mshMessage)
                .append(CARRIAGE_RETURN)
                .append(msaMessage)
                .append(CARRIAGE_RETURN)
                .append(END_OF_BLOCK)
                .append(CARRIAGE_RETURN);

        return ackMessage.toString();
    }

    public List<String> createHL7MessageRDSO13(int idReceta, ConectionDB con, int idUsuario) throws SQLException {
        RecetaImpl daoRecetas = new RecetaImpl("%Y%m%d", con, idReceta, idUsuario);
        Receta receta = daoRecetas.reconstruirReceta(idReceta);

        MedicoImpl daoMedico = new MedicoImpl(con.getConn());
        Medico foundMedico = daoMedico.reconstruirMedico(receta.getIdMedico());

        List<String> rdso13MessageList = new ArrayList<>();

        String md5MessageId = DigestUtils.md5Hex(String.valueOf(new Date().getTime()));
        String recetaFecha = receta.getFecha();

        String mshMessage = "MSH|^~\\&|EXT_SYS_RECETA|" + foundMedico.getUnidad()
                + "|ALERT|" + foundMedico.getUnidad() + "|" + recetaFecha + "||RDS^O13^RDS_O13|"
                + md5MessageId + "|P|2.5|";

        String curpData = "";
        PacienteDaoImpl dao = new PacienteDaoImpl(con.getConn(), "%Y%m%d");
        Paciente paciente = dao.reconstruirPaciente(receta.getIdPaciente());

        String pidMessage = "PID|1||" + receta.getIdPaciente() + "^^^ADT^PN~"
                + curpData + "^^^SEGOB^NI||" + paciente.getApellidoPaterno() + "&"
                + paciente.getApellidoMaterno() + "^" + paciente.getNombreCompleto() + "^"
                + paciente.getNombre() + "||" + paciente.getNacimiento() + "|" + paciente.getSexo() + "|";

        String tipoConsulta = null;
        switch (receta.getTipoConsulta()) {
            case Modelos.Receta.URGENCIAS_TIPO_CONSULTA:
                tipoConsulta = "E";
                break;

            case Modelos.Receta.HOSPITALIZACION_TIPO_CONSULTA:
                tipoConsulta = "I";
                break;

            case Modelos.Receta.CONSULTA_TIPO_CONSULTA:
                tipoConsulta = "O";
                break;
        }

//            String pv1Message = "PV1|1|<CLASE_PACIENTE>|<CODIGO_SERVICIO>^<CODIGO_SALA>^<CODIGO_CAMA>^HG1||||||||||||||||<NUMERO_VISITA>|||||||||||||||||||||||||<FECHA_HORA_ADMISION>|";
        String pv1Message = "PV1|1|" + tipoConsulta + "|^^^HG1||||||||||||||||||||||||||||||||||||||||||";

        String orcMessage = "ORC|NW|" + receta.getId() + "^ALERT||" + foundMedico.getUnidad() + "^ALERT|||||" + recetaFecha + "|";

        String tq1Message1 = "TQ1|";

        PreparedStatement ps = con.getConn().prepareStatement("SELECT DATE_FORMAT( DATE_ADD(r.fecha_hora, INTERVAL ? DAY), '%Y%m%d' ) AS fin, DATE_FORMAT(NOW(), '%Y%m%d' ) AS surtido FROM receta AS r WHERE r.id_rec = ?;");
        ResultSet rs;
        //rxeMessageList
        //String rxeMessage = "RXE|<RXE_CANTIDAD_MEDICAMENTO>^&<RXE_INTERVALO_REPETICION>&<RXE_HORA_INTERVALO_REPETICION>^<RXE_DURACION_TRATAMIENTO>^<RXE_FECHA_INICIO_PRESCRIPCION>^<RXE_FECHA_FIN_PRESCRIPCION>^<RXE_CONDICIONES_DE_USO>^< TEXTO_ADICIONAL_PRESCRIPCION>|<RXE_ID_MEDICAMENTO>^<RXE_DESCRIPCION_MEDICAMENTO>^<RXE_ID_MEDICAMENTO_SISTEMA>|<RXE_CANTIDAD_MINIMA_A_OTORGAR>|<RXE_CANTIDAD_MAXIMA_A_OTORGAR>|<RXE_UNIDAD__DE_MEDICA>|<RXE_FORMA_DE_DOSIFICACION>|<RXE_INSTRUCCIONES_PARA_PROVEEDOR>|||<RXE_CANTIDAD_A_ADMINITRAR>|";
        MovimientosImpl daoMovimientos = new MovimientosImpl(idUsuario, con);
        List<DetalleCapturado> listDetalleReceta = daoRecetas.reconstruirDetalles(idReceta);
        MovimientoProducto movimiento;
        for (DetalleCapturado aux : listDetalleReceta) {
            String indicacionesString = aux.detalleReceta.getIndicaciones();
            Integer numdias = 0;
            Integer piezas = 0;
            String formaDosificacion = "";
            String[] indicaciones = null;
            if (indicacionesString != null && !indicacionesString.isEmpty()) {
                indicaciones = indicacionesString.split("|");
                if (indicaciones != null && indicaciones[0] != null) {
                    String str = indicaciones[0].replaceAll("[^-?0-9]+", " ");
                    List<String> arrayNumbers = Arrays.asList(str.trim().split(" "));
                    if (arrayNumbers != null && !arrayNumbers.isEmpty() && arrayNumbers.size() > 1) {
                        numdias = Integer.valueOf(arrayNumbers.get(1) != null ? arrayNumbers.get(1) : "0");
                        piezas = Integer.valueOf(arrayNumbers.get(0) != null ? arrayNumbers.get(0) : "0");
                    }
                }
            }
            String rxeFechaIni = recetaFecha;

            ps.setInt(1, numdias);
            ps.setInt(2, idReceta);
            rs = ps.executeQuery();
            rs.next();

            String rxeFechaFin = rs.getString("fin");
            String rxdFechaSurt = rs.getString("surtido");

            rs.close();
            ps.clearParameters();

            movimiento = daoMovimientos.obtenerProducto(aux.detalleProducto.getId());
            String idProducto = movimiento.producto.getClave().replaceAll("^0+(?!$)", "");
            //Obtener medicamentos

            int canSol = aux.detalleReceta.getCantidadSolicitada();
            int canSur = aux.detalleReceta.getCantidadSurtida();

//                                  "RXE|<RXE_CANTIDAD_MEDICAMENTO>^&<RXE_INTERVALO_REPETICION>&<RXE_HORA_INTERVALO_REPETICION>^<RXE_DURACION_TRATAMIENTO>^<RXE_FECHA_INICIO_PRESCRIPCION>^<RXE_FECHA_FIN_PRESCRIPCION>^<RXE_CONDICIONES_DE_USO>^< TEXTO_ADICIONAL_PRESCRIPCION>|<RXE_ID_MEDICAMENTO>^<RXE_DESCRIPCION_MEDICAMENTO>^<RXE_ID_MEDICAMENTO_SISTEMA>|<RXE_CANTIDAD_MINIMA_A_OTORGAR>|<RXE_CANTIDAD_MAXIMA_A_OTORGAR>|<RXE_UNIDAD__DE_MEDICA>|<RXE_FORMA_DE_DOSIFICACION>|<RXE_INSTRUCCIONES_PARA_PROVEEDOR>|||<RXE_CANTIDAD_A_ADMINITRAR>|"
            String rxeMessage = "RXE|" + canSol + "^^" + numdias + "^" + rxeFechaIni + "^" + rxeFechaFin + "^^" + (indicaciones != null ? indicaciones[1] : "")
                    + "|" + movimiento.producto.getClave() + "^" + movimiento.producto.getDescripcion() + "^" + idProducto + "|" + canSol + "|" + canSol + "|" + foundMedico.getUnidad() + "|" + formaDosificacion + "||||" + piezas + "|";
            //rxeMessageList.add(rxeMessage);

            String tq1Message2 = "TQ1|";

            String rxrMessage = "RXR|1|";
            int resultadoSurtimiento = canSol - canSur;

            Integer status = -1;

            if (resultadoSurtimiento == 0) {//RECETA CON MEDICAMENTO SURTIDO COMPLETAMENTE
                status = 1;
            } else if (resultadoSurtimiento == canSol) {// RECETA CON MEDICMENTO NO SURTIDO
                status = 3;
            } else if (resultadoSurtimiento < canSol) {//RECETA CON MEDICAMENTO SURTIDO DE MANERA PARCIAL
                status = 2;
            }

            String rxdMessage = "RXD|" + aux.detalleReceta.getId() + "|" + movimiento.producto.getClave()
                    + "^" + movimiento.producto.getDescripcion() + "|" + rxdFechaSurt + "|" + canSur
                    + "|" + movimiento.producto.getPresentacion() + "||" + receta.getId()
                    + "||||||||||||||||||||||||||" + status + "|";

            String rxrMessage2 = "RXR|1|";

            StringBuilder sbMessage = new StringBuilder();
            sbMessage.append(START_OF_BLOCK)
                    .append(mshMessage)
                    .append(CARRIAGE_RETURN)
                    .append(pidMessage)
                    .append(CARRIAGE_RETURN)
                    .append(pv1Message)
                    .append(CARRIAGE_RETURN)
                    .append(orcMessage)
                    .append(CARRIAGE_RETURN)
                    .append(tq1Message1)
                    .append(CARRIAGE_RETURN)
                    .append(rxeMessage)
                    .append(CARRIAGE_RETURN)
                    .append(tq1Message2)
                    .append(CARRIAGE_RETURN)
                    .append(rxrMessage)
                    .append(CARRIAGE_RETURN)
                    .append(rxdMessage)
                    .append(CARRIAGE_RETURN)
                    .append(rxrMessage2)
                    .append(CARRIAGE_RETURN)
                    .append(END_OF_BLOCK)
                    .append(CARRIAGE_RETURN);

            rdso13MessageList.add(sbMessage.toString());
        }
        ps.close();

        return rdso13MessageList;
    }

    public String createHL7MessageRDSO13(int idMedico, String observacionHL7,
            DetalleReceta detalle, ConectionDB con) throws SQLException {

        MedicoImpl dao = new MedicoImpl(con.getConn());
        Medico foundMedico = dao.reconstruirMedico(idMedico);

        PatientOrc patientOrc = new PatientOrc();
        PatientMsh patientMsh = new PatientMsh();
        PatientVisit patientVisit1 = new PatientVisit();
        PatientPid patientPid = new PatientPid();
        PatientRxe patientRxe = new PatientRxe();
        Gson gson = new Gson();

        Hl7Message hl7message = gson.fromJson(observacionHL7, Hl7Message.class);

        if (hl7message != null) {
            patientOrc = hl7message.getPatientOrc();
            patientVisit1 = hl7message.getPatientVisit();
            patientMsh = hl7message.getPatientMsh();
            patientPid = hl7message.getPatientPid();
            patientRxe = hl7message.getPatientRxe();
        }

        String md5MessageId = DigestUtils.md5Hex(String.valueOf(new Date().getTime()));

        String mshMessage = "MSH|^~\\&|EXT_SYS_RECETA|" + foundMedico.getUnidad()
                + "|ALERT|" + foundMedico.getUnidad() + "|" + patientMsh.getFecha()
                + "||RDS^O13^RDS_O13|" + md5MessageId + "|P|2.5|";

        String curpData = "SIN CURP";
        if (patientPid.getCurp() != null) {
            curpData = patientPid.getCurp();
        }

        String pidNombres = patientPid.getApPaterno() != null ? patientPid.getApPaterno() : ""
                + "&" + patientPid.getApMaterno() != null ? patientPid.getApMaterno() : ""
                        + "^" + patientPid.getPrimerNombre() != null ? patientPid.getPrimerNombre() : ""
                                + patientPid.getSegundoNombre() != null ? "^" + patientPid.getSegundoNombre() : "";

        String pidMessage = "PID|1||" + patientPid.getIdPaciente() + "^^^ADT^PN~"
                + curpData + "^^^SEGOB^NI||" + pidNombres + "||"
                + patientPid.getFechaNacimiento() + "|" + patientPid.getSexo() + "|";

        String pv1Message = "PV1|1|O|" + patientVisit1.getPuntoAtencion() + "^"
                + patientVisit1.getCuarto() + "^^" + patientVisit1.getUnidadMedica()
                + "||||||||||||||||" + patientVisit1.getNumeroVisita() + "^^^"
                + patientVisit1.getIdPersona() + "^ADT^" + patientVisit1.getIdUnidadMedica()
                + "|||||||||||||||||||||||||" + patientVisit1.getFechaSolicitud() + "|";

        //String orcMessage = "ORC|NW|" + patientOrc.getIdReceta()+ "^ALERT||" + foundMedico.getClauni() + "^ALERT|||||" + patientMsh.getFecha() + "|";
        String orcMessage = "ORC|NW|" + patientOrc.getIdReceta() + "^ALERT||"
                + patientOrc.getGroupNumberId() + "^ALERT|||||" + patientOrc.getFecha()
                + "|||" + patientOrc.getProveedorOrden() + "^" + patientOrc.getApellidoPaterno()
                + "^" + patientOrc.getPrimerNombre() + "|";

        String tq1Message1 = "TQ1|";

        String indicacionesString = detalle.getIndicaciones();
        Integer numdias = 0;
        Integer piezas = 0;
        String formaDosificacion = "";
        String[] indicaciones = null;
        if (indicacionesString != null && !indicacionesString.isEmpty()) {
            indicaciones = indicacionesString.split("|");
            if (indicaciones != null && indicaciones[0] != null) {
                String str = indicaciones[0].replaceAll("[^-?0-9]+", " ");
                List<String> arrayNumbers = Arrays.asList(str.trim().split(" "));
                if (arrayNumbers != null && !arrayNumbers.isEmpty() && arrayNumbers.size() > 1) {
                    numdias = Integer.valueOf(arrayNumbers.get(1) != null ? arrayNumbers.get(1) : "0");
                    piezas = Integer.valueOf(arrayNumbers.get(0) != null ? arrayNumbers.get(0) : "0");
                }
            }
        }

        String rxeFechaIni;
        String rxeFechaFin;
        String rxdFechaSurt;
        try (PreparedStatement ps = con.getConn().prepareStatement("SELECT DATE_FORMAT( DATE_ADD(r.fecha_hora, INTERVAL ? DAY), '%Y%m%d%H%i%S' ) AS fin, DATE_FORMAT(NOW(), '%Y%m%d%H%i%S' ) AS surtido, DATE_FORMAT(r.fecha_hora, '%Y%m%d%H%i%S' ) AS inicio FROM receta AS r WHERE r.id_rec = ?;")) {
            ResultSet rs;
            ps.setInt(1, numdias);
            ps.setInt(2, detalle.getIdReceta());
            rs = ps.executeQuery();
            rs.next();
            rxeFechaIni = rs.getString("inicio");
            rxeFechaFin = rs.getString("fin");
            rxdFechaSurt = rs.getString("surtido");
            rs.close();
        }

        MovimientosImpl daoMovimientos = new MovimientosImpl(idMedico, con);
        MovimientoProducto movimiento = daoMovimientos.obtenerProducto(detalle.getDetalleProducto());
        //catalogoIsem
        String idProducto = movimiento.detalleProducto.getClave().replaceAll("^0+(?!$)", "");

        if (!(idProducto.contains("."))) {
            idProducto = idProducto + ".00";
        }

        int solicitado = detalle.getCantidadSolicitada();
        int surtido = detalle.getCantidadSurtida();

        String rxeMessage = "RXE|" + solicitado + "^^" + numdias + "^" + rxeFechaIni
                + "^" + rxeFechaFin + "^^" + (indicaciones != null ? indicaciones[1] : "")
                + "|" + idProducto + "^" + movimiento.producto.getDescripcion() + "^" + patientRxe.getCodigoMedicamento()
                + "|" + solicitado + "|" + solicitado + "|" + foundMedico.getUnidad() + "|"
                + formaDosificacion + "||||" + piezas + "|";

        String tq1Message2 = "TQ1|";

        String rxrMessage = "RXR|1|";

        int resultadoSurtimiento = solicitado - surtido;

        Integer status = -1;

        if (resultadoSurtimiento == 0) {//RECETA CON MEDICAMENTO SURTIDO COMPLETAMENTE
            status = 1;
        } else if (resultadoSurtimiento == solicitado) {// RECETA CON MEDICMENTO NO SURTIDO
            status = 3;
        } else if (resultadoSurtimiento < solicitado) {//RECETA CON MEDICAMENTO SURTIDO DE MANERA PARCIAL
            status = 2;
        }

        String rxdMessage = "RXD|1|" + idProducto + "^" + movimiento.producto.getDescripcion()
                + "^" + patientRxe.getCodigoMedicamento() + "|" + rxdFechaSurt + "|"
                + surtido + "|" + movimiento.producto.getPresentacion() + "||" + patientOrc.getIdReceta()
                + "||||||||||||||||||||||||||" + status + "|";

        String rxrMessage2 = "RXR|1|";

        StringBuilder sbMessage = new StringBuilder();
        sbMessage.append(START_OF_BLOCK)
                .append(mshMessage)
                .append(CARRIAGE_RETURN)
                .append(pidMessage)
                .append(CARRIAGE_RETURN)
                .append(pv1Message)
                .append(CARRIAGE_RETURN)
                .append(orcMessage)
                .append(CARRIAGE_RETURN)
                .append(tq1Message1)
                .append(CARRIAGE_RETURN)
                .append(rxeMessage)
                .append(CARRIAGE_RETURN)
                .append(tq1Message2)
                .append(CARRIAGE_RETURN)
                .append(rxrMessage)
                .append(CARRIAGE_RETURN)
                .append(rxdMessage)
                .append(CARRIAGE_RETURN)
                .append(rxrMessage2)
                .append(CARRIAGE_RETURN)
                .append(END_OF_BLOCK)
                .append(CARRIAGE_RETURN);

        return sbMessage.toString();
    }

    public void createHl7RecetaLog(String tipo, String mensaje, Integer status, String proceso) throws SQLException {
        RecetaHl7Log recetaLog = new RecetaHl7Log();
        recetaLog.setEstatus(status);
        java.util.Date date = new java.util.Date();
        recetaLog.setFecha(date);
        recetaLog.setHl7_mensaje(mensaje);
        recetaLog.setProceso(proceso);
        recetaLog.setTipo_mensaje(tipo);
        hl7RecetaLogDao.addRecetaHl7Log(recetaLog);
        //System.out.println(result);
    }

    public Long createMensajeHl7(String mensaje, Integer status, Integer idrec, Integer nummsg) {
        Long result = null;
        try {

            MensajeHl7 mensajeHl7 = new MensajeHl7();
            mensajeHl7.setMensaje(mensaje);
            mensajeHl7.setStatus(status);
            mensajeHl7.setIdrec(idrec);
            mensajeHl7.setNummsg(nummsg);
            if (nummsg != null && nummsg > 0) {
                boolean existshl7msg = mensajeDao.checkIfHl7MessageExists(idrec, nummsg);
                if (!existshl7msg) {
                    result = mensajeDao.addMensajeHl7(mensajeHl7);
                }
            } else {
                result = mensajeDao.addMensajeHl7(mensajeHl7);
            }
            return result;
        } catch (SQLException ex) {
            result = null;
            Logger.getLogger(MessageHL7Service.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String createHL7MessageRDEO11(String observacionHL7) {
        Date date = new Date();
        Gson gson = new Gson();
        String messageId = "";
        String messageDate = "";
        PatientPid patientPid = new PatientPid();
        PatientVisit patientPv1 = new PatientVisit();
        PatientOrc orcpatient = new PatientOrc();
        PatientMsh patientmsh = new PatientMsh();

        Hl7Message hl7message = gson.fromJson(observacionHL7, Hl7Message.class);
        if (hl7message != null) {
            orcpatient = hl7message.getPatientOrc();
            patientmsh = hl7message.getPatientMsh();
            patientPid = hl7message.getPatientPid();
            patientPv1 = hl7message.getPatientVisit();

            messageId = orcpatient.getIdReceta();
            messageDate = patientmsh.getFecha();
        }
        String dateString = new SimpleDateFormat("yyyyMMddHHmmss").format(date);
        String md5MessageId = DigestUtils.md5Hex(messageId);
        String mshMessage = "MSH|^~\\&|EXT_SYS_RECETA|" + patientmsh.getIdUnidadMedica() + "|ALERT|" + patientmsh.getIdUnidadMedica() + "|" + dateString + "||RDE^O11^RDE_O11|" + md5MessageId + "|P|2.5|";

        String arg1 = patientPid.getIdPaciente().toString();
        String arg2 = patientPid.getCurp() != null ? patientPid.getCurp() : "SIN CURP";
        String arg3 = patientPid.getApPaterno();
        String arg4 = patientPid.getApMaterno();
        String arg5 = patientPid.getPrimerNombre();
        String arg6 = patientPid.getSegundoNombre() != null ? patientPid.getSegundoNombre() : "";
        String arg7 = patientPid.getFechaNacimiento();
        String arg8 = patientPid.getSexo();

        String pidMessage = "PID|1||" + arg1 + "^^^ADT^PN~" + arg2 + "^^^SEGOB^NI||" + arg3 + "&" + arg4 + "^" + arg5 + " " + arg6 + "||" + arg7 + "|" + arg8 + "|";

        String pv1Message = "PV1|1|" + patientPv1.getViaAdmon() + "|" + patientPv1.getPuntoAtencion() + "^" + patientPv1.getCuarto() + "^^" + patientPv1.getUnidadMedica() + "||||||||||||||||" + patientPv1.getNumeroVisita() + "^^^" + patientPv1.getIdPersona() + "^ADT^" + patientPv1.getUnidadMedica() + "|||||||||||||||||||||||||" + messageDate + "|";

        //                   ORC|OK|841216000097^ALERT||68129^ALERT|||||20160406081839|||290044^PRUEBA^PRUEBAS M|
        String orcMessage = "ORC|OK|" + messageId + "^ALERT||" + orcpatient.getGroupNumberId() + "^ALERT|||||" + messageDate + "|||" + orcpatient.getProveedorOrden() + "^" + orcpatient.getApellidoPaterno() + "^" + orcpatient.getPrimerNombre() + "|";

        String rxeMessage = "RXE||||||";
        String nteMessage = "NTE|||Solicitud de receta: " + messageDate + ", Fecha Notificacion Asistencia : " + dateString + "|";
        String tq1Message = "TQ1|";
        String rxrMessage = "RXR||";
        String rxcMessage = "RXC|||||";
        StringBuilder ackMessage = new StringBuilder();
        ackMessage.append(START_OF_BLOCK)
                .append(mshMessage)
                .append(CARRIAGE_RETURN)
                .append(pidMessage)
                .append(CARRIAGE_RETURN)
                .append(pv1Message)
                .append(CARRIAGE_RETURN)
                .append(orcMessage)
                .append(CARRIAGE_RETURN)
                .append(rxeMessage)
                .append(CARRIAGE_RETURN)
                .append(nteMessage)
                .append(CARRIAGE_RETURN)
                .append(tq1Message)
                .append(CARRIAGE_RETURN)
                .append(rxrMessage)
                .append(CARRIAGE_RETURN)
                .append(rxcMessage)
                .append(CARRIAGE_RETURN)
                .append(END_OF_BLOCK)
                .append(CARRIAGE_RETURN);

        return ackMessage.toString();
    }

    public boolean checkIfExistsHl7Mensaje(Integer idrec, Integer nummsg) throws SQLException {
        return mensajeDao.checkIfHl7MessageExists(idrec, nummsg);
    }

}
