/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Clases.ConectionDB;
import Clases.LoggerUsuario;
import Clases.loadReq;
import in.co.sneh.controller.FileUploadServlet;
import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 * Carga de archivo de requerimiento
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
@MultipartConfig
public class requerimiento extends HttpServlet {

    loadReq lee = new loadReq();
    private static final String SAVE_DIR = "CVS_Abastos";
    private static final String SELECT_USER = "SELECT usuarios.id_usu FROM usuarios WHERE usuarios.id_usu = ? AND usuarios.pass = MD5(?)";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        HttpSession sesion = request.getSession(true);
        String mensaje = "Error al consultar en la base de datos.";
        ConectionDB con = new ConectionDB();
        String claveUni = (String) sesion.getAttribute("cla_uni");
        try {
            String id = (String) sesion.getAttribute("id_usu");
            con.conectar();

            PreparedStatement ps = con.getConn().prepareStatement(SELECT_USER);
            ps.setInt(1, Integer.parseInt(id));
            ps.setString(2, request.getParameter("contra"));
            ResultSet rs = ps.executeQuery();
            if (!rs.isBeforeFirst()) {
                mensaje = "La contraseña ingresada es incorrecta.";
                throw new SQLException(mensaje, ps.toString());
            }

            Part cvs = request.getPart("archivo");
            String rutaServlet = request.getServletContext().getRealPath("");
            String rutaCarpeta = rutaServlet + File.separatorChar + SAVE_DIR;
            String nombreArchivo = cvs.getSubmittedFileName();

            File carpeta = new File(rutaCarpeta);
            if (!carpeta.exists()) {
                carpeta.mkdir();
            }

            String rutaArchvio = rutaCarpeta + File.separatorChar + nombreArchivo;
            cvs.write(rutaArchvio);
            String ver;
            sesion.setAttribute("nomArchivo", rutaArchvio);
            if (lee.leeCSV(rutaArchvio, claveUni)) {
                ver = "si";
                sesion.setAttribute("nomArchivo", nombreArchivo);
            } else {
                cvs.delete();
                ver = "no";
                mensaje = String.format("El archivo cvs contiene errores, por favor revise el log en '%s'", LoggerUsuario.getRutaArchivoActual());
                sesion.setAttribute("info_upload", mensaje);
            }

            sesion.setAttribute("ver", ver);

        } catch (IOException | ServletException ex) {
            sesion.setAttribute("ver", "no");
            sesion.setAttribute("info_upload", "El archivo no puedo subirse al servidor.");
            Logger.getLogger(FileUploadServlet.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } catch (SQLException ex) {
            Logger.getLogger(FileUploadServlet.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            sesion.setAttribute("ver", "no");
            sesion.setAttribute("info_upload", mensaje);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(FileUploadServlet.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }

        response.sendRedirect("farmacia/requerimiento.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
