/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Dao.kardexViewDao;
import Impl.kardexViewClaveDaoImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelos.kardexVista;
import com.google.gson.Gson;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Generación de kardex
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
@WebServlet(name = "kardexVista", urlPatterns = {"/kardexVista"})
public class kardexVistaServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet kardexVista</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet kardexVista at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        String accion = request.getParameter("accion");
        String clave = "";
        switch (accion) {
            case "CLAVE":
                clave = request.getParameter("clave");
                Map kardexMap = new LinkedHashMap();
                kardexViewDao kardex = new kardexViewClaveDaoImpl();
                kardexVista k;
                k = kardex.datosKardex(clave);
                kardexMap.put("clave", k.getClave());
                kardexMap.put("descripcion", k.getDescripcion());
                kardexMap.put("existencia", k.getExistencias());
                String json1;
                json1 = new Gson().toJson(kardexMap);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json1);

                break;
            case "kardex":
                JSONObject json = new JSONObject();
                JSONArray jsona = new JSONArray();

                clave = request.getParameter("clave");
                kardex = new kardexViewClaveDaoImpl();
                List<kardexVista> lk;
                lk = kardex.listaKardexByClave(clave);
                for (kardexVista kModel : lk) {
                    json.put("clave", kModel.getClave());
                    json.put("lote", kModel.getLote());
                    json.put("cad", kModel.getCaducidad());
                    json.put("origen", kModel.getOrigen());
                    json.put("cantidad", kModel.getCantidad());
                    json.put("tipoMov", kModel.getTipoMovimiento());
                    json.put("abasto", kModel.getAbasto());
                    json.put("folRec", kModel.getFolioReceta());
                    json.put("paciente", kModel.getPaciente());
                    json.put("medico", kModel.getMedico());
                    json.put("fecha", kModel.getFecha());
                    json.put("obs", kModel.getObservaciones());
                    json.put("usuario", kModel.getUsuario());
                    json.put("sumatoria", kModel.getSumatoria());

                    jsona.add(json);
                    json = new JSONObject();
                }
                out.print(jsona);
                out.close();

                break;
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
