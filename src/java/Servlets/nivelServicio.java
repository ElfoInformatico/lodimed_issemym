/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Dao.nivelDao;
import Impl.actNivelDao;
import Modelos.nivelModel;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Reporte nivel de servicio
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class nivelServicio extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet nivelServicio</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet nivelServicio at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        int bandera = Integer.parseInt(request.getParameter("ban"));

        switch (bandera) {

            case 1:

                String f1 = request.getParameter("f1");
                String f2 = request.getParameter("f2");
                JSONObject json = new JSONObject();
                JSONArray jsona = new JSONArray();

                nivelDao n = new actNivelDao();

                List<nivelModel> nivel = new ArrayList<nivelModel>();
                nivel = n.byFecOnly(f1, f2);
                for (int i = 0; i < nivel.size(); i++) {

                    nivelModel niv = new nivelModel();
                    niv = (nivelModel) nivel.get(i);

                    json.put("fecha", niv.getFec());
                    json.put("pzSol", niv.getPzSol());
                    json.put("pzNoSur", niv.getPzNoSur());
                    json.put("claSol", niv.getClaSol());
                    json.put("claNoSur", niv.getClaNoSur());
                    json.put("recSol", niv.getRecSol());
                    json.put("recPar", niv.getRecPar());
                    json.put("porc", niv.getPorc());
                    json.put("pzVend", niv.getPzVen());
                    json.put("importe", niv.getImpor());

                    jsona.add(json);
                    json = new JSONObject();

                }
                out.print(jsona);
                out.close();

                break;
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
