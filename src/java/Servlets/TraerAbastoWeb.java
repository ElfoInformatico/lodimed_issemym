/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Clases.CargaAbasto;
import Clases.ConectionDB;
import Clases.TraerAbasto;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Carga de abasto web
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
@WebServlet(name = "TraerAbastoWeb", urlPatterns = {"/TraerAbastoWeb"})
public class TraerAbastoWeb extends HttpServlet {

    TraerAbasto traer = new TraerAbasto();
    CargaAbasto carga = new CargaAbasto();

    private static final String SELECT_USER = "SELECT usuarios.id_usu FROM usuarios WHERE usuarios.id_usu = ? AND usuarios.pass = MD5(?)";
    private static final String SELECT_FOLIO = "SELECT k.id_kardex FROM kardex AS k WHERE k.fol_aba = ? GROUP BY k.fol_aba";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        String mensaje = "Error al consultar en la base de datos.";
        ConectionDB con = new ConectionDB();

        try {
            String id = (String) sesion.getAttribute("id_usu");
            con.conectar();

            PreparedStatement ps = con.getConn().prepareStatement(SELECT_USER);
            ps.setInt(1, Integer.parseInt(id));
            ps.setString(2, request.getParameter("contra"));
            ResultSet rs = ps.executeQuery();
            if (!rs.next()) {
                mensaje = "La contraseña ingresada es incorrecta.";
                throw new SQLException(mensaje, ps.toString());
            }

            /*
             ps = con.getConn().prepareStatement(SELECT_FOLIO);
             ps.setString(1, nombreArchivo);
             rs = ps.executeQuery();
             if (rs.isBeforeFirst()) {
             mensaje = "Este archivo de abasto ya fue cargado.";
             throw new SQLException(mensaje, ps.toString());
             }
             */
            String ver;

            String unidad, noFolio;

            unidad = (String) sesion.getAttribute("cla_uni");
            noFolio = traer.comprobarAbasto(unidad);

            if (!noFolio.equals("")) {
                if (traer.traerAbastoWeb(unidad)) {//traer abasto
                    ver = "si";
                    sesion.setAttribute("nomArchivo", "WEB-" + noFolio);
                    //sesion.setAttribute("nomArchivo", nombreArchivo);
                } else {
                    ver = "no";
                    mensaje = String.format("El abasto contiene errores, por favor contacte a soporte");
                    sesion.setAttribute("info_upload", mensaje);
                }
            } else {
                ver = "no";
                mensaje = String.format("No se tienen abastos pendientes para la unidad");
                sesion.setAttribute("info_upload", mensaje);
            }

            sesion.setAttribute("ver", ver);

        } catch (SQLException ex) {
            Logger.getLogger(TraerAbastoWeb.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            sesion.setAttribute("ver", "no");
            sesion.setAttribute("info_upload", mensaje);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(TraerAbastoWeb.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }

        response.sendRedirect("farmacia/cargaAbastoWeb.jsp");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
