package Servlets;

import Clases.ConectionDB;
import Dao.operacionesDao;
import Dao.PacienteDao;
import Impl.MedicoImpl;
import Impl.operacionesImpl;
import Impl.PacienteDaoImpl;
import Modelos.DriveManager;
import Modelos.Medico;
import Modelos.Paciente;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Validación vigencia sp, rango de folios por médico parametros
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
//Servlet para calculo de minimos y máximos de obtenerProximoRango, acutalizar el parametro de los obtenerProximoRango.
public class Operaciones extends HttpServlet {

public static final int OBTENER_INICIAL_FINAL_MEDICO = 0;
public static final int OBTENER_RANGO_FOLIO_MEDICO = 1;
public static final int ACTUALIZAR_RANGO_MEDICO = 2;
public static final int ACTUALIZAR_TIPO_SISTEMA = 3;
public static final int OBTENER_TIPO_SISTEMA = 4;
public static final int OBTENER_PREFIJO_RECETA = 5;
public static final int ACTUALIZAR_PREFIJO_RECETA = 6;
public static final int OBTENER_AFILIADOS = 7;
public static final int OBTENER_VIGENCIA = 8;

/**
 * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
 *
 * @param request servlet request
 * @param response servlet response
 * @throws ServletException if a servlet-specific error occurs
 * @throws IOException if an I/O error occurs
 */
protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    try (PrintWriter out = response.getWriter()) {
        /* TODO output your page here. You may use following sample code. */
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Servlet Operaciones</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Servlet Operaciones at " + request.getContextPath() + "</h1>");
        out.println("</body>");
        out.println("</html>");
    }
}

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
/**
 * Handles the HTTP <code>GET</code> method.
 *
 * @param request servlet request
 * @param response servlet response
 * @throws ServletException if a servlet-specific error occurs
 * @throws IOException if an I/O error occurs
 */
@Override
protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
}

/**
 * Handles the HTTP <code>POST</code> method.
 *
 * @param request servlet request
 * @param response servlet response
 * @throws ServletException if a servlet-specific error occurs
 */
@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException {
    PrintWriter out;
    HttpSession sesion = request.getSession(true);
    //Variable la cual define la operacion a realizar se obtiene el parametro desde ajax
    int bandera = Integer.parseInt(request.getParameter("ban"));
    Map TUsu = new LinkedHashMap();
    String json1;

    operacionesDao op = new operacionesImpl();
    Paciente paciente;
    PacienteDao pacientes;
    ConectionDB con;

    try {
        out = response.getWriter();
        switch (bandera) {
        //Rutina para obtener el folio inicial y final del médico
        case OBTENER_INICIAL_FINAL_MEDICO:
            con = new ConectionDB();
            con.conectar();
            MedicoImpl dao = new MedicoImpl(con.getConn());
            Medico m;

            m = dao.obtenerProximoRango();
            TUsu.put("folIni", m.getFolioInicial());
            TUsu.put("folFin", m.getFolioFinal());

            json1 = new Gson().toJson(TUsu);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json1);

            break;
        //Rutina para obtener el rango de los obtenerProximoRango asignados al médico   
        case OBTENER_RANGO_FOLIO_MEDICO:
            TUsu = new LinkedHashMap();
            int folParam = op.folRan();

            TUsu.put("rango", folParam);

            json1 = new Gson().toJson(TUsu);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json1);

            break;
        //Rutina para actualizar el rango de los obtenerProximoRango asignados a los médicos    
        case ACTUALIZAR_RANGO_MEDICO:
            int id = Integer.parseInt(request.getParameter("ran"));
            TUsu = new LinkedHashMap();
            boolean reload = op.newRang(id);
            if (reload) {
                TUsu.put("msj", "Rango actualizado con éxito.");
            } else {
                TUsu.put("msj", "Error al actualizar los datos llamar al departamento de sistemas.");
            }

            json1 = new Gson().toJson(TUsu);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json1);

            break;
        //Rutina para seleccionar el sistema.    
        case ACTUALIZAR_TIPO_SISTEMA:
            int sist = Integer.parseInt(request.getParameter("sis"));
            TUsu = new LinkedHashMap();
            reload = op.tipFarm(sist);
            String tipoSistema = "";
            if (sist == 1) {
                tipoSistema = "Farmacia";
            } else if (sist == 2) {
                tipoSistema = "Médico - Farmacia";
            } else if (sist == 3) {
                tipoSistema = "HL7";
            } else if (sist == 4) {
                tipoSistema = "Rurales";
            }

            if (reload) {
                TUsu.put("msj", "Sistema " + tipoSistema + " elegido con éxito");
            } else {
                TUsu.put("msj", "Error al actualizar los datos llamar al departamento de sistemas.");
            }

            json1 = new Gson().toJson(TUsu);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json1);

            break;

        case OBTENER_TIPO_SISTEMA:
            TUsu = new LinkedHashMap();
            int sis = 0;
            sis = op.tipSis();
            sesion.setAttribute("tipoSistema", sis);
            TUsu.put("sis", sis);
            json1 = new Gson().toJson(TUsu);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json1);
            break;

        case OBTENER_PREFIJO_RECETA:
            TUsu = new LinkedHashMap();
            String prefijo = "";
            prefijo = op.prefijo();
            TUsu.put("pre", prefijo);
            json1 = new Gson().toJson(TUsu);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json1);
            break;
        case ACTUALIZAR_PREFIJO_RECETA:
            TUsu = new LinkedHashMap();
            boolean realizado = false;
            String per = request.getParameter("per");
            realizado = op.updatePrefijo(per);
            if (realizado) {
                TUsu.put("msj", "Prefijo actualizado con éxito.");
            } else {
                TUsu.put("msj", "Error al actualizar el prefijo intentar de nuevo.");
            }

            json1 = new Gson().toJson(TUsu);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json1);
            break;

        case OBTENER_AFILIADOS:
            String numAfi = request.getParameter("na");
            Map<String, String> lj = new LinkedHashMap<String, String>();
            con = new ConectionDB();
            con.conectar();
            pacientes = new PacienteDaoImpl(con.getConn());
            List<Paciente> lP;
            lP = pacientes.obtenerAfiliados(numAfi);

            for (int i = 0; i < lP.size(); i++) {
                paciente = (Paciente) lP.get(i);
                lj.put(paciente.getNombreCompleto(), paciente.getNombreCompleto());
            }
            String jon;
            jon = new Gson().toJson(lj);
            response.setContentType("aplication/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(jon);
            con.cierraConexion();

            break;

        case OBTENER_VIGENCIA:
            TUsu = new LinkedHashMap();
            numAfi = request.getParameter("na");
            con = new ConectionDB();
            con.conectar();

            pacientes = new PacienteDaoImpl(con.getConn());
            paciente = pacientes.obtenerVigencias(numAfi);
            TUsu.put("fi", paciente.getInicioVigencia());
            TUsu.put("ff", paciente.getFinVigencia());

            json1 = new Gson().toJson(TUsu);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json1);
            con.cierraConexion();

            break;

        case 9:
            String cu = "";
            cu = (String) sesion.getAttribute("cla_uni");
            TUsu = new LinkedHashMap();
            int frec = 0;
            frec = op.frecuencia(cu);
            TUsu.put("frec", frec);
            json1 = new Gson().toJson(TUsu);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json1);

            break;

        case 10:
            TUsu = new LinkedHashMap();
            realizado = false;
            cu = (String) sesion.getAttribute("cla_uni");
            int frecuencia = Integer.parseInt(request.getParameter("frec"));
            realizado = op.updateFrecuencia(frecuencia, cu);
            if (realizado) {
                TUsu.put("msj", "Frecuencia actualizada con éxito.");
            } else {
                TUsu.put("msj", "Error al actualizar la frecuencia intentar de nuevo.");
            }

            json1 = new Gson().toJson(TUsu);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json1);
            break;

        /*
                 Permite asignar o actualizar la jornada de trabajo, el número 
                 de backups por día y el maximo de dias a guardar de la unidad.
         */
        case 11:
            con = new ConectionDB();
            TUsu = new LinkedHashMap();
            try {
                con.conectar();
                String init = request.getParameter("init");
                String end = request.getParameter("end");

                int backups = Integer.parseInt(request.getParameter("backups"));
                int maximum = Integer.parseInt(request.getParameter("maximum"));

                DriveManager drive = new DriveManager(con);
                drive.updateConfig(init, end, backups, maximum);

                TUsu.put("msj", "Parámetros actualizados con éxito.");
            } catch (SQLException ex) {
                Logger.getLogger(Operaciones.class.getName()).log(Level.SEVERE, null, ex);
                TUsu.put("msj", "Error al actualizar la parámetros, intentar de nuevo.");
            }

            json1 = new Gson().toJson(TUsu);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json1);

            break;
        case 12:
            int captura = Integer.parseInt(request.getParameter("captura"));
            TUsu = new LinkedHashMap();
            reload = op.tipCaptura(captura);
            String Captura = "";
            if (captura == 1) {
                Captura = "Código de barras";
            } else {
                Captura = "Automática";
            }

            if (reload) {
                TUsu.put("msj", "Tipo de captura " + Captura + " elegido con éxito");
            } else {
                TUsu.put("msj", "Error al actualizar los datos llamar al departamento de sistemas.");
            }

            json1 = new Gson().toJson(TUsu);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json1);
            break;
        case 13:
            TUsu = new LinkedHashMap();
            int cap = 0;
            cap = op.tipCap();
            TUsu.put("cap", cap);
            json1 = new Gson().toJson(TUsu);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json1);
            break;

        }
        out.close();
    } catch (IOException | SQLException ex) {
        Logger.getLogger(Operaciones.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
    }
}

/**
 * Returns a short description of the servlet.
 *
 * @return a String containing servlet description
 */
@Override
public String getServletInfo() {
    return "Short description";
}// </editor-fold>

}
