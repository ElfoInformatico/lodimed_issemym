package Servlets;

import Clases.ConectionDB;
import Dao.UnidadDao;
import Impl.CausesImpl;
import Impl.MedicoImpl;
import Impl.PacienteDaoImpl;
import Impl.RecetaImpl;
import Impl.UnidadDaoImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.gnk.hl7.bean.Hl7Message;
import mx.gnk.hl7.bean.PatientOrc;
import mx.gnk.hl7.bean.PatientPid;

import Modelos.Usuario;
import Impl.UsuarioImpl;
import Modelos.Causes;
import Modelos.Medico;
import Modelos.Paciente;
import com.gnkl.services.hl7.MessageHL7Service;
import com.gnkl.services.hl7.SocketUtils;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import mx.gnk.hl7.bean.PatientRxe;
import mx.gnk.hl7.bean.PatientVisit;

/**
 * Procesamiento de receta hl7
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class ProcessHl7Message extends HttpServlet {

    PrintWriter out = null;
    String msg = "";
    ConectionDB con = new ConectionDB();
    public static final String NUEVA_RECETA_ACCION = "NW";
    public static final String CANCELACION_ACCION = "CA";
    public static final String ID_TIPO_IND = "1";
    public static final String ID_TIPO_COL = "2";
    public static final String PREFIJO = "HL7";
    public static final String MESSAGE_RDE_O11 = "RDE_O11";
    public static final String MESSAGE_RDS_O13 = "RDS_O13";
    public static final String USER_HL7 = "HL7User";
    public static final String PWD_HL7 = USER_HL7;

    private MessageHL7Service serviceHL7;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException, NullPointerException, SQLException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        try {

            if (request.getParameter("message") == null) {
                throw new NullPointerException("No se incluyo el mensaje HL7");
            }

            UsuarioImpl usuarioImpl = new UsuarioImpl();
            int idUser;

            Gson gson = new GsonBuilder().create();
            Hl7Message hl7message = gson.fromJson(request.getParameter("message"), Hl7Message.class);

            Usuario usuario = usuarioImpl.validar(USER_HL7, PWD_HL7);
            idUser = usuario.getId();

            if (!hl7message.getPatientMsh().getEstructuraMensaje().equals(MESSAGE_RDE_O11)) {
                throw new IllegalArgumentException(String.format("Este tipo de mensaje no ha sido implementado: %s", hl7message.getPatientMsh().getEstructuraMensaje()));
            }

            PatientOrc orcPatient = hl7message.getPatientOrc();
            String mensaje;
            this.con.conectar();
            this.serviceHL7 = null;
            con.getConn().setAutoCommit(false);

            switch (orcPatient.getOrderControl()) {
                case NUEVA_RECETA_ACCION:
                    processRDEO11Hl7MessageNW(hl7message, idUser, request, response);
                    mensaje = "RECIBIMIENTO NUEVA RECETA";
                    break;
                case CANCELACION_ACCION:
                    processRDEO11Hl7MessageCA(hl7message, idUser, request, response);
                    mensaje = "RECIBIMIENTO CANCELAR RECETA";
                    break;
                case "OK":
                    //TODO definir el proceso que se realizara en el caso de notificacion
                    mensaje = "RECIBIMIENTO OK";
                    break;
                default:
                    throw new IllegalArgumentException(String.format("Este tipo de acción no ha sido implementado: %s", orcPatient.getOrderControl()));
            }
            this.getService(this.con.getConn()).createHl7RecetaLog("RDE_O11", request.getParameter("message"), 0, mensaje);

            this.con.getConn().commit();

        } catch (RuntimeException ex) {

            if (con.getConn() != null) {
                if (!con.getConn().getAutoCommit()) {
                    con.getConn().rollback();
                }
            }

            String mensaje = ex.getMessage();
            if (mensaje == null) {
                StringWriter errors = new StringWriter();
                ex.printStackTrace(new PrintWriter(errors));
                mensaje = errors.toString();
            }

            mensaje = String.format("%s msg: %s, error: %s", getServletContext().getContextPath(), mensaje,
                    ex.getClass().getSimpleName());
            Logger.getLogger(ProcessHl7Message.class.getSimpleName()).log(Level.SEVERE,
                    mensaje, ex);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            out = response.getWriter();
            out.println(mensaje);
        } catch (Exception ex) {

            if (con.getConn() != null) {
                if (!con.getConn().getAutoCommit()) {
                    con.getConn().rollback();
                }
            }

            String mensaje = ex.getMessage();
            if (mensaje == null) {
                StringWriter errors = new StringWriter();
                ex.printStackTrace(new PrintWriter(errors));
                mensaje = errors.toString();
            }

            mensaje = String.format("%s msg: %s, error: %s", getServletContext().getContextPath(), mensaje,
                    ex.getClass().getSimpleName());
            Logger.getLogger(ProcessHl7Message.class.getSimpleName()).log(Level.SEVERE,
                    mensaje, ex);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            out = response.getWriter();
            out.println(mensaje);
        } finally {
            if (out != null) {
                out.close();
            }

            if (con.getConn() != null) {
                try {
                    con.getConn().close();
                } catch (SQLException ex) {
                    Logger.getLogger(ProcessHl7Message.class.getSimpleName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void processRDEO11Hl7MessageCA(Hl7Message hl7message, int idUser, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        PatientOrc orcPatient = hl7message.getPatientOrc();
        String idReceta = orcPatient.getIdReceta();

        try {

            RecetaImpl rImp = new RecetaImpl(con, Modelos.Receta.MANUAL_TIPO, idUser);
            Modelos.Receta receta = rImp.reconstruirReceta(PREFIJO + idReceta);
            if (receta == null) {
                throw new NullPointerException(
                        String.format("Receta previamente cancelada o inexistente. Id buscado: %s", idReceta));
            }
            rImp.cancelarReceta(receta.getFolio(), receta.getId(), receta.getTipoReceta(), idUser);

        } catch (SQLException ex) {
            Logger.getLogger(ProcessHl7Message.class.getSimpleName()).log(Level.SEVERE,
                    String.format("msg: %s, error: %d", ex.getMessage(), ex.getErrorCode()), ex);
            throw new SQLException(String.format("Problemas al intentar cancelar la receta. Id: %s", idReceta), ex);

        }
    }

    public void processRDEO11Hl7MessageNW(Hl7Message hl7message, int idUser, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, NullPointerException, SQLException, Exception {
        PatientOrc patientOrc = hl7message.getPatientOrc();
        PatientRxe patientRxe = hl7message.getPatientRxe();

        if (patientRxe.getCantidadTotal() == null) {
            throw new NullPointerException("No se envio el total de detalles de receta.");
        }

        if (hl7message.getPatientOrc().getProveedorOrden() == null) {
            throw new NullPointerException("No se envio el id del médico.");
        }

        if (hl7message.getPatientPid().getIdPaciente() == null) {
            throw new NullPointerException("No se envio el id del paciente.");
        }

        String[] contadorDetalles = patientRxe.getCantidadTotal().split("-");

        Medico medico = findMedicoHl7(hl7message, con.getConn());
        if (medico == null) {
            medico = addMedicoToGnk(hl7message, con.getConn());
        }

        Paciente paciente = findPacienteHl7(hl7message, con.getConn());
        if (paciente == null) {
            paciente = addPacienteToGnk(hl7message, con.getConn());
        }

        if (findCausesHl7(hl7message, con.getConn()) == null) {
            addCausesToGnk(hl7message, con.getConn());
        }

        String folio = PREFIJO + patientOrc.getIdReceta();
        Modelos.Receta receta = getRecetaByFolRec(folio, idUser, con);
        if (receta == null) {
            receta = new Modelos.Receta();
            receta.setId(addRecetaToGnk(hl7message, paciente, medico.getId(), idUser, con));
        }

        //Busca si ya chiste el detalle receta.
        boolean msgexists = getService(con.getConn()).checkIfExistsHl7Mensaje(receta.getId(), Integer.valueOf(contadorDetalles[1]));
        SocketUtils utilsHl7 = new SocketUtils(this.con.getConn());
        if (msgexists == true) {
            utilsHl7.sendDatatoSocket(getService(con.getConn()).createHL7MessageRREO12(null,
                    "El mensaje ya existe dentro del sistema GNKL. Detalle: " + patientRxe.getCantidadTotal(), null), receta.getId());
            return;
        }

        addMedicamentoToRecetaGnk(hl7message, receta.getId(), idUser, con);
    }

    private Modelos.Receta getRecetaByFolRec(String folRec, int idUsuario, ConectionDB con) throws SQLException {
        RecetaImpl dao = new RecetaImpl(con, Modelos.Receta.MANUAL_TIPO, idUsuario);
        return dao.reconstruirReceta(folRec);
    }

    private int addRecetaToGnk(Hl7Message hl7message, Paciente paciente,
            Integer idMedico, int idUsuario, ConectionDB con) throws SQLException, Exception {

        try {
            int Receta = 0;
            PatientOrc orcpatient = hl7message.getPatientOrc();
            PatientVisit patientvisit = hl7message.getPatientVisit();
            PatientRxe patientrxe = hl7message.getPatientRxe();
            PatientPid pidpatient = hl7message.getPatientPid();

            int TipoReceta = Integer.parseInt(pidpatient.getAssignAutoridad());

            if (TipoReceta == 1) {
                Receta = Modelos.Receta.MEDICO_TIPO;
            } else {
                Receta = Modelos.Receta.MEDICO_TIPO_COLECTIVA;
            }

            //Envian en formato X-Z, siendo X total de detalles y Z el detalle actual.
            String[] contadorDetalles = patientrxe.getCantidadTotal().split("-");
            Gson gson = new Gson();
            String jsonHl7 = gson.toJson(hl7message);

            String formatoFecha = "STR_TO_DATE(?, '%Y%m%d%H%i%S')";
            String fecha = orcpatient.getFecha();

            if (fecha == null) {
                fecha = "";
                formatoFecha = "NOW() ?";
            }

            /* TODO: Implementar la creacción de encabezado manual.
        
             PatientPid patientpid = hl7message.getPatientPid();
             if (patientpid.getAssignAutoridad() != null && patientpid.getAssignAutoridad().equals("0")) {
             isColectiva = true;
             newReceta.setId_tip(ID_TIPO_COL);
             } else {
             isColectiva = false;
             newReceta.setId_tip(ID_TIPO_IND);
             }
        
             */
            String folio = PREFIJO + orcpatient.getIdReceta();
            String tipoConsulta = null;
            switch (patientvisit.getViaAdmon()) {
                case "E":
                    tipoConsulta = Modelos.Receta.URGENCIAS_TIPO_CONSULTA;
                    break;

                case "I":
                    tipoConsulta = Modelos.Receta.HOSPITALIZACION_TIPO_CONSULTA;
                    break;

                case "O":
                    tipoConsulta = Modelos.Receta.CONSULTA_TIPO_CONSULTA;
                    break;
            }

            String observacion = String.format("Total detalles: %s", contadorDetalles[0]);

            /*RecetaImpl dao = new RecetaImpl(con, Modelos.Receta.MEDICO_TIPO, idUsuario, formatoFecha);
            int idGenerado = dao.crearEncabezado(Modelos.Receta.MEDICO_TIPO, paciente.getId(), idMedico,
                    idUsuario, folio, tipoConsulta, fecha, paciente.getExpediente(),
                    observacion, jsonHl7);*/
            RecetaImpl dao = new RecetaImpl(con, Receta, idUsuario, formatoFecha);
            int idGenerado = dao.crearEncabezado(Receta, paciente.getId(), idMedico,
                    idUsuario, folio, tipoConsulta, fecha, paciente.getExpediente(),
                    observacion, jsonHl7);

            return idGenerado;
        } catch (SQLException ex) {
            Logger.getLogger(ProcessHl7Message.class.getName()).log(Level.SEVERE, null, ex);
            con.getConn().rollback();
            throw ex;
        } catch (Exception ex) {
            Logger.getLogger(ProcessHl7Message.class.getName()).log(Level.SEVERE, null, ex);
            if (!con.getConn().getAutoCommit()) {
                con.getConn().rollback();
            }
            throw new Exception("Error desconocido al añadir la receta HL7.", ex);
        }

    }

    private void addMedicamentoToRecetaGnk(Hl7Message hl7message, int idReceta,
            int idUsuario, ConectionDB con) throws SQLException, Exception {

        try {
            PatientRxe patientrxe = hl7message.getPatientRxe();

            String[] contadorDetalles = patientrxe.getCantidadTotal().split("-");
            String clave = getResolvedProducto(patientrxe.getIdMedicamento(), "^[A-Za-z0-9.]+\\.0+$");
            int cantidadSolicitada = Integer.parseInt(patientrxe.getCantMinEntrega());
            String indicaciones = String.format("%s CAJAS POR %d  DIA(S)",
                    patientrxe.getCantMinEntrega(), patientrxe.getDuracionTratamiento());

            String cause = hl7message.getPatientFt1().getCodigoDiagnostico();
            cause = cause == null ? "999" : cause;
            String cause2 = ""; //Solo envian un diagnostico.

            RecetaImpl dao = new RecetaImpl(con, Modelos.Receta.MEDICO_TIPO, idUsuario);
            dao.capturaMedicamento(idReceta, clave, cantidadSolicitada, indicaciones,
                    cause, cause2, RecetaImpl.MEDICO_CAPTURA);

            boolean ultimoDetalle = Objects.equals(Integer.valueOf(contadorDetalles[1]),
                    Integer.valueOf(contadorDetalles[0]));
            if (ultimoDetalle) {
                dao.validarReceta(idReceta, RecetaImpl.MEDICO_CAPTURA);
            }

            Gson gson = new Gson();
            String jsonHl7 = gson.toJson(hl7message);
            getService(con.getConn()).createMensajeHl7(jsonHl7, 1, idReceta, Integer.valueOf(contadorDetalles[1]));

        } catch (SQLException ex) {
            Logger.getLogger(ProcessHl7Message.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } catch (Exception ex) {
            Logger.getLogger(ProcessHl7Message.class.getName()).log(Level.SEVERE, null, ex);
            if (!con.getConn().getAutoCommit()) {
                con.getConn().rollback();
            }
            throw new Exception(String.format("Error desconocido al añadir el detalle receta HL7. idReceta: %d", idReceta), ex);
        }

    }

    public String getDescUnidadByCve(String cveUnidad) {
        UnidadDao unidadDao = new UnidadDaoImpl();
        return unidadDao.getUnidadByIdHl7(cveUnidad);
    }

    private String getResolvedProducto(String idMedicamento, String regexp) {
        String clapro = idMedicamento.replaceAll(regexp, "");
        if (idMedicamento.length() < 4) {
            clapro = "0" + idMedicamento;
        } else if (idMedicamento.matches(regexp)) {
            clapro = idMedicamento.replaceAll("\\.0+$", "");
        }

        if (clapro.length() < 4) {
            clapro = "0" + clapro;
        }

        if (clapro.contains(".")) {
            String[] itemsClapro = clapro.split("\\.");
            if (itemsClapro.length > 0) {
                String first = itemsClapro[0];
                if (first.length() <= 3) {
                    clapro = "0" + clapro;
                }
            }
        }
        return clapro;
    }

    private Paciente findPacienteHl7(Hl7Message message, Connection connection) throws SQLException {

        long idPaciente = message.getPatientPid().getIdPaciente();
        idPaciente = idPaciente == 0L ? 1L : idPaciente; //Debe revisarse puesto este id es COLECTIVA

        return findPacienteHl7(idPaciente, connection);
    }

    private Paciente findPacienteHl7(Long idPaciente, Connection connection) throws SQLException {

//        System.out.println("finding paciente: " + idPaciente);
        PacienteDaoImpl dao = new PacienteDaoImpl(connection);
        Paciente encontrado;
        encontrado = dao.reconstruirPaciente(idPaciente);

        return encontrado;

    }

    private Paciente addPacienteToGnk(Hl7Message hl7message, Connection connection) throws SQLException, Exception {

        PatientPid pidpatient = hl7message.getPatientPid();

        Paciente paciente = new Paciente();
        String ValorIdHl7 = String.valueOf(pidpatient.getIdPaciente());
        paciente.setIdpac_hl7(ValorIdHl7);
        paciente.setApellidoPaterno(pidpatient.getApPaterno() != null ? pidpatient.getApPaterno().toUpperCase() : "APEPAT");
        paciente.setApellidoMaterno(pidpatient.getApMaterno() != null ? pidpatient.getApMaterno().toUpperCase() : "APEMAT");
        paciente.setNombre(pidpatient.getPrimerNombre() != null ? pidpatient.getPrimerNombre().toUpperCase() : "NOMPAC");

        paciente.setNombreCompleto(String.format("%s %s %s", paciente.getNombre(),
                paciente.getApellidoPaterno(), paciente.getApellidoMaterno()));

        String nacimiento = pidpatient.getFechaNacimiento();
        String formato = "%Y%m%d%H%i%S";
        String inicioVigencia = "20150101120000";
        String finVigencia = "20361231120000";
        if (nacimiento == null) {
            nacimiento = Paciente.SIN_NACIMIENTO;
            formato = Paciente.FORMATO_DEFECTO_NACIMIENTO;
            inicioVigencia = Paciente.SIN_INICIO_VIGENCIA;
            finVigencia = Paciente.SIN_FIN_VIGENCIA;
        }

        paciente.setNacimiento(nacimiento);
        paciente.setFormatoFecha(formato); //Se indica el formato de entrada de las fechas
        paciente.setSexo(pidpatient.getSexo() != null ? pidpatient.getSexo() : "-");
        paciente.setNumAfi("");
        paciente.setTipCob("PA");
        paciente.setInicioVigencia(inicioVigencia);
        paciente.setFinVigencia(finVigencia);
        paciente.setExpediente("S/E");
        paciente.setSts(Paciente.ACTIVO_STATUS);

        PacienteDaoImpl dao = new PacienteDaoImpl(connection);

        long id = dao.adicionarPaciente(paciente);
        paciente.setId(id);

        return paciente;

    }

    private Medico findMedicoHl7(Hl7Message message, Connection connection) throws SQLException {

        MedicoImpl dao = new MedicoImpl(connection);
        Medico medico = dao.reconstruirMedico(message.getPatientOrc().getProveedorOrden());
        return medico;
    }

    private Causes findCausesHl7(Hl7Message message, Connection connection) throws SQLException {

        CausesImpl dao = new CausesImpl(connection);

        Causes causesHl7 = dao.reconstruirCauses(message.getPatientFt1().getCodigoDiagnostico());
        return causesHl7;
    }

    private Medico addMedicoToGnk(Hl7Message hl7message, Connection connection) throws SQLException, Exception {

        Medico nuevo = new Medico();

        nuevo.setCedula(hl7message.getPatientOrc().getProveedorOrden());
        nuevo.setUnidad(hl7message.getPatientMsh().getIdUnidadMedica());

        String paterno = hl7message.getPatientOrc().getApellidoPaterno();
        paterno = paterno != null ? paterno : Medico.SIN_APELLIDO;
        paterno = paterno.toUpperCase();
        nuevo.setApellidoPaterno(paterno.toUpperCase());

        String materno = hl7message.getPatientOrc().getApellidoMaterno();
        materno = materno != null ? materno : Medico.SIN_APELLIDO;
        materno = materno.toUpperCase();
        nuevo.setApellidoMaterno(materno);

        String nombre = hl7message.getPatientOrc().getPrimerNombre();
        nombre = nombre != null ? nombre : Medico.SIN_NOMBRE;
        nombre = nombre.toUpperCase();
        nuevo.setNombre(nombre);
        nuevo.setNombreCompleto(nombre, paterno, materno);
        nuevo.setRfc(Medico.SIN_APELLIDO);
        nuevo.setStatus(Medico.ACTIVO_STATUS);
        nuevo.setNacimiento(Medico.SIN_NACIMIENTO);
        nuevo.setFolioInicial(Medico.SIN_FOLIO);
        nuevo.setFolioFinal(Medico.SIN_FOLIO);
        nuevo.setFolioActual(Medico.SIN_FOLIO);
        nuevo.setWeb(Medico.SIN_WEB);

        MedicoImpl dao = new MedicoImpl(connection);
        int idMedico = dao.adicionarMedico(nuevo);
        nuevo.setId(idMedico);

        return nuevo;

    }

    private Causes addCausesToGnk(Hl7Message hl7message, Connection connection) throws SQLException, Exception {

        Causes nuevoCauses = new Causes();

        nuevoCauses.setIdCauses(hl7message.getPatientFt1().getCodigoDiagnostico());
        nuevoCauses.setDesCauses(hl7message.getPatientFt1().getNombreCodigoDiagnostico());

        CausesImpl dao = new CausesImpl(connection);
        String idcauses = dao.adicionarCauses(nuevoCauses);
        nuevoCauses.setIdCauses(idcauses);

        return nuevoCauses;

    }

    private MessageHL7Service getService(Connection con) {
        if (this.serviceHL7 == null) {
            this.serviceHL7 = new MessageHL7Service(con);
        }

        return this.serviceHL7;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ProcessHl7Message.class.getName()).log(Level.SEVERE, null, ex);
            throw new ServletException("No puedo procesarse el mensaje.", ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(ProcessHl7Message.class.getName()).log(Level.SEVERE, null, ex);
            throw new ServletException("No puedo procesarse el mensaje.", ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
