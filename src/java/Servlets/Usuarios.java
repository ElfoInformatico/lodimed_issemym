/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Dao.adminUsuDao;
import Impl.adminUsuDaoImpl;
import Modelos.Usuario;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Administración de usuarios
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class Usuarios extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        HttpSession sesion = request.getSession(true);
        int accion = Integer.parseInt(request.getParameter("ban"));

        switch (accion) {
            case 0:
                int id = Integer.parseInt(request.getParameter("id"));
                Map TUsu = new LinkedHashMap();
                try {
                    Usuario u;
                    adminUsuDao ua = new adminUsuDaoImpl();
                    u = ua.Usu(id);
                    TUsu.put("Nom", u.getNom());
                    TUsu.put("Ap", u.getaP());
                    TUsu.put("Am", u.getaM());
                    TUsu.put("Uni", u.getUni());
                    TUsu.put("User", u.getUsu());
                    TUsu.put("Sts", u.getDesBaj());
                    TUsu.put("Id", u.getId());
                    String json1;
                    json1 = new Gson().toJson(TUsu);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().write(json1);

                } catch (Exception e) {
                    Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, e);
                }

                break;
            case 1:
                id = Integer.parseInt(request.getParameter("id"));
                String nom = request.getParameter("nom");
                String ap = request.getParameter("ap");
                String am = request.getParameter("am");
                String pass = request.getParameter("pass");
                String sts = request.getParameter("sts");

                try {

                    TUsu = new LinkedHashMap();
                    adminUsuDao ua = new adminUsuDaoImpl();
                    boolean save = ua.edit(id, nom, ap, am, pass, sts);
                    if (save) {
                        TUsu.put("msj", "Usuario editado con éxito.");
                    } else {
                        TUsu.put("msj", "Error contactar al departamento de sistemas.");
                    }
                    String json1;
                    json1 = new Gson().toJson(TUsu);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().write(json1);
                } catch (Exception e) {
                    Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, e);
                }
                break;

            case 2:

                nom = request.getParameter("nom");
                ap = request.getParameter("ap");
                am = request.getParameter("am");
                pass = request.getParameter("pass");
                String usu = request.getParameter("usu");
                String uni = (String) sesion.getAttribute("cla_uni");
                int rol = Integer.parseInt(request.getParameter("rol"));
                try {

                    TUsu = new LinkedHashMap();
                    adminUsuDao ua = new adminUsuDaoImpl();

                    boolean save = ua.save(nom, ap, am, pass, usu, uni, rol);

                    if (save) {

                        TUsu.put("msj", "Usuario creado con éxito.");

                    } else {
                        TUsu.put("msj", "Error contactar al departamento de sistemas.");
                    }

                    String json1;
                    json1 = new Gson().toJson(TUsu);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().write(json1);

                } catch (Exception e) {
                    Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, e);
                }

                break;
            case 3:
                uni = (String) sesion.getAttribute("cla_uni");
                try {
                    TUsu = new LinkedHashMap();
                    adminUsuDao ua = new adminUsuDaoImpl();
                    String json1;

                    String unidad = ua.uniName(uni);
                    TUsu.put("uni", unidad);

                    json1 = new Gson().toJson(TUsu);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().write(json1);
                } catch (Exception e) {
                    Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, e);
                }

                break;
            case 4:
                usu = request.getParameter("usu");
                try {
                    TUsu = new LinkedHashMap();
                    adminUsuDao ua = new adminUsuDaoImpl();
                    String json1;

                    String usuario = ua.user(usu);

                    if (usuario.equals("uso")) {
                        TUsu.put("msj", "ocupado");
                    } else {
                        TUsu.put("msj", usuario);
                    }

                    json1 = new Gson().toJson(TUsu);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().write(json1);
                } catch (Exception e) {
                    Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, e);
                }
                break;
            case 5:
                id = Integer.parseInt(request.getParameter("id"));

                JSONObject json = new JSONObject();
                JSONArray jsona = new JSONArray();

                try {

                    adminUsuDao ua = new adminUsuDaoImpl();
                    if (ua.borrar(id)) {
                        json.put("resp", "si");
                        out.println();
                        jsona.add(json);
                        out.println(jsona);
                    }

                } catch (Exception e) {
                    Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, e);
                }

                break;
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
