package Servlets;

import Clases.ConectionDB;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

/**
 * Generación de reporte kardex a excel
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
@WebServlet(name = "KardexTotalXlsx", urlPatterns = {"/KardexTotalXlsx"})
public class KardexTotalXlsx extends HttpServlet {

    private static final String SAVE_DIR = "Kardex Total";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ConectionDB con = new ConectionDB();
        PreparedStatement ps;
        ResultSet rs;
        String query;

        query = "SELECT dp.cla_pro AS clave, dp.lot_pro AS lote, dp.cad_pro AS caducidad, dp.id_ori AS origen, IF ( LOCATE('SALIDA', k.tipo_mov) > 0, k.cant * - 1, k.cant ) AS cantidad, k.tipo_mov AS movimiento, k.fol_aba AS fol_aba, r.fol_rec AS folio, IF ( LOCATE('SALIDA RECETA', k.tipo_mov) > 0, pa.nom_com, '-' ) AS paciente, IF ( LOCATE('SALIDA RECETA', k.tipo_mov) > 0, m.nom_com, '-' ) AS medico, k.fecha AS fecha, k.obser AS obser, u.nombre_completo AS usuario FROM kardex AS k INNER JOIN detalle_productos AS dp ON k.det_pro = dp.det_pro LEFT JOIN receta AS r ON r.id_rec = k.id_rec INNER JOIN pacientes AS pa ON r.id_pac = pa.id_pac INNER JOIN medicos AS m ON m.cedula = r.cedula INNER JOIN usuarios AS u ON k.id_usu = u.id_usu WHERE k.cant > 0 ORDER BY fecha ASC;";

        String leyenda = "Kardex Total   ";
        String nombre = "kardexTotal.xlsx";

        PrintWriter pw = null;
        try {

            response.setContentType("text/html;charset=UTF-8");

            String path = request.getServletContext().getRealPath("");
            String rutaCarpeta = path + File.separatorChar + SAVE_DIR;

            //Verifica existencia carpeta
            File carpeta = new File(rutaCarpeta);
            if (!carpeta.exists()) {
                carpeta.mkdir();
            }

            String filename = rutaCarpeta + File.separatorChar + "KardexTotal.xlsx";
            String sheetName = "Kardex";
            SXSSFWorkbook wb = new SXSSFWorkbook(1000);

            SXSSFSheet sheet = wb.createSheet(sheetName);
            int width = 20;
            sheet.setAutobreaks(true);
            sheet.setDefaultColumnWidth(width);

            int index = 0, sumatoria = 0;

            Row rowLeyenda = sheet.createRow(index);
            Cell cell = rowLeyenda.createCell(0);
            cell.setCellValue(leyenda);
            sheet.addMergedRegion(new CellRangeAddress(
                    0, //first row (0-based)
                    0, //last row  (0-based)
                    0, //first column (0-based)
                    2 //last column  (0-based)
            ));
            index++;
            rowLeyenda = sheet.createRow((short) index);
            cell = rowLeyenda.createCell((short) 0);
            cell.setCellValue("");
            sheet.addMergedRegion(new CellRangeAddress(
                    1, //first row (0-based)
                    1, //last row  (0-based)
                    0, //first column (0-based)
                    2 //last column  (0-based)
            ));

            index++;
            SXSSFRow rowHeadInv = sheet.createRow(index);
            rowHeadInv.createCell(0).setCellValue("Clave");
            rowHeadInv.createCell(1).setCellValue("Lote");
            rowHeadInv.createCell(2).setCellValue("Caducidad");
            rowHeadInv.createCell(3).setCellValue("Origen");
            rowHeadInv.createCell(4).setCellValue("Cantidad");
            rowHeadInv.createCell(5).setCellValue("Tipo Movimiento");
            rowHeadInv.createCell(6).setCellValue("Abasto");
            rowHeadInv.createCell(7).setCellValue("Folio Receta");
            rowHeadInv.createCell(8).setCellValue("Paciente");
            rowHeadInv.createCell(9).setCellValue("Médico");
            rowHeadInv.createCell(10).setCellValue("Fecha");
            rowHeadInv.createCell(11).setCellValue("Observaciones");
            rowHeadInv.createCell(12).setCellValue("Usuario");
            rowHeadInv.createCell(13).setCellValue("Sumatoria");

            con.conectar();
            ps = con.getConn().prepareStatement(query);

            rs = ps.executeQuery();
            SXSSFRow row;
            while (rs.next()) {
                index++;
                row = sheet.createRow(index);
                sumatoria += rs.getInt("cantidad");
                row.createCell(0).setCellValue(rs.getString("clave"));
                row.createCell(1).setCellValue(rs.getString("lote"));
                row.createCell(2).setCellValue(rs.getString("caducidad"));
                row.createCell(3).setCellValue(rs.getString("origen"));
                row.createCell(4).setCellValue(rs.getDouble("cantidad"));
                row.createCell(5).setCellValue(rs.getString("movimiento"));
                row.createCell(6).setCellValue(rs.getString("fol_aba"));
                row.createCell(7).setCellValue(rs.getString("folio"));
                row.createCell(8).setCellValue(rs.getString("paciente"));
                row.createCell(9).setCellValue(rs.getString("medico"));
                row.createCell(10).setCellValue(rs.getString("fecha"));
                row.createCell(11).setCellValue(rs.getString("obser"));
                row.createCell(12).setCellValue(rs.getString("usuario"));
                row.createCell(13).setCellValue(sumatoria);

            }
            try (FileOutputStream fileOut = new FileOutputStream(filename)) {
                wb.write(fileOut);
            }
            rs.close();
            ps.close();
            wb.dispose();
            wb.close();

            String disHeader = "Attachment;Filename=\"" + nombre + "\"";
            response.setHeader("Content-Disposition", disHeader);
            File desktopFile = new File(filename);
            OutputStream outStream = response.getOutputStream();
            try (FileInputStream fileInputStream = new FileInputStream(desktopFile)) {
                int j;
                byte[] buffer = new byte[4096];
                while ((j = fileInputStream.read(buffer)) != -1) {
                    outStream.write(buffer, 0, j);
                }
                outStream.flush();
                outStream.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(KardexTotalXlsx.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                response.flushBuffer();
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (IOException | SQLException ex) {
                Logger.getLogger(KardexTotalXlsx.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
