/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Impl.catDaoImpl;
import Modelos.catalogo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONArray;

/**
 * Reporte catálogos
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class Catalogo extends HttpServlet {

    public static final int OBTENER_CATALOGO_NIVEL = 1;
    public static final int OBTENER_CATALAGO_CAUSES = 2;
    public static final int OBTENER_CATALAGO_PRODUCTOS = 3;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Catalogo</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Catalogo at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession sesion = request.getSession(true);
        //Variable la cual define la operacion a realizar se obtiene el parametro desde ajax
        int bandera = Integer.parseInt(request.getParameter("ban"));
        Map resultado = new LinkedHashMap();
        JSONArray lista;
        String json1;
        catDaoImpl dao = new catDaoImpl();
        try {
            int nivel = Integer.parseInt((String) sesion.getAttribute("nivel"));
            Gson gson = new GsonBuilder().create();
            switch (bandera) {
                case OBTENER_CATALOGO_NIVEL:
                    catalogo ct;
                    ct = dao.lc(nivel);
                    resultado.put("nivel", nivel);
                    resultado.put("ca", ct.getCc());
                    resultado.put("vd", ct.getVd());
                    resultado.put("tot", ct.getTotCl());
                    json1 = gson.toJson(resultado);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().write(json1);
                    break;

                case OBTENER_CATALAGO_CAUSES:
                    lista = dao.obtenerCauses();
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().print(lista);

                    break;

                case OBTENER_CATALAGO_PRODUCTOS:
                    lista = dao.obtenerDescripcionProductos();
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().print(lista);

                    break;
            }
        } catch (IOException ex) {
            Logger.getLogger(Operaciones.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
