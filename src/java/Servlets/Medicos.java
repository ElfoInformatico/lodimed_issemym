/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Clases.ConectionDB;
import Impl.MedicoImpl;
import Modelos.Medico;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Registro de médicos
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
@MultipartConfig
public class Medicos extends HttpServlet {

/**
 * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
 *
 * @param request servlet request
 * @param response servlet response
 * @throws ServletException if a servlet-specific error occurs
 * @throws IOException if an I/O error occurs
 */
private static final String SAVE_DIR = "reportes/imagen/firmas";

@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
    request.setCharacterEncoding("UTF-8");
    response.setContentType("text/html");
    ConectionDB con = new ConectionDB();
    response.setContentType("");
    PrintWriter out = response.getWriter();
    try {
        JSONObject json = new JSONObject();
        JSONArray jsona = new JSONArray();

        String fechaNac = request.getParameter("fec_nac");

        con.conectar();

        int foliop = 0;
        String Unidad;

        String ape_pat = request.getParameter("ape_pat").toUpperCase();
        String ape_mat = request.getParameter("ape_mat").toUpperCase();
        String tipoConsu = request.getParameter("slcTipoConsu");
        String nombre = request.getParameter("nombre").toUpperCase();
        String imagen = request.getParameter("chkImg");
        Unidad = request.getParameter("cla_uni");
        MedicoImpl dao = new MedicoImpl(con.getConn());

        con.getConn().setAutoCommit(false);
        Medico nuevo = dao.reconstruirMedico(nombre, ape_pat, ape_mat);
        if (nuevo != null) {
            out.println("<script>alert('Médico existente verificar datos.')</script>");
            out.println("<script>window.location='admin/medicos/alta_medico.jsp'</script>");
            out.println();
            jsona.add(json);
            out.println(jsona);
            out.close();
            con.cierraConexion();
            return;
        }

        nuevo = new Medico();
        nuevo.setNombre(nombre);
        nuevo.setApellidoMaterno(ape_mat);
        nuevo.setApellidoPaterno(ape_pat);
        nuevo.setNombreCompleto(nombre, ape_pat, ape_mat);
        nuevo.setNacimiento(fechaNac);
        nuevo.setRfc(request.getParameter("rfc").toUpperCase());
        nuevo.setUnidad(Unidad);
        nuevo.setCedula(request.getParameter("cedula"));
        nuevo.setStatus(Medico.ACTIVO_STATUS);
        nuevo.setFolioInicial(Integer.valueOf(request.getParameter("folIni")));
        nuevo.setFolioFinal(Integer.valueOf(request.getParameter("folFin")));
        nuevo.setFolioActual(nuevo.getFolioInicial());
        nuevo.setTipoConsulta(tipoConsu);

        foliop = dao.adicionarMedico(nuevo);

        con.insertar("insert into usuarios values(0,'" + nombre.toUpperCase() + "','" + request.getParameter("usuario") + "',MD5('" + request.getParameter(
                  "password") + "'),'2','1','" + Unidad + "','" + foliop + "','','" + request.getParameter("ape_pat") + "','" + request.getParameter("ape_mat")
                  + "', '" + nombre.toUpperCase() + " " + request.getParameter("ape_pat") + " " + request.getParameter("ape_mat") + "')");
        try {
            if (imagen == null) {
                String pad = getServletContext().getRealPath("/imagenes/blanca.jpg");
                String rutaServlet = request.getServletContext().getRealPath("");
                String rutaCarpeta = rutaServlet + SAVE_DIR;
                String nombreArchivo = String.format("%s.jpg", request.getParameter("cedula"));
                String rutaArchvio = rutaCarpeta + File.separator + nombreArchivo;
                try {
                    File inFile = new File(pad);
                    File outFile = new File(rutaArchvio);

                    FileOutputStream salida;
                    try (FileInputStream original = new FileInputStream(inFile)) {
                        salida = new FileOutputStream(outFile);
                        int c;
                        while ((c = original.read()) != -1) {
                            salida.write(c);
                        }
                    }
                    salida.close();
                    out.println("<script>alert('El médico se agregó correctamente')</script>");
                    out.println("<script>window.location='admin/medicos/alta_medico.jsp'</script>");

                } catch (IOException e) {
                    Logger.getLogger(Medicos.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                }

            } else {
                Part firma = request.getPart("txtFirma");

                String fileName = Paths.get(firma.getSubmittedFileName()).getFileName().toString();
                InputStream fileContent = firma.getInputStream();

                String rutaServlet = request.getServletContext().getRealPath("");
                String rutaCarpeta = rutaServlet + SAVE_DIR;
                String nombreArchivo = String.format("%s.jpg", request.getParameter("cedula"));
                String rutaArchvio = rutaCarpeta + File.separator + nombreArchivo;

                File fileSaveDir = new File(rutaCarpeta);
                if (!fileSaveDir.exists()) {
                    fileSaveDir.mkdir();
                }

                firma.write(rutaArchvio);
            }
            out.println("<script>alert('El médico se agregó correctamente')</script>");
            out.println("<script>window.location='admin/medicos/alta_medico.jsp'</script>");

        } catch (UnsupportedEncodingException | NumberFormatException e) {
            json.put("mensaje", "El Médico no se pudo dar de alta");
            Logger.getLogger(Medicos.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }

        con.getConn().commit();
        out.println();
        jsona.add(json);
        out.println(jsona);

    } catch (IOException | ServletException e) {
        Logger.getLogger(Medicos.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        try {
            con.getConn().rollback();
        } catch (SQLException ex1) {
            Logger.getLogger(Medicos.class.getName()).log(Level.SEVERE, null, ex1);
        }
        out.println("<script>window.location='admin/medicos/alta_medico.jsp'</script>");
        out.println("<script>alert('Error al registrar al médico contactar al departamento de sistemas.')</script>");
    } catch (SQLException ex) {
        try {
            con.getConn().rollback();
        } catch (SQLException ex1) {
            Logger.getLogger(Medicos.class.getName()).log(Level.SEVERE, null, ex1);
        }
        Logger.getLogger(Medicos.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
    } finally {
        try {
            if (con.estaConectada()) {
                con.cierraConexion();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Medicos.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        }
    }

}

/**
 * Returns a short description of the servlet.
 *
 * @return a String containing servlet description
 */
@Override
public String getServletInfo() {
    return "Short description";
}// </editor-fold>

}
