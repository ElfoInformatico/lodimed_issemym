/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Dao.reabDao;
import Impl.reabDaoImpl;
import Modelos.reabast;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import Clases.ConectionDB;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Creación de reabastecimiento
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class Reabastecimiento extends HttpServlet {

/**
 * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
 *
 * @param request servlet request
 * @param response servlet response
 * @throws ServletException if a servlet-specific error occurs
 * @throws IOException if an I/O error occurs
 */
protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    try (PrintWriter out = response.getWriter()) {
        /* TODO output your page here. You may use following sample code. */
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Servlet Reabastecimiento</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Servlet Reabastecimiento at " + request.getContextPath() + "</h1>");
        out.println("</body>");
        out.println("</html>");
    }
}

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
/**
 * Handles the HTTP <code>GET</code> method.
 *
 * @param request servlet request
 * @param response servlet response
 * @throws ServletException if a servlet-specific error occurs
 * @throws IOException if an I/O error occurs
 */
@Override
protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
}

/**
 * Handles the HTTP <code>POST</code> method.
 *
 * @param request servlet request
 * @param response servlet response
 * @throws ServletException if a servlet-specific error occurs
 * @throws IOException if an I/O error occurs
 */
@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

    PrintWriter out = response.getWriter();
    response.setContentType("text/html");
    HttpSession sesion = request.getSession(true);
    try {
        int bandera = Integer.parseInt(request.getParameter("ban"));
        switch (bandera) {
        case 1:
            JSONObject json = new JSONObject();
            JSONArray jsona = new JSONArray();

            reabDao rea = new reabDaoImpl();

            List<reabast> lr = new ArrayList<>();
            lr = rea.lR();
            for (int i = 0; i < lr.size(); i++) {
                reabast r = new reabast();
                r = (reabast) lr.get(i);

                json.put("cl", r.getClave());
                json.put("des", r.getDes());
                json.put("cdm", r.getCdm());
                json.put("ifs", r.getIfs());
                json.put("exist", r.getInv());
                json.put("sob", r.getSobreAbasto());
                json.put("cs", r.getCantSug());

                jsona.add(json);
                json = new JSONObject();

            }
            out.print(jsona);
            out.close();

            break;
        case 2:
            json = new JSONObject();
            jsona = new JSONArray();
            int dias = Integer.parseInt(request.getParameter("dias"));
            rea = new reabDaoImpl();

            lr = new ArrayList<>();
            lr = rea.dias(dias);
            for (int i = 0; i < lr.size(); i++) {
                reabast r = new reabast();
                r = (reabast) lr.get(i);

                json.put("cl", r.getClave());
                json.put("des", r.getDes());
                json.put("cdm", r.getCdm());
                json.put("ifs", r.getIfs());
                json.put("exist", r.getInv());
                json.put("sob", r.getSobreAbasto());
                json.put("cs", r.getCantSug());

                jsona.add(json);
                json = new JSONObject();

            }
            out.print(jsona);
            out.close();

            break;
        case 3:
            Map TUsu = new LinkedHashMap();
            jsona = new JSONArray();
            String cu = (String) sesion.getAttribute("cla_uni");

            rea = new reabDaoImpl();

            String arriba = rea.upload(cu);
            switch (arriba) {
            case "esta":
                TUsu.put("msj", "El requerimiento ya fué enviado.");
                break;
            case "Exito":
                TUsu.put("msj", "El requerimiento se envió con éxito");
                break;
            case "fallo":
                TUsu.put("msj", "Error al enviar el requerimiento contactar al departamento de sistemas.");
                break;
            }

            String json1;
            json1 = new Gson().toJson(TUsu);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json1);

            break;
        }
    } catch (Exception e) {
    }
    String accion = request.getParameter("accion");

    switch (accion) {
    case "cargaReabastecimiento":
        JSONObject json = new JSONObject();
        JSONArray jsona = new JSONArray();
        reabDao rea = new reabDaoImpl();

        ConectionDB con = new ConectionDB();
         {
            try {
                con.conectar();
                ResultSet rset = con.consulta("select p.cla_pro, p.des_pro from reabastecimiento r INNER JOIN productos p on r.clave = p.cla_pro");
                while (rset.next()) {
                    String cantidad = request.getParameter("clave" + rset.getString(1));

                    if (cantidad.equals("")) {
                        cantidad = "0";
                    }
                    System.out.println("update reabastecimiento set sugerido = '" + cantidad + "' where clave = '" + rset.getString(1) + "'");
                    con.actualizar("update reabastecimiento set sugerido = '" + cantidad + "' where clave = '" + rset.getString(1) + "'");
                }
                con.cierraConexion();
            } catch (SQLException ex) {
                System.out.println(ex);
                Logger.getLogger(Reabastecimiento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Map TUsu = new LinkedHashMap();
        jsona = new JSONArray();
        String cu = (String) sesion.getAttribute("cla_uni");

        rea = new reabDaoImpl();

        String arriba = rea.upload(cu);
        switch (arriba) {
        case "esta":
            TUsu.put("msj", "El requerimiento ya fué enviado.");
            break;
        case "Exito":
            TUsu.put("msj", "El requerimiento se envió con éxito");
            break;
        case "fallo":
            TUsu.put("msj", "Error al enviar el requerimiento contactar al departamento de sistemas.");
            break;
        }
        out.println("<script>alert('Requerimiento subido correctamente')</script>");
        out.println("<script>window.location='reportes/rep_reabastecimiento_manual.jsp'</script>");
        break;
    }
}

public void cargaRequerimiento() throws SQLException {

}

/**
 * Returns a short description of the servlet.
 *
 * @return a String containing servlet description
 */
@Override
public String getServletInfo() {
    return "Short description";
}// </editor-fold>

}
