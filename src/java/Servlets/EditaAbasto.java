/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Clases.CargaAbasto;
import Clases.ConectionDB;
import Clases.LeerCSV;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONObject;

/**
 * Modificación del abasto
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class EditaAbasto extends HttpServlet {

private static final String UPDATE_PRODUCTO = "UPDATE carga_abasto SET lote =?, caducidad =?, cantidad =?, origen =?, observacion=? WHERE id =?";
private static final String SELECT_PRODUCTO
          = "SELECT carga_abasto.clave, carga_abasto.lote, carga_abasto.caducidad, carga_abasto.cantidad, carga_abasto.origen, carga_abasto.observacion FROM carga_abasto WHERE id =?";

public static final String SELECT_DETALLE
          = "SELECT ra.clave, ra.lote, ra.caducidad, ra.cantidad, ra.origen, ra.observacion, ra.cargado FROM reporte_abasto AS ra WHERE ra.nom_abasto = ?";

/**
 * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
 *
 * @param request servlet request
 * @param response servlet response
 * @throws ServletException if a servlet-specific error occurs
 * @throws IOException if an I/O error occurs
 */
protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    PrintWriter out = response.getWriter();
    HttpSession sesion = request.getSession();
    ConectionDB con = new ConectionDB();
    JSONObject json = new JSONObject();
    PreparedStatement ps;
    ResultSet rs;
    try {
        String accion = request.getParameter("accion");
        if (accion == null) {
            accion = "";
        }
        switch (accion) {
        case "Ver":
            sesion.setAttribute("id", request.getParameter("id"));
            break;
        case "Editar":
            String msnError = "Actualización existosa.";
            try {
                DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
                int id = Integer.parseInt(request.getParameter("id"));
                String clave = request.getParameter("clave");
                Date caducidad = new Date(df2.parse(request.getParameter("caduc")).getTime());
                String origen = request.getParameter("ori");
                String[] validacion = LeerCSV.validarCampos(id, clave, caducidad, origen, true);
                LeerCSV.EstadoCheck check = LeerCSV.EstadoCheck.fromInt(Integer.parseInt(validacion[0]));
                
                if (check != LeerCSV.EstadoCheck.SIN_ERROR) {
                    msnError = validacion[1];
                    throw new IOException(msnError);
                }
                
                String[] camposCambiados = request.getParameterValues("cambios[]");
                String[] valoresCambiados = request.getParameterValues("valores[]");
                String SELECT_ANTERIOR_DATA = "SELECT %s, observacion FROM carga_abasto WHERE id = ?";
                String campos = "";
                for (int i = 0; i < camposCambiados.length; i++) {
                    String campoCambiado = camposCambiados[i];
                    if (i == (camposCambiados.length - 1)) {
                        campos += campoCambiado;
                    } else {
                        campos += String.format("%s,", campoCambiado);
                    }
                }
                
                con.conectar();
                
                SELECT_ANTERIOR_DATA = String.format(SELECT_ANTERIOR_DATA, campos);
                ps = con.getConn().prepareStatement(SELECT_ANTERIOR_DATA);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                
                String auxObservacion = "";
                String anteriorObservacion = "";
                if (rs.next()) {
                    int i;
                    for (i = 0; i < camposCambiados.length; i++) {
                        String campoCambiado = camposCambiados[i];
                        String valorCambiado = valoresCambiados[i];
                        auxObservacion += String.format("%s: anterior(%s) - actual(%s) | ", campoCambiado, rs.getString(campoCambiado), valorCambiado);
                    }
                    anteriorObservacion = rs.getString(i + 1);
                } else {
                    msnError = "No se genero ningún cambio en la base de datos";
                    throw new SQLException(msnError, ps.toString());
                }
                String observacion = String.format("%s ||| CAMBIOS: %s JUSTIFICACIÓN: %s.",
                          anteriorObservacion, auxObservacion, request.getParameter("observacion"));
                
                ps = con.getConn().prepareStatement(UPDATE_PRODUCTO);
                ps.setString(1, request.getParameter("lote"));
                ps.setDate(2, caducidad);
                ps.setInt(3, Integer.parseInt(request.getParameter("cant")));
                ps.setInt(4, Integer.parseInt(request.getParameter("ori")));
                ps.setString(5, observacion);
                ps.setInt(6, id);
                
                if (ps.executeUpdate() == 0) {
                    msnError = "No se genero ningún cambio en la base de datos";
                    throw new SQLException(msnError, ps.toString());
                }
                
                ps = con.getConn().prepareStatement(SELECT_PRODUCTO);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                if (rs.next()) {
                    json.put("clave", rs.getString(1));
                    json.put("lote", rs.getString(2));
                    json.put("caducidad", rs.getString(3));
                    json.put("cantidad", rs.getString(4));
                    json.put("origen", rs.getString(5));
                    json.put("observacion", rs.getString(5));
                    
                } else {
                    msnError = "No se genero ningún cambio en la base de datos";
                    throw new SQLException(msnError, ps.toString());
                }
                
            } catch (IOException e) {
                Logger.getLogger(EditaAbasto.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            } catch (SQLException e) {
                String mensaje = String.format("m: %s, sql: %s", e.getMessage(), e.getSQLState());
                Logger.getLogger(EditaAbasto.class.getName()).log(Level.SEVERE, mensaje, e);
                msnError = (!msnError.equals("Actualización existosa.")) ? msnError : "Error al intentar conectar con la base de datos.";
            } catch (NumberFormatException | ParseException e) {
                Logger.getLogger(EditaAbasto.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                msnError = "Error al intentar actulizar el producto.";
            } finally {
                try {
                    if (con.estaConectada()) {
                        con.cierraConexion();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(EditaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                }
            }
            json.put("msg", msnError);
            out.println(json);
            break;
        
        case "Validar":
            try {
                CargaAbasto carga = new CargaAbasto();
                String mensaje = carga.cargaAbasto((String) sesion.getAttribute("cla_uni"), (String) sesion.getAttribute("id_usu"), (String) sesion.
                          getAttribute("nomArchivo"));
                if (mensaje.equals("1")) {
                    con.conectar();
                    con.actualizar(
                              "INSERT INTO reporte_abasto ( clave, lote, caducidad, cantidad, origen, cb, fecha, nom_abasto, cargado, observacion) SELECT clave, lote, caducidad, cantidad, origen, cb, NOW(), '"
                              + (String) sesion.getAttribute("nomArchivo") + "', cargado, observacion FROM carga_abasto");
                    out.print("Abasto cargado con éxito");
                    sesion.setAttribute("ver", "no");
                } else {
                    out.println(mensaje);
                }
            } catch (SQLException ex) {
                Logger.getLogger(EditaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                out.print("Error al guardar el reporte de abasto--");
            } finally {
                try {
                    if (con.estaConectada()) {
                        con.cierraConexion();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(EditaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                }
            }
            break;
        case "ValidarRural":
            try {
                String nomArchivo = (String) sesion.getAttribute("nomArchivo");
                nomArchivo = nomArchivo.substring(7, 11);
                CargaAbasto carga = new CargaAbasto();
                String mensaje = carga.cargaAbastoRural(request.getParameter("unidad"), (String) sesion.getAttribute("id_usu"), nomArchivo);
                if (mensaje.equals("1")) {
                    con.conectar();
                    con.actualizar(
                              "INSERT INTO reporte_abasto ( clave, lote, caducidad, cantidad, origen, cb, fecha, nom_abasto, cargado, observacion) SELECT clave, lote, caducidad, cantidad, origen, cb, NOW(), '"
                              + nomArchivo + "', cargado, observacion FROM carga_abasto");
                    out.print("Abasto cargado con éxito");
                    sesion.setAttribute("ver", "no");
                } else {
                    out.println(mensaje);
                }
            } catch (SQLException ex) {
                Logger.getLogger(EditaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                out.print("Error al guardar el reporte de abasto--");
            } finally {
                try {
                    if (con.estaConectada()) {
                        con.cierraConexion();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(EditaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                }
            }
            break;
        case "ValidarTraspaso":
            try {
                CargaAbasto carga = new CargaAbasto();
                String mensaje = carga.cargaTraspaso((String) sesion.getAttribute("cla_uni"), (String) sesion.getAttribute("id_usu"), (String) sesion.
                          getAttribute("nomArchivo"));
                if (mensaje.equals("1")) {
                    con.conectar();
                    con.actualizar(
                              "INSERT INTO reporte_abasto ( clave, lote, caducidad, cantidad, origen, cb, fecha, nom_abasto, cargado, observacion) SELECT clave, lote, caducidad, cantidad, origen, cb, NOW(), '"
                              + (String) sesion.getAttribute("nomArchivo") + "', cargado, observacion FROM carga_abasto");
                    out.print("Abasto cargado con éxito");
                    sesion.setAttribute("ver", "no");
                } else {
                    out.println(mensaje);
                }
            } catch (SQLException ex) {
                Logger.getLogger(EditaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                out.print("Error al guardar el reporte de abasto--");
            } finally {
                try {
                    if (con.estaConectada()) {
                        con.cierraConexion();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(EditaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                }
            }
            break;
        case "Detalle":
            String nombre = request.getParameter("nom");
            msnError = "";
            try {
                con.conectar();
                String SUM_CANTIDAD = "SELECT SUM(ra.cantidad) FROM reporte_abasto AS ra WHERE ra.nom_abasto = ?";
                ps = con.getConn().prepareStatement(SUM_CANTIDAD);
                ps.setString(1, nombre);
                rs = ps.executeQuery();
                if (!rs.isBeforeFirst()) {
                    msnError = "No se pudo obtener la cantidad de productos por abasto.";
                    throw new SQLException(msnError, ps.toString());
                }
                rs.next();
                json.put("cantidad", rs.getString(1));
                
                SUM_CANTIDAD = "SELECT SUM(ra.cantidad) FROM reporte_abasto AS ra WHERE ra.nom_abasto = ? AND ra.cargado = 1";
                ps = con.getConn().prepareStatement(SUM_CANTIDAD);
                ps.setString(1, nombre);
                rs = ps.executeQuery();
                if (!rs.isBeforeFirst()) {
                    msnError = "No se pudo obtener la cantidad de productos filtrados por abasto.";
                    throw new SQLException(msnError, ps.toString());
                }
                rs.next();
                json.put("cantidad_filtro", rs.getInt(1));
                
                ps = con.getConn().prepareStatement(SELECT_DETALLE);
                ps.setString(1, nombre);
                rs = ps.executeQuery();
                if (!rs.isBeforeFirst()) {
                    msnError = "No se pudo obtener el detalle del abasto.";
                    throw new SQLException(msnError, ps.toString());
                }
                JSONObject aux;
                int id = 0;
                while (rs.next()) {
                    aux = new JSONObject();
                    aux.put("clave", rs.getString(1));
                    aux.put("lote", rs.getString(2));
                    aux.put("caducidad", rs.getString(3));
                    aux.put("cantidad", rs.getString(4));
                    aux.put("origen", rs.getString(5));
                    aux.put("observacion", rs.getString(6));
                    aux.put("cargado", rs.getString(7));
                    json.put(String.valueOf(id), aux);
                    id += 1;
                }
                
            } catch (SQLException ex) {
                String msn = String.format("m:%s, sql: %s", ex.getMessage(), ex.getSQLState());
                Logger.getLogger(EditaAbasto.class.getName()).log(Level.SEVERE, msn, ex);
                msnError = "Error al intentar consultar en la base de datos";
            } finally {
                try {
                    if (con.estaConectada()) {
                        con.cierraConexion();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(EditaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                }
            }
            
            json.put("msg", msnError);
            out.println(json);
            break;
        }
    } finally {
        out.close();
    }
}

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
/**
 * Handles the HTTP <code>GET</code> method.
 *
 * @param request servlet request
 * @param response servlet response
 * @throws ServletException if a servlet-specific error occurs
 * @throws IOException if an I/O error occurs
 */
@Override
protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
}

/**
 * Handles the HTTP <code>POST</code> method.
 *
 * @param request servlet request
 * @param response servlet response
 * @throws ServletException if a servlet-specific error occurs
 * @throws IOException if an I/O error occurs
 */
@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
}

/**
 * Returns a short description of the servlet.
 *
 * @return a String containing servlet description
 */
@Override
public String getServletInfo() {
    return "Short description";
}// </editor-fold>

}
