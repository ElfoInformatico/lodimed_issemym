/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Clases.ConectionDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Movimiento al inventario ajuste
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class SalidaAjuste extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        ConectionDB con = new ConectionDB();
        JSONObject json = new JSONObject();
        JSONArray jsona = new JSONArray();
        HttpSession sesion = request.getSession(true);
        PreparedStatement ps;
        ResultSet rs;
        String accion;
        String query, claveProducto, cb, descProducto, claveObtenida, claveUnidad = null, idOrigen, descOrigen, lote, caducidad;

        accion = request.getParameter("accion");

        if (accion == null) {
            accion = "";
        }

        if (accion.equals("buscarInsumo")) {
            claveProducto = request.getParameter("clave");
            cb = request.getParameter("cb");
            try {

                con.conectar();

                String id_usu = (String) sesion.getAttribute("id_usu");

                query = "SELECT cla_uni FROM usuarios WHERE id_usu = ? ";
                ps = con.getConn().prepareStatement(query);
                ps.setString(1, id_usu);
                rs = ps.executeQuery();
                while (rs.next()) {
                    claveUnidad = rs.getString("cla_uni");
                }
                query = "SELECT cla_pro, des_pro FROM productos p, tb_codigob cb WHERE p.cla_pro = cb.F_Clave AND (p.cla_pro = ? OR cb.F_Cb=?) GROUP BY cla_pro";

                ps = con.getConn().prepareStatement(query);
                ps.setString(1, claveProducto);
                ps.setString(2, cb);
                rs = ps.executeQuery();
                while (rs.next()) {
                    descProducto = rs.getString("des_pro");
                    claveObtenida = rs.getString("cla_pro");
                    json.put("clave", claveObtenida);
                    json.put("descripcion", descProducto);
                    json.put("msg", "ok");
                }
                out.println(json);
            } catch (SQLException ex) {
                Logger.getLogger(SalidaAjuste.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            } finally {
                out.close();
                try {
                    if (con.estaConectada()) {
                        con.cierraConexion();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(SalidaAjuste.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                }
            }

        }
        if (accion.equals("traerOrigen")) {
            String clave = request.getParameter("clave");
            try {
                con.conectar();
                String id_usu = (String) sesion.getAttribute("id_usu");

                query = "SELECT cla_uni FROM usuarios WHERE id_usu = ? ";
                ps = con.getConn().prepareStatement(query);
                ps.setString(1, id_usu);
                rs = ps.executeQuery();
                while (rs.next()) {
                    claveUnidad = rs.getString("cla_uni");
                }
                //cargar lotes caducidades y origenes
                query = "SELECT origen.id_ori,origen.des_ori FROM existencias,origen WHERE cla_pro = ? and cla_uni=? AND origen.id_ori=existencias.id_ori group by existencias.id_ori order by existencias.id_ori";
                ps = con.getConn().prepareStatement(query);
                ps.setString(1, clave);
                ps.setString(2, claveUnidad);

                ResultSet rs2 = ps.executeQuery();

                while (rs2.next()) {
                    json.put("origen", rs2.getString("id_ori"));
                    json.put("desc", rs2.getString("des_ori"));
                    json.put("msg", "ok");
                    jsona.add(json);
                    json = new JSONObject();
                }
                out.println(jsona);

            } catch (SQLException ex) {
                Logger.getLogger(SalidaAjuste.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            } finally {
                out.close();
                try {
                    if (con.estaConectada()) {
                        con.cierraConexion();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(SalidaAjuste.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                }
            }
        }
        if (accion.equals("traerCaducidad")) {
            String clave = request.getParameter("clave");
            try {
                con.conectar();

                query = "SELECT cad_pro FROM detalle_productos WHERE cla_pro = ? ";
                ps = con.getConn().prepareStatement(query);
                ps.setString(1, clave);
                //System.out.println(ps);

                ResultSet rs2 = ps.executeQuery();

                while (rs2.next()) {
                    json.put("caducidad", rs2.getString("cad_pro"));
                    json.put("msg", "ok");
                    jsona.add(json);
                    json = new JSONObject();
                }
                out.println(jsona);
            } catch (SQLException ex) {
                Logger.getLogger(SalidaAjuste.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            } finally {
                out.close();
                try {
                    if (con.estaConectada()) {
                        con.cierraConexion();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(SalidaAjuste.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                }
            }
        }
        if (accion.equals("traerLotes")) {
            String clave = request.getParameter("clave");
            try {
                con.conectar();

                query = "SELECT lot_pro FROM detalle_productos WHERE cla_pro = ? ";
                ps = con.getConn().prepareStatement(query);
                ps.setString(1, clave);

                ResultSet rs2 = ps.executeQuery();

                while (rs2.next()) {
                    json.put("lote", rs2.getString("lot_pro"));
                    json.put("msg", "ok");
                    jsona.add(json);
                    json = new JSONObject();
                }
                out.println(jsona);
            } catch (SQLException ex) {
                Logger.getLogger(SalidaAjuste.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            } finally {
                out.close();
                try {
                    if (con.estaConectada()) {
                        con.cierraConexion();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(SalidaAjuste.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                }
            }
        }
        if (accion.equals("seleccionar")) {

            try {

                String cantidadExistente;
                con.conectar();
                claveProducto = request.getParameter("clave");
                idOrigen = request.getParameter("id_ori");
                lote = request.getParameter("lot_pro");
                caducidad = request.getParameter("cad_pro");

                query = "SELECT ex.cant, ex.id_inv, o.des_ori FROM existencias AS ex, origen AS o WHERE o.id_ori = ex.id_ori AND ex.cla_pro = ? AND ex.lot_pro = ? AND ex.cad_pro = ? AND o.id_ori = ? ";
                ps = con.getConn().prepareStatement(query);
                ps.setString(1, claveProducto);
                ps.setString(2, lote);
                ps.setString(3, caducidad);
                ps.setString(4, idOrigen);
                rs = ps.executeQuery();
                while (rs.next()) {
                    cantidadExistente = rs.getString("cant");
                    json.put("cantidadExistente", cantidadExistente);
                    json.put("id_inv", rs.getString(2));
                    json.put("msg", "ok");
                    json.put("lote", lote);
                    json.put("caducidad", caducidad);
                    json.put("origen", rs.getString("des_ori"));
                }
                out.println(json);
            } catch (SQLException ex) {
                Logger.getLogger(SalidaAjuste.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            } finally {
                out.close();
                try {
                    if (con.estaConectada()) {
                        con.cierraConexion();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(SalidaAjuste.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                }
            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
