/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Clases.ConectionDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Americo
 */
public class ReportesFacturacion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            ConectionDB con = new ConectionDB();
            /* TODO output your page here. You may use following sample code. */

 /*out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Servlet ReportesFacturacion</title>");        
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Servlet ReportesFacturacion at " + request.getContextPath() + "</h1>");
        out.println("</body>");
        out.println("</html>");*/
            String accion = request.getParameter("accion");
            switch (accion) {
                case "generaPDFJuris":
                    String juris = request.getParameter("juris");
                    String tipoRural = request.getParameter("tipoRural");
                    String fechaInicial = request.getParameter("fechaInicial");
                    String fechaFinal = request.getParameter("fechaFinal");
                    String remision = request.getParameter("remision");
                    String tipo_producto = request.getParameter("tipoProducto");
                    int remisionI = Integer.parseInt(remision);
                    try {
                        con.conectar();
                        ResultSet rset = con.consulta("select DISTINCT(unidad), folio as unidad from reporte_rurales where JURISDICCION = '" + juris + "' AND Fecha BETWEEN '"
                                + fechaInicial + "' AND '" + fechaFinal + "'");
                        out.println("<html><body>");
                        out.println("<script type=\"text/javascript\">");
                        while (rset.next()) {
                            //response.sendRedirect("../reportes/ReporteFacturacionRuralPDF.jsp");
                            out.println("window.open(\"" + "reportes/ReporteFacturacionRuralPDF.jsp?folio=" + rset.getString(2) + "&unidad=" + rset.getString(1)
                                    + "&fechaInicial="
                                    + fechaInicial + "&jurisdiccion=" + juris
                                    + "&fechaFinal=" + fechaFinal + "&tipoRural=" + tipoRural + "&tipo_producto=" + tipo_producto + "&remision=" + remisionI + " \")");
                            remisionI++;
                        }

                        con.cierraConexion();
                        out.println("window.close()");
                        out.println("</script>");
                        out.println("</body></html>");
                    } catch (SQLException e) {
                        System.out.println(e);
                    }
                    break;

                case "generaXLS":
                    try {
                        fechaInicial = request.getParameter("fechaInicial");
                        fechaFinal = request.getParameter("fechaFinal");
                        con.conectar();

                        ResultSet rset = con.consulta("select DISTINCT(cla_uni) from usuarios");

                        String[] clasificacion = {"ABIERTO", "CONTROLADO"};
                        String[] tipoReporte = {"1", "2"};
                        String[] cobertura = {"SEGURO POPULAR", "SIGLO XXI", "POBLACIÓN ABIERTA", "GASTOS CATASTROFICOS"};
                        String[] tipoProducto = {"MEDICAMENTO", "MATERIAL DE CURACION"};
                        int[] origen = {1, 2};

                        out.println("<html><body>");
                        out.println("<script type=\"text/javascript\">");
                        while (rset.next()) {
                            for (int m = 0; m < tipoReporte.length; m++) {
                                for (int l = 0; l < tipoProducto.length; l++) {
                                    for (int k = 0; k < clasificacion.length; k++) {
                                        for (int j = 0; j < cobertura.length; j++) {
                                            for (int i = 0; i < origen.length; i++) {
                                                //response.sendRedirect("../reportes/ReporteFacturacionRuralPDF.jsp");

                                                out.println("window.open(\"" + "reportes/ReportesFacturacionXLS.jsp?cla_uni=" + rset.getString(1) + "&fecha_inicial="
                                                        + fechaInicial + "&fecha_final=" + fechaFinal + "&clasificacion=" + clasificacion[k] + "&cobertura="
                                                        + cobertura[j] + "&origen=" + origen[i] + "&tipoProducto=" + tipoProducto[l] + "&tipoReporte="
                                                        + tipoReporte[m]
                                                        + " \")");
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        con.cierraConexion();
                        out.println("window.close()");
                        out.println("</script>");
                        out.println("</body></html>");
                    } catch (SQLException e) {
                        System.out.println(e);
                    }

                    break;
                case "generaXLSRurales":
                    try {
                        fechaInicial = request.getParameter("fechaInicial");
                        fechaFinal = request.getParameter("fechaFinal");
                        con.conectar();

                        ResultSet rset = con.consulta("select DISTINCT(folio) from reporte_rurales where fecha BETWEEN '" + fechaInicial + "' and '" + fechaFinal + "'");
                        out.println("<html><body>");
                        out.println("<script type=\"text/javascript\">");
                        while (rset.next()) {
                            //response.sendRedirect("../reportes/ReporteFacturacionRuralPDF.jsp");
                            out.println("window.open(\"" + "reportes/ReportesFacturacionRuralesXLS.jsp?folio=" + rset.getString(1) + "&fecha_inicial="
                                    + fechaInicial
                                    + "&fecha_final=" + fechaFinal + " \")");

                        }

                        con.cierraConexion();
                        out.println("window.close()");
                        out.println("</script>");
                        out.println("</body></html>");
                    } catch (SQLException e) {
                        System.out.println(e);
                    }
                    break;
            }

        }
    }

    public void generaPDF() {

    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
