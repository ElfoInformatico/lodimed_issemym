/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Clases.ConectionDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Reporte de los movimientos
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class ReporteMovClaes extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        ConectionDB con = new ConectionDB();
        JSONObject json = new JSONObject();
        JSONArray jsona = new JSONArray();
        PreparedStatement ps;
        ResultSet rs;
        String accion;
        String query;

        accion = request.getParameter("accion");

        if (accion == null) {
            accion = "";
        }

        if (accion.equals("alto")) {
            try {
                con.conectar();
                query = "SELECT cla_pro, des_pro, sum(cant_sur) AS cant_sur FROM recetas GROUP BY cla_pro ORDER BY cant_sur DESC LIMIT 0, 10";

                ps = con.getConn().prepareStatement(query);
                rs = ps.executeQuery();
                while (rs.next()) {
                    json.put("clave", rs.getString(1));
                    json.put("descripcion", rs.getString(2));
                    json.put("cantidad", rs.getString(3));
                    json.put("msg", "ok");
                    jsona.add(json);
                    json = new JSONObject();
                }
                out.println(jsona);
                con.cierraConexion();

            } catch (SQLException ex) {
                Logger.getLogger(ReporteMovClaes.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();
                try {
                    con.cierraConexion();
                } catch (SQLException ex) {
                    Logger.getLogger(ReporteMovClaes.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        if (accion.equals("bajo")) {
            try {
                con.conectar();
                query = "SELECT cla_pro, des_pro, sum(cant_sur) AS cant_sur FROM recetas WHERE cant_sur != 0 GROUP BY cla_pro ORDER BY cant_sur ASC LIMIT 0, 10";

                ps = con.getConn().prepareStatement(query);
                rs = ps.executeQuery();
                while (rs.next()) {
                    json.put("clave", rs.getString(1));
                    json.put("descripcion", rs.getString(2));
                    json.put("cantidad", rs.getString(3));
                    json.put("msg", "ok");
                    jsona.add(json);
                    json = new JSONObject();
                }
                out.println(jsona);
                con.cierraConexion();

            } catch (SQLException ex) {
                Logger.getLogger(ReporteMovClaes.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();
                try {
                    con.cierraConexion();
                } catch (SQLException ex) {
                    Logger.getLogger(ReporteMovClaes.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        if (accion.equals("nulo")) {
            try {
                con.conectar();
                query = "SELECT cla_pro, des_pro FROM productos WHERE f_status ='A' and cla_pro NOT IN (SELECT distinct cla_pro FROM recetas)";

                ps = con.getConn().prepareStatement(query);
                rs = ps.executeQuery();
                while (rs.next()) {
                    json.put("clave", rs.getString(1));
                    json.put("descripcion", rs.getString(2));
                    json.put("msg", "ok");
                    jsona.add(json);
                    json = new JSONObject();
                }
                out.println(jsona);
                con.cierraConexion();

            } catch (SQLException ex) {
                Logger.getLogger(ReporteMovClaes.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();
                try {
                    con.cierraConexion();
                } catch (SQLException ex) {
                    Logger.getLogger(ReporteMovClaes.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
