/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Clases.ConectionDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Modificación datos pacientes
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class ModiPacientes extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        ConectionDB con = new ConectionDB();
        PrintWriter out = response.getWriter();
        try {
            JSONObject json = new JSONObject();
            JSONArray jsona = new JSONArray();

            DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy");
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            con.conectar();

            try {

                String ape_pat = request.getParameter("ape_pat");
                String ape_mat = request.getParameter("ape_mat");
                String nombre = request.getParameter("nombre");
                String folio = request.getParameter("");
                String completo = ape_pat.toUpperCase() + " " + ape_mat.toUpperCase() + " " + nombre.toUpperCase();

                con.actualizar("update pacientes set num_afi='" + request.getParameter("no_afi") + "',tip_cob='" + request.getParameter("afi") + "',ape_pat='" + ape_pat.toUpperCase() + "',ape_mat='" + ape_mat.toUpperCase() + "',nom_pac='" + nombre.toUpperCase() + "',nom_com='" + completo + "',fec_nac='" + df.format(df2.parse(request.getParameter("fec_nac"))) + "',sexo='" + request.getParameter("sexo").toUpperCase() + "',expediente= '" + request.getParameter("no_exp") + "',f_status='" + request.getParameter("estatus") + "', "
                        + "ini_vig='" + df.format(df2.parse(request.getParameter("fecIni"))) + "', fin_vig='" + df.format(df2.parse(request.getParameter("fecFin"))) + "' WHERE id_pac='" + request.getParameter("no_pac") + "'");

                con.actualizar("UPDATE pacientes SET ini_vig='" + df.format(df2.parse(request.getParameter("fecIni"))) + "', fin_vig='" + df.format(df2.parse(request.getParameter("fecFin"))) + "' WHERE num_afi='" + request.getParameter("no_afi") + "'");

                json.put("mensaje", "Paciente actualizado correctamente con el folio " + request.getParameter("no_pac") + " ");
                json.put("ban", "1");

            } catch (ParseException e) {
                json.put("mensaje", "El paciente no se pudo actualizar");
                Logger.getLogger(ModiPacientes.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
            out.println();
            jsona.add(json);
            out.println(jsona);
        } catch (SQLException ex) {
            Logger.getLogger(ModiPacientes.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            out.close();
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(ModiPacientes.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
