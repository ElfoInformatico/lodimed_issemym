/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Clases.Hash;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Impl.UsuarioImpl;
import Modelos.Usuario;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;

/**
 * Validación de usuario para ingreso al sistema
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class Login extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        UsuarioImpl usuarioImpl = new UsuarioImpl();
        String url, mensaje;
        String nombreUsuario, password, hash;
        hash = request.getParameter("sesion");
        nombreUsuario = request.getParameter("user");
        password = request.getParameter("pass");

        ArrayList<String> parametros = new ArrayList<>();
        parametros.add(nombreUsuario);
        parametros.add(password);
        int intentos = 0;
        if (sesion.getAttribute("cont") == null) {
            intentos = 0;
        } else {
            //recuperas de la session el número de intentos, p. ej así
            intentos = ((Integer) request.getSession().getAttribute("cont"));

        }
        boolean sesionCorrecta = Hash.checkMD5(sesion.getId(), hash, parametros);
        url = "login.jsp";
        mensaje = "Usuario no válido";

        if (sesionCorrecta) {
            Usuario usuario = usuarioImpl.validar(nombreUsuario, password);
            if (usuario != null) {
                int tipoCaptura = usuarioImpl.tipSistema();
                sesion.setAttribute("id_usu", usuario.getId() + "");
                sesion.setAttribute("nombre", usuario.getNom());
                sesion.setAttribute("rol", usuario.getRol());
                sesion.setAttribute("cla_uni", usuario.getUni());
                sesion.setAttribute("des_uni", usuario.getDesUni());
                sesion.setAttribute("cedula", usuario.getCedula());
                sesion.setAttribute("tipo", usuario.getTipo());
                sesion.setAttribute("nivel", usuario.getNivel());
                sesion.setAttribute("ap", usuario.getaP());
                sesion.setAttribute("am", usuario.getaM());
                sesion.setAttribute("tipCap", tipoCaptura);
                if (usuario.getTipo().equals("FARMACIA") || usuario.getTipo().equals("SUPERVISOR")) {
                    sesion.setAttribute("farm", usuario.getFarm());
                }
                url = "main_menu.jsp";
                usuarioImpl.insertEntrada(nombreUsuario, 1);
                mensaje = null;
            } else {
                usuarioImpl.insertEntrada(nombreUsuario, 0);
                sesion.setAttribute("cont", ++intentos);
            }
        }
        sesion.setAttribute("mensaje", mensaje);
        response.sendRedirect(url);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
