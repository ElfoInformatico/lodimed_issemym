/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.movientos;

import Clases.ConectionDB;
import Impl.MovimientosImpl;
import Modelos.MovimientoProducto;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Movimiento al inventario productos
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class MovimentosProductos extends HttpServlet {

public static final String ENTRADA = "entrada";
public static final String AJUSTE_SALIDA = "ajuste";
public static final String TRASPASO_SALIDA = "traspaso";
public static final String BUSQUEDA_PRODUCTO = "buscar";
public static final String APARTADO_SALIDA = "apartar";
public static final String QUITAR_APARTADO_SALIDA = "quitar";

/**
 * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
 *
 * @param request servlet request
 * @param response servlet response
 * @throws ServletException if a servlet-specific error occurs
 * @throws IOException if an I/O error occurs
 */
protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    request.setCharacterEncoding("UTF-8");
    HttpSession sesion = request.getSession();

    String accion;
    int idUsuario;
    boolean isErrorUsuario = false;

    SimpleDateFormat formatoEntrada = new SimpleDateFormat("dd/mm/yyyyy");
    SimpleDateFormat formatoSalida = new SimpleDateFormat("yyyy-mm-dd");

    ConectionDB con = new ConectionDB();
    PrintWriter out = null;
    try {
        out = response.getWriter();
        accion = request.getParameter("accion");
        idUsuario = Integer.valueOf((String) sesion.getAttribute("id_usu"));
        MovimientosImpl movimientosImpl = new MovimientosImpl(idUsuario, con);
        JSONObject jo;
        int cantidad, detalleProducto;
        String observaciones;
        con.conectar();

        switch (accion) {

        case ENTRADA:
            String clave = request.getParameter("clave");
            String lote = request.getParameter("lote");
            String caducidad = request.getParameter("caducidad");
            caducidad = formatoSalida.format(formatoEntrada.parse(caducidad));
            String codigoBarra = request.getParameter("codigoBarra");
            cantidad = Integer.valueOf(request.getParameter("cantidad"));
            observaciones = request.getParameter("observaciones");

            con.getConn().setAutoCommit(false);
            cantidad = movimientosImpl.adicionarPorEntrada(clave, lote, caducidad, codigoBarra, cantidad, observaciones);
            con.getConn().commit();

            if (cantidad == MovimientosImpl.EXISTANCIA_INSUFIENTE_ERROR) {
                isErrorUsuario = true;
                throw new SQLException("Existencias insuficientes para realizar este movimiento.");
            }

            jo = new JSONObject();
            jo.put("nuevo", cantidad);

            out.println(jo);
            break;

        case AJUSTE_SALIDA:
            detalleProducto = Integer.valueOf(request.getParameter("detalle"));
            cantidad = Integer.valueOf(request.getParameter("cantidad"));
            observaciones = request.getParameter("observaciones");

            con.getConn().setAutoCommit(false);
            cantidad = movimientosImpl.disminuirPorAjuste(detalleProducto, cantidad, observaciones);

            if (cantidad == MovimientosImpl.EXISTANCIA_INSUFIENTE_ERROR) {
                isErrorUsuario = true;
                throw new SQLException("Existencias insuficientes para realizar este movimiento.");
            }

            con.getConn().commit();

            jo = new JSONObject();
            jo.put("nuevo", cantidad);

            out.println(jo);
            break;

        case APARTADO_SALIDA:
            detalleProducto = Integer.valueOf(request.getParameter("detalle"));
            cantidad = Integer.valueOf(request.getParameter("cantidad"));

            con.getConn().setAutoCommit(false);
            int idApartado = movimientosImpl.apartarInsumo(detalleProducto, cantidad);

            con.getConn().commit();

            jo = new JSONObject();
            jo.put("id", idApartado);

            out.println(jo);
            break;

        case QUITAR_APARTADO_SALIDA:
            idApartado = Integer.valueOf(request.getParameter("idApartado"));

            con.getConn().setAutoCommit(false);
            movimientosImpl.quitarApartado(idApartado);

            con.getConn().commit();
            jo = new JSONObject();

            out.println(jo);
            break;

        case TRASPASO_SALIDA:
            idApartado = Integer.valueOf(request.getParameter("idApartado"));
            observaciones = request.getParameter("observaciones");

            con.getConn().setAutoCommit(false);
            cantidad = movimientosImpl.generarMovimientoIDPRoducto(idApartado, observaciones);
            con.getConn().commit();

            jo = new JSONObject();
            jo.put("nuevo", cantidad);

            out.println(jo);
            break;

        case BUSQUEDA_PRODUCTO:
            String claveCodigoBarra = request.getParameter("busqueda");
            List<MovimientoProducto> resultados = movimientosImpl.listarProductoDisponible(claveCodigoBarra);

            if (resultados.isEmpty()) {
                isErrorUsuario = true;
                throw new SQLException("No se encontró ninguna coincidencia.");
            }

            JSONArray array = new JSONArray();
            for (MovimientoProducto aux : resultados) {
                array.add(aux.toJSON());
            }

            out.println(array);
            break;

        default:
            out.println("Función no implementada");
        }

    } catch (SQLException | ParseException ex) {
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        try {
            out = response.getWriter();
            String msg = "ERROR: No pudo procesarse su solicitud intentar de nuevo.";

            if (isErrorUsuario) {
                msg = ex.getMessage();
            }

            out.println(msg);

            con.getConn().rollback();
        } catch (SQLException ex1) {
            Logger.getLogger(MovimentosProductos.class.getSimpleName()).log(Level.SEVERE, String.format("msg: %s | error: %d", ex1.getMessage(), ex1.
                      getErrorCode()), ex1);
        }
        Logger.getLogger(MovimentosProductos.class.getSimpleName()).log(Level.SEVERE, String.format("msg: %s", ex.getMessage()), ex);
    } catch (Exception ex) {
        Logger.getLogger(MovimentosProductos.class.getSimpleName()).log(Level.SEVERE, null, ex);
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        out = response.getWriter();

        out.print("ERROR: Hemos tenido inconvenientes, por favor contactar a soporte técnico.");
        try {
            con.getConn().rollback();
        } catch (SQLException ex1) {
            Logger.getLogger(MovimentosProductos.class.getSimpleName()).log(Level.SEVERE, null, ex1);
        }
    } finally {
        if (out != null) {
            out.close();
        }

        if (con.getConn() != null) {
            try {
                con.getConn().close();
            } catch (SQLException ex) {
                Logger.getLogger(MovimentosProductos.class.getSimpleName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
/**
 * Handles the HTTP <code>GET</code> method.
 *
 * @param request servlet request
 * @param response servlet response
 * @throws ServletException if a servlet-specific error occurs
 * @throws IOException if an I/O error occurs
 */
@Override
protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
}

/**
 * Handles the HTTP <code>POST</code> method.
 *
 * @param request servlet request
 * @param response servlet response
 * @throws ServletException if a servlet-specific error occurs
 * @throws IOException if an I/O error occurs
 */
@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
}

/**
 * Returns a short description of the servlet.
 *
 * @return a String containing servlet description
 */
@Override
public String getServletInfo() {
    return "Short description";
}// </editor-fold>

}
