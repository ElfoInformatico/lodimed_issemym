/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Clases.ConectionDB;
import com.google.gson.Gson;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Eliminación del producto de la receta
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class EliminaClave extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        ConectionDB con = new ConectionDB();
        HttpSession sesion = request.getSession(true);
        Map TUsu = new LinkedHashMap();
        try {
            con.conectar();

            String det_pro = "", id_rec = "", folio;
            int n_cant, cant_sur = 0, cant_inv = 0;
            folio = request.getParameter("fol_det");
            PreparedStatement ps;
            ps = con.getConn().prepareStatement("SELECT dr.det_pro, dr.can_sol, dr.cant_sur, i.cant, dr.id_rec FROM detreceta dr, detalle_productos dp, inventario i WHERE dr.det_pro = dp.det_pro AND dp.det_pro = i.det_pro and dr.fol_det = ? ");
            ps.setString(1, folio);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                det_pro = rset.getString(1);
                cant_sur = rset.getInt(3);
                cant_inv = rset.getInt(4);
                id_rec = rset.getString(5);
            }

            n_cant = cant_inv + cant_sur;

            ps.clearParameters();
            ps = con.getConn().prepareStatement("UPDATE detreceta SET can_sol = '0', cant_sur = '0', baja='1' WHERE fol_det = ? ");
            ps.setString(1, folio);
            ps.executeUpdate();
            //con.insertar("UPDATE detreceta SET can_sol = '0', cant_sur = '0', baja='1' WHERE fol_det = '" + request.getParameter("fol_det") + "' ");

            ps.clearParameters();
            ps = con.getConn().prepareStatement("UPDATE inventario SET cant = ? WHERE det_pro = ? ");
            ps.setInt(1, n_cant);
            ps.setString(2, det_pro);
            ps.executeUpdate();
            //con.insertar("UPDATE inventario SET cant = '" + n_cant + "' WHERE det_pro = '" + det_pro + "' ");

            ps.clearParameters();
            ps = con.getConn().prepareStatement("INSERT INTO kardex VALUES ('0', ?, ?, ?, 'REINTEGRA AL INVENTARIO', '-', NOW(), 'SE ELIMINA INSUMO DE RECETA', ?, '0'); ");
            ps.setString(1, id_rec);
            ps.setString(2, det_pro);
            ps.setInt(3, cant_sur);
            ps.setString(4, (String) sesion.getAttribute("id_usu"));
            ps.executeUpdate();
            //con.insertar("INSERT INTO kardex VALUES ('0', '" + id_rec + "', '" + det_pro + "', '" + cant_sur + "', 'REINTEGRA AL INVENTARIO', '-', NOW(), 'SE ELIMINA INSUMO DE RECETA', '" + sesion.getAttribute("id_usu") + "', '0'); ");
            TUsu.put("msj", "Insumo eliminado con éxito.");
            String json1;
            json1 = new Gson().toJson(TUsu);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json1);
            con.cierraConexion();
        } catch (SQLException ex) {
            Logger.getLogger(EditaMedicamento.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(EliminaClave.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
