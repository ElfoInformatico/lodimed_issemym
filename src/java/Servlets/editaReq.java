/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Clases.ConectionDB;
import Clases.ConectionDBPruebas;
import Clases.enviaReq;
import Dao.reqDao;
import Impl.reqDaoImpl;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONObject;

/**
 * Modificación del requerimiento
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class editaReq extends HttpServlet {

    private static final String UPDATE_PRODUCTO = "UPDATE carga_requerimiento SET cant =?, observaciones=? WHERE id =?";
    private static final String SELECT_PRODUCTO = "SELECT c.clave, p.des_pro,c.cant,c.observaciones,c.id,c.cargado  FROM carga_requerimiento c, productos p WHERE c.clave=p.cla_pro AND c.id=?;";
    private static final String SAVE_DIR = "CVS_Abastos";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet editaReq</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet editaReq at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession sesion = request.getSession();
        ConectionDBPruebas con1 = new ConectionDBPruebas();
        ConectionDB con = new ConectionDB();
        JSONObject json = new JSONObject();
        PreparedStatement ps;
        ResultSet rs;
        try {
            String accion = request.getParameter("accion");
            if (accion == null) {
                accion = "";
            }
            switch (accion) {
                case "Validar":
                    try {

                        con.conectar();
                        String tipReq = request.getParameter("tipReq");
                        String ruta = (String) sesion.getAttribute("nomArchivo");
                        String rutaServlet = request.getServletContext().getRealPath("");
                        String rutaCarpeta = rutaServlet + File.separatorChar + SAVE_DIR;
                        String nombreArchivo = String.format("%s.csv", (String) sesion.getAttribute("cla_uni"));
                        String rutaArchvio = rutaCarpeta + File.separatorChar + nombreArchivo;
                        reqDao csv = new reqDaoImpl();

                        csv.generaCsv(rutaArchvio);
                        enviaReq en = new enviaReq();
                        en.EnviaReq(rutaArchvio, nombreArchivo, (String) sesion.getAttribute("cla_uni"));
                        con1.conectar();

                        String qry = "SELECT clave_unidad,fecha,clave,cant,observaciones FROM carga_requerimiento WHERE cargado=1;";
                        rs = con.consulta(qry);
                        while (rs.next()) {
                            con1.actualizar("INSERT INTO unireq  (clave_unidad,clave_producto, piezas_requeridas, fecha_carga, fecha_validacion,solicitado,observaciones,tipoReq) VALUES ('" + rs.getString("clave_unidad") + "','" + rs.getString("clave") + "','" + rs.getString("cant") + "','" + rs.getString("fecha") + "','" + rs.getString("fecha") + "','" + rs.getString("cant") + "','" + rs.getString("observaciones") + "','" + tipReq + "')");

                        }

                        out.print("Requerimiento enviado con éxito");
                        sesion.setAttribute("ver", "no");

                    } catch (SQLException ex) {
                        Logger.getLogger(EditaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                        out.print("Error al guardar el reporte de abasto--");
                    } finally {
                        try {
                            if (con.estaConectada()) {
                                con.cierraConexion();
                                con1.cierraConexion();
                            }
                        } catch (SQLException ex) {
                            Logger.getLogger(editaReq.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                        }
                    }
                    break;
                case "Ver":
                    sesion.setAttribute("id", request.getParameter("id"));
                    break;
                case "Editar":
                    String msnError = "Actualización existosa.";
                    try {
                        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
                        int id = Integer.parseInt(request.getParameter("id"));
                        String clave = request.getParameter("clave");

                        String[] camposCambiados = request.getParameterValues("cambios[]");
                        String[] valoresCambiados = request.getParameterValues("valores[]");
                        String SELECT_ANTERIOR_DATA = "SELECT %s, observaciones FROM carga_requerimiento WHERE id = ?";
                        String campos = "";
                        for (int i = 0; i < camposCambiados.length; i++) {
                            String campoCambiado = camposCambiados[i];
                            if (i == (camposCambiados.length - 1)) {
                                campos += campoCambiado;
                            } else {
                                campos += String.format("%s,", campoCambiado);
                            }
                        }

                        con.conectar();

                        SELECT_ANTERIOR_DATA = String.format(SELECT_ANTERIOR_DATA, campos);
                        ps = con.getConn().prepareStatement(SELECT_ANTERIOR_DATA);
                        ps.setInt(1, id);
                        rs = ps.executeQuery();

                        String auxObservacion = "";
                        String anteriorObservacion = "";
                        if (rs.next()) {
                            int i;
                            for (i = 0; i < camposCambiados.length; i++) {
                                String campoCambiado = camposCambiados[i];
                                String valorCambiado = valoresCambiados[i];
                                auxObservacion += String.format("%s: anterior(%s) - actual(%s) | ", campoCambiado, rs.getString(campoCambiado), valorCambiado);
                            }
                            anteriorObservacion = rs.getString(i + 1);
                        } else {
                            msnError = "No se genero ningún cambio en la base de datos";
                            throw new SQLException(msnError, ps.toString());
                        }
                        String observacion = String.format("%s ||| CAMBIOS: %s JUSTIFICACIÓN: %s.",
                                anteriorObservacion, auxObservacion, request.getParameter("observacion"));

                        ps = con.getConn().prepareStatement(UPDATE_PRODUCTO);
                        ps.setInt(1, Integer.parseInt(request.getParameter("cant")));
                        ps.setString(2, observacion);
                        ps.setInt(3, id);

                        if (ps.executeUpdate() == 0) {
                            msnError = "No se genero ningún cambio en la base de datos";
                            throw new SQLException(msnError, ps.toString());
                        }

                        ps = con.getConn().prepareStatement(SELECT_PRODUCTO);
                        ps.setInt(1, id);
                        rs = ps.executeQuery();
                        if (rs.next()) {
                            json.put("clave", rs.getString(1));
                            json.put("descripcion", rs.getString(2));
                            json.put("cantidad", rs.getString(3));
                            json.put("observacion", rs.getString(4));

                        } else {
                            msnError = "No se genero ningún cambio en la base de datos";
                            throw new SQLException(msnError, ps.toString());
                        }

                    } catch (SQLException e) {
                        String mensaje = String.format("m: %s, sql: %s", e.getMessage(), e.getSQLState());
                        Logger.getLogger(EditaAbasto.class.getName()).log(Level.SEVERE, mensaje, e);
                        msnError = (!msnError.equals("Actualización existosa.")) ? msnError : "Error al intentar conectar con la base de datos.";
                    } catch (NumberFormatException e) {
                        Logger.getLogger(EditaAbasto.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                        msnError = "Error al intentar actulizar el producto.";
                    } finally {
                        try {
                            if (con.estaConectada()) {
                                con.cierraConexion();
                            }
                        } catch (SQLException ex) {
                            Logger.getLogger(editaReq.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                        }
                    }
                    json.put("msg", msnError);
                    out.println(json);
                    break;

            }
        } finally {
            out.close();
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
