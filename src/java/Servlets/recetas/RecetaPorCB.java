package Servlets.recetas;

import Clases.ConectionDB;
import Impl.RecetaImpl;
import Modelos.DetalleCapturado;
import Modelos.LoteDisponible;
import Modelos.Medico;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelos.Receta;
import Modelos.Usuario;
import com.gnkl.services.hl7.MessageHL7Service;
import com.gnkl.services.hl7.SocketUtils;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Se encarga de la captura de todos los tipos de captura de receta: por
 * famacia, medico o por CB.
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class RecetaPorCB extends HttpServlet {

    public static final String OBTENER_FOLIO_MEDICO = "folio_medico";
    public static final String VALIDAR_ENCABEZADO_ACCION = "validar_encabezado";
    public static final String CREAR_ENCABEZADO_ACCION = "encabezado";
    public static final String CANCELAR_RECETA_ACCION = "cancelar";

    public static final String CAPTURA_DETALLE_ACCION = "captura";
    public static final String CANCELAR_DETALLE_ACCION = "cancelar_detalle";
    public static final String RECARGAR_DETALLES_ACCION = "detalles";
    public static final String VALIDAR_RECETA_ACCION = "validar";

    public static final String OBTENER_LOTES_ACCION = "lotes";
    public static final String CAMBIAR_LOTE_ACCION = "cambiar";
    public static final String SURTIR_LOTE_ACCION = "surtir";

    public static final String FINALIZAR_RECETA_ACCION = "finalizar";
    public static final String ACTUALIZAR_CADUCAS_ACCTION = "caducas";
    public static final String NOTIFICAR_PACIENTE = "hl7notif";
    public static final String NOTIFICAR_SURTIDO_HL7 = "mensajehl7";

    public static Date ULTIMA_VERIFICACIÓN;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String msg = "";
        String accion = request.getParameter("accion");
        int tipo = Integer.valueOf(request.getParameter("tipo"));
        HttpSession sesion = request.getSession();
        int idUsuario = Integer.valueOf((String) sesion.getAttribute("id_usu"));
        ConectionDB con = new ConectionDB();
        RecetaImpl rImp = new RecetaImpl(con, tipo, idUsuario);
        PrintWriter out = null;
        int receta, producto, surtido;
        String folio;

        int tipoCaptura = (Integer) sesion.getAttribute("tipCap");
        int rol = (Integer) sesion.getAttribute("rol");
        if (rol == Usuario.MEDICO_ROL) {
            tipoCaptura = RecetaImpl.MEDICO_CAPTURA;
        }

        try {
            out = response.getWriter();
            JSONObject jo;
            con.conectar();
            switch (accion) {

                case OBTENER_FOLIO_MEDICO:

                    Medico medico = rImp.obtenerMedicoAsociado();
                    folio = rImp.obtenerFolioMedico(medico.getId());

                    jo = new JSONObject();
                    jo.put("id", folio);
                    jo.put("nombre_completo", medico.getNombreCompleto());
                    jo.put("tipo_consulta", medico.getTipoConsulta());

                    out.println(jo);

                    break;

                case VALIDAR_ENCABEZADO_ACCION:

                    folio = request.getParameter("folio");

                    if (rImp.validarFolio(folio)) {
                        msg = String.format("La receta que intenta capturar, YA EXISTE. Folio: %s", folio);
                        throw new SQLException(msg);
                    }

                    jo = new JSONObject();
                    jo.put("id", folio);

                    out.println(jo);
                    break;

                case CREAR_ENCABEZADO_ACCION:
                    con.getConn().setAutoCommit(false);
                    int id = crearEncabezado(request, tipo, idUsuario, rImp);
                    con.getConn().commit();

                    if (id == RecetaImpl.FOLIO_EXISTENTE) {
                        folio = request.getParameter("folio");
                        msg = String.format("La receta que intenta capturar, YA EXISTE. Folio: %s", folio);
                        throw new SQLException(msg);
                    }

                    jo = new JSONObject();
                    jo.put("id", id);

                    out.println(jo);
                    break;

                case CANCELAR_RECETA_ACCION:
                    folio = request.getParameter("folio");
                    receta = Integer.valueOf(request.getParameter("id"));
                    con.getConn().setAutoCommit(false);
                    rImp.cancelarReceta(folio, receta, tipo, idUsuario);
                    con.getConn().commit();

                    out.printf("Cancelación correcta del folio: %s ", folio);
                    break;

                case CAPTURA_DETALLE_ACCION:
                    con.getConn().setAutoCommit(false);
                    String resultadoCaptura = capturarProducto(request, rImp, tipoCaptura);
                    if (!resultadoCaptura.isEmpty()) {
                        msg = resultadoCaptura;
                        throw new SQLException(msg);
                    }
                    con.getConn().commit();
                    out.println(resultadoCaptura);
                    break;

                case RECARGAR_DETALLES_ACCION:
                    receta = Integer.valueOf(request.getParameter("id"));
                    JSONArray detalles = new JSONArray();
                    List<DetalleCapturado> resultados = rImp.reconstruirDetalles(receta);
                    for (DetalleCapturado detalle : resultados) {
                        detalles.add(detalle.toJSON());
                    }

                    out.print(detalles);
                    break;

                case CANCELAR_DETALLE_ACCION:
                    receta = Integer.valueOf(request.getParameter("id"));
                    String clave = request.getParameter("clave");
                    con.getConn().setAutoCommit(false);
                    rImp.cancelarDetalle(receta, clave, tipo, idUsuario);
                    con.getConn().commit();

                    out.print(String.format("Cancelación exitosa de la clave: %s", clave));
                    break;

                case VALIDAR_RECETA_ACCION:
                    receta = Integer.valueOf(request.getParameter("id"));
                    con.getConn().setAutoCommit(false);
                    rImp.validarReceta(receta, tipoCaptura);

                    con.getConn().commit();

                    out.print(String.format("Validación exitosa. Se imprimirá un ticket para surtir la receta."));
                    break;

                case OBTENER_LOTES_ACCION:
                    receta = Integer.valueOf(request.getParameter("id"));
                    String cb = request.getParameter("cb");
                    con.getConn().setAutoCommit(false);
                    List<LoteDisponible> resultado = rImp.obtenerLotesDisponibles(cb, receta);
                    con.getConn().commit();

                    if (resultado.isEmpty()) {
                        msg = String.format("El código de barras escaneado no pertenece a ninguna lote en la receta. CB: %s | Receta: %d", cb, receta);
                        throw new SQLException(msg);
                    }
                    JSONArray lotes = new JSONArray();
                    for (LoteDisponible lote : resultado) {
                        lotes.add(lote.toJSON());
                    }
                    out.print(lotes);
                    break;

                case CAMBIAR_LOTE_ACCION:
                    receta = Integer.valueOf(request.getParameter("id"));
                    producto = Integer.valueOf(request.getParameter("producto"));
                    surtido = Integer.valueOf(request.getParameter("surtido"));
                    con.getConn().setAutoCommit(false);
                    boolean cambio = rImp.cambiarLote(receta, producto, surtido);
                    if (!cambio) {
                        msg = String.format("No puede surtir más de lo solicitado para la clave.");
                        throw new SQLException(msg);
                    }
                    con.getConn().commit();
                    break;

                case SURTIR_LOTE_ACCION:

                    receta = Integer.valueOf(request.getParameter("id"));
                    producto = Integer.valueOf(request.getParameter("producto"));
                    surtido = Integer.valueOf(request.getParameter("surtido"));
                    con.getConn().setAutoCommit(false);

                    int totalSurtido = rImp.surtirReceta(receta, producto, surtido, tipoCaptura);
                    con.getConn().commit();

                    if (totalSurtido == RecetaImpl.SURTIDO_MAYOR) {
                        msg = String.format("No puede surtir más de lo solicitado. Verifique por favor.");

                        throw new SQLException(msg);
                    }

                    jo = new JSONObject();
                    jo.put("producto", producto);
                    jo.put("surtido", totalSurtido);

                    out.println(jo);

                    break;

                case FINALIZAR_RECETA_ACCION:

                    receta = Integer.valueOf(request.getParameter("id"));
                    con.getConn().setAutoCommit(false);
                    rImp.finalizarReceta(receta);
                    con.getConn().commit();

                    break;

                case NOTIFICAR_SURTIDO_HL7:

                    receta = Integer.valueOf(request.getParameter("id"));
                    Receta recetaSurtida = rImp.reconstruirReceta(receta);
                    sendHL7MessageBySurtimiento(recetaSurtida, rImp);

                    break;

                case NOTIFICAR_PACIENTE:

                    receta = Integer.valueOf(request.getParameter("id"));
                    con.getConn().setAutoCommit(false);

                    Receta aux = rImp.reconstruirReceta(receta);
                    rImp.actualizarNotificacionHL7(receta);
                    MessageHL7Service serviceHL7 = new MessageHL7Service(con.getConn());
                    SocketUtils utilsHl7 = new SocketUtils(con.getConn());
                    String hl7message = serviceHL7.createHL7MessageRDEO11(aux.getObservacionHL7());
                    utilsHl7.sendDatatoSocket(hl7message, receta);
                    serviceHL7.createHl7RecetaLog("RDE_O11", hl7message, 1, "NOTIFICACION USUARIO");

                    con.getConn().commit();
                    out.println("<script>window.location='farmacia/modSurteFarmaciaP.jsp'</script>");
                    out.println("<script>alert('Se envia notificacion de usuario presente.')</script>");
                    break;

                case ACTUALIZAR_CADUCAS_ACCTION:

                    if (!esNecesarioActualizar()) {
                        break;
                    }

                    con.getConn().setAutoCommit(false);
                    rImp.actualizarCaducas();
                    con.getConn().commit();
                    break;

                default:
                    throw new AssertionError();
            }

        } catch (SQLException ex) {
            Logger.getLogger(RecetaPorCB.class.getSimpleName()).log(Level.SEVERE,
                    String.format("msg: %s, error: %d", ex.getMessage(), ex.getErrorCode()), ex);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            out = response.getWriter();

            if (msg.isEmpty()) {
                msg = "ERROR: No pudo procesarse su solicitud intentar de nuevo. Si el problema persiste, contactar a soporte técnico.";
            }

            out.println(msg);
            try {
                if (!con.getConn().getAutoCommit()) {
                    con.getConn().rollback();
                }
            } catch (SQLException ex1) {
                Logger.getLogger(RecetaPorCB.class.getSimpleName()).log(Level.SEVERE, null, ex1);
            }
        } catch (ParseException ex) {
            Logger.getLogger(RecetaPorCB.class.getSimpleName()).log(Level.SEVERE, null, ex);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            out = response.getWriter();

            if (msg.isEmpty()) {
                msg = "ERROR: El formato de la fecha no es el adecuado.";
            }

            out.println(msg);
            try {
                if (!con.getConn().getAutoCommit()) {
                    con.getConn().rollback();
                }
            } catch (SQLException ex1) {
                Logger.getLogger(RecetaPorCB.class.getSimpleName()).log(Level.SEVERE, null, ex1);
            }
        } catch (Exception ex) {
            Logger.getLogger(PacienteNuevo.class.getSimpleName()).log(Level.SEVERE, null, ex);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            out = response.getWriter();

            if (msg.isEmpty()) {
                msg = "ERROR: Hemos tenido inconvenientes, por favor contactar a soporte técnico.";
            }

            out.print(msg);
            try {
                if (!con.getConn().getAutoCommit()) {
                    con.getConn().rollback();
                }
            } catch (SQLException ex1) {
                Logger.getLogger(RecetaPorCB.class.getSimpleName()).log(Level.SEVERE, null, ex1);
            }
        } finally {
            if (out != null) {
                out.close();
            }

            if (con.getConn() != null) {
                try {
                    con.getConn().close();
                } catch (SQLException ex) {
                    Logger.getLogger(RecetaPorCB.class.getSimpleName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    private String capturarProducto(HttpServletRequest request, RecetaImpl implementacion, int tipoSistema) throws SQLException {

        int receta = Integer.valueOf(request.getParameter("id"));
        String clave = request.getParameter("clave");
        int solicitado = Integer.valueOf(request.getParameter("solicitado"));

        String indicacion = request.getParameter("indicacion");
        String cause1 = request.getParameter("cause");
        String cause2 = request.getParameter("cause2");

        String resultadoCaptura = implementacion.capturaMedicamento(receta, clave, solicitado, indicacion, cause1, cause2, tipoSistema);

        return resultadoCaptura;
    }

    private int crearEncabezado(HttpServletRequest request, int tipo,
            int idUsuario, RecetaImpl implementacion) throws SQLException, ParseException {
        int idNuevo = 0, idMedico;
        String folio, fecha;

        folio = request.getParameter("folio");
        fecha = request.getParameter("fecha");
        fecha = convertirFechaMySQL(fecha);
        idMedico = Integer.valueOf(request.getParameter("medico"));

        if (Receta.COLECTIVA_TIPO == tipo) {
            int idServicio = Integer.valueOf(request.getParameter("servicio"));
            idNuevo = implementacion.crearEncabezado(tipo, idMedico, idUsuario,
                    idServicio, folio, fecha);
        } else {
            int idPaciente = Integer.valueOf(request.getParameter("paciente"));
            String tipoConsulta, carnet;
            tipoConsulta = request.getParameter("consulta");
            carnet = request.getParameter("expediente");
            idNuevo = implementacion.crearEncabezado(tipo, idPaciente, idMedico,
                    idUsuario, folio, tipoConsulta, fecha, carnet);
        }

        return idNuevo;
    }

    private String convertirFechaMySQL(String fecha) throws ParseException {

        SimpleDateFormat original = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat requerido = new SimpleDateFormat("yyyy-MM-dd");
        fecha = requerido.format(original.parse(fecha));

        return fecha;
    }

    public void sendHL7MessageBySurtimiento(Receta receta, RecetaImpl implementacion) throws SQLException, IOException, InterruptedException {

        MessageHL7Service serviceHL7 = new MessageHL7Service(implementacion.getCon().getConn());
        SocketUtils utilsHl7 = new SocketUtils(implementacion.getCon().getConn());

        List<DetalleCapturado> listDetalleReceta = implementacion.reconstruirDetalles(receta.getId());
        String hl7message;

        for (DetalleCapturado detalle : listDetalleReceta) {
            hl7message = serviceHL7.createHL7MessageRDSO13(receta.getIdMedico(),
                    receta.getObservacionHL7(), detalle.detalleReceta, implementacion.getCon());
            utilsHl7.sendDatatoSocket(hl7message, receta.getId());

            int canSol = detalle.detalleReceta.getCantidadSolicitada();
            int canSur = detalle.detalleReceta.getCantidadSurtida();
            int resultadoSurtimiento = canSol - canSur;

            if (resultadoSurtimiento == 0) {//RECETA CON MEDICAMENTO SURTIDO COMPLETAMENTE
                serviceHL7.createHl7RecetaLog("RDS_O13", hl7message, 2, "SURTIMIENTO COMPLETO");
            } else if (resultadoSurtimiento == canSol) {// RECETA CON MEDICMENTO NO SURTIDO
                serviceHL7.createHl7RecetaLog("RDS_O13", hl7message, 2, "SURTIMIENTO NO SURTIDO");
            } else if (resultadoSurtimiento < canSol) {//RECETA CON MEDICAMENTO SURTIDO DE MANERA PARCIAL
                serviceHL7.createHl7RecetaLog("RDS_O13", hl7message, 2, "SURTIMIENTO PARCIAL");
            }

        }

    }

    /**
     *
     * @return
     */
    public boolean esNecesarioActualizar() {

        if (ULTIMA_VERIFICACIÓN == null) {
            ULTIMA_VERIFICACIÓN = new Date();
            return true;
        }

        Date hoy = new Date();
        long diferencia = hoy.getTime() - ULTIMA_VERIFICACIÓN.getTime();
        long dias = TimeUnit.MILLISECONDS.toDays(diferencia);

        boolean necesario = false;
        necesario = dias > 1;

        if (necesario) {
            ULTIMA_VERIFICACIÓN = hoy;
        }

        return necesario;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
