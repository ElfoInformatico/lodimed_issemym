/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets.recetas;

import Clases.ConectionDB;
import Impl.PacienteDaoImpl;
import Modelos.Paciente;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Se encarga de la captura de todos los tipos de captura de receta: por
 * famacia, medico o por CB.
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
@WebServlet(name = "PacienteCB", urlPatterns = {"/paciente/nombre"})
public class PacienteNuevo extends HttpServlet {

    public static final String BUSCAR_POR_NOMBRE = "porNombre";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        String accion = request.getParameter("accion");
        PrintWriter out = null;
        ConectionDB con;
        try {

            switch (accion) {

                case BUSCAR_POR_NOMBRE:
                    String nombre = request.getParameter("nombre");
                    con = new ConectionDB();
                    con.conectar();
                    PacienteDaoImpl impl = new PacienteDaoImpl(con.getConn());

                    Paciente paciente = impl.reconstruirPaciente(nombre);

                    String resultado = String.format("Paciente '%s' no encontrado.", nombre);
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    if (paciente != null) {
                        resultado = paciente.toJSON().toJSONString();
                        response.setStatus(HttpServletResponse.SC_OK);
                    }
                    out = response.getWriter();

                    out.print(resultado);
                    break;

                default:
                    throw new AssertionError();
            }

        } catch (SQLException ex) {
            Logger.getLogger(PacienteNuevo.class.getSimpleName()).log(Level.SEVERE, null, ex);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            out = response.getWriter();

            String msg = "ERROR: No se encontro el paciente indicado. Si el problema persiste, contactar a soporte técnico.";
            out.print(msg);

        } catch (Exception ex) {
            Logger.getLogger(PacienteNuevo.class.getSimpleName()).log(Level.SEVERE, null, ex);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            out = response.getWriter();

            String msg = "ERROR: Hemos tenido inconvenientes, por favor contactar a soporte técnico.";
            out.print(msg);

        } finally {
            if (out != null) {
                out.close();
            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
