package Servlets;

import Clases.ConectionDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.simple.JSONObject;

/**
 * Usuarios farmacias
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class Farmacias extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ConectionDB con = new ConectionDB();

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession sesion = request.getSession(true);
        ResultSet rset;

        String accion = request.getParameter("accion");
        if (accion == null || accion.equals("")) {
            accion = "";
        }

        String SELECT_VERIFICAR_USUARIO = "SELECT u.nombre FROM usuarios AS u WHERE u.id_usu = ? AND u.pass = MD5(?)     ORDER BY u.id_usu ASC";
        switch (accion) {

            case "verificar":
                try {
                    JSONObject json = new JSONObject();
                    String password;
                    int id;
                    con.conectar();
                    PreparedStatement ps = con.getConn().prepareStatement(SELECT_VERIFICAR_USUARIO);

                    id = Integer.parseInt(sesion.getAttribute("id_usu").toString());
                    password = request.getParameter("pass");
                    ps.setInt(1, id);
                    ps.setString(2, password);

                    rset = ps.executeQuery();
                    if (rset.isBeforeFirst()) {
                        json.put("permitido", true);
                    } else {
                        json.put("permitido", false);
                    }
                    out.println(json);
                } catch (SQLException | NumberFormatException ex) {
                    Logger.getLogger(Farmacias.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), SELECT_VERIFICAR_USUARIO), ex);
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    response.getWriter().write("No puedo verificarse su información.");
                    response.flushBuffer();
                } finally {
                    try {
                        if (con.estaConectada()) {
                            con.cierraConexion();
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(Farmacias.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                    }
                }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
