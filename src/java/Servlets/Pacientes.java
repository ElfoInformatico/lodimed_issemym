/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Clases.ConectionDB;
import Dao.PacienteDao;
import Impl.PacienteDaoImpl;
import Modelos.Paciente;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Registro y consulta de pacientes hl7
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class Pacientes extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ConectionDB con = new ConectionDB();
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        StringBuilder mensaje = new StringBuilder("<script>alert(\"");
        Connection conn = null;
        try {

            con.conectar();

            int numAfiliados = Integer.parseInt(request.getParameter("numeroAfiliados"));
            for (int i = 1; i <= numAfiliados; i++) {

                String TipCob = request.getParameter("tip_cob1").toUpperCase();

                String fecIni;
                String fecFin;
                String ape_pat = request.getParameter(i + "ape_pat").toUpperCase();
                String ape_mat = request.getParameter(i + "ape_mat").toUpperCase();
                String nombre = request.getParameter(i + "nombre").toUpperCase();
                String fecNac = request.getParameter(i + "fec_nac");

                if (TipCob.equals("SP")) {
                    fecIni = request.getParameter("fecIni");
                    fecFin = request.getParameter("fecFin");
                    if (fecIni.contains("/")) {
                        fecIni = fecIni.replace("/", "-");
                    }
                    if (fecFin.contains("/")) {
                        fecFin = fecFin.replace("/", "-");
                    }
                } else {
                    fecIni = "01-01-2015";
                    fecFin = "31-12-2036";
                }
                String completo = ape_pat.toUpperCase() + " " + ape_mat.toUpperCase() + " " + nombre.toUpperCase();
                conn = con.getConn();
                PacienteDao dao = new PacienteDaoImpl(conn);

                mensaje.append("\nEl paciente '");
                mensaje.append(completo);
                mensaje.append("'");

                if (dao.reconstruirPaciente(completo) != null) {
                    mensaje.append(" ya existía previamente.");
                    continue;
                }

                if (fecNac.isEmpty()) {
                    mensaje.append(" ingrese la fecha de nacimiento por favor.");
                    continue;
                }

                if (fecIni.isEmpty()) {
                    mensaje.append(" ingrese la fecha de inicio de vigencia.");
                    continue;
                }

                if (fecFin.isEmpty()) {
                    mensaje.append(" ingrese la fecha fin de vigencia.");
                    continue;
                }

                String sexo = request.getParameter(i + "sexo").toUpperCase();
                String numeroAfilacion = request.getParameter("no_afi");
                String expediente = request.getParameter(i + "no_exp");

                conn.setAutoCommit(false);

                Paciente paciente = new Paciente(0, ape_pat, ape_mat, nombre, completo, fecNac, sexo,
                        numeroAfilacion, TipCob, fecIni, fecFin, expediente, Paciente.ACTIVO_STATUS, Paciente.SIN_ID_HL7);
                long id = dao.adicionarPaciente(paciente);

                conn.commit();
                conn.setAutoCommit(true);

                mensaje.append(" creado correctamente con el id '");
                mensaje.append(id);
                mensaje.append("'.");
            }

            mensaje.append("\")</script>");

            out.println(mensaje.toString());

            out.println("<script>window.close();</script>");

        } catch (SQLException ex) {
            Logger.getLogger(Pacientes.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            if (conn != null) {
                try {
                    if (!conn.getAutoCommit()) {
                        conn.rollback();
                    }
                } catch (SQLException ex1) {
                    Logger.getLogger(Pacientes.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        } finally {
            out.close();
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(Pacientes.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
