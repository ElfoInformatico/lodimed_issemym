/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Clases.ConectionDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Catálogo de médicos
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
@WebServlet(name = "CatalogoMedicos", urlPatterns = {"/CatalogoMedico"})
public class CatalogoMedicos extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        ConectionDB con = new ConectionDB();
        JSONObject json = new JSONObject();
        JSONArray jsona = new JSONArray();
        PreparedStatement ps;
        ResultSet rs;
        String accion;
        String query;

        accion = request.getParameter("accion");

        if (accion == null) {
            accion = "";
        }

        if (accion.equals("medicos")) {
            try {
                con.conectar();
                query = "SELECT medicos.cedula, medicos.nom_com, unidades.des_uni, medicos.iniRec, medicos.finRec, medicos.folAct FROM medicos INNER JOIN unidades ON unidades.cla_uni = medicos.clauni where cedula !=1";

                ps = con.getConn().prepareStatement(query);
                rs = ps.executeQuery();
                while (rs.next()) {
                    json.put("cedula", rs.getString(1));
                    json.put("nombre", rs.getString(2));
                    json.put("unidad", rs.getString(3));
                    json.put("folioInicio", rs.getString(4));
                    json.put("folioFin", rs.getString(5));
                    json.put("folioActual", rs.getString(6));
                    jsona.add(json);
                    json = new JSONObject();
                }
                out.println(jsona);
                con.cierraConexion();

            } catch (SQLException ex) {
                Logger.getLogger(CatalogoMedicos.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();
                try {
                    con.cierraConexion();
                } catch (SQLException ex) {
                    Logger.getLogger(CatalogoMedicos.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
