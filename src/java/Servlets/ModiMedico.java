/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Clases.ConectionDB;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Modificación datos médicos
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
@MultipartConfig
public class ModiMedico extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private static final String SAVE_DIR = "reportes/imagen/firmas";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ConectionDB con = new ConectionDB();
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            JSONObject json = new JSONObject();
            JSONArray jsona = new JSONArray();
            int baja;
            int ban = 0;
            con.conectar();
            try {

                String ape_pat = request.getParameter("ape_pat");
                String ape_mat = request.getParameter("ape_mat");

                String nombre = request.getParameter("nombre");
                String id = request.getParameter("id");
                String tipoConsu = request.getParameter("txtTipoConsu");
                String imagen = request.getParameter("chkImg");
                String cedula = request.getParameter("clave");
                String completo = ape_pat.toUpperCase() + " " + ape_mat.toUpperCase() + " " + nombre.toUpperCase();

                try {
                    int iniRec = 0, finRec = 0;
                    iniRec = Integer.parseInt(request.getParameter("iniRec"));
                    finRec = Integer.parseInt(request.getParameter("finRec"));
                    ResultSet rs = con.consulta("SELECT iniRec, finRec FROM medicos where (iniRec BETWEEN " + iniRec + " and " + finRec + " OR finRec  BETWEEN " + iniRec + " and " + finRec + " ) AND nom_med<>'RECETA'  AND cedula!=" + cedula + ";");
                    while (rs.next()) {
                        ban = 1;
                    }
                } catch (NumberFormatException e) {
                    Logger.getLogger(Medicos.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                    json.put("msg", "Error->" + e);
                    json.put("pasa", "1");
                }

                if (ban == 1) {
                    out.println("<script>alert('El rango de folios esta en conflicto con otro médico verificar.')</script>");
                    out.println("<script>window.location='admin/medicos/editar_medico.jsp?id=" + id + "'</script>");

                } else {
                    con.actualizar("update medicos set tipoConsulta='" + tipoConsu + "', ape_pat='" + ape_pat.toUpperCase() + "',ape_mat='" + ape_mat.toUpperCase() + "',nom_med='" + nombre.toUpperCase() + "',nom_com='" + (ape_pat.toUpperCase() + " " + ape_mat.toUpperCase() + " " + nombre.toUpperCase()) + "',rfc='" + request.getParameter("rfc") + "',cedulapro='" + request.getParameter("cedula") + "',f_status='" + request.getParameter("estatus") + "', iniRec='" + request.getParameter("iniRec") + "',finRec='" + request.getParameter("finRec") + "',folAct='" + request.getParameter("folAct") + "' WHERE cedula='" + request.getParameter("clave") + "'");
                    if (request.getParameter("estatus").equals("A")) {
                        baja = 1;
                    } else {
                        baja = 0;
                    }
                    String pass = request.getParameter("password");
                    String reemplazo = "";
                    if (!pass.isEmpty()) {
                        reemplazo = String.format(" pass=MD5('%s'), ", pass);
                    }
                    con.actualizar("UPDATE usuarios SET " + reemplazo + "baja='" + baja + "', ape_pat='" + ape_pat.toUpperCase() + "',ape_mat='" + ape_mat.toUpperCase() + "',nombre='" + nombre.toUpperCase() + "', user = '" + request.getParameter("usuario") + "'  WHERE cedula='" + request.getParameter("clave") + "'");
                    if (imagen != null) {

                        Part firma = request.getPart("txtFirma");
                        String rutaServlet = request.getServletContext().getRealPath("");
                        String rutaCarpeta = rutaServlet + File.separator + SAVE_DIR;
                        String nombreArchivo = String.format("%s.jpg", request.getParameter("cedula"));
                        String rutaArchvio = rutaCarpeta + File.separator + nombreArchivo;
                        firma.write(rutaArchvio);
                        out.println("<script>alert('Médico actualizado correctamente con la Clave " + request.getParameter("clave") + "')</script>");
                        out.println("<script>window.location='admin/medicos/editar_medico.jsp?id=" + id + "'</script>");
                    }
                    out.println("<script>alert('Médico actualizado correctamente con la Clave " + request.getParameter("clave") + "')</script>");
                    out.println("<script>window.location='admin/medicos/editar_medico.jsp?id=" + id + "'</script>");
                }
            } catch (UnsupportedEncodingException e) {
                json.put("mensaje", "El Médico no se pudo actualizar");
                Logger.getLogger(ModiMedico.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
            out.println();

            jsona.add(json);
            out.println(jsona);
        } catch (SQLException ex) {
            Logger.getLogger(ModiMedico.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            out.close();
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(ModiMedico.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
