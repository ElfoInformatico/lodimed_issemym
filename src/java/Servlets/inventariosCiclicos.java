/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Dao.ciclicosDao;
import Impl.ciclicosDaoImpl;
import Modelos.ciclicos;
import com.google.gson.Gson;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Reporte inventario ciclico
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class inventariosCiclicos extends HttpServlet {

    private static final String SAVE_DIR = "CiclicosExcel";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet inventariosCiclicos</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet inventariosCiclicos at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        int accion = Integer.parseInt(request.getParameter("ban"));
        int folio = Integer.parseInt(request.getParameter("folio"));
        switch (accion) {
            case 1:
                ciclicosDao ciclico = new ciclicosDaoImpl();
                List<ciclicos> listaCiclicos;
                listaCiclicos = ciclico.reporteExcel(folio);

                PrintWriter pw = null;
                try {
                    pw = response.getWriter();
                    response.setContentType("text/html;charset=UTF-8");
                    HttpSession sesion = request.getSession();

                    String path = request.getServletContext().getRealPath("");
                    String rutaCarpeta = path + File.separatorChar + SAVE_DIR;

                    //Verifica existencia carpeta
                    File carpeta = new File(rutaCarpeta);
                    if (!carpeta.exists()) {
                        carpeta.mkdir();
                    }

                    String filename = rutaCarpeta + File.separatorChar + "Ciclico_" + folio + ".xlsx";
                    String sheetName = "Ciclico";
                    XSSFWorkbook wb = new XSSFWorkbook();

                    XSSFSheet sheet = wb.createSheet(sheetName);
                    int width = 20;
                    sheet.setAutobreaks(true);
                    sheet.setDefaultColumnWidth(width);

                    int index = 0;

                    XSSFRow rowHeadInv = sheet.createRow(index);
                    rowHeadInv.createCell((int) 0).setCellValue("Unidad");
                    rowHeadInv.createCell((int) 1).setCellValue("Fecha");
                    rowHeadInv.createCell((int) 2).setCellValue("Hora");
                    rowHeadInv.createCell((int) 3).setCellValue("Folio");
                    rowHeadInv.createCell((int) 4).setCellValue("Clave");
                    rowHeadInv.createCell((int) 5).setCellValue("Descripción");
                    rowHeadInv.createCell((int) 6).setCellValue("Lote");
                    rowHeadInv.createCell((int) 7).setCellValue("Caducidad");
                    rowHeadInv.createCell((int) 8).setCellValue("Cantidad Física");
                    rowHeadInv.createCell((int) 9).setCellValue("Inventario");
                    rowHeadInv.createCell((int) 10).setCellValue("Diferencia");
                    rowHeadInv.createCell((int) 11).setCellValue("Tipo");

                    for (ciclicos c : listaCiclicos) {
                        index++;
                        XSSFRow row = sheet.createRow(index);
                        row.createCell((int) 0).setCellValue(c.getUnidad());
                        row.createCell((int) 1).setCellValue(c.getFecha());
                        row.createCell((int) 2).setCellValue(c.getHora());
                        row.createCell((int) 3).setCellValue(c.getFolio());
                        row.createCell((int) 4).setCellValue(c.getClave());
                        row.createCell((int) 5).setCellValue(c.getDescripcion());
                        row.createCell((int) 6).setCellValue(c.getLote());
                        row.createCell((int) 7).setCellValue(c.getCaducidad());
                        row.createCell((int) 8).setCellValue(c.getFisico());
                        row.createCell((int) 9).setCellValue(c.getInventario());
                        row.createCell((int) 10).setCellValue(c.getDiferencia());
                        row.createCell((int) 11).setCellValue(c.getTipoCiclico());

                    }

                    try (FileOutputStream fileOut = new FileOutputStream(filename)) {
                        wb.write(fileOut);
                    }
                    String disHeader = "Attachment;Filename=\"Ciclico_" + folio + ".xlsx\"";
                    response.setHeader("Content-Disposition", disHeader);
                    File desktopFile = new File(filename);
                    try (FileInputStream fileInputStream = new FileInputStream(desktopFile)) {
                        int j;
                        while ((j = fileInputStream.read()) != -1) {
                            pw.write(j);
                        }
                        fileInputStream.close();
                    }
                } catch (IOException ex) {
                    Logger.getLogger(inventariosCiclicos.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    try {
                        response.flushBuffer();
                        pw.flush();

                        pw.close();

                    } catch (Exception ex) {
                        Logger.getLogger(inventariosCiclicos.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        HttpSession sesion = request.getSession();
        int bandera = Integer.parseInt(request.getParameter("ban"));

        switch (bandera) {
            case 1:

                JSONObject json = new JSONObject();
                JSONArray jsona = new JSONArray();
                int cantidad = Integer.parseInt(request.getParameter("cantidad"));

                try {

                    ciclicosDao ciclico = new ciclicosDaoImpl();
                    List<ciclicos> listCiclicos = new ArrayList<>();
                    listCiclicos = ciclico.ListaCiclicos(cantidad);

                    for (int i = 0; i < listCiclicos.size(); i++) {
                        ciclicos c = new ciclicos();
                        c = (ciclicos) listCiclicos.get(i);

                        json.put("clave", c.getClave());
                        json.put("lote", c.getLote());
                        json.put("caducidad", c.getCaducidad());
                        json.put("detProduct", c.getDetalleProducto());
                        json.put("idInv", c.getIdInventario());
                        json.put("descripcion", c.getDescripcion());

                        jsona.add(json);
                        json = new JSONObject();
                    }

                    out.print(jsona);
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 2:

                json = new JSONObject();
                jsona = new JSONArray();
                try {
                    String folNew = request.getParameter("detPro");
                    int tipoCiclico = Integer.parseInt(request.getParameter("tipo"));
                    boolean insert = false;
                    ciclicosDao ciclico = new ciclicosDaoImpl();
                    insert = ciclico.insertLista(folNew, tipoCiclico);
                    List<ciclicos> lC = new ArrayList<>();
                    if (insert) {
                        lC = ciclico.ListaResultado();
                        for (int i = 0; i < lC.size(); i++) {
                            ciclicos c = new ciclicos();
                            c = (ciclicos) lC.get(i);

                            json.put("clave", c.getClave());
                            json.put("lote", c.getLote());
                            json.put("caducidad", c.getCaducidad());
                            json.put("detProduct", c.getDetalleProducto());
                            json.put("descripcion", c.getDescripcion());
                            json.put("fisico", c.getFisico());
                            json.put("inventario", c.getInventario());
                            json.put("diferencia", c.getDiferencia());
                            json.put("folio", c.getFolio());
                            jsona.add(json);
                            json = new JSONObject();
                        }

                        out.print(jsona);
                        out.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 3:
                Map TUsu = new LinkedHashMap();
                try {
                    ciclicosDao ciclico = new ciclicosDaoImpl();
                    if (ciclico.guardar()) {
                        TUsu.put("Mensaje", "Ciclico guardado con éxito");
                    } else {
                        TUsu.put("Mensaje", "Error contactar al departamento de sistemas.");
                    }
                    String json1;
                    json1 = new Gson().toJson(TUsu);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().write(json1);
                } catch (Exception e) {
                    Logger.getLogger(inventariosCiclicos.class.getName()).log(Level.SEVERE, null, e);
                }
                break;
            case 4:

                json = new JSONObject();
                jsona = new JSONArray();
                cantidad = Integer.parseInt(request.getParameter("cantidad"));

                try {

                    ciclicosDao ciclico = new ciclicosDaoImpl();
                    List<ciclicos> listCiclicos;
                    listCiclicos = ciclico.ListaCiclicosCosto(cantidad);

                    for (int i = 0; i < listCiclicos.size(); i++) {
                        ciclicos c;
                        c = listCiclicos.get(i);

                        json.put("clave", c.getClave());
                        json.put("lote", c.getLote());
                        json.put("caducidad", c.getCaducidad());
                        json.put("detProduct", c.getDetalleProducto());
                        json.put("idInv", c.getIdInventario());
                        json.put("descripcion", c.getDescripcion());

                        jsona.add(json);
                        json = new JSONObject();
                    }

                    out.print(jsona);
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 5:
                json = new JSONObject();
                jsona = new JSONArray();
                try {
                    int rol = 0;
                    rol = (int) sesion.getAttribute("rol");
                    ciclicosDao ciclico = new ciclicosDaoImpl();
                    List<ciclicos> listCiclicos;
                    listCiclicos = ciclico.ReporteCiclicos(rol);
                    for (int i = 0; i < listCiclicos.size(); i++) {
                        ciclicos c;
                        c = listCiclicos.get(i);

                        json.put("folio", c.getFolio());
                        json.put("fecha", c.getFecha());
                        json.put("hora", c.getHora());
                        json.put("diferencia", c.getDiferencia());
                        json.put("claves", c.getNumeroClaves());
                        json.put("tipo", c.getTipoCiclico());

                        jsona.add(json);
                        json = new JSONObject();
                    }

                    out.print(jsona);
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 6:
                json = new JSONObject();
                jsona = new JSONArray();
                cantidad = Integer.parseInt(request.getParameter("cantidad"));

                try {

                    ciclicosDao ciclico = new ciclicosDaoImpl();
                    List<ciclicos> listCiclicos;
                    listCiclicos = ciclico.ListaCiclicosControlado(cantidad);

                    for (int i = 0; i < listCiclicos.size(); i++) {
                        ciclicos c;
                        c = listCiclicos.get(i);

                        json.put("clave", c.getClave());
                        json.put("lote", c.getLote());
                        json.put("caducidad", c.getCaducidad());
                        json.put("detProduct", c.getDetalleProducto());
                        json.put("idInv", c.getIdInventario());
                        json.put("descripcion", c.getDescripcion());

                        jsona.add(json);
                        json = new JSONObject();
                    }

                    out.print(jsona);
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
