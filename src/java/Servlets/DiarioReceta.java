/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Clases.ConectionDB;
import Modelos.Origen;
import Modelos.RegistroReporteDiario;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Reporte diario de recetas
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class DiarioReceta extends HttpServlet {

    public ConectionDB con;
    public HttpSession sesion;
    public PreparedStatement ps;
    public ResultSet rst;
    public boolean isColectiva;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        String id_usu = (String) sesion.getAttribute("id_usu");
        String f1 = request.getParameter("txtf_caduc");
        String f2 = request.getParameter("txtf_caduci");
        String orie = request.getParameter("id_ori");

        request.setAttribute("f1", f1);
        request.setAttribute("f2", f2);
        request.setAttribute("orie", orie);

        try {
            con.conectar();
            ArrayList<String> unidad = getUnidad(id_usu);
            request.setAttribute("cla_uni", unidad.get(1));
            request.setAttribute("nombre_uni", unidad.get(0));

            ArrayList<Origen> origenes = getOrigenes();
            request.setAttribute("origenes", origenes);
            if (orie != null) {
                for (Origen origen : origenes) {
                    if (orie.equalsIgnoreCase(origen.getId())) {
                        request.setAttribute("nombre_ori", origen.getNombre());
                    }
                }
            }

            int[] totales = {0, 0};
            ArrayList<Object> resultado = getRegistros(unidad.get(1), f1, f2, orie, totales);
            request.setAttribute("registros", resultado.get(0));
            totales = (int[]) resultado.get(1);
            request.setAttribute("totalCajas", totales[0]);
            request.setAttribute("totalPendientes", totales[1]);
            request.setAttribute("totalRecetas", totalRecetas());

        } catch (SQLException ex) {
            Logger.getLogger(DiarioReceta.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }

            } catch (SQLException ex) {
                Logger.getLogger(DiarioReceta.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }
        String dir = "../reportes/diarioReceta.jsp";
        if (isColectiva) {
            dir = "../reportes/diarioRecetaCol.jsp";
        }
        request.getRequestDispatcher(dir).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        con = new ConectionDB();
        sesion = request.getSession();

        isColectiva = request.getParameter("esColectiva") != null ? Boolean.parseBoolean(request.getParameter("esColectiva")) : false;
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        con = new ConectionDB();
        sesion = request.getSession();
        isColectiva = request.getParameter("esColectiva") != null ? Boolean.parseBoolean(request.getParameter("esColectiva")) : false;
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private ArrayList<String> getUnidad(String id_usu) throws SQLException {
        ArrayList<String> resultado = new ArrayList<>();
        String qry = "SELECT un.des_uni, us.cla_uni FROM usuarios us, unidades un WHERE us.cla_uni = un.cla_uni AND id_usu = ?";
        ps = con.getConn().prepareStatement(qry);
        ps.setString(1, id_usu);
        rst = ps.executeQuery();

        while (rst.next()) {
            resultado.add(rst.getString(1));
            resultado.add(rst.getString(2));
        }

        return resultado;
    }

    private ArrayList<Origen> getOrigenes() throws SQLException {
        ArrayList<Origen> origenes = new ArrayList<>();
        String qry = "SELECT origen.id_ori, origen.des_ori FROM origen";
        ps = con.getConn().prepareStatement(qry);
        rst = ps.executeQuery();

        Origen origen;
        while (rst.next()) {
            origen = new Origen(rst.getString(1), rst.getString(2));
            origenes.add(origen);
        }

        return origenes;
    }

    private ArrayList<Object> getRegistros(String cla_uni, String f1, String f2, String orie, int[] totales) throws SQLException {
        ArrayList<Object> resultado = new ArrayList<>();
        ArrayList<RegistroReporteDiario> registros = new ArrayList<>();
        String qry = "SELECT r.fecha_hora, r.fol_rec, m.nom_med, pa.nom_com, p.cla_pro, p.des_pro, dp.lot_pro, dp.cad_pro, dr.can_sol AS sol, dr.cant_sur AS sur, dp.cla_fin, p.amp_pro, dp.id_ori FROM receta r, pacientes pa, medicos m, detreceta dr, detalle_productos dp, productos p WHERE r.cedula = m.cedula AND r.id_rec = dr.id_rec AND dr.det_pro = dp.det_pro AND dp.cla_pro = p.cla_pro AND DATE(r.fecha_hora) BETWEEN ? AND ? AND r.id_tip IN (1,3) AND dr.baja != 1 AND r.id_pac=pa.id_pac GROUP BY dr.fol_det;";
        if (isColectiva) {
            //qry = "SELECT r.fecha_hora, r.fol_rec, m.nom_med, pa.nom_pac, p.cla_pro, p.des_pro, dp.lot_pro, dp.cad_pro, dr.can_sol AS sol, dr.cant_sur AS sur, dp.cla_fin, p.amp_pro FROM unidades un, usuarios us, receta r, pacientes pa, medicos m, detreceta dr, detalle_productos dp, productos p WHERE un.cla_uni = us.cla_uni AND us.id_usu = r.id_usu AND r.cedula = m.cedula AND r.id_rec = dr.id_rec AND dr.det_pro = dp.det_pro AND dp.cla_pro = p.cla_pro AND un.cla_uni = ? AND r.fecha_hora BETWEEN ? AND ? AND dp.id_ori LIKE ? AND r.id_tip = '2' AND dr.baja != 1 GROUP BY dr.fol_det;";
            qry = "SELECT r.fecha_hora,r.fol_rec,m.nom_com,'RECETA COLECTIVA',p.cla_pro,REPLACE(p.des_pro,',',' '),dp.lot_pro,dp.cad_pro,dr.can_sol AS sol,dr.cant_sur AS sur,dp.cla_fin,p.amp_pro, dp.id_ori FROM receta r, medicos m, productos p, detalle_productos dp, detreceta dr  WHERE DATE(r.fecha_hora) BETWEEN ? AND ? AND r.id_tip IN (2,4) AND r.baja!=1 AND r.cedula=m.cedula AND dr.id_rec=r.id_rec AND dr.can_sol>0 AND dr.det_pro=dp.det_pro AND dp.cla_pro=p.cla_pro GROUP BY dr.fol_det;";
        }
        orie = String.format("%%%s%%", orie);
        //f1 = String.format("%s 00:00:00", f1);
        //f2 = String.format("%s 23:59:59", f2);

        ps = con.getConn().prepareStatement(qry);
        //ps.setString(1, cla_uni);
        ps.setString(1, f1);
        ps.setString(2, f2);
        //ps.setString(4, orie);
        rst = ps.executeQuery();

        RegistroReporteDiario registro;
        String financiamiento, valor, cajas;
        int totalCajas = 0, totalPendientes = 0;

        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DecimalFormatSymbols custom = new DecimalFormatSymbols();
        custom.setDecimalSeparator('.');
        custom.setGroupingSeparator(',');
        formatter.setDecimalFormatSymbols(custom);

        while (rst.next()) {
            financiamiento = "";
            valor = rst.getString(11);
            if (valor.equals("1")) {
                financiamiento = "Seguro Popular";
            } else if (valor.equals("2")) {
                financiamiento = "FASSA";
            }

            cajas = formatter.format(rst.getDouble("sur") / rst.getDouble(12));

            registro = new RegistroReporteDiario(rst.getString(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getString(6), rst.getString(7), rst.getString(8), rst.getString(9), rst.getString(10), financiamiento, cajas, rst.getString(13));
            registros.add(registro);

            totalCajas += rst.getInt("sol");
            totalPendientes += rst.getInt("sur");

        }
        totales[0] = totalCajas;
        totales[1] = totalPendientes;

        resultado.add(registros);
        resultado.add(totales);
        return resultado;
    }

    private int totalRecetas() throws SQLException {
        int total = 0;
        String qry = "SELECT COUNT(fol_rec) as recetas FROM receta WHERE baja!=1 AND id_tip=1;";

        if (isColectiva) {
            qry = "SELECT COUNT(fol_rec) as recetas FROM receta WHERE baja!=1 AND id_tip=2 AND fol_rec<>0;";
        }

        ps = con.getConn().prepareStatement(qry);
        rst = ps.executeQuery();

        while (rst.next()) {
            total = rst.getInt("recetas");
        }

        return total;

    }
}
