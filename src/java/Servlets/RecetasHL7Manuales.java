/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Dao.RecetaHL7ManualesDao;
import Impl.RecetaHL7ManualesDaoImpl;
import Modelos.HL7ManualModel;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Recetas hl7
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
@WebServlet(name = "RecetasHL7Manuales", urlPatterns = {"/RecetasHL7Manuales"})
public class RecetasHL7Manuales extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RecetasHL7Manuales</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RecetasHL7Manuales at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        int bandera = Integer.parseInt(request.getParameter("ban"));
        String f1 = "", f2 = "";

        JSONObject json = new JSONObject();
        JSONArray jsona = new JSONArray();

        switch (bandera) {

            case 1:

                f1 = request.getParameter("f1");
                f2 = request.getParameter("f2");

                RecetaHL7ManualesDao n = new RecetaHL7ManualesDaoImpl();

                List<HL7ManualModel> recetae = new ArrayList<HL7ManualModel>();
                recetae = n.byFecOnly(f1, f2);
                for (int i = 0; i < recetae.size(); i++) {

                    HL7ManualModel rcta = new HL7ManualModel();
                    rcta = (HL7ManualModel) recetae.get(i);

                    json.put("fecha", rcta.getFec());
                    json.put("receta", rcta.getRctalaboradas());
                    json.put("recetahl7", rcta.getRctahl7());
                    json.put("recetamanual", rcta.getRctamanuales());
                    json.put("recetasur", rcta.getRctasurtidas());
                    json.put("recetapen", rcta.getRctapendientes());
                    json.put("solicitado", rcta.getPzassol());
                    json.put("surtido", rcta.getPzassur());

                    jsona.add(json);
                    json = new JSONObject();

                }
                out.print(jsona);
                out.close();

                break;

            case 2:

                f1 = request.getParameter("f1");
                f2 = request.getParameter("f2");
                RecetaHL7ManualesDao Manu = new RecetaHL7ManualesDaoImpl();
                List<HL7ManualModel> recetam = new ArrayList<HL7ManualModel>();
                recetam = Manu.byFecOnlyManual(f1, f2);
                for (int i = 0; i < recetam.size(); i++) {

                    HL7ManualModel rcta = new HL7ManualModel();
                    rcta = (HL7ManualModel) recetam.get(i);

                    json.put("fecha", rcta.getFec());
                    json.put("receta", rcta.getFolio());
                    json.put("medico", rcta.getMedico());
                    json.put("paciente", rcta.getPaciente());
                    json.put("solicitado", rcta.getPzassol());
                    json.put("surtido", rcta.getPzassur());

                    jsona.add(json);
                    json = new JSONObject();

                }
                out.print(jsona);
                out.close();

                break;
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
