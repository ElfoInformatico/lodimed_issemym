/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Clases.ConectionDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;

/**
 * Creación de recetas
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class Receta extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        ConectionDB con = new ConectionDB();
        String query;
        PreparedStatement ps;
        ResultSet rs;

        try {

            if (request.getParameter("encargado") != null) {
                /*
                 *Para Receta Colectiva
                 */
                JSONObject json = new JSONObject();
                JSONArray jsona = new JSONArray();
                con.conectar();
                HttpSession sesion = request.getSession(true);
                String folio_rec = request.getParameter("folio");
                String id_rec = "";
                if (folio_rec.equals("")) {
                    try {
                        ResultSet rset = con.consulta("SELECT id_rec FROM indices");
                        while (rset.next()) {
                            id_rec = rset.getString(1);
                        }
                        if (id_rec.equals("")) {
                            con.actualizar("INSERT INTO indices (id_rec) values ('2')");
                            id_rec = "1";
                        } else {
                            con.actualizar("UPDATE indices SET id_rec= '" + (Integer.parseInt(id_rec) + 1) + "' ");
                        }
                        json.put("fol_rec", id_rec);
                    } catch (SQLException | NumberFormatException e) {
                        Logger.getLogger(Receta.class.getName()).log(Level.SEVERE, null, e);
                    }
                    out.println();
                    sesion.setAttribute("folio_rec", id_rec);
                    folio_rec = id_rec;
                } else {
                    json.put("fol_rec", folio_rec);
                }
                try {
                    int ban = 0;
                    ResultSet rset2 = con.consulta("SELECT id_rec FROM receta WHERE fol_rec = '" + folio_rec + "' ");
                    while (rset2.next()) {
                        ban = 1;
                    }
                    if (ban == 0) {
                        con.insertar("INSERT INTO receta VALUES ('0', '" + folio_rec + "', '0', '-', '2', '" + sesion.getAttribute("id_usu") + "', '" + request.getParameter("encargado") + "', '-', '" + request.getParameter("select_serv") + "', NOW(), '1', '0', '0','')");
                    }
                    try {
                        if (request.getParameter("btn_clave").equals("1")) {
                            ResultSet rset = con.consulta("select des_pro, amp_pro from productos where cla_pro = '" + request.getParameter("cla_pro") + "' ");
                            while (rset.next()) {
                                json.put("des_pro", rset.getString(1));
                                json.put("amp_pro", rset.getString(2));
                            }
                            rset = con.consulta("SELECT sum(i.cant)\n"
                                    + "FROM productos p, detalle_productos dp, inventario i\n"
                                    + "WHERE\n"
                                    + "p.cla_pro = dp.cla_pro\n"
                                    + "and dp.det_pro = i.det_pro\n"
                                    + "and p.cla_pro='" + request.getParameter("cla_pro") + "' and p.f_status='A'\n"
                                    + ";");
                            while (rset.next()) {
                                json.put("total", rset.getString(1));
                            }

                            rset = con.consulta("SELECT sum(i.cant)\n"
                                    + "FROM productos p, detalle_productos dp, inventario i\n"
                                    + "WHERE\n"
                                    + "p.cla_pro = dp.cla_pro\n"
                                    + "and dp.det_pro = i.det_pro\n"
                                    + "and dp.id_ori = '1'\n"
                                    + "and p.cla_pro='" + request.getParameter("cla_pro") + "' and p.f_status='A'\n"
                                    + ";");
                            while (rset.next()) {
                                json.put("origen1", rset.getString(1));
                            }

                            rset = con.consulta("SELECT sum(i.cant)\n"
                                    + "FROM productos p, detalle_productos dp, inventario i\n"
                                    + "WHERE\n"
                                    + "p.cla_pro = dp.cla_pro\n"
                                    + "and dp.det_pro = i.det_pro\n"
                                    + "and dp.id_ori = '2'\n"
                                    + "and p.cla_pro='" + request.getParameter("cla_pro") + "' and p.f_status='A'\n"
                                    + ";");
                            while (rset.next()) {
                                json.put("origen2", rset.getString(1));
                            }

                            con.cierraConexion();
                            jsona.add(json);
                            json = new JSONObject();
                        }
                    } catch (Exception e) {
                        Logger.getLogger(Receta.class.getName()).log(Level.SEVERE, null, e);
                    }
                    try {
                        if (request.getParameter("btn_descripcion").equals("1")) {
                            ResultSet rset = con.consulta("select cla_pro, amp_pro from productos where des_pro = '" + request.getParameter("des_pro") + "' ");
                            while (rset.next()) {
                                json.put("cla_pro", rset.getString(1));
                                json.put("amp_pro", rset.getString(2));
                            }
                            rset = con.consulta("SELECT sum(i.cant)\n"
                                    + "FROM productos p, detalle_productos dp, inventario i\n"
                                    + "WHERE\n"
                                    + "p.cla_pro = dp.cla_pro\n"
                                    + "and dp.det_pro = i.det_pro\n"
                                    + "and p.des_pro='" + request.getParameter("des_pro") + "' and p.f_status='A'\n"
                                    + ";");
                            while (rset.next()) {
                                json.put("total", rset.getString(1));
                            }
                            rset = con.consulta("SELECT sum(i.cant)\n"
                                    + "FROM productos p, detalle_productos dp, inventario i\n"
                                    + "WHERE\n"
                                    + "p.cla_pro = dp.cla_pro\n"
                                    + "and dp.det_pro = i.det_pro\n"
                                    + "and dp.id_ori = '1'\n"
                                    + "and p.des_pro='" + request.getParameter("des_pro") + "' and p.f_status='A'\n"
                                    + ";");
                            while (rset.next()) {
                                json.put("origen1", rset.getString(1));
                            }
                            rset = con.consulta("SELECT sum(i.cant)\n"
                                    + "FROM productos p, detalle_productos dp, inventario i\n"
                                    + "WHERE\n"
                                    + "p.cla_pro = dp.cla_pro\n"
                                    + "and dp.det_pro = i.det_pro\n"
                                    + "and dp.id_ori = '2'\n"
                                    + "and p.des_pro='" + request.getParameter("des_pro") + "' and p.f_status='A'\n"
                                    + ";");
                            while (rset.next()) {
                                json.put("origen2", rset.getString(1));
                            }
                            con.cierraConexion();
                            jsona.add(json);
                        }
                    } catch (Exception e) {
                        Logger.getLogger(Receta.class.getName()).log(Level.SEVERE, null, e);
                    }
                } catch (Exception e) {
                    Logger.getLogger(Receta.class.getName()).log(Level.SEVERE, null, e);
                }
                out.println(jsona);
            } else {
                /*
                 *Para Receta Farmacia
                 */
                DateFormat df2 = new SimpleDateFormat("dd/MM/yyyy");
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                JSONObject json = new JSONObject();
                JSONArray jsona = new JSONArray();
                con.conectar();
                HttpSession sesion = request.getSession(true);
                String folio_rec = request.getParameter("folio");
                String id_rec = "";
                if (folio_rec.equals("")) {
                    try {
                        String idRec = "";
                        ResultSet rset = con.consulta("sele max(id_rec) as id_rec from receta where id_usu = '" + sesion.getAttribute("id_usu") + "' and transito='1'");
                        while (rset.next()) {
                            idRec = rset.getString("id_rec");
                        }
                        if (idRec.equals("")) {
                            //id_rec = "1";

                            rset = con.consulta("select m.folAct from medicos m, usuarios u where u.cedula = m.cedula and u.id_usu = '" + sesion.getAttribute("id_usu") + "' ");
                            while (rset.next()) {
                                id_rec = rset.getString(1);
                            }
                            con.actualizar("update medicos m, usuarios u set m.folAct= '" + (Integer.parseInt(id_rec) + 1) + "' where u.cedula = m.cedula and u.id_usu = '" + sesion.getAttribute("id_usu") + "' ");

                        } else {

                            rset = con.consulta("select m.folAct from medicos m, usuarios u where u.cedula = m.cedula and u.id_usu = '" + sesion.getAttribute("id_usu") + "' ");
                            while (rset.next()) {
                                id_rec = rset.getString(1);
                            }
                            if (Integer.parseInt(idRec) + 1 == Integer.parseInt(id_rec)) {
                                con.actualizar("update medicos m, usuarios u set m.folAct= '" + (Integer.parseInt(id_rec) + 1) + "' where u.cedula = m.cedula and u.id_usu = '" + sesion.getAttribute("id_usu") + "' ");
                            } else {
                                id_rec = idRec;
                            }
                        }
                        json.put("fol_rec", id_rec);
                    } catch (SQLException | NumberFormatException e) {
                        Logger.getLogger(Receta.class.getName()).log(Level.SEVERE, null, e);
                    }
                    out.println();
                } else {
                    json.put("fol_rec", folio_rec);
                }
                int ban = 0;
                try {

                    query = "SELECT id_pac, nom_com, sexo, fec_nac, num_afi,expediente,fin_vig, IF ( CURDATE() <= fin_vig, 1, IF ( CURDATE() <= DATE_ADD(fin_vig, INTERVAL 60 DAY), 2, 0 )) AS vigencia FROM pacientes WHERE f_status='A' AND num_afi = ?";
                    ps = con.getConn().prepareStatement(query);
                    ps.setString(1, request.getParameter("sp_pac"));
                    rs = ps.executeQuery();

                    while (rs.next()) {
                        ban = 1;
                        sesion.setAttribute("folio_rec", folio_rec);
                        sesion.setAttribute("id_pac", rs.getString(1));
                        sesion.setAttribute("nom_com", rs.getString(2));
                        sesion.setAttribute("sexo", rs.getString(3));
                        sesion.setAttribute("fec_nac", df2.format(df.parse(rs.getString(4))));
                        sesion.setAttribute("num_afi", rs.getString(5));
                        sesion.setAttribute("carnet", rs.getString(6));
                        int vigencia = rs.getInt("vigencia");
                        json.put("id_pac", rs.getString(1));
                        json.put("nom_com", rs.getString(2));
                        json.put("sexo", rs.getString(3));
                        json.put("fec_nac", df2.format(df.parse(rs.getString(4))));
                        json.put("num_afi", rs.getString(5));
                        json.put("carnet", rs.getString(6));
                        json.put("mensaje", "");
                        json.put("vig", vigencia);
                        jsona.add(json);
                        json = new JSONObject();
                    }
                    if (ban == 0) {
                        json.put("id_pac", "");
                        json.put("nom_com", "");
                        json.put("sexo", "");
                        json.put("fec_nac", "");
                        json.put("num_afi", "");
                        json.put("carnet", "");
                        json.put("mensaje", "Paciente inexistente");
                        jsona.add(json);
                    }
                } catch (SQLException | ParseException e) {
                    Logger.getLogger(Receta.class.getName()).log(Level.SEVERE, null, e);
                }
                out.println(jsona);
            }

        } catch (Exception e) {
            Logger.getLogger(Receta.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(Receta.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
