/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Dao.solSurDao;
import Impl.solSurDaoImpl;
import Modelos.solSur;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Reporte solicitado y surtido
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class repSolSur extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet repSolSur</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet repSolSur at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        int bandera = Integer.parseInt(request.getParameter("ban"));
        switch (bandera) {
            case 1:
                String f1 = request.getParameter("hora_ini");
                String f2 = request.getParameter("hora_fin");
                // Se inicializan variables Json
                JSONObject json = new JSONObject();
                JSONArray jsona = new JSONArray();
                // Se inicializa la interfaz para recuperar datos del lado de la implementación.
                solSurDao sol = new solSurDaoImpl();
                //Se inicializa una lista para su llenado.
                List<solSur> ls = new ArrayList<>();
                //Se mandan variables para la correcta implementacion.
                ls = sol.datos(f1, f2);
                //Se inicia el ciclo for para llenado de variables Json
                for (int i = 0; i < ls.size(); i++) {
                    solSur s = new solSur();
                    s = (solSur) ls.get(i);

                    json.put("cons", s.getIdRec());
                    json.put("fol", s.getFolio());
                    json.put("cl", s.getClave());
                    json.put("des", s.getDescrip());
                    json.put("sol", s.getSol());
                    json.put("sur", s.getSur());

                    jsona.add(json);
                    json = new JSONObject();
                }
                //Se manda a la vista los datos obtenidos.
                out.print(jsona);
                out.close();

                break;
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
