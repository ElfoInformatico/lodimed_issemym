/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Clases.ConectionDB;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Reporte dispensado por receta
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
@WebServlet(name = "ReporteDispensadoRec", urlPatterns = {"/ReporteDispRec"})
public class ReporteDispensadoRec extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        ConectionDB con = new ConectionDB();
        JSONObject json = new JSONObject();
        JSONArray jsona = new JSONArray();
        PreparedStatement ps;
        ResultSet rs;
        String accion;
        String query, fechaInicial, fechaFinal, tipo;
        accion = request.getParameter("accion");
        tipo = request.getParameter("tipo");

        if (accion == null) {
//            System.out.println("Queso");
            accion = "";
        }

        query = "SELECT p.cla_pro, p.des_pro, sum(dr.cant_sur) as cant FROM productos p, detalle_productos dp, detreceta dr, receta r, usuarios u, unidades un WHERE r.id_tip IN (1,3) and p.cla_pro = dp.cla_pro AND dp.det_pro = dr.det_pro AND dr.id_rec = r.id_rec AND r.id_usu = u.id_usu AND u.cla_uni = un.cla_uni AND DATE(r.fecha_hora)  BETWEEN ? and ? and dr.baja!=1 and dr.cant_sur<>0 GROUP BY p.cla_pro, dr.baja  ORDER BY dp.cla_pro+0 ASC ;";

        if (tipo.equals("2")) {
            query = "SELECT p.cla_pro, p.des_pro, sum(dr.cant_sur) as cant FROM productos p, detalle_productos dp, detreceta dr, receta r, usuarios u, unidades un WHERE r.id_tip IN (2,4) and p.cla_pro = dp.cla_pro AND dp.det_pro = dr.det_pro AND dr.id_rec = r.id_rec AND r.id_usu = u.id_usu AND u.cla_uni = un.cla_uni AND DATE(r.fecha_hora)  BETWEEN ? and ? and dr.baja!=1 and dr.cant_sur<>0 GROUP BY p.cla_pro, dr.baja  ORDER BY dp.cla_pro+0 ASC ;";
        }

        if (accion.equals("receta")) {
            response.setContentType("text/html;charset=UTF-8");

            fechaInicial = request.getParameter("fechaInicial");
            fechaFinal = request.getParameter("fechaFinal");
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date tempDate = simpleDateFormat.parse(fechaInicial);
                SimpleDateFormat outputDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                fechaInicial = outputDateFormat.format(tempDate);
                tempDate = simpleDateFormat.parse(fechaFinal);
                fechaFinal = outputDateFormat.format(tempDate);
            } catch (ParseException ex) {
                Logger.getLogger(ReporteDispensadoRec.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {

                con.conectar();

                ps = con.getConn().prepareStatement(query);
                ps.setString(1, fechaInicial);
                ps.setString(2, fechaFinal);
                rs = ps.executeQuery();
                while (rs.next()) {
                    json.put("clave", rs.getString("cla_pro"));
                    json.put("descripcion", rs.getString("des_pro"));
                    json.put("cantidad", rs.getString("cant"));
                    jsona.add(json);
                    json = new JSONObject();
                }
                out.println(jsona);
            } catch (SQLException ex) {
                Logger.getLogger(ReporteMovClaes.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                out.close();
                try {
                    con.cierraConexion();
                } catch (SQLException ex) {
                    Logger.getLogger(ReporteMovClaes.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        if (accion.equals("generarExcelReceta")) {
            fechaInicial = request.getParameter("fechaInicial");
            fechaFinal = request.getParameter("fechaFinal");

            String leyenda = "Reporte Dispensado Receta   ";
            String nombre = "ReporteDispensadoReceta.xlsx";
            if (tipo.equals("2")) {
                leyenda = "Reporte Dispensado Receta   ";
                nombre = "ReporteDispensadoColectivo.xlsx";
            }
            leyenda += fechaInicial + " al " + fechaFinal;
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date tempDate = simpleDateFormat.parse(fechaInicial);
                SimpleDateFormat outputDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                fechaInicial = outputDateFormat.format(tempDate);
                tempDate = simpleDateFormat.parse(fechaFinal);
                fechaFinal = outputDateFormat.format(tempDate);
            } catch (ParseException ex) {
                Logger.getLogger(ReporteDispensadoRec.class.getName()).log(Level.SEVERE, null, ex);
            }

            PrintWriter pw = null;
            try {
                pw = response.getWriter();
                response.setContentType("text/html;charset=UTF-8");
                HttpSession sesion = request.getSession();

                String path = sesion.getServletContext().getRealPath("/");
                String contextName = request.getContextPath().substring(1);

                char pathSep = File.separatorChar;
                path = path.substring(0, path.lastIndexOf(pathSep + contextName));
                path = path + pathSep;
                String filename = path + nombre;
                String sheetName = "Reporte";
                XSSFWorkbook wb = new XSSFWorkbook();

                XSSFSheet sheet = wb.createSheet(sheetName);
                int width = 20;
                sheet.setAutobreaks(true);
                sheet.setDefaultColumnWidth(width);

                con.conectar();
                int index = 0;

                Row rowLeyenda = sheet.createRow((short) index);
                Cell cell = rowLeyenda.createCell((short) 0);
                cell.setCellValue(leyenda);
                sheet.addMergedRegion(new CellRangeAddress(
                        0, //first row (0-based)
                        0, //last row  (0-based)
                        0, //first column (0-based)
                        2 //last column  (0-based)
                ));
                index++;
                rowLeyenda = sheet.createRow((short) index);
                cell = rowLeyenda.createCell((short) 0);
                cell.setCellValue("");
                sheet.addMergedRegion(new CellRangeAddress(
                        1, //first row (0-based)
                        1, //last row  (0-based)
                        0, //first column (0-based)
                        2 //last column  (0-based)
                ));

                index++;
                XSSFRow rowHeadInv = sheet.createRow(index);
                rowHeadInv.createCell((int) 0).setCellValue("Clave");
                rowHeadInv.createCell((int) 1).setCellValue("Descripción");
                rowHeadInv.createCell((int) 2).setCellValue("Cantidad");

                ps = con.getConn().prepareStatement(query);
                ps.setString(1, fechaInicial);
                ps.setString(2, fechaFinal);

                rs = ps.executeQuery();
                while (rs.next()) {
                    index++;
                    XSSFRow row = sheet.createRow(index);
                    row.createCell((int) 0).setCellValue(rs.getString(1));
                    row.createCell((int) 1).setCellValue(rs.getString(2));
                    row.createCell((int) 2).setCellValue(rs.getString(3));
                }

                try (FileOutputStream fileOut = new FileOutputStream(filename)) {
                    wb.write(fileOut);
                }
                String disHeader = "Attachment;Filename=\"" + nombre + "\"";
                response.setHeader("Content-Disposition", disHeader);
                File desktopFile = new File(filename);
                try (FileInputStream fileInputStream = new FileInputStream(desktopFile)) {
                    int j;
                    while ((j = fileInputStream.read()) != -1) {
                        pw.write(j);
                    }
                    fileInputStream.close();
                }
            } catch (IOException | SQLException ex) {
                Logger.getLogger(ReporteDispensadoRec.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    response.flushBuffer();
                    pw.flush();
                    pw.close();
                    con.cierraConexion();
                } catch (IOException | SQLException ex) {
                    Logger.getLogger(ReporteDispensadoRec.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
