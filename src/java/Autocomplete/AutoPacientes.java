/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autocomplete;

import Clases.ConectionDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Autocomplete Pacientes
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class AutoPacientes extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        ConectionDB con = new ConectionDB();
        JSONObject json = new JSONObject();
        JSONArray jsona = new JSONArray();
        String nombre = request.getParameter("nombre");
        boolean esReceta = null != request.getParameter("receta");

        try {
            con.conectar();

            PreparedStatement ps;
            nombre = "%" + nombre + "%";
            String qry = "SELECT id_pac, nom_com FROM pacientes WHERE nom_com LIKE ? AND f_status='A' LIMIT 0,50";
            if (esReceta) {
                qry = "SELECT id_pac, nom_com, f_status FROM pacientes WHERE nom_com LIKE ? LIMIT 0,50";
            }
            ps = con.getConn().prepareStatement(qry);
            ps.setString(1, nombre);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                json.put("id_pac", rset.getString("id_pac"));
                json.put("nom_com", rset.getString("nom_com"));
                if (esReceta) {
                    json.put("status", rset.getString("f_status"));
                }
                jsona.add(json);
                json = new JSONObject();
            }
            out.println(jsona);

        } catch (SQLException ex) {
            Logger.getLogger(AutoPacientes.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(AutoPacientes.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
