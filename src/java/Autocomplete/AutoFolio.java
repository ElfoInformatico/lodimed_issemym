/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autocomplete;

import Clases.ConectionDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Autocomplete folios
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class AutoFolio extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        ConectionDB con = new ConectionDB();
        JSONObject json = new JSONObject();
        JSONArray jsona = new JSONArray();
        String fol_rec = "", qry = "", qryPendientes = "", qrySurtidas = "", qryCanceladas = "";
        String folio = request.getParameter("folio");
        String Tipo = request.getParameter("Tipo");

        try {
            con.conectar();

            PreparedStatement ps;
            folio = "%" + folio + "%";
            qryPendientes = "SELECT fol_rec FROM receta r INNER JOIN (SELECT id_rec,SUM(can_sol) AS can_sol,SUM(cant_sur) AS cant_sur FROM detreceta GROUP BY id_rec) d on r.id_rec=d.id_rec AND d.can_sol!=d.cant_sur WHERE baja!=1 AND fol_rec LIKE ? LIMIT 0,50";

            qrySurtidas = "SELECT fol_rec FROM receta r INNER JOIN (SELECT id_rec,SUM(can_sol) AS can_sol,SUM(cant_sur) AS cant_sur FROM detreceta GROUP BY id_rec) d on r.id_rec=d.id_rec AND d.cant_sur>0 WHERE baja!=1 AND fol_rec LIKE ? LIMIT 0,50";

            qryCanceladas = "SELECT fol_rec FROM receta r INNER JOIN (SELECT id_rec,SUM(can_sol) AS can_sol,SUM(cant_sur) AS cant_sur FROM detreceta GROUP BY id_rec) d on r.id_rec=d.id_rec WHERE baja=1 AND fol_rec LIKE ? LIMIT 0,50";

            switch (Tipo) {
                case "Pendientes":
                    qry = qryPendientes;
                    break;
                case "Surtidas":
                    qry = qrySurtidas;
                    break;
                case "Canceladas":
                    qry = qryCanceladas;
                    break;
            }

            ps = con.getConn().prepareStatement(qry);
            ps.setString(1, folio);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                fol_rec = rset.getString(1);

                if (fol_rec.contains("HL7")) {
                    fol_rec = fol_rec.substring(3);
                }

                json.put("fol_rec", fol_rec);

                jsona.add(json);
                json = new JSONObject();
            }
            out.println(jsona);

        } catch (SQLException ex) {
            Logger.getLogger(AutoFolio.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(AutoFolio.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
