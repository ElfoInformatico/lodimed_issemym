/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Modelos.Paciente;
import java.sql.SQLException;
import java.util.List;

/**
 * Busca todos los pacientes que posean el mismo folio de afilación.
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 * @throws SQLException
 */
public interface PacienteDao {

    public List<Paciente> obtenerAfiliados(String folio) throws SQLException;

    public Paciente obtenerVigencias(String folio) throws SQLException;

    public Paciente reconstruirPaciente(String nombre) throws SQLException;

    public long adicionarPaciente(Paciente paciente) throws SQLException;

    public Paciente reconstruirPaciente(long idPaciente) throws SQLException;
}
