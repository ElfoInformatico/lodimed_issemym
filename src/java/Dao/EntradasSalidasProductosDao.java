/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Modelos.MovimientoProducto;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface entradas y salidas movimiento al inventario
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface EntradasSalidasProductosDao {

public int apartarInsumo(int detalleProducto, int cantidadDisminuir) throws SQLException;

public void quitarApartado(int detalleProducto) throws SQLException;

public int disminuirPorAjuste(int detalleProducto, int cantidadDisminuir, String observaciones) throws SQLException;

public int disminuirPorTraspaso(int detalleProducto, int cantidadDisminuir, String observaciones) throws SQLException;

public int disminuirPorTraspaso(int idApartado, String observaciones) throws SQLException;

public int adicionarPorEntrada(String clave, String lote, String caducidad, String codigo, int cantidadAdicionar, String observaciones) throws SQLException;

public List<MovimientoProducto> listarProductoDisponible(String claveCodigoBarras) throws SQLException;

public MovimientoProducto obtenerProducto(int detalleProducto) throws SQLException;
}
