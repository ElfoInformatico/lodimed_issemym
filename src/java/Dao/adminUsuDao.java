/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Modelos.Usuario;

/**
 * Interface de administración de usuarios
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface adminUsuDao {

    //Interfaz para obtener los datos del usuario para su edición
    public Usuario Usu(int id);

    //Edición del usuario
    public boolean edit(int id, String nom, String ap, String am, String pass, String sts);

    //Nuevo usuario
    public boolean save(String nom, String ap, String am, String pass, String user, String uni, int rol);

    //Nombre de la unidad
    public String uniName(String uni);

    //Checar usuario en uso
    public String user(String user);

    //Baja de usuario
    public boolean borrar(int id);
}
