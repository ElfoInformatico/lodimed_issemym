/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Clases.ConectionDB;
import Modelos.BQuerySQL;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * QuerySql
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class QuerySQL {

    private ConectionDB conn;
    private BQuerySQL vars;

    public QuerySQL() {
        try {
            this.conn.conectar();
        } catch (SQLException ex) {
            finishQuery();
            Logger.getLogger(QuerySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void fillSelect(String consulta, String columnId, String columnName) {
        vars = new BQuerySQL();

        try {
            vars.setPreStatemnt(this.conn.getConn().prepareStatement(consulta));
            if (vars.getRs() != null) {
                while (vars.getRs().next()) {
                    vars.getRs().getString(columnId);
                    vars.getRs().getString(columnName);
                }
            }
        } catch (SQLException ex) {
            finishQuery();
            Logger.getLogger(QuerySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void finishQuery() {
        if (this.conn.estaConectada()) {
            try {
                this.conn.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(QuerySQL.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
