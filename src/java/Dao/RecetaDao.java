/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Modelos.DetalleCapturado;
import Modelos.DetalleReceta;
import Modelos.LoteDisponible;
import Modelos.Receta;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface receta agregar y surtir
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface RecetaDao {

    public boolean validarFolio(String folio) throws SQLException;

    public void cancelarReceta(String folio, int idReceta, int tipoReceta, int idUsuario) throws SQLException;

    public void cancelarDetalle(int idReceta, String claveProducto, int tipoReceta, int idUsuario) throws SQLException;

    public int surtirReceta(int idReceta, int detalleProducto, int cantidadSurtida, int tipoSistemas) throws SQLException;

    public boolean cambiarLote(int idReceta, int nuevoDetalleProducto, int cantidad) throws SQLException;

    public List<LoteDisponible> obtenerLotesDisponibles(String codigoBarras, int idReceta) throws SQLException;

    public boolean surtirReceta(DetalleReceta detalle, int cantidadSurtir) throws SQLException;

    public int agregarDetalle(DetalleReceta detalle) throws SQLException;

    public int contarClavesPorReceta(int idReceta, String clave) throws SQLException;

    public String capturaMedicamento(int idReceta, String clave, int cantSol, String indica, String cause, String cause2, int tipoCaptura) throws SQLException;

    public int agregarDetalleProducto(String clave) throws SQLException;

    public int crearEncabezado(int tipoReceta, long idPaciente, int idMedico, int idUsuario, String folio, String tipoConsulta, String fecha, String carnet) throws SQLException;

    public int crearEncabezado(int tipoReceta, int idMedico, int idUsuario, int idServicio, String folio, String fecha) throws SQLException;

    public void validarReceta(int idReceta, int tipoCaptura) throws SQLException;

    public void finalizarReceta(int idReceta) throws SQLException;

    public Receta reconstruirReceta(int idReceta) throws SQLException;

    public List<DetalleCapturado> reconstruirDetalles(int receta) throws SQLException;

    public boolean validarMensaje(int folio) throws SQLException;
}
