/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Modelos.registro;
import java.util.List;

/**
 * Interface Lista de ingreso al sistema
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface entradasDao {

    public List<registro> lR();
}
