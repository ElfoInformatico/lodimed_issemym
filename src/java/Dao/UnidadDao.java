/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

/**
 * Interface datos unidad médica
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface UnidadDao {

    String getUnidadByDescripcion(String descUnidad);

    String getUnidadByIdHl7(String idUnidad);

}
