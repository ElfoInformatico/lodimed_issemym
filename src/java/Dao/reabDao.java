/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Modelos.reabast;
import java.util.List;

/**
 * Interface de reabastecimiento y lista de cdm
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface reabDao {

    public List<reabast> lR();

    public List<reabast> dias(int dias);

    public String upload(String cu);
}
