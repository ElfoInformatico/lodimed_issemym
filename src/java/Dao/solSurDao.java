/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Modelos.solSur;
import java.util.List;

/**
 * Interface Lista solicitado y surtido
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface solSurDao {

    //Interfaz que recupera los valores obtenidos de la implementación    
    public List<solSur> datos(String f1, String f2);
}
