/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Modelos.HL7ManualModel;
import java.util.List;

/**
 * Interaface receta manual hl7
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface RecetaHL7ManualesDao {

    @Deprecated
    public void hl7manual();

    public List<HL7ManualModel> byFecOnly(String f1, String f2);

    public List<HL7ManualModel> byFecOnlyManual(String f1, String f2);
}
