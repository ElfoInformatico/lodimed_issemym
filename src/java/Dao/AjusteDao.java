/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Interface datos del medicamento
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface AjusteDao {

    public JSONObject buscarInsumo(String claveProducto, String cb);

    public JSONArray traerOrigen(String clave, String claveUnidad);

    public JSONArray traerCaducidad(String clave);

    public JSONArray traerLotes(String clave);

    public JSONObject seleccionar(String claveProducto, String lote, String caducidad, String idOrigen);

    public String traerClaveUnidad(String usuario);

}
