/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import java.sql.SQLException;
import mx.gnk.hl7.bean.MensajeHl7;

/**
 * Interface agrega y verifica receta hl7
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface MensajeHl7Dao {

    public Long addMensajeHl7(MensajeHl7 mensaje) throws SQLException;

    public boolean checkIfHl7MessageExists(Integer idrec, Integer nummsg) throws SQLException;

}
