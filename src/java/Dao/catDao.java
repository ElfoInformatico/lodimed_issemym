/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Modelos.catalogo;

/**
 * Interface de administración catálogos
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface catDao {

    public catalogo lc(int nivel);

    public catalogo lcExist(int nivel);

}
