/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Modelos.Kardex;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface agregar movimiento y consulta kardex
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface KardexDao {

    public boolean agregarMovimiento(Kardex nuevoMovimiento) throws SQLException;

    public List<Kardex> getKardexByIdRecDetPro(Long idReceta, Long detPro);
}
