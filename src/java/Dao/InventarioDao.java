package Dao;

import Modelos.Inventario;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface Actualización de inventario
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface InventarioDao {

    public List<Inventario> obtenerDisponibles(String clave) throws SQLException;

    public boolean actualizaInventario(int idInventario, int nuevaExistencia) throws SQLException;

    public int agregaLote(int detalleProducto, int cantidad) throws SQLException;

}
