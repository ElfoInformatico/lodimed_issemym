/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Modelos.Usuario;

/**
 * Interface validación de usuarios
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface UsuarioDao {

    public Usuario validar(String usuario, String Password);

    public void insertEntrada(String usuario, int status);

    public int tipSistema();
}
