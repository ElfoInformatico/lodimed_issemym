/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import java.sql.SQLException;
import mx.gnk.hl7.bean.RecetaHl7Log;

/**
 * Interface Registrar receta HL7 log
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface Hl7LogDao {
    
    public Long addRecetaHl7Log(RecetaHl7Log log) throws SQLException;
    
}
