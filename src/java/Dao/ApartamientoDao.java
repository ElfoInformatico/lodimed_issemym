package Dao;

import Modelos.Apartado;
import java.sql.SQLException;

/**
 * Interface apartado de insumo médico para el surtido
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface ApartamientoDao {

    public int agregarApartado(int cantidad, int detalleProducto, int detalleReceta, String tipo) throws SQLException;

    public void modificarApartado(int detalleReceta, int status) throws SQLException;

    public void modificarApartado(int detalleReceta, int detalleProducto, int status) throws SQLException;
    
    public void modificarApartadoID(int idApartado, int status) throws SQLException;

    public Apartado reconstruirApartado(int id) throws SQLException;
    
    
}
