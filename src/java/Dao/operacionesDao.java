/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

/**
 * Interface parametros
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface operacionesDao {

    public int folRan();

    public boolean newRang(int rango);

    public boolean tipFarm(int tipSis);

    public int tipSis();

    public String prefijo();

    public boolean updatePrefijo(String pre);

    public int frecuencia(String cu);

    public boolean updateFrecuencia(int frecuencia, String cu);

    public boolean tipCaptura(int tipCap);

    public int tipCap();
}
