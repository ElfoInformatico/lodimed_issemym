/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Modelos.reabModel;

/**
 * Interface de requerimiento unidad
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface reqDao {

    public reabModel totPz();

    public void generaCsv(String ruta);

}
