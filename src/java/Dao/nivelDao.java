/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Modelos.nivelModel;
import java.util.List;

/**
 * Interface consulta por fecha
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface nivelDao {

    @Deprecated
    public void actNivel();

    public List<nivelModel> byFecOnly(String f1, String f2);
}
