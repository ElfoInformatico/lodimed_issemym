/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Modelos.ciclicos;
import java.util.List;

/**
 * Interface de Inventario ciclico
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface ciclicosDao {

    public List<ciclicos> ListaCiclicos(int cantidadClaves);

    public boolean insertLista(String jsonCaptura, int tipoInventario);

    public List<ciclicos> ListaResultado();

    public boolean guardar();

    public List<ciclicos> ListaCiclicosCosto(int cantidadClaves);

    public List<ciclicos> ReporteCiclicos(int tipo);

    public List<ciclicos> reporteExcel(int folio);

    public List<ciclicos> ListaCiclicosControlado(int cantidadClaves);
}
