/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Modelos.kardexVista;
import java.util.List;

/**
 * Interface kardex movimiento
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public interface kardexViewDao {

    public List<kardexVista> listaKardexByClave(String clave);

    public kardexVista datosKardex(String clave);

}
