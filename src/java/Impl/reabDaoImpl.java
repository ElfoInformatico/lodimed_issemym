/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Impl;

import Clases.ConectionDB;
import Clases.ConectionDBPruebas;
import Dao.reabDao;
import Modelos.reabast;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implementación de reabastecimiento de acuerdo al cdm
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class reabDaoImpl implements reabDao {

    ConectionDB con = new ConectionDB();
    ConectionDBPruebas con1 = new ConectionDBPruebas();

    //Obtener reabastecimiento
    @Override
    public List<reabast> lR() {
        //Declaración de variables
        List<reabast> lr = new ArrayList<>();
        //Variables de comparacion de fechas
        String fechaCarga = "";
        int ban = 0;
        Date date;
        int cu = 0;
        int frecuenciaPaso = 0;
        Date fechaDate1 = null;
        Date fechaDate2 = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fechaSistema = "";
        String fechaAnt = "";
        //
        //Variables de calculo de reabastecimiento
        String cant_rf = "0", cant_rc = "0", cant_inv = "0";
        float cant_total = 0, con_diario = 0, cons_dia = 0, cant_quincenal = 0, cant_semana = 0, sobre = 0, cant_re = 0, x = 0;
        float min_con = 0, y = 0;
        double dia_abasto2 = 0.0;
        int dias = 0, dia_abasto = 0, exist_fut = 0, total_t = 0;

        try {
            //Conexion bd

            con.conectar();
            // Se obtiene la fecha de la carga del cdm
            String qryFec = "SELECT IFNULL(DATE_FORMAT(MAX(FFec),'%Y-%m-%d'), '2016-01-01') AS fecha FROM tbcdm;";

            PreparedStatement ps = con.getConn().prepareStatement(qryFec);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                fechaCarga = rset.getString("fecha");
            }

            try {
                // Se hace la rutina para anexar un mes a la fecha de carga, al superar esta fecha el sistema tomará 
                //datos de sus propios consumos
                date = sdf.parse(fechaCarga);
                Calendar c = Calendar.getInstance();
                c.setTime(date);
                c.add(Calendar.MONTH, 1);  // añade un mes

                Date nuevaFecha = c.getTime();
                fechaCarga = sdf.format(nuevaFecha);

                Date fechaActual = new Date();
                SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
                fechaSistema = formateador.format(fechaActual);

                fechaDate1 = formateador.parse(fechaCarga);
                fechaDate2 = formateador.parse(fechaSistema);

                c = Calendar.getInstance();
                c.setTime(fechaActual);
                c.add(Calendar.MONTH, -1);
                Date fecAnterior = c.getTime();
                fechaAnt = sdf.format(fecAnterior);

            } catch (ParseException ex) {
                Logger.getLogger(reabDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (fechaDate1.before(fechaDate2)) {
                ban = 0;
            } else {
                ban = 1;
            }

            //Seleccionar claves activas de la tabla productos
            String qryClave = "select cla_pro, des_pro from productos where f_status=? order by cla_pro";
            String borrar = "TRUNCATE TABLE reabastecimiento;";
            ps = con.getConn().prepareStatement(borrar);
            ps.executeUpdate();
            ps = con.getConn().prepareStatement(qryClave);
            ps.setString(1, "A");
            ResultSet rs = ps.executeQuery();
            String reabastecimiento = "INSERT INTO reabastecimiento (clave, cdm, inventario,sobreAbasto, stockMaximo,sugerido) VALUES (?,?,?,?,?,?)";
            PreparedStatement ps2 = con.getConn().prepareStatement(reabastecimiento);
            while (rs.next()) {
                reabast r = new reabast();

                r.setClave(rs.getString("cla_pro"));
                r.setDes(rs.getString("des_pro"));

                if (ban == 1) {
                    String qryCdm = "SELECT FCdm FROM tbcdm WHERE cla_pro=?";
                    ps = con.getConn().prepareStatement(qryCdm);
                    ps.setString(1, r.getClave());
                } else {
                    String qryCdm = "select sum(can_sol) from recetas where DATE(fecha_hora) between ? and DATE(NOW()) and cla_pro=? and baja!=1 group by cla_pro";
                    ps = con.getConn().prepareStatement(qryCdm);
                    ps.setString(1, fechaAnt);
                    ps.setString(2, r.getClave());
                }

                ResultSet rs2 = ps.executeQuery();
                if (rs2.next()) {
                    cant_rf = rs2.getString(1);
                    if (cant_rf.equals("")) {
                        cant_rf = "0";
                    }
                } else {
                    cant_rf = "0";
                }

                String qry_inv = "select i.cla_uni,sum(i.cant) from inventario i, detalle_productos dp where i.det_pro = dp.det_pro and dp.cla_pro=? group by dp.cla_pro";
                ps = con.getConn().prepareStatement(qry_inv);
                ps.setString(1, r.getClave());
                rs2 = ps.executeQuery();
                if (rs2.next()) {
                    cant_inv = rs2.getString(2);
                    if (cant_inv.equals("")) {
                        cant_inv = "0";
                    }
                    cu = rs2.getInt(1);
                } else {
                    cant_inv = "0";
                }

                String qryPaso = "SELECT frecuenciaDePaso FROM unidades WHERE cla_uni=?;";
                ps = con.getConn().prepareStatement(qryPaso);
                ps.setInt(1, cu);
                rs2 = ps.executeQuery();
                while (rs2.next()) {
                    frecuenciaPaso = rs2.getInt("frecuenciaDePaso");
                }
                cant_total = (Float.parseFloat(cant_rf));

                if (cant_total > -1) {
                    con_diario = (cant_total / 30);
                    cons_dia = con_diario;
                    dia_abasto2 = Math.ceil(cons_dia * dias);
                    dia_abasto = (int) (dia_abasto2);
                    cant_quincenal = (float) ((Math.ceil(cant_total / frecuenciaPaso)) * 2);
                    cant_semana = (float) (Math.ceil(cant_quincenal / 2));

                    exist_fut = (Integer.parseInt(cant_inv)) - dia_abasto;

                    cant_re = (cant_quincenal) - ((int) (exist_fut));
                    if (cant_re <= 0) {
                        sobre = (cant_re) * -1;
                        cant_re = 0;
                    }
                    x = 3;
                    y = 30;
                    min_con = (x / y);

                    total_t = (int) (cant_re);

                    if (exist_fut <= 0) {
                        total_t = (int) cant_quincenal;
                    }
                    if (exist_fut > (int) (cant_quincenal)) {//Sobreabasto
                        total_t = 0;
                    }
                    if (con_diario <= min_con) {
                        total_t = 1;
                    }
                    if (cant_total == 0) {
                        total_t = 0;
                    }
                    if (cant_total == 1) {
                        cant_quincenal = 1;
                        total_t = 1;
                    }

                    r.setCdm((int) cant_total);
                    r.setIfs((int) cant_quincenal);
                    r.setInv(Integer.parseInt(cant_inv));
                    r.setSobreAbasto((int) sobre);
                    r.setCantSug(total_t);

                    ps2.setString(1, r.getClave());
                    ps2.setInt(2, r.getCdm());
                    ps2.setInt(3, r.getInv());
                    ps2.setInt(4, r.getSobreAbasto());
                    ps2.setInt(5, r.getIfs());
                    ps2.setInt(6, r.getCantSug());
                    ps2.addBatch();

                    lr.add(r);

                    con_diario = 0;
                    cons_dia = 0;
                    dia_abasto = 0;
                    dia_abasto2 = 0;
                    cant_quincenal = 0;
                    cant_semana = 0;
                    exist_fut = 0;
                    cant_re = 0;
                    sobre = 0;
                    min_con = 0;
                    total_t = 0;
                }

            }
            ps2.executeBatch();

        } catch (SQLException ex) {
            Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }

        return lr;
    }

    @Override
    public List<reabast> dias(int dias) {

        //Declaración de variables
        List<reabast> lr = new ArrayList<>();
        //Variables de comparacion de fechas
        String fechaCarga = "";
        int ban = 0;
        Date date;
        Date fechaDate1 = null;
        Date fechaDate2 = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fechaSistema = "";
        //
        //Variables de calculo de reabastecimiento
        String cant_rf = "0", cant_rc = "0", cant_inv = "0";
        float cant_total = 0, con_diario = 0, cons_dia = 0, cant_quincenal = 0, cant_semana = 0, sobre = 0, cant_re = 0, x = 0;
        float min_con = 0, y = 0;
        double dia_abasto2 = 0.0;
        int dia_abasto = 0, exist_fut = 0, total_t = 0;

        try {
            //Conexion bd

            con.conectar();
            // Se obtiene la fecha de la carga del cdm
            String qryFec = "SELECT IFNULL(DATE_FORMAT(MAX(FFec),'%Y-%m-%d'), '2016-01-01') AS fecha FROM tbcdm;";
            PreparedStatement ps = con.getConn().prepareStatement(qryFec);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                fechaCarga = rset.getString("fecha");
            }

            try {
                // Se hace la rutina para anexar un mes a la fecha de carga, al superar esta fecha el sistema tomará 
                //datos de sus propios consumos
                date = sdf.parse(fechaCarga);
                Calendar c = Calendar.getInstance();
                c.setTime(date);
                c.add(Calendar.MONTH, 1);  // añade un mes

                Date nuevaFecha = c.getTime();
                fechaCarga = sdf.format(nuevaFecha);

                Date fechaActual = new Date();
                SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
                fechaSistema = formateador.format(fechaActual);

                fechaDate1 = formateador.parse(fechaCarga);
                fechaDate2 = formateador.parse(fechaSistema);

            } catch (ParseException ex) {
                Logger.getLogger(reabDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (fechaDate1.before(fechaDate2)) {
                ban = 0;
            } else {
                ban = 1;
            }

            //Seleccionar claves activas de la tabla productos
            String qryClave = "select cla_pro, des_pro from productos where f_status=? order by cla_pro";

            ps = con.getConn().prepareStatement(qryClave);
            ps.setString(1, "A");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                reabast r = new reabast();

                r.setClave(rs.getString("cla_pro"));
                r.setDes(rs.getString("des_pro"));

                if (ban == 1) {
                    String qryCdm = "SELECT FCdm FROM tbcdm WHERE cla_pro=?";
                    ps = con.getConn().prepareStatement(qryCdm);
                    ps.setString(1, r.getClave());
                } else {
                    String qryCdm = "select sum(can_sol) from recetas where DATE(fecha_hora) between ? and DATE(NOW()) and cla_pro='?' and baja!=1 group by cla_pro";
                    ps = con.getConn().prepareStatement(qryCdm);
                    ps.setString(1, fechaSistema);
                    ps.setString(2, r.getClave());
                }

                ResultSet rs2 = ps.executeQuery();
                if (rs2.next()) {
                    cant_rf = rs2.getString(1);
                    if (cant_rf.equals("")) {
                        cant_rf = "0";
                    }
                } else {
                    cant_rf = "0";
                }

                String qry_inv = "select sum(i.cant) from inventario i, detalle_productos dp where i.det_pro = dp.det_pro and dp.cla_pro=? group by dp.cla_pro";
                ps = con.getConn().prepareStatement(qry_inv);
                ps.setString(1, r.getClave());
                rs2 = ps.executeQuery();
                if (rs2.next()) {
                    cant_inv = rs2.getString(1);
                    if (cant_inv.equals("")) {
                        cant_inv = "0";
                    }
                } else {
                    cant_inv = "0";
                }
                cant_total = (Float.parseFloat(cant_rf));

                if (cant_total > -1) {
                    con_diario = (cant_total / 30);
                    cons_dia = con_diario;
                    dia_abasto2 = Math.ceil(cons_dia * dias);
                    dia_abasto = (int) (dia_abasto2);
                    cant_quincenal = (float) (Math.ceil(cant_total / 4));
                    cant_semana = (float) (Math.ceil(cant_quincenal / 2));

                    exist_fut = (Integer.parseInt(cant_inv)) - dia_abasto;

                    cant_re = (cant_quincenal) - ((int) (exist_fut));
                    if (cant_re <= 0) {
                        sobre = (cant_re) * -1;
                        cant_re = 0;
                    }
                    x = 3;
                    y = 30;
                    min_con = (x / y);

                    total_t = (int) (cant_re);

                    if (exist_fut <= 0) {
                        total_t = (int) cant_quincenal;
                    }
                    if (exist_fut > (int) (cant_quincenal)) {//Sobreabasto
                        total_t = 0;
                    }
                    if (con_diario <= min_con) {
                        total_t = 1;
                    }

                    r.setCdm((int) cant_total);
                    r.setIfs((int) cant_quincenal);
                    r.setInv(Integer.parseInt(cant_inv));
                    r.setSobreAbasto((int) sobre);
                    r.setCantSug(total_t);
                    lr.add(r);

                    con_diario = 0;
                    cons_dia = 0;
                    dia_abasto = 0;
                    dia_abasto2 = 0;
                    cant_quincenal = 0;
                    cant_semana = 0;
                    exist_fut = 0;
                    cant_re = 0;
                    sobre = 0;
                    min_con = 0;
                    total_t = 0;
                }

            }

        } catch (SQLException ex) {
            Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }

        return lr;
    }

    @Override
    public String upload(String cu) {
        String arriba = "fallo";
        try {
            con.conectar();
            con1.conectar();
            String fechaSistema = "";
            int encontrado = 0;
            Date fechaActual = new Date();
            SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
            fechaSistema = formateador.format(fechaActual);

            String qry = "SELECT clave_unidad FROM unireq WHERE DATE(fecha_carga)='" + fechaSistema + "' AND tipoReq=1 AND clave_unidad='" + cu + "' GROUP BY clave_unidad;";
            PreparedStatement ps = con1.getConn().prepareStatement(qry);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                encontrado = rset.getInt("clave_unidad");
            }

            if (encontrado != 0) {
                arriba = "esta";
            } else {
                qry = "SELECT clave,sugerido,inventario,cdm FROM reabastecimiento;";
                ps = con.getConn().prepareStatement(qry);
                rset = ps.executeQuery();
                while (rset.next()) {
                    con1.actualizar("INSERT INTO unireq  (clave_unidad,clave_producto, piezas_requeridas, fecha_carga, fecha_validacion,solicitado,observaciones,tipoReq,inventarioUnidad,cpm) VALUES ('" + cu + "','" + rset.getString("clave") + "','" + rset.getString("sugerido") + "','" + fechaSistema + "','" + fechaSistema + "','" + rset.getString("sugerido") + "','-','1','" + rset.getString("inventario") + "','" + rset.getString("cdm") + "')");
                }
                arriba = "Exito";
            }

        } catch (SQLException ex) {
            Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                    con1.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }

        return arriba;
    }

}
