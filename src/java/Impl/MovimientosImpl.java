package Impl;

import Clases.ConectionDB;
import Dao.EntradasSalidasProductosDao;
import Modelos.Apartado;
import Modelos.Kardex;
import Modelos.MovimientoProducto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Permite realizar el inserción y consulta de movimientos al inventario
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class MovimientosImpl implements EntradasSalidasProductosDao {

int idUsuario;
ConectionDB con;

public final static String OBTENER_DETALLE_PRODUCTO_POR_ID_APARTADO
          = "SELECT a.detalle_producto, a.cantidad from apartamiento a where a.id = ? and status = '1'";

public final static String OBTENER_NUEVO_INVENTARIO_QUERY
          = "SELECT i.id_inv AS id, (i.cant - ?) AS nuevoInventario FROM inventario AS i WHERE i.det_pro = ? FOR UPDATE;";

public final static String OBTENER_DETALLE_CB_QUERY
          = "SELECT IFNULL(dp.det_pro, - 1) AS id_detalle, IFNULL(cb.F_Id, - 1) AS id_codigo FROM ( SELECT F_Clave AS id, F_Id FROM tb_codigob WHERE F_Clave = ? AND F_Lote = ? AND F_Cadu = ? ) AS cb LEFT OUTER JOIN ( SELECT cla_pro AS id, det_pro FROM detalle_productos WHERE cla_pro = ? AND lot_pro = ? AND cad_pro = ? ) AS dp ON dp.id = cb.id UNION SELECT IFNULL(dp.det_pro, - 1) AS id_detalle, IFNULL(cb.F_Id, - 1) AS id_codigo FROM ( SELECT F_Clave AS id, F_Id FROM tb_codigob WHERE F_Clave = ? AND F_Lote = ? AND F_Cadu = ? ) AS cb RIGHT OUTER JOIN ( SELECT cla_pro AS id, det_pro FROM detalle_productos WHERE cla_pro = ? AND lot_pro = ? AND cad_pro = ? ) AS dp ON dp.id = cb.id FOR UPDATE;";

public final static String INSERT_DETALLE_PRODUCTO_QUERY
          = "INSERT INTO `detalle_productos` SET `cla_pro` = ?, `lot_pro` = ?, `cad_pro` = ?, `cla_fin` = '1', `id_ori` = '0', `web` = '0';";

public final static String INSERT_CODIGO_BARRAS_QUERY = "INSERT INTO `tb_codigob` SET `F_Cb` = ?, `F_Clave` = ?, `F_Lote` = ?, `F_Cadu` = ?;";

public final static String OBTENER_DETALLE_QUERY = "SELECT det_pro AS id_detalle FROM detalle_productos WHERE cla_pro = ? AND lot_pro = ? AND cad_pro = ?";

public final static String OBTENER_PRODUCTOS_QUERY
          = "SELECT DISTINCT dp.det_pro AS detalle_producto, dp.cla_pro AS clave, dp.lot_pro AS lote, dp.cad_pro AS caducidad, p.des_pro AS descripcion, IFNULL(i.inventario, 0) - IFNULL(a.apartado, 0) AS cantidad, i.id AS id_inventario, dp.id_ori AS origen, o.des_ori AS origen_descripcion FROM detalle_productos AS dp INNER JOIN origen AS o ON o.id_ori = dp.id_ori INNER JOIN tb_codigob AS cb ON cb.F_Clave = dp.cla_pro INNER JOIN productos AS p ON dp.cla_pro = p.cla_pro INNER JOIN ( SELECT inv.id_inv AS id, inv.det_pro AS producto, SUM(inv.cant) AS inventario FROM inventario AS inv WHERE inv.cant > 0 GROUP BY inv.det_pro ) AS i ON i.producto = dp.det_pro LEFT JOIN ( SELECT ap.detalle_producto AS producto, SUM(ap.cantidad) AS apartado FROM apartamiento AS ap USE INDEX (producto_status) WHERE ap.`status` = ? GROUP BY ap.detalle_producto ) AS a ON a.producto = dp.det_pro WHERE (dp.cla_pro = ? OR cb.F_Cb = ?) FOR UPDATE;";

public final static String INSERT_PRODUCTO_FANTASMA
          = "INSERT INTO `productos` SET `cla_pro` = 'NINGUNA', `des_pro` = 'PRODUCTO USADO PARA MOVIMIENTOS EN EL SISTEMA', `tip_pro` = 'MEDICAMENTO', `cos_pro` = '0.00', `pres_pro` = 'pz', `amp_pro` = '1', `f_status` = 'A', `cla_lic` = '-', `origen` = '0';";

public final static String INSERT_DETALLE_PRODUCTO_FANTASMA
          = "INSERT INTO `detalle_productos` SET `cla_pro` = 'NINGUNA', `lot_pro` = '-', `cad_pro` = '2020-01-01', `cla_fin` = '1', `id_ori` = '0', `web` = '0';";

public final static String INSERT_DETALLE_RECETA_FANTASMA
          = "INSERT INTO `detreceta` SET `det_pro` = ?, `can_sol` = '0', `cant_sur` = '0', `fec_sur` = '2020-01-01', `status` = '3', `id_rec` = '1', `hor_sur` = '15:24:00', `id_cau` = 'Z00', `indicaciones` = 'DETALLE PARA MOVIMIENTOS DE PRODUCTOS', `baja` = '1', `web` = '0', `id_cau2` = '';";

public final static String OBTENER_DETALLE_RECETA_FANTASMA
          = "SELECT dr.fol_det AS id FROM detreceta AS dr INNER JOIN detalle_productos AS dp ON dr.det_pro = dp.det_pro WHERE dp.cla_pro = 'NINGUNA' FOR UPDATE;";

public final static String OBTENER_PRODUCTO_FANTASMA
          = "SELECT cla_pro FROM productos WHERE cla_pro = 'NINGUNA' FOR UPDATE;";

public final static String OBTENER_PRODUCTO_QUERY
          = "SELECT DISTINCT dp.det_pro AS detalle_producto, dp.cla_pro AS clave, dp.lot_pro AS lote, dp.cad_pro AS caducidad, p.des_pro AS descripcion, IFNULL(i.cant, 0) AS cantidad, dp.id_ori AS origen, IFNULL(i.id_inv, - 1) AS id_inventario, p.pres_pro AS presentacion FROM detalle_productos AS dp INNER JOIN productos AS p ON dp.cla_pro = p.cla_pro LEFT JOIN inventario AS i ON i.det_pro = dp.det_pro WHERE dp.det_pro = ?;";

public final static String OBTENER_DETALLE_APARTADO = "SELECT detalle_producto, cantidad from apartamiento where id=?;";

public final static int EXISTANCIA_INSUFIENTE_ERROR = -1;

public MovimientosImpl(int idUsuario, ConectionDB con) {
    this.idUsuario = idUsuario;
    this.con = con;
}

public int generarMovimientoIDPRoducto(int idApartamiento, String observacion) throws SQLException {

    InventarioImpl inventario = new InventarioImpl();
    inventario.setCon(con);

    PreparedStatement ps;
    ResultSet rs;

    ApartamientoImpl apartamientoImpl = new ApartamientoImpl(con);
    int detalleReceta = obtenerDetalleReceta();

    ps = con.getConn().prepareStatement(OBTENER_DETALLE_PRODUCTO_POR_ID_APARTADO,
              ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
    ps.setInt(1, idApartamiento);

    rs = ps.executeQuery();

    int detalleProducto = 0;
    int cantidad = 0;

    if (rs.next()) {
        detalleProducto = rs.getInt("detalle_producto");
        cantidad = rs.getInt("cantidad");

        generarMovimientoProducto(detalleProducto, cantidad, Kardex.AJUSTE_SALIDA_RECETA_TIPO, observacion, Kardex.ID_RECETA_SALIDA_MOVIMIENTO);

    }

    apartamientoImpl.modificarApartadoID(idApartamiento, Apartado.DISPENSADO_STATUS);

    return detalleProducto;

}

private int generarMovimientoProducto(int detalleProducto, int cantidadDisminuir,
          String tipoMovimiento, String observacion, int idReceta) throws SQLException {

    InventarioImpl inventario = new InventarioImpl();
    inventario.setCon(con);

    PreparedStatement ps;
    ResultSet rs;

    ps = con.getConn().prepareStatement(OBTENER_NUEVO_INVENTARIO_QUERY,
              ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
    ps.setInt(1, cantidadDisminuir);
    ps.setInt(2, detalleProducto);

    rs = ps.executeQuery();

    int idInventario, nuevaCantidad;
    if (rs.next()) {
        idInventario = rs.getInt("id");
        nuevaCantidad = rs.getInt("nuevoInventario");
    } else {
        idInventario = inventario.agregaLote(detalleProducto, 0);
        nuevaCantidad = (0 - cantidadDisminuir);
    }

    ps.close();
    rs.close();

    if (nuevaCantidad < 0) {
        return EXISTANCIA_INSUFIENTE_ERROR;
    }

    boolean resultado = inventario.actualizaInventario(idInventario, nuevaCantidad);
    if (!resultado) {
        String mensajeError = String.format("NO se modifico el inventario para el movimiento.| detalleProducto: %d | id_inventario: %d | cantidad: %d ",
                  detalleProducto, idInventario, cantidadDisminuir);
        throw new SQLException(mensajeError);
    }

    Kardex kardex = new Kardex();
    kardex.setIdReceta(idReceta);
    kardex.setTipoMovimiento(tipoMovimiento);
    kardex.setNombreAbasto(Kardex.SIN_ABASTO);
    kardex.setObservacion(observacion);
    kardex.setIdUsuario(this.idUsuario);
    kardex.setDetalleProducto(detalleProducto);
    kardex.setCantidad(cantidadDisminuir);

    KardexImpl kardexImpl = new KardexImpl();
    kardexImpl.setCon(con);

    resultado = kardexImpl.agregarMovimiento(kardex);
    if (!resultado) {
        String mensajeError = String.format("NO se modifico el kardex para el movimiento.| detalleProducto: %d | kardex: %s ", detalleProducto, kardex.
                  toString());
        throw new SQLException(mensajeError);
    }

    return nuevaCantidad;
}

@Override
public void quitarApartado(int detalleProducto) throws SQLException {

    ApartamientoImpl apartamientoImpl = new ApartamientoImpl(con);
    apartamientoImpl.modificarApartadoID(detalleProducto, Apartado.CANCELADO_STATUS);

}

public void quitarApartadoID(int idApartado) throws SQLException {

    ApartamientoImpl apartamientoImpl = new ApartamientoImpl(con);
    int detalleReceta = obtenerDetalleReceta();
    apartamientoImpl.modificarApartado(idApartado, Apartado.CANCELADO_STATUS);

}

@Override
public int apartarInsumo(int detalleProducto, int cantidadDisminuir) throws SQLException {

    ApartamientoImpl apartamientoImpl = new ApartamientoImpl(con);
    int detalleReceta = obtenerDetalleReceta();
    int id = apartamientoImpl.agregarApartado(cantidadDisminuir, detalleProducto,
              detalleReceta, Apartado.POR_AJUSTE_TIPO);

    return id;
}

@Override
public int disminuirPorAjuste(int detalleProducto, int cantidadDisminuir,
          String observaciones) throws SQLException {

    ApartamientoImpl apartamientoImpl = new ApartamientoImpl(con);
    int detalleReceta = obtenerDetalleReceta();

    int nuevaExistencia = generarMovimientoProducto(detalleProducto, cantidadDisminuir,
              Kardex.AJUSTE_SALIDA_RECETA_TIPO, observaciones, Kardex.ID_RECETA_SALIDA_MOVIMIENTO);

    apartamientoImpl.modificarApartado(detalleReceta, detalleProducto, Apartado.DISPENSADO_STATUS);

    return nuevaExistencia;
}

@Override
public int disminuirPorTraspaso(int detalleProducto, int cantidadDisminuir,
          String observaciones) throws SQLException {

    ApartamientoImpl apartamientoImpl = new ApartamientoImpl(con);
    int detalleReceta = obtenerDetalleReceta();
    apartamientoImpl.agregarApartado(cantidadDisminuir, detalleProducto, detalleReceta, Apartado.POR_AJUSTE_TIPO);

    int nuevaExistencia = generarMovimientoProducto(detalleProducto, cantidadDisminuir,
              Kardex.TRANSPASO_SALIDA_RECETA_TIPO, observaciones, Kardex.ID_RECETA_SALIDA_MOVIMIENTO);

    apartamientoImpl.modificarApartado(detalleReceta, detalleProducto, Apartado.DISPENSADO_STATUS);

    return nuevaExistencia;

}

@Override
public int disminuirPorTraspaso(int idApartado, String observaciones) throws SQLException {
    int nuevaExistencia = 0;

    PreparedStatement ps;
    ResultSet rs;
    ps = con.getConn().prepareStatement(OBTENER_DETALLE_APARTADO);
    ps.setString(1, idApartado + "");
    rs = ps.executeQuery();
    while (rs.next()) {
        System.out.println(rs.getString(1));
        generarMovimientoIDPRoducto(idApartado, observaciones);
        //disminuirPorAjuste(idApartado, rs.getInt(2), observaciones);
    }
    /*nuevaExistencia = generarMovimientoProducto(detalleProducto, cantidadDisminuir,
                Kardex.TRANSPASO_SALIDA_RECETA_TIPO, observaciones, Kardex.ID_RECETA_SALIDA_MOVIMIENTO);*/
    return nuevaExistencia;
}

@Override
public int adicionarPorEntrada(String clave, String lote, String caducidad,
          String codigoBarras, int cantidadAdicionar, String observaciones) throws SQLException {
    int detalleProducto = RecetaImpl.NO_ENCONTRADO, idCB = RecetaImpl.NO_ENCONTRADO;

    PreparedStatement ps, psInsert;
    ResultSet rs, rsInsert;

    ps = con.getConn().prepareStatement(OBTENER_DETALLE_CB_QUERY);
    ps.setString(1, clave);
    ps.setString(2, lote);
    ps.setString(3, caducidad);
    ps.setString(4, clave);
    ps.setString(5, lote);
    ps.setString(6, caducidad);
    ps.setString(7, clave);
    ps.setString(8, lote);
    ps.setString(9, caducidad);
    ps.setString(10, clave);
    ps.setString(11, lote);
    ps.setString(12, caducidad);

    rs = ps.executeQuery();
    if (rs.next()) {
        detalleProducto = rs.getInt("id_detalle");
        idCB = rs.getInt("id_codigo");
    }

    if (detalleProducto == RecetaImpl.NO_ENCONTRADO) {
        psInsert = con.getConn().prepareStatement(INSERT_DETALLE_PRODUCTO_QUERY,
                  PreparedStatement.RETURN_GENERATED_KEYS);
        psInsert.setString(1, clave);
        psInsert.setString(2, lote);
        psInsert.setString(3, caducidad);

        boolean ok = psInsert.executeUpdate() > 0;
        if (!ok) {
            ps.close();
            String mensajeError = String.format("NO se pudo inserta el detalle del producto.| clave: %s | lote: %s | caducidad: %s", clave, lote, caducidad);
            throw new SQLException(mensajeError);
        }

        rsInsert = psInsert.getGeneratedKeys();
        rsInsert.next();
        detalleProducto = rsInsert.getInt(1);

        rsInsert.close();
        psInsert.close();
    }

    if (idCB == RecetaImpl.NO_ENCONTRADO) {
        psInsert = con.getConn().prepareStatement(INSERT_CODIGO_BARRAS_QUERY);
        psInsert.setString(1, codigoBarras);
        psInsert.setString(2, clave);
        psInsert.setString(3, lote);
        psInsert.setString(4, caducidad);

        boolean ok = psInsert.executeUpdate() > 0;
        if (!ok) {
            ps.close();
            String mensajeError = String.format("NO se pudo insertar en codigo de barras.| clave: %s | lote: %s | caducidad: %s", clave, lote, caducidad);
            throw new SQLException(mensajeError);
        }

        psInsert.close();
    }

    cantidadAdicionar *= -1; //Necesario para que sume la cantidad;
    return generarMovimientoProducto(detalleProducto, cantidadAdicionar,
              Kardex.TRANSPASO_ENTRADA_RECETA_TIPO, observaciones, Kardex.ID_RECETA_ENTRADA_MOVIMIENTO);
}

@Override
public List<MovimientoProducto> listarProductoDisponible(String claveCodigoBarras) throws SQLException {
    List<MovimientoProducto> resultado = new ArrayList<>();

    PreparedStatement ps;

    ps = con.getConn().prepareStatement(OBTENER_PRODUCTOS_QUERY,
              ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
    ps.setInt(1, Apartado.APARTADO_STATUS);
    ps.setString(2, claveCodigoBarras);
    ps.setString(3, claveCodigoBarras);

    ResultSet rs;
    rs = ps.executeQuery();

    MovimientoProducto aux;
    while (rs.next()) {
        aux = new MovimientoProducto();
        aux.detalleProducto.setId(rs.getInt("detalle_producto"));
        aux.detalleProducto.setClave(rs.getString("clave"));
        aux.detalleProducto.setCaducidad(rs.getString("caducidad"));
        aux.detalleProducto.setLote(rs.getString("lote"));
        aux.detalleProducto.setOrigen(rs.getInt("origen"));
        aux.detalleProducto.setOrigenDescripcion(rs.getString("origen_descripcion"));

        aux.inventario.setCantidad(rs.getInt("cantidad"));
        aux.inventario.setDetalleProducto(rs.getInt("detalle_producto"));
        aux.inventario.setId(rs.getInt("id_inventario"));

        aux.producto.setClave(rs.getString("clave"));
        aux.producto.setDescripcion(rs.getString("descripcion"));

        resultado.add(aux);
    }

    rs.close();
    ps.close();

    return resultado;
}

private int agregarDetalleReceta() throws SQLException {
    PreparedStatement ps,ps2;
    boolean ok;
    ps = con.getConn().prepareStatement(OBTENER_PRODUCTO_FANTASMA);
    ResultSet rs;
    rs = ps.executeQuery();
    if (!rs.next()) {

        ps2 = con.getConn().prepareStatement(INSERT_PRODUCTO_FANTASMA);
        ok = ps2.executeUpdate() > 0;
        if (!ok) {
            ps2.close();
            String mensajeError = "NO se pudo insertar en producto para movimientos.";
            throw new SQLException(mensajeError);
        }
    }
    ps.close();

    ps = con.getConn().prepareStatement(INSERT_DETALLE_PRODUCTO_FANTASMA, PreparedStatement.RETURN_GENERATED_KEYS);
    ok = ps.executeUpdate() > 0;
    if (!ok) {
        ps.close();
        String mensajeError = "NO se pudo insertar en detalle producto para movimientos.";
        throw new SQLException(mensajeError);
    }

    rs = ps.getGeneratedKeys();
    rs.next();
    int id = rs.getInt(1);
    rs.close();
    ps.close();

    ps = con.getConn().prepareStatement(INSERT_DETALLE_RECETA_FANTASMA, PreparedStatement.RETURN_GENERATED_KEYS);
    ps.setInt(1, id);
    ok = ps.executeUpdate() > 0;
    if (!ok) {
        ps.close();
        String mensajeError = "NO se pudo insertar en detalle receta para movimientos.";
        throw new SQLException(mensajeError);
    }

    rs = ps.getGeneratedKeys();
    rs.next();
    id = rs.getInt(1);
    rs.close();
    ps.close();

    return id;
}

private int obtenerDetalleReceta() throws SQLException {

    PreparedStatement ps;

    ps = con.getConn().prepareStatement(OBTENER_DETALLE_RECETA_FANTASMA,
              ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);

    ResultSet rs;
    rs = ps.executeQuery();

    int id = 0;

    if (!rs.next()) {
        rs.close();
        ps.close();
        return agregarDetalleReceta();
    }

    id = rs.getInt("id");
    rs.close();
    ps.close();

    return id;
}

@Override
public MovimientoProducto obtenerProducto(int detalleProducto) throws SQLException {
    PreparedStatement ps;

    ps = con.getConn().prepareStatement(OBTENER_PRODUCTO_QUERY);
    ps.setInt(1, detalleProducto);

    ResultSet rs;
    rs = ps.executeQuery();

    MovimientoProducto aux;
    rs.next();
    aux = new MovimientoProducto();
    aux.detalleProducto.setId(rs.getInt("detalle_producto"));
    aux.detalleProducto.setClave(rs.getString("clave"));
    aux.detalleProducto.setCaducidad(rs.getString("caducidad"));
    aux.detalleProducto.setLote(rs.getString("lote"));

    aux.inventario.setCantidad(rs.getInt("cantidad"));
    aux.inventario.setDetalleProducto(rs.getInt("detalle_producto"));
    aux.inventario.setId(rs.getInt("id_inventario"));

    aux.producto.setClave(rs.getString("clave"));
    aux.producto.setDescripcion(rs.getString("descripcion"));
    aux.producto.setPresentacion(rs.getString("presentacion"));

    rs.close();
    ps.close();

    return aux;
}
}
