/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Impl;

import Clases.ConectionDB;
import Dao.adminUsuDao;
import Modelos.Usuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Consulta y registro de usuarios
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class adminUsuDaoImpl implements adminUsuDao {

    ConectionDB con = new ConectionDB();

    @Override
    public Usuario Usu(int id) {

        //Se inicializa el objeto users el cual contiene los datos de nuestra tabla usuarios
        Usuario u = new Usuario();
        try {
            con.conectar();
            String qry = "SELECT  u.id_usu AS id, u.nombre AS nom, u.ape_pat AS ap, u.ape_mat AS am, un.des_uni AS nomUni, u.baja AS sts, u.`user` AS usu FROM usuarios u, unidades un WHERE u.cla_uni=un.cla_uni  AND u.id_usu=" + id + ";";
            ResultSet rs = con.consulta(qry);
            while (rs.next()) {
                u.setId(rs.getInt("id"));

                u.setNom(rs.getString("nom"));
                u.setaP(rs.getString("ap"));
                u.setaM(rs.getString("am"));
                u.setUni(rs.getString("nomUni"));
                u.setBaja(rs.getInt("sts"));
                if (u.getBaja() == 1) {
                    u.setDesBaj("A");
                } else {
                    u.setDesBaj("S");
                }
                u.setUsu(rs.getString("usu"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(adminUsuDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(adminUsuDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }

        return u;

    }

    @Override
    public boolean edit(int id, String nom, String ap, String am, String pass, String sts) {
        boolean save = false;
        try {
            int estatus = 0;
            if (sts.equals("A")) {
                estatus = 1;
            }

            String reemplazo = "";
            if (!pass.isEmpty()) {
                reemplazo = String.format(" pass=MD5('%s'), ", pass);
            }
            con.conectar();
            con.insertar("UPDATE usuarios SET nombre='" + nom + "', ape_pat='" + ap + "', ape_mat='" + am + "'," + reemplazo + " baja=" + estatus + " WHERE id_usu=" + id + " ");
            save = true;
        } catch (SQLException ex) {
            Logger.getLogger(adminUsuDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(adminUsuDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }
        return save;
    }

    @Override
    public boolean save(String nom, String ap, String am, String pass, String user, String uni, int rol) {

        boolean guardar = false;

        try {
            con.conectar();
            String qry = "INSERT INTO usuarios (nombre,user,pass,rol,baja,cla_uni,cedula,email,ape_pat,ape_mat,nombre_completo) VALUES ('" + nom + "','" + user + "', MD5('" + pass + "')," + rol + ",1,'" + uni + "','-','','" + ap + "','" + am + "', '" + nom + " " + ap + " " + am + "');";
            con.insertar(qry);
            guardar = true;
        } catch (SQLException ex) {
            Logger.getLogger(adminUsuDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(adminUsuDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }

        return guardar;
    }

    @Override
    public String uniName(String uni) {
        String unidad = "";

        try {
            con.conectar();
            String qry = "SELECT des_uni FROM unidades WHERE cla_uni='" + uni + "';";
            ResultSet rs = con.consulta(qry);
            while (rs.next()) {
                unidad = rs.getString("des_uni");
            }
        } catch (SQLException ex) {
            Logger.getLogger(adminUsuDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(adminUsuDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }

        return unidad;
    }

    @Override
    public String user(String user) {
        String usu = "";
        int sta = 0;
        try {
            con.conectar();
            String qry = "SELECT id_usu FROM usuarios WHERE user='" + user + "'";
            ResultSet rs = con.consulta(qry);
            while (rs.next()) {
                sta = rs.getInt("id_usu");
            }

            if (sta == 0) {
                usu = "vacio";
            } else {
                usu = "uso";
            }
        } catch (SQLException ex) {
            Logger.getLogger(adminUsuDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(adminUsuDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }
        return usu;
    }

    @Override
    public boolean borrar(int id) {
        boolean del = false;
        try {
            con.conectar();
            con.actualizar("UPDATE usuarios SET baja=0 WHERE id_usu=" + id + ";");
            del = true;
        } catch (SQLException ex) {
            Logger.getLogger(adminUsuDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(adminUsuDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }
        return del;
    }

}
