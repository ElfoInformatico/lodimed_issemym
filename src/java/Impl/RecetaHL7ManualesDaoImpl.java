/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Impl;

import Clases.ConectionDB;
import Dao.RecetaHL7ManualesDao;
import Modelos.HL7ManualModel;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Permite agregar receta manual hl7
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class RecetaHL7ManualesDaoImpl implements RecetaHL7ManualesDao {

    ConectionDB con = new ConectionDB();

    @Override
    public void hl7manual() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<HL7ManualModel> byFecOnly(String f1, String f2) {
        List<HL7ManualModel> ln = new ArrayList<>();

        String qry = "SELECT CFECHAS.FECHA,IFNULL(RCTAE.RECETASE,0) AS RECETASELAB,IFNULL(RCTAHL7.HL7,0) AS HL7,IFNULL(RCTAE.RECETASE,0) - IFNULL(RCTAHL7.HL7,0) AS MANUAL,IFNULL(PZASSURT.can_sol,0) AS CANTSOL,IFNULL(PZASSURT.cant_sur,0) AS CANTSURT,IFNULL(PENDTE.PENDIENTE,0) AS PENDIENTE,IFNULL(RCTAE.RECETASE,0) - IFNULL(PENDTE.PENDIENTE,0) AS RCTASURT FROM (SELECT DATE(fecha_hora) AS FECHA FROM receta WHERE transito IN (0, 2) AND baja IN (0, 2) AND DATE(fecha_hora) BETWEEN ? AND ? GROUP BY FECHA) AS CFECHAS LEFT JOIN (SELECT COUNT(fol_rec) AS RECETASE,DATE(fecha_hora) AS FECHA FROM receta WHERE transito IN (0, 2) AND baja IN (0, 2) AND DATE(fecha_hora) BETWEEN ? AND ? GROUP BY FECHA) AS RCTAE ON CFECHAS.FECHA=RCTAE.FECHA LEFT JOIN (SELECT DATE(R.fecha_hora) AS FECHA,SUM(D.can_sol) AS can_sol,SUM(D.cant_sur) AS cant_sur FROM receta R INNER JOIN detreceta D ON R.id_rec=D.id_rec WHERE transito IN (0, 2) AND R.baja IN (0, 2) AND DATE(fecha_hora) BETWEEN ? AND ? GROUP BY FECHA) AS PZASSURT ON CFECHAS.FECHA=PZASSURT.FECHA LEFT JOIN (SELECT DATE(fecha_hora) AS FECHA,COUNT(fol_rec) AS HL7 FROM receta WHERE transito IN (0, 2) AND baja IN (0, 2) AND fol_rec LIKE '%HL7%' AND DATE(fecha_hora) BETWEEN ? AND ? GROUP BY FECHA) AS RCTAHL7 ON CFECHAS.FECHA=RCTAHL7.FECHA LEFT JOIN (SELECT CFCHAS.FECHA,COUNT(PEND.receta) AS PENDIENTE FROM (SELECT DATE(fecha_hora) AS FECHA FROM receta WHERE transito IN (0, 2) AND baja IN (0, 2) AND DATE(fecha_hora) BETWEEN ? AND ? GROUP BY FECHA ) AS CFCHAS LEFT JOIN (SELECT DATE(fecha_hora) AS FECHA,R.fol_rec AS receta,SUM(can_sol) AS can_sol,SUM(cant_sur) AS cant_sur  FROM receta AS R INNER JOIN detreceta AS D ON D.id_rec = R.id_rec  WHERE R.transito IN (0, 2) AND R.baja IN (0, 2)   AND DATE(fecha_hora) BETWEEN ? AND ? GROUP BY R.fol_rec,FECHA HAVING can_sol<>cant_sur) AS PEND ON CFCHAS.FECHA=PEND.FECHA GROUP BY CFCHAS.FECHA) AS PENDTE ON CFECHAS.FECHA=PENDTE.FECHA;";
        try {
            con.conectar();
            PreparedStatement ps = con.getConn().prepareStatement(qry);
            ps.setString(1, f1);
            ps.setString(2, f2);
            ps.setString(3, f1);
            ps.setString(4, f2);
            ps.setString(5, f1);
            ps.setString(6, f2);
            ps.setString(7, f1);
            ps.setString(8, f2);
            ps.setString(9, f1);
            ps.setString(10, f2);
            ps.setString(11, f1);
            ps.setString(12, f2);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                HL7ManualModel n = new HL7ManualModel();
                n.setFec(rs.getString(1));
                n.setRctalaboradas(rs.getInt(2));
                n.setRctahl7(rs.getInt(3));
                n.setRctamanuales(rs.getInt(4));
                n.setRctasurtidas(rs.getInt(8));
                n.setRctapendientes(rs.getInt(7));
                n.setPzassol(rs.getInt(5));
                n.setPzassur(rs.getInt(6));

                ln.add(n);
            }

        } catch (SQLException ex) {
            Logger.getLogger(RecetaHL7ManualesDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(RecetaHL7ManualesDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return ln;
    }

    @Override
    public List<HL7ManualModel> byFecOnlyManual(String f1, String f2) {
        List<HL7ManualModel> ln = new ArrayList<>();

        String qry = "SELECT DATE_FORMAT(fecha_hora,'%d/%m/%Y') AS fecha_hora,fol_rec,M.nom_com,P.nom_com AS nompac,D.can_sol,D.cant_sur FROM receta R INNER JOIN medicos M ON R.cedula=M.cedula  INNER JOIN pacientes P ON R.id_pac=P.id_pac INNER JOIN (SELECT id_rec,SUM(can_sol) AS can_sol,SUM(cant_sur) AS cant_sur FROM detreceta GROUP BY id_rec) AS D  ON R.id_rec=D.id_rec WHERE fol_rec NOT LIKE '%HL7%' AND id_tip IN (1,3) AND DATE(fecha_hora) BETWEEN ? AND ?;";
        try {
            con.conectar();
            PreparedStatement ps = con.getConn().prepareStatement(qry);
            ps.setString(1, f1);
            ps.setString(2, f2);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                HL7ManualModel n = new HL7ManualModel();
                n.setFec(rs.getString(1));
                n.setFolio(rs.getString(2));
                n.setMedico(rs.getString(3));
                n.setPaciente(rs.getString(4));
                n.setPzassol(rs.getInt(5));
                n.setPzassur(rs.getInt(6));

                ln.add(n);
            }

        } catch (SQLException ex) {
            Logger.getLogger(RecetaHL7ManualesDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(RecetaHL7ManualesDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return ln;
    }

}
