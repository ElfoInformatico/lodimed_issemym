package Impl;

import Dao.PacienteDao;
import Modelos.Paciente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementación de pacientes para la busqueda
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class PacienteDaoImpl implements PacienteDao {

    Connection con;
    String formatoFecha;

    public static final String OBTENER_PACIENTE_QUERY = "SELECT p.id_pac AS id, p.nom_com AS nombre, p.ape_pat AS paterno, p.ape_mat AS materno, DATE_FORMAT(p.fec_nac, ?) AS nacimiento, p.sexo AS sexo, p.num_afi AS folio, p.tip_cob AS cobranza, DATE_FORMAT(p.ini_vig, ?) AS inicio, DATE_FORMAT(p.fin_vig, ?) AS fin, p.expediente AS expediente, p.f_status AS `status`, DATEDIFF(p.fin_vig, NOW()) AS vigencia FROM pacientes AS p WHERE %s;";
    public static final String INSERTAR_PACIENTE_QUERY = "INSERT INTO pacientes SET `id_pac` = ?, `ape_pat` = ?, `ape_mat` = ?, `nom_pac` = ?, `nom_com` = ?, `fec_nac` = STR_TO_DATE(?, ?), `sexo` = ?, `num_afi` = ?, `tip_cob` = ?, `ini_vig` = STR_TO_DATE(?, ?), `fin_vig` = STR_TO_DATE(?, ?), `web` = ?, `expediente` = ?, `f_status` = ?, `direccion` = ?, `idpac_hl7` = ?;";

    public PacienteDaoImpl(Connection con) {
        this.con = con;
        this.formatoFecha = "%d/%m/%Y";
    }

    public PacienteDaoImpl(Connection con, String formatoFecha) {
        this.con = con;
        this.formatoFecha = formatoFecha;
    }

    @Override
    public List<Paciente> obtenerAfiliados(String fol) throws SQLException {
        List<Paciente> lp = new ArrayList<>();

        String qry = "SELECT id_pac,nom_com,ini_vig,fin_vig FROM pacientes WHERE num_afi=?";
        PreparedStatement ps = con.prepareStatement(qry);
        ps.setString(1, fol);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Paciente p = new Paciente();
            p.setId(rs.getInt(1));
            p.setNombreCompleto(rs.getString(2));
            lp.add(p);
        }

        rs.close();
        ps.close();

        return lp;
    }

    @Override
    public Paciente obtenerVigencias(String fol) throws SQLException {
        Paciente p = new Paciente();

        String qry = "SELECT DATE_FORMAT(ini_vig, ?) AS inicio, DATE_FORMAT(fin_vig, ?) AS fin FROM pacientes WHERE num_afi=? LIMIT 1";
        PreparedStatement ps = con.prepareStatement(qry);
        ps.setString(1, formatoFecha);
        ps.setString(2, formatoFecha);
        ps.setString(3, fol);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            p.setInicioVigencia(rs.getString("inicio"));
            p.setFinVigencia(rs.getString("fin"));
        }

        rs.close();
        ps.close();

        return p;
    }

    @Override
    public Paciente reconstruirPaciente(String nombre) throws SQLException {
        String qry = String.format(OBTENER_PACIENTE_QUERY, "p.nom_com = ?");
        Paciente resultado;
        try (PreparedStatement ps = con.prepareStatement(qry)) {
            ps.setString(1, formatoFecha);
            ps.setString(2, formatoFecha);
            ps.setString(3, formatoFecha);
            ps.setString(4, nombre);
            ResultSet rs = ps.executeQuery();
            if (!rs.isBeforeFirst()) {
                return null;
            }
            rs.next();
            resultado = reconstruirPaciente(rs);
            rs.close();
        }

        return resultado;
    }

    @Override
    public long adicionarPaciente(Paciente paciente) throws SQLException {
        long id_generado;

        try (PreparedStatement statement = con.prepareStatement(INSERTAR_PACIENTE_QUERY,
                Statement.RETURN_GENERATED_KEYS)) {
            statement.setLong(1, paciente.getId());
            statement.setString(2, paciente.getApellidoPaterno());
            statement.setString(3, paciente.getApellidoMaterno());
            statement.setString(4, paciente.getNombre());
            statement.setString(5, paciente.getNombreCompleto());
            statement.setString(6, paciente.getNacimiento());
            statement.setString(7, paciente.getFormatoFecha());
            statement.setString(8, paciente.getSexo());
            statement.setString(9, paciente.getNumAfi());
            statement.setString(10, paciente.getTipCob());
            statement.setString(11, paciente.getInicioVigencia());
            statement.setString(12, paciente.getFormatoFecha());
            statement.setString(13, paciente.getFinVigencia());
            statement.setString(14, paciente.getFormatoFecha());
            statement.setInt(15, paciente.getWeb());
            statement.setString(16, paciente.getExpediente());
            statement.setString(17, paciente.getSts());
            statement.setString(18, paciente.getDireccion());
            statement.setString(19, paciente.getIdpac_hl7());

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creación de paciente fallido, no se afecto ninguna fila en la insercción.");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();
            generatedKeys.next();
            id_generado = generatedKeys.getLong(1);
            paciente.setId(id_generado);
        }

        return id_generado;
    }

    @Override
    public Paciente reconstruirPaciente(long idPacienteHL7) throws SQLException {
        String qry = String.format(OBTENER_PACIENTE_QUERY, "p.idpac_hl7 = ?");
        PreparedStatement ps = con.prepareStatement(qry);
        ps.setString(1, formatoFecha);
        ps.setString(2, formatoFecha);
        ps.setString(3, formatoFecha);
        ps.setLong(4, idPacienteHL7);

        ResultSet rs = ps.executeQuery();

        if (!rs.isBeforeFirst()) {
            return null;
        }

        rs.next();

        Paciente resultado = reconstruirPaciente(rs);

        rs.close();
        ps.close();

        return resultado;
    }

    private Paciente reconstruirPaciente(ResultSet rs) throws SQLException {

        Paciente resultado = new Paciente();
        resultado.setId(rs.getLong("id"));
        resultado.setNombreCompleto(rs.getString("nombre"));
        resultado.setApellidoPaterno(rs.getString("paterno"));
        resultado.setApellidoMaterno(rs.getString("materno"));
        resultado.setNacimiento(rs.getString("nacimiento"));
        resultado.setSexo(rs.getString("sexo"));
        resultado.setNumAfi(rs.getString("folio"));
        resultado.setTipCob(rs.getString("cobranza"));
        resultado.setInicioVigencia(rs.getString("inicio"));
        resultado.setFinVigencia(rs.getString("fin"));
        resultado.setExpediente(rs.getString("expediente"));
        resultado.setSts(rs.getString("status"));
        resultado.setVigencia(rs.getInt("vigencia"));

        return resultado;
    }
}
