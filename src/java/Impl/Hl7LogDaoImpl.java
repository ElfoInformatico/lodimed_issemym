/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Impl;

import Dao.Hl7LogDao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import mx.gnk.hl7.bean.RecetaHl7Log;

/**
 * Implementación de la receta HL7 log
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class Hl7LogDaoImpl implements Hl7LogDao {

    PreparedStatement ps;
    private final Connection con;

    public Hl7LogDaoImpl(Connection con) {
        this.con = con;
    }

    @Override
    public Long addRecetaHl7Log(RecetaHl7Log recetalog) throws SQLException {
        String SQL_INSERT = "INSERT INTO receta_hl7_log(`tipo_mensaje`, `hl7_mensaje`, `fecha`, `estatus`, `proceso`) values(?,?,?,?,?)";

        Long id_generado = null;

        ps = con.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, recetalog.getTipo_mensaje());
        ps.setString(2, recetalog.getHl7_mensaje());
        ps.setTimestamp(3, new Timestamp(recetalog.getFecha().getTime()));
        ps.setInt(4, recetalog.getEstatus());
        ps.setString(5, recetalog.getProceso());

        int affectedRows = ps.executeUpdate();

        if (affectedRows == 0) {
            throw new SQLException("Creating receta hl7 log, no rows affected.");
        }

        try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
            generatedKeys.next();
            id_generado = generatedKeys.getLong(1);

        }
        ps.close();

        return id_generado;
    }

}
