/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Impl;

import Modelos.Causes;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Obtener y cargar causes CIE-10
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class CausesImpl {

    private final Connection con;

    public static final String OBTENER_CAUSES_QUERY = "SELECT id_cau,des_cau FROM causes WHERE id_cau=?";

    public static final String INSERTAR_CAUSES_QUERY = "INSERT INTO causes SET id_cau = ?, des_cau = ?;";

    public CausesImpl(Connection con) {
        this.con = con;
    }

    public CausesImpl(Connection con, String formatoFecha) {
        this.con = con;
    }

    public Causes reconstruirCauses(String causes) throws SQLException {

        Causes causesHl7;
        try (PreparedStatement ps = con.prepareStatement(String.format(OBTENER_CAUSES_QUERY))) {
            ps.setString(1, causes);
            ResultSet rs = ps.executeQuery();
            if (!rs.isBeforeFirst()) {
                return null;
            }
            rs.next();
            causesHl7 = reconstruirCauses(rs);
            rs.close();
        }

        return causesHl7;

    }

    private Causes reconstruirCauses(ResultSet rs) throws SQLException {
        Causes causesHl = new Causes();

        causesHl.setIdCauses(rs.getString(1));
        causesHl.setDesCauses(rs.getString(2));

        return causesHl;
    }

    public String adicionarCauses(Causes cause) throws SQLException {
        String idGenerado;

        try (PreparedStatement ps = con.prepareStatement(INSERTAR_CAUSES_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, cause.getIdCauses());
            ps.setString(2, cause.getDesCauses());

            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creación de Causes fallido, no se afecto ninguna fila en la insercción.");
            }

            //ResultSet generatedKeys = ps.getGeneratedKeys();
            //generatedKeys.next();
            //idGenerado = generatedKeys.getString(1);
            idGenerado = cause.getIdCauses();
        }

        return idGenerado;
    }

}
