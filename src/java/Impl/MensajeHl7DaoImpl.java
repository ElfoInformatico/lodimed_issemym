package Impl;

import Dao.MensajeHl7Dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import mx.gnk.hl7.bean.MensajeHl7;

/**
 * Verifica receta
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class MensajeHl7DaoImpl implements MensajeHl7Dao {

    PreparedStatement ps;
    private final Connection con;

    public MensajeHl7DaoImpl(Connection con) {
        this.con = con;
    }

    @Override
    public Long addMensajeHl7(MensajeHl7 mensaje) throws SQLException {
        String SQL_INSERT = "INSERT INTO mensaje_hl7(`mensaje`, `status`, `fecha`, `idrec`, `nummsg`) values(?,?,NOW(),?,?)";

        Long id_generado = null;

        ps = con.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, mensaje.getMensaje());
        ps.setInt(2, mensaje.getStatus());
        ps.setInt(3, mensaje.getIdrec() != null ? mensaje.getIdrec() : 0);
        ps.setInt(4, mensaje.getNummsg() != null ? mensaje.getNummsg() : 0);

        int affectedRows = ps.executeUpdate();

        if (affectedRows == 0) {
            throw new SQLException("Creating mensaje hl7, no rows affected.");
        }

        try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
            generatedKeys.next();
            id_generado = generatedKeys.getLong(1);

        }
        ps.close();

        return id_generado;
    }

    @Override
    public boolean checkIfHl7MessageExists(Integer idrec, Integer nummsg) throws SQLException {
        String SQL_SEARCH_MSG = "SELECT 1 FROM mensaje_hl7 WHERE idrec=? AND nummsg=?;";

        ResultSet rs;
        try (PreparedStatement statement = con.prepareStatement(SQL_SEARCH_MSG)) {
            statement.setInt(1, idrec);
            statement.setInt(2, nummsg);
            rs = statement.executeQuery();

            boolean exists = rs.isBeforeFirst();
            rs.close();

            return exists;
        }
    }

}
