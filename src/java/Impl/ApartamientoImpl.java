package Impl;

import Clases.ConectionDB;
import Dao.ApartamientoDao;
import Modelos.Apartado;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Implementación de apartado de productos para el surtido
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class ApartamientoImpl implements ApartamientoDao {

    ConectionDB con;

    public static final String OBTENER_APARTDO_QUERY
            = "SELECT a.detalle_producto AS detalle_producto, a.cantidad AS cantidad, a.fecha_actualizacion AS fecha, a.`status` AS `status`, a.tipo AS tipo FROM apartamiento AS a WHERE a.id = ?;";

    public static final String INSERT_APARTDO_QUERY
            = "INSERT INTO `apartamiento` SET `detalle_producto` = ?, `detalle_receta` = ?, `cantidad` = ?, `fecha_actualizacion` = NOW(), `status` = ?, `tipo` = ?;";

    public static final String UPDATE_APARTDO_QUERY = "UPDATE `apartamiento` SET `status` = ? WHERE (`detalle_receta` = ? %s);";
    
    public static final String UPDATE_IDAPARTDO_QUERY = "UPDATE `apartamiento` SET `status` = ? WHERE id =? ;";

    public ApartamientoImpl(ConectionDB con) {
        this.con = con;
    }

    @Override
    public int agregarApartado(int cantidad, int detalleProducto, int detalleReceta, String tipo) throws SQLException {
        PreparedStatement ps;
        ResultSet rs;
        int id;

        ps = con.getConn().prepareStatement(INSERT_APARTDO_QUERY, PreparedStatement.RETURN_GENERATED_KEYS);
        ps.setInt(1, detalleProducto);
        ps.setInt(2, detalleReceta);
        ps.setInt(3, cantidad);
        ps.setInt(4, Apartado.APARTADO_STATUS);
        ps.setString(5, tipo);

        boolean ok = ps.executeUpdate() > 0;
        if (!ok) {
            ps.close();
            String mensajeError = String.format("NO se agrego el apartado del detalle receta.| detalle_receta: %d | detalle_producto: %d | cantidad: %d | tipo: %s",
                    detalleReceta, detalleProducto, cantidad, tipo);
            throw new SQLException(mensajeError);
        }

        rs = ps.getGeneratedKeys();
        rs.next();
        id = rs.getInt(1);

        rs.close();
        ps.close();

        return id;
    }

    @Override
    public void modificarApartado(int detalleReceta, int status) throws SQLException {
        PreparedStatement ps;

        ps = con.getConn().prepareStatement(String.format(UPDATE_APARTDO_QUERY, ""));
        ps.setInt(1, status);
        ps.setInt(2, detalleReceta);

        boolean ok = ps.executeUpdate() > 0;
        if (!ok) {
            ps.close();
            String mensajeError = String.format("NO se cambio el status del apartado.| detalleReceta: %d | status: %d ",
                    detalleReceta, status);
            throw new SQLException(mensajeError);
        }

        ps.close();
    }

    @Override
    public Apartado reconstruirApartado(int id) throws SQLException {
        PreparedStatement ps;
        ResultSet rs;

        ps = con.getConn().prepareStatement(OBTENER_APARTDO_QUERY);
        ps.setInt(1, id);

        rs = ps.executeQuery();
        rs.next();

        Apartado resultado = new Apartado();
        resultado.setId(id);
        resultado.setDetalleProducto(rs.getInt("detalle_producto"));
        resultado.setCantidad(rs.getInt("cantidad"));
        resultado.setFecha(rs.getString("fecha"));
        resultado.setTipo(rs.getString("tipo"));
        resultado.setStatus(rs.getInt("status"));

        rs.close();
        ps.close();

        return resultado;

    }

    @Override
    public void modificarApartado(int detalleReceta, int detalleProducto, int status) throws SQLException {

        PreparedStatement ps;

        ps = con.getConn().prepareStatement(String.format(UPDATE_APARTDO_QUERY, "AND `id` = ?"));
        ps.setInt(1, status);
        ps.setInt(2, detalleReceta);
        ps.setInt(3, detalleProducto);
        System.out.println(UPDATE_APARTDO_QUERY);
        boolean ok = ps.executeUpdate() > 0;
        if (!ok) {
            ps.close();
            String mensajeError = String.format("NO se cambio el status del apartado.| detalleReceta: %d | status: %d ",
                    detalleReceta, status);
            throw new SQLException(mensajeError);
        }

        ps.close();
    }
    
    @Override
    public void modificarApartadoID(int idApartado, int status) throws SQLException {

        PreparedStatement ps;

        ps = con.getConn().prepareStatement(UPDATE_IDAPARTDO_QUERY);
        ps.setInt(1, status);
        ps.setInt(2, idApartado);
        //System.out.println(UPDATE_IDAPARTDO_QUERY);
        boolean ok = ps.executeUpdate() > 0;
        if (!ok) {
            ps.close();
            String mensajeError = String.format("NO se cambio el status del apartado.| detalleReceta: %d | status: %d ",
                    idApartado, status);
            throw new SQLException(mensajeError);
        }

        ps.close();
    }
    
    

}
