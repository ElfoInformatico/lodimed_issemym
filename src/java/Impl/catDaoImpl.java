/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Impl;

import Clases.ConectionDB;
import Dao.catDao;
import Modelos.catalogo;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Administración de catálogos
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class catDaoImpl implements catDao {

    ConectionDB con = new ConectionDB();

    @Override
    public catalogo lc(int nivel) {
        catalogo c = new catalogo();
        try {
            con.conectar();

            String qry = "SELECT COUNT(p.cla_pro) AS cc FROM productos p, productosnivel pv WHERE p.cla_pro=pv.clave AND pv.nivel=" + nivel + " AND p.f_status='A' AND p.origen=1;";
            ResultSet rs = con.consulta(qry);
            while (rs.next()) {
                c.setCc(rs.getInt("cc"));
            }

            qry = "SELECT COUNT(p.cla_pro) AS cc FROM productos p, productosnivel pv WHERE p.cla_pro=pv.clave AND pv.nivel=" + nivel + " AND p.f_status='A' AND p.origen=2;";
            rs = con.consulta(qry);
            while (rs.next()) {
                c.setVd(rs.getInt("cc"));
            }
            qry = "SELECT COUNT(p.cla_pro) AS cc FROM productos p, productosnivel pv WHERE p.cla_pro=pv.clave AND pv.nivel=" + nivel + " AND p.f_status='A'";
            rs = con.consulta(qry);
            while (rs.next()) {
                c.setTotCl(rs.getInt("cc"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(catDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(catDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }

        return c;
    }

    @Override
    public catalogo lcExist(int nivel) {
        catalogo c = new catalogo();
        try {
            con.conectar();

            String qry = "SELECT COUNT(p.cla_pro) AS cc FROM productos p, productosnivel pv WHERE p.cla_pro=pv.clave AND pv.nivel=" + nivel + " AND p.f_status='A' AND p.origen=1;";
            ResultSet rs = con.consulta(qry);
            while (rs.next()) {
                c.setCc(rs.getInt("cc"));
            }

            qry = "SELECT COUNT(p.cla_pro) AS cc FROM productos p, productosnivel pv WHERE p.cla_pro=pv.clave AND pv.nivel=" + nivel + " AND p.f_status='A' AND p.origen=2;";
            rs = con.consulta(qry);
            while (rs.next()) {
                c.setVd(rs.getInt("cc"));
            }
            qry = "SELECT COUNT(p.cla_pro) AS cc FROM productos p, productosnivel pv WHERE p.cla_pro=pv.clave AND pv.nivel=" + nivel + " AND p.f_status='A'";
            rs = con.consulta(qry);
            while (rs.next()) {
                c.setTotCl(rs.getInt("cc"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(catDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(catDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }

        return c;
    }

    public JSONArray obtenerCauses() {
        JSONArray resultado = new JSONArray();
        JSONObject jo;
        try {
            con.conectar();

            Statement ps = con.getConn().createStatement();
            ResultSet rs = ps.executeQuery("SELECT c.id_cau AS id, CONCAT( c.id_cau, ' - ', REPLACE ( REPLACE (c.des_cau, '\\\\r', ''), '\\\\n', '' )) AS ambos FROM causes AS c WHERE CHARACTER_LENGTH(c.id_cau) = 3");
            while (rs.next()) {
                jo = new JSONObject();
                jo.put("id", rs.getString("id"));
                jo.put("ambos", rs.getString("ambos"));
                resultado.add(jo);
            }

            rs.close();
            ps.close();

        } catch (SQLException ex) {
            Logger.getLogger(catDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(catDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }

        return resultado;
    }

    public JSONArray obtenerDescripcionProductos() {
        JSONArray resultado = new JSONArray();
        JSONObject jo;

        try {
            con.conectar();

            Statement ps = con.getConn().createStatement();
            ResultSet rs = ps.executeQuery("SELECT p.cla_pro AS id, REPLACE ( REPLACE (p.des_pro, '\\r', ''), '\\n', '' ) AS descripcion FROM productos AS p WHERE p.cla_pro <> 'NINGUNA'");
            while (rs.next()) {
                jo = new JSONObject();
                jo.put("id", rs.getString("id"));
                jo.put("descripcion", rs.getString("descripcion"));
                resultado.add(jo);
            }

            rs.close();
            ps.close();

        } catch (SQLException ex) {
            Logger.getLogger(catDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(catDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }

        return resultado;
    }
}
