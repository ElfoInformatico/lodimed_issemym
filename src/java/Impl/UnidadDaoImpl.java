/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Impl;

import Clases.ConectionDB;
import Dao.UnidadDao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.dbutils.BeanProcessor;

/**
 * Consulta de unidad médica
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class UnidadDaoImpl implements UnidadDao {

    ConectionDB con = new ConectionDB();
    PreparedStatement ps;
    ResultSet rs;
    String query;

    @Override
    public String getUnidadByDescripcion(String descUnidad) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getUnidadByIdHl7(String idUnidad) {
        try {
            String qry = "SELECT des_uni FROM unidades WHERE cla_uni=?;";
            con.conectar();
            ps = con.getConn().prepareStatement(qry);
            ps.setString(1, idUnidad);
            rs = ps.executeQuery();

            BeanProcessor bp = new BeanProcessor();
            if (rs.next()) {
                return rs.getString("des_uni");
            }

        } catch (SQLException ex) {
            Logger.getLogger(adminUsuDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(adminUsuDaoImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }
        return null;
    }

}
