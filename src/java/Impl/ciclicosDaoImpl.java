/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Impl;

import Clases.ConectionDB;
import Clases.ConectionDBnube;
import Dao.ciclicosDao;
import Modelos.ciclicos;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Realización de inventario ciclico
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class ciclicosDaoImpl implements ciclicosDao {

    String listaMedControlado = "0107,0132.01,0202,0221,0226,0242,0243,1544,2097,2098,2099,2100,2102,2107,2108,2499,2500,2601,2602,2612,2613,2614,2619,2654,2657,3204,3215,3241,3241.01,3247,3253,3255,3259,3259.01,3302,3305,4026,4054,4057,4060,4470.01,4471.01,4472,4472.01,4481,4481.01,5351,5476,5478,5483,5484";
    public static final String BORRAR_TABLA_INVENTARIOCICLICO = "DELETE FROM inventariociclico";

    public static final String INSERTAR_TABLA_INVENTARIOCICLICO = "INSERT INTO inventariociclico (FId,FFechaInventario,FHora,FDetalleProducto,FFisico,FInventario,FDiferencia,FTipoCiclico) VALUES (0,DATE(NOW()),TIME(NOW()),?,?,0,0,?);";

    public static final String ACUTALIZAR_INVENTARIOSCICLICOS = "UPDATE inventariociclico SET FInventario=?, FDiferencia=? WHERE FDetalleProducto=?;";

    public static final String LEER_TABLA_INVENTARIOSCICLICOS = "SELECT i.cant AS inventario,ic.FFisico - i.cant  AS diferencia  FROM inventariociclico ic, inventario i WHERE i.det_pro=? AND i.det_pro=ic.FDetalleProducto;";

    public static final String LEER_TABLA_INVENTARIOCICLICOS_RESULTADOS = "SELECT dp.cla_pro,p.des_pro,dp.lot_pro,dp.cad_pro,i.FFisico,i.FInventario,i.FDiferencia FROM productos p, detalle_productos dp, inventariociclico i WHERE i.FDetalleProducto=dp.det_pro AND p.cla_pro=dp.cla_pro;";

    public static final String GUARDAR_CICLICO = "INSERT INTO inventariociclicoHistorico (FFechaInventario,FHora,FDetalleProducto,FFisico,FInventario,FDiferencia,FFolioCiclicos,FTipoCiclico) (SELECT FFechaInventario,FHora,FDetalleProducto,FFisico,FInventario,FDiferencia,ic.ciclicos,FTipoCiclico FROM inventariociclico i, indices ic);";

    public static final String ACTUALIZAR_FOLIO_CICLICO = "UPDATE indices SET ciclicos=ciclicos+1";

    public static final String OBTENER_FOLIO_CICLICO = "SELECT ciclicos FROM indices";

    public static final String OBTENER_FOLIO_CICLICOHISTORICO = "SELECT MAX(FFolioCiclicos) AS folio FROM inventariociclicoHistorico;";

    public static final String OBTENER_LISTA_CICLICOS = "SELECT i.cla_uni,c.FFechaInventario,c.FHora, d.cla_pro,p.des_pro,d.lot_pro,d.cad_pro,c.FFisico,c.FInventario,c.FDiferencia,c.FFolioCiclicos,ca.FTipoCiclico FROM  inventariociclicoHistorico c,inventario i,productos p,detalle_productos d,catalogoCiclicos ca WHERE c.FFolioCiclicos=? AND c.FDetalleProducto=d.det_pro AND d.cla_pro=p.cla_pro AND i.det_pro=d.det_pro AND c.FTipoCiclico=ca.FId ORDER BY d.cla_pro;";

    public static final String INSERTAR_CICLICO_NUBE = "INSERT INTO inventarioCiclicoNube (FClaveUnidad,FFechaInventario,FHora,FClave,FDescripcion,FLote,FCaducidad,FFisico,FInventario,FDiferencia,FFolioCiclico,FTipoCiclico) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

    public static final String OBTENER_CICLICOS = "SELECT c.FFolioCiclicos AS folio ,c.FFechaInventario AS fecha ,c.FHora AS hora, SUM(c.FDiferencia) AS diferencia, COUNT(c.FDetalleProducto) AS numClaves , ca.FTipoCiclico AS tipo  FROM inventariociclicoHistorico c, catalogoCiclicos ca WHERE ca.FId=c.FTipoCiclico GROUP BY c.FFolioCiclicos;";

    public static final String OBTENER_CICLICOS_NORMAL = "SELECT c.FFolioCiclicos AS folio ,c.FFechaInventario AS fecha ,c.FHora AS hora, SUM(c.FDiferencia) AS diferencia, COUNT(c.FDetalleProducto) AS numClaves , ca.FTipoCiclico AS tipo  FROM inventariociclicoHistorico c, catalogoCiclicos ca WHERE c.FTipoCiclico=1 AND  ca.FId=c.FTipoCiclico GROUP BY c.FFolioCiclicos;";

    public static final String DATOS_PARA_EXCEL = "SELECT u.des_uni as unidad,DATE(c.FFechaInventario) as fecha,c.FHora hora,d.cla_pro as clave,p.des_pro as descripcion,d.lot_pro as lote, d.cad_pro as caducidad, c.FFisico as fisico, c.FInventario as inventario, c.FDiferencia as diferencia ,c.FFolioCiclicos as foliociclico , ca.FTipoCiclico as tipo FROM  inventariociclicoHistorico c,inventario i,productos p,detalle_productos d,catalogoCiclicos ca, unidades u WHERE c.FFolioCiclicos=? AND c.FDetalleProducto=d.det_pro AND d.cla_pro=p.cla_pro AND i.det_pro=d.det_pro AND c.FTipoCiclico=ca.FId AND i.cla_uni=u.cla_uni ORDER BY d.cla_pro;";

    ConectionDB con = new ConectionDB();
    ConectionDBnube conNube = new ConectionDBnube();
    PreparedStatement ps;
    ResultSet rs;
    PreparedStatement ps1;
    ResultSet rs1;
    String query;

    @Override
    public List<ciclicos> ListaCiclicos(int cantidadClaves) {
        List<ciclicos> lC = new ArrayList<>();
        try {
            con.conectar();
            query = "SELECT cla_pro,des_pro,lot_pro,cad_pro,det_pro,id_inv FROM existencias WHERE cant!=0 AND cos_pro < 500 AND cla_pro NOT IN (" + listaMedControlado + ") ORDER BY RAND() LIMIT ?;";
            ps = con.getConn().prepareStatement(query);
            ps.setInt(1, cantidadClaves);
            rs = ps.executeQuery();
            while (rs.next()) {
                ciclicos c = new ciclicos();
                c.setClave(rs.getString("cla_pro"));
                c.setLote(rs.getString("lot_pro"));
                c.setCaducidad(rs.getString("cad_pro"));
                c.setDetalleProducto(rs.getInt("det_pro"));
                c.setIdInventario(rs.getInt("id_inv"));
                c.setDescripcion(rs.getString("des_pro"));
                lC.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ciclicosDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(ciclicosDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return lC;
    }

    @Override
    public boolean insertLista(String jsonCaptura, int tipoInventario) {
        boolean insertar = false;
        try {
            con.conectar();
            ps = con.getConn().prepareStatement(BORRAR_TABLA_INVENTARIOCICLICO);
            ps.execute();
            JSONArray jsonArr = new JSONArray(jsonCaptura);
            for (int i = 0; i < jsonArr.length(); i++) {
                JSONObject jsonObj = jsonArr.getJSONObject(i);
                int id = jsonObj.getInt("id");
                int cantidad = jsonObj.getInt("cantidad");
                int inventario = 0;
                int diferencia = 0;
                ps = con.getConn().prepareStatement(INSERTAR_TABLA_INVENTARIOCICLICO);
                ps.setInt(1, id);
                ps.setInt(2, cantidad);
                ps.setInt(3, tipoInventario);
                ps.execute();

                ps.clearParameters();

                ps = con.getConn().prepareStatement(LEER_TABLA_INVENTARIOSCICLICOS);
                ps.setInt(1, id);
                rs = ps.executeQuery();
                while (rs.next()) {
                    inventario = rs.getInt("inventario");
                    diferencia = rs.getInt("diferencia");
                }

                ps.clearParameters();

                ps = con.getConn().prepareStatement(ACUTALIZAR_INVENTARIOSCICLICOS);
                ps.setInt(1, inventario);
                ps.setInt(2, diferencia);
                ps.setInt(3, id);
                ps.execute();
            }
            insertar = true;
            guardar();

        } catch (SQLException | JSONException ex) {
            Logger.getLogger(ciclicosDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(ciclicosDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return insertar;
    }

    @Override
    public List<ciclicos> ListaResultado() {
        List<ciclicos> lC = new ArrayList<>();
        int folioCiclico = 0;
        try {
            con.conectar();

            ps = con.getConn().prepareStatement(OBTENER_FOLIO_CICLICOHISTORICO);
            rs = ps.executeQuery();
            while (rs.next()) {
                folioCiclico = rs.getInt("folio");
            }
            ps = con.getConn().prepareStatement(LEER_TABLA_INVENTARIOCICLICOS_RESULTADOS);
            rs = ps.executeQuery();
            while (rs.next()) {
                ciclicos c = new ciclicos();
                c.setClave(rs.getString(1));
                c.setDescripcion(rs.getString(2));
                c.setLote(rs.getString(3));
                c.setCaducidad(rs.getString(4));
                c.setFisico(rs.getInt(5));
                c.setInventario(rs.getInt(6));
                c.setDiferencia(rs.getInt(7));
                c.setFolio(folioCiclico);
                lC.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ciclicosDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(ciclicosDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return lC;
    }

    @Override
    public boolean guardar() {
        boolean guardar = false;
        int ciclico = 0;
        boolean conectadoNube = false;
        try {

            con.conectar();
            conNube.conectar();
            ps = con.getConn().prepareStatement(OBTENER_FOLIO_CICLICO);
            rs = ps.executeQuery();
            while (rs.next()) {
                ciclico = rs.getInt("ciclicos") + 1;
            }
            ps.clearParameters();

            ps = con.getConn().prepareStatement(ACTUALIZAR_FOLIO_CICLICO);
            ps.execute();
            ps.clearParameters();

            ps = con.getConn().prepareStatement(GUARDAR_CICLICO);
            ps.execute();
            ps.clearParameters();

            conectadoNube = conNube.getConn() != null;

            if (conectadoNube) {
                ps = con.getConn().prepareStatement(OBTENER_LISTA_CICLICOS);
                ps.setInt(1, ciclico);
                rs = ps.executeQuery();
                while (rs.next()) {
                    ps1 = conNube.getConn().prepareStatement(INSERTAR_CICLICO_NUBE);
                    ps1.setString(1, rs.getString("cla_uni"));
                    ps1.setString(2, rs.getString("FFechaInventario"));
                    ps1.setString(3, rs.getString("FHora"));
                    ps1.setString(4, rs.getString("cla_pro"));
                    ps1.setString(5, rs.getString("des_pro"));
                    ps1.setString(6, rs.getString("lot_pro"));
                    ps1.setString(7, rs.getString("cad_pro"));
                    ps1.setInt(8, rs.getInt("FFisico"));
                    ps1.setInt(9, rs.getInt("FInventario"));
                    ps1.setInt(10, rs.getInt("FDiferencia"));
                    ps1.setInt(11, rs.getInt("FFolioCiclicos"));
                    ps1.setString(12, rs.getString("FTipoCiclico"));
                    ps1.execute();
                }
            }

            guardar = true;
        } catch (SQLException ex) {
            Logger.getLogger(ciclicosDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
                if (conectadoNube) {
                    conNube.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(ciclicosDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return guardar;
    }

    @Override
    public List<ciclicos> ListaCiclicosCosto(int cantidadClaves) {
        List<ciclicos> lC = new ArrayList<>();
        try {
            con.conectar();
            query = "SELECT cla_pro,des_pro,lot_pro,cad_pro,det_pro,id_inv FROM existencias WHERE cant!=0 AND cos_pro >500 AND cla_pro NOT IN (" + listaMedControlado + ") ORDER BY RAND() LIMIT ?;";
            ps = con.getConn().prepareStatement(query);
            ps.setInt(1, cantidadClaves);
            rs = ps.executeQuery();
            while (rs.next()) {
                ciclicos c = new ciclicos();
                c.setClave(rs.getString("cla_pro"));
                c.setLote(rs.getString("lot_pro"));
                c.setCaducidad(rs.getString("cad_pro"));
                c.setDetalleProducto(rs.getInt("det_pro"));
                c.setIdInventario(rs.getInt("id_inv"));
                c.setDescripcion(rs.getString("des_pro"));
                lC.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ciclicosDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(ciclicosDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return lC;
    }

    @Override
    public List<ciclicos> ReporteCiclicos(int tipo) {

        List<ciclicos> lC = new ArrayList<>();

        try {
            con.conectar();
            if (tipo == 3) {
                ps = con.getConn().prepareStatement(OBTENER_CICLICOS);

            } else {
                ps = con.getConn().prepareStatement(OBTENER_CICLICOS_NORMAL);
            }

            rs = ps.executeQuery();
            while (rs.next()) {
                ciclicos c = new ciclicos();
                c.setFolio(rs.getInt("folio"));
                c.setFecha(rs.getString("fecha"));
                c.setHora(rs.getString("hora"));
                c.setDiferencia(rs.getInt("diferencia"));
                c.setNumeroClaves(rs.getInt("numClaves"));
                c.setTipoCiclico(rs.getString("tipo"));
                lC.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ciclicosDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(ciclicosDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return lC;
    }

    @Override
    public List<ciclicos> reporteExcel(int folio) {
        List<ciclicos> lC = new ArrayList<>();
        try {
            con.conectar();
            ps = con.getConn().prepareStatement(DATOS_PARA_EXCEL);
            ps.setInt(1, folio);
            rs = ps.executeQuery();
            while (rs.next()) {
                ciclicos c = new ciclicos();
                c.setUnidad(rs.getString("unidad"));
                c.setFecha(rs.getString("fecha"));
                c.setHora(rs.getString("hora"));
                c.setClave(rs.getString("clave"));
                c.setDescripcion(rs.getString("descripcion"));
                c.setLote(rs.getString("lote"));
                c.setCaducidad(rs.getString("caducidad"));
                c.setFisico(rs.getInt("fisico"));
                c.setInventario(rs.getInt("inventario"));
                c.setDiferencia(rs.getInt("diferencia"));
                c.setFolio(rs.getInt("foliociclico"));
                c.setTipoCiclico(rs.getString("tipo"));
                lC.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ciclicosDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(ciclicosDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return lC;
    }

    @Override
    public List<ciclicos> ListaCiclicosControlado(int cantidadClaves) {
        List<ciclicos> lC = new ArrayList<>();
        try {
            con.conectar();
            query = "SELECT cla_pro,des_pro,lot_pro,cad_pro,det_pro,id_inv FROM existencias WHERE cant!=0  AND cla_pro IN (" + listaMedControlado + ") ORDER BY RAND() LIMIT ?;";
            ps = con.getConn().prepareStatement(query);
            ps.setInt(1, cantidadClaves);
            rs = ps.executeQuery();
            while (rs.next()) {
                ciclicos c = new ciclicos();
                c.setClave(rs.getString("cla_pro"));
                c.setLote(rs.getString("lot_pro"));
                c.setCaducidad(rs.getString("cad_pro"));
                c.setDetalleProducto(rs.getInt("det_pro"));
                c.setIdInventario(rs.getInt("id_inv"));
                c.setDescripcion(rs.getString("des_pro"));
                lC.add(c);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ciclicosDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(ciclicosDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return lC;
    }

}
