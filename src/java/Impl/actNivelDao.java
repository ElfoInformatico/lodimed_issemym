/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Impl;

import Clases.ConectionDB;
import Dao.nivelDao;
import Modelos.nivelModel;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Reporte de entregas
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class actNivelDao implements nivelDao
{
    ConectionDB con = new ConectionDB();
    @Override
    public List<nivelModel> byFecOnly(String f1, String f2) {
         List<nivelModel> ln = new ArrayList<>();
        
        String qry="SELECT completo.fecha AS fecha, completo.pz_sol AS pz_sol, completo.pz_nosur AS pz_nosur, completo.pz_vend AS pz_vend, completo.claves_sol AS claves_sol, IFNULL(incompleto.claves_nosur, 0) AS claves_nosur, completo.rec_sol AS rec_sol, CONCAT( CAST( completo.rec_sol - IFNULL(incompleto.rec_sur, 0) AS CHAR ), \"/\", CAST( IFNULL(incompleto.rec_sur, 0) AS CHAR )) AS rec_par, TRUNCATE (( completo.pz_vend / completo.pz_sol ) * 100, 2 ) AS porcentaje FROM ( SELECT DATE(r.fecha_hora) AS fecha, SUM(r.can_sol) AS pz_sol, SUM(r.cant_sur) AS pz_vend, SUM(r.can_sol) - SUM(r.cant_sur) AS pz_nosur, COUNT(DISTINCT r.cla_pro) AS claves_sol, COUNT(DISTINCT r.id_rec) AS rec_sol FROM recetas AS r WHERE r.fecha_hora BETWEEN CONCAT(?, \" 00:00:00\") AND CONCAT(?, \" 23:59:59\") AND r.baja <> 1 GROUP BY fecha ) AS completo LEFT JOIN ( SELECT DATE(r.fecha_hora) AS fecha, COUNT(DISTINCT r.cla_pro) AS claves_nosur, COUNT(DISTINCT r.id_rec) AS rec_sur FROM recetas AS r WHERE r.fecha_hora BETWEEN CONCAT(?, \" 00:00:00\") AND CONCAT(?, \" 23:59:59\") AND r.baja <> 1 AND r.can_sol > r.cant_sur GROUP BY fecha ) AS incompleto ON incompleto.fecha = completo.fecha;";
        try 
            
            {
                con.conectar();
                PreparedStatement ps = con.getConn().prepareStatement(qry);
                ps.setString(1, f1);
                ps.setString(2, f2);
                ps.setString(3, f1);
                ps.setString(4, f2);
                
                ResultSet rs = ps.executeQuery();
                while(rs.next())
                    {
                        nivelModel n = new nivelModel();
                        n.setFec(rs.getString("fecha"));
                        n.setPzSol(rs.getInt("pz_sol"));
                        n.setPzNoSur(rs.getInt("pz_nosur"));
                        n.setClaSol(rs.getInt("claves_sol"));
                        n.setClaNoSur(rs.getInt("claves_nosur"));
                        n.setRecSol(rs.getInt("rec_sol"));
                        n.setRecPar(rs.getString("rec_par"));
                        n.setPorc(rs.getDouble("porcentaje"));
                        n.setPzVen(rs.getInt("pz_vend"));
                        n.setImpor(0); //Por cambio de origen no importa el precio.
                        n.setTotales(0);
                        /*n.setImpor(rs.getDouble("importe"));
                        n.setTotales(rs.getInt("totales"));*/
                        ln.add(n);
                    }
                               
            }
        catch (SQLException ex) {
            Logger.getLogger(solSurDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(solSurDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return ln;
    }
    
    @Deprecated
    @Override
    public void actNivel() {
        throw new UnsupportedOperationException("Not supported yet."); //Se hace innecesario
    }
    
}
