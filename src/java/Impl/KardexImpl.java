/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Impl;

import Clases.ConectionDB;
import Dao.KardexDao;
import Modelos.Kardex;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Implementación Kardex y insertar movimiento
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class KardexImpl implements KardexDao {

    ConectionDB con;
    PreparedStatement ps;
    ResultSet rs;
    //INSERT INTO kardex set id_rec=?,det_pro=?,cant=?,tipo_mov=?,fol_aba=?,fecha=NOW(),obser=?,id_usu=?,web=0;
    String QUERY_ALTA_MOVIMIENTO = "INSERT INTO kardex SET id_rec=?, det_pro=?, cant=?, tipo_mov=?, fol_aba=?, fecha=NOW(), obser=?, id_usu=?, web=0;";

    public ConectionDB getCon() {
        return con;
    }

    public void setCon(ConectionDB con) {
        this.con = con;
    }

    @Override
    public boolean agregarMovimiento(Kardex nuevoMovimiento) throws SQLException {
        boolean ok;
        ps = con.getConn().prepareStatement(QUERY_ALTA_MOVIMIENTO);

        ps.setInt(1, nuevoMovimiento.getIdReceta());
        ps.setInt(2, nuevoMovimiento.getDetalleProducto());
        ps.setInt(3, nuevoMovimiento.getCantidad());
        ps.setString(4, nuevoMovimiento.getTipoMovimiento());
        ps.setString(5, nuevoMovimiento.getNombreAbasto());
        ps.setString(6, nuevoMovimiento.getObservacion());
        ps.setInt(7, nuevoMovimiento.getIdUsuario());

        //verifica que efectivamente realice una modificación sobre la BD. 
        ok = ps.executeUpdate() > 0;
        ps.close();

        return ok;

    }

    @Override
    public List<Kardex> getKardexByIdRecDetPro(Long idReceta, Long detPro) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
