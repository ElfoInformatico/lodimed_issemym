/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Impl;

import Clases.ConectionDB;
import Dao.UsuarioDao;
import Modelos.Usuario;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Valida usuario e ingreso al sistema
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class UsuarioImpl implements UsuarioDao {

    private final ConectionDB con = new ConectionDB();
    private PreparedStatement ps;
    private ResultSet rs;
    private String query;

    @Override
    public Usuario validar(String usuario, String Password) {
        Usuario user = null;
        try {
            query = "SELECT u.id_usu AS id_usu, nombre, rol, u.cla_uni AS cla_uni,uni.des_uni AS des_uni, u.cedula AS cedula, uni.nivel AS nivel,ape_pat, ape_mat FROM usuarios u, unidades uni WHERE u.user = ? AND u.pass = MD5(?) AND u.baja != '0'  AND uni.cla_uni = u.cla_uni;";
            con.conectar();
            ps = con.getConn().prepareStatement(query);
            ps.setString(1, usuario);
            ps.setString(2, Password);
            rs = ps.executeQuery();
            if (rs.next()) {
                user = new Usuario();
                user.setId(rs.getInt("id_usu"));
                user.setNom(rs.getString("nombre"));
                user.setRol(rs.getInt("rol"));
                user.setUni(rs.getString("cla_uni"));
                user.setDesUni(rs.getString("des_uni"));
                user.setCedula(rs.getString("cedula"));
                user.setNivel(rs.getString("nivel"));
                user.setaM(rs.getString("ape_mat"));
                user.setaP(rs.getString("ape_pat"));
                switch (user.getRol()) {
                    case Usuario.ADMON_ROL:
                        user.setTipo("ADMON");
                        break;
                    case Usuario.MEDICO_ROL:
                        user.setTipo("MEDICO");
                        break;
                    case Usuario.FARMACIA_ROL:
                        user.setTipo("FARMACIA");
                        break;
                    case Usuario.SUPERVISOR_ROL:
                        user.setTipo("SUPERVISOR");
                        break;
                    case Usuario.HL7_ROL:
                        user.setTipo("HL7");
                        break;
                    case Usuario.RURAL_ROL:
                        user.setTipo("RURAL");
                        break;
                }

                if (user.getTipo().equals("FARMACIA") || user.getTipo().equals("SUPERVISOR")) {
                    query = "SELECT tipRec FROM indices;";
                    ps = con.getConn().prepareStatement(query);
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        user.setFarm(rs.getString("tipRec"));
                    }
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(UsuarioImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return user;
    }

    @Override
    public void insertEntrada(String usuario, int status) {
        try {
            query = "INSERT INTO registro_entradas (user,status) VALUES (?,?);";
            con.conectar();
            ps = con.getConn().prepareStatement(query);
            ps.setString(1, usuario);
            ps.setInt(2, status);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(UsuarioImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public int tipSistema() {
        int tipoCaptura = 0;
        try {
            query = "SELECT tipoCaptura FROM indices;";
            con.conectar();
            ps = con.getConn().prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                tipoCaptura = rs.getInt("tipoCaptura");
            }

        } catch (SQLException ex) {
            Logger.getLogger(UsuarioImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(UsuarioImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return tipoCaptura;

    }

}
