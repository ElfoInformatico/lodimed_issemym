/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Impl;

import Clases.ConectionDB;
import Dao.kardexViewDao;
import Modelos.kardexVista;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Consulta de movientos al inventario
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class kardexViewClaveDaoImpl implements kardexViewDao {

    //Se inicializa la clase de la conexión a la base de datos
    ConectionDB con = new ConectionDB();
    //Se inicializa un preparedStatement que será usado en las distintas consultas
    PreparedStatement ps;
    //Se declara un ResultSet para obtener los resultados de las consultas.
    ResultSet rs;
    //Consultas a la base de datos
    //Consulta para obtener todos los movimientos de una clave
    public static final String LISTAR_MOVIMIENTOS_POR_CLAVE = "SELECT dp.cla_pro AS clave, dp.lot_pro AS lote, dp.cad_pro AS caducidad, dp.id_ori AS origen, IF ( LOCATE(\"SALIDA\", k.tipo_mov) > 0, k.cant * - 1, k.cant ) AS cantidad, k.tipo_mov AS movimiento, k.fol_aba AS fol_aba, r.fol_rec AS folio, IF ( LOCATE('SALIDA RECETA', k.tipo_mov) > 0, pa.nom_com, '-' ) AS paciente, IF ( LOCATE('SALIDA RECETA', k.tipo_mov) > 0, m.nom_com, '-' ) AS medico, k.fecha AS fecha, k.obser AS obser, u.nombre_completo AS usuario FROM kardex AS k INNER JOIN detalle_productos AS dp ON k.det_pro = dp.det_pro LEFT JOIN receta AS r ON r.id_rec = k.id_rec INNER JOIN pacientes AS pa ON r.id_pac = pa.id_pac INNER JOIN medicos AS m ON m.cedula = r.cedula INNER JOIN usuarios AS u ON k.id_usu = u.id_usu WHERE dp.cla_pro = ? AND k.cant > 0 ORDER BY fecha ASC;";
    //Consulta que trae datos de la clave a consultar
    public static final String DATOS_CLAVE = "SELECT e.cla_pro AS clave, e.des_pro AS descripcion, Sum(e.cant) AS existencia FROM existencias AS e WHERE e.cla_pro = ? GROUP BY e.cla_pro;";

    @Override
    public List<kardexVista> listaKardexByClave(String clave) {
        List<kardexVista> Lv = new ArrayList<>();
        try {
            int sumatoria = 0;
            con.conectar();
            ps = con.getConn().prepareStatement(LISTAR_MOVIMIENTOS_POR_CLAVE);
            ps.setString(1, clave);
            rs = ps.executeQuery();
            while (rs.next()) {
                kardexVista k = new kardexVista();
                k.setClave(rs.getString("clave"));
                k.setLote(rs.getString("lote"));
                k.setCaducidad(rs.getString("caducidad"));
                k.setOrigen(rs.getInt("origen"));
                k.setCantidad(rs.getInt("cantidad"));
                k.setTipoMovimiento(rs.getString("movimiento"));
                k.setAbasto(rs.getString("fol_aba"));
                k.setFolioReceta(rs.getString("folio"));
                k.setPaciente(rs.getString("paciente"));
                k.setMedico(rs.getString("medico"));
                k.setFecha(rs.getString("fecha"));
                k.setObservaciones(rs.getString("obser"));
                k.setUsuario(rs.getString("usuario"));
                sumatoria += k.getCantidad();
                k.setSumatoria(sumatoria);
                Lv.add(k);

            }

        } catch (SQLException ex) {
            Logger.getLogger(kardexViewClaveDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(kardexViewClaveDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return Lv;

    }

    @Override
    public kardexVista datosKardex(String clave) {
        kardexVista k = new kardexVista();
        try {
            con.conectar();
            ps = con.getConn().prepareStatement(DATOS_CLAVE);
            ps.setString(1, clave);
            rs = ps.executeQuery();
            while (rs.next()) {
                k.setClave(rs.getString("clave"));
                k.setDescripcion(rs.getString("descripcion"));
                k.setExistencias(rs.getInt("existencia"));
            }

        } catch (SQLException ex) {
            Logger.getLogger(kardexViewClaveDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(kardexViewClaveDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return k;

    }

}
