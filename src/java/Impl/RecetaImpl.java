package Impl;

import Clases.ConectionDB;
import Dao.RecetaDao;
import Modelos.Apartado;
import Modelos.DetalleCapturado;
import Modelos.DetalleProducto;
import Modelos.DetalleReceta;
import Modelos.Inventario;
import Modelos.Kardex;
import Modelos.LoteDisponible;
import Modelos.Medico;
import Modelos.Receta;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Permite crear receta y surtir
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class RecetaImpl implements RecetaDao {

    public RecetaImpl(ConectionDB con, int tipoReceta, int idUsuario) {
        this.con = con;
        this.idUsuario = idUsuario;
        this.formatoReceta = "CONCAT(?, ' ', CURTIME())";
        this.formatoRecetaSalida = "%d-%m-%Y";
        if (Receta.COLECTIVA_TIPO == tipoReceta) {
            this.tipoRecetaString = Kardex.COLECTIVA_OBSERVACION;
        } else {
            this.tipoRecetaString = Kardex.MANUAL_OBSERVACION;
        }
    }

    public RecetaImpl(ConectionDB con, int tipoReceta, int idUsuario, String formatoReceta) {
        this(con, tipoReceta, idUsuario);
        this.formatoReceta = formatoReceta;
    }

    public RecetaImpl(String formatoSalida, ConectionDB con, int tipoReceta, int idUsuario) {
        this(con, tipoReceta, idUsuario);
        this.formatoRecetaSalida = formatoSalida;
    }

    ConectionDB con;
    private final String tipoRecetaString;
    private String formatoReceta;
    private String formatoRecetaSalida;
    int idUsuario;

    public static final int SURTIDO_MAYOR = Integer.MIN_VALUE;
    public static final int CODIGO_BARRAS_CAPTURA = 1;
    public static final int FARMACIA_CAPTURA = 2;
    public static final int MEDICO_CAPTURA = 3;
    public static final int NO_ENCONTRADO = -1;
    public static final int FOLIO_EXISTENTE = NO_ENCONTRADO;
        //TODO: Agregar parametro al indice del sistema.
    public static final int TOTAL_CLAVES_POR_RECETA = 6;
    public static final String LIMITE = "ERROR: Limite de claves alcanzado. Máximo: 6";

    public static final String OBTENER_FOLIO_MEDICO_QUERY = "SELECT i.paramRec AS prefijo, m.folAct AS actual, m.finRec AS maximo, CONCAT(i.paramRec, m.folAct) AS folio FROM medicos AS m, indices AS i WHERE m.cedula = ? FOR UPDATE";

    public static final String OBTENER_NUEVO_FOLIO_MEDICO_QUERY = "SELECT r.cedula AS medico, IF ( LOCATE(i.paramRec, r.fol_rec) = 0, - 2, SUBSTR( r.fol_rec FROM ( LOCATE(i.paramRec, r.fol_rec) + 1 ))) AS folio FROM receta AS r, indices AS i WHERE r.id_rec = ? FOR UPDATE;";

    public static final String ACTUALIZAR_FOLIO_MEDICO_QUERY = "UPDATE medicos SET folAct = ? WHERE cedula = ?;";

    public static final String OBTENER_MEDICO_QUERY = "SELECT m.folAct AS actual, m.finRec AS maximo, m.nom_com AS nombres, m.nom_med AS nombre, m.ape_pat AS paterno, m.ape_mat AS materno, m.cedula AS id, m.web, m.rfc, m.clauni AS unidad, m.cedulapro AS cedula, m.f_status, m.fecnac AS fecha_nacimiento, m.iniRec AS inicio, m.tipoConsulta FROM usuarios AS u INNER JOIN medicos AS m ON u.cedula = m.cedula, indices AS i WHERE u.id_usu = ?";

    public static final String LISTAR_DETALLE_RECETA_QUERY = "SELECT GROUP_CONCAT( DISTINCT dr.fol_det SEPARATOR ',' ) AS id, IFNULL(i.id_inv ,- 1) AS id_inventario, dr.det_pro AS detalle_producto, IFNULL((i.cant + SUM(dr.cant_sur)), 0 ) AS nueva_cantidad, SUM(dr.cant_sur) AS cantidad_reintegro, IFNULL(ap.cantidad ,- 1) AS tiene_apartado FROM detreceta AS dr INNER JOIN detalle_productos AS dp ON dr.det_pro = dp.det_pro LEFT JOIN inventario AS i ON i.det_pro = dp.det_pro LEFT JOIN apartamiento AS ap ON ap.detalle_receta = dr.fol_det AND ap.detalle_producto = dp.det_pro WHERE dr.id_rec = ? AND dr.baja = ? %s GROUP BY dr.det_pro FOR UPDATE;";

    public static final String CANCELACION_DETALLE_QUERY = "UPDATE detreceta SET can_sol = 0, cant_sur = 0, baja = ? WHERE fol_det IN (%s)";

    public static final String ACTUALIZAR_FOLIO_RECETA_QUERY = "UPDATE `receta` SET `fol_rec` = CONCAT(`fol_rec`, '-C-', NOW()), baja = ?, transito = ? WHERE `id_rec` = ?";

    public static final String OBTENER_RECETA_QUERY = "SELECT r.id_rec AS id, r.fol_rec AS folio, r.id_pac AS paciente, r.cedula AS medico, r.id_tip AS tipo_receta, r.id_usu AS usuario, r.id_ser AS servicio, DATE_FORMAT(r.fecha_hora, '%s') AS fecha, r.transito AS transito, r.baja AS baja, r.tip_cons AS tipo_consulta, IFNULL(r.hl7_obser,'-') AS observacionHL7 FROM receta AS r WHERE %s;";

    public static final String OBTENER_NUMEROCLAVES_QUERY = "SELECT COUNT(DISTINCT dp.cla_pro) AS total FROM detalle_productos AS dp INNER JOIN detreceta AS dr ON dr.det_pro = dp.det_pro WHERE dr.id_rec = ? AND dp.cla_pro <> ? AND baja!=1";

    public static final String OBTENER_CANTIDADES_CAPTURADAS_QUERY = "SELECT det_pro,cla_pro,can_sol,cant_sur FROM recetas WHERE id_rec=?;";

    public static final String INSERT_DETALLEPRODUCTO_QUERY = "INSERT INTO `detalle_productos` SET `cla_pro` = ?, `lot_pro` = ?, `cad_pro` = ?, `cla_fin` = '1', `id_ori` = '0', `web` = '0';";

    public static final String INSERT_DETALLERECETA_QUERY = "INSERT INTO `detreceta` SET `det_pro` = ?, `can_sol` = ?, `cant_sur` = ?, `fec_sur` = CURDATE(), `status` = ?, `id_rec` = ?, `hor_sur` = CURTIME(), `id_cau` = ?, `indicaciones` = ?, `baja` = ?, `web` = '0', `id_cau2` = ?;";

    public static final String OBTENER_DETALLEPRODUCTO_QUERY = "SELECT dp.det_pro AS id FROM detalle_productos AS dp WHERE dp.cla_pro = ? AND dp.lot_pro = ?;";

    public static final String OBTENER_CLAVES_PENDIENTES_QUERY = "SELECT DISTINCT dr.fol_det AS id, dp.cla_pro AS clave, (dr.can_sol - dr.cant_sur) AS faltante, dr.fec_sur AS fecha, dr.indicaciones AS indicaciones, dr.id_cau AS cause1, dr.id_cau2 AS cause2 FROM detalle_productos AS dp INNER JOIN detreceta AS dr ON dr.det_pro = dp.det_pro WHERE dr.id_rec = ? AND dr.can_sol <> dr.cant_sur;";

    public static final String ACTUALIZAR_TRANSITO_QUERY = "UPDATE `receta` SET `transito` = ? WHERE (`id_rec` = ?);";

    public static final String ELIMINA_PENDIENTE_QUERY = "DELETE FROM `detreceta` WHERE (`fol_det` = ?);";

    public static final String INSERT_EMCABEZADO_QUERY = "INSERT INTO `receta` SET `fol_rec` = ?, `id_pac` = ?, `cedula` = ?, `id_tip` = ?, `id_usu` = ?, `enc_ser` = ?, `carnet` = ?, `id_ser` = ?, `fecha_hora` = %s, `transito` = ?, `baja` = ?, `notificahl7` = ?, `obser` = ?, `tip_cons` = ?, `hl7_obser` = ?;";

    public static final String ACTUALIZAR_SOLICITADO_QUERY = "UPDATE `detreceta` SET `can_sol` = ?, `fec_sur` = CURDATE(), `hor_sur` = CURTIME() WHERE `fol_det` = ?;";

    public static final String OBTENER_ANTERIOR_SOLICITADO_QUERY = "SELECT dr.can_sol AS anterior, dr.fol_det AS id FROM detreceta AS dr WHERE dr.id_rec = ? AND dr.det_pro = ? AND dr.cant_sur = 0 AND baja = ? AND dr.`status`IN (?,?) FOR UPDATE;";

    public static final String OBTENER_LOTES_DISPONIBLES_QUERY = "SELECT DISTINCT i.id_inv AS inventario, i.det_pro AS detalle, p.clave AS clave, p.lote AS lote, p.caducidad AS caducidad, IF ( p.solicitado = 0, - 1, p.solicitado ) AS solicitado FROM inventario AS i INNER JOIN ( SELECT detp2.det_pro AS id, detp2.cla_pro AS clave, detp2.lot_pro AS lote, detp2.cad_pro AS caducidad, SUM( IF ( detp2.det_pro = detp.det_pro, detr.can_sol, 0 )) AS solicitado FROM detreceta AS detr INNER JOIN detalle_productos AS detp ON detr.det_pro = detp.det_pro INNER JOIN detalle_productos AS detp2 ON detp2.cla_pro = detp.cla_pro INNER JOIN tb_codigob AS cb ON cb.F_Clave = detp2.cla_pro AND cb.F_Lote = detp2.lot_pro AND cb.F_Cadu = detp2.cad_pro WHERE detr.id_rec = ? AND detr.cant_sur = 0 AND cb.F_Cb = ? GROUP BY id ) AS p ON p.id = i.det_pro WHERE i.cant > 0 FOR UPDATE;";

    public static final String OBTENER_CAUSES_INDICACIONES_ANTERIORES_QUERY = "SELECT IF(dr.cant_sur = 0,1, -1) AS cambiar, dp.lot_pro AS viejo_lote, dp2.lot_pro AS nuevo_lote, dr.id_cau AS cause, dr.id_cau2 AS cause2, dr.fol_det AS id, dr.indicaciones AS indicaciones, IFNULL(dr.can_sol,0) AS solicitado FROM detreceta AS dr INNER JOIN detalle_productos AS dp ON dr.det_pro = dp.det_pro INNER JOIN detalle_productos AS dp2 ON dp.cla_pro = dp2.cla_pro WHERE dr.id_rec = ? AND dp2.det_pro = ? AND dp.det_pro <> dp2.det_pro FOR UPDATE;";

    public static final String OBTENER_NUEVA_EXISTENCIAS_DETALLE_QUERY = "SELECT i.id_inv AS id, (i.cant - ?) AS nuevo_inventario FROM inventario AS i WHERE i.det_pro = ? FOR UPDATE;";

    public static final String ACTUALIZAR_EMCABEZADO_RECETA_QUERY = "UPDATE `receta` SET `transito` = ? WHERE (`id_rec` = ?);";

    public static final String ACTUALIZAR_INDICACIONES_DETALLE_QUERY = "UPDATE `detreceta` SET `indicaciones` = CONCAT( `indicaciones`, ' ', ? %s, `status` = ? WHERE (`fol_det` = ?);";

    public static final String OBTENER_DETALLE_POR_DETALLE_PRODUCTO_QUERY = "SELECT dr.fol_det AS id, dr.det_pro AS detalle_producto, dr.can_sol AS solicitado, dr.cant_sur AS surtido, dr.fec_sur AS fecha, dr.`status` AS `status`, dr.id_rec AS receta, dr.hor_sur AS hora, dr.id_cau AS cause, dr.indicaciones AS indicaciones, dr.id_cau2 AS cause2 FROM detreceta AS dr WHERE dr.id_rec = ? AND dr.det_pro = ? AND dr.baja = 0;";

    public static final String OBTENER_PENDIENTE_CAMBIO_LOTE_QUERY = "SELECT sol.id AS ids, det.det_pro AS producto, /*SUM( IF ( det.`status` = 1, det.can_sol, 0 )) + sol.solicitado ) AS solicitado, Sum(det.cant_sur) AS surtido,*/ (( SUM( IF ( det.`status` = ?, det.can_sol, 0 )) + sol.solicitado ) - Sum(det.cant_sur)) AS diferencia, det.indicaciones AS indicaciones, det.id_cau AS cause, det.id_cau2 AS cause2, dp.cla_pro AS clave FROM detreceta AS det INNER JOIN detalle_productos AS dp ON det.det_pro = dp.det_pro RIGHT JOIN ( SELECT dr.id_rec AS receta, GROUP_CONCAT(dr.fol_det SEPARATOR ',') AS id, Sum(dr.can_sol) AS solicitado, dr.det_pro AS producto, dp.cla_pro AS clave FROM detreceta AS dr INNER JOIN detalle_productos AS dp ON dr.det_pro = dp.det_pro WHERE dr.id_rec = ? AND dr.`status` = ? AND dr.cant_sur = 0 GROUP BY dp.cla_pro ) AS sol ON sol.clave = dp.cla_pro AND sol.receta = det.id_rec WHERE det.`status` = ? GROUP BY dp.cla_pro FOR UPDATE;";

    public static final String OBTENER_PENDIENTE_SURTIDO_PARCIAL_QUERY = "SELECT dr.fol_det AS id, IF ( dr.can_sol - dr.cant_sur > 0, 1, 0 ) AS es_pendiente, dr.can_sol - (dr.cant_sur) AS diferencia, dr.can_sol - ( dr.cant_sur + IFNULL(drec2.surtido, 0)) AS faltante, dr.det_pro AS producto, dr.cant_sur AS surtido, dr.id_cau AS cause, dr.id_cau2 AS cause2, dr.indicaciones AS indicaciones, i.id_inv AS inventario, IFNULL(ap.id, - 1) AS apartamiento, (i.cant + dr.cant_sur) AS nueva_existencia, dp.cla_pro AS clave FROM detreceta AS dr INNER JOIN detalle_productos AS dp ON dp.det_pro = dr.det_pro INNER JOIN inventario AS i ON i.det_pro = dr.det_pro LEFT JOIN apartamiento AS ap ON ap.detalle_receta = dr.fol_det AND ap.detalle_receta = dr.det_pro LEFT JOIN ( SELECT dr2.fol_det AS id, dr2.det_pro AS detalle, dr2.cant_sur AS surtido, dp2.cla_pro AS clave FROM detreceta AS dr2 INNER JOIN detalle_productos AS dp2 ON dr2.det_pro = dp2.det_pro WHERE dr2.id_rec = ? AND dr2.`status` = ? AND dr2.baja = ? ) AS drec2 ON drec2.id <> dr.fol_det AND drec2.clave = dp.cla_pro AND drec2.detalle <> dp.det_pro WHERE dr.id_rec = ? AND dr.`status` = ? AND dr.baja = ? AND dp.lot_pro <> '-' HAVING es_pendiente = 1 FOR UPDATE;";

    public static final String OBTENER_ENCABEZADO_FOLIO_QUERY = "SELECT 1 FROM receta AS r WHERE r.fol_rec = ? FOR UPDATE;";

    public static final String ACTUALIZAR_SURTIDO_DETALLE_QUERY = "UPDATE `detreceta` SET `cant_sur` = ?, `status` = IF ( `status` NOT IN (?,?) ,?, `status` ), `indicaciones` = SUBSTRING_INDEX(indicaciones, ?, 1) WHERE (`fol_det` = ?);";

    public static final String OBTENER_DETALLES_RECETA_QUERY = "SELECT dr.can_sol AS solicitado, dr.cant_sur AS surtida, dr.fec_sur AS fecha, dr.`status` AS `status`, dr.hor_sur AS hora, dr.id_cau AS cause, dr.baja AS baja, dr.indicaciones AS indicaciones, dr.id_cau2 AS cause2, dp.cla_pro AS clave, dp.lot_pro AS lote, dp.cad_pro AS caducidad, productos.des_pro AS descripcion, dr.fol_det AS id, dr.det_pro AS producto FROM detreceta AS dr INNER JOIN detalle_productos AS dp ON dr.det_pro = dp.det_pro INNER JOIN productos ON dp.cla_pro = productos.cla_pro WHERE dr.id_rec = ? AND dr.baja = 0 ORDER BY id ASC;";

    public static final String OBTENER_FOLIO_COLECTIVA_QUERY = "SELECT indices.id_rec AS folio FROM `indices` FOR UPDATE;";

    public static final String LISTAR_FOLIOS_DISPONIBLES_QUERY = "SELECT d.id AS folio, IFNULL(r.id_rec, - 1) AS disponible FROM (%s) AS d LEFT JOIN receta AS r ON cast(d.id AS CHAR(100)) = r.fol_rec ORDER BY folio";

    public static final String ACTUALIZAR_FOLIO_COLECTIVA_QUERY = "UPDATE `indices` SET `id_rec` = ?;";

    public static final String OBTENER_DETALLE_PRODUCTO_POR_ID_QUERY = "SELECT dp.cla_pro AS clave, dp.lot_pro AS lote, dp.cad_pro AS caducidad, dp.id_ori AS origen FROM detalle_productos AS dp WHERE dp.det_pro = ?;";

    public static final String OBTENER_ENCABEZADO_ID_QUERY = "SELECT 1 FROM receta WHERE id_rec = ? FOR UPDATE;";

    public static final String OBTENER_RECETAS_CADUCAS_QUERY = "SELECT r.id_rec AS id FROM receta AS r WHERE  r.id_rec <> 1 AND r.baja = ? AND r.transito <> ? AND TIMESTAMPDIFF(MONTH, r.fecha_hora, NOW()) > ? FOR UPDATE;";

    public static final String ACTUALIZAR_ESTADO_NOTIFICACION_QUERY = "UPDATE receta SET notificahl7 = ? WHERE id_rec = ?;";

    public ConectionDB getCon() {
        return con;
    }

    public void setCon(ConectionDB con) {
        this.con = con;
    }

    private Kardex crearMovimientoCancelacion(int idReceta, String observacion) {
        Kardex kardex = new Kardex();
        kardex.setIdReceta(idReceta);
        kardex.setTipoMovimiento(Kardex.REINTEGRO_TIPO);
        kardex.setNombreAbasto(Kardex.SIN_ABASTO);
        kardex.setObservacion(observacion);
        kardex.setIdUsuario(this.idUsuario);

        return kardex;
    }

    @Override
    public void cancelarReceta(String folio, int idReceta, int tipoReceta, int idUsuario) throws SQLException {

        PreparedStatement ps;
        ResultSet rs;

        int cantidadReintegro, idInventario, nuevaCantidad, idDetalleProducto;
        String mensajeError, idDetalles;

        InventarioImpl inventario = new InventarioImpl();
        inventario.setCon(con);

        Kardex kardex = crearMovimientoCancelacion(idReceta, Kardex.CANCELACION_RECETA_OBSERVACION);

        KardexImpl kardexImpl = new KardexImpl();
        kardexImpl.setCon(con);

        ps = con.getConn().prepareStatement(String.format(LISTAR_DETALLE_RECETA_QUERY, ""),
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
        ps.setInt(1, idReceta);
        ps.setInt(2, Receta.ACTIVA_BAJA);
        rs = ps.executeQuery();

        ApartamientoImpl apartamientoImpl = new ApartamientoImpl(con);
        ApartamientoImpl aux;

        while (rs.next()) {
            idInventario = rs.getInt("id_inventario");
            idDetalles = rs.getString("id");
            cantidadReintegro = rs.getInt("cantidad_reintegro");
            nuevaCantidad = rs.getInt("nueva_cantidad");
            idDetalleProducto = rs.getInt("detalle_producto");

            aux = null;
            if (rs.getInt("tiene_apartado") != NO_ENCONTRADO) {
                aux = apartamientoImpl;
            }

            cancelarDetalle(idDetalles, idInventario, cantidadReintegro,
                    nuevaCantidad, idDetalleProducto, kardex, kardexImpl, aux);
        }

        rs.close();
        ps.close();

        ps = con.getConn().prepareStatement(ACTUALIZAR_FOLIO_RECETA_QUERY);
        ps.setInt(1, Receta.CANCELADA_BAJA);
        ps.setInt(2, Receta.SURTIDA_TRANSITO);
        ps.setInt(3, idReceta);

        boolean ok = ps.executeUpdate() > 0;
        if (!ok) {
            ps.close();
            mensajeError = String.format("NO actualizo el encabezado de la receta al cancelarse.| id_receta: %d | folio: %s", idReceta, folio);
            throw new SQLException(mensajeError);
        }

        ps.close();

    }

    @Override
    public void cancelarDetalle(int idReceta, String claveProducto, int tipoReceta, int idUsuario) throws SQLException {
        PreparedStatement ps;
        ResultSet rs;

        int cantidadReintegro, idInventario, nuevaCantidad, detalleProducto;
        String idDetalles;

        Kardex kardex = crearMovimientoCancelacion(idReceta, Kardex.CANCELACION_DETALLE_OBSERVACION);

        KardexImpl kardexImpl = new KardexImpl();
        kardexImpl.setCon(con);

        ApartamientoImpl apartamientoImpl = new ApartamientoImpl(con);

        ps = con.getConn().prepareStatement(String.format(LISTAR_DETALLE_RECETA_QUERY, "AND dp.cla_pro = ?"),
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
        ps.setInt(1, idReceta);
        ps.setInt(2, Receta.ACTIVA_BAJA);
        ps.setString(3, claveProducto);
        rs = ps.executeQuery();
        while (rs.next()) {
            idInventario = rs.getInt("id_inventario");
            idDetalles = rs.getString("id");
            cantidadReintegro = rs.getInt("cantidad_reintegro");
            nuevaCantidad = rs.getInt("nueva_cantidad");
            detalleProducto = rs.getInt("detalle_producto");

            cancelarDetalle(idDetalles, idInventario, cantidadReintegro, nuevaCantidad,
                    detalleProducto, kardex, kardexImpl, apartamientoImpl);
        }

        rs.close();
        ps.close();
    }

    private void cancelarDetalle(String idDetallesRecetas, int idInventario, int cantidadReintegro, int nuevaCantidad,
            int detalleProducto, Kardex kardex, KardexImpl kardexImpl, ApartamientoImpl apartamientoImpl) throws SQLException {

        String mensajeError;
        boolean resultado;
        PreparedStatement ps;

        //Se cancela el detalle de la receta
        String qry = String.format(CANCELACION_DETALLE_QUERY, idDetallesRecetas);
        ps = con.getConn().prepareStatement(qry);
        ps.setInt(1, Receta.CANCELADA_BAJA);

        resultado = ps.executeUpdate() > 0;
        ps.close();
        if (!resultado) {
            mensajeError = String.format("NO se modifico el detalle de la receta para la cancelacion.| id_detReceta: %s ", idDetallesRecetas);
            throw new SQLException(mensajeError);
        }

        //Es el detalle de un pendiente y no es necesario mover el inventario.
        if (idInventario == NO_ENCONTRADO) {
            return;
        }

        //Nunca se surtio, luego no se movio el inventario.
        if (cantidadReintegro == 0) {
            return;
        }

        //Para los detalles que no han sido apartados. Captura médico o recetas sistema viejo.
        if (apartamientoImpl != null) {

            String[] ids = {idDetallesRecetas};
            if (idDetallesRecetas.contains(",")) {
                ids = idDetallesRecetas.split(",");
            }

            for (String id : ids) {
                apartamientoImpl.modificarApartado(Integer.valueOf(id), Apartado.CANCELADO_STATUS);
            }
        }

        //Se reintegra al inventario
        InventarioImpl inventario = new InventarioImpl();
        inventario.setCon(con);
        resultado = inventario.actualizaInventario(idInventario, nuevaCantidad);
        if (!resultado) {
            mensajeError = String.format("NO se modifico el inventario para la cancelacion.| id_detReceta: %s | id_inventario: %d | cantidad: %d ", idDetallesRecetas, idInventario, cantidadReintegro);
            throw new SQLException(mensajeError);
        }

        //Se genera un movimiento en kardex
        kardex.setDetalleProducto(detalleProducto);
        kardex.setCantidad(cantidadReintegro);

        resultado = kardexImpl.agregarMovimiento(kardex);
        if (!resultado) {
            mensajeError = String.format("NO se modifico el kardex para la cancelacion.| id_detReceta: %s | kardex: %s ", idDetallesRecetas, kardex.toString());
            throw new SQLException(mensajeError);
        }

    }

    @Override
    public int surtirReceta(int idReceta, int detalleProducto,
            int cantidadSurtida, int tipoSistemas) throws SQLException {

        DetalleReceta pendiente, auxiliar;
        DetalleProducto producto;

        pendiente = reconstruirDetalleReceta(idReceta, detalleProducto);
        producto = reconstruirDetalleProducto(pendiente.getDetalleProducto());

        if (pendiente.getCantidadSolicitada() < (pendiente.getCantidadSurtida() + cantidadSurtida)) {
            return SURTIDO_MAYOR;
        }

        InventarioImpl dao = new InventarioImpl(con);
        List<Inventario> disponibles = dao.obtenerDisponibles(producto.getClave());
        ApartamientoImpl apartamientoImpl = new ApartamientoImpl(con);
        Inventario disponible;

        int diferencia, cantidadApartada, faltante, total, accion, modificaciones;
        faltante = cantidadSurtida;
        total = disponibles.size();
        boolean esAdicional = false;
        modificaciones = 0;

        for (int i = 0; i < total; i++) {

            accion = 0;
            disponible = disponibles.get(i);
            diferencia = disponible.getCantidad() - faltante; //Diferencia entre existencias actuales y faltante a surtir.
            cantidadApartada = diferencia >= 0 ? faltante : disponible.getCantidad();

            if (producto.isFaltante()) {
                cambiarLote(pendiente.getIdReceta(), disponible.getDetalleProducto(), cantidadApartada);
                modificaciones++;
                accion++;
            }

            if (disponible.getDetalleProducto() == producto.getId()) {
                //Aplica tanto para surtido parcial o completo.
                apartamientoImpl.agregarApartado(cantidadApartada, disponible.getDetalleProducto(),
                        pendiente.getId(), Apartado.POR_RECETA_TIPO);
                surtirReceta(pendiente, cantidadApartada);
                disponibles.remove(i);
                total = disponibles.size();
                i = 0;
                esAdicional = true;
                modificaciones++;
                accion++;
            } else if (esAdicional) {
                auxiliar = agregarDetalle(disponible.getDetalleProducto(), cantidadApartada, cantidadApartada,
                        pendiente.getCausePrimario(), pendiente.getCauseSegundario(),
                        pendiente.getIndicaciones(), idReceta, DetalleReceta.DETALLE_ADICIONADO_STATUS);

                apartamientoImpl.agregarApartado(cantidadApartada, disponible.getDetalleProducto(),
                        auxiliar.getId(), Apartado.POR_RECETA_TIPO);
                surtirReceta(auxiliar, cantidadApartada);
                modificaciones++;
                accion++;
            }

            if (accion > 0) {
                faltante -= cantidadApartada;
            }

            if (faltante == 0) {
                return cantidadSurtida; // Surtio completamente.
            }

            //El detalle original de producto ya no posee existencias.
            if ((i == total - 1) && (modificaciones == 0)) {
                producto.setCaducidad(DetalleProducto.CADUCIDAD_FALTANTE);
                producto.setLote(DetalleProducto.LOTE_FALTANTE);
                i = i == 0 ? -1 : 0; // Para reiniciar la búsqueda.
            }
        }
        return faltante * (-1);
    }

    @Override
    public boolean surtirReceta(DetalleReceta detalle, int cantidadSurtir) throws SQLException {
        PreparedStatement ps;
        ResultSet rs;
        int nuevasExistencias, idInventario;

        ps = con.getConn().prepareStatement(OBTENER_NUEVA_EXISTENCIAS_DETALLE_QUERY,
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
        ps.setInt(1, cantidadSurtir);
        ps.setInt(2, detalle.getDetalleProducto());

        rs = ps.executeQuery();
        rs.next();
        nuevasExistencias = rs.getInt("nuevo_inventario");
        idInventario = rs.getInt("id");

        rs.close();
        ps.close();

        if (nuevasExistencias < 0) {
            //No se posee existencia para surtir.
            return false;
        }

        Kardex kardex;
        KardexImpl kardexImpl = new KardexImpl();
        kardexImpl.setCon(con);
        InventarioImpl inventario = new InventarioImpl();
        inventario.setCon(con);
        kardex = new Kardex();
        kardex.setIdReceta(detalle.getIdReceta());
        kardex.setTipoMovimiento(Kardex.MOVIMIENTO_SALIDA_RECETA_TIPO);
        kardex.setNombreAbasto(Kardex.SIN_ABASTO);
        kardex.setObservacion(tipoRecetaString);
        kardex.setIdUsuario(this.idUsuario);
        kardex.setCantidad(cantidadSurtir);
        kardex.setDetalleProducto(detalle.getDetalleProducto());

        boolean resultado = kardexImpl.agregarMovimiento(kardex);
        if (!resultado) {
            String mensajeError = String.format("NO se agrego el movimiento en kardex.| id_detReceta: %d | detProducto: %d", detalle.getIdReceta(), detalle.getDetalleProducto());
            throw new SQLException(mensajeError);
        }

        resultado = inventario.actualizaInventario(idInventario, nuevasExistencias);
        if (!resultado) {
            String mensajeError = String.format("NO se modifico la cantidad en el inventario.| id_detReceta: %d | detProducto: %d", detalle.getIdReceta(), detalle.getDetalleProducto());
            throw new SQLException(mensajeError);
        }

        //Se quita apartamiento para este 
        ApartamientoImpl apartamientoImpl = new ApartamientoImpl(con);
        apartamientoImpl.modificarApartado(detalle.getId(), Apartado.DISPENSADO_STATUS);

        //Se actualiza el detalle receta
        ps = con.getConn().prepareStatement(ACTUALIZAR_SURTIDO_DETALLE_QUERY);
        ps.setInt(1, cantidadSurtir);
        ps.setInt(2, DetalleReceta.NORMAL_STATUS);
        ps.setInt(3, DetalleReceta.DETALLE_ADICIONADO_STATUS);
        ps.setInt(4, DetalleReceta.NORMAL_STATUS);
        ps.setString(5, " ||");
        ps.setInt(6, detalle.getId());

        resultado = ps.executeUpdate() > 0;
        if (!resultado) {
            String mensajeError = String.format("NO se modifico el detalle receta.| id_detReceta: %d | detProducto: %d", detalle.getIdReceta(), detalle.getDetalleProducto());
            throw new SQLException(mensajeError);
        }

        return resultado;
    }

    private DetalleReceta agregarDetalle(int detalleProducto, int cantidadSolicitado, int cantidadSurtida, String cause, String cause2,
            String indicaciones, int idReceta, int status) throws SQLException {
        return agregarDetalle(detalleProducto, cantidadSolicitado,
                cantidadSurtida, cause, cause2, indicaciones, idReceta, status, true);
    }

    private DetalleReceta agregarDetalle(int detalleProducto, int cantidadSolicitado, int cantidadSurtida, String cause, String cause2,
            String indicaciones, int idReceta, int status, boolean buscar) throws SQLException {

        int id;
        PreparedStatement ps;
        ResultSet rs;

        DetalleReceta detalle = new DetalleReceta();
        detalle.setDetalleProducto(detalleProducto);
        detalle.setCantidadSolicitada(cantidadSolicitado);
        detalle.setCausePrimario(cause);
        detalle.setCauseSegundario(cause2);
        detalle.setIndicaciones(indicaciones);
        detalle.setIdReceta(idReceta);
        detalle.setBaja(Receta.ACTIVA_BAJA);
        detalle.setCantidadSurtida(cantidadSurtida);
        detalle.setStatus(status);

        if (buscar) {

            ps = con.getConn().prepareStatement(OBTENER_ANTERIOR_SOLICITADO_QUERY, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
            ps.setInt(1, idReceta);
            ps.setInt(2, detalleProducto);
            ps.setInt(3, Receta.ACTIVA_BAJA);
            ps.setInt(4, DetalleReceta.NORMAL_STATUS);
            ps.setInt(5, DetalleReceta.DETALLE_ADICIONADO_STATUS);

            rs = ps.executeQuery();
            if (rs.next()) { //Existe previo
                cantidadSolicitado += rs.getInt("anterior");
                id = rs.getInt("id");
                rs.close();
                ps.close();

                //Se actualiza anterior.
                ps = con.getConn().prepareStatement(ACTUALIZAR_SOLICITADO_QUERY);
                ps.setInt(1, cantidadSolicitado);
                ps.setInt(2, id);

                boolean ok = ps.executeUpdate() > 0;
                if (!ok) {
                    ps.close();
                    String mensajeError = String.format("NO se actualizo el solicitado del pendiente.| idReceta: %d | id_detReceta: %d", idReceta, detalleProducto);
                    throw new SQLException(mensajeError);
                }

                detalle.setId(id);
                return detalle;
            }
        }
        //Sino lo añade.
        id = agregarDetalle(detalle);
        detalle.setId(id);

        return detalle;
    }

    private void agregarDetallePendiente(String clave, int cantidadSolicitado, String cause, String cause2,
            String indicaciones, int idReceta) throws SQLException {
        PreparedStatement ps;
        ResultSet rs;
        int idDetalle;

        ps = con.getConn().prepareStatement(OBTENER_DETALLEPRODUCTO_QUERY);
        ps.setString(1, clave);
        ps.setString(2, "-");
        rs = ps.executeQuery();
        if (!rs.next()) { //Sino existe se añade un detalle con lote '-'.

            rs.close();
            ps.close();

            ps = con.getConn().prepareStatement(INSERT_DETALLEPRODUCTO_QUERY);
            ps.setString(1, clave);
            ps.setString(2, DetalleProducto.LOTE_FALTANTE);
            ps.setString(3, DetalleProducto.CADUCIDAD_FALTANTE);
            boolean ok = ps.executeUpdate() > 0;
            if (!ok) {
                ps.close();
                String mensajeError = String.format("NO se agrego el detalle producto vacio.| idReceta: %d | clave: %s", idReceta, clave);
                throw new SQLException(mensajeError);
            }
            ps.close();

            ps = con.getConn().prepareStatement(OBTENER_DETALLEPRODUCTO_QUERY);
            ps.setString(1, clave);
            ps.setString(2, "-");
            rs = ps.executeQuery();
            rs.next();
        }

        idDetalle = rs.getInt("id");
        rs.close();
        ps.close();

        //Sino existe ya un pendiente.
        agregarDetalle(idDetalle, cantidadSolicitado, 0, cause, cause2, indicaciones, idReceta, DetalleReceta.NORMAL_STATUS);

    }

    @Override
    public int agregarDetalle(DetalleReceta detalle) throws SQLException {
        int id;
        PreparedStatement ps;
        ResultSet rs;

        ps = con.getConn().prepareStatement(INSERT_DETALLERECETA_QUERY, PreparedStatement.RETURN_GENERATED_KEYS);
        ps.setInt(1, detalle.getDetalleProducto());
        ps.setInt(2, detalle.getCantidadSolicitada());
        ps.setInt(3, detalle.getCantidadSurtida());
        ps.setInt(4, detalle.getStatus());
        ps.setInt(5, detalle.getIdReceta());
        ps.setString(6, detalle.getCausePrimario());
        ps.setString(7, detalle.getIndicaciones());
        ps.setInt(8, detalle.getBaja());
        ps.setString(9, detalle.getCauseSegundario());

        boolean ok = ps.executeUpdate() > 0;
        if (!ok) {
            ps.close();
            String mensajeError = String.format("NO se agrego el detalle de la receta.| idReceta: %d | detalle_producto: %d | solicitado: %d",
                    detalle.getIdReceta(), detalle.getDetalleProducto(), detalle.getCantidadSolicitada());
            throw new SQLException(mensajeError);
        }

        rs = ps.getGeneratedKeys();
        rs.next();
        id = rs.getInt(1);

        rs.close();
        ps.close();

        return id;
    }

    @Override
    public int contarClavesPorReceta(int idReceta, String clave) throws SQLException {
        PreparedStatement ps;
        ResultSet rs;
        ps = con.getConn().prepareStatement(OBTENER_NUMEROCLAVES_QUERY);
        ps.setInt(1, idReceta);
        ps.setString(2, clave);

        rs = ps.executeQuery();
        rs.next();

        int total = rs.getInt("total");
        rs.close();
        ps.close();

        return total;
    }

    @Override
    public Receta reconstruirReceta(int idReceta) throws SQLException {
        PreparedStatement ps;
        ResultSet rs;

        ps = con.getConn().prepareStatement(String.format(OBTENER_RECETA_QUERY, this.formatoRecetaSalida, "r.id_rec = ?"));
        ps.setInt(1, idReceta);
        rs = ps.executeQuery();
        if (!rs.isBeforeFirst()) {
            return null;
        }
        rs.next();

        Receta receta = reconstruirReceta(rs);

        //se cierran objetos temporales de consulta
        rs.close();
        ps.close();

        return receta;
    }

    public Receta reconstruirReceta(String folio) throws SQLException {
        PreparedStatement ps;
        ResultSet rs;

        ps = con.getConn().prepareStatement(String.format(OBTENER_RECETA_QUERY, this.formatoRecetaSalida, "r.fol_rec = ?"));
        ps.setString(1, folio);
        rs = ps.executeQuery();
        if (!rs.isBeforeFirst()) {
            return null;
        }
        rs.next();

        Receta receta = reconstruirReceta(rs);

        //se cierran objetos temporales de consulta
        rs.close();
        ps.close();

        return receta;
    }

    private Receta reconstruirReceta(ResultSet rs) throws SQLException {
        Receta receta = new Receta();
        receta.setId(rs.getInt("id"));
        receta.setTipoReceta(rs.getInt("tipo_receta"));
        receta.setIdPaciente(rs.getLong("paciente"));
        receta.setIdMedico(rs.getInt("medico"));
        receta.setIdUsuario(rs.getInt("usuario"));
        receta.setIdServicio(rs.getInt("servicio"));
        receta.setTrasito(rs.getInt("transito"));
        receta.setBaja(rs.getInt("baja"));

        receta.setFolio(rs.getString("folio"));
        receta.setTipoConsulta(rs.getString("tipo_consulta"));
        receta.setFecha(rs.getString("fecha"));
        receta.setObservacionHL7(rs.getString("observacionHL7"));

        return receta;
    }

    @Override
    public String capturaMedicamento(int idReceta, String clave, int cantidadSolicitada,
            String indicaciones, String cause, String cause2, int tipoCaptura) throws SQLException {

        if (this.tipoRecetaString.equals(Kardex.MANUAL_OBSERVACION)) {
            //Verifica que pueda captura más medicamentos.
            if (contarClavesPorReceta(idReceta, clave) == TOTAL_CLAVES_POR_RECETA) {
                return LIMITE;
            }
        }

        //Asigna para colectiva.
        if (this.tipoRecetaString.equals(Kardex.COLECTIVA_OBSERVACION)) {
            indicaciones = DetalleReceta.INDICACIONES_COLECTIVO;
            cause = DetalleReceta.CAUSE_COLECTIVO;
            cause2 = "";
        }

        //Declaracion de variables globales
        // Se obtiene el id del cause
        String[] arrayCauses = cause.split(" - ");
        if (arrayCauses.length > 1) {
            cause = arrayCauses[0];
        }

        arrayCauses = cause2.split(" - ");
        if (arrayCauses.length > 1) {
            cause2 = arrayCauses[0];
        }

        ApartamientoImpl apartamientoImpl = new ApartamientoImpl(con);
        DetalleReceta detalleReceta;
        int faltante, detalleProducto, diferencia,
                existenciasIniciales, cantidadApartada;

        InventarioImpl inventarioImpl = new InventarioImpl(con);

        List<Inventario> disponibles = inventarioImpl.obtenerDisponibles(clave);
        faltante = cantidadSolicitada;

        for (Inventario disponible : disponibles) {
            detalleProducto = disponible.getDetalleProducto();
            existenciasIniciales = disponible.getCantidad();

            diferencia = faltante - existenciasIniciales; //Diferencia entre existencias actuales y faltante a surtir.
            cantidadApartada = diferencia >= 0 ? existenciasIniciales : faltante;

            detalleReceta = agregarDetalle(detalleProducto, cantidadApartada,
                    0, cause, cause2, indicaciones, idReceta, DetalleReceta.NORMAL_STATUS);

            if (tipoCaptura == FARMACIA_CAPTURA) {
                apartamientoImpl.agregarApartado(cantidadApartada, detalleProducto,
                        detalleReceta.getId(), Apartado.POR_RECETA_TIPO);
                surtirReceta(detalleReceta, cantidadApartada);
            } else {
                //Este aplica para MEDICO_CAPTURA o CODIGO_BARRAS_CAPTURA

                if (tipoCaptura == CODIGO_BARRAS_CAPTURA) {
                    apartamientoImpl.agregarApartado(cantidadApartada, detalleProducto,
                            detalleReceta.getId(), Apartado.POR_RECETA_TIPO);
                }
            }

            faltante = diferencia;

            if (diferencia <= 0) {
                break; // Surtido completo, no es necesario mirar mas detalles.
            }

        }

        if (faltante > 0) {
            agregarDetallePendiente(clave, faltante, cause, cause2, indicaciones, idReceta);
        }

        return "";
    }

    @Override
    public int agregarDetalleProducto(String clave) throws SQLException {
        int agregado = 0;
        PreparedStatement ps;

        ps = con.getConn().prepareStatement(INSERT_DETALLEPRODUCTO_QUERY);
        ps.setString(1, clave);
        ps.executeUpdate();

        ps = con.getConn().prepareStatement("SELECT MAX(det_pro) AS detPro FROM detalle_productos;");
        ResultSet rs;
        rs = ps.executeQuery();
        while (rs.next()) {
            agregado = rs.getInt("detPro");
        }

        ps.close();
        rs.close();

        return agregado;
    }

    private int crearEncabezado(int tipoReceta, long idPaciente, int idMedico,
            int idUsuario, int idServicio, String folio, String tipoConsulta,
            String fecha, String encargadoServicio, String carnet, String observacion,
            String observacionHL7) throws SQLException {

        PreparedStatement ps;
        ResultSet rs;

        //Verifica la existencia de ese folio en otra receta.
        if (validarFolio(folio)) {
            return FOLIO_EXISTENTE;
        }

        ps = con.getConn().prepareStatement(String.format(INSERT_EMCABEZADO_QUERY, this.formatoReceta),
                PreparedStatement.RETURN_GENERATED_KEYS);
        ps.setString(1, folio);
        ps.setLong(2, idPaciente);
        ps.setInt(3, idMedico);
        ps.setInt(4, tipoReceta);
        ps.setInt(5, idUsuario);
        ps.setString(6, encargadoServicio);
        ps.setString(7, carnet);
        ps.setInt(8, idServicio);
        ps.setString(9, fecha);
        ps.setInt(10, Receta.EN_CAPTURA_TRANSITO);
        ps.setInt(11, Receta.ACTIVA_BAJA);
        ps.setInt(12, Receta.SIN_NOTIFICAR_HL7_NOTIFICACION);
        ps.setString(13, observacion);
        ps.setString(14, tipoConsulta);
        ps.setString(15, observacionHL7);

        boolean ok = ps.executeUpdate() == 1;
        if (!ok) {
            ps.close();
            String mensajeError = String.format("NO creo el encabezado de la receta.| Folio: %s | id_Medico: %d | id_Paciente: %s", folio, idMedico, idPaciente);
            throw new SQLException(mensajeError);
        }

        rs = ps.getGeneratedKeys();
        rs.next();

        int id = rs.getInt(1);

        rs.close();
        ps.close();

        return id;
    }

    /**
     * Uso para captura manual
     *
     * @param tipoReceta
     * @param idPaciente
     * @param idMedico
     * @param idUsuario
     * @param folio
     * @param tipoConsulta
     * @param fecha
     * @param carnet
     * @return
     * @throws SQLException
     */
    @Override
    public int crearEncabezado(int tipoReceta, long idPaciente, int idMedico,
            int idUsuario, String folio, String tipoConsulta,
            String fecha, String carnet) throws SQLException {

        return crearEncabezado(tipoReceta, idPaciente, idMedico, idUsuario,
                Receta.ID_SERVICIO_MANUAL_TIPO, folio, tipoConsulta, fecha,
                Receta.ENCARGADO_SERVICIO_MANUAL_TIPO, carnet,
                Receta.OBSERVACION_MANUAL_TIPO, Receta.OBSERVACION_HL7_MANUAL_TIPO);
    }

    /**
     * Uso para captura manual por HL7
     *
     * @param tipoReceta
     * @param idPaciente
     * @param idMedico
     * @param idUsuario
     * @param folio
     * @param tipoConsulta
     * @param fecha
     * @param carnet
     * @param observacion
     * @param ObservacionHL7
     * @return
     * @throws SQLException
     */
    public int crearEncabezado(int tipoReceta, long idPaciente, int idMedico,
            int idUsuario, String folio, String tipoConsulta,
            String fecha, String carnet, String observacion, String ObservacionHL7) throws SQLException {

        return crearEncabezado(tipoReceta, idPaciente, idMedico, idUsuario,
                Receta.ID_SERVICIO_MANUAL_TIPO, folio, tipoConsulta, fecha,
                Receta.ENCARGADO_SERVICIO_MANUAL_TIPO, carnet,
                observacion, ObservacionHL7);
    }

    /**
     * Uso para captura colectiva
     *
     * @param tipoReceta
     * @param idMedico
     * @param idUsuario
     * @param idServicio
     * @param folio
     * @param fecha
     * @return
     * @throws SQLException
     */
    @Override
    public int crearEncabezado(int tipoReceta, int idMedico, int idUsuario,
            int idServicio, String folio, String fecha) throws SQLException {
        String nuevoFolio = folio;
        PreparedStatement ps;

        if (folio.isEmpty()) {
            ResultSet rs;
            ps = con.getConn().prepareStatement(OBTENER_FOLIO_COLECTIVA_QUERY,
                    ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
            rs = ps.executeQuery();
            rs.next();
            int actual = rs.getInt("folio");
            rs.close();
            ps.close();

            StringBuilder subquery = new StringBuilder();
            subquery.append("SELECT ");
            subquery.append(actual);
            subquery.append(" AS id ");
            int ultimo = actual + 15; //Crea lista con indices 15 superiores al actual.
            for (int i = actual; i < ultimo; i++) {
                subquery.append(" UNION SELECT ");
                subquery.append(i);
            }

            ps = con.getConn().prepareStatement(String.format(LISTAR_FOLIOS_DISPONIBLES_QUERY, subquery.toString()));
            rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getInt("disponible") != NO_ENCONTRADO) {
                    continue;
                }

                nuevoFolio = rs.getString("folio");
                break;
            }
            
            if (nuevoFolio.isEmpty()) { //toma el último en caso de ninguno de los 10 disponibles.
                rs.last();
                nuevoFolio = rs.getString("folio");
            }
            
            rs.close();
            ps.close();
        }

        int resultado = crearEncabezado(tipoReceta, Receta.ID_PACIENTE_COLECTIVA_TIPO,
                idMedico, idUsuario, idServicio, nuevoFolio, Receta.TIPO_CONSULTA_COLECTIVA_TIPO,
                fecha, String.valueOf(idMedico), Receta.CARNET_COLECTIVA_TIPO,
                Receta.OBSERVACION_MANUAL_TIPO, Receta.OBSERVACION_HL7_MANUAL_TIPO);

        if (folio.isEmpty()) {
            ps = con.getConn().prepareStatement(ACTUALIZAR_FOLIO_COLECTIVA_QUERY);
            ps.setString(1, nuevoFolio);
            boolean ok = ps.executeUpdate() > 0;
            if (!ok) {
                ps.close();
                String mensajeError = String.format("NO se actualizo el indice del encabezado.| Folio: %s ", nuevoFolio);
                throw new SQLException(mensajeError);
            }
            ps.close();
        }

        return resultado;
    }

    @Override
    public List<LoteDisponible> obtenerLotesDisponibles(String codigoBarras, int idReceta) throws SQLException {
        PreparedStatement ps;
        ResultSet rs;

        ps = con.getConn().prepareStatement(OBTENER_LOTES_DISPONIBLES_QUERY,
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
        ps.setInt(1, idReceta);
        ps.setString(2, codigoBarras);

        rs = ps.executeQuery();
        List<LoteDisponible> resultado = new ArrayList<>();
        LoteDisponible temp;

        while (rs.next()) {
            temp = new LoteDisponible();

            temp.inventario.setId(rs.getInt("inventario"));
            temp.detalleProducto.setId(rs.getInt("detalle"));
            temp.detalleProducto.setClave(rs.getString("clave"));
            temp.detalleProducto.setLote(rs.getString("lote"));
            temp.detalleProducto.setCaducidad(rs.getString("caducidad"));
            temp.setCantidadSolicitada(rs.getInt("solicitado"));

            resultado.add(temp);
        }

        rs.close();
        ps.close();

        return resultado;
    }

    @Override
    public boolean cambiarLote(int idReceta, int nuevoDetalleProducto, int cantidad) throws SQLException {
        PreparedStatement ps, psIndicaciones;
        ResultSet rs;
        String cause = "", cause2 = "", indicaciones = "";
        DetalleReceta detalleReceta;
        int cambiar = 0;

        ps = con.getConn().prepareStatement(OBTENER_CAUSES_INDICACIONES_ANTERIORES_QUERY,
                ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ps.setInt(1, idReceta);
        ps.setInt(2, nuevoDetalleProducto);

        rs = ps.executeQuery();
        String mensajeCambio = "|| CAMBIO LOTE: %s -> %s | SURTIDO: %d";
        psIndicaciones = con.getConn().prepareStatement(String.format(ACTUALIZAR_INDICACIONES_DETALLE_QUERY,
                ", ' | FALTANTE: ', can_sol, ' ||' )"));
        int totalSolicitado = 0;

        while (rs.next()) {
            cause = rs.getString("cause");
            cause2 = rs.getString("cause2");
            indicaciones = rs.getString("indicaciones");
            cambiar = rs.getInt("cambiar");
            totalSolicitado += rs.getInt("solicitado");

            //Cuando el detalle esta surtido [parcialmente] no es necesario marcarlo como cambio de lote.
            if (cambiar == NO_ENCONTRADO) {
                continue;
            }

            psIndicaciones.setString(1, String.format(mensajeCambio,
                    rs.getString("viejo_lote"), rs.getString("nuevo_lote"), cantidad));
            psIndicaciones.setInt(2, DetalleReceta.CAMBIO_LOTE_STATUS);
            psIndicaciones.setInt(3, rs.getInt("id"));
            psIndicaciones.addBatch();
        }

        if (totalSolicitado < cantidad) {
            psIndicaciones.close();
            rs.close();
            ps.close();
            return false;
        }

        psIndicaciones.executeBatch();

        psIndicaciones.close();
        rs.close();
        ps.close();

        detalleReceta = agregarDetalle(nuevoDetalleProducto, cantidad, 0, cause,
                cause2, indicaciones, idReceta, DetalleReceta.DETALLE_ADICIONADO_STATUS, false);

        ApartamientoImpl apartamientoImpl = new ApartamientoImpl(con);
        apartamientoImpl.agregarApartado(cantidad, nuevoDetalleProducto,
                detalleReceta.getId(), Apartado.POR_RECETA_TIPO);

        return surtirReceta(detalleReceta, cantidad);
    }

    @Override
    public void validarReceta(int idReceta, int tipoCaptura) throws SQLException {
        cambiarTransitoReceta(idReceta, Receta.PENDIENTE_SURTIR_TRANSITO);

        if (tipoCaptura == FARMACIA_CAPTURA) {
            this.finalizarReceta(idReceta);
        }

        if (tipoCaptura == MEDICO_CAPTURA) {
            this.actualizarFolioMedico(idReceta);
        }

    }

    private void cambiarTransitoReceta(int idReceta, int transito) throws SQLException {
        PreparedStatement ps;

        ps = con.getConn().prepareStatement(ACTUALIZAR_EMCABEZADO_RECETA_QUERY);
        ps.setInt(1, transito);
        ps.setInt(2, idReceta);

        boolean ok = ps.executeUpdate() > 0;
        if (!ok) {
            ps.close();
            String mensajeError = String.format("NO se cambio el transito en el emcabezado.| id_receta: %d | transito: %d ", idReceta, transito);
            throw new SQLException(mensajeError);
        }

    }

    private DetalleProducto reconstruirDetalleProducto(int detalleProducto) throws SQLException {
        DetalleProducto nuevo = new DetalleProducto();

        PreparedStatement ps;
        ResultSet rs;

        ps = con.getConn().prepareStatement(OBTENER_DETALLE_PRODUCTO_POR_ID_QUERY);
        ps.setInt(1, detalleProducto);

        rs = ps.executeQuery();
        rs.next();

        nuevo.setId(detalleProducto);
        nuevo.setClave(rs.getString("clave"));
        nuevo.setLote(rs.getString("lote"));
        nuevo.setCaducidad(rs.getString("caducidad"));
        nuevo.setOrigen(rs.getInt("origen"));

        rs.close();
        ps.close();

        return nuevo;
    }

    private DetalleReceta reconstruirDetalleReceta(int idReceta, int detalleProducto) throws SQLException {

        PreparedStatement ps;
        ResultSet rs;

        ps = con.getConn().prepareStatement(OBTENER_DETALLE_POR_DETALLE_PRODUCTO_QUERY);
        ps.setInt(1, idReceta);
        ps.setInt(2, detalleProducto);

        rs = ps.executeQuery();
        rs.next();

        DetalleReceta resultado = reconstruirDetalleReceta(rs);

        rs.close();
        ps.close();

        return resultado;
    }

    /**
     * Mapea del resultset a el objeto. Campos necesarios: id, detalle_producto,
     * solicitado, surtido, fecha, `status`, receta, hora, cause, indicaciones,
     * cause2
     *
     * @param nueva
     * @param rs
     * @return
     * @throws SQLException
     */
    private DetalleReceta reconstruirDetalleReceta(ResultSet rs) throws SQLException {

        DetalleReceta nueva = new DetalleReceta();

        nueva.setId(rs.getInt("id"));
        nueva.setDetalleProducto(rs.getInt("detalle_producto"));
        nueva.setCantidadSolicitada(rs.getInt("solicitado"));
        nueva.setCantidadSurtida(rs.getInt("surtido"));
        nueva.setFechaSurtido(rs.getString("fecha"));
        nueva.setStatus(rs.getInt("status"));
        nueva.setIdReceta(rs.getInt("receta"));
        nueva.setHoraSurtido(rs.getString("hora"));
        nueva.setCausePrimario(rs.getString("cause"));
        nueva.setIndicaciones(rs.getString("indicaciones"));
        nueva.setCauseSegundario(rs.getString("cause2"));

        return nueva;
    }

    @Override
    public void finalizarReceta(int idReceta) throws SQLException {
        PreparedStatement ps, psDetalle;

        //Obtener pendientes en los que hubo cambio de lote.
        ps = con.getConn().prepareStatement(OBTENER_PENDIENTE_CAMBIO_LOTE_QUERY,
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
        ps.setInt(1, DetalleReceta.NORMAL_STATUS);
        ps.setInt(2, idReceta);
        ps.setInt(3, DetalleReceta.CAMBIO_LOTE_STATUS);
        ps.setInt(4, DetalleReceta.DETALLE_ADICIONADO_STATUS);

        ResultSet rs;
        rs = ps.executeQuery();

        int diferencia, faltante;
        String clave, cause, cause2, indicaciones;
        String[] ids;
        ApartamientoImpl apartamientoImpl = new ApartamientoImpl(con);
        boolean tienePendiente = false, ok;
        while (rs.next()) {
            diferencia = rs.getInt("diferencia");

            if (diferencia > 0) {
                clave = rs.getString("clave");
                cause = rs.getString("cause");
                cause2 = rs.getString("cause2");
                indicaciones = rs.getString("indicaciones");
                agregarDetallePendiente(clave, diferencia, cause, cause2, indicaciones, idReceta);
                tienePendiente = true;
            }

            ids = rs.getString("ids").split(",");
            for (String id : ids) {
                cancelarDetalle(id, NO_ENCONTRADO, 0, 0, 0, null, null, apartamientoImpl);
            }
        }

        rs.close();
        ps.close();

        //Se buscan los registros con faltante físico o no dispensacion.
        ps = con.getConn().prepareStatement(OBTENER_PENDIENTE_SURTIDO_PARCIAL_QUERY,
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
        ps.setInt(1, idReceta);
        ps.setInt(2, DetalleReceta.DETALLE_ADICIONADO_STATUS);
        ps.setInt(3, Receta.ACTIVA_BAJA);
        ps.setInt(4, idReceta);
        ps.setInt(5, DetalleReceta.NORMAL_STATUS);
        ps.setInt(6, Receta.ACTIVA_BAJA);

        rs = ps.executeQuery();
        int surtido, detalleProducto, id,
                nuevaExistencia, idInventario;
        DetalleReceta temp;
        String mensajeFaltante = "|| FALTANTE FÍSICO: %d | SOLICITADO: %d | SURTIDO: %d ||";

        Kardex kardex = crearMovimientoCancelacion(idReceta, Kardex.CANCELACION_DETALLE_OBSERVACION);

        KardexImpl kardexImpl = new KardexImpl();
        kardexImpl.setCon(con);
        ApartamientoImpl aux;
        while (rs.next()) {

            // Se cancela el anterior detalle.
            detalleProducto = rs.getInt("producto");
            surtido = rs.getInt("surtido");
            nuevaExistencia = rs.getInt("nueva_existencia");
            idInventario = rs.getInt("inventario");
            id = rs.getInt("id");
            aux = null;

            //En el caso de médico no se realiza apartamiento o recetas sistema viejo.
            if (rs.getInt("apartamiento") != NO_ENCONTRADO) {
                aux = apartamientoImpl;
            }

            cancelarDetalle(String.valueOf(id), idInventario, surtido, nuevaExistencia,
                    detalleProducto, kardex, kardexImpl, aux);

            //marca el anterior registro como faltante fisico.
            diferencia = rs.getInt("diferencia");
            psDetalle = con.getConn().prepareStatement(String.format(ACTUALIZAR_INDICACIONES_DETALLE_QUERY, ")"));
            psDetalle.setString(1, String.format(mensajeFaltante, diferencia, surtido + diferencia, surtido));
            psDetalle.setInt(2, DetalleReceta.FALTANTE_FISICO_STATUS);
            psDetalle.setInt(3, id);

            ok = psDetalle.executeUpdate() > 0;
            if (!ok) {
                ps.close();
                String mensajeError = String.format("NO se marco el detalle receta como faltante físico.| id_detReceta: %d | diferencia: %d ", id, diferencia);
                throw new SQLException(mensajeError);
            }
            psDetalle.close();

            faltante = rs.getInt("faltante");
            cause = rs.getString("cause");
            cause2 = rs.getString("cause2");
            indicaciones = rs.getString("indicaciones");
            clave = rs.getString("clave");
            if (faltante > 0) {
                //agrega un detalle con lo pendiente.
                tienePendiente = true;
                agregarDetallePendiente(clave, faltante, cause, cause2, indicaciones, idReceta);
            }

            if (surtido == 0) {
                continue;
            }

            //agrega un detalle con lo surtido.
            temp = agregarDetalle(detalleProducto, surtido, 0, cause, cause2,
                    indicaciones, idReceta, DetalleReceta.DETALLE_ADICIONADO_STATUS);
            apartamientoImpl.agregarApartado(surtido, detalleProducto, temp.getId(), Apartado.POR_RECETA_TIPO);
            surtirReceta(temp, surtido);
        }

        //Busca si quedaron claves sin surtir.
        rs.close();
        ps.close();

        ps = con.getConn().prepareStatement(OBTENER_CLAVES_PENDIENTES_QUERY);
        ps.setInt(1, idReceta);

        rs = ps.executeQuery();

        tienePendiente = tienePendiente || rs.isBeforeFirst();

        if (tienePendiente) {
            return;
        }

        cambiarTransitoReceta(idReceta, Receta.SURTIDA_TRANSITO);
    }

    @Override
    public List<DetalleCapturado> reconstruirDetalles(int receta) throws SQLException {
        List<DetalleCapturado> detalles = new ArrayList<>();

        PreparedStatement ps;

        ps = con.getConn().prepareStatement(OBTENER_DETALLES_RECETA_QUERY);
        ps.setInt(1, receta);

        ResultSet rs;

        rs = ps.executeQuery();
        DetalleReceta aux;
        DetalleProducto producto;
        while (rs.next()) {

            aux = new DetalleReceta();
            aux.setId(rs.getInt("id"));
            aux.setCantidadSolicitada(rs.getInt("solicitado"));
            aux.setCantidadSurtida(rs.getInt("surtida"));
            aux.setCausePrimario(rs.getString("cause"));
            aux.setCauseSegundario(rs.getString("cause2"));
            aux.setIndicaciones(rs.getString("indicaciones"));
            aux.setDetalleProducto(rs.getInt("producto"));
            aux.setIdReceta(receta);

            producto = new DetalleProducto();
            producto.setClave(rs.getString("clave"));
            producto.setLote(rs.getString("lote"));
            producto.setCaducidad(rs.getString("caducidad"));

            detalles.add(new DetalleCapturado(producto, aux, rs.getString("descripcion")));
        }

        return detalles;
    }

    @Override
    public boolean validarFolio(String folio) throws SQLException {
        PreparedStatement ps;
        ResultSet rs;
        boolean existe;
        ps = con.getConn().prepareStatement(OBTENER_ENCABEZADO_FOLIO_QUERY,
                ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ps.setString(1, folio);

        //Verifica la existencia de ese folio en otra receta.
        rs = ps.executeQuery();
        existe = rs.isBeforeFirst();

        rs.close();
        ps.close();

        return existe;
    }

    /**
     * Obtiene el folio actual del médico para captura "Médico-Farmacia"
     *
     * @param medico id de este.
     * @return
     * @throws SQLException
     */
    public String obtenerFolioMedico(int medico) throws SQLException {

        String folio, prefijo;
        int actual, maximo;
        boolean existe = true;

        PreparedStatement ps;
        ResultSet rs;

        ps = con.getConn().prepareStatement(OBTENER_FOLIO_MEDICO_QUERY,
                ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ps.setInt(1, medico);

        //Verifica la existencia de ese folio en otra receta.
        rs = ps.executeQuery();
        rs.next();
        folio = rs.getString("folio");
        prefijo = rs.getString("prefijo");
        actual = rs.getInt("actual");
        maximo = rs.getInt("maximo");

        rs.close();
        ps.close();

        //Busca hasta encontrar un folio disponible
        while (existe) {
            actual++;
            if (actual > maximo) {
                throw new SQLException(String.format("Médico sin folio disponibles.| usuario: %d ", medico));
            }
            folio = String.format("%s%d", prefijo, actual);
            existe = validarFolio(folio);
        }

        return folio;
    }

    /**
     * Obtiene el médico asociado a el id del usuario que esta capturando la
     * receta.
     *
     * @return el médico.
     */
    public Medico obtenerMedicoAsociado() throws SQLException {
        Medico actual = new Medico();
        PreparedStatement ps;
        ResultSet rs;

        ps = con.getConn().prepareStatement(OBTENER_MEDICO_QUERY);
        ps.setInt(1, this.idUsuario);

        rs = ps.executeQuery();
        rs.next();

        actual.setFolioActual(rs.getInt("actual"));
        actual.setFolioFinal(rs.getInt("maximo"));
        actual.setNombreCompleto(rs.getString("nombres"));
        actual.setNombre(rs.getString("nombre"));
        actual.setApellidoPaterno(rs.getString("paterno"));
        actual.setApellidoMaterno(rs.getString("materno"));
        actual.setUnidad(rs.getString("unidad"));
        actual.setId(rs.getInt("id"));
        actual.setTipoConsulta(rs.getString("tipoConsulta"));
//        TODO: Faltan añadir campos de la tabla.

        rs.close();
        ps.close();

        return actual;
    }

    /**
     * Actualiza el folio actual del medico
     *
     * @param idReceta de la ultima receta capturada.
     */
    public void actualizarFolioMedico(int idReceta) throws SQLException {
        PreparedStatement ps, psActualizar;
        ResultSet rs;

        ps = con.getConn().prepareStatement(OBTENER_NUEVO_FOLIO_MEDICO_QUERY,
                ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ps.setInt(1, idReceta);

        //Obtiene el último folio usado.
        rs = ps.executeQuery();
        rs.next();

        int folio = rs.getInt("folio");
        int medico = rs.getInt("medico");
        boolean ok;

        psActualizar = con.getConn().prepareStatement(ACTUALIZAR_FOLIO_MEDICO_QUERY);
        psActualizar.setInt(1, folio);
        psActualizar.setInt(2, medico);

        ok = psActualizar.executeUpdate() > 0;
        if (!ok) {
            ps.close();
            String mensajeError = String.format("NO se pudo actualizar el folio usado por el médico.| id_detReceta: %d | diferencia: %d ",
                    idReceta, folio);
            throw new SQLException(mensajeError);
        }
        psActualizar.close();

        rs.close();
        ps.close();
    }

    @Override
    public boolean validarMensaje(int idReceta) throws SQLException {
        PreparedStatement ps;
        ResultSet rs;
        boolean existe;
        ps = con.getConn().prepareStatement(OBTENER_ENCABEZADO_ID_QUERY,
                ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ps.setInt(1, idReceta);

        //Verifica la existencia de ese folio en otra receta.
        rs = ps.executeQuery();
        existe = rs.isBeforeFirst();

        rs.close();
        ps.close();

        return existe;
    }

    /**
     * Actualiza el encabeza cuando la receta fue caputra N meses antes. En el
     * modelo de Receta se definen los meses necesarios para considerar caduca.
     *
     * @throws SQLException Cuando no puede actualizar el encabezado.
     */
    public void actualizarCaducas() throws SQLException {
        PreparedStatement ps;
        ResultSet rs;

        ps = con.getConn().prepareStatement(OBTENER_RECETAS_CADUCAS_QUERY,
                ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ps.setInt(1, Receta.ACTIVA_BAJA);
        ps.setInt(2, Receta.CADUCA_TRANSITO);
        ps.setInt(3, Receta.MESES_PARA_CADUCO);

        //Verifica la existencia de ese folio en otra receta.
        rs = ps.executeQuery();

        while (rs.next()) {
            cambiarTransitoReceta(rs.getInt("id"), Receta.CADUCA_TRANSITO);
        }

        rs.close();
        ps.close();

    }

    public void actualizarNotificacionHL7(int idReceta) throws SQLException {
        PreparedStatement ps;

        ps = con.getConn().prepareStatement(ACTUALIZAR_ESTADO_NOTIFICACION_QUERY);
        ps.setInt(1, Receta.NOTIFICADO_HL7_NOTIFICACION);
        ps.setInt(2, idReceta);

        boolean ok = ps.executeUpdate() > 0;
        if (!ok) {
            ps.close();
            String mensajeError = String.format("NO se pudo actualizar el estado de notificacion HL7.| id_detReceta: %d ",
                    idReceta);
            throw new SQLException(mensajeError);
        }

        ps.close();
    }
}
