/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Impl;

import Clases.ConectionDB;
import Dao.solSurDao;
import Modelos.solSur;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Reporte solicitado y surtido
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class solSurDaoImpl implements solSurDao {

    // Se preparan las variables de conexión a base de datos.
    ConectionDB con = new ConectionDB();
    PreparedStatement ps;
    String FolioRec = "";
    ResultSet rs;
    String query;

    @Override
    public List<solSur> datos(String f1, String f2) {
        // se inicializa la variable lista que será retornada al servlet.
        List<solSur> ls = new ArrayList<>();
        // Se prepara la consulta sql en un String.
        query = "select id_rec, fol_rec, cla_pro,des_pro,SUM(can_sol) as can_sol, SUM(cant_sur) as cant_sur from repsolsur where DATE(fecha_hora) between ? and ? and can_sol!=0 group by id_rec, cla_pro order by id_rec asc;";
        try {
            //Se abre la conexión a base de datos y se mandan las variables correspondientes
            // para la consulta.            
            con.conectar();
            ps = con.getConn().prepareStatement(query);
            ps.setString(1, f1);
            ps.setString(2, f2);
            rs = ps.executeQuery();
            while (rs.next()) {
                FolioRec = rs.getString(2);
                if (FolioRec.contains("HL7")) {
                    FolioRec = FolioRec.substring(3);
                }
                //Se instancia el modelo para su llenado de datos provenientes de la consulta.
                solSur s = new solSur();
                s.setIdRec(rs.getInt("id_rec"));
                s.setFolio(FolioRec);
                s.setClave(rs.getString("cla_pro"));
                s.setDescrip(rs.getString("des_pro"));
                s.setSol(rs.getInt("can_sol"));
                s.setSur(rs.getInt("cant_sur"));
                // Se agrega el modelo a la lista.
                ls.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(solSurDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(solSurDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return ls;
    }

}
