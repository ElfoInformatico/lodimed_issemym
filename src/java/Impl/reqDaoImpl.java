/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Impl;

import Clases.ConectionDB;
import Dao.reqDao;
import Modelos.reabModel;
import java.util.ArrayList;
import java.util.List;
import com.csvreader.CsvWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implementación de requerimiento de la unidad
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class reqDaoImpl implements reqDao {

    ConectionDB con = new ConectionDB();
    PreparedStatement ps;
    ResultSet rs;
    String query;

    @Override
    public reabModel totPz() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void generaCsv(String ruta) {

        List<reabModel> req = new ArrayList<reabModel>();

        this.query = "SELECT c.clave_unidad,c.clave, p.des_pro,c.cant,c.observaciones,c.id,c.cargado  FROM carga_requerimiento c, productos p WHERE c.clave=p.cla_pro  ORDER BY c.clave ASC ;";
        try {
            con.conectar();
            ps = con.getConn().prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                reabModel reqModel = new reabModel();
                reqModel.setCu(rs.getString(1));
                reqModel.setClaveInsumo(rs.getString(2));
                reqModel.setDescrip(rs.getString(3));
                reqModel.setCantidad(rs.getInt(4));
                reqModel.setObs(rs.getString(5));
                req.add(reqModel);

            }

        } catch (SQLException ex) {
            Logger.getLogger(AjustesImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(AjustesImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        String outputFile = ruta;
        boolean alreadyExists = new File(outputFile).exists();

        if (alreadyExists) {
            File ficheroUsuarios = new File(outputFile);
            ficheroUsuarios.delete();
        }

        try {

            CsvWriter csvOutput = new CsvWriter(new FileWriter(outputFile, true), ',');

            csvOutput.write("Clave de la unidad");
            csvOutput.write("Clave");
            csvOutput.write("Descripcion");
            csvOutput.write("Cantidad");
            csvOutput.write("Obsercaciones");

            csvOutput.endRecord();

            for (reabModel us : req) {

                csvOutput.write(us.getCu());
                csvOutput.write(us.getClaveInsumo());
                csvOutput.write(us.getDescrip());
                csvOutput.write(Integer.toString(us.getCantidad()));
                csvOutput.write(us.getObs());
                csvOutput.endRecord();
            }

            csvOutput.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
