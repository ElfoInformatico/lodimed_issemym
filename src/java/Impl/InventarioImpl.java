/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Impl;

import Clases.ConectionDB;
import Dao.InventarioDao;
import Modelos.Apartado;
import Modelos.Inventario;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementación para actualizar inventario
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class InventarioImpl implements InventarioDao {

    ConectionDB con;
    PreparedStatement ps;
    ResultSet rs;
    //INSERT INTO kardex set id_rec=?,det_pro=?,cant=?,tipo_mov=?,fol_aba=?,fecha=NOW(),obser=?,id_usu=?,web=0;
    String UPDATE_INVENTARIO_QUERY = "UPDATE inventario SET cant=? WHERE id_inv=?";
    String INSERT_INVENTARIO_QUERY = "INSERT INTO `inventario` SET `fec_ela`=CURDATE(), `cla_uni`=?, `det_pro`=?, `cant`=?, `web`='0';";
    String OBTENER_UNIDAD_ACTUAL_QUERY = "SELECT u.cla_uni AS unidad FROM usuarios AS u WHERE u.`user` = 'admon';";
    public static final String OBTENER_EXISTENCIAS_POR_CLAVE_QUERY = "SELECT dp.det_pro AS id, ( IFNULL(i.inventario, 0) - IFNULL(a.apartado, 0)) AS existencia FROM detalle_productos AS dp INNER JOIN ( SELECT inv.det_pro AS producto, SUM(inv.cant) AS inventario FROM inventario AS inv WHERE inv.cant > 0 GROUP BY inv.det_pro ) AS i ON i.producto = dp.det_pro LEFT JOIN ( SELECT ap.detalle_producto AS producto, SUM(ap.cantidad) AS apartado FROM apartamiento AS ap USE INDEX (producto_status) WHERE ap.`status` = ? GROUP BY ap.detalle_producto ) AS a ON a.producto = dp.det_pro WHERE dp.cla_pro = ? AND DATEDIFF(DATE(dp.cad_pro),now()) > 0 ORDER BY dp.cad_pro ASC, dp.id_ori ASC, existencia ASC FOR UPDATE;";

    public InventarioImpl(ConectionDB con) {
        this.con = con;
    }

    public InventarioImpl() {
    }

    public ConectionDB getCon() {
        return con;
    }

    public void setCon(ConectionDB con) {
        this.con = con;
    }

    @Override
    public boolean actualizaInventario(int idInventario, int nuevaExistencia) throws SQLException {

        ps = con.getConn().prepareStatement(UPDATE_INVENTARIO_QUERY);
        ps.setInt(1, nuevaExistencia);
        ps.setInt(2, idInventario);

        boolean ok = ps.executeUpdate() > 0;
        ps.close();

        return ok;
    }

    @Override
    public int agregaLote(int detalleProducto, int cantidad) throws SQLException {

        ps = con.getConn().prepareStatement(OBTENER_UNIDAD_ACTUAL_QUERY);
        rs = ps.executeQuery();
        rs.next();

        //int unidad = rs.getInt("unidad");
        String unidad = rs.getString("unidad");
        rs.close();
        ps.close();

        ps = con.getConn().prepareStatement(INSERT_INVENTARIO_QUERY,
                PreparedStatement.RETURN_GENERATED_KEYS);
        ps.setString(1, unidad);
        ps.setInt(2, detalleProducto);
        ps.setInt(3, cantidad);

        boolean ok = ps.executeUpdate() > 0;
        if (!ok) {
            ps.close();
            String mensajeError = String.format("NO se pudo inserta el inventario.| detalle_producto: %d | cantidad: %d ", detalleProducto, cantidad);
            throw new SQLException(mensajeError);
        }

        rs = ps.getGeneratedKeys();
        rs.next();

        int id = rs.getInt(1);
        rs.close();
        ps.close();

        return id;
    }

    /**
     * Regresa los detalles productos que tienen existencias disponibles.
     *
     * @param clave
     * @return lista con los detalles productos y la cantidad.
     * @throws SQLException
     */
    @Override
    public List<Inventario> obtenerDisponibles(String clave) throws SQLException {

        ps = con.getConn().prepareStatement(OBTENER_EXISTENCIAS_POR_CLAVE_QUERY,
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
        ps.setInt(1, Apartado.APARTADO_STATUS);
        ps.setString(2, clave);

        rs = ps.executeQuery();
        List<Inventario> resultado = new ArrayList<>();
        while (rs.next()) {
            resultado.add(new Inventario("0", "0", 0, rs.getInt("existencia"), rs.getInt("id")));
        }

        rs.close();
        ps.close();

        return resultado;
    }

}
