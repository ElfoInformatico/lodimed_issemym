/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Impl;

import Clases.ConectionDB;
import Dao.entradasDao;
import Modelos.registro;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Se obtiene lista de ingreso de los usuarios al sistema
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class entradasDaoImpl implements entradasDao {

    private final ConectionDB con = new ConectionDB();
    private PreparedStatement ps;
    private ResultSet rs;
    private String query;

    @Override
    public List<registro> lR() {
        List<registro> lr = new ArrayList<>();

        try {
            query = "SELECT user AS usuario,DATE_FORMAT(fecha, '%d/%m/%Y') AS fecha, DATE_FORMAT(fecha, '%H:%i:%s')  AS hora, status AS estado  FROM registro_entradas ORDER BY fecha ASC";
            con.conectar();
            ps = con.getConn().prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                registro r = new registro();
                r.setUser(rs.getString("usuario"));
                r.setFecha(rs.getString("fecha"));
                r.setHora(rs.getString("hora"));
                r.setSts(rs.getInt("estado"));
                if (r.getSts() == 1) {
                    r.setEstatus("Correcto");
                } else {
                    r.setEstatus("Error");
                }
                lr.add(r);

            }

        } catch (SQLException ex) {
            Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }

        return lr;
    }

}
