/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Impl;

import Clases.ConectionDB;
import Dao.AjusteDao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Implementación AjusteDao busca de datos del medicamento
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class AjustesImpl implements AjusteDao {

    ConectionDB con = new ConectionDB();
    PreparedStatement ps;
    ResultSet rs;
    String query;

    @Override
    public JSONObject buscarInsumo(String claveProducto, String cb) {
        JSONObject jsonO = new JSONObject();
        try {
            con.conectar();
            query = "SELECT cla_pro, des_pro FROM productos p, tb_codigob cb WHERE p.cla_pro = cb.F_Clave AND (p.cla_pro = ? OR cb.F_Cb=?) GROUP BY cla_pro";
            ps = con.getConn().prepareStatement(query);
            ps.setString(1, claveProducto);
            ps.setString(2, cb);
            rs = ps.executeQuery();
            if (rs.next()) {
                jsonO.put("clave", rs.getString("cla_pro"));
                jsonO.put("descripcion", rs.getString("des_pro"));
                jsonO.put("msg", "ok");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AjustesImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(AjustesImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return jsonO;
    }

    @Override
    public JSONArray traerOrigen(String clave, String claveUnidad) {
        JSONObject jsonO = new JSONObject();
        JSONArray jsonA = new JSONArray();

        try {
            con.conectar();
            query = "SELECT origen.id_ori,origen.des_ori FROM existencias,origen WHERE cla_pro = ? and cla_uni=? AND origen.id_ori=existencias.id_ori group by existencias.id_ori order by existencias.id_ori";
            ps = con.getConn().prepareStatement(query);
            ps.setString(1, clave);
            ps.setString(2, claveUnidad);
            rs = ps.executeQuery();
            while (rs.next()) {
                jsonO.put("origen", rs.getString("id_ori"));
                jsonO.put("desc", rs.getString("des_ori"));
                jsonO.put("msg", "ok");
                jsonA.add(jsonO);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AjustesImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(AjustesImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return jsonA;
    }

    @Override
    public JSONArray traerCaducidad(String clave) {
        JSONObject jsonO = new JSONObject();
        JSONArray jsonA = new JSONArray();

        try {
            con.conectar();
            query = "SELECT cad_pro FROM detalle_productos WHERE cla_pro = ? ";
            ps = con.getConn().prepareStatement(query);
            ps.setString(1, clave);
            rs = ps.executeQuery();
            while (rs.next()) {
                jsonO.put("caducidad", rs.getString("cad_pro"));
                jsonO.put("msg", "ok");
                jsonA.add(jsonO);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AjustesImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(AjustesImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return jsonA;
    }

    @Override
    public JSONArray traerLotes(String clave) {
        JSONObject jsonO = new JSONObject();
        JSONArray jsonA = new JSONArray();

        try {
            con.conectar();
            query = "SELECT lot_pro FROM detalle_productos WHERE cla_pro = ? ";
            ps = con.getConn().prepareStatement(query);
            ps.setString(1, clave);
            rs = ps.executeQuery();
            while (rs.next()) {
                jsonO.put("lote", rs.getString("lot_pro"));
                jsonO.put("msg", "ok");
                jsonA.add(jsonO);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AjustesImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(AjustesImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return jsonA;
    }

    @Override
    public JSONObject seleccionar(String claveProducto, String lote, String caducidad, String idOrigen) {
        JSONObject jsonO = new JSONObject();
        try {
            con.conectar();
            query = "SELECT ex.cant, ex.id_inv, o.des_ori FROM existencias AS ex, origen AS o WHERE o.id_ori = ex.id_ori AND ex.cla_pro = ? AND ex.lot_pro = ? AND ex.cad_pro = ? AND o.id_ori = ? ";
            ps = con.getConn().prepareStatement(query);
            ps.setString(1, claveProducto);
            ps.setString(2, lote);
            ps.setString(3, caducidad);
            ps.setString(4, idOrigen);
            rs = ps.executeQuery();
            if (rs.next()) {
                jsonO.put("cantidadExistente", rs.getString("cant"));
                jsonO.put("id_inv", rs.getString(2));
                jsonO.put("msg", "ok");
                jsonO.put("lote", lote);
                jsonO.put("caducidad", caducidad);
                jsonO.put("origen", rs.getString("des_ori"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AjustesImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(AjustesImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return jsonO;
    }

    @Override
    public String traerClaveUnidad(String usuario) {
        String claveUnidad = null;
        try {
            con.conectar();
            query = "SELECT cla_uni FROM usuarios WHERE id_usu = ? ";
            ps = con.getConn().prepareStatement(query);
            ps.setString(1, usuario);
            rs = ps.executeQuery();
            if (rs.next()) {
                claveUnidad = rs.getString("cla_uni");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AjustesImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(AjustesImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return claveUnidad;
    }

}
