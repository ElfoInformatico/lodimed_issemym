/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Impl;

import Clases.ConectionDB;
import Dao.operacionesDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Actualización de los indices parametros
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class operacionesImpl implements operacionesDao {

    ConectionDB con = new ConectionDB();

    //Se obtiene el rango de los obtenerProximoRango asignados automaticamente para los médicos
    @Override
    public int folRan() {
        int rango = 0;
        try {
            con.conectar();
            String qry = "SELECT paramFol FROM indices";
            ResultSet rs = con.consulta(qry);
            while (rs.next()) {
                rango = rs.getInt("paramFol");
            }
        } catch (SQLException ex) {
            Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }

        return rango;
    }

    //Se modifica el rango de obtenerProximoRango para los médicos
    @Override
    public boolean newRang(int rango) {
        boolean update = false;
        try {
            con.conectar();
            con.actualizar("UPDATE indices SET paramFol=" + rango + "");
            update = true;
        } catch (SQLException ex) {
            Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }

        return update;
    }

    // Se detarmina el tipo de sistema.
    @Override
    public boolean tipFarm(int tipSis) {
        boolean update = false;
        try {
            con.conectar();
            con.actualizar("UPDATE indices SET tipRec=" + tipSis + "");
            update = true;

        } catch (SQLException ex) {
            Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }
        return update;
    }

    @Override
    public int tipSis() {
        int sis = 0;

        try {
            con.conectar();
            ResultSet rs = con.consulta("SELECT tipRec FROM indices;");
            while (rs.next()) {
                sis = rs.getInt("tipRec");
            }

        } catch (SQLException ex) {
            Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }

        return sis;
    }

    @Override
    public String prefijo() {
        String pref = "";

        try {
            con.conectar();
            ResultSet rs = con.consulta("SELECT paramRec FROM indices;");
            while (rs.next()) {
                pref = rs.getString("paramRec");
            }

        } catch (SQLException ex) {
            Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }

        return pref;
    }

    @Override
    public boolean updatePrefijo(String pre) {
        boolean actualizar = false;

        try {
            con.conectar();
            con.insertar("UPDATE indices SET paramRec='" + pre + "';");
            actualizar = true;

        } catch (SQLException ex) {
            Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }
        return actualizar;
    }

    @Override
    public int frecuencia(String cu) {
        int frec = 0;

        try {
            con.conectar();
            ResultSet rs = con.consulta("SELECT frecuenciaDePaso FROM unidades WHERE cla_uni='" + cu + "';");
            while (rs.next()) {
                frec = rs.getInt("frecuenciaDePaso");
            }

        } catch (SQLException ex) {
            Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }

        return frec;
    }

    @Override
    public boolean updateFrecuencia(int frecuencia, String cu) {
        boolean actualizar = false;

        try {
            con.conectar();
            con.insertar("UPDATE unidades SET frecuenciaDePaso=" + frecuencia + " WHERE cla_uni='" + cu + "' ;");
            actualizar = true;

        } catch (SQLException ex) {
            Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }
        return actualizar;
    }

    @Override
    public boolean tipCaptura(int tipCap) {
        boolean update = false;
        try {
            con.conectar();
            con.actualizar("UPDATE indices SET tipoCaptura=" + tipCap + "");
            update = true;

        } catch (SQLException ex) {
            Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }
        return update;
    }

    @Override
    public int tipCap() {
        int cap = 0;

        try {
            con.conectar();
            ResultSet rs = con.consulta("SELECT tipoCaptura FROM indices;");
            while (rs.next()) {
                cap = rs.getInt("tipoCaptura");
            }

        } catch (SQLException ex) {
            Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }
        return cap;
    }

}
