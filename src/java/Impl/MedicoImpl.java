package Impl;

import Modelos.Medico;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Permite realizar el inserción, búsqueda de médicos.
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class MedicoImpl {

    private final Connection con;
    private final String formatoFecha;

    public static final String OBTENER_MEDICO_QUERY = "SELECT m.cedula AS id, m.nom_med AS nombre, m.ape_pat AS paterno, m.ape_mat AS materno, m.nom_com AS nombre_completo, m.rfc, m.clauni AS unidad, m.cedulapro AS cedula, m.f_status AS `status`, DATE_FORMAT(m.fecnac ,?) AS nacimiento, m.iniRec AS inicial, m.finRec AS final, m.folAct AS actual, m.tipoConsulta FROM medicos AS m WHERE %s";
    public static final String OBTENER_SIGUIENTE_FOLIO_QUERY = "SELECT IFNULL(m.folio,0) + 1 AS inicial, IFNULL(m.folio,0) + i.paramFol AS final FROM indices AS i, ( SELECT MAX(finRec) AS folio FROM medicos WHERE ape_pat <> 'RECETA' ) AS m;";
    public static final String INSERTAR_MEDICO_QUERY = "INSERT INTO medicos SET cedula = ?, nom_med = ?, ape_pat = ?, ape_mat = ?, nom_com = ?, rfc = ?, clauni = ?, cedulapro = ?, f_status = ?, fecnac = STR_TO_DATE(?, ?), iniRec = ?, finRec = ?, folAct = ?, tipoConsulta = ?, web = ?;";

    public MedicoImpl(Connection con) {
        this.con = con;
        this.formatoFecha = "%d/%m/%Y";
    }

    public MedicoImpl(Connection con, String formatoFecha) {
        this.con = con;
        this.formatoFecha = formatoFecha;
    }

    public Medico reconstruirMedico(int id) throws SQLException {
        Medico medico;
        try (PreparedStatement ps = con.prepareStatement(String.format(OBTENER_MEDICO_QUERY, "m.cedula = ?"))) {
            ps.setString(1, formatoFecha);
            ps.setInt(2, id);
            ResultSet rs = ps.executeQuery();
            if (!rs.isBeforeFirst()) {
                return null;
            }
            rs.next();
            medico = reconstruirMedico(rs);
            rs.close();
        }

        return medico;
    }

    public Medico reconstruirMedico(String cedula) throws SQLException {

        Medico medico;
        try (PreparedStatement ps = con.prepareStatement(String.format(OBTENER_MEDICO_QUERY, "m.cedulapro = ?"))) {
            ps.setString(1, formatoFecha);
            ps.setString(2, cedula);
            ResultSet rs = ps.executeQuery();
            if (!rs.isBeforeFirst()) {
                return null;
            }
            rs.next();
            medico = reconstruirMedico(rs);
            rs.close();
        }

        return medico;

    }

    public Medico reconstruirMedico(String nombre, String materno, String paterno) throws SQLException {

        Medico medico = new Medico();
        medico.setNombreCompleto(nombre, paterno, materno);

        try (PreparedStatement ps = con.prepareStatement(String.format(OBTENER_MEDICO_QUERY, "m.nom_com = ?"))) {
            ps.setString(1, formatoFecha);
            ps.setString(2, medico.getNombreCompleto());
            ResultSet rs = ps.executeQuery();
            if (!rs.isBeforeFirst()) {
                return null;
            }
            rs.next();
            medico = reconstruirMedico(rs);
            rs.close();
        }

        return medico;

    }

    public int adicionarMedico(Medico medico) throws SQLException {
        int idGenerado;

        try (PreparedStatement ps = con.prepareStatement(INSERTAR_MEDICO_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            ps.setInt(1, medico.getId());
            ps.setString(2, medico.getNombre());
            ps.setString(3, medico.getApellidoPaterno());
            ps.setString(4, medico.getApellidoMaterno());
            ps.setString(5, medico.getNombreCompleto());
            ps.setString(6, medico.getRfc());
            ps.setString(7, medico.getUnidad());
            ps.setString(8, medico.getCedula());
            ps.setString(9, medico.getStatus());
            ps.setString(10, medico.getNacimiento());
            ps.setString(11, medico.getFormatoFecha());
            ps.setInt(12, medico.getFolioInicial());
            ps.setInt(13, medico.getFolioFinal());
            ps.setInt(14, medico.getFolioActual());
            ps.setString(15, medico.getTipoConsulta());
            ps.setInt(16, medico.getWeb());

            int affectedRows = ps.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creación de médico fallido, no se afecto ninguna fila en la insercción.");
            }

            ResultSet generatedKeys = ps.getGeneratedKeys();
            generatedKeys.next();
            idGenerado = generatedKeys.getInt(1);
        }

        return idGenerado;
    }

    private Medico reconstruirMedico(ResultSet rs) throws SQLException {
        Medico medico = new Medico();

        medico.setId(rs.getInt("id"));
        medico.setNombre(rs.getString("nombre"));
        medico.setApellidoPaterno(rs.getString("paterno"));
        medico.setApellidoMaterno(rs.getString("materno"));
        medico.setRfc(rs.getString("rfc"));
        medico.setUnidad(rs.getString("unidad"));
        medico.setCedula(rs.getString("cedula"));
        medico.setStatus(rs.getString("status"));
        medico.setNacimiento(rs.getString("nacimiento"));
        medico.setFolioInicial(rs.getInt("inicial"));
        medico.setFolioFinal(rs.getInt("final"));
        medico.setFolioActual(rs.getInt("actual"));
        medico.setTipoConsulta(rs.getString("tipoConsulta"));

        return medico;
    }

    public Medico obtenerProximoRango() throws SQLException {

        Medico m;
        try (Statement st = con.createStatement(); ResultSet rs = st.executeQuery(OBTENER_SIGUIENTE_FOLIO_QUERY)) {
            rs.next();
            m = new Medico();
            m.setFolioInicial(rs.getInt("inicial"));
            m.setFolioFinal(rs.getInt("final"));
        }

        return m;
    }
}
