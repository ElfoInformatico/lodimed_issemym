/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Admin;

import Autocomplete.AutoMedico;
import Clases.ConectionDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 * Consulta registros de ingreso al sistema por usuario
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class EstadoSistema extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        ConectionDB con = new ConectionDB();
        JSONObject json = new JSONObject();

        PreparedStatement ps;

        try {
            con.conectar();
            ps = con.getConn().prepareStatement("SELECT reincio_sistema, seleccion_unidad, cargo_servicios, cargo_cdm FROM indices;");
            ResultSet rset2 = ps.executeQuery();
            while (rset2.next()) {
                json.put("sistema", rset2.getInt("reincio_sistema") == 1);
                json.put("unidad", rset2.getInt("seleccion_unidad") == 1);
                json.put("servicio", rset2.getInt("cargo_servicios") == 1);
                json.put("cdm", rset2.getInt("cargo_cdm") == 1);
            }

            //Solo busca usuario farmacia.
            ps = con.getConn().prepareStatement("SELECT COUNT(id_usu) AS cantidad FROM usuarios WHERE `user` <> 'admon' AND rol = 1;");
            rset2 = ps.executeQuery();
            while (rset2.next()) {
                json.put("usuario", rset2.getInt("cantidad") > 0);
            }
            out.println(json);

        } catch (SQLException e) {
            Logger.getLogger(AutoMedico.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", e.getMessage(), e.getSQLState()), e);
        } finally {
            out.close();
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(AutoMedico.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }

    }


}
