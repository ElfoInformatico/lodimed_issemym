package Admin;

import Clases.ConectionDB;
import Impl.MedicoImpl;
import Impl.PacienteDaoImpl;
import Modelos.Medico;
import Modelos.Paciente;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Reinicio de los registro de las tablas
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
@WebServlet(name = "administrarSistema", urlPatterns = {"/administrarSistema"})
public class administrarSistema extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        ConectionDB con = new ConectionDB();
        HttpSession sesion = request.getSession();
        String nivel = (String) sesion.getAttribute("nivel");
        try {
            try {
                con.conectar();
                switch (request.getParameter("accion")) {
                    case "actualizarFolio":
                        try {
                            con.insertar("update indices set id_rec = '" + request.getParameter("id_rec") + "'");
                            out.println("<script>alert('Folio Actualizado Correctamente')</script>");
                        } catch (Exception e) {
                            Logger.getLogger(administrarSistema.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                            out.println("<script>alert('Error al actualizar folio:" + e.getMessage() + "')</script>");
                        }
                        out.println("<script>window.location='admin/recetas/folio.jsp'</script>");
                        break;
                    case "reiniciarExistenciasYRecetas":
                        try {
                            //Reinicio de tablas principales del sistema
                            Statement statement = con.getConn().createStatement();
                            statement.addBatch("SET FOREIGN_KEY_CHECKS = 0");
                            statement.addBatch("TRUNCATE TABLE  apartamiento");
                            statement.addBatch("TRUNCATE TABLE  detreceta");
                            statement.addBatch("TRUNCATE TABLE  receta");
                            statement.addBatch("TRUNCATE TABLE  detalle_productos");
                            statement.addBatch("TRUNCATE TABLE  kardex");
                            statement.addBatch("TRUNCATE TABLE  inventario");
                            statement.addBatch("TRUNCATE TABLE  inventario_inicial");
                            statement.addBatch("TRUNCATE TABLE  inventariociclico");
                            statement.addBatch("TRUNCATE TABLE  inventariociclicohistorico");
                            statement.addBatch("TRUNCATE TABLE  indices");
                            statement.addBatch("TRUNCATE TABLE  reporte_abasto");
                            statement.addBatch("TRUNCATE TABLE  detalle_productos");
                            statement.addBatch("TRUNCATE TABLE  medicos");
                            statement.addBatch("TRUNCATE TABLE  registro_entradas");
                            statement.addBatch("TRUNCATE TABLE  usuarios");
                            statement.addBatch("TRUNCATE TABLE  carga_abasto");
                            statement.addBatch("TRUNCATE TABLE  tb_codigob");
                            statement.addBatch("TRUNCATE TABLE  servicios");
                            statement.addBatch("TRUNCATE TABLE  pacientes");
                            statement.addBatch("TRUNCATE TABLE  tbcdm");
                            statement.addBatch("TRUNCATE TABLE  mensaje_hl7");
                            statement.addBatch("TRUNCATE TABLE  receta_hl7_log");
                            statement.addBatch("SET FOREIGN_KEY_CHECKS = 1");
                            statement.executeBatch();
                            statement.close();

                            con.insertar("INSERT INTO `indices` ( `id_rec`, `paramFol`, `tipRec`, `reincio_sistema`, `seleccion_unidad`, `cargo_servicios`, `cargo_cdm`, `paramRec`, `inicio_jornada`, `fin_jornada`, `numero_por_dia`, `maximo_dias`, `ciclicos`, `tipoCaptura` ) VALUES (1,1000,1,1,0,0,0,'E',NOW(),NOW(),3,5,0,2);");
                            con.insertar("INSERT INTO usuarios VALUES (0, 'admon', 'admon', MD5('#M4$t3Rs018'), 0, 1,'0001','-','-','admon','admon', 'admon admon admon');");

                            Medico medico = new Medico("RECETA", "RECETA", "COLECTIVA",
                                    "RECETA COLECTIVA", 1, 1, "12452D", "0001", "123AS", Medico.SUSPENDIDO_STATUS,
                                    "00/00/0000", 1, 10000000, 0, "");
                            medico.setFormatoFecha("%d/%m/%Y");

                            MedicoImpl dao = new MedicoImpl(con.getConn());
                            int idMedico = dao.adicionarMedico(medico);

                            Paciente paciente = new Paciente(0, "RECETA", "COLECTIVA", "RECETA COLECTIVA",
                                    "RECETA COLECTIVA", "01-01-1987", "M", "", "PA", "01-01-2015",
                                    "31-12-2025", "S/E", Paciente.SUSPENDIDO_STATUS, Paciente.SIN_ID_HL7);
                            paciente.setDireccion("0-1-1-1");

                            PacienteDaoImpl daoPaciente = new PacienteDaoImpl(con.getConn());
                            long idPaciente = daoPaciente.adicionarPaciente(paciente);

                            //NO se usa implementación porque es un hack, TODO: Corregir.
                            con.insertar(String.format("INSERT INTO receta ( fol_rec, id_pac, cedula, id_tip, id_usu, fecha_hora ) VALUES (0, %d, %d, 2, 1, NOW());",
                                    idPaciente, idMedico));

                            con.insertar("INSERT INTO servicios VALUES (0,'-')");

                            //Rutina para borrar las imagenes de las firmas de los médicos
                            String ruta = getServletContext().getRealPath("/reportes/imagen/firmas/");
                            File file = new File(ruta);
                            String[] contenido = file.list();
                            if (contenido != null) {
                                for (String contenido1 : contenido) {
                                    File fil = new File(ruta + "/" + contenido1);
                                    if (!fil.delete()) {
                                        fil = new File(ruta + "/" + contenido1);
                                    }
                                    if (fil.isDirectory()) {
                                        fil.delete();
                                    }
                                }
                            }

                            out.println("<script>alert('Limpieza correcta del sistema')</script>");
                        } catch (Exception e) {
                            Logger.getLogger(administrarSistema.class.getName()).log(Level.SEVERE, e.getMessage(), e);
                            out.println("<script>alert(\"Error al limpiar:" + e.getMessage() + "\")</script>");
                            out.flush();
                        }
                        out.println("<script>window.location='admin/recetas/folio.jsp'</script>");
                        break;
                    case "uni":
                        try {

                            con.actualizar("UPDATE usuarios SET cla_uni='" + request.getParameter("uni") + "'");
                            sesion.setAttribute("cla_uni", request.getParameter("uni"));

                            ResultSet rs = con.consulta("SELECT nivel FROM unidades WHERE cla_uni='" + request.getParameter("uni") + "'");
                            while (rs.next()) {
                                nivel = rs.getString("nivel");
                            }

                            con.actualizar("UPDATE productos SET f_status ='S' ");
                            con.actualizar("UPDATE indices SET seleccion_unidad = 1;");
                            con.insertar("update productos SET f_status ='A' WHERE cla_pro IN (SELECT DISTINCT clave FROM productosnivel  WHERE  nivel=" + nivel + ");");
                            con.insertar("update productos SET f_status ='S' WHERE cla_pro NOT IN (SELECT DISTINCT clave FROM productosnivel  WHERE  nivel=" + nivel + ");");

                            out.print("<script>alert('Unidad asignada con éxito')</script>");
                            out.print("<script>window.location='admin/recetas/folio.jsp'</script>");
                        } catch (Exception e) {
                            Logger.getLogger(administrarSistema.class.getName()).log(Level.SEVERE, null, e);
                        }
                        break;
                }
            } catch (Exception e) {
                Logger.getLogger(administrarSistema.class.getName()).log(Level.SEVERE, null, e);
                out.println(e.getMessage());
            } finally {
                try {
                    if (con.estaConectada()) {
                        con.cierraConexion();
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(administrarSistema.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                }
            }
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
