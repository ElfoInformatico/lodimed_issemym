/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Admin;

import Clases.ConectionDB;
import Clases.LoggerUsuario;
import Clases.leedServ;
import in.co.sneh.controller.FileUploadServlet;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 * Proceso de carga de servicio
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
@MultipartConfig
public class loadServ extends HttpServlet {

    private static final String SAVE_DIR = "CVS_Abastos";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        leedServ lee = new leedServ();
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession sesion = request.getSession(true);
        String mensaje;

        try {

            out = response.getWriter();

            Part cvs = request.getPart("archivo");
            String rutaServlet = request.getServletContext().getRealPath("");
            String rutaCarpeta = rutaServlet + File.separatorChar + SAVE_DIR;
            String nombreArchivo = cvs.getSubmittedFileName();

            File carpeta = new File(rutaCarpeta);
            if (!carpeta.exists()) {
                carpeta.mkdir();
            }

            String rutaArchvio = rutaCarpeta + File.separatorChar + nombreArchivo;
            cvs.write(rutaArchvio);
            if (lee.leeCSV(rutaArchvio)) {
                sesion.setAttribute("ver", "si");
                sesion.setAttribute("nomArchivo", nombreArchivo);
                ConectionDB con = new ConectionDB();
                PreparedStatement ps;
                int resultado;
                try {
                    con.conectar();
                    ps = con.getConn().prepareStatement("UPDATE indices SET cargo_servicios = 1;");
                    resultado = ps.executeUpdate();
                    if (resultado == 0) {
                        throw new SQLException("No pudo actualizarse el indice al subir los servicios.", ps.toString());
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(loadServ.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                } finally {
                    try {
                        if (con.estaConectada()) {
                            con.cierraConexion();
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(loadServ.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                    }
                }
            } else {
                cvs.delete();
                sesion.setAttribute("ver", "no");
                mensaje = String.format("El archivo cvs contiene errores, por favor revise el log en '%s'", LoggerUsuario.getRutaArchivoActual());
                sesion.setAttribute("info_upload", mensaje);
            }

        } catch (IOException | ServletException ex) {
            sesion.setAttribute("ver", "no");
            sesion.setAttribute("info_upload", "El archivo no puedo subirse al servidor.");
            Logger.getLogger(FileUploadServlet.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            LoggerUsuario.getLoggerUsuario(FileUploadServlet.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }

        out.println("<script>alert('Los servicios se cargaron con éxito')</script>");
        out.println("<script>window.location='admin/servicios/cargaServicios.jsp'</script>");

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
