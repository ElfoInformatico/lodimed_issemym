/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Admin;

import Clases.ConectionDB;
import Clases.LoggerUsuario;
import Clases.loadCdm;
import in.co.sneh.controller.FileUploadServlet;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 * proceso para cargar cdm
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
@MultipartConfig
public class servletCdm extends HttpServlet {

    private static final String SAVE_DIR = "CVS_Abastos";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        loadCdm lee = new loadCdm();
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession sesion = request.getSession(true);
        String mensaje;
        try {
            out = response.getWriter();
            Part cvs = request.getPart("archivo");
            String rutaServlet = request.getServletContext().getRealPath("");
            String rutaCarpeta = rutaServlet + SAVE_DIR;
            String nombreArchivo = cvs.getSubmittedFileName();

            File carpeta = new File(rutaCarpeta);
            if (!carpeta.exists()) {
                carpeta.mkdir();
            }

            String rutaArchvio = rutaCarpeta + File.separatorChar + nombreArchivo;
            cvs.write(rutaArchvio);
            if (lee.leeCSV(rutaArchvio)) {
                sesion.setAttribute("ver", "si");
                sesion.setAttribute("nomArchivo", nombreArchivo);
                out.println("<script>alert('El CDM se cargó con éxito')</script>");
                ConectionDB con = new ConectionDB();
                PreparedStatement ps;
                int resultado;
                try {
                    con.conectar();
                    ps = con.getConn().prepareStatement("UPDATE indices SET cargo_cdm = 1;");
                    resultado = ps.executeUpdate();
                    if (resultado == 0) {
                        throw new SQLException("No pudo actualizarse el indice al subir los servicios.", ps.toString());
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(servletCdm.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                } finally {
                    try {
                        if (con.estaConectada()) {
                            con.cierraConexion();
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(servletCdm.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                    }
                }
            } else {
                cvs.delete();
                sesion.setAttribute("ver", "no");
                out.println("<script>alert('El CDM no se pudo cargar')</script>");
                mensaje = String.format("El archivo cvs contiene errores, por favor revise el log en '%s'", LoggerUsuario.getRutaArchivoActual());
                sesion.setAttribute("info_upload", mensaje);
            }

        } catch (IOException | ServletException ex) {
            sesion.setAttribute("ver", "no");
            sesion.setAttribute("info_upload", "El archivo no puedo subirse al servidor.");
            out.println("<script>window.location='admin/Cdm/cargaCdm.jsp'</script>");
            Logger.getLogger(FileUploadServlet.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            LoggerUsuario.getLoggerUsuario(FileUploadServlet.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }

        out.println("<script>window.location='admin/Cdm/cargaCdm.jsp'</script>");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
