/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package in.co.sneh.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Clases.*;
import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.Part;

/**
 * Proceso de cargar de archivo de abasto
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
@MultipartConfig
public class FileUploadServlet extends HttpServlet {

    LeerCSV lee = new LeerCSV();
    CargaAbasto carga = new CargaAbasto();

    private static final String SAVE_DIR = "CVS_Abastos";
    private static final String SELECT_USER = "SELECT usuarios.id_usu FROM usuarios WHERE usuarios.id_usu = ? AND usuarios.pass = MD5(?)";
    private static final String SELECT_FOLIO = "SELECT k.id_kardex FROM kardex AS k WHERE k.fol_aba = ? GROUP BY k.fol_aba";

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        response.setContentType("text/html");
        HttpSession sesion = request.getSession(true);
        String mensaje = "Error al consultar en la base de datos.";
        ConectionDB con = new ConectionDB();

        try {
            String id = (String) sesion.getAttribute("id_usu");
            con.conectar();

            PreparedStatement ps = con.getConn().prepareStatement(SELECT_USER);
            ps.setInt(1, Integer.parseInt(id));
            ps.setString(2, request.getParameter("contra"));
            ResultSet rs = ps.executeQuery();
            if (!rs.isBeforeFirst()) {
                mensaje = "La contraseña ingresada es incorrecta.";
                throw new SQLException(mensaje, ps.toString());
            }

            Part cvs = request.getPart("archivo");
            String rutaServlet = request.getServletContext().getRealPath("");
            String rutaCarpeta = rutaServlet + File.separatorChar + SAVE_DIR;
            String nombreArchivo = cvs.getSubmittedFileName();

            ps = con.getConn().prepareStatement(SELECT_FOLIO);
            ps.setString(1, nombreArchivo);
            rs = ps.executeQuery();
            if (rs.isBeforeFirst()) {
                mensaje = "Este archivo de abasto ya fue cargado.";
                throw new SQLException(mensaje, ps.toString());
            }

            File carpeta = new File(rutaCarpeta);
            if (!carpeta.exists()) {
                carpeta.mkdir();
            }

            String rutaArchvio = rutaCarpeta + File.separatorChar + nombreArchivo;
            cvs.write(rutaArchvio);
            String ver;
            if (lee.leeCSV(rutaArchvio)) {
                ver = "si";
                sesion.setAttribute("nomArchivo", nombreArchivo);
            } else {
                cvs.delete();
                ver = "no";
                mensaje = String.format("El archivo cvs contiene errores, por favor revise el log en '%s'", LoggerUsuario.getRutaArchivoActual());
                sesion.setAttribute("info_upload", mensaje);
            }

            sesion.setAttribute("ver", ver);

        } catch (IOException | ServletException ex) {
            sesion.setAttribute("ver", "no");
            sesion.setAttribute("info_upload", "El archivo no puedo subirse al servidor.");
            Logger.getLogger(FileUploadServlet.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } catch (SQLException ex) {
            Logger.getLogger(FileUploadServlet.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            sesion.setAttribute("ver", "no");
            sesion.setAttribute("info_upload", mensaje);
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(FileUploadServlet.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }
        String pagina = request.getParameter("pag");
        response.sendRedirect(pagina);

    }

}
