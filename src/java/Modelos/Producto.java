/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 * Modelo datos productos
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class Producto {

    String clave, descripcion, tipoPrdoucto, status, presentacion;
    double costo;

    public Producto() {
    }

    public Producto(String clave, String descripcion, String tipoPrdoucto, String status, String presentacion, double costo) {
        this.clave = clave;
        this.descripcion = descripcion;
        this.tipoPrdoucto = tipoPrdoucto;
        this.status = status;
        this.presentacion = presentacion;
        this.costo = costo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipoPrdoucto() {
        return tipoPrdoucto;
    }

    public void setTipoPrdoucto(String tipoPrdoucto) {
        this.tipoPrdoucto = tipoPrdoucto;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(String presentacion) {
        this.presentacion = presentacion;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

}
