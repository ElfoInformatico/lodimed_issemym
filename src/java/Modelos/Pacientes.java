/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.io.Serializable;
import java.util.Date;

/**
 * Modelo datos pacientes
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class Pacientes implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id_pac;
    private String ape_pat;
    private String ape_mat;
    private String nom_pac;
    private String nom_com;
    private Date fec_nac;
    private String sexo;
    private String num_afi;
    private String tip_cob;
    private Date ini_vig;
    private Date fin_vig;
    private int web;
    private String expediente;
    private String f_status;
    private String direccion;

    public Pacientes() {
    }

    public Pacientes(Long id_pac) {
        this.id_pac = id_pac;
    }

    public Pacientes(Long id_pac, String ape_pat, String ape_mat, String nom_pac, String nom_com, Date fec_nac, String sexo, String num_afi, String tip_cob, Date ini_vig, Date fin_vig, int web, String expediente, String f_status) {
        this.id_pac = id_pac;
        this.ape_pat = ape_pat;
        this.ape_mat = ape_mat;
        this.nom_pac = nom_pac;
        this.nom_com = nom_com;
        this.fec_nac = fec_nac;
        this.sexo = sexo;
        this.num_afi = num_afi;
        this.tip_cob = tip_cob;
        this.ini_vig = ini_vig;
        this.fin_vig = fin_vig;
        this.web = web;
        this.expediente = expediente;
        this.f_status = f_status;
    }

    public int getWeb() {
        return web;
    }

    public void setWeb(int web) {
        this.web = web;
    }

    public String getExpediente() {
        return expediente;
    }

    public void setExpediente(String expediente) {
        this.expediente = expediente;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId_pac() != null ? getId_pac().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pacientes)) {
            return false;
        }
        Pacientes other = (Pacientes) object;
        if ((this.getId_pac() == null && other.getId_pac() != null) || (this.getId_pac() != null && !this.id_pac.equals(other.id_pac))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelos.Pacientes[ idPac=" + getId_pac() + " ]";
    }

    /**
     * @return the id_pac
     */
    public Long getId_pac() {
        return id_pac;
    }

    /**
     * @param id_pac the id_pac to set
     */
    public void setId_pac(Long id_pac) {
        this.id_pac = id_pac;
    }

    /**
     * @return the ape_pat
     */
    public String getApe_pat() {
        return ape_pat;
    }

    /**
     * @param ape_pat the ape_pat to set
     */
    public void setApe_pat(String ape_pat) {
        this.ape_pat = ape_pat;
    }

    /**
     * @return the ape_mat
     */
    public String getApe_mat() {
        return ape_mat;
    }

    /**
     * @param ape_mat the ape_mat to set
     */
    public void setApe_mat(String ape_mat) {
        this.ape_mat = ape_mat;
    }

    /**
     * @return the nom_pac
     */
    public String getNom_pac() {
        return nom_pac;
    }

    /**
     * @param nom_pac the nom_pac to set
     */
    public void setNom_pac(String nom_pac) {
        this.nom_pac = nom_pac;
    }

    /**
     * @return the nom_com
     */
    public String getNom_com() {
        return nom_com;
    }

    /**
     * @param nom_com the nom_com to set
     */
    public void setNom_com(String nom_com) {
        this.nom_com = nom_com;
    }

    /**
     * @return the fec_nac
     */
    public Date getFec_nac() {
        return fec_nac;
    }

    /**
     * @param fec_nac the fec_nac to set
     */
    public void setFec_nac(Date fec_nac) {
        this.fec_nac = fec_nac;
    }

    /**
     * @return the sexo
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * @param sexo the sexo to set
     */
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    /**
     * @return the num_afi
     */
    public String getNum_afi() {
        return num_afi;
    }

    /**
     * @param num_afi the num_afi to set
     */
    public void setNum_afi(String num_afi) {
        this.num_afi = num_afi;
    }

    /**
     * @return the tip_cob
     */
    public String getTip_cob() {
        return tip_cob;
    }

    /**
     * @param tip_cob the tip_cob to set
     */
    public void setTip_cob(String tip_cob) {
        this.tip_cob = tip_cob;
    }

    /**
     * @return the ini_vig
     */
    public Date getIni_vig() {
        return ini_vig;
    }

    /**
     * @param ini_vig the ini_vig to set
     */
    public void setIni_vig(Date ini_vig) {
        this.ini_vig = ini_vig;
    }

    /**
     * @return the fin_vig
     */
    public Date getFin_vig() {
        return fin_vig;
    }

    /**
     * @param fin_vig the fin_vig to set
     */
    public void setFin_vig(Date fin_vig) {
        this.fin_vig = fin_vig;
    }

    /**
     * @return the f_status
     */
    public String getF_status() {
        return f_status;
    }

    /**
     * @param f_status the f_status to set
     */
    public void setF_status(String f_status) {
        this.f_status = f_status;
    }

}
