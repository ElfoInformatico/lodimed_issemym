/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 * Modelo datos catálogos
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class catalogo {

    private int nivel;
    private int cc;
    private int vd;
    private int totCl;

    public catalogo() {
    }

    public catalogo(int nivel, int cc, int vd, int totCl) {
        this.nivel = nivel;
        this.cc = cc;
        this.vd = vd;
        this.totCl = totCl;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getCc() {
        return cc;
    }

    public void setCc(int cc) {
        this.cc = cc;
    }

    public int getVd() {
        return vd;
    }

    public void setVd(int vd) {
        this.vd = vd;
    }

    public int getTotCl() {
        return totCl;
    }

    public void setTotCl(int totCl) {
        this.totCl = totCl;
    }

}
