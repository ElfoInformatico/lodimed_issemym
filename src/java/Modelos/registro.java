/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 * Modelo datos registro
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class registro {

    private String user;
    private String fecha;
    private String hora;
    private int sts;
    private String estatus;

    public registro() {
    }

    public registro(String user, String fecha, String hora, int sts, String estatus) {
        this.user = user;
        this.fecha = fecha;
        this.hora = hora;
        this.sts = sts;
        this.estatus = estatus;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public int getSts() {
        return sts;
    }

    public void setSts(int sts) {
        this.sts = sts;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

}
