/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 * Modelo datos inventario ciclico
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class ciclicos {

    private String clave;
    private String lote;
    private String caducidad;
    private int detalleProducto;
    private int idInventario;
    private String descripcion;
    private int inventario;
    private int diferencia;
    private int fisico;
    private int folio;
    private String fecha;
    private String hora;
    private int numeroClaves;
    private String tipoCiclico;
    private String unidad;

    public ciclicos() {
    }

    public ciclicos(String clave, String lote, String caducidad, int detalleProducto, int idInventario, String descripcion, int inventario, int diferencia, int fisico, int folio, String fecha, String hora, int numeroClaves, String tipoCiclico, String unidad) {
        this.clave = clave;
        this.lote = lote;
        this.caducidad = caducidad;
        this.detalleProducto = detalleProducto;
        this.idInventario = idInventario;
        this.descripcion = descripcion;
        this.inventario = inventario;
        this.diferencia = diferencia;
        this.fisico = fisico;
        this.folio = folio;
        this.fecha = fecha;
        this.hora = hora;
        this.numeroClaves = numeroClaves;
        this.tipoCiclico = tipoCiclico;
        this.unidad = unidad;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public int getNumeroClaves() {
        return numeroClaves;
    }

    public void setNumeroClaves(int numeroClaves) {
        this.numeroClaves = numeroClaves;
    }

    public String getTipoCiclico() {
        return tipoCiclico;
    }

    public void setTipoCiclico(String tipoCiclico) {
        this.tipoCiclico = tipoCiclico;
    }

    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public int getFisico() {
        return fisico;
    }

    public void setFisico(int fisico) {
        this.fisico = fisico;
    }

    public int getInventario() {
        return inventario;
    }

    public void setInventario(int inventario) {
        this.inventario = inventario;
    }

    public int getDiferencia() {
        return diferencia;
    }

    public void setDiferencia(int diferencia) {
        this.diferencia = diferencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getCaducidad() {
        return caducidad;
    }

    public void setCaducidad(String caducidad) {
        this.caducidad = caducidad;
    }

    public int getDetalleProducto() {
        return detalleProducto;
    }

    public void setDetalleProducto(int detalleProducto) {
        this.detalleProducto = detalleProducto;
    }

    public int getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(int idInventario) {
        this.idInventario = idInventario;
    }

}
