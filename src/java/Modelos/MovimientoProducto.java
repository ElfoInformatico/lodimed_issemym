/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import org.json.simple.JSONObject;

/**
 * Modelo datos movimiento de productos
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class MovimientoProducto {

public Producto producto;
public DetalleProducto detalleProducto;
public Inventario inventario;

public MovimientoProducto() {
    producto = new Producto();
    detalleProducto = new DetalleProducto();
    inventario = new Inventario();
}

@Override
public String toString() {
    return "MovimientoProducto{" + "producto=" + producto.toString() + ", detalleProducto=" + detalleProducto.toString() + ", inventario=" + inventario.
              toString() + '}';
}

public JSONObject toJSON() {

    JSONObject jo = new JSONObject();
    jo.put("detalle_producto", this.detalleProducto.id);
    jo.put("clave", this.detalleProducto.clave);
    jo.put("caducidad", this.detalleProducto.caducidad);
    jo.put("lote", this.detalleProducto.lote);
    jo.put("origen", this.detalleProducto.origen);
    jo.put("origen_descripcion", this.detalleProducto.origenDescripcion);

    jo.put("cantidad", this.inventario.cantidad);
    jo.put("id_inventario", this.inventario.id);

    jo.put("descripcion", this.producto.descripcion);

    return jo;
}

}
