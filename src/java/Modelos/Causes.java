/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 * Datos de causes
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class Causes {

    private String idCauses;
    private String desCauses;

    public Causes(String id, String desC) {
        this();
        this.idCauses = id;
        this.desCauses = desC;
    }

    public Causes() {
        this.idCauses = "1";
        this.desCauses = "-";
    }

    public String getIdCauses() {
        return idCauses;
    }

    public void setIdCauses(String idCauses) {
        this.idCauses = idCauses;
    }

    public String getDesCauses() {
        return desCauses;
    }

    public void setDesCauses(String desCauses) {
        this.desCauses = desCauses;
    }

}
