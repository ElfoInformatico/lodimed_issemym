package Modelos;

/**
 * Permite separar una cantidad durante la captura de la receta.
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class Apartado {

    public final static String POR_RECETA_TIPO = "SALIDA POR RECETA";
    public final static String POR_AJUSTE_TIPO = "SALIDA POR AJUSTE";
    public final static String POR_TRASPASO_TIPO = "SALIDA POR TRASPASO";

    public final static int DISPENSADO_STATUS = 3;
    public final static int APARTADO_STATUS = 1;
    public final static int CANCELADO_STATUS = 2;

    int id, detalleProducto, detalleReceta, cantidad, status;
    String fecha, tipo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDetalleProducto() {
        return detalleProducto;
    }

    public void setDetalleProducto(int detalleProducto) {
        this.detalleProducto = detalleProducto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Apartamiento{" + "id=" + id + ", detalle_producto=" + detalleProducto + ", cantidad=" + cantidad + ", status=" + status + ", fecha=" + fecha + ", tipo=" + tipo + '}';
    }

}
