/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.io.Serializable;
import java.util.Date;

/**
 * Consulta detalles de la receta
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class DetReceta implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer fol_det;
    private Integer det_pro;
    private Integer can_sol;
    private Integer cant_sur;
    private Date fec_sur;
    private Integer status;
    private Date hor_sur;
    private String indicaciones;
    private Integer baja;
    private Integer web;
    private String id_cau2;

    public DetReceta() {
    }

    public DetReceta(Integer fol_det) {
        this.fol_det = fol_det;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getFol_det() != null ? getFol_det().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetReceta)) {
            return false;
        }
        DetReceta other = (DetReceta) object;
        if ((this.getFol_det() == null && other.getFol_det() != null) || (this.getFol_det() != null && !this.fol_det.equals(other.fol_det))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelos.Detreceta[ fol_det=" + getFol_det() + " ]";
    }

    /**
     * @return the fol_det
     */
    public Integer getFol_det() {
        return fol_det;
    }

    /**
     * @param fol_det the fol_det to set
     */
    public void setFol_det(Integer fol_det) {
        this.fol_det = fol_det;
    }

    /**
     * @return the can_sol
     */
    public Integer getCan_sol() {
        return can_sol;
    }

    /**
     * @param can_sol the can_sol to set
     */
    public void setCan_sol(Integer can_sol) {
        this.can_sol = can_sol;
    }

    /**
     * @return the cant_sur
     */
    public Integer getCant_sur() {
        return cant_sur;
    }

    /**
     * @param cant_sur the cant_sur to set
     */
    public void setCant_sur(Integer cant_sur) {
        this.cant_sur = cant_sur;
    }

    /**
     * @return the fec_sur
     */
    public Date getFec_sur() {
        return fec_sur;
    }

    /**
     * @param fec_sur the fec_sur to set
     */
    public void setFec_sur(Date fec_sur) {
        this.fec_sur = fec_sur;
    }

    /**
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return the hor_sur
     */
    public Date getHor_sur() {
        return hor_sur;
    }

    /**
     * @param hor_sur the hor_sur to set
     */
    public void setHor_sur(Date hor_sur) {
        this.hor_sur = hor_sur;
    }

    /**
     * @return the indicaciones
     */
    public String getIndicaciones() {
        return indicaciones;
    }

    /**
     * @param indicaciones the indicaciones to set
     */
    public void setIndicaciones(String indicaciones) {
        this.indicaciones = indicaciones;
    }

    /**
     * @return the baja
     */
    public Integer getBaja() {
        return baja;
    }

    /**
     * @param baja the baja to set
     */
    public void setBaja(Integer baja) {
        this.baja = baja;
    }

    /**
     * @return the web
     */
    public Integer getWeb() {
        return web;
    }

    /**
     * @param web the web to set
     */
    public void setWeb(Integer web) {
        this.web = web;
    }

    /**
     * @return the id_cau2
     */
    public String getId_cau2() {
        return id_cau2;
    }

    /**
     * @param id_cau2 the id_cau2 to set
     */
    public void setId_cau2(String id_cau2) {
        this.id_cau2 = id_cau2;
    }

    /**
     * @return the det_pro
     */
    public Integer getDet_pro() {
        return det_pro;
    }

    /**
     * @param det_pro the det_pro to set
     */
    public void setDet_pro(Integer det_pro) {
        this.det_pro = det_pro;
    }

}
