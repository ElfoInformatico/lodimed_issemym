/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 * Modelo datos receta manual HL7
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class HL7ManualModel {

    private String fec;
    private int rctalaboradas;
    private int rctahl7;
    private int rctamanuales;
    private int rctasurtidas;
    private int rctapendientes;
    private int pzassol;
    private int pzassur;
    private String medico;
    private String paciente;
    private String folio;

    public HL7ManualModel() {
    }

    public HL7ManualModel(String fec, int rctalaboradas, int rctahl7, int rctamanuales, int rctasurtidas, int rctapendientes, int pzassol, int pzassur, String medico, String paciente, String folio) {
        this.fec = fec;
        this.rctalaboradas = rctalaboradas;
        this.rctahl7 = rctahl7;
        this.rctamanuales = rctamanuales;
        this.rctasurtidas = rctasurtidas;
        this.rctapendientes = rctapendientes;
        this.pzassol = pzassol;
        this.pzassur = pzassur;
        this.medico = medico;
        this.paciente = paciente;
        this.folio = folio;
    }

    public String getFec() {
        return fec;
    }

    public void setFec(String fec) {
        this.fec = fec;
    }

    public int getRctalaboradas() {
        return rctalaboradas;
    }

    public void setRctalaboradas(int rctalaboradas) {
        this.rctalaboradas = rctalaboradas;
    }

    public int getRctahl7() {
        return rctahl7;
    }

    public void setRctahl7(int rctahl7) {
        this.rctahl7 = rctahl7;
    }

    public int getRctamanuales() {
        return rctamanuales;
    }

    public void setRctamanuales(int rctamanuales) {
        this.rctamanuales = rctamanuales;
    }

    public int getRctasurtidas() {
        return rctasurtidas;
    }

    public void setRctasurtidas(int rctasurtidas) {
        this.rctasurtidas = rctasurtidas;
    }

    public int getRctapendientes() {
        return rctapendientes;
    }

    public void setRctapendientes(int rctapendientes) {
        this.rctapendientes = rctapendientes;
    }

    public int getPzassol() {
        return pzassol;
    }

    public void setPzassol(int pzassol) {
        this.pzassol = pzassol;
    }

    public int getPzassur() {
        return pzassur;
    }

    public void setPzassur(int pzassur) {
        this.pzassur = pzassur;
    }

    public String getMedico() {
        return medico;
    }

    public void setMedico(String medico) {
        this.medico = medico;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

}
