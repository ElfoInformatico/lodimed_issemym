/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 * Modelo datos administración reabastecimiento
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class reabModel {

    private int id;
    private String cu;
    private String claveInsumo;
    private String descrip;
    private int cantidad;
    private int total;
    private int carga;
    private String obs;

    public reabModel() {
    }

    public reabModel(int id, String cu, String claveInsumo, int cantidad, int total, int carga, String descrip, String obs) {
        this.id = id;
        this.cu = cu;
        this.claveInsumo = claveInsumo;
        this.cantidad = cantidad;
        this.total = total;
        this.carga = carga;
        this.descrip = descrip;
        this.obs = obs;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public int getCarga() {
        return carga;
    }

    public void setCarga(int carga) {
        this.carga = carga;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCu() {
        return cu;
    }

    public void setCu(String cu) {
        this.cu = cu;
    }

    public String getClaveInsumo() {
        return claveInsumo;
    }

    public void setClaveInsumo(String claveInsumo) {
        this.claveInsumo = claveInsumo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

}
