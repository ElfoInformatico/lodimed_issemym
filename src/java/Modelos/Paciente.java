package Modelos;

import org.json.simple.JSONObject;

/**
 * Modelo datos registro pacientes
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class Paciente {

    public static String ACTIVO_STATUS = "A";
    public static String SUSPENDIDO_STATUS = "S";
    public static String SIN_ID_HL7 = "-";
    public static String SIN_NACIMIENTO = "01-01-1900";
    public static String SIN_INICIO_VIGENCIA = "01-01-2015";
    public static String SIN_FIN_VIGENCIA = "31-12-2036";
    public static String FORMATO_DEFECTO_NACIMIENTO = "%d-%m-%Y";

    private long id;
    private String ap;
    private String am;
    private String nombre;
    private String nombreCompleto;
    private String nacimiento;
    private String sexo;
    private String numAfi;
    private String tipoCobro;
    private String inicioVigencia;
    private String finVigencia;
    private String expediente;
    private String sts;
    private int vigencia;
    private int web;
    private String direccion;
    private String formatoFecha;
    private String idpac_hl7;

    public Paciente() {
        this.id = 0L;
        this.web = 0;
        this.direccion = "";
        this.formatoFecha = FORMATO_DEFECTO_NACIMIENTO;
        this.idpac_hl7 = SIN_ID_HL7;
    }

    public Paciente(int id, String ap, String am, String nombre,
            String nc, String fecNac, String sexo, String numAfi,
            String tipCob, String iniVig, String finVig, String expediente,
            String sts, String idpac_hl7) {
        this();
        this.id = id;
        this.ap = ap;
        this.am = am;
        this.nombre = nombre;
        this.nombreCompleto = nc;
        this.nacimiento = fecNac;
        this.sexo = sexo;
        this.numAfi = numAfi;
        this.tipoCobro = tipCob;
        this.inicioVigencia = iniVig;
        this.finVigencia = finVig;
        this.expediente = expediente;
        this.sts = sts;
        this.idpac_hl7 = idpac_hl7;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getApellidoPaterno() {
        return ap;
    }

    public void setApellidoPaterno(String ap) {
        this.ap = ap;
    }

    public String getApellidoMaterno() {
        return am;
    }

    public void setApellidoMaterno(String am) {
        this.am = am;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(String fecNac) {
        this.nacimiento = fecNac;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getNumAfi() {
        return numAfi;
    }

    public void setNumAfi(String numAfi) {
        this.numAfi = numAfi;
    }

    public String getTipCob() {
        return tipoCobro;
    }

    public void setTipCob(String tipCob) {
        this.tipoCobro = tipCob;
    }

    public String getInicioVigencia() {
        return inicioVigencia;
    }

    public void setInicioVigencia(String iniVig) {
        this.inicioVigencia = iniVig;
    }

    public String getFinVigencia() {
        return finVigencia;
    }

    public void setFinVigencia(String finVig) {
        this.finVigencia = finVig;
    }

    public String getExpediente() {
        return expediente;
    }

    public void setExpediente(String expediente) {
        this.expediente = expediente;
    }

    public String getSts() {
        return sts;
    }

    public void setSts(String sts) {
        this.sts = sts;
    }

    public int getVigencia() {
        return vigencia;
    }

    public void setVigencia(int vigencia) {
        this.vigencia = vigencia;
    }

    public String getIdpac_hl7() {
        return idpac_hl7;
    }

    public void setIdpac_hl7(String idpac_hl7) {
        this.idpac_hl7 = idpac_hl7;
    }

    @Override
    public String toString() {
        return "Paciente{" + "id=" + id + ", ap=" + ap + ", am=" + am + ", nombre=" + nombre + ", nombreCompleto=" + nombreCompleto + ", fecNac=" + nacimiento + ", sexo=" + sexo + ", numAfi=" + numAfi + ", tipCob=" + tipoCobro + ", iniVig=" + inicioVigencia + ", finVig=" + finVigencia + ", expediente=" + expediente + ", sts=" + sts + ", vigencia=" + vigencia + '}';
    }

    public JSONObject toJSON() {

        JSONObject jo = new JSONObject();
        jo.put("id", this.id);
        jo.put("paterno", this.ap);
        jo.put("materno", this.am);
        jo.put("nombre", nombreCompleto);
        jo.put("nacimiento", this.nacimiento);
        jo.put("sexo", this.sexo);
        jo.put("folio", this.numAfi);
        jo.put("cobranza", this.tipoCobro);
        jo.put("inicio", this.inicioVigencia);
        jo.put("fin", this.finVigencia);
        jo.put("expediente", this.expediente);
        jo.put("status", this.sts);
        jo.put("vigencia", this.vigencia);

        return jo;
    }

    public int getWeb() {
        return web;
    }

    public void setWeb(int web) {
        this.web = web;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFormatoFecha() {
        return formatoFecha;
    }

    public void setFormatoFecha(String formatoFecha) {
        this.formatoFecha = formatoFecha;
    }

}
