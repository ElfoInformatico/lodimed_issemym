/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import org.json.simple.JSONObject;

/**
 * Consulta detalles de las receta capturada
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class DetalleCapturado {

    public DetalleProducto detalleProducto;
    public DetalleReceta detalleReceta;
    String descripcion;

    public DetalleCapturado(DetalleProducto detalleProducto, DetalleReceta detalleReceta, String descripcion) {
        this.detalleProducto = detalleProducto;
        this.detalleReceta = detalleReceta;
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "DetalleCapturado{" + "detalleProducto=" + detalleProducto.toString() + ", detalleReceta=" + detalleReceta.toString() + '}';
    }

    public JSONObject toJSON() {

        JSONObject jo = new JSONObject();
        jo.put("clave", this.detalleProducto.clave);
        jo.put("caducidad", this.detalleProducto.caducidad);
        jo.put("lote", this.detalleProducto.lote);

        jo.put("id", this.detalleReceta.id);
        jo.put("solicitado", this.detalleReceta.cantidadSolicitada);
        jo.put("surtido", this.detalleReceta.cantidadSurtida);
        jo.put("cause", this.detalleReceta.causePrimario);
        jo.put("cause2", this.detalleReceta.causeSegundario);
        jo.put("indicaciones", this.detalleReceta.indicaciones);
        jo.put("descripcion", this.descripcion);

        return jo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
