/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 * Consulta detalles de la receta y tipo de receta
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class DetalleReceta {

    public static final int NORMAL_STATUS = 1;
    public static final int CAMBIO_LOTE_STATUS = 2;
    public static final int FALTANTE_FISICO_STATUS = 3;
    public static final int DETALLE_ADICIONADO_STATUS = 4;

    public static final String INDICACIONES_COLECTIVO = "SIN INDICACIONES";
    public static final String CAUSE_COLECTIVO = "998";

    String fechaSurtido, horaSurtido, causePrimario, causeSegundario, indicaciones;
    int id, cantidadSolicitada, cantidadSurtida, detalleProducto, folioDetalle, status, idReceta, baja;

    public String getFechaSurtido() {
        return fechaSurtido;
    }

    public void setFechaSurtido(String fechaSurtido) {
        this.fechaSurtido = fechaSurtido;
    }

    public String getHoraSurtido() {
        return horaSurtido;
    }

    public void setHoraSurtido(String horaSurtido) {
        this.horaSurtido = horaSurtido;
    }

    public String getCausePrimario() {
        return causePrimario;
    }

    public void setCausePrimario(String causePrimario) {
        this.causePrimario = causePrimario;
    }

    public String getCauseSegundario() {
        return causeSegundario;
    }

    public void setCauseSegundario(String causeSegundario) {
        this.causeSegundario = causeSegundario;
    }

    public String getIndicaciones() {
        return indicaciones;
    }

    public void setIndicaciones(String indicaciones) {
        this.indicaciones = indicaciones;
    }

    public int getCantidadSolicitada() {
        return cantidadSolicitada;
    }

    public void setCantidadSolicitada(int cantidadSolicitada) {
        this.cantidadSolicitada = cantidadSolicitada;
    }

    public int getCantidadSurtida() {
        return cantidadSurtida;
    }

    public void setCantidadSurtida(int cantidadSurtida) {
        this.cantidadSurtida = cantidadSurtida;
    }

    public int getDetalleProducto() {
        return detalleProducto;
    }

    public void setDetalleProducto(int detalleProducto) {
        this.detalleProducto = detalleProducto;
    }

    public int getFolioDetalle() {
        return folioDetalle;
    }

    public void setFolioDetalle(int folioDetalle) {
        this.folioDetalle = folioDetalle;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIdReceta() {
        return idReceta;
    }

    public void setIdReceta(int idReceta) {
        this.idReceta = idReceta;
    }

    public int getBaja() {
        return baja;
    }

    public void setBaja(int baja) {
        this.baja = baja;
    }

    public DetalleReceta() {
    }

    //constructor sin id
    public DetalleReceta(String fechaSurtido, String horaSurtido, String causePrimario, String causeSegundario, String indicaciones, int cantidadSolicitada, int cantidadSurtida, int detalleProducto, int status, int idReceta, int baja) {
        this.fechaSurtido = fechaSurtido;
        this.horaSurtido = horaSurtido;
        this.causePrimario = causePrimario;
        this.causeSegundario = causeSegundario;
        this.indicaciones = indicaciones;
        this.cantidadSolicitada = cantidadSolicitada;
        this.cantidadSurtida = cantidadSurtida;
        this.detalleProducto = detalleProducto;
        this.status = status;
        this.idReceta = idReceta;
        this.baja = baja;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
