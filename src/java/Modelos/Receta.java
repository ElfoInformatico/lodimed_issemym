package Modelos;

/**
 * Modelo datos receta
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class Receta {

    public static final int COLECTIVA_TIPO = 2;
    public static final int MANUAL_TIPO = 1;
    public static final int MEDICO_TIPO = 3;
    public static final int MEDICO_TIPO_COLECTIVA = 4;
    public static final int ID_SERVICIO_MANUAL_TIPO = 1;
    public static final int ID_PACIENTE_COLECTIVA_TIPO = 1;

    public static final String ENCARGADO_SERVICIO_MANUAL_TIPO = "-";
    public static final String TIPO_CONSULTA_COLECTIVA_TIPO = "-";
    public static final String CARNET_COLECTIVA_TIPO = " ";

    public static final int EN_CAPTURA_TRANSITO = 1;
    public static final int PENDIENTE_SURTIR_TRANSITO = 2;
    public static final int SURTIDA_TRANSITO = 0;
    public static final int CADUCA_TRANSITO = 3;

    public static final int MESES_PARA_CADUCO = 4;

    public static final int ACTIVA_BAJA = 0;
    public static final int CANCELADA_BAJA = 1;

    public static final int SIN_NOTIFICAR_HL7_NOTIFICACION = 0;
    public static final int NOTIFICADO_HL7_NOTIFICACION = 1;
    public static final String OBSERVACION_HL7_MANUAL_TIPO = "-";
    public static final String OBSERVACION_MANUAL_TIPO = "";

    public static final String URGENCIAS_TIPO_CONSULTA = "Urgencias";
    public static final String CONSULTA_TIPO_CONSULTA = "Consulta Externa";
    public static final String HOSPITALIZACION_TIPO_CONSULTA = "Hospitalización";

    //campo cedula=idMedico
    int id, tipoReceta, idMedico, idUsuario, idServicio, trasito, baja;
    String folio, tipoConsulta, fecha, observacionHL7;
    long idPaciente;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTipoReceta() {
        return tipoReceta;
    }

    public void setTipoReceta(int tipoReceta) {
        this.tipoReceta = tipoReceta;
    }

    public long getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(long idPaciente) {
        this.idPaciente = idPaciente;
    }

    public int getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(int idMedico) {
        this.idMedico = idMedico;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(int idServicio) {
        this.idServicio = idServicio;
    }

    public int getTrasito() {
        return trasito;
    }

    public void setTrasito(int trasito) {
        this.trasito = trasito;
    }

    public int getBaja() {
        return baja;
    }

    public void setBaja(int baja) {
        this.baja = baja;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getTipoConsulta() {
        return tipoConsulta;
    }

    public void setTipoConsulta(String tipoConsulta) {
        this.tipoConsulta = tipoConsulta;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getObservacionHL7() {
        return observacionHL7;
    }

    public void setObservacionHL7(String observacionHL7) {
        this.observacionHL7 = observacionHL7;
    }

    public Receta(int tipoReceta, int idPaciente, int idMedico, int idUsuario, int idServicio, int trasito, int baja, String folio, String tipoConsulta, String fecha) {
        this();
        this.tipoReceta = tipoReceta;
        this.idPaciente = idPaciente;
        this.idMedico = idMedico;
        this.idUsuario = idUsuario;
        this.idServicio = idServicio;
        this.trasito = trasito;
        this.baja = baja;
        this.folio = folio;
        this.tipoConsulta = tipoConsulta;
        this.fecha = fecha;
    }

    public Receta() {
        this.observacionHL7 = OBSERVACION_HL7_MANUAL_TIPO;
    }

    @Override
    public String toString() {
        return "Receta{" + "id=" + id + ", tipoReceta=" + tipoReceta + ", idPaciente=" + idPaciente + ", idMedico=" + idMedico + ", idUsuario=" + idUsuario + ", idServicio=" + idServicio + ", trasito=" + trasito + ", baja=" + baja + ", folio=" + folio + ", tipoConsulta=" + tipoConsulta + ", fecha=" + fecha + '}';
    }

}
