package Modelos;

import Clases.ConectionDB;
import com.google.api.client.http.FileContent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.TreeMap;

/**
 * Administra el flujo de la subida y busqueda de archivos de archivos en el
 * google drive.
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class DriveManager {

    public enum SearchStatus {

        FOLDER_NOT_FOUND, FILE_NOT_FOUND, FILE_FOUND
    }

    public ConectionDB con;
    public Drive service;
    private String fileName;
    private String idPharmacy;
    private String idFile;
    private String idFolder;

    private final static String BACKUP_NUMBER_QRY = "SELECT @rango := ( TIME_TO_SEC( TIMEDIFF(fin_jornada, inicio_jornada)) / (i.numero_por_dia * 60 * 60)) AS rango, @diff := FLOOR( TIME_TO_SEC( TIMEDIFF( CURTIME(), TIME(inicio_jornada))) / (@rango * 60 * 60)) + 1 AS diff, @numero := IF ( @diff > i.numero_por_dia, i.numero_por_dia, @diff ) numero, CONCAT( CURDATE(), '_' ,@numero, '.zip' ) AS nombre FROM indices AS i LIMIT 1;";

    private final static String ID_QRY = "SELECT us.cla_uni AS id FROM usuarios AS us WHERE us.`user` = 'admon' LIMIT 1;";

    private final static String MAXIMUM_QRY = "SELECT i.maximo_dias AS maximo FROM indices AS i LIMIT 1;";

    private final static String UPDATE_QRY = "UPDATE `indices` SET `inicio_jornada` = ?, `fin_jornada` = ?, `numero_por_dia` = ?, `maximo_dias` = ?;";

    private final static String IS_24H = "24";

    public DriveManager(ConectionDB con) {
        this.con = con;
    }

    /**
     * Genera un nombre dependiendo del rango de la jornada laboral en la que se
     * encuentre actualmente, por ejemplo: La jornada es 8 a las 17, se divide
     * el rango en 3, sería cada tres horas. Por tanto si son las 13 este
     * generaría un nombre como "2016-08-25_2.zip".
     *
     * @return Concatena la fecha actual con el número del backup, dependiendo
     * de la hora.
     * @throws SQLException
     */
    public String getFileName() throws SQLException {
        if (fileName == null) {
            PreparedStatement ps = con.getConn().prepareStatement(BACKUP_NUMBER_QRY);
            ResultSet rs = ps.executeQuery();

            rs.next();
            fileName = rs.getString("nombre");

            rs.close();
            ps.close();
        }

        return fileName;
    }

    /**
     * Verifica si existe el archivo de la unidad actual y en el número de
     * backup dependiendo de la hora actual.
     *
     * @return Tres posibles estados: SearchStatus.FOLDER_NOT_FOUND si no existe
     * ningun backup de la unidad, SearchStatus.FILE_NOT_FOUND si no existe el
     * archivo para la hora actual y SearchStatus.FILE_FOUND si ya existe un
     * respaldo.
     * @throws IOException
     * @throws SQLException
     */
    public SearchStatus existFile() throws IOException, SQLException {

        String pageToken = null;
        idFolder = null;
        FileList searchResult;
        String qry = String.format("mimeType = 'application/vnd.google-apps.folder' and name = '%s'", getIDPharmacy());
//        System.out.println(qry);
        do {
            searchResult = service.files().list()
                    .setQ(qry)
                    .setSpaces("drive")
                    .setFields("nextPageToken, files(id)")
                    .setPageToken(pageToken)
                    .setPageSize(10)
                    .execute();

            for (File file : searchResult.getFiles()) {
                /*System.out.printf("Found folder: %s\n",
                 file.getId());*/
                idFolder = file.getId();
                break;
            }
            pageToken = searchResult.getNextPageToken();
        } while (pageToken != null);

        if (idFolder == null) {
            return SearchStatus.FOLDER_NOT_FOUND;
        }

        pageToken = null;
        qry = String.format("'%s' in parents and name = '%s'", idFolder, getFileName());
        idFile = null;
        do {
            searchResult = service.files().list()
                    .setQ(qry)
                    .setSpaces("drive")
                    .setFields("nextPageToken, files(id)")
                    .setPageToken(pageToken)
                    .setPageSize(10)
                    .execute();

            for (File file : searchResult.getFiles()) {
                /*System.out.printf("Found file: %s\n",
                 file.getId());*/
                this.idFile = file.getId();
                break;
            }
            pageToken = searchResult.getNextPageToken();
        } while (pageToken != null);

        if (idFile == null) {
            return SearchStatus.FILE_NOT_FOUND;
        }

        return SearchStatus.FILE_FOUND;
    }

    /**
     *
     * @return Devuelve el ID de la unidad asignada en el sistema.
     * @throws SQLException
     */
    public String getIDPharmacy() throws SQLException {

        if (idPharmacy == null) {
            PreparedStatement ps = con.getConn().prepareStatement(ID_QRY);
            ResultSet rs = ps.executeQuery();

            rs.next();
            idPharmacy = rs.getString("id");

            rs.close();
            ps.close();
        }

        return idPharmacy;
    }

    /**
     * Crea una carpeta para la unidad actual.
     *
     * @return Devuelve al ID asignado por google a la carpeta.
     * @throws SQLException
     * @throws IOException
     */
    public String createFolder() throws SQLException, IOException {
        File fileMetadata = new File();
        fileMetadata.setName(getIDPharmacy());
        fileMetadata.setMimeType("application/vnd.google-apps.folder");

        File file = service.files().create(fileMetadata)
                .setFields("id")
                .execute();
        //System.out.println("Folder ID: " + file.getId());
        idFolder = file.getId();

        return idFolder;
    }

    /**
     * Sube el archivo a la carpeta de la unidad en google drive.
     *
     * @param path Ruta local del archivo.
     * @return El id del archivo en google drive.
     * @throws SQLException
     * @throws IOException
     */
    public String insertFile(String path) throws SQLException, IOException {

        File fileMetadata = new File();
        fileMetadata.setName(getFileName());
        fileMetadata.setParents(Collections.singletonList(this.idFolder));

        java.io.File filePath = new java.io.File(path);
        FileContent mediaContent = new FileContent("application/zip", filePath);
        File file = service.files().create(fileMetadata, mediaContent)
                .setFields("id, parents")
                .execute();
        //System.out.println("File ID: " + file.getId());
        this.idFile = file.getId();

        return this.idFile;
    }

    /**
     * Utiliza el id del archivo para actualizarlo, debe ejecutarse primero
     * existFile().
     *
     * @param path Ruta del archivo local.
     * @return Id del archivo en google drive.
     * @throws SQLException
     * @throws IOException
     */
    public String updateFile(String path) throws SQLException, IOException {
        File fileMetadata = new File();
        fileMetadata.setName(getFileName());

        java.io.File filePath = new java.io.File(path);
        FileContent mediaContent = new FileContent("application/zip", filePath);
        File file = service.files().update(this.idFile, fileMetadata, mediaContent)
                .setFields("id")
                .execute();

        //System.out.println("File ID: " + file.getId());
        this.idFile = file.getId();

        return this.idFile;
    }

    /**
     * Mueve el archivo especificado a la papelera de re.
     *
     * @return Id del archivo en google drive.
     * @throws IOException
     */
    public String sendTrashFile(String id) throws IOException {
        File fileMetadata = new File();
        fileMetadata.setTrashed(Boolean.TRUE);

        File file = service.files().update(id, fileMetadata)
                .setFields("id,trashed")
                .execute();

        /*System.out.println("File ID: " + file.getId()
         + "is Trashed: " + file.getTrashed());*/
        this.idFile = file.getId();

        return this.idFile;
    }

    public void uploadFile(String path) throws IOException, SQLException {
        SearchStatus seachResult = existFile();

        switch (seachResult) {

            case FOLDER_NOT_FOUND:
                createFolder();
                insertFile(path);
                break;

            case FILE_NOT_FOUND:
                insertFile(path);
                break;

            case FILE_FOUND:
                updateFile(path);
                break;

            default:
                throw new AssertionError();
        }

        /*Elimina archivos archivos de la carpeta de la unidad
         si supera el número máximo de días.*/
        checkMaximumDays();
    }

    /**
     * Elimina los archivos de una unidad, si estos supera el número máximo de
     * días que se pueden almacenar.
     *
     * @throws SQLException
     * @throws IOException
     */
    public void checkMaximumDays() throws SQLException, IOException {
        int maximum = 0;

        PreparedStatement ps = con.getConn().prepareStatement(MAXIMUM_QRY);
        ResultSet rs = ps.executeQuery();

        rs.next();
        maximum = rs.getInt("maximo");

        rs.close();
        ps.close();

        TreeMap<String, ArrayList<String>> dates = new TreeMap<>();
        ArrayList<String> ids;
        String pageToken = null;
        FileList searchResult;
        String qry = String.format("'%s' in parents", idFolder);
        String name;
        do {
            searchResult = service.files().list()
                    .setQ(qry)
                    .setSpaces("drive")
                    .setFields("nextPageToken, files(id,name)")
                    .setPageToken(pageToken)
                    .setPageSize(10)
                    .execute();

            for (File file : searchResult.getFiles()) {

                //Toma la primera parte que es la fecha.
                name = file.getName().split("_")[0];

                if (!dates.containsKey(name)) {
                    dates.put(name, new ArrayList<String>());
                }

                ids = dates.get(name);
                ids.add(file.getId());
            }
            pageToken = searchResult.getNextPageToken();
        } while (pageToken != null);

        //Elimina de ser necesario archivos si se supera el numero máximo de días.
        Set<String> sortedDates = dates.keySet();
        String first;
        int size = sortedDates.size();
        while (size > maximum) {
            first = String.valueOf(sortedDates.toArray()[0]);
            for (String id : dates.get(first)) {
                sendTrashFile(id);
            }
            sortedDates.remove(first);
            size = sortedDates.size();
        }

    }

    /**
     * Actualiza la información de configuración de los backups para la unidad.
     *
     * @param init
     * @param end
     * @param backupsPerDay
     * @param maximumBackups
     * @throws SQLException
     */
    public void updateConfig(String init, String end,
            int backupsPerDay, int maximumBackups) throws SQLException {

        try (PreparedStatement ps = con.getConn().prepareStatement(UPDATE_QRY)) {

            //Añade una fecha para completar el formato
            String initHour = String.format("2016-08-25 %s", init);
            String endHour = String.format("2016-08-25 %s", end);
            if (init.equalsIgnoreCase(IS_24H)) {
                initHour = "2016-08-25 08:00:00";
                endHour = "2016-08-26 08:00:00";
            }

            ps.setString(1, initHour);
            ps.setString(2, endHour);

            ps.setInt(3, backupsPerDay);
            ps.setInt(4, maximumBackups);

            boolean ok = ps.executeUpdate() > 0;

            if (!ok) {
                throw new SQLException("No se pudo actualizar la información de los backups.");
            }

        }
    }
}
