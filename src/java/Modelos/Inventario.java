/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 * Modelo datos inventario
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class Inventario {

    String claveUnidad, fechaElaboracion;
    int id, cantidad, detalleProducto;

    public String getClaveUnidad() {
        return claveUnidad;
    }

    public void setClaveUnidad(String claveUnidad) {
        this.claveUnidad = claveUnidad;
    }

    public String getFechaElaboracion() {
        return fechaElaboracion;
    }

    public void setFechaElaboracion(String fechaElaboracion) {
        this.fechaElaboracion = fechaElaboracion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getDetalleProducto() {
        return detalleProducto;
    }

    public void setDetalleProducto(int detalleProducto) {
        this.detalleProducto = detalleProducto;
    }

    public Inventario() {
    }

    public Inventario(String claveUnidad, String fechaElaboracion, int id, int cantidad, int detalleProducto) {
        this.claveUnidad = claveUnidad;
        this.fechaElaboracion = fechaElaboracion;
        this.id = id;
        this.cantidad = cantidad;
        this.detalleProducto = detalleProducto;
    }

    @Override
    public String toString() {
        return "Inventario{" + "claveUnidad=" + claveUnidad + ", fechaElaboracion=" + fechaElaboracion + ", id=" + id + ", cantidad=" + cantidad + ", detalleProducto=" + detalleProducto + '}';
    }

}
