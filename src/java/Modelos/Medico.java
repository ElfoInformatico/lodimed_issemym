package Modelos;

/**
 * Modelo datos registro de nuevo médico
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class Medico {

    public static String ACTIVO_STATUS = "A";
    public static String SUSPENDIDO_STATUS = "S";

    public static String SIN_APELLIDO = "N/A(HL7)";
    public static String SIN_NOMBRE = "HL7";
    public static int SIN_WEB = 1;
    public static String SIN_NACIMIENTO = "01-01-1900";
    public static int SIN_FOLIO = -1;

    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String nombreCompleto;
    private int id;
    private int web;
    private String rfc;
    private String unidad;
    private String cedula;
    private String status;
    private String Nacimiento;
    private int folioInicial;
    private int folioFinal;
    private int folioActual;
    private String tipoConsulta;
    private String formatoFecha;

    public Medico(String nom, String ap, String am, String nomC, int cedula, int web, String rfc, String claUni, String cedPro, String sts, String fecNac, int folIni, int folFin, int folAct, String tipCons) {
        this();

        this.nombre = nom;
        this.apellidoPaterno = ap;
        this.apellidoMaterno = am;
        this.nombreCompleto = nomC;
        this.id = cedula;
        this.web = web;
        this.rfc = rfc;
        this.unidad = claUni;
        this.cedula = cedPro;
        this.status = sts;
        this.Nacimiento = fecNac;
        this.folioInicial = folIni;
        this.folioFinal = folFin;
        this.folioActual = folAct;
        this.tipoConsulta = tipCons;
    }

    public Medico() {
        this.id = 0;
        this.web = 1;
        this.formatoFecha = "%d-%m-%Y";
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public void setNombreCompleto(String nombre, String paterno, String materno) {
        this.nombreCompleto = String.format("%s %s %s", nombre,
                paterno, materno);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWeb() {
        return web;
    }

    public void setWeb(int web) {
        this.web = web;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNacimiento() {
        return Nacimiento;
    }

    public void setNacimiento(String Nacimiento) {
        this.Nacimiento = Nacimiento;
    }

    public int getFolioInicial() {
        return folioInicial;
    }

    public void setFolioInicial(int folioInicial) {
        this.folioInicial = folioInicial;
    }

    public int getFolioFinal() {
        return folioFinal;
    }

    public void setFolioFinal(int folioFinal) {
        this.folioFinal = folioFinal;
    }

    public int getFolioActual() {
        return folioActual;
    }

    public void setFolioActual(int folioActual) {
        this.folioActual = folioActual;
    }

    public String getTipoConsulta() {
        return tipoConsulta;
    }

    public void setTipoConsulta(String tipoConsulta) {
        this.tipoConsulta = tipoConsulta;
    }

    public String getFormatoFecha() {
        return formatoFecha;
    }

    public void setFormatoFecha(String formatoFecha) {
        this.formatoFecha = formatoFecha;
    }

}
