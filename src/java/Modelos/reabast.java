/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 * Modelo datos reabastecimiento
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class reabast {

    private String clave;
    private String des;
    private int cdm;
    private int ifs;
    private int inv;
    private int sobreAbasto;
    private int cantSug;

    public reabast() {

    }

    public reabast(String clave, String des, int cdm, int ifs, int inv, int sobreAbasto, int cantSug) {
        this.clave = clave;
        this.des = des;
        this.cdm = cdm;
        this.ifs = ifs;
        this.inv = inv;
        this.sobreAbasto = sobreAbasto;
        this.cantSug = cantSug;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public int getCdm() {
        return cdm;
    }

    public void setCdm(int cdm) {
        this.cdm = cdm;
    }

    public int getIfs() {
        return ifs;
    }

    public void setIfs(int ifs) {
        this.ifs = ifs;
    }

    public int getInv() {
        return inv;
    }

    public void setInv(int inv) {
        this.inv = inv;
    }

    public int getSobreAbasto() {
        return sobreAbasto;
    }

    public void setSobreAbasto(int sobreAbasto) {
        this.sobreAbasto = sobreAbasto;
    }

    public int getCantSug() {
        return cantSug;
    }

    public void setCantSug(int cantSug) {
        this.cantSug = cantSug;
    }

}
