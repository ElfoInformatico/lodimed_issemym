/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 * Modelo datos usuarios
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class Usuario {

    public static final int ADMON_ROL = 0;
    public static final int FARMACIA_ROL = 1;
    public static final int MEDICO_ROL = 2;
    public static final int SUPERVISOR_ROL = 3;
    public static final int HL7_ROL = 4;
    public static final int RURAL_ROL = 5;
    
    private int id;
    private String nom;
    private String usu;
    private int rol;
    private int baja;
    private String desBaj;
    private String uni;
    private String desUni;
    private String cedula;
    private String mail;
    private String aP;
    private String aM;
    private String nivel, tipo;
    private String farm;

    public Usuario() {
    }

    public Usuario(int id, String nom, String usu, int rol, int baja, String desBaj, String uni, String desUni, String cedula, String mail, String aP, String aM, String farm) {
        this.id = id;
        this.nom = nom;
        this.usu = usu;
        this.rol = rol;
        this.baja = baja;
        this.desBaj = desBaj;
        this.uni = uni;
        this.desUni=desUni;
        this.cedula = cedula;
        this.mail = mail;
        this.aP = aP;
        this.aM = aM;
        this.farm = farm;
    }

    public String getFarm() {
        return farm;
    }

    public void setFarm(String farm) {
        this.farm = farm;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getUsu() {
        return usu;
    }

    public void setUsu(String usu) {
        this.usu = usu;
    }

    public int getRol() {
        return rol;
    }

    public void setRol(int rol) {
        this.rol = rol;
    }

    public int getBaja() {
        return baja;
    }

    public void setBaja(int baja) {
        this.baja = baja;
    }

    public String getDesBaj() {
        return desBaj;
    }

    public void setDesBaj(String desBaj) {
        this.desBaj = desBaj;
    }

    public String getUni() {
        return uni;
    }
    public String getDesUni() {
        return desUni;
    }
    public void setUni(String uni) {
        this.uni = uni;
    }
    public void setDesUni(String desUni) {
        this.desUni = desUni;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getaP() {
        return aP;
    }

    public void setaP(String aP) {
        this.aP = aP;
    }

    public String getaM() {
        return aM;
    }

    public void setaM(String aM) {
        this.aM = aM;
    }

}
