/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 * Modelo datos reporte diario
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class RegistroReporteDiario {

public RegistroReporteDiario(String fecha, String folioReceta, String medico, String paciente, String clave, String producto, String lote, String caducidad,
          String solicitado, String surtido, String financiamiento, String ampuleo, String origen) {
    this.fecha = fecha;
    this.folioReceta = folioReceta;
    this.medico = medico;
    this.paciente = paciente;
    this.clave = clave;
    this.producto = producto;
    this.lote = lote;
    this.caducidad = caducidad;
    this.solicitado = solicitado;
    this.surtido = surtido;
    this.financiamiento = financiamiento;
    this.ampuleo = ampuleo;
    this.origen = origen;
}

private String origen;

public String getOrigen() {
    return origen;
}

public void setOrigen(String fecha) {
    this.origen = fecha;
}

private String fecha;

public String getFecha() {
    return fecha;
}

public void setFecha(String fecha) {
    this.fecha = fecha;
}

private String folioReceta;

public String getFolioReceta() {
    return folioReceta;
}

public void setFolioReceta(String folioReceta) {
    this.folioReceta = folioReceta;
}

private String medico;

public String getMedico() {
    return medico;
}

public void setMedico(String medico) {
    this.medico = medico;
}

private String paciente;

public String getPaciente() {
    return paciente;
}

public void setPaciente(String paciente) {
    this.paciente = paciente;
}

private String clave;

public String getClave() {
    return clave;
}

public void setClave(String clave) {
    this.clave = clave;
}

private String producto;

public String getProducto() {
    return producto;
}

public void setProducto(String producto) {
    this.producto = producto;
}

private String lote;

public String getLote() {
    return lote;
}

public void setLote(String lote) {
    this.lote = lote;
}

private String caducidad;

public String getCaducidad() {
    return caducidad;
}

public void setCaducidad(String caducidad) {
    this.caducidad = caducidad;
}

private String solicitado;

public String getSolicitado() {
    return solicitado;
}

public void setSolicitado(String solicitado) {
    this.solicitado = solicitado;
}

private String surtido;

public String getSurtido() {
    return surtido;
}

public void setSurtido(String surtido) {
    this.surtido = surtido;
}

private String financiamiento;

public String getFinanciamiento() {
    return financiamiento;
}

public void setFinanciamiento(String financiamiento) {
    this.financiamiento = financiamiento;
}

private String ampuleo;

public String getAmpuleo() {
    return ampuleo;
}

public void setAmpuleo(String ampuleo) {
    this.ampuleo = ampuleo;
}

}
