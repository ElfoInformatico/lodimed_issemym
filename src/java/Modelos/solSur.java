/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 * Modelo datos solicitado y surtido
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class solSur {

    private String cu;
    private String nomUni;
    private int consecutivo;
    private int idRec;
    private String folio;
    private String clave;
    private String descrip;
    private int sol;
    private int sur;
    private String f1;
    private String f2;

    public solSur() {

    }

    public solSur(String cu, String nomUni, int consecutivo, String folio, String clave, String descrip, int sol, int sur, String f1, String f2, int idRec) {
        this.cu = cu;
        this.nomUni = nomUni;
        this.consecutivo = consecutivo;
        this.folio = folio;
        this.clave = clave;
        this.descrip = descrip;
        this.sol = sol;
        this.sur = sur;
        this.f1 = f1;
        this.f2 = f2;
        this.idRec = idRec;
    }

    public int getIdRec() {
        return idRec;
    }

    public void setIdRec(int idRec) {
        this.idRec = idRec;
    }

    public String getCu() {
        return cu;
    }

    public void setCu(String cu) {
        this.cu = cu;
    }

    public String getNomUni() {
        return nomUni;
    }

    public void setNomUni(String nomUni) {
        this.nomUni = nomUni;
    }

    public int getConsecutivo() {
        return consecutivo;
    }

    public void setConsecutivo(int consecutivo) {
        this.consecutivo = consecutivo;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public int getSol() {
        return sol;
    }

    public void setSol(int sol) {
        this.sol = sol;
    }

    public int getSur() {
        return sur;
    }

    public void setSur(int sur) {
        this.sur = sur;
    }

    public String getF1() {
        return f1;
    }

    public void setF1(String f1) {
        this.f1 = f1;
    }

    public String getF2() {
        return f2;
    }

    public void setF2(String f2) {
        this.f2 = f2;
    }
}
