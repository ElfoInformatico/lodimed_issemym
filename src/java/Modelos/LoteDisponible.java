package Modelos;

import org.json.simple.JSONObject;

/**
 * lote disponible para un codigo de barras.
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class LoteDisponible {

    public Inventario inventario;
    public DetalleProducto detalleProducto;
    int cantidadSolicitada;

    public int getCantidadSolicitada() {
        return cantidadSolicitada;
    }

    public void setCantidadSolicitada(int cantidadSolicitada) {
        this.cantidadSolicitada = cantidadSolicitada;
    }

    public LoteDisponible() {
        inventario = new Inventario();
        detalleProducto = new DetalleProducto();
    }

    @Override
    public String toString() {
        return "LoteDisponible{" + "inventario=" + inventario.toString() + ", detalleProducto=" + detalleProducto.toString() + ", cantidadSolicitada=" + cantidadSolicitada + '}';
    }

    public JSONObject toJSON() {

        JSONObject jo = new JSONObject();
        jo.put("id_inventario", this.inventario.id);

        jo.put("detalle_producto", this.detalleProducto.id);
        jo.put("clave", this.detalleProducto.clave);
        jo.put("lote", this.detalleProducto.lote);
        jo.put("caducidad", this.detalleProducto.caducidad);

        jo.put("solicitado", this.cantidadSolicitada);

        return jo;
    }

}
