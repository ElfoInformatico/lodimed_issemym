/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 * Modelo datos kardex consulta
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class Kardex {

    public static final String REINTEGRO_TIPO = "REINTEGRA AL INVENTARIO";
    public static final String MOVIMIENTO_SALIDA_RECETA_TIPO = "SALIDA RECETA";
    public static final String TRANSPASO_SALIDA_RECETA_TIPO = "SALIDA POR TRASPASO";
    public static final String AJUSTE_SALIDA_RECETA_TIPO = "SALIDA POR AJUSTE";
    public static final String TRANSPASO_ENTRADA_RECETA_TIPO = "ENTRADA POR TRASPASO";
    public static final String SIN_ABASTO = "-";
    public static final String MANUAL_OBSERVACION = "SALIDA POR RECETA FAR";
    public static final String COLECTIVA_OBSERVACION = "SALIDA POR RECETA COL";
    public static final String CANCELACION_DETALLE_OBSERVACION = "SE ELIMINA INSUMO DE RECETA";
    public static final String CANCELACION_RECETA_OBSERVACION = "SE CANCELA RECETA";
    public static final int ID_RECETA_ENTRADA_MOVIMIENTO = 1;
    public static final int ID_RECETA_SALIDA_MOVIMIENTO = 1;

    int id, detalleProducto, idReceta, cantidad, idUsuario;
    String tipoMovimiento, nombreAbasto, fechaMovimiento, observacion;

    public Kardex(int detalleProducto, int idReceta, int cantidad, int idUsuario, String tipoMovimiento, String nombreAbasto, String observacion) {
        this.detalleProducto = detalleProducto;
        this.idReceta = idReceta;
        this.cantidad = cantidad;
        this.idUsuario = idUsuario;
        this.tipoMovimiento = tipoMovimiento;
        this.nombreAbasto = nombreAbasto;
        this.observacion = observacion;
    }

    public Kardex() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDetalleProducto() {
        return detalleProducto;
    }

    public void setDetalleProducto(int detalleProducto) {
        this.detalleProducto = detalleProducto;
    }

    public int getIdReceta() {
        return idReceta;
    }

    public void setIdReceta(int idReceta) {
        this.idReceta = idReceta;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad >= 0 ? cantidad : cantidad * (-1);
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public String getNombreAbasto() {
        return nombreAbasto;
    }

    public void setNombreAbasto(String nombreAbasto) {
        this.nombreAbasto = nombreAbasto;
    }

    public String getFechaMovimiento() {
        return fechaMovimiento;
    }

    public void setFechaMovimiento(String fechaMovimiento) {
        this.fechaMovimiento = fechaMovimiento;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @Override
    public String toString() {
        return "Kardex{" + "id=" + id + ", detalleProducto=" + detalleProducto + ", idReceta=" + idReceta + ", cantidad=" + cantidad + ", tipoMovimiento=" + tipoMovimiento + ", nombreAbasto=" + nombreAbasto + ", fechaMovimiento=" + fechaMovimiento + ", observacion=" + observacion + ", idUsuario=" + idUsuario + '}';
    }
}
