/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 * Modelo de nivel
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class nivelModel {

    private String fec;
    private int pzSol;
    private int pzNoSur;
    private int claSol;
    private int claNoSur;
    private int recSol;
    private String recPar;
    private double porc;
    private int pzVen;
    private double impor;
    private int totales;
    private int count;

    public nivelModel() {
    }

    public nivelModel(String fec, int pzSol, int pzNoSur, int claSol, int claNoSur, int recSol, String recPar, double porc, int pzVen, double impor, int totales, int count) {
        this.fec = fec;
        this.pzSol = pzSol;
        this.pzNoSur = pzNoSur;
        this.claSol = claSol;
        this.claNoSur = claNoSur;
        this.recSol = recSol;
        this.recPar = recPar;
        this.porc = porc;
        this.pzVen = pzVen;
        this.impor = impor;
        this.totales = totales;
        this.count = count;
    }

    public String getFec() {
        return fec;
    }

    public void setFec(String fec) {
        this.fec = fec;
    }

    public int getPzSol() {
        return pzSol;
    }

    public void setPzSol(int pzSol) {
        this.pzSol = pzSol;
    }

    public int getPzNoSur() {
        return pzNoSur;
    }

    public void setPzNoSur(int pzNoSur) {
        this.pzNoSur = pzNoSur;
    }

    public int getClaSol() {
        return claSol;
    }

    public void setClaSol(int claSol) {
        this.claSol = claSol;
    }

    public int getClaNoSur() {
        return claNoSur;
    }

    public void setClaNoSur(int claNoSur) {
        this.claNoSur = claNoSur;
    }

    public int getRecSol() {
        return recSol;
    }

    public void setRecSol(int recSol) {
        this.recSol = recSol;
    }

    public String getRecPar() {
        return recPar;
    }

    public void setRecPar(String recPar) {
        this.recPar = recPar;
    }

    public double getPorc() {
        return porc;
    }

    public void setPorc(double porc) {
        this.porc = porc;
    }

    public int getPzVen() {
        return pzVen;
    }

    public void setPzVen(int pzVen) {
        this.pzVen = pzVen;
    }

    public double getImpor() {
        return impor;
    }

    public void setImpor(double impor) {
        this.impor = impor;
    }

    public int getTotales() {
        return totales;
    }

    public void setTotales(int totales) {
        this.totales = totales;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
