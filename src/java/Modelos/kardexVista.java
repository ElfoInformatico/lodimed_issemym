/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 * Modelo datos kardex
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class kardexVista {

    private String clave;
    private String lote;
    private String caducidad;
    private int origen;
    private int cantidad;
    private String tipoMovimiento;
    private String abasto;
    private String folioReceta;
    private String paciente;
    private String medico;
    private String fecha;
    private String observaciones;
    private int sumatoria;
    private String descripcion;
    private int existencias;
    private String usuario;

    public kardexVista() {
    }

    public kardexVista(String clave, String lote, String caducidad, int origen, int cantidad, String tipoMovimiento, String abasto, String folioReceta, String paciente, String medico, String fecha, String observaciones, int sumatoria, String descripcion, int existencias) {
        this.clave = clave;
        this.lote = lote;
        this.caducidad = caducidad;
        this.origen = origen;
        this.cantidad = cantidad;
        this.tipoMovimiento = tipoMovimiento;
        this.abasto = abasto;
        this.folioReceta = folioReceta;
        this.paciente = paciente;
        this.medico = medico;
        this.fecha = fecha;
        this.observaciones = observaciones;
        this.sumatoria = sumatoria;
        this.descripcion = descripcion;
        this.existencias = existencias;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getCaducidad() {
        return caducidad;
    }

    public void setCaducidad(String caducidad) {
        this.caducidad = caducidad;
    }

    public int getOrigen() {
        return origen;
    }

    public void setOrigen(int origen) {
        this.origen = origen;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public String getAbasto() {
        return abasto;
    }

    public void setAbasto(String abasto) {
        this.abasto = abasto;
    }

    public String getFolioReceta() {
        return folioReceta;
    }

    public void setFolioReceta(String folioReceta) {
        this.folioReceta = folioReceta;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public String getMedico() {
        return medico;
    }

    public void setMedico(String medico) {
        this.medico = medico;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public int getSumatoria() {
        return sumatoria;
    }

    public void setSumatoria(int sumatoria) {
        this.sumatoria = sumatoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getExistencias() {
        return existencias;
    }

    public void setExistencias(int existencias) {
        this.existencias = existencias;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

}
