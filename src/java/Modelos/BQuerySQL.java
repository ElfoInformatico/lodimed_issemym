/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Modelo MQuerySQL
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class BQuerySQL {

    private PreparedStatement preStatemnt;
    private Statement statemnt;
    private ResultSet rs;
    private int indexParameter;

    public BQuerySQL() {
        this.preStatemnt = null;
        this.statemnt = null;
        this.rs = null;
        this.indexParameter = 1;
    }

    public BQuerySQL(PreparedStatement preStatemnt, Statement statemnt, ResultSet rs) {
        this.preStatemnt = preStatemnt;
        this.statemnt = statemnt;
        this.rs = rs;
        this.indexParameter = 1;
    }

    public PreparedStatement getPreStatemnt() {
        return preStatemnt;
    }

    public void setPreStatemnt(PreparedStatement preStatemnt) {
        this.preStatemnt = preStatemnt;
    }

    public Statement getStatemnt() {
        return statemnt;
    }

    public void setStatemnt(Statement statemnt) {
        this.statemnt = statemnt;
    }

    public ResultSet getRs() {
        return rs;
    }

    public void setRs(ResultSet rs) {
        this.rs = rs;
    }

    public void addParameterString(String data) {
        try {
            this.preStatemnt.setString(this.indexParameter, data);
            this.indexParameter += 1;
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            System.out.println("**Cola**");
            e.printStackTrace();
        }
    }

    public void addParameterInt(int data) {
        try {
            this.preStatemnt.setInt(this.indexParameter, data);
            this.indexParameter += 1;
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            System.out.println("**Cola**");
            e.printStackTrace();
        }
    }

    public void executeQueryPS() {
        try {
            this.rs = this.preStatemnt.executeQuery();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            System.out.println("**Cola**");
            e.printStackTrace();
        }
    }

    public int executeUpdatePS() {
        try {
            return this.preStatemnt.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            System.out.println("**Cola**");
            e.printStackTrace();
        }
        return 0;
    }
}
