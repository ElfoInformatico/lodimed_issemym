/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 * Consulta detalles del medicamento
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class DetalleProducto {

public static final String CADUCIDAD_FALTANTE = "2020-01-01";
public static final String LOTE_FALTANTE = "-";

int id, origen;
String clave, lote, caducidad, origenDescripcion;

public int getId() {
    return id;
}

public void setId(int id) {
    this.id = id;
}

public int getOrigen() {
    return origen;
}

public void setOrigen(int origen) {
    this.origen = origen;
}

public String getClave() {
    return clave;
}

public void setClave(String clave) {
    this.clave = clave;
}

public String getLote() {
    return lote;
}

public void setLote(String lote) {
    this.lote = lote;
}

public String getCaducidad() {
    return caducidad;
}

public void setCaducidad(String caducidad) {
    this.caducidad = caducidad;
}

public String getOrigenDescripcion() {
    return origenDescripcion;
}

public void setOrigenDescripcion(String origenDescripcion) {
    this.origenDescripcion = origenDescripcion;
}

public boolean isFaltante() {

    return (this.getCaducidad().equals(DetalleProducto.CADUCIDAD_FALTANTE))
              && (this.getLote().equals(DetalleProducto.LOTE_FALTANTE));
}

@Override
public String toString() {
    return "DetalleProducto{" + "id=" + id + ", origen=" + origen + ", clave=" + clave + ", lote=" + lote + ", caducidad=" + caducidad + '}';
}

}
