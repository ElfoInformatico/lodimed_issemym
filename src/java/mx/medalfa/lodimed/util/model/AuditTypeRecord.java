package mx.medalfa.lodimed.util.model;

/**
 * Indica el tipo de operación que se desea registrar en el sistema. 
 * 
 * @author Sebastian
 */
public enum AuditTypeRecord {

    INSERT(1),
    UPDATE(2),
    DELETE(3);

    public final int value;

    AuditTypeRecord(int value) {
        this.value = value;
    }
}
