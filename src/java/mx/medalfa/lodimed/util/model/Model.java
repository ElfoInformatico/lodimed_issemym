package mx.medalfa.lodimed.util.model;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import mx.medalfa.lodimed.util.db.ConnectionManager;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Encapsula los metodos para realizar INSERTs y UPDATEs sobre la base de datos.
 * Además permite la gestión de los registros de auditoría automáticamente.
 *
 * @author Sebastian
 */
public abstract class Model {

    protected int id;
    private final String table;
    protected int owner;
    protected final Connection connection;
    protected boolean isUpdated;
    protected PreparedStatement auditRecords;
    protected Map<String, String> dbFields;

    public static final int EMPTY_INT = -1;
    public static final String OLD_VALUE_INSERT = "-";
    public static final String STRING_TYPE = "String";
    public static final String BIGDECIMAL_TYPE = "BigDecimal";
    public static final String INT_TYPE = "int";

    public static final String DEFAULT_INIT_DATE_FORMATED = "dd/MM/yyyy";
    public static final String DEFAULT_ENDED_DATE_FORMATED = "yyyy-MM-dd";
    
    private static String NO_SE_REALIZO_NINGUN_CAMBIO = "No se realizo ningún cambio, por favor verifique.";

    /**
     * Permite la creación de un modelo en la base de datos.
     *
     * @param table Nombre que posee en la base de datos.
     * @param con Conexión directa a la base de datos.
     * @param owner Quién esta realizando las modificaciones sobre el objeto.
     */
    protected Model(String table, Connection con, int owner) {
        this.table = table;
        this.connection = con;
        this.id = EMPTY_INT;
        this.isUpdated = false;
        this.owner = owner;
    }
    
    /**
     * Evalua si el objeto no tiene una representación en la db.
     * 
     * @return Si el objeto tiene un id igual a EMPTY_INT({@value #EMPTY_INT}).
     */
    protected boolean isNew() {
        return this.id == EMPTY_INT;
    }
    
    /**
     * Guarda el objeto en caso que sea nuevo, sino realiza una actualización sobre
     * la tabla del objeto y realiza el reporte sobre auditoria.
     * 
     * @see #isNew()
     * @see AuditTypeRecord
     * 
     * @throws IllegalArgumentException 
     * 
     * @throws IllegalArgumentException En caso de que no cumpla las validaciones
     * incluidas en el método {@link #checkAttributesToInsert()}. Tambien en caso 
     * que se intente guardar un objeto sin modificaciones o nuevo sin datos.
     * 
     * @throws SQLException En caso de realizar una operación indebida en la db,
     * por ejemplo, no insertar campos requeridos, valores de logitud permitido 
     * en el campo, etc.
     */
    public void save() throws IllegalAccessException, SQLException {

        //Registro nuevo.
        if (this.isNew()) {
            internalSave();
        } else if (isUpdated) {
            update();
        } else {
            throw new IllegalArgumentException(NO_SE_REALIZO_NINGUN_CAMBIO);
        }

        postSave();
    }

    private void postSave() throws SQLException, IllegalAccessException {
        updateAduitRecords();
        afterSave();
    }

    private void update() throws IllegalAccessException, SQLException {
        try (PreparedStatement ps = connection.prepareStatement(getUpdateQuery())) {
            prepareUpdateQuery(ps);
            ConnectionManager.checkUpdatedSuccessfully(ps, this.getClass().getName(), id);
        }
    }

    private void internalSave() throws SQLException,
            IllegalAccessException {

        //Verifica la valides de los atributos.
        this.checkAttributesToInsert();

        try (PreparedStatement ps = connection.prepareStatement(getInsertQuery(), PreparedStatement.RETURN_GENERATED_KEYS)) {
            prepareInsertQuery(ps);
            ConnectionManager.checkUpdatedSuccessfully(ps, this.getClass().getName(), id);
            setId(ConnectionManager.getLastRow(ps));
            setInsertAuditRecords();
        }
    }

    /**
     * Permite eliminar este registro de la db. Este metodo no se fija en los
     * registros hijo de este registro, si desea que se eliminen también es
     * necesario poner la restricción desde el motor de base de datos.
     *
     * @throws java.sql.SQLException
     */
    public void delete() throws SQLException {

        checkBeforeDelete();

        String query = String.format("DELETE FROM `%s` WHERE `id` = ?;", this.table);
        try (PreparedStatement ps = this.connection.prepareStatement(query)) {
            ps.setInt(1, this.id);
            ConnectionManager.checkUpdatedSuccessfully(ps, this.getClass().getName(), id);
        }

        try (PreparedStatement psAudit = getQueryAuditRecords(connection)) {
            addAduitRecord(psAudit, owner, table, this.id, "id",
                    AuditTypeRecord.DELETE, OLD_VALUE_INSERT, String.valueOf(this.id));
            ConnectionManager.checkUpdatedSuccessfullyBatch(psAudit, this.getClass().getName(), id);
        }
    }

    protected void updateAduitRecords() throws SQLException {
        ConnectionManager.checkUpdatedSuccessfullyBatch(auditRecords,
                this.getClass().getName(), EMPTY_INT);
        this.auditRecords.clearBatch();
    }

    protected void addAduitRecord(String field, AuditTypeRecord type,
            String newValue, String oldValue) throws SQLException {

        this.auditRecords = getAuditRecords();
        addAduitRecord(this.auditRecords, this.owner, this.table, this.id, field, type, newValue, oldValue);
    }

    protected PreparedStatement getAuditRecords() throws SQLException {
        if (this.auditRecords == null) {
            this.auditRecords = getQueryAuditRecords(connection);
        }

        return this.auditRecords;
    }

    protected static PreparedStatement getQueryAuditRecords(Connection con) throws SQLException {
        return con.prepareStatement("INSERT INTO `registros_auditoria` ( `usuario`, `tabla`, `id_registro`, `campo`, `tipo`, `valor_nuevo`, `valor_anterior` ) VALUES ( ?, ?, ?, ?, ?, ?, ?);");
    }

    protected static PreparedStatement addAduitRecord(PreparedStatement records, int owner, String table,
            int id, String field, AuditTypeRecord type, String newValue, String oldValue) throws SQLException {

        records.setInt(1, owner);
        records.setString(2, table);
        records.setInt(3, id);
        records.setString(4, field);
        records.setInt(5, type.value);
        records.setString(6, newValue);
        records.setString(7, oldValue);
        records.addBatch();

        return records;
    }

    protected static PreparedStatement addAduitRecord(PreparedStatement records, int owner, String table,
            int id, String field, int newValue) throws SQLException {

        String nValue = String.valueOf(newValue);

        return addAduitRecord(records, owner, table, id, field, AuditTypeRecord.INSERT, nValue, OLD_VALUE_INSERT);
    }

    protected static PreparedStatement addAduitRecord(PreparedStatement records, int owner, String table,
            int id, String field, AuditTypeRecord type, int newValue, int oldValue) throws SQLException {

        String nValue = String.valueOf(newValue);
        String oValue = String.valueOf(oldValue);

        return addAduitRecord(records, owner, table, id, field, type, nValue, oValue);
    }

    protected static PreparedStatement addAduitRecord(PreparedStatement records, int owner, String table,
            int id, String field, AuditTypeRecord type, BigDecimal newValue, BigDecimal oldValue) throws SQLException {

        String nValue = String.valueOf(newValue);
        String oValue = String.valueOf(oldValue);

        return addAduitRecord(records, owner, table, id, field, type, nValue, oValue);
    }

    protected static PreparedStatement addAduitRecord(PreparedStatement records, int owner, String table,
            int id, String field, AuditTypeRecord type, BigDecimal newValue) throws SQLException {

        String nValue = String.valueOf(newValue);

        return addAduitRecord(records, owner, table, id, field, type, nValue, OLD_VALUE_INSERT);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    protected void set(boolean isUpdated, String columnName,
            int oldValue, int newValue) throws SQLException {
        this.set(isUpdated, columnName, String.valueOf(oldValue), String.valueOf(newValue));

    }

    protected void set(boolean isUpdated, String columnName,
            BigDecimal oldValue, BigDecimal newValue) throws SQLException {
        this.set(isUpdated, columnName, oldValue.toPlainString(), newValue.toPlainString());

    }

    protected void set(boolean isUpdated, String columnName,
            String oldValue, String newValue) throws SQLException {
        if (isUpdated) {
            this.isUpdated = true;
            this.addAduitRecord(columnName, AuditTypeRecord.UPDATE, newValue, oldValue);
        }

    }

    private boolean isEmptyField(Object newValue) {

        if (newValue == null) {
            return true;
        }

        if (String.class.isInstance(newValue) && newValue.toString().isEmpty()) {
            return true;
        }

        return (Integer.class.isInstance(newValue)
                && Integer.valueOf(newValue.toString()) == EMPTY_INT);
    }

    protected void setInsertAuditRecords() throws
            IllegalAccessException, SQLException {
        Field[] allFields = this.getClass().getDeclaredFields();

        this.auditRecords = null;
        this.dbFields = this.getFieldsMap();
        Object newValue;
        for (Field each : allFields) {
            if (dbFields.containsKey(each.getName())) {

                each.setAccessible(true);
                newValue = each.get(this);

                if (isEmptyField(newValue)) {
                    continue;
                }

                this.addAduitRecord(dbFields.get(each.getName()), AuditTypeRecord.INSERT,
                        String.valueOf(newValue), OLD_VALUE_INSERT);
            }
        }
    }

    protected Field[] getAllDeclaredFields() {
        Field[] allFieldsChild = this.getClass().getDeclaredFields();
        Field[] allFieldsParent = this.getClass().getSuperclass().getDeclaredFields();
        Field[] allFields = new Field[allFieldsChild.length + allFieldsParent.length];

        System.arraycopy(allFieldsChild, 0, allFields, 0, allFieldsChild.length);
        System.arraycopy(allFieldsParent, 0, allFields, allFieldsChild.length, allFieldsParent.length);

        return allFields;
    }

    protected String getInsertQuery() throws IllegalAccessException {
        StringBuilder query = new StringBuilder("INSERT INTO `");
        query.append(this.table);
        query.append("` SET ");

        Field[] allFields = getAllDeclaredFields();

        this.dbFields = this.getFieldsMap();
        Object newValue;
        String delimiter = "";
        for (Field each : allFields) {
            if (dbFields.containsKey(each.getName())) {
                each.setAccessible(true);
                newValue = each.get(this);

                if (isEmptyField(newValue)) {
                    continue;
                }

                query.append(delimiter);
                query.append("`");
                query.append(dbFields.get(each.getName()));
                query.append("`=?");
                delimiter = ", ";
            }
        }

        return query.toString();
    }

    protected String getUpdateQuery() throws IllegalAccessException {
        String query = getInsertQuery().replace("INSERT INTO ", "UPDATE ");
        query = String.format("%s WHERE `id`=?", query);

        return query;
    }

    protected PreparedStatement prepareUpdateQuery(PreparedStatement ps) throws
            IllegalAccessException, SQLException {
        return prepareUpdateQuery(ps, false);
    }

    protected PreparedStatement prepareUpdateQuery(PreparedStatement ps,
            boolean setPrevious) throws IllegalAccessException, SQLException {

        ps = prepareInsertQuery(ps, setPrevious);

        // Asigna el ID al where del update.
        ParameterMetaData metaData = ps.getParameterMetaData();
        ps.setInt(metaData.getParameterCount(), id);

        return ps;
    }

    protected PreparedStatement prepareInsertQuery(PreparedStatement ps)
            throws IllegalAccessException, SQLException {
        return prepareInsertQuery(ps, false);
    }

    protected PreparedStatement prepareInsertQuery(PreparedStatement ps,
            boolean setPrevious) throws IllegalAccessException, SQLException {

        Field[] allFields = getAllDeclaredFields();

        this.dbFields = this.getFieldsMap();
        Object newValue;
        int column = 1;
        for (Field each : allFields) {

            if (dbFields.containsKey(each.getName())) {
                each.setAccessible(true);
                newValue = each.get(this);

                if (isEmptyField(newValue)) {
                    if (setPrevious) {
                        ps.setNull(column, Types.VARCHAR);
                        column++;
                    }
                    continue;
                }

                switch (each.getType().getSimpleName()) {

                    case STRING_TYPE:
                        ps.setString(column, (String) newValue);
                        break;
                    case BIGDECIMAL_TYPE:
                        ps.setBigDecimal(column, (BigDecimal) newValue);
                        break;
                    case INT_TYPE:
                        ps.setInt(column, each.getInt(this));
                        break;
                    default:
                        throw new IllegalArgumentException("Tipo de dato no soportado para el mapeo.");
                }

                column++;
            }
        }

        return ps;
    }

    protected String getInsertQueryValuesSyntax() throws IllegalAccessException {
        StringBuilder query = new StringBuilder("INSERT INTO `");
        query.append(this.table);
        query.append("` ( ");

        StringBuilder columns = new StringBuilder();
        StringBuilder values = new StringBuilder();

        Field[] allFields = this.getClass().getDeclaredFields();

        this.dbFields = this.getFieldsMap();
        Object newValue;
        String delimiter = "";
        String defaultValue;
        for (Field each : allFields) {
            if (dbFields.containsKey(each.getName())) {
                each.setAccessible(true);
                newValue = each.get(this);
                defaultValue = "?";

                if (isEmptyField(newValue)) {
                    defaultValue = String.format("COALESCE(?, DEFAULT(`%s`))", dbFields.get(each.getName()));
                }

                columns.append(delimiter);
                columns.append("`");
                columns.append(dbFields.get(each.getName()));
                columns.append("`");
                values.append(delimiter);
                values.append(defaultValue);
                delimiter = ", ";
            }
        }

        query.append(columns.toString());
        query.append(" ) VALUES ( ");
        query.append(values.toString());
        query.append(" );");

        return query.toString();
    }

    protected String getUpdateQueryBatchSyntax() {
        StringBuilder query = new StringBuilder("UPDATE `");
        query.append(this.table);
        query.append("` SET ");

        Field[] allFields = getAllDeclaredFields();

        this.dbFields = this.getFieldsMap();
        String delimiter = "";
        for (Field each : allFields) {
            if (dbFields.containsKey(each.getName())) {

                query.append(delimiter);
                query.append("`");
                query.append(dbFields.get(each.getName()));
                query.append("` = IFNULL(?, `");
                query.append(dbFields.get(each.getName()));
                query.append("`)");
                delimiter = ", ";
            }
        }
        query.append(" WHERE `id`=?");

        return query.toString();
    }

    public static void saveList(List<? extends Model> list) throws SQLException,
            IllegalAccessException {

        if (list.isEmpty()) {
            return;
        }

        String query = "";
        String queryUpdate = list.get(0).getUpdateQueryBatchSyntax();
        for (Model aux : list) {
            if (aux.isNew()) {
                query = aux.getInsertQueryValuesSyntax();
                break;
            }
        }

        Connection con = list.get(0).connection;
        List<Model> listInsert = new ArrayList<>();
        List<Model> listUpdate = new ArrayList<>();

        try (PreparedStatement ps = con.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
                PreparedStatement psUpdate = con.prepareStatement(queryUpdate)) {
            for (Model aux : list) {
                if (aux.isNew()) {
                    listInsert.add(aux);
                    aux.checkAttributesToInsert();
                    aux.prepareInsertQuery(ps, true);
                    ps.addBatch();
                } else if (aux.isUpdated) {
                    listUpdate.add(aux);
                    aux.prepareUpdateQuery(psUpdate, true);
                    psUpdate.addBatch();
                }
            }

            if (listInsert.isEmpty() && listUpdate.isEmpty()) {
                throw new IllegalArgumentException(NO_SE_REALIZO_NINGUN_CAMBIO);
            }

            if (!listInsert.isEmpty()) {
                ConnectionManager.checkUpdatedSuccessfullyBatch(ps,
                        list.get(0).getClass().getName(), EMPTY_INT);
                int cont = 0;
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    Model aux;
                    while (rs.next()) {
                        aux = listInsert.get(cont);
                        aux.setId(rs.getInt(1));
                        aux.setInsertAuditRecords();
                        aux.postSave();
                        cont++;
                    }
                }
            }

            if (!listUpdate.isEmpty()) {
                ConnectionManager.checkUpdatedSuccessfullyBatch(psUpdate,
                        list.get(0).getClass().getName(), EMPTY_INT);
            }

            for (Model aux : listUpdate) {
                aux.postSave();
            }
        }
    }

    protected PreparedStatement addAllRelatedIDs(PreparedStatement ps,
            Integer[] entitiesIds) throws SQLException {
        for (int idMany : entitiesIds) {
            ps.setInt(1, this.id);
            ps.setInt(2, idMany);
            ps.addBatch();
        }
        return ps;
    }

    public void deleteRelatedEntities(Integer[] entitiesIds, String table,
            String columnNameOnly, String columnNameMany) throws SQLException {

        String query = String.format("SELECT id FROM %s WHERE %s = ? AND %s IN (%s) FOR UPDATE;", table,
                columnNameOnly, columnNameMany, ConnectionManager.joinArray(entitiesIds, ","));

        try (PreparedStatement psIDs = connection.prepareStatement(query,
                ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE)) {
            psIDs.setInt(1, this.id);
            try (ResultSet rsIDs = psIDs.executeQuery()) {

                query = String.format("DELETE FROM %s WHERE %s = ? AND %s = ?;", table,
                        columnNameOnly, columnNameMany);
                try (PreparedStatement ps = connection.prepareStatement(query)) {
                    addAllRelatedIDs(ps, entitiesIds);
                    ConnectionManager.checkUpdatedSuccessfullyBatch(ps, this.getClass().getName(), id);

                }

                try (PreparedStatement psAudit = getQueryAuditRecords(connection)) {
                    int aux;
                    while (rsIDs.next()) {
                        aux = rsIDs.getInt(1);
                        addAduitRecord(psAudit, owner, table, aux, columnNameOnly,
                                AuditTypeRecord.DELETE, OLD_VALUE_INSERT, String.valueOf(aux));
                    }
                    ConnectionManager.checkUpdatedSuccessfullyBatch(psAudit, this.getClass().getName(), id);
                }
            }
        }
    }

    /**
     * Permite añadir registros a tablas de uno a muchos.
     *
     * @param entitiesIds contiene todos los IDs de la relación de muchos.
     * @param table nombre de la tabla donde se realizará la inserción.
     * @param columnNameOnly nombre del campo de la relación de uno.
     * @param columnNameMany nombre del campo de la relación de muchos.
     * @throws SQLException
     */
    public void addRelatedEntities(Integer[] entitiesIds, String table,
            String columnNameOnly, String columnNameMany) throws SQLException {

        String query = String.format("INSERT INTO %s (`%s`, `%s`) VALUES (?,?);", table,
                columnNameOnly, columnNameMany);
        try (PreparedStatement ps = connection.prepareStatement(query, new String[]{"id"})) {
            addAllRelatedIDs(ps, entitiesIds);
            ConnectionManager.checkUpdatedSuccessfullyBatch(ps, this.getClass().getName(), id);

            try (ResultSet rs = ps.getGeneratedKeys();
                    PreparedStatement psAudit = getQueryAuditRecords(connection)) {
                int aux;
                int cont = 0;
                while (rs.next()) {
                    aux = rs.getInt(1);
                    addAduitRecord(psAudit, owner, table, aux, columnNameOnly, this.id);
                    addAduitRecord(psAudit, owner, table, aux, columnNameMany, entitiesIds[cont]);
                    cont++;
                }
                ConnectionManager.checkUpdatedSuccessfullyBatch(psAudit, this.getClass().getName(), id);
            }
        }
    }

    /**
     * Regresa un único registro convertido en JSON del {@link PreparedStatement}
     * enviado. Los nombres para el objeto se basa en los alias 
     * o nombres del campo en la consulta. Por ejemplo:<p>
     * 
     * {@code "SELECT u.user, u.password AS ´pwd´ FROM usuarios"}, lo convertiría a:<p>
     * {@code {user:"test", pwd: "XXXX"}}
     *
     * @param ps {@link PreparedStatement} con la información del query.
     * @return
     * @throws SQLException Si el {@link ResultSet} no tiene registros.
     */
    protected static JSONObject getObject(PreparedStatement ps) throws SQLException {
        return getObject(ps, false);
    }

    /**
     * Regresa un único registro convertido en JSON del {@link PreparedStatement}
     * enviado. Además evalua si posee o no registros en el {@link ResultSet}, Si isNull
     * es true. se retorna un JSON vacio. Los nombres para el objeto se basa en los alias 
     * o nombres del campo en la consulta. Por ejemplo:<p>
     * 
     * {@code "SELECT u.user, u.password AS ´pwd´ FROM usuarios"}, lo convertiría a:<p>
     * {@code {user:"test", pwd: "XXXX"}}
     *
     * @see JSONObject
     * 
     * @param ps {@link PreparedStatement} con la información del query.
     * @param isNull Validación para regresar JSON vacio.
     * 
     * @return {@link JSONObject} con la conversion del query.
     * 
     * @throws SQLException Si isNull es {@code false} lanzara esta excepción si el
     * {@link ResultSet} no tiene registros.
     */
    protected static JSONObject getObject(PreparedStatement ps, boolean isNull) throws SQLException {
        JSONObject row;
        try (ResultSet rs = ps.executeQuery()) {
            ResultSetMetaData meta = rs.getMetaData();
            int columns = meta.getColumnCount();
            row = new JSONObject();

            if (isNull && !rs.isBeforeFirst()) {
                return row;
            }

            rs.next();
            String columnName;
            for (int i = 1; i <= columns; i++) {
                columnName = meta.getColumnLabel(i);
                columnName = columnName == null ? meta.getColumnName(i) : columnName;
                row.put(columnName, rs.getString(i));
            }
        }
        return row;
    }
    
    /**
     * Permite convertir el resultado de un query a un array de objetos JSON, este
     * se basa en los alias o nombres del campo en la consulta. Por ejemplo:<p>
     * 
     * {@code "SELECT u.user, u.password AS ´pwd´ FROM usuarios"}, lo convertiría a:<p>
     * {@code [{user:"test", pwd: "XXXX"}, {user:"user2", pwd: "YYYY"}]}
     * 
     * @see JSONArray
     * @see JSONObject
     * 
     * @param ps {@link PreparedStatement} con el query a convertir.
     * @return {@link JSONArray}
     * @throws SQLException 
     */
    protected static JSONArray getListAsObjectArray(PreparedStatement ps) throws SQLException {
        ResultSet rs = ps.executeQuery();
        JSONArray result = new JSONArray();
        JSONObject row;
        ResultSetMetaData meta = rs.getMetaData();
        int columns = meta.getColumnCount();
        String columnName;

        while (rs.next()) {
            row = new JSONObject();
            for (int i = 1; i <= columns; i++) {
                columnName = meta.getColumnLabel(i);
                columnName = columnName == null ? meta.getColumnName(i) : columnName;
                row.put(columnName, rs.getString(i));
            }
            result.put(row);
        }

        return result;
    }
    
    /**
     * Permite convertir el resultado de un query a un array de array en JSON, este
     * se basa en los alias o nombres del campo en la consulta. Por ejemplo:<p>
     * 
     * {@code "SELECT u.user, u.password AS ´pwd´ FROM usuarios"}, lo convertiría a:<p>
     * {@code [["test", "XXXX"], ["user2","YYYY"]]}
     * 
     * @see JSONArray
     * @see JSONObject
     * 
     * @param ps {@link PreparedStatement} con el query a convertir.
     * @return {@link JSONArray}
     * @throws SQLException 
     */
    protected static JSONArray getListAsArray(PreparedStatement ps) throws SQLException {
        ResultSet rs = ps.executeQuery();
        int totalColumns = rs.getMetaData().getColumnCount();
        if (totalColumns == 1) {
            return getListAsArrayWithOneColumn(rs);
        }

        return getListAsArray(rs, totalColumns);
    }

    private static JSONArray getListAsArrayWithOneColumn(ResultSet rs) throws SQLException {

        JSONArray result = new JSONArray();
        while (rs.next()) {
            result.put(rs.getString(1));
        }

        return result;
    }

    private static JSONArray getListAsArray(ResultSet rs, int columns) throws SQLException {

        JSONArray result = new JSONArray();
        JSONArray row;

        while (rs.next()) {
            row = new JSONArray();
            for (int i = 1; i <= columns; i++) {
                row.put(rs.getString(i));
            }
            result.put(row);
        }

        return result;
    }

    protected String getDefaultDate(String date) throws ParseException {
        return getDateFormated(date, DEFAULT_INIT_DATE_FORMATED, DEFAULT_ENDED_DATE_FORMATED);
    }

    protected static String getDefaultDate() {
        SimpleDateFormat format = new SimpleDateFormat(DEFAULT_ENDED_DATE_FORMATED);

        return format.format(new Date());
    }

    protected String getDateFormated(String date, String initFormat,
            String endedFormat) throws ParseException {

        SimpleDateFormat format = new SimpleDateFormat(initFormat);
        SimpleDateFormat format2 = new SimpleDateFormat(endedFormat);

        return format2.format(format.parse(date));
    }

    public boolean isDateAfterToday(String date) throws ParseException {

        return isDateAfterToday(date, DEFAULT_ENDED_DATE_FORMATED);
    }

    public boolean isDateAfterToday(String date, String format) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(format);

        return formatter.parse(date).before(new Date());
    }

    /**
     * Permite el mapeo de los campos del modelo y los campos en la tabla de la
     * base de datos.
     *
     * @return
     */
    protected abstract Map<String, String> getFieldsMap();
    
    /**
     * Permite definir las condiciones que se deben evaluar en el objeto antes
     * de su insercción.
     * 
     * 
     * @throws IllegalArgumentException En caso de que no cumpla las validaciones
     * incluidas en el método.
     * 
     * @throws SQLException En caso de no se pueda realizar una operación sobre la 
     * db, por ejemplo, busqueda para verificación de un parámetro.
     */
    protected abstract void checkAttributesToInsert() throws SQLException;
    
    /**
     * En caso que se desee verificar o relizar una acción después del guardo de 
     * un objeto (tipo trigger) deberá sobre escribirse este método.
     * 
     * @throws SQLException 
     */
    protected abstract void afterSave() throws SQLException;
    
    /**
     * En caso que se desee verificar o relizar una acción antes de eliminar 
     * un objeto (tipo trigger) deberá sobre escribirse este método.
     * 
     * @throws SQLException 
     */
    protected abstract void checkBeforeDelete() throws SQLException;
}
