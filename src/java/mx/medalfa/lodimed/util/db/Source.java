package mx.medalfa.lodimed.util.db;

/**
 * Permite acceder a los recursos definidos para las conexiones de la aplicación.
 * La definición de estos se encuentra en el archivo context.xml.
 * 
 * @author Sebastian
 */
public enum Source {

    USER_POST("java:comp/env/jdbc/usuario_post"),
    USER_GET("java:comp/env/jdbc/usuario_get");

    public final String text;

    private Source(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return String.format("%s:%s", this.name(), this.text);
    }

}
