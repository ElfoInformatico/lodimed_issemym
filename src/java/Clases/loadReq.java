/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Carga del requerimiento
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class loadReq 
{
    private final String DELETE_ABASTO = "DELETE FROM carga_requerimiento;";
    private final String INSERT_ABASTO = "INSERT INTO carga_requerimiento (clave_unidad, clave, cant,observaciones,cargado) VALUES (?,?,?,?,?);";
    private static final String selectProduc = "SELECT productos.cla_pro FROM productos WHERE productos.f_status='A'";
    private final int TOTAL_CAMPOS = 2;
    private static final ArrayList<String> claves = new ArrayList();  
    
    public static enum EstadoAbasto {

        CARGO(1), NO_CARGO(0);
        private final int value;

        private EstadoAbasto(int value) {
            this.value = value;
        }

        public static EstadoAbasto fromInt(int x) {
            switch (x) {
                case 1:
                    return CARGO;
                case 0:
                    return NO_CARGO;
            }
            return null;
        }
    }

    public static enum EstadoCheck {

        SIN_ERROR(0), ERROR(1), EXCEPCION(2);
        private final int value;

        private EstadoCheck(int value) {
            this.value = value;
        }

        public static EstadoCheck fromInt(int x) {
            switch (x) {
                case 0:
                    return SIN_ERROR;
                case 1:
                    return ERROR;
                case 2:
                    return EXCEPCION;
            }
            return null;
        }

    }
    
    public boolean leeCSV(String ruta, String clave_uni) throws IOException 
    {

      
        DecimalFormat formatter = new DecimalFormat("0000");
        DecimalFormat formatterDeci = new DecimalFormat("0000.00");
        DecimalFormat formatterDeci2 = new DecimalFormat("0000.0");
        ConectionDB con = new ConectionDB();

        DecimalFormatSymbols puntoSimbolo = new DecimalFormatSymbols(Locale.US);
        formatterDeci.setDecimalFormatSymbols(puntoSimbolo);
        formatterDeci2.setDecimalFormatSymbols(puntoSimbolo);
        
        String cvsSplitBy = ",";
        int lineaActual = 0;
        int campoActual = 0;
        String valorActual = "";
        boolean coincidio, terminoCorrectamente;
        EstadoAbasto estado;
        BufferedReader br = null;
        try {
            String line = "";
            con.conectar();
            PreparedStatement ps;
            ResultSet consulta;

            ps = con.getConn().prepareStatement(DELETE_ABASTO);
            ps.executeUpdate();

            br = new BufferedReader(new FileReader(ruta));

            String clave,auxCantidad,observacion;
            
            int cantidad, resultado;

            //System.out.println(System.getProperty("user.home"));
            while ((line = br.readLine()) != null) {
                lineaActual += 1;
                campoActual = 0;
                valorActual = "";
                estado = EstadoAbasto.CARGO;
                observacion = "";

                String[] linea = line.split(cvsSplitBy);
                if (linea.length != TOTAL_CAMPOS) {
                    throw new IOException("no contiene la totalidad de campos, esta poseé " + linea.length + "/" + TOTAL_CAMPOS + " campos.");
                }

                
                
                clave = linea[0];
                if (clave.equals("")) {
                    campoActual = 1;
                    throw new IOException("no contiene la clave del producto.");
                }

                auxCantidad = linea[1];
                if (auxCantidad.equals("")) {
                    campoActual = 2;
                    throw new IOException("no contiene la cantidad del producto.");
                }

                //Se formatea la clave
                if (clave.indexOf('.') == -1) {
                    clave = formatter.format(Long.parseLong(clave));
                } else {
                    Pattern p = Pattern.compile("\\d+\\.\\d{1}");
                    Matcher m = p.matcher(clave);
                    coincidio = m.matches();

                    if (coincidio) { //si se tiene .1 en la clave
                        clave = formatterDeci2.format(Float.parseFloat(clave));
                    } else { //si se tiene .01 o similares en la clave
                        clave = formatterDeci.format(Float.parseFloat(clave));
                    }
                }

               
                
                campoActual = 1;
                valorActual = clave;
                ps = con.getConn().prepareStatement(INSERT_ABASTO);
                ps.setString(1, clave_uni);
                ps.setString(2, clave);

                //se valida la cantidad.
                campoActual = 2;

                cantidad = Integer.parseInt(auxCantidad);
                if (!(cantidad > 0)) {
                    throw new IOException("la cantidad no es valida.");
                }
                ps.setInt(3, cantidad);

                String[] validacion = validarCampos(lineaActual, clave,cantidad, lineaActual == 1);

                EstadoCheck check = EstadoCheck.fromInt(Integer.parseInt(validacion[0]));
                if (check == EstadoCheck.ERROR) {
                    estado = EstadoAbasto.NO_CARGO;
                    observacion = validacion[1];
                } else if (check == EstadoCheck.EXCEPCION) {
                    throw new IOException(validacion[1]);
                }
               
                ps.setString(4, observacion);
                ps.setInt(5, estado.value);
                resultado = ps.executeUpdate();

                if (resultado == 0) {
                    throw new SQLException("No se hizo la insercción del registro", ps.toString());
                }
            }
            terminoCorrectamente = true;
        } catch (SQLException ex) {
            String mensaje = String.format("En línea: %d, campo: %d, valor: %s, m: %s, sql: %s", lineaActual, campoActual, valorActual, ex.getMessage(), ex.getSQLState());
            Logger.getLogger(LeerCSV.class.getName()).log(Level.SEVERE, mensaje, ex);
            LoggerUsuario.getLoggerUsuario(LeerCSV.class.getName()).log(Level.SEVERE, mensaje);
            terminoCorrectamente = false;
        } catch (NumberFormatException | FileNotFoundException e) {
            String mensaje = String.format("En línea: %d, campo: %d, valor: %s, m: %s", lineaActual, campoActual, valorActual, e.getMessage());
            Logger.getLogger(LeerCSV.class.getName()).log(Level.SEVERE, mensaje, e);
            LoggerUsuario.getLoggerUsuario(LeerCSV.class.getName()).log(Level.SEVERE, mensaje);
            terminoCorrectamente = false;
        } catch (IOException ex) {
            if (br != null) {
                br.close();
            }
            String mensaje = String.format("En línea: %d, campo: %d, valor: %s, m: %s", lineaActual, campoActual, valorActual, ex.getMessage());
            Logger.getLogger(LeerCSV.class.getName()).log(Level.SEVERE, mensaje, ex);
            LoggerUsuario.getLoggerUsuario(LeerCSV.class.getName()).log(Level.SEVERE, mensaje);
            terminoCorrectamente = false;
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(ConectionDB.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }
        
        return terminoCorrectamente;
    }
    
    
    public static String[] validarCampos(int lineaActual, String clave, int auxCantidad, boolean reload) {

        PreparedStatement ps;
        ResultSet consulta;
        EstadoCheck estado = EstadoCheck.SIN_ERROR;
        String mensaje = "", valorActual;
        String[] resultado = {String.valueOf(estado.value), mensaje};

        int campoActual;

        ConectionDB con = new ConectionDB();

        try {

            if ((claves.isEmpty())) {
                reload = true;
            }

            if (reload) {
                con.conectar();
                
                ps = con.getConn().prepareStatement(selectProduc);
                consulta = ps.executeQuery();

                while (consulta.next()) {
                    claves.add(consulta.getString(1));
                }

            }

            campoActual = 1;
            valorActual = clave;
            if (!claves.contains(clave)) {
                mensaje = "La clave de este producto no está en el catálogo.";
                String auxMensaje = String.format("En linea: %d, campo: %d, valor: %s, m: %s", lineaActual, campoActual, valorActual, mensaje);
                LoggerUsuario.getLoggerUsuario(LeerCSV.class.getName()).log(Level.INFO, auxMensaje);
                estado = EstadoCheck.ERROR;
            }


        } catch (SQLException ex) {
            Logger.getLogger(LeerCSV.class.getName()).log(Level.SEVERE, ex.getSQLState(), ex);
            mensaje = "Error al consultar la base datos.";
            estado = EstadoCheck.EXCEPCION;
        }

        if (reload) {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(LeerCSV.class.getName()).log(Level.SEVERE, ex.getSQLState(), ex);
                mensaje = "Error al cerrar la base datos.";
                estado = EstadoCheck.EXCEPCION;
            }
        }

        resultado[0] = String.valueOf(estado.value);
        resultado[1] = mensaje;

        return resultado;
    }
    
    
}
