/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.IOException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Carga de abasto csv
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class TraerAbasto {

    private final String DELETE_ABASTO = "DELETE FROM carga_abasto;";
    private final String INSERT_ABASTO = "INSERT INTO carga_abasto (clave, lote, caducidad, cantidad, origen, cb, cargado, observacion) VALUES (?,?,?,?,?,?,?,?);";
    private static final String SELECT_ORIGEN = "SELECT origen.id_ori FROM origen";
    private static final String SELECT_PRODUCTOS = "SELECT productos.cla_pro FROM productos WHERE productos.f_status LIKE 'A'";
    private static final String SELECT_ABASTO_WEB = "SELECT A.clave, A.lote, A.caducidad, A.cantidad, A.cb, A.origen FROM abasto AS A WHERE A.unidad = ? AND A.`status` = 1 ";
    private static final String SELECT_ABASTO_EXISTE = "SELECT A.folioRemision FROM abasto AS A WHERE A.unidad = ? AND A.`status` = 1 ";
    private static final String UPDATE_ABASTO_CERRAR = "SELECT A.folioRemision FROM abasto AS A WHERE A.unidad = ? AND A.`status` = 1 ";

    private static ArrayList<Integer> origenes = new ArrayList();
    private static ArrayList<String> claves = new ArrayList();

    public static enum EstadoAbasto {

        CARGO(1), NO_CARGO(0);
        private final int value;

        private EstadoAbasto(int value) {
            this.value = value;
        }

        public static EstadoAbasto fromInt(int x) {
            switch (x) {
                case 1:
                    return CARGO;
                case 0:
                    return NO_CARGO;
            }
            return null;
        }
    }

    public static enum EstadoCheck {

        SIN_ERROR(0), ERROR(1), EXCEPCION(2);
        private final int value;

        private EstadoCheck(int value) {
            this.value = value;
        }

        public static EstadoCheck fromInt(int x) {
            switch (x) {
                case 0:
                    return SIN_ERROR;
                case 1:
                    return ERROR;
                case 2:
                    return EXCEPCION;
            }
            return null;
        }

    }

    public void cerrarAbasto() {

    }

    public String comprobarAbasto(String unidad) {
        ConectionDBnube conNube = new ConectionDBnube();
        PreparedStatement psNube;
        ResultSet rsNube;
        String folio = "";

        try {
            conNube.conectar();
            psNube = conNube.getConn().prepareStatement(SELECT_ABASTO_EXISTE);
            psNube.setString(1, unidad);
            rsNube = psNube.executeQuery();
            if (rsNube.next()) {
                folio = rsNube.getString(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(TraerAbasto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return folio;
    }

    public boolean traerAbastoWeb(String unidad) {

        DateFormat df2 = new SimpleDateFormat("yyyy-mm-dd");
        DecimalFormat formatter = new DecimalFormat("0000");
        DecimalFormat formatterDeci = new DecimalFormat("0000.00");
        DecimalFormat formatterDeci2 = new DecimalFormat("0000.0");
        ConectionDB con = new ConectionDB();
        ConectionDBnube conNube = new ConectionDBnube();

        DecimalFormatSymbols puntoSimbolo = new DecimalFormatSymbols(Locale.US);
        formatterDeci.setDecimalFormatSymbols(puntoSimbolo);
        formatterDeci2.setDecimalFormatSymbols(puntoSimbolo);

        int lineaActual = 0;
        int campoActual = 0;
        String valorActual = "";
        boolean coincidio, terminoCorrectamente;
        EstadoAbasto estado;

        PreparedStatement ps;
        PreparedStatement psNube;
        ResultSet rsNube;

        try {
            con.conectar();
            conNube.conectar();
            ps = con.getConn().prepareStatement(DELETE_ABASTO);
            ps.executeUpdate();

            psNube = conNube.getConn().prepareStatement(SELECT_ABASTO_WEB);
            psNube.setString(1, unidad);
            rsNube = psNube.executeQuery();

            String clave, lote, auxCantidad, auxCaducidad,
                    auxOrigen, codigoBarras, observacion;
            Date caducidad;
            int cantidad, origen, resultado;

            while (rsNube.next()) {
                lineaActual += 1;
                campoActual = 0;
                valorActual = "";
                estado = EstadoAbasto.CARGO;
                observacion = "";

                clave = rsNube.getString("clave");
                if (clave.equals("")) {
                    campoActual = 1;
                    throw new IOException("no contiene la clave del producto.");
                }

                lote = rsNube.getString("lote");
                if (lote.equals("")) {
                    campoActual = 3;
                    throw new IOException("no contiene el lote del producto.");
                }

                auxCaducidad = rsNube.getString("caducidad");
                if (auxCaducidad.equals("")) {
                    campoActual = 4;
                    throw new IOException("no contiene la caducidad del producto.");
                }

                auxCantidad = rsNube.getString("cantidad");
                if (auxCantidad.equals("")) {
                    campoActual = 5;
                    throw new IOException("no contiene la cantidad del producto.");
                }

                auxOrigen = rsNube.getString("origen");
                if (auxOrigen.equals("")) {
                    campoActual = 6;
                    throw new IOException("no contiene el origen del producto.");
                }

                codigoBarras = rsNube.getString("cb");

                //Se formatea la clave
                if (clave.indexOf('.') == -1) {
                    clave = formatter.format(Long.parseLong(clave));
                } else {
                    Pattern p = Pattern.compile("\\d+\\.\\d{1}");
                    Matcher m = p.matcher(clave);
                    coincidio = m.matches();

                    if (coincidio) { //si se tiene .1 en la clave
                        clave = formatterDeci2.format(Double.parseDouble(clave));
                    } else { //si se tiene .01 o similares en la clave
                        clave = formatterDeci.format(Double.parseDouble(clave));
                    }
                }

                campoActual = 1;
                valorActual = clave;
                ps = con.getConn().prepareStatement(INSERT_ABASTO);
                ps.setString(1, clave);

                campoActual = 3;
                valorActual = lote;
                ps.setString(2, lote);

                campoActual = 4;
                valorActual = auxCaducidad;
                caducidad = new Date(df2.parse(auxCaducidad).getTime());
                ps.setDate(3, caducidad);

                //se valida la cantidad.
                campoActual = 5;

                cantidad = Integer.parseInt(auxCantidad);
                if (!(cantidad > 0)) {
                    throw new IOException("la cantidad no es valida.");
                }
                ps.setInt(4, cantidad);

                campoActual = 6;
                valorActual = auxOrigen;
                origen = Integer.parseInt(auxOrigen);
                ps.setInt(5, origen);

                campoActual = 7;
                valorActual = codigoBarras;
                ps.setString(6, codigoBarras);
                String[] validacion = validarCampos(lineaActual, clave, caducidad, auxOrigen, lineaActual == 1);

                EstadoCheck check = EstadoCheck.fromInt(Integer.parseInt(validacion[0]));
                if (check == EstadoCheck.ERROR) {
                    estado = EstadoAbasto.NO_CARGO;
                    observacion = validacion[1];
                } else if (check == EstadoCheck.EXCEPCION) {
                    throw new IOException(validacion[1]);
                }
                ps.setInt(7, estado.value);
                ps.setString(8, observacion);

                resultado = ps.executeUpdate();

                if (resultado == 0) {
                    throw new SQLException("No se hizo la insercción del registro", ps.toString());
                }
            }
            terminoCorrectamente = true;
        } catch (SQLException ex) {
            String mensaje = String.format("En línea: %d, campo: %d, valor: %s, m: %s, sql: %s", lineaActual, campoActual, valorActual, ex.getMessage(), ex.getSQLState());
            Logger.getLogger(LeerCSV.class.getName()).log(Level.SEVERE, mensaje, ex);
            LoggerUsuario.getLoggerUsuario(LeerCSV.class.getName()).log(Level.SEVERE, mensaje);
            terminoCorrectamente = false;
        } catch (ParseException e) {
            String mensaje = String.format("En línea: %d, campo: %d, valor: %s, m: %s, offset: %d", lineaActual, campoActual, valorActual, e.getMessage(), e.getErrorOffset());
            Logger.getLogger(LeerCSV.class.getName()).log(Level.SEVERE, mensaje, e);
            LoggerUsuario.getLoggerUsuario(LeerCSV.class.getName()).log(Level.SEVERE, mensaje);
            terminoCorrectamente = false;
        } catch (IOException ex) {
            String mensaje = String.format("En línea: %d, campo: %d, valor: %s, m: %s", lineaActual, campoActual, valorActual, ex.getMessage());
            Logger.getLogger(LeerCSV.class.getName()).log(Level.SEVERE, mensaje, ex);
            LoggerUsuario.getLoggerUsuario(LeerCSV.class.getName()).log(Level.SEVERE, mensaje);
            terminoCorrectamente = false;
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(LeerCSV.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }

            try {
                conNube.cierraConexion();
            } catch (Exception ex) {
                Logger.getLogger(TraerAbasto.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return terminoCorrectamente;
    }

    /**
     * Metodo encargado de validar los campos al momento del abasto.
     *
     * @param lineaActual
     * @param clave
     * @param caducidad
     * @param auxOrigen
     * @param reload Indica si debe ser recargada la lista de origenes y claves.
     * @return Retorda el estado de la carga y el mensaje despues de la carga.
     */
    public static String[] validarCampos(int lineaActual, String clave, Date caducidad, String auxOrigen, boolean reload) {

        PreparedStatement ps;
        ResultSet consulta;
        EstadoCheck estado = EstadoCheck.SIN_ERROR;
        String mensaje = "", valorActual;
        String[] resultado = {String.valueOf(estado.value), mensaje};

        int campoActual, origen;

        ConectionDB con = new ConectionDB();

        try {

            if ((origenes.isEmpty()) || (claves.isEmpty())) {
                reload = true;
            }

            if (reload) {
                con.conectar();
                ps = con.getConn().prepareStatement(SELECT_ORIGEN);
                consulta = ps.executeQuery();

                while (consulta.next()) {
                    origenes.add(consulta.getInt(1));
                }

                ps = con.getConn().prepareStatement(SELECT_PRODUCTOS);
                consulta = ps.executeQuery();

                while (consulta.next()) {
                    claves.add(consulta.getString(1));
                }

            }

            campoActual = 1;
            valorActual = clave;
            if (!claves.contains(clave)) {
                mensaje = "La clave de este producto no está en el catálogo.";
                String auxMensaje = String.format("En linea: %d, campo: %d, valor: %s, m: %s", lineaActual, campoActual, valorActual, mensaje);
                LoggerUsuario.getLoggerUsuario(LeerCSV.class.getName()).log(Level.INFO, auxMensaje);
                estado = EstadoCheck.ERROR;
            }

            campoActual = 4;
            valorActual = caducidad.toString();
            if (caducidad.before(new java.util.Date())) {
                mensaje += (!mensaje.equals("")) ? " La fecha de caducidad de este producto está caducada." : "La fecha de caducidad de este producto está caducado.";
                String auxMensaje = String.format("En linea: %d, campo: %d, valor: %s, m: %s", lineaActual, campoActual, valorActual, mensaje);
                LoggerUsuario.getLoggerUsuario(LeerCSV.class.getName()).log(Level.INFO, auxMensaje);
                estado = EstadoCheck.ERROR;
            }

            campoActual = 6;
            valorActual = auxOrigen;
            origen = Integer.parseInt(auxOrigen);
            if (!origenes.contains(origen)) {
                mensaje += (!mensaje.equals("")) ? " El origen no es valido." : "El origen no es valido.";
                String auxMensaje = String.format("En linea: %d, campo: %d, valor: %s, m: %s", lineaActual, campoActual, valorActual, mensaje);
                LoggerUsuario.getLoggerUsuario(LeerCSV.class.getName()).log(Level.INFO, auxMensaje);
                estado = EstadoCheck.EXCEPCION;
            }

        } catch (SQLException ex) {
            Logger.getLogger(LeerCSV.class.getName()).log(Level.SEVERE, ex.getSQLState(), ex);
            mensaje = "Error al consultar la base datos.";
            estado = EstadoCheck.EXCEPCION;
        }

        if (reload) {
            try {
                con.cierraConexion();
            } catch (SQLException ex) {
                Logger.getLogger(LeerCSV.class.getName()).log(Level.SEVERE, ex.getSQLState(), ex);
                mensaje = "Error al cerrar la base datos.";
                estado = EstadoCheck.EXCEPCION;
            }
        }

        resultado[0] = String.valueOf(estado.value);
        resultado[1] = mensaje;

        return resultado;
    }
}
