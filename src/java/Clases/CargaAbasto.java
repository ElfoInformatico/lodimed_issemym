/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Carga de abasto
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class CargaAbasto {

ConectionDB con = new ConectionDB();

public String cargaAbasto(String cla_uni, String id_usu, String fol_abasto) {
    String mensaje;
    try {
        con.conectar();
        boolean invIni = inventarioInicial(cla_uni);
        try {
            ResultSet rset = con.consulta("SELECT * FROM carga_abasto WHERE cargado = 1");
            while (rset.next()) {
                String det_pro = comparaInsumo(rset.getString(2), rset.getString(3), rset.getString(4), rset.getString(6));
                String id_inv = devuelveIndiceInventario(det_pro, cla_uni);
                String CB = comparaCB(rset.getString(2), rset.getString(3), rset.getString(4), rset.getString(7));
                int cantAmp = devuelveAmpInsumo(rset.getString("clave"), rset.getInt(5));
                if (CB.equals("0")) {
                    //Logger.getLogger(CargaAbasto.class.getName()).log(Level.INFO, String.format("CB: %s", CB));
                    insertaInexistenteCB(rset.getString(7), rset.getString(2), rset.getString(3), rset.getString(4));
                }
                if (invIni) {
                    insertaInicialInexistente(det_pro, cantAmp, cla_uni, fol_abasto, id_usu);
                }
                /*
                     * Para insumo nuevo
                 */
                if (id_inv.equals("NA")) {
                    //System.out.println("inexistente");
                    insertaInexistente(det_pro, cantAmp, cla_uni, fol_abasto, id_usu);
                } /*
                     * Para insumo existente
                 */ else {
                    //System.out.println("existente");
                    insertaExistente(det_pro, cantAmp, cla_uni, fol_abasto, id_usu);
                }

            }
            mensaje = "1";
        } catch (Exception e) {
            mensaje = e.getMessage();
            Logger.getLogger(CargaAbasto.class.getName()).log(Level.SEVERE, mensaje, e);
        }
    } catch (Exception e) {
        mensaje = e.getMessage();
        Logger.getLogger(CargaAbasto.class.getName()).log(Level.SEVERE, mensaje, e);
    } finally {
        try {
            if (con.estaConectada()) {
                con.cierraConexion();
            }
        } catch (SQLException ex) {
            Logger.getLogger(CargaAbasto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    return mensaje;
}

public String cargaAbastoRural(String cla_uni, String id_usu, String fol_abasto) throws SQLException {
    String mensaje = "1";

    String des_jur = "", des_uni = "", fecha = "", servicio = "", tipo_requerimiento = "", cobertura = "", tipo_unidad = "", claisem = "", localidad
              = "";
    con.conectar();
    ResultSet rset = con.consulta(
              "SELECT j.des_jur, u.des_uni, DATE_FORMAT(NOW(), '%Y-%m-%d') AS fecha, 'CONSULTA EXTERNA' AS servicio, 'PAQUETE' AS tipo_requerimiento, 'SEGURO POPULAR' AS cobertura, 'CENTRO DE SALUD RURAL' tipo_unidad, u.claisem, u.localidad FROM unidades u INNER JOIN jurisdicciones j ON u.clajur = j.cla_jur WHERE cla_uni = '"
              + cla_uni + "'");
    while (rset.next()) {
        des_jur = rset.getString(1);
        des_uni = rset.getString(2);
        fecha = rset.getString(3);
        servicio = rset.getString(4);
        tipo_requerimiento = rset.getString(5);
        cobertura = rset.getString(6);
        tipo_unidad = rset.getString(7);
        claisem = rset.getString(8);
        localidad = rset.getString(9);
    }
    rset = con.consulta(
              "select p.cla_lic as cla_lic, ca.caducidad as caducidad, ca.lote as lote, ca.origen as origen, ca.cantidad as cantidad, p.des_pro as des_pro FROM carga_abasto ca INNER JOIN productos p on ca.clave = p.cla_pro AND ca.cargado=1");
    while (rset.next()) {

        con.actualizar(
                  "INSERT INTO `reporte_rurales` (`JURISDICCION`, `UNIDAD`, `Fecha`, `Folio`, `Servicio`, `CLAVE_CB`, `Description`, `Lote`, `Caducidad`, `Cant_Sol`, `Cant_Sur`, `tipo_de_requerimiento`, `SUMINISTRO`, `COBERTURA`, `PRECIO_UNITARIO`, `TIPO_DE_UNIDAD`, `CLUES`, `Localidad`) VALUES ('"
                  + des_jur + "', '" + des_uni + "', '" + fecha + "', '" + fol_abasto + "', '" + servicio + "', '" + rset.getString("cla_lic") + "', '" + rset.
                  getString("des_pro") + "', '" + rset.getString("lote") + "', '" + rset.getString("caducidad") + "', '" + rset.getString("cantidad") + "', '"
                  + rset.getString("cantidad") + "', '" + tipo_requerimiento + "', 'MEDICAMENTO', '"
                  + cobertura + "', '3.39', '" + tipo_unidad + "', '" + claisem + "', '" + localidad + "')");

    }
    con.cierraConexion();

    return mensaje;
}

public String cargaTraspaso(String cla_uni, String id_usu, String fol_abasto) {
    String mensaje;
    try {
        con.conectar();
        boolean invIni = inventarioInicial(cla_uni);
        try {
            ResultSet rset = con.consulta("SELECT * FROM carga_abasto WHERE cargado = 1");
            while (rset.next()) {
                String det_pro = comparaInsumo(rset.getString(2), rset.getString(3), rset.getString(4), rset.getString(6));
                String id_inv = devuelveIndiceInventario(det_pro, cla_uni);
                String CB = comparaCB(rset.getString(2), rset.getString(3), rset.getString(4), rset.getString(7));
                int cantAmp = devuelveAmpInsumo(rset.getString("clave"), rset.getInt(5));
                if (CB.equals("0")) {
                    //Logger.getLogger(CargaAbasto.class.getName()).log(Level.INFO, String.format("CB: %s", CB));
                    insertaInexistenteCB(rset.getString(7), rset.getString(2), rset.getString(3), rset.getString(4));
                }
                if (invIni) {
                    insertaInicialInexistente(det_pro, cantAmp, cla_uni, fol_abasto, id_usu);
                }
                /*
                     * Para insumo nuevo
                 */
                if (id_inv.equals("NA")) {
                    //System.out.println("inexistente");
                    insertaInexistenteTraspaso(det_pro, cantAmp, cla_uni, fol_abasto, id_usu);
                } /*
                     * Para insumo existente
                 */ else {
                    //System.out.println("existente");
                    insertaExistenteTraspaso(det_pro, cantAmp, cla_uni, fol_abasto, id_usu);
                }

            }
            mensaje = "1";
        } catch (Exception e) {
            mensaje = e.getMessage();
            Logger.getLogger(CargaAbasto.class.getName()).log(Level.SEVERE, mensaje, e);
        }
    } catch (Exception e) {
        mensaje = e.getMessage();
        Logger.getLogger(CargaAbasto.class.getName()).log(Level.SEVERE, mensaje, e);
    } finally {
        try {
            if (con.estaConectada()) {
                con.cierraConexion();
            }
        } catch (SQLException ex) {
            Logger.getLogger(CargaAbasto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    return mensaje;
}

public int devuelveAmpInsumo(String cla_pro, int cant) {
    int cantTotal = cant;
    int amp = 1;
    try {
        ResultSet rset = con.consulta("select amp_pro from productos where cla_pro = '" + cla_pro + "'");
        while (rset.next()) {
            amp = rset.getInt("amp_pro");
        }
    } catch (SQLException ex) {
        Logger.getLogger(CargaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
    }
    cantTotal = cantTotal * amp;
    return cantTotal;
}

public String devuelveIndiceInventario(String det_pro, String cla_uni) {
    String id_inv = "NA";
    try {
        ResultSet rset = con.consulta("select id_inv from inventario where det_pro = '" + det_pro + "' and cla_uni = '" + cla_uni + "' ");
        while (rset.next()) {
            id_inv = rset.getString("id_inv");
        }
    } catch (SQLException ex) {
        Logger.getLogger(CargaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
    }
    return id_inv;
}

public String comparaInsumo(String clave, String lote, String cadu, String origen) {
    String det_pro = "NA";
    try {

        ResultSet rset = con.consulta("select det_pro from detalle_productos where cla_pro = '" + clave + "' and lot_pro = '" + lote + "' and cad_pro = '"
                  + cadu + "' and id_ori = '" + origen + "'  ");
        while (rset.next()) {
            det_pro = rset.getString("det_pro");
            //System.out.println(det_pro);
        }
    } catch (SQLException ex) {
        Logger.getLogger(CargaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
    }
    if (det_pro.equals("NA")) {
        creaDetalleProducto(clave, lote, cadu, origen);
        try {

            ResultSet rset = con.consulta("select det_pro from detalle_productos where cla_pro = '" + clave + "' and lot_pro = '" + lote + "' and cad_pro = '"
                      + cadu + "' and id_ori = '" + origen + "'  ");
            while (rset.next()) {
                det_pro = rset.getString("det_pro");
            }

        } catch (SQLException ex) {
            Logger.getLogger(CargaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        }
    }
    return det_pro;
}

public String comparaCB(String clave, String lote, String cadu, String cb) {
    int cont = 0;
    String CB = "0";
    try {

        ResultSet rset = con.consulta("select * from tb_codigob where f_cb='" + cb + "' and F_Clave='" + clave + "' and F_Lote='" + lote + "' and F_Cadu='"
                  + cadu + "'");
        while (rset.next()) {
            cont++;
        }
        CB = Integer.toString(cont);

    } catch (SQLException ex) {
        Logger.getLogger(CargaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
    }

    return CB;
}

public void insertaInexistente(String det_pro, int cant, String cla_uni, String fol_abasto, String id_usu) {
    try {
        con.insertar("insert into inventario values (NOW(), '" + cla_uni + "', '" + det_pro + "', '" + cant + "', '0', '0')");
        con.insertar("INSERT INTO kardex SELECT '0', id_rec, '" + det_pro + "', '" + cant + "', 'CARGA DE ABASTO', '" + fol_abasto
                  + "', NOW(), 'CARGA DE ABASTO', '" + id_usu
                  + "', '0' FROM receta JOIN pacientes ON receta.id_pac = pacientes.id_pac WHERE pacientes.nom_com = 'RECETA COLECTIVA' AND receta.id_usu = 1;");

    } catch (SQLException ex) {
        Logger.getLogger(CargaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
    }
}

public void insertaInexistenteTraspaso(String det_pro, int cant, String cla_uni, String fol_abasto, String id_usu) {
    try {
        con.insertar("insert into inventario values (NOW(), '" + cla_uni + "', '" + det_pro + "', '" + cant + "', '0', '0')");
        con.insertar("INSERT INTO kardex SELECT '0', id_rec, '" + det_pro + "', '" + cant + "', 'ENTRADA POR TRASPASO', '" + fol_abasto
                  + "', NOW(), 'ENTRADA POR TRASPASO', '" + id_usu
                  + "', '0' FROM receta JOIN pacientes ON receta.id_pac = pacientes.id_pac WHERE pacientes.nom_com = 'RECETA COLECTIVA' AND receta.id_usu = 1;");

    } catch (SQLException ex) {
        Logger.getLogger(CargaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
    }
}

public void insertaInicialInexistente(String det_pro, int cant, String cla_uni, String fol_abasto, String id_usu) {
    try {
        con.insertar("insert into inventario_inicial values (NOW(), '" + cla_uni + "', '" + det_pro + "', '" + cant + "', '0', '0')");
    } catch (SQLException ex) {
        Logger.getLogger(CargaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
    }
}

public void insertaInexistenteCB(String cb, String clave, String lote, String cadu) {
    try {
        con.insertar("insert into tb_codigob  values (0, '" + cb + "', '" + clave + "', '" + lote + "', '" + cadu + "')");

    } catch (SQLException ex) {
        Logger.getLogger(CargaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
    }
}

public void insertaExistente(String det_pro, int cant, String cla_uni, String fol_abasto, String id_usu) {
    int cant1 = 0;
    int cantf = 0;
    String dpro = "";
    try {
        ResultSet rset = con.consulta("select cant,det_pro from inventario where det_pro = '" + det_pro + "' and cla_uni = '" + cla_uni + "' ");
        while (rset.next()) {
            cant1 = rset.getInt(1);
            dpro = rset.getString(2);
        }
        if (!dpro.equals("")) {
            cantf = cant1 + cant;
            con.insertar("update inventario set cant = '" + cantf + "' where det_pro = '" + det_pro + "' and cla_uni ='" + cla_uni + "'");
        } else {
            con.insertar("insert into inventario values (NOW(), '" + cla_uni + "', '" + det_pro + "', '" + cant + "', 0, '0');");
        }

        con.insertar("INSERT INTO kardex SELECT '0', id_rec, '" + det_pro + "', '" + cant + "', 'CARGA DE ABASTO', '" + fol_abasto
                  + "', NOW(), 'CARGA DE ABASTO', '" + id_usu
                  + "', '0' FROM receta JOIN pacientes ON receta.id_pac = pacientes.id_pac WHERE pacientes.nom_com = 'RECETA COLECTIVA' AND receta.id_usu = 1;");

    } catch (SQLException ex) {
        Logger.getLogger(CargaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
    }
}

public void insertaExistenteTraspaso(String det_pro, int cant, String cla_uni, String fol_abasto, String id_usu) {
    int cant1 = 0;
    int cantf = 0;
    String dpro = "";
    try {
        ResultSet rset = con.consulta("select cant,det_pro from inventario where det_pro = '" + det_pro + "' and cla_uni = '" + cla_uni + "' ");
        while (rset.next()) {
            cant1 = rset.getInt(1);
            dpro = rset.getString(2);
        }
        if (!dpro.equals("")) {
            cantf = cant1 + cant;
            con.insertar("update inventario set cant = '" + cantf + "' where det_pro = '" + det_pro + "' and cla_uni ='" + cla_uni + "'");
        } else {
            con.insertar("insert into inventario values (NOW(), '" + cla_uni + "', '" + det_pro + "', '" + cant + "', 0, '0');");
        }

        con.insertar("INSERT INTO kardex SELECT '0', id_rec, '" + det_pro + "', '" + cant + "', 'ENTRADA POR TRASPASO', '" + fol_abasto
                  + "', NOW(), 'ENTRADA POR TRASPASO', '" + id_usu
                  + "', '0' FROM receta JOIN pacientes ON receta.id_pac = pacientes.id_pac WHERE pacientes.nom_com = 'RECETA COLECTIVA' AND receta.id_usu = 1;");

    } catch (SQLException ex) {
        Logger.getLogger(CargaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
    }
}

public boolean inventarioInicial(String cla_uni) {
    int renglones = 0;
    try {
        ResultSet rset = con.consulta("select count(*) from inventario_inicial where cla_uni = '" + cla_uni + "' ");
        while (rset.next()) {
            renglones = rset.getInt(1);
        }

    } catch (SQLException ex) {
        Logger.getLogger(CargaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
    }
    return renglones == 0;
}

public String creaDetalleProducto(String clave, String lote, String cadu, String ori) {
    try {
        con.insertar("insert into detalle_productos values('0', '" + clave + "', '" + lote + "', '" + cadu + "', '1', '" + ori + "', '0')");

    } catch (SQLException ex) {
        Logger.getLogger(CargaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
    }
    return "a";
}

public boolean buscaAbasto(String abasto) {
    boolean existe = false;
    try {
        ResultSet rset = con.consulta("select id_kardex from kardex where fol_aba = '" + abasto + "' ");
        while (rset.next()) {
            existe = true;
            break;
        }
    } catch (SQLException ex) {
        Logger.getLogger(CargaAbasto.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
    }
    return existe;
}

}
