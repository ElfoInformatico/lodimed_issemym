/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Creación de carpeta y administración de la session
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class LoggerUsuario {

    private static String rutaArchivoActual;
    private static String rutaCarpeta;
    private Logger logger;
    private static HashMap<String, LoggerUsuario> loogers = new HashMap<>();

    public LoggerUsuario(String name) {
        logger = Logger.getLogger("-user");
        rutaArchivoActual = LoggerUsuario.getRutaArchivoActual();
        File carpeta = new File(rutaCarpeta);

        try {
            if (!carpeta.exists()) {
                Logger.getLogger(LoggerUsuario.class.getName()).log(Level.INFO, "Creando carpeta en: {0}", rutaCarpeta);
                carpeta.mkdir();
                Logger.getLogger(LoggerUsuario.class.getName()).log(Level.INFO, "Carpeta creada en: {0}", rutaCarpeta);
            }

            FileHandler fh = new FileHandler(rutaArchivoActual);
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
            logger.setUseParentHandlers(false);

        } catch (SecurityException se) {
            String mensaje = String.format("Error creando carpeta en: %s, m: %s", rutaCarpeta, se.getMessage());
            Logger.getLogger(LoggerUsuario.class.getName()).log(Level.INFO, mensaje, se);
        } catch (IOException ex) {
            String mensaje = String.format("Error creando carpeta en: %s, m: %s", rutaCarpeta, ex.getMessage());
            Logger.getLogger(LoggerUsuario.class.getName()).log(Level.SEVERE, mensaje, ex);
        }

    }

    public static Logger getLoggerUsuario(String name) {

        if (loogers.containsKey(name)) {
            return loogers.get(name).logger;
        } else {
            LoggerUsuario l = new LoggerUsuario(name);
            loogers.put(name, l);
            return l.logger;
        }

    }

    public static String getRutaArchivoActual() {
        if (rutaArchivoActual == null) {
            String rutaHome = System.getProperty("user.home");
            rutaCarpeta = rutaHome + File.separator + "Logs_CEAPS";
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            rutaArchivoActual = rutaCarpeta + File.separator + dateFormat.format(new Date()) + ".log";
        }

        return rutaArchivoActual;
    }

}
