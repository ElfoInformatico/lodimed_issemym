/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Conexión base de datos Nube
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class ConectionDBnube {

//variables miembro
    private String usuario;
    private String clave;
    private String url;
    private String driverClassName;
    private Connection conn = null;
    private Statement estancia;
    public Object objeto;

    public String id_medico;
    

    public ConectionDBnube(String usuario, String clave, String url, String driverClassName, Statement estancia, Object objeto, String id_medico) {
        this.usuario = usuario;
        this.clave = clave;
        this.url = url;
        this.driverClassName = driverClassName;
        this.estancia = estancia;
        this.objeto = objeto;
        this.id_medico = id_medico;
    }

//CONSTRUCTORES 
    //Constructor que toma los datos de conexion por medio de parametros
    public ConectionDBnube(String usuario, String clave, String url, String driverClassName) {
        this.usuario = usuario;
        this.clave = clave;
        this.url = url;
        this.driverClassName = driverClassName;
    }

    //Constructor que crea la conexion sin parametros con unos definidos en la clase
    //(meter los datos correspondientes)
    public ConectionDBnube() {
        //poner los datos apropiados
        this.usuario = "ceaps_edomex";
        this.clave = "CFyNJRaE042N!ED";
        this.url = "jdbc:mysql://gnklb.zerebro.mx:3306/ceaps_edomex?zeroDateTimeBehavior=convertToNull&useUnicode=yes&characterEncoding=UTF-8";
        this.driverClassName = "org.gjt.mm.mysql.Driver";


    }

    //metodos para recuperar los datos de conexion
    public String getClave() {
        return clave;
    }

    public String getUrl() {
        return url;
    }

    public String getUsuario() {
        return usuario;
    }

    public Connection getConn() {
        return conn;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    //metodos para establecer los valores de conexion
    public void setClave(String clave) {
        this.clave = clave;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUsuario(String usuario) throws SQLException {
        this.usuario = usuario;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

//la conexion propiamente dicha
    public void conectar() throws SQLException {
        try {
            Class.forName(this.driverClassName).newInstance();
            this.conn = DriverManager.getConnection(this.url, this.usuario, this.clave);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            Logger.getLogger(ConectionDBnube.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        } catch (SQLException ex) {
            Logger.getLogger(ConectionDBnube.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
        } 
    }
    //Cerrar la conexion

    public void cierraConexion() throws SQLException {
        this.conn.close();
    }

    /**
     * Does some thing in old style.
     *
     * @deprecated use {@link #PreparedStatement} instead.
     */
    @Deprecated
    public ResultSet consulta(String consulta) throws SQLException {
        //System.out.println(consulta);
        this.estancia = (Statement) conn.createStatement();
        return this.estancia.executeQuery(consulta);
    }

    /**
     * Does some thing in old style.
     *
     * @deprecated use {@link #PreparedStatement} instead.
     */
    @Deprecated
    public void actualizar(String actualiza) throws SQLException {
        //System.out.println(actualiza);

        this.estancia = (Statement) conn.createStatement();
        estancia.executeUpdate(actualiza);
        //this.conn.commit();
    }
    
    /**
     * Does some thing in old style.
     *
     * @deprecated use {@link #PreparedStatement} instead.
     */
    @Deprecated
    public ResultSet borrar(String borra) throws SQLException {
        Statement st = (Statement) this.conn.createStatement();
        return (ResultSet) st.executeQuery(borra);
    }
    
    /**
     * Does some thing in old style.
     *
     * @deprecated use {@link #PreparedStatement} instead.
     */
    @Deprecated
    public void borrar2(String borra) throws SQLException {
        this.estancia = (Statement) conn.createStatement();
        estancia.executeUpdate(borra);
    }
    
    /**
     * Does some thing in old style.
     *
     * @deprecated use {@link #PreparedStatement} instead.
     */
    @Deprecated
    public int insertar(String inserta) throws SQLException {
        //System.out.println(inserta);
        Statement st = (Statement) this.conn.createStatement();
        return st.executeUpdate(inserta);
    }
    
    /**
     * Este metodo verifica que si conexión es nula o es esta abierta.
     * @return false en caso de que sea nula el objeto de conexion o este cerrada,
     * true en caso de que aún este abierta.
     */
    public boolean estaConectada() {
        if (this.conn == null){
            return false;
        }
        try {
            return !this.conn.isClosed();
        } catch (SQLException ex) {
            Logger.getLogger(ConectionDBnube.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            return false;
        }
    }
}