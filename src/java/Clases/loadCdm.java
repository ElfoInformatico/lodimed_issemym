/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Carga del consumo declarado mensual
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class loadCdm {

    private final String DELETE_CDM = "DELETE FROM tbcdm;";
    
   
    private final int TOTAL_CAMPOS = 2;

    public static enum EstadoAbasto {

        CARGO(1), NO_CARGO(0);
        private final int value;

        private EstadoAbasto(int value) {
            this.value = value;
        }

        public static EstadoAbasto fromInt(int x) {
            switch (x) {
                case 1:
                    return CARGO;
                case 0:
                    return NO_CARGO;
            }
            return null;
        }
    }

    public static enum EstadoCheck {

        SIN_ERROR(0), ERROR(1), EXCEPCION(2);
        private final int value;

        private EstadoCheck(int value) {
            this.value = value;
        }

        public static EstadoCheck fromInt(int x) {
            switch (x) {
                case 0:
                    return SIN_ERROR;
                case 1:
                    return ERROR;
                case 2:
                    return EXCEPCION;
            }
            return null;
        }

    }

    public boolean leeCSV(String ruta) throws IOException {
        ConectionDB con = new ConectionDB();
        String cvsSplitBy = ",";
        int lineaActual = 0;
        int campoActual = 0;
        String valorActual = "";
        boolean terminoCorrectamente;
        BufferedReader br;
        try {
            String line;
            con.conectar();
            PreparedStatement ps;

            ps = con.getConn().prepareStatement(DELETE_CDM);
            ps.executeUpdate();

            br = new BufferedReader(new FileReader(ruta));

            String servicio;
            int cant;
            //System.out.println(System.getProperty("user.home"));
            while ((line = br.readLine()) != null) {
                lineaActual += 1;
                campoActual = 0;
                valorActual = "";

                String[] linea = line.split(cvsSplitBy);
                if (linea.length != TOTAL_CAMPOS) {
                    throw new IOException("no contiene la totalidad de campos, esta poseé " + linea.length + "/" + TOTAL_CAMPOS + " campos.");
                }

                servicio = linea[0];
                if (servicio.equals("")) {
                    campoActual = 1;
                    throw new IOException("no contiene la clave del producto.");
                }
                
                cant = Integer.parseInt(linea[1]);
                

                con.insertar("INSERT INTO tbcdm (cla_pro, FCdm) VALUES ('"+servicio+"',"+cant+")");
                con.insertar("UPDATE tbcdm SET cla_pro=CONCAT('0',cla_pro) WHERE  CHAR_LENGTH(cla_pro)=3;");
                

            }
            terminoCorrectamente = true;
        } catch (SQLException ex) {
            String mensaje = String.format("En línea: %d, campo: %d, valor: %s, m: %s, sql: %s", lineaActual, campoActual, valorActual, ex.getMessage(), ex.getSQLState());
            Logger.getLogger(LeerCSV.class.getName()).log(Level.SEVERE, mensaje, ex);
            LoggerUsuario.getLoggerUsuario(LeerCSV.class.getName()).log(Level.SEVERE, mensaje);
            terminoCorrectamente = false;
        } finally {
            if (con.estaConectada()) {
                try {
                    con.cierraConexion();
                } catch (SQLException ex) {
                    Logger.getLogger(LeerCSV.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                }
            }

        }

        return terminoCorrectamente;
    }

}
