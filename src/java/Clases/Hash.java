/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Permite la verificación de los campos enviados en la petición HTTP.
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class Hash {

    /**
     * Genera un Hash que sirve para ofuscar la información enviada.
     *
     * @param id de la sesión.
     * @return
     */
    public static String SessionToHash(String id) {
        int len = id.length();
        String nueva = id.substring(len - 4, len) + id.substring(2, 6);
        nueva = "pu" + nueva.toLowerCase() + "ca";

        return nueva;
    }

    /**
     *
     * @param id
     * @param original
     * @param parametros
     * @return
     */
    public static boolean checkMD5(String id, String original, ArrayList<String> parametros) {

        StringBuilder hexString;
        MessageDigest md;
        String nueva = "";
        String auxMD5 = "";
        String basura = Hash.SessionToHash(id);

        for (String parametro : parametros) {
            hexString = new StringBuilder();
            try {
                md = MessageDigest.getInstance("MD5");
                byte[] hash = md.digest(parametro.getBytes());

                for (int i = 0; i < hash.length; i++) {
                    if ((0xff & hash[i]) < 0x10) {
                        hexString.append("0").append(Integer.toHexString((0xFF & hash[i])));
                    } else {
                        hexString.append(Integer.toHexString(0xFF & hash[i]));
                    }
                }
                auxMD5 = hexString.toString();
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(Hash.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }

            nueva += basura + auxMD5;
        }

        return original.equals(nueva);
    }
}
