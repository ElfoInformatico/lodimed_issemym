/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import Impl.operacionesImpl;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Envío de requerimiento por correo
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class enviaReq {

    ConectionDB con = new ConectionDB();

    public boolean EnviaReq(String ruta, String nombre, String unidad) {
        try {
            String uniName = "";
            con.conectar();
            SimpleDateFormat df2 = new java.text.SimpleDateFormat("yyyy/MM/dd:HH-mm");
            Date fecha = new Date();
            /* TODO output your page here. You may use following sample code. */
            Properties props = new Properties();
            props.setProperty("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.user", "ricardo.wence@gnkl.mx");
            props.setProperty("mail.smtp.auth", "true");

            String qryUni = "SELECT des_uni FROM unidades WHERE cla_uni=?;";

            PreparedStatement ps;
            ps = con.getConn().prepareStatement(qryUni);
            ps.setString(1, unidad);
            ResultSet rs2 = ps.executeQuery();
            while (rs2.next()) {
                uniName = rs2.getString("des_uni");
            }
            MimeMultipart multi = new MimeMultipart();
            BodyPart archivo = new MimeBodyPart();
            // Preparamos la sesion
            Session session = Session.getDefaultInstance(props);

            // Construimos el mensaje
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress("ricardo.wence@gnkl.mx"));
            message.addRecipient(
                    Message.RecipientType.TO,
                    new InternetAddress("elideth.martinez@gnkl.mx"));//Aqui se pone la direccion a donde se enviara el correo
            message.addRecipient(
                    Message.RecipientType.TO,
                    new InternetAddress("andres.lopez@gnkl.mx"));
            message.addRecipient(
                    Message.RecipientType.TO,
                    new InternetAddress("yolanda.orozco@gnkl.mx"));
            message.addRecipient(
                    Message.RecipientType.TO,
                    new InternetAddress("oscar.arellano@gnkl.mx"));
            message.addRecipient(
                    Message.RecipientType.TO,
                    new InternetAddress("gustavo.arriola@gnkl.mx"));
            message.setSubject("Requerimiento de la unidad:" + uniName + " " + df2.format(fecha));
            message.setText("");

            archivo.setDataHandler(new DataHandler(new FileDataSource(ruta)));
            archivo.setFileName(nombre);
            multi.addBodyPart(archivo);
            message.setContent(multi);

            // Lo enviamos.
            Transport t = session.getTransport("smtp");
            t.connect("ricardo.wence@gnkl.mx", "ricardo.wence+111");
            t.sendMessage(message, message.getAllRecipients());
            // Cierre.
            t.close();
            return true;
        } catch (Exception e) {
            Logger.getLogger(enviaReq.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return false;
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(operacionesImpl.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }
    }

}
