/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servletCorreo;

import Clases.ConectionDB;
import java.nio.file.Files;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.annotation.WebServlet;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.servlet.jsp.JspFactory;

/**
 * Proceso de envío de correos
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
@WebServlet(name = "CorreoGmail", urlPatterns = {"/servletCorreo"})
public class CorreoGmail extends HttpServlet {

    //private final String[] cuentas_soporte = {"sebastian.cardona3@udea.edu.co"};
    private final String[] cuentas_soporte = {"lodimedfarm@gmail.com"};
    private final String SELECT_UNIDAD = "SELECT unidades.des_uni FROM unidades INNER JOIN usuarios ON usuarios.cla_uni = unidades.cla_uni WHERE usuarios.id_usu = ?;";
    private String cuenta_correo;
    private String nombre;
    private String comentario;
    private String qry;
    ConectionDB con;
    private SimpleDateFormat df;
    private SimpleDateFormat df3;
    private String telefono;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws javax.mail.MessagingException
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, MessagingException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        df = new java.text.SimpleDateFormat("yyyy-MM-dd");
        df3 = new java.text.SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        String fechaContacto = df.format(date) + " " + df3.format(date);
        String nombreUnidad = "";
        boolean error = false;

        try {
            qry = "insert into contactos values(0,'" + nombre + "','" + cuenta_correo + "','" + comentario + "','" + df.format(date) + "','" + df3.format(date) + "');";
            con = new ConectionDB();
            con.conectar();
            con.insertar(qry);

        } catch (SQLException ex) {
            Logger.getLogger(CorreoGmail.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            error = true;
        } finally {
            try {
                if (con.estaConectada()) {
                    con.cierraConexion();
                }
            } catch (SQLException ex) {
                Logger.getLogger(CorreoGmail.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
            }
        }

        try (PrintWriter out = response.getWriter()) {
            Properties props = new Properties();
            props.setProperty("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.user", "lodimed2018@gmail.com");
            props.setProperty("mail.smtp.auth", "true");

            // Preparamos la sesion
            Session session = Session.getDefaultInstance(props);

            // Construimos el datosEquipo
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress("soporte@gnkl.mx"));
            message.addRecipient(
                    Message.RecipientType.TO,
                    new InternetAddress(cuenta_correo));//Aqui se pone la direccion a donde se enviara el correo
            message.setSubject("Recibimos su Información / Área de sistemas");
            message.setText(
                    "Recibimos sus comentarios, gracias por enviarlos y esperamos seguir en contacto. \nMEDALFA - Área de Desarrollo de Aplicaciones WEB \n "
                    + " \n Enviado en la fecha: " + date + ""
            );

            try {
                // Lo enviamos al usuario.
                Transport t = session.getTransport("smtp");
                t.connect("lodimed2018@gmail.com", "MedalfaHH2018");
                t.sendMessage(message, message.getAllRecipients());

                //Construimos el de soporte si es necesario.
                boolean esSoporte = request.getParameter("esSoporte") != null ? Boolean.parseBoolean(request.getParameter("esSoporte")) : false;
                message = new MimeMessage(session);
                message.setFrom(new InternetAddress("soporte@gnkl.mx"));
                for (String cuenta : cuentas_soporte) {
                    message.addRecipient(
                            Message.RecipientType.TO,
                            new InternetAddress(cuenta));
                }

                String mensajeUsuario = String.format("Datos del contacto:\n"
                        + "\n- Nombre: %s\n- Fecha del envio: %s\n- Teléfono: %s\n- E-mail: %s\n- Comentario: '%s'\n", nombre, fechaContacto, telefono, cuenta_correo,
                        comentario);

                if (esSoporte) {
                    int id_usuario = Integer.parseInt(request.getParameter("id"));

                    PreparedStatement ps;
                    ResultSet consulta;
                    try {
                        con.conectar();
                        ps = con.getConn().prepareStatement(SELECT_UNIDAD);
                        ps.setInt(1, id_usuario);
                        consulta = ps.executeQuery();
                        if (!consulta.isBeforeFirst()) {
                            throw new SQLException(String.format("Para el id: '%d' no fue encontrada una unidad.", id_usuario), ps.toString());
                        }

                        consulta.next();
                        nombreUnidad = consulta.getString(1);
                    } catch (SQLException ex) {
                        nombreUnidad = ex.getMessage();
                        Logger.getLogger(CorreoGmail.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                        error = true;
                    } finally {
                        try {
                            if (con.estaConectada()) {
                                con.cierraConexion();
                            }
                        } catch (SQLException ex) {
                            Logger.getLogger(CorreoGmail.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getSQLState()), ex);
                        }
                    }

                    String versionJava = System.getProperty("java.version");
                    String ubicacionJava = System.getProperty("java.home");
                    String versionVM = System.getProperty("java.vm.version");
                    String sistema = System.getProperty("os.name");
                    String versionSO = System.getProperty("os.version");
                    String homeUsuario = System.getProperty("user.home");
                    String nombreServer = request.getServerName();
                    String puertoServer = String.valueOf(request.getServerPort());
                    String versionJSP = JspFactory.getDefaultFactory().getEngineInfo().getSpecificationVersion();
                    String tomcat = getServletContext().getServerInfo();
                    String aplicacion = "";
                    PrintService imprePredet = PrintServiceLookup.lookupDefaultPrintService();
                    String nombreImpresora = imprePredet.getName();
                    Properties prop = new Properties();
                    InputStream input = null;

                    try {

                        prop.load(getClass().getResourceAsStream("/servletCorreo/version.properties"));

                        aplicacion = prop.getProperty("aplicacion.version");
                        aplicacion += prop.getProperty("compilacion.numero");

                    } catch (IOException ex) {
                        Logger.getLogger(CorreoGmail.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                        error = true;
                    } finally {
                        if (input != null) {
                            try {
                                input.close();
                            } catch (IOException ex) {
                                Logger.getLogger(CorreoGmail.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                            }
                        }
                    }

                    //Información tomcat.
                    String datosEquipo = "\n-- Java --\n"
                            + " Versión: %s\n Ubicación: %s\n VM: %s\n"
                            + "\n-- SO --\n"
                            + " Nombre: %s\n Versión: %s\n Home: %s\n"
                            + "\n-- Server --\n"
                            + " IP: %s\n Puerto: %s\n JSP: %s\n Tomcat: %s\n"
                            + "\n-- Impresora --\n"
                            + " Nombre: %s\n";
                    datosEquipo = String.format(datosEquipo, versionJava, ubicacionJava, versionVM,
                            sistema, versionSO, homeUsuario,
                            nombreServer, puertoServer, versionJSP, tomcat, nombreImpresora);

                    message.setSubject(String.format("Solicitud soporte  - %s", nombreUnidad));
                    BodyPart parteTexto = new MimeBodyPart();

                    String userAgent = request.getParameter("navegador");
                    userAgent = String.format("http://www.useragentstring.com/?%s&getJSON=all", userAgent);
                    parteTexto.setText(String.format("%s\n"
                            + "\nDatos del equipo:\n"
                            + "%s"
                            + "\nVersión del sistema: %s\n"
                            + "\nUserAgent: %s\n", mensajeUsuario, datosEquipo, aplicacion, userAgent));

                    Multipart multipart = new MimeMultipart();

                    multipart.addBodyPart(parteTexto);

                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    String fecha = dateFormat.format(new Date());
                    Path rutaRequest = Paths.get(getServletContext().getRealPath(""));
                    //System.out.println(rutaRequest.toString());
                    rutaRequest = rutaRequest.getParent().getParent();
                    //System.out.println("2:"+rutaRequest);
                    rutaRequest = rutaRequest.resolve("logs");
                    Path rutaPath = Paths.get("../logs");
                    String[] rutasLogs = {String.format("catalina.%s.log", fecha),
                        String.format("manager.%s.log", fecha)};

                    Path ruta;
                    for (String rutaLog : rutasLogs) {
                        BodyPart logs = new MimeBodyPart();
                        ruta = rutaRequest.resolve(rutaLog).toAbsolutePath().normalize();
//                        System.out.println("d: "+ruta.toString());
                        if (!Files.exists(ruta)) {
                            ruta = rutaPath.resolve(rutaLog).toAbsolutePath().normalize();
                             if (!Files.exists(ruta)) {
                                 continue;
                             }
                        }

//                        System.out.println("d2: "+ruta.toString());
                        DataSource source = new FileDataSource(ruta.toString());
                        logs.setDataHandler(new DataHandler(source));

                        //Los nombres del archivo no varian
                        logs.setFileName(rutaLog);

                        multipart.addBodyPart(logs);
                    }

                    message.setContent(multipart);

                } else {
                    message.setSubject("Solicitud soporte  - Login");
                    message.setText(mensajeUsuario);
                }
                t.sendMessage(message, message.getAllRecipients());

                // Cierre.
                t.close();
            } catch (MessagingException ex) {
                Logger.getLogger(CorreoGmail.class.getName()).log(Level.SEVERE, String.format("m: %s, sql: %s", ex.getMessage(), ex.getCause()), ex);
                error = true;
            }

            if (error) {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                response.getWriter().write("No pudo entregarse el correo al servidor de correos.");
                response.flushBuffer();

            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (MessagingException | SQLException ex) {
            Logger.getLogger(CorreoGmail.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            this.nombre = request.getParameter("txtf_nom");
            this.cuenta_correo = request.getParameter("txtf_cor");
            this.comentario = request.getParameter("txta_com");
            this.telefono = request.getParameter("txtf_tel");

            processRequest(request, response);

        } catch (MessagingException | SQLException ex) {
            Logger.getLogger(CorreoGmail.class
                    .getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
