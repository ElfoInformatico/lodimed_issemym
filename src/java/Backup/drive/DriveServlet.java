/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Backup.drive;

import Backup.AdministrarBackup;
import Clases.ConectionDB;
import Modelos.DriveManager;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Carga de respaldo bd al drive
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
@WebServlet(name = "DriveServlet", urlPatterns = {"/drive"})
public class DriveServlet extends HttpServlet {

    private static HttpTransport HTTP_TRANSPORT;

    private static final List<String> SCOPES
            = Arrays.asList(DriveScopes.DRIVE);

    private DriveManager driveManager;

    static {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.driveManager = new DriveManager(new ConectionDB());
        super.service(req, resp);
    }

    private HttpRequestInitializer setHttpTimeout(final HttpRequestInitializer requestInitializer) {
        return new HttpRequestInitializer() {
            @Override
            public void initialize(HttpRequest httpRequest) throws IOException {
                requestInitializer.initialize(httpRequest);
                httpRequest.setConnectTimeout(5 * 60000);  // 5 minutes connect timeout
                httpRequest.setReadTimeout(5 * 60000);  // 5 minutes read timeout
            }
        };
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            //System.out.println("Llego al get");
            //InputStream key = getClass().getResourceAsStream("/Backup/drive/backupDrive.p12");
            //System.out.println(getServletContext().getRealPath("/WEB-INF/classes/Backup/drive/backupDrive.p12"));
            File key = new File(getServletContext().getRealPath("/WEB-INF/classes/Backup/drive/backupDrive.p12"));
            //GoogleCredential credential = GoogleCredential.fromStream(key, HTTP_TRANSPORT, GsonFactory.getDefaultInstance()).createScoped(SCOPES);
            GoogleCredential credential = new GoogleCredential.Builder()
                    .setTransport(HTTP_TRANSPORT)
                    .setJsonFactory(GsonFactory.getDefaultInstance())
                    .setServiceAccountId("backupdrive-141913@appspot.gserviceaccount.com")
                    .setServiceAccountPrivateKeyFromP12File(key)
                    .setServiceAccountScopes(SCOPES)
                    .setServiceAccountUser("soporte.sistemas@gnkl.mx")
                    .build();

            Drive service = new Drive.Builder(
                    HTTP_TRANSPORT, GsonFactory.getDefaultInstance(), setHttpTimeout(credential))
                    .setApplicationName("drive-backup")
                    .build();

            driveManager.service = service;

            doBackup(driveManager, resp, "farmacia/existencias.jsp");
        } catch (GeneralSecurityException ex) {
            Logger.getLogger(DriveServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Logger.getLogger(DriveServlet.class.getName()).log(Level.WARNING, "Llego al post a backup y no se puede procesar.");
    }

    public void doBackup(DriveManager drive, HttpServletResponse response, String url) throws IOException {

        AdministrarBackup backup = new AdministrarBackup(drive, getServletContext().getRealPath("/"));
        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {
            if (backup.Iniciar()) {
                out.println("<script>alert('Respaldo creado Correctamente')</script>");
            } else {
                out.println("<script>alert('Error al crear el respaldo')</script>");
            }
            out.println("<script>window.location.href='" + url + "'</script>");
        }

    }
}
