package Backup;

import Modelos.DriveManager;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Envío de respaldo de bd al drive
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class EnviaBackup {

    public boolean EnviaBackup(String ruta, DriveManager drive) {
        try {
            drive.con.conectar();

            drive.uploadFile(ruta);

            drive.con.cierraConexion();

            return true;
        } catch (SQLException | IOException e) {
            Logger.getLogger(EnviaBackup.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return false;
        }
    }
}
