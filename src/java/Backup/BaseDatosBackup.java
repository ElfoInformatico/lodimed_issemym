/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Backup;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.FileOutputStream;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Realización de respaldo bd
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public class BaseDatosBackup {

    private final int STREAM_BUFFER = 512000;

    public boolean backupDatabase(String host, String port, String user, String password, String dbname, String rootpath, String dbexepath) {

        boolean success = false;
        try {
            String dump = getServerDumpData(host, port, user, password, dbname, dbexepath);
            if (!dump.isEmpty()) {
                byte[] data = dump.getBytes();
                DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmm");
                Date date = new Date();

                Path currentPath = Paths.get(rootpath, String.format("%s_%s", dbname, dateFormat.format(date)));
                if (Files.notExists(currentPath)) {
                    Files.createDirectory(currentPath);
                }
                
                
                Path filepath = Paths.get(currentPath.toString(),  String.format("%s%s.sql", dbname, dateFormat.format(date)));
                File filedst = new File(filepath.toString());
                try (FileOutputStream dest = new FileOutputStream(filedst)) {
                    dest.write(data);
                }
                success = true;
            }
        } catch (Exception e) {
            Logger.getLogger(BaseDatosBackup.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return success;
    }

    public String getServerDumpData(String host, String port, String user, String password, String db, String mysqlpath) {

        StringBuilder dumpdata = new StringBuilder();
        String execline = mysqlpath;
        try {
            String command[] = new String[]{execline,
                "--host=" + host,
                "--port=" + port,
                "--user=" + user,
                "--password=" + password,
                db};

            ProcessBuilder pb = new ProcessBuilder(command);
            Process process = pb.start();
            try (InputStream in = process.getInputStream(); BufferedReader br = new BufferedReader(new InputStreamReader(in))) {

                int count;
                char[] cbuf = new char[STREAM_BUFFER];

                while ((count = br.read(cbuf, 0, STREAM_BUFFER)) != -1) {
                    dumpdata.append(cbuf, 0, count);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(BaseDatosBackup.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return "";
        }
        return dumpdata.toString();
    }
}
