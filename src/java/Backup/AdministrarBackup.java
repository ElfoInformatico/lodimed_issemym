package Backup;

import Modelos.DriveManager;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Admistración de respaldo bd
 *
 * @author GNKL SOFTWARE
 * @version 2.2
 */
public final class AdministrarBackup {

    private String DB_HOST;
    private String DB_NAME;
    private String DB_PORT;
    private String DB_USER;
    private String DB_PASS;
    private String FOLDER_NAME;
    private String DB_EXE;
    private String DESTINATION;

    public BigInteger timeNoted;
    String projectName = new String();
    private final DriveManager drive;
    private final String root;
    private Path folderPath;
    private Path zipPath;
    private String destinationPath;

    public AdministrarBackup(DriveManager drive, String path) {
        this.drive = drive;
        this.root = path;
        readConfiguration();
    }

    public void readConfiguration() {

        try {

            folderPath = Paths.get(this.root, "backup");
            if (Files.notExists(folderPath)) {
                Files.createDirectory(folderPath);
            }

            zipPath = Paths.get(this.root, "zip");
            if (Files.notExists(zipPath)) {
                Files.createDirectory(zipPath);
            }

            setDB_HOST("localhost");
            setDB_NAME("scr_transaccional");
            setDB_PORT("3306");
            setDB_USER("root");
            setDB_PASS("eve9397");
            setFOLDER_NAME(folderPath.toString());
            setDB_EXE("mysqldump");
            setDESTINATION(zipPath.toString());
        } catch (IOException ex) {
            Logger.getLogger(AdministrarBackup.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }

    }

    public Boolean databaseBackup() {
        return new BaseDatosBackup().backupDatabase(getDB_HOST(), getDB_PORT(), getDB_USER(), getDB_PASS(), getDB_NAME(), getFOLDER_NAME(), getDB_EXE());
    }

    public Boolean folderBackup() throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmm");
        Date nowtime = new Date();
        String path = Paths.get(folderPath.toString(),
                String.format("%s_%s", getDB_NAME(), dateFormat.format(nowtime))).toString();

        String filenameZip = String.format("%s_%s.zip", getDB_NAME(), dateFormat.format(nowtime));
        this.destinationPath = Paths.get(zipPath.toString(), filenameZip).toString();

        return new CarpetaZipBackup().zipFolder(path, this.destinationPath);
    }

    public Boolean enviaBackup() throws Exception {
        return new EnviaBackup().EnviaBackup(this.destinationPath, this.drive);
    }

    public boolean Iniciar() {
        try {
            if (this.databaseBackup()) {
                if (this.folderBackup()) {
                    Logger.getLogger(AdministrarBackup.class.getName()).log(Level.INFO, "Proceso de respaldo completo, iniciando envio por correo...");
                    if (this.enviaBackup()) {
                        Logger.getLogger(AdministrarBackup.class.getName()).log(Level.INFO, "Se envio correctamente el archivo.");
                        return true;
                    } else {
                        Logger.getLogger(AdministrarBackup.class.getName()).log(Level.INFO, "Error al enviar el archivo por correo");
                        return false;
                    }
                } else {
                    Logger.getLogger(AdministrarBackup.class.getName()).log(Level.SEVERE, "Folder backup process failed.");
                    return false;
                }
            } else {
                Logger.getLogger(AdministrarBackup.class.getName()).log(Level.SEVERE, "Process failed.");
                return false;
            }
        } catch (Exception e) {
            Logger.getLogger(AdministrarBackup.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return false;
    }

    /**
     * @return the DB_HOST
     */
    public String getDB_HOST() {
        return DB_HOST;
    }

    /**
     * @param DB_HOST the DB_HOST to set
     */
    public void setDB_HOST(String DB_HOST) {
        this.DB_HOST = DB_HOST;
    }

    /**
     * @return the DB_NAME
     */
    public String getDB_NAME() {
        return DB_NAME;
    }

    /**
     * @param DB_NAME the DB_NAME to set
     */
    public void setDB_NAME(String DB_NAME) {
        this.DB_NAME = DB_NAME;
    }

    /**
     * @return the DB_PORT
     */
    public String getDB_PORT() {
        return DB_PORT;
    }

    /**
     * @param DB_PORT the DB_PORT to set
     */
    public void setDB_PORT(String DB_PORT) {
        this.DB_PORT = DB_PORT;
    }

    /**
     * @return the DB_USER
     */
    public String getDB_USER() {
        return DB_USER;
    }

    /**
     * @param DB_USER the DB_USER to set
     */
    public void setDB_USER(String DB_USER) {
        this.DB_USER = DB_USER;
    }

    /**
     * @return the DB_PASS
     */
    public String getDB_PASS() {
        return DB_PASS;
    }

    /**
     * @param DB_PASS the DB_PASS to set
     */
    public void setDB_PASS(String DB_PASS) {
        this.DB_PASS = DB_PASS;
    }

    /**
     * @return the FOLDER_NAME
     */
    public String getFOLDER_NAME() {
        return FOLDER_NAME;
    }

    /**
     * @param FOLDER_NAME the FOLDER_NAME to set
     */
    public void setFOLDER_NAME(String FOLDER_NAME) {
        this.FOLDER_NAME = FOLDER_NAME;
    }

    /**
     * @return the DB_EXE
     */
    public String getDB_EXE() {
        return DB_EXE;
    }

    /**
     * @param DB_EXE the DB_EXE to set
     */
    public void setDB_EXE(String DB_EXE) {
        this.DB_EXE = DB_EXE;
    }

    /**
     * @return the DESTINATION
     */
    public String getDESTINATION() {
        return DESTINATION;
    }

    /**
     * @param DESTINATION the DESTINATION to set
     */
    public void setDESTINATION(String DESTINATION) {
        this.DESTINATION = DESTINATION;
    }
}
